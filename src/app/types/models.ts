import {
    Sku,
    CartItem,
    Category,
    CategoryMeta,
    CollectionDetail,
    OutOfStockInfoStateType,
} from "@models";

export interface CartItemDict {
    [itemId: number]: CartItem;
}

export interface SkuDict {
    [id: number]: Sku;
}

export interface CategoryDict {
    [id: number]: Category;
}

export interface CategoryMetaDict {
    [id: number]: CategoryMeta;
}

export interface CollectionDict {
    [id: number]: CollectionDetail;
}

export interface NotifiedDict {
    [skuId: number]: OutOfStockInfoStateType;
}
