import { AUTODEBIT_TOKEN_STATUS } from "@constants";

import * as SuprModels from "@models";

import { Frequency, SuprMaps } from "@types";
import { RewardsInfo, OfferDetails } from "@models";
import { RewardsDetails } from "@shared/models/user.model";
import { SELFSERVE } from "@models";

export namespace SuprApi {
    export interface ApiRes {
        statusCode: number;
        data?: any;
        statusMessage: string;
    }

    export interface ReqExtras {
        urlParams?: string | number;
        queryParams?: {
            [name: string]: string | number | boolean | string[] | number[];
        };
        service?: string;
    }

    export interface Meta {
        context?: string;
    }

    export interface HttpParams {
        silent?: boolean;
        meta?: Meta;
        retryCount?: number;
        useCustomErrorHandler?: boolean;
    }

    export interface VersionCheckRes extends ApiRes {
        status?: string;
        platform?: string;
        version?: number;
    }

    export interface AddressBody {
        type?: string;
        cityId?: number;
        flat_number?: string;
        building?: string;
        wing?: string;
        land_mark?: string;
        label?: string;
        cancel_all?: boolean;
        latitude?: string;
        longitude?: string;
    }

    export interface AddressRes {
        statusCode: number;
        statusMessage: string;
        data: SuprModels.Address;
    }

    export interface VacationBody {
        end_date: string;
        start_date: string;
    }

    export interface SubscriptionBody {
        quantity: number;
        frequency: Frequency;
    }

    export interface SubscriptionRes extends ApiRes {
        data: {
            subscriptions: SuprModels.Subscription[];
        };
    }

    export interface SubscriptionRechargeBody {
        id: number;
        recharge_quantity: number;
        payable_amount: number;
        paid_amount: number;
        coupon_code?: string;
    }

    export interface SubscriptionRechargeRes {
        refresh: boolean;
        schedule: ScheduleRes;
        recharge: SuprModels.Recharge;
        wallet_balance: number;
        balance: number;
        collect_feedback?: boolean;
    }

    export interface SubscriptionInstructionBody {
        schedule_date: string;
        quantity: number;
    }

    export interface SubscriptionInstructionRes extends ApiRes {
        data: {
            collect_feedback?: boolean;
        };
    }

    export interface AddonBody {
        delivery_date?: string;
        sku?: number;
        address?: number;
        quantity: number;
        payable_amount: number;
        paid_amount: number;
        coupon_code: string;
    }

    export interface VersionCheckBody {
        platform: string;
        version: number;
    }

    export interface FetchExperimentsBody {
        platform: string;
        version: number;
        userId: number;
    }

    export interface AddonRes {
        schedule: {
            [date: string]: SuprModels.Schedule[];
        };
        wallet_balance: number;
        collect_feedback?: boolean;
    }

    export interface ScheduleRes extends ApiRes {
        data: SuprModels.ScheduleApiData;
    }

    export interface PaymentRes {
        success: boolean;
        wallet_balance: number;
        supr_credits_balance?: number;
        recharge_amount?: number;
        benefit_applied?: boolean;
        benefits?: PaymentBenefits;
        buy_supr_credits?: boolean;
        message?: {
            error?: string[];
        };
    }

    export interface RecurringPaymentReq {
        razorpay_order_id: string;
        razorpay_payment_id: string;
        refund_payment: boolean;
        create_supr_pass: boolean;
        supr_pass_amount: number;
        validity_in_days: number;
        plan_id: number;
    }

    export interface RecurringPaymentRes {
        auto_debit_status: AUTODEBIT_TOKEN_STATUS;
        supr_pass_id?: number;
    }

    export interface BalanceRes {
        online: number;
        suprCredits?: number;
        expiry?: SuprModels.WalletExpiryDetails;
    }

    export interface PaymentBenefits {
        cashback_benefits?: BenefitProps[];
        discount_benefits?: BenefitProps[];
        supr_credits_benefits?: BenefitProps[];
    }

    export interface BenefitProps {
        id: number;
        benefit_value: number;
        expiry_datetime: string;
    }

    export interface CashCollectionBody {
        start_date: string;
        end_date: string;
        amount: number;
    }

    export interface DiscountBody {
        coupon_code: string;
        payable_amount: number;
        skuId: number;
        category: number;
        quantity: number;
        mode: string;
        date: string;
        repeatDays: boolean[];
        doorBell: boolean;
        recharge: number;
        totalQuantity: number;
        promoCode?: string;
        addressId: number;
        addToWallet?: number;
        logged?: boolean;
        subscriptionId?: number;
    }

    export interface DiscountRes {
        discount_amount: number;
        valid: boolean;
        message?: string;
    }

    export interface CreateRequestBody {
        text: string;
        type: string;
    }

    export interface UpdateSkuOOSInfoRequestBody {
        sku_id: number;
        state: SuprModels.OutOfStockInfoStateType;
    }

    export interface CreateRequestRes extends CreateRequestBody {
        id: number;
        customer: number;
    }

    export interface GeoCodeRes {
        success: boolean;
        statusCode: number;
        statusMessage: string;
        data?: {
            results: SuprMaps.GeoCodeResult[];
        };
    }

    export interface PlaceSuggestionRes {
        success: boolean;
        statusCode: number;
        statusMessage: string;
        data?: {
            suggestions: SuprMaps.PlaceSuggestion[];
        };
    }

    export interface FaqRes {
        statusCode: number;
        statusMessage: string;
        success?: boolean;
        data?: SuprModels.Faq;
    }

    export interface LocationStatusRes {
        statusCode: number;
        statusMessage: string;
        data: {
            city: {
                id?: number;
                name?: string;
            };
            collectArea: boolean;
            unServiceableMessage: string;
        };
    }

    export interface FeaturedRes extends ApiRes {
        data?: {
            featured?: SuprModels.Featured.List;
        };
    }

    export interface CategoryRes extends ApiRes {
        data: {
            category: SuprModels.Category;
        };
    }

    export interface CategoryListRes extends ApiRes {
        data: {
            categories: SuprModels.Category[];
        };
    }

    export interface EssentialsRes extends ApiRes {
        data: {
            essential: SuprModels.Category[];
        };
    }

    export interface VacationRes extends ApiRes {
        data: {
            vacation: SuprModels.Vacation;
            system_vacation?: SuprModels.SystemVacation;
        };
    }

    export interface CartBody {
        items: SuprModels.CartItem[];
        coupon_code?: string;
        uuid?: string;
        use_supr_credits?: boolean;
        auto_add_supr_access?: boolean;
        auto_apply_referral_code?: boolean;
        add_reward_sku_to_cart?: boolean;
        auto_add_reward_sku?: boolean;
    }

    export interface CartRes extends ApiRes {
        data: {
            cart: SuprModels.Cart;
        };
    }

    export interface CancelAddonRes extends ApiRes {
        data: {
            addon: SuprModels.Addon;
        };
    }

    export interface UpdateAddonRes extends ApiRes {
        data: {
            addon: SuprModels.Addon;
        };
    }

    export interface OrderHistoryData {
        message: string;
        sku_details: SuprModels.Sku[];
        order_history: SuprModels.OrderHistory[];
        is_multiple_delivery_proof_enabled?: boolean;
    }

    export interface OrderHistoryRes extends ApiRes {
        data: OrderHistoryData;
    }

    export interface PremiseSearchReq {
        cityId: number;
        latitude?: any;
        longitude?: any;
        searchTerm: string;
    }

    export interface PremiseSearchRes {
        id: number;
        name: string;
        streetAddress: string;
    }

    export interface PremiseAddressRes {
        id: number;
        name: string;
        streetAddress: string;
        category?: string;
        cityId?: number;
        landmark?: string;
        buildings: SuprModels.Building[];
    }

    export interface ApplyReferralCodeRes {
        applied?: boolean;
        referralId?: number;
        errorMessage?: string;
        referralCodeAppliedBanner?: SuprModels.Banner;
    }

    export interface FilterReferralContactsRes {
        contacts: string[];
    }

    export interface Profile {
        customer: {
            id: number;
            address: SuprModels.Address;
            email: string;
            firstName: string;
            lastName: string;
            mobileNumber: string;
        };
        subscriptions: SuprModels.Subscription[];
        walletBalance: number;
    }

    export interface ProfileQueryParams {
        address?: boolean;
        wallet?: boolean;
        subscriptions?: boolean;
        t_plus_one?: boolean;
    }

    export interface ProfileResData {
        walletBalance?: number;
        customer?: SuprModels.User;
        referralBenefit?: SuprModels.UserReferralBenefit;
        subscriptions?: SuprModels.Subscription[];
        tPlusOneEnabled?: boolean;
        walletVersion?: string;
        suprCreditsEnabled?: boolean;
        suprCreditsBalance?: number;
        suprPassEligible?: boolean;
        suprPassDetails?: SuprModels.ProfileLevelSuprPassDetails;
        referralEligible?: boolean;
        referralDetails?: ReferralDetails;
        rewardsEligible?: boolean;
        rewardsDetails: RewardsDetails;
        alternateDeliveryEligible?: boolean;
    }

    export interface ReferralDetails {
        showReferralCodeBlock?: boolean;
    }

    export interface UpdateSubscriptionInstructionResData {
        collect_feedback?: boolean;
    }

    export interface DeliveryStatusRes extends ApiRes {
        data: SuprModels.DeliveryStatus;
    }

    export interface CheckoutRes extends ApiRes {
        data: SuprModels.CartOrder | SuprModels.CheckoutError;
    }

    export interface PasswordReqBody {
        phone_number: string;
        is_whatsapp_optin?: boolean;
        resend?: boolean;
    }

    export interface PasswordRes {
        mobile: string;
        isWhatsAppOptIn?: boolean;
    }

    export interface RatingOptionsRes extends ApiRes {
        data: {
            options?: SuprModels.RatingOption[];
        };
    }

    export interface RatingFeedbackBody {
        feedbackContext: RatingFeedbackContext;
        meta: any;
    }

    export interface RatingFeedbackContext {
        optionId: number;
        reasons: number[];
        comments?: string;
    }

    export interface RatingActionBody {
        section: string;
        actionType: string;
        actionMeta?: any;
    }

    export interface MilestoneStartActionBody {
        user_milestone_id: number;
    }

    export interface SimilarProductsRes extends ApiRes {
        data: {
            skuList: number[];
        };
    }

    export interface SubscriptionTransactionsRes extends ApiRes {
        data: {
            transactions: SuprModels.SubscriptionTransaction[];
        };
    }

    export interface SuprPassInfoRes extends ApiRes {
        data: SuprModels.SuprPassInfo;
    }

    export interface ReferralInfoRes extends ApiRes {
        data: SuprModels.ReferralInfo;
    }

    export interface RewardsInfoRes extends ApiRes {
        data: RewardsInfo;
    }

    export interface ComplaintListV2Res extends ApiRes {
        data: SuprModels.ComplaintViewV2[];
    }

    export interface AnonymousTokenReqParams {
        city_id: number;
    }

    export interface AnonymousTokenRes {
        key: string;
        user_type: string;
    }

    export interface OfferDetailsRes extends ApiRes {
        data: OfferDetails;
    }

    export interface PaymentMethodsReqParams {
        coupon_code?: string;
        method?: string;
        wallets?: string[];
        banks?: string[];
        networks?: string[];
        types?: string[];
        issuers?: string[];
    }

    export interface PaymentMethodsResData {
        ui_version?: number;
        customer_id?: number;
        all_available_methods?: Array<string>;
        "payment-methods":
            | SuprModels.PaymentMethods
            | SuprModels.PaymentMethodsV2;
    }

    export interface PaymentMethodsRes extends ApiRes {
        data: PaymentMethodsResData;
    }

    export interface CustomPaymentCaptureReq {
        payment_id: string;
        order_id: string;
        partner_customer_id: string;
        payment_partner: string;
        amount: number;
        coupon_code: string;
        buy_supr_credits: boolean;
    }

    export interface CartOffersDiscoveryRes extends ApiRes {
        data: {
            offer_sections: SuprModels.OffersList;
        };
    }

    export interface WalletOffersDiscoveryRes extends ApiRes {
        data: {
            offer_sections: SuprModels.OffersList;
        };
    }

    export interface SkuSeachRes extends ApiRes {
        data: SuprModels.SkuSearchResponse;
    }

    export interface SelfServeRequest {
        node_id: number;
        order_date: string;
        order_id?: number;
        time_slot?: string;
        interaction_id: string;
    }

    export interface ComplaintCreationRequest {
        node_id: number;
        order_date: string;
        order_id?: number;
        time_slot?: string;
        interaction_id: string;
        [key: string]: SELFSERVE.Node_TYPE[] | string | number;
    }

    export interface PopularSkuSearchRes extends ApiRes {
        data: {
            searches: Array<SuprModels.PopularSkuSearch>;
        };
    }

    export interface DeliveryProofQueryParams {
        delivery_date: string;
        delivery_slot: string;
    }

    export interface DeliveryProofFeedbackPayload {
        image_type: string;
        delivery_date: string;
        delivery_slot: string;
        answer: {
            key: string;
            tags?: Array<string>;
        };
    }

    export interface DeliveryProofReportPayload {
        image_type: string;
        description?: string;
        delivery_date: string;
        issues: Array<string>;
    }

    export interface DeliveryProofRes extends ApiRes {
        response: Array<SuprModels.DeliveryProof>;
    }
}
