import { City } from "@shared/models";

export namespace SuprMaps {
    export interface StructuredFormatting {
        main_text: string;
        secondary_text: string;
    }

    export interface PlaceSuggestion {
        description: string;
        place_id: string;
        structured_formatting: StructuredFormatting;
    }

    export interface AddressComponent {
        long_name: string;
        short_name: string;
        types: string[];
    }

    export interface Location {
        lat: number;
        lng: number;
    }

    export interface Viewport {
        northeast: Location;
        southwest: Location;
    }

    export interface Geometry {
        location: Location;
        location_type: string;
        viewport: Viewport;
    }

    export interface PlusCode {
        compound_code: string;
        global_code: string;
    }

    export interface GeoCodeResult {
        address_components?: AddressComponent[];
        formatted_address?: string;
        geometry?: Geometry;
        place_id?: string;
        plus_code?: PlusCode;
        types?: string[];
    }

    export interface GeocodeInput {
        latlng?: string;
        place_id?: string;
        address?: string;
    }

    export interface MapAddress {
        latLng?: SuprMaps.Location;
        address?: string;
    }

    export interface MapComponentLocationInfo {
        location?: SuprMaps.Location;
        authorization?: string;
    }

    export interface MapSearchAddressInfo {
        city?: City;
        state?: string;
        addressText: string;
    }
}
