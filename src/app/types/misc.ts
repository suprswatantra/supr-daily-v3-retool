import { ErrorType } from "@types";
import { ToastButton } from "@ionic/core";

import {
    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE,
    SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES,
} from "@constants";

import {
    User,
    Subscription,
    SearchInstrumentation,
    RechargeCouponInfo,
} from "@models";

declare global {
    interface Window {
        plugins: {
            OneSignal: any;
            appsFlyer: any;
        };

        analytics: {
            identify: any;
            reset: any;
            track: any;
            page: any;
        };

        GA: {
            trackEvent: any;
            trackView: any;
            setUserId: any;
            startTrackerWithId: any;
            getAdId: any;
        };

        Freshchat: any;

        Freshbots: any;

        LaunchReview: any;
    }

    interface CordovaPlugins {
        idfa: any;
    }
}

export namespace Color {
    export interface HSL {
        h: number;
        s: number;
        l: number;
    }
}

export interface RazorpayOptions {
    key: string;
    currency: string;
    theme: { color: string; hide_topbar: boolean };
    amount: number;
    prefill: {
        email: string;
        contact: string | number;
        name: string;
        method?: string;
    };
    config?: { display: any };
}

export interface RazorpayAutoDebitOptions extends RazorpayOptions {
    customer_id: string;
    order_id: string;
    recurring: string;
}

export interface RazorpayRecurringPaymentOrderData {
    customer_id: string;
    order_id: string;
}

export interface RazorpayCustomPaymentOrderCreateReq {
    amount: number;
    payment_partner: string;
    coupon_code?: string;
    gateway_offer_id?: string;
    buy_supr_credits?: boolean;
}
export interface RazorpayCustomPaymentOrderData {
    payment_partner_customer_id: string;
    payment_partner_order_id: string;
    state: string;
}

export interface RazorpayWebviewCustomMsgObject {
    razorpay_payment_id?: string;
    razorpay_order_id?: string;
    success: boolean;
    error?: Error;
    dataType?: RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE | null;
}

export interface RazorpayPaymentMethodInfo {
    bank?: string;
    token?: string;
    method?: string;
    "card[cvv]"?: string;
    "card[name]"?: string;
    "card[number]"?: number;
    "card[expiry_year]"?: number;
    "card[expiry_month]"?: number;
    standardCheckout?: boolean;
}

export interface RazorpayPaymentMethodFormInfo
    extends RazorpayPaymentMethodInfo {
    vpa?: string;
    save?: number;
    card?: string;
    expiry?: string;
    network?: string;
    validateExpiryAndCvv?: boolean;
}

export interface RazorpayCreateCustomPaymentPayload {
    amount: number;
    user: User;
    paymentGateway: string;
    paymentInfo: RazorpayPaymentMethodInfo;
    buySuprCredits?: boolean;
    couponInfo?: RechargeCouponInfo;
}

export interface SelectedPaymentInfoValidation {
    valid: boolean;
    error?: SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES;
}

export interface RazorpayCustomPaymentSuccessRes {
    order_id: string;
    payment_id: string;
    customer_id: string;
    payment_gateway: string;
    amount: number;
}

export interface GlobalError {
    statusCode?: number;
    title?: string;
    subTitle?: string;
    actionText?: string;
    errorType?: ErrorType;
    errorUrl?: string;
}

export interface SentryLoggerExtraInfo {
    context?: {
        [key: string]: any;
    };
    message?: string;
}

export interface FreshchatOptions {
    appId: string;
    appKey: string;
    gallerySelectionEnabled?: boolean;
    cameraCaptureEnabled?: boolean;
    teamMemberInfoVisible?: boolean;
}

export type ToastPosition = "bottom" | "top" | "middle";

export interface SuprToastOptions {
    cssClass?: string;
    position?: ToastPosition;
    autoHide?: boolean;
    duration?: number;
    delay?: number;
    actionText?: string;
    actionHandler?: Function;
    buttons?: ToastButton[];
}

export interface DeviceMeta {
    uuid?: string;
    type?: string;
    model?: string;
    osName?: string;
    osVersion?: string;
    userAgent?: string;
    platform?: string;
    appVersion?: string;
    manufacturer?: string;
}

export interface UrlData {
    path: string;
    queryParams?: {
        redirect?: string;
    };
}

export interface SubscriptionServiceData {
    list?: Subscription[];
    details?: Subscription;
    dictionary?: {
        [subscriptionId: number]: Subscription;
    };
}

export interface AppsFlyerOptions {
    devKey: string;
    appId?: string;
    isDebug?: boolean;
    onInstallConversionDataListener?: boolean;
}

export interface AppTrackingData {
    appName: string;
    androidAppId?: string;
    iOSAppId?: string;
    installed?: boolean;
}

export interface AppTrackingMeta {
    version: number;
    apps: Array<AppTrackingData>;
}

export type CartItemsFilterKey = "sku_id" | "subscription_id";

export interface SearchInstrumentationDict {
    [skuId: number]: SearchInstrumentation;
}
