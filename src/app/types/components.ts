import {
    Sku,
    Image,
    CartItem,
    Schedule,
    Subscription,
    OrderHistory,
    PaymentMethodData,
    PaymentMethodDataV2,
    OutOfStockInfoStateType,
    DeliveryProofFeedbackTag,
    DeliveryProofFeedbackAnswer,
} from "@models";

import {
    RazorpayPaymentMethodInfo,
    RazorpayPaymentMethodFormInfo,
} from "./misc";

export namespace Calendar {
    export interface DayData {
        date?: number;
        month?: number;
        year?: number;
        day?: number;
        time?: number;
        dateText?: string;
        dayName?: string;
        monthYear?: string;
        fullDateText?: string;
        jsDate?: Date;
    }

    export interface WeekData extends Array<Calendar.DayData> {}

    export interface MonthData extends Array<Calendar.WeekData> {}

    export interface YearData {
        [id: number]: WeekData[];
    }
}

export interface ProfileFormError {
    name?: string;
    email?: string;
}

export interface RadioButton {
    index?: number;
    text: string;
    icon?: string;
    disabled?: boolean;
}

export interface AppData {
    isLoggedin?: boolean;
    customerCity?: string;
}

export interface ScheduleDict {
    [date: string]: Schedule;
}

export class SubscriptionDict {
    [id: number]: Subscription;
}

export interface CartMiniItem {
    item: CartItem;
    sku: Sku;
}

export interface CartMini {
    itemCount: number;
    cartTotal: number;
    items?: CartMiniItem[];
}

export interface Frequency {
    [day: string]: boolean;
}

export interface OrderDict {
    [day: string]: OrderHistory;
}

export interface CouponCode {
    couponCode?: string;
    isApplied?: boolean;
    couponApplying?: boolean;
    message?: string;
}

export interface ScheduleCalendarDictionary {
    [date: string]: boolean;
}

export interface FetchOrdersActionPayload {
    days?: number;
    toDate: string;
    firstLoad?: boolean;
}

export interface SubscriptionStepsState {
    [step: number]: boolean;
}

export interface UpdateCalendarActionPayload {
    reset?: boolean;
    history?: boolean;
    startDate?: string;
    numberOfDays?: number;
}

export interface FetchScheduleActionPayload {
    startDate?: string;
    thankYouFlag?: boolean;
    subscriptionId?: number;
    numberOfDays?: number;
}

export interface TileGridData {
    id: number;
    imgUrl: string;
    image: Image;
    name: string;
    bgColor: string;
}

export interface UpdateSubInstructionActionPayload {
    date: string;
    quantity: number;
    subscriptionId: number;
}

export interface NudgeMessage {
    img?: string;
    title?: string;
    subTitle?: string;
    btnText?: string;
}

export interface OrderSuccessWhatsappImpressionData {
    date: Date;
    totalCount: number;
    periodicCount: number;
}

export interface CartBannerConfig {
    cta?: string;
    type?: string;
    icon?: string;
    imageUrl?: string;
    title?: string;
    titles?: string[];
    ctaIcon?: string;
    subtitle?: string;
}

export enum CartBannerTypes {
    CAUTION = "caution",
    SUCCESS = "success",
}

export interface PaymentMethodV2Component extends PaymentMethodDataV2 {
    group?: string;
    componentMeta?: PaymentMethodComponentMeta;
}

export interface PaymentMethodComponent extends PaymentMethodData {
    group: string;
}

export interface SupportCalendar {
    orderDateNumber: number;
    orderDateLimit: number;
    title: string;
    enabled: boolean;
    call: string;
    chat: string;
    isAccordion?: boolean;
    last?: boolean;
    chat_access?: boolean;
    call_access?: boolean;
    number?: string;
}
export interface FetchSimilarProductsActionData {
    skuId: number;
    intent?: string;
}

export interface AutoDebitPaymentModeModalConfig {
    title: string;
    subtitle: string;
}

export interface OOSInfoComponentUpdateStateData {
    updateInLocal: string;
    state: OutOfStockInfoStateType;
}

export interface DropdownOptions {
    label: string;
    value: string;
    selected: boolean;
    disabled: boolean;
    id?: number;
}

export interface ConfigurableIcon {
    iconType?: string;
    iconName: string;
    iconSize?: string;
    iconColor?: string;
}

export enum PaymentModeComponentTypes {
    UPI = "upi",
    CARD = "card",
    TILE = "tile",
    LIST = "list",
}

export interface PaymentMethodComponentMeta {
    type: string;
    header?: string;
    ctaText?: string;
}

export interface WalletNewCardFormMeta {
    network?: string;
    cvvLength?: number;
    validation?: {
        [field: string]: { valid?: boolean; message?: string };
    };
}

export interface SelectPaymentMethodV2 {
    method?: string;
    /* string for custom options like add new card or vpa */
    id: number | string;
    paymentInfo: RazorpayPaymentMethodInfo | RazorpayPaymentMethodFormInfo;
}

export interface SkuSearchParams {
    query?: string;
    page?: number;
    category_id?: number;
}

export namespace SuprTimelineProps {
    export interface Header {
        text?: string;
        highlightText?: string;
    }

    export interface LineItem {
        icon: string;
        checked: boolean;
        title: {
            text?: string;
            highlightText?: string;
        };
        subtitle: {
            text?: string;
            highlightText?: string;
        };
        image: {
            url: string;
            text?: string;
            highlightText?: string;
        };
    }
}

export interface OrderRefundModalAdjustmentInfo {
    info?: string;
    title: string;
    date?: string;
    subtitle?: string;
}

export interface PODFeedbackTagComponentInfo extends DeliveryProofFeedbackTag {
    selected?: boolean;
}

export interface PODFeedbackClickHandlerInfo {
    tagIndex?: number;
    imageIndex?: number;
    tag?: PODFeedbackTagComponentInfo;
    answer?: DeliveryProofFeedbackAnswer;
}

export interface DeliveryProofPageQueryParams {
    delivery_date: string;
    delivery_slot: string;
}

export interface ReportPODImageQueryParams {
    image_type: string;
    delivery_date: string;
    delivery_slot: string;
}

export interface ReportPODIssue {
    key: string;
    icon?: string;
    value: string;
    selected?: boolean;
}

export interface ReportPODTextConfig {
    header: string;
    issues: Array<ReportPODIssue>;
}
