import { Banner, TextFragment } from "@shared/models";

export namespace RatingSetting {
    export interface Text {
        title: string;
        subTitle?: string;
        subTitleIos?: string;
        primaryText: string;
        secondaryText?: string;
        neverAskText?: string;
        placeHolderText?: string;
    }

    export interface NudgeText {
        prompt?: Text;
        thankyou?: Text;
        feedback?: Text;
        toastText?: string;
    }

    export interface NudgeTrigger {
        delayTime?: number;
        enabled?: boolean;
    }

    export interface NudgeTriggers {
        [key: string]: NudgeTrigger;
    }

    export interface Nudge {
        enabled?: boolean;
        text?: NudgeText;
        triggers?: NudgeTriggers;
        nudgeCoolOffTime: number;
    }
}

export namespace AutoDebitPaymentFailureSetting {
    export interface PrimaryButton {
        text: string;
        action: boolean;
    }

    export interface SecondaryButton {
        text: string;
    }

    export interface Nudge {
        type: string;
        title: string;
        subTitle: string;
        noteText: string;
        primaryButton: PrimaryButton;
        secondaryButton: SecondaryButton;
    }
}

export namespace GenericNudgeSetting {
    export interface NudgeLink {
        text: string;
        textColor?: string;
        fontSize?: string;
        outsideUrl?: string;
        appUrl?: string;
    }

    export interface NudgeActionButton {
        text: string;
        textColor?: string;
        bgColor?: string;
        borderColor?: string;
        action: NudgeAction;
        route?: string;
    }

    export type NudgeAction = "close" | "route";

    export interface Nudge {
        type?: string;
        title: string;
        imgCarousal?: string[];
        upperTextFragment?: TextFragment;
        banner?: Banner;
        lowerTextFragment?: TextFragment;
        linkCta?: NudgeLink;
        actionButtons?: NudgeActionButton[];
    }
}

export namespace AddressRetroSetting {
    export interface PrimaryButton {
        text: string;
        action: boolean;
    }

    export interface Nudge {
        type: string;
        img: string;
        title: string;
        subTitle: string;
        showSkip: boolean;
        primaryButton: PrimaryButton;
        nudgeCoolOffTime: number;
        nudgeDisplayPages: string[];
    }
}

export namespace CampaignPopupSetting {
    export interface Nudge {
        type: string;
        img: string;
        enabled: boolean;
        nudgeDisplayPages: string[];
    }
}

export namespace OrderFeedbackSetting {
    interface Header {
        optionsHeader: string;
        defaultOptionsPosHeader: string;
    }

    export interface Nudge {
        enabled: boolean;
        type?: string;
        nudgeCoolOffTime?: number;
        nudgeDisplayPages?: string[];
        title: string;
        subtitle: string;
        addComment: string;
        commentLable: string;
        commentPlaceHolder: string;
        submit: string;
        proceed: string;
        thumbsUp: string;
        thumbsDown: string;
        negativeTitle: string;
        contactSupport: string;
        negativeSubtitle: string;
        up: Header;
        down: Header;
        feedbackCalenderTitle: string;
        feedbackCalenderSubtitle: string;
        feedbackCalenderThanks: string;
        channel: string;
        feedbackSuccess: string;
        successTimer: number;
    }
}

export namespace ExitCartSetting {
    export interface PrimaryButton {
        text: string;
        action: boolean;
    }

    export interface SecondaryButton {
        text: string;
    }

    export interface Nudge {
        type: string;
        title: string;
        subTitle: string;
        primaryButton: PrimaryButton;
        secondaryButton: SecondaryButton;
        vibrationEnabled: boolean;
        vibrationTimeInMs: 200;
    }
}

export interface FreshbotSetting {
    ENABLED: boolean;
    ID?: string;
    ASYNC?: boolean;
    DATA_SELF_INIT?: string;
    DATA_INIT_TYPE?: string;
    SRC: string;
    DATA_BOT_VERSION: string;
    DATA_ENV?: string;
    DATA_REGION?: string;
}

export namespace BannerSetting {
    export interface TPOne {
        ICON: string;
        TITLE: string;
        SUBTITLE: string;
        AVAILABLE_AFTER_FIRST_DELIVERY: string;
    }
}

export interface OrderSuccessADModalConfig {
    title: string;
    enabled: boolean;
    subtitle: string;
    impressionLimit: number;
    lineItems: Array<string>;
}

export interface OrderSuccessWhatsappConfig {
    enabled: boolean;
    impressionDayOffset: number;
    impressionTotalLimit: number;
    impressionPeriodicLimit: number;
}

export interface ComplaintSetting {
    COMPLAINT_ACTION: string;
    COMPLAINT_ACTION_LABLE: string;
    COMPLAINT_SUPPORT_TITLE: string;
    COMPLAINT_SUPPORT_SUBTITLE: string;
    HIDE_COMPLAINT_ACTIONS: boolean;
    ARCHIVE_COMPLAINT_COUNT: number;
    CALL: string;
    CHAT: string;
    HELP: string;
    ENABLED: boolean;
    NEW: string;
    ACTIVE_COMPLAINT_NUMBER: number;
    ACTIVE_COMPLAINT_DAYS: number;
    ACTIVE_COMPLAINT_STATUS: string[];
    COMPLAINT_SUPPORT: {
        DAYS: number;
        PRE_DATE: {
            IS_CALL: boolean;
            IS_CHAT: boolean;
        };
        POST_DATE: {
            IS_CALL: boolean;
            IS_CHAT: boolean;
        };
    };
}

export interface SentryFilter {
    pattern: string;
    regexMatch?: boolean;
    allowedPercentage: number;
}

export interface SelfServe {
    enabled: boolean;
    experiments: string[];
    pseudoDropdown: string;
    defaultDropdown: string;
    deliveryPopup: {
        title: string;
        desc: string;
        buttonText: string;
    };
    orderCalendarDays: number;
    orderCalendarToSelfServeDays: number;
    summaryPage: {
        summaryHeader: string;
        editButton: string;
        proceedButton: string;
        successToast: string;
    };
    agentComplaintMessage: string;
    agentFallbackMessage: string;
    selfServePage: {
        proceedButton: string;
        complaintTypesOptionsTitle: string;
        feedbackTitle: string;
        otherQueries: string;
        otherQueriesTitle: string;
        proceedButtonTitle: string;
    };
    issueRaised: string;
    bannerConfig: {
        icon: string;
        subtitle: string;
        title: string;
        type: string;
    };
    imageUploadPage: {
        proceedButton: string;
        header: string;
        title: string;
        subTitle: string;
        textTitle: string;
        textSubtitle: string;
        success: string;
        error: string;
    };
    resumeChat: {
        resumeChatEnabled: boolean;
        resumeChatText: string;
        resumeChatCountMessage: string;
        agentTransferResumeMinutes: number;
    };
    faq: {
        chatEnabled: boolean;
        chat: string;
        header: string;
        chatheader: string;
        title: string;
        subTitle: string;
        description: string;
        uploadTitle: string;
        submit: string;
        error: string;
        footerTitle: string;
        chatTitle: string;
        chatSubTitle: string;
    };
    supportPage: {
        otherQueriesHeader: string;
    };
    isStaticFlowEnabled: boolean;
}
