export interface SuprDictStr {
    [key: string]: string;
}

export interface SuprDictNumBool {
    [key: number]: boolean;
}

export interface SuprDictStrBool {
    [key: string]: boolean;
}
