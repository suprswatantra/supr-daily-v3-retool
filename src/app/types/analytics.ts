export namespace Segment {
    export interface App {
        name: string;
        version: string;
        build?: string;
        namespace?: string;
    }

    export interface Device {
        id: string;
        advertisingId?: string;
        manufacturer?: string;
        model?: string;
        name?: string;
        type?: string; // ios || android
        token?: string; // PN token
    }

    export interface Location {
        city?: string;
        country?: string;
        latitude?: number;
        longitude?: number;
    }

    export interface Network {
        carrier?: string;
        cellular?: boolean;
        wifi?: boolean;
    }

    export interface Os {
        name?: string;
        version: string;
    }

    export interface Context {
        active?: boolean;
        app?: App;
        device?: Device;
        ip?: string;
        location?: Location;
        network?: Network;
        os?: Os;
    }

    export interface Integrations {
        [key: string]: boolean;
    }

    export interface User {
        id: string;
        email: string;
        firstName: string;
        lastName?: string;
        phone: string;
        city: string;
        cityId: number;
    }

    export interface Options {
        context?: Context;
        integrations?: Integrations;
    }

    export interface Traits {
        currentScreen?: string;
        referralScreen?: string;
        modalName?: string;
        objectName?: string;
        objectValue?: string;
        objectTag?: string;
        context?: string;
        position?: number;
        context_1_name?: string;
        context_1_value?: string;
        context_2_name?: string;
        context_2_value?: string;
        context_3_name?: string;
        context_3_value?: string;
        isSuprCreditsEnabled?: boolean;
        activeExperimentNames?: string;
        networkType?: string;
        isSampledEvent?: boolean;
        suprCreditsBalance?: number;
        suprWalletBalance?: number;
        ionicPatchVersion?: number;
        ionicBuild?: boolean;
        sessionId?: string;
        suprAccessMember?: boolean;
        suprAccessValidity?: number;
        isAnonymousUser?: boolean;
        sourceLink?: SourceLink;
        sourceType?: string;
        sourceUrl?: string;
        utm_source?: string;
        utm_medium?: string;
        utm_campaign?: string;
        utm_term?: string;
        utm_content?: string;
    }

    export interface ContextListItem {
        name?: string;
        value?: any;
    }

    export interface SourceLink {
        type?: string;
        url?: string;
    }
}

export namespace RudderStack {
    export interface App {
        name: string;
        version: string;
        build?: string;
        namespace?: string;
    }

    export interface Device {
        id: string;
        advertisingId?: string;
        manufacturer?: string;
        model?: string;
        name?: string;
        type?: string; // ios || android
        token?: string; // PN token
    }

    export interface Location {
        city?: string;
        country?: string;
        latitude?: number;
        longitude?: number;
    }

    export interface Network {
        carrier?: string;
        cellular?: boolean;
        wifi?: boolean;
    }

    export interface Os {
        name?: string;
        version: string;
    }

    export interface Context {
        active?: boolean;
        app?: App;
        device?: Device;
        ip?: string;
        location?: Location;
        network?: Network;
        os?: Os;
    }

    export interface Integrations {
        [key: string]: boolean;
    }

    export interface User {
        id: string;
        email: string;
        firstName: string;
        lastName?: string;
        phone: string;
        city: string;
        cityId: number;
    }

    export interface Options {
        context?: Context;
        integrations?: Integrations;
    }

    export interface Traits {
        currentScreen?: string;
        referralScreen?: string;
        modalName?: string;
        objectName?: string;
        objectValue?: string;
        context?: string;
        position?: number;
        context_1_name?: string;
        context_1_value?: string;
        context_2_name?: string;
        context_2_value?: string;
        isSuprCreditsEnabled?: boolean;
        activeExperimentNames?: string;
        networkType?: string;
        isSampledEvent?: boolean;
        suprCreditsBalance?: number;
        suprWalletBalance?: number;
        ionicPatchVersion?: number;
        ionicBuild?: boolean;
        sessionId?: string;
        suprAccessMember?: boolean;
        suprAccessValidity?: number;
        isAnonymousUser?: boolean;
        sourceLink?: SourceLink;
        sourceType?: string;
        sourceUrl?: string;
        utm_source?: string;
        utm_medium?: string;
        utm_campaign?: string;
        utm_term?: string;
        utm_content?: string;
    }

    export interface ContextListItem {
        name?: string;
        value?: any;
    }

    export interface SourceLink {
        type?: string;
        url?: string;
    }
}

export namespace Ecommerce {
    export enum CartType {
        DELIVER_ONCE = "deliver-once",
        SUBSCRIPTION = "subscription",
        BOTH = "both",
    }

    export interface ProductTraits {
        product_id?: number;
        sku?: string | number;
        name?: string;
        price?: number;
        position?: number;
        category?: string;
        quantity?: number;
        coupon?: string;
        url?: string;
        image_url?: string;
    }

    export interface OrderPlaced {
        cart_type: CartType;
        cart_id?: string;
        order_id?: string;
        currency?: string;
        value?: number;
        total?: number;
        discount?: number;
        discount_type?: string;
        benefit_source?: string;
        product_ids?: number[];
        product_count?: number;
        coupon?: string;
        isSubscriptionRechargePresent?: boolean;
    }
}

export interface ApiInfo {
    url?: string;
    method?: string;
    httpCode?: number;
    statusCode?: number;
    context?: string;
    responseTime?: number;
    silent?: boolean;
}

export enum ErrorType {
    CLIENT = "client",
    API = "api",
    NO_INTERNET = "no_internet",
    TIMEOUT = "timout",
    API_GENERIC = "api_generic",
}

export interface ErrorInfo extends ApiInfo {
    type: ErrorType;
    name?: string;
    message?: string;
}

export interface IdfaInfo {
    idfa?: string;
    aaid?: string;
    limitAdTracking: boolean;
}

export interface AfTrait {
    af_content_id?: any;
    af_content_type?: any;
    af_quantity?: any;
    af_price?: any;
    af_currency?: string;
    af_revenue?: any;
    af_coupon_code?: any;
    af_product_identifier?: number;
}
