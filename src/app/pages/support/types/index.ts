export interface SupportList {
    issues: SupportListItem;
}

export interface SupportListItem {
    nodeType: string;
    name: string;
    data: {
        icon: string;
        nav: string;
    };
}

export interface SupportCalendarConfig {
    orderDateNumber: number;
    orderDateLimit: number;
    title: string;
    enabled: boolean;
    call: string;
    chat_access: boolean;
    call_access: boolean;
    calendarNavigationText: string;
    calendarNavigationLinkText: string;
}

export interface IvrPopup {
    enabled: boolean;
    header: string;
    title: string;
    subTitle: string;
    listHeader: string;
    list: string[];
    nonOrderButton: string;
    previousOrderButton: string;
    todayOrderButton: string;
}

export interface SupportConfig {
    number: string;
    isCall: boolean;
    isChat: boolean;
    call: string;
    chat: string;
    help: string;
    archive: ArchiveIssues;
    calendar: SupportCalendarConfig;
    ivr_popup: IvrPopup;
}

export interface TodayOrder {
    isTodayOrders: boolean;
    todayDate: string;
    slot: string;
}

export interface ArchiveIssues {
    id: number;
    nodeType: string;
    name: string;
    description: string;
    data: {
        icon: string;
        title: string;
    };
}
