import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { User } from "@models";
import { SupportPageAdapter as Adapter } from "@pages/support/services/support.adapter";
import { FetchOrdersActionPayload } from "@types";

@Component({
    selector: "supr-support-container",
    template: `
        <supr-support-layout
            [user]="user$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            (fetchOrders)="fetchOrders($event)"
        ></supr-support-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SupportPageContainer implements OnInit {
    user$: Observable<User>;
    isAnonymousUser$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.user$ = this.adapter.userState$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
    }

    fetchOrders(payload: FetchOrdersActionPayload) {
        this.adapter.getOrders(payload);
    }
}
