import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { SupportPageAdapter as Adapter } from "@pages/support/services/support.adapter";
import { Observable } from "rxjs";

import { User } from "@models";

@Component({
    selector: "supr-self-serve-summary-container",
    template: `<supr-self-serve-summary-layout
        [user]="user$ | async"
    ></supr-self-serve-summary-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelfServeSummaryContainer implements OnInit {
    user$: Observable<User>;
    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.user$ = this.adapter.userState$;
    }
}
