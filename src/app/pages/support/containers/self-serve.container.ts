import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { ActivatedRoute } from "@angular/router";
import { UtilService } from "@services/util/util.service";

import { SupportPageAdapter as Adapter } from "@pages/support/services/support.adapter";
import { OrderDeliverySlotMap } from "@models";

@Component({
    selector: "supr-self-serve-container",
    template: `<supr-self-serve-layout
        [orderHistory]="orderHistory$ | async"
        [date]="date"
    ></supr-self-serve-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelfServeContainer implements OnInit {
    orderHistory$: Observable<OrderDeliverySlotMap>;
    date: string;

    constructor(
        private adapter: Adapter,
        private activatedRoute: ActivatedRoute,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        const date = this.utilService.getNestedValue(
            this.activatedRoute,
            "snapshot.params.id"
        );
        this.date = date;
        if (date) {
            this.getOrderDetails(date);
        }
    }

    getOrderDetails(date: string) {
        this.orderHistory$ = this.adapter.getOrderDeliveryByDate(date);
    }
}
