import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-self-serve-images-container",
    template: `<supr-self-serve-images-layout></supr-self-serve-images-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelfServeImageContainer {}
