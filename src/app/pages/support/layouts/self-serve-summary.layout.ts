import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
    OnDestroy,
    ChangeDetectorRef,
} from "@angular/core";
import { Location } from "@angular/common";
import { take } from "rxjs/operators";

import { RouterService } from "@services/util/router.service";
import { SelfServeStore } from "@store/self-serve";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SelfServe } from "@types";
import { Subscription } from "rxjs";

import { MODAL_NAMES, MODAL_TYPES, SELF_SERVE } from "@constants";
import { User } from "@models";

@Component({
    selector: "supr-self-serve-summary-layout",
    template: ` <div class="suprContainer">
        <supr-page-header>
            <supr-text type="subheading">{{
                selfServeConfig?.summaryPage?.summaryHeader
            }}</supr-text>
        </supr-page-header>
        <div class="suprScrollContent">
            <div class="selfServe">
                <supr-order-issues [summary]="summary"></supr-order-issues>
                <supr-service-issues [disabled]="summary"></supr-service-issues>
                <supr-notes [disabled]="summary"></supr-notes>
                <supr-add-image
                    [summary]="summary"
                    *ngIf="selfServeStore.uploadedImages?.length > 0"
                ></supr-add-image>
            </div>

            <supr-popup
                [showModal]="isModal"
                [data]="selfServeConfig?.deliveryPopup"
                modalName="${MODAL_NAMES.SELF_SERVE_DELIVERY}"
                modalType="${MODAL_TYPES.BOTTOM_SHEET}"
                (closeModal)="handleCloseModal()"
                (buttonClick)="handleCloseModal()"
            ></supr-popup>
        </div>
        <supr-page-footer>
            <div class="suprRow spaceBetween summaryButtons">
                <div class="actionbutton">
                    <supr-button
                        (click)="selfServePage()"
                        class="inactive"
                        [disabled]="isComplaintLoad"
                        saImpression
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                            .SELF_SERVE_EDIT}"
                        >{{
                            selfServeConfig?.summaryPage?.editButton
                        }}</supr-button
                    >
                </div>
                <div class="actionbutton">
                    <supr-button
                        (click)="createComplaint()"
                        [disabled]="isComplaintLoad"
                        >{{
                            selfServeConfig?.summaryPage?.proceedButton
                        }}</supr-button
                    >
                </div>
            </div>
        </supr-page-footer>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../styles/self-serve.page.scss"],
})
export class SelfServeSummaryLayout implements OnInit, OnDestroy {
    @Input() user: User;

    summary: boolean = true;
    isModal: boolean = false;
    selectedOrders = [];
    isComplaintLoad: boolean = false;
    selfServeConfig: SelfServe;
    subscribe$: Subscription;

    constructor(
        private routerService: RouterService,
        private location: Location,
        public selfServeStore: SelfServeStore,
        private analyticsService: AnalyticsService,
        private cd: ChangeDetectorRef,
        private settingsService: SettingsService,
        private utilService: UtilService,
        private toastService: ToastService
    ) {}

    ngOnInit() {
        this.selectedOrders = this.selfServeStore.selectedOrdersWithIssue;
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    createComplaint() {
        this.analyticsService.trackClick({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_SUBMIT,
            objectValue: JSON.stringify({
                interaction_id: this.selfServeStore.interaction_id,
                orders: this.selectedOrders,
                service: this.selfServeStore.selectedServiceIssues,
            }),
            context: this.selfServeStore.date,
        });
        this.isComplaintLoad = true;
        this.subscribe$ = this.selfServeStore
            .complaintCreation()
            .pipe(take(1))
            .subscribe(
                (data: any) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_COMPLAINT_SUCCESS,
                        objectValue: JSON.stringify(data),
                    });
                    this.isComplaintLoad = false;

                    this.handleResponse(data);
                },
                (error) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_COMPLAINT_ERROR,
                        objectValue: JSON.stringify(error),
                    });
                    this.isComplaintLoad = false;

                    this.agentTransfer(error);
                }
            );
    }

    handleResponse(data) {
        if (!this.utilService.isEmpty(data)) {
            if (data.type === 0) {
                this.analyticsService.trackImpression({
                    objectName:
                        SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_NAVIGATE_SUPPORT,
                    objectValue: JSON.stringify(data),
                });
                this.toastService.present(
                    this.selfServeConfig &&
                        this.selfServeConfig.summaryPage &&
                        this.selfServeConfig.summaryPage.successToast
                );
                this.routerService.gotoSupportPageRoot({
                    state: {
                        issueRaised: true,
                        complaints: (data && data.complaints) || [],
                    },
                    replaceUrl: true,
                });
            } else {
                this.handleResponseTypes(data);
            }
        } else {
            this.analyticsService.trackImpression({
                objectName:
                    SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_COMPLAINT_ERROR,
                objectValue: JSON.stringify(data),
            });
            this.agentTransfer(data);
        }
        this.cd.markForCheck();
    }

    agentTransfer(error: any) {
        const agentMessage = `Complaint details:
        CID: ${this.user.id},
        First name: ${this.user.firstName}, Last name: ${this.user.lastName},
        ${this.selfServeStore.agentMessage}
        ${this.selfServeConfig.agentComplaintMessage}`;

        this.analyticsService.trackImpression({
            objectName:
                SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_COMPLAINT_AGENT_TRANSFER,
            objectValue: agentMessage,
            context: JSON.stringify(error),
        });

        this.routerService.gotoSupportPageRoot({
            state: {
                agentTransfer: agentMessage,
            },
            replaceUrl: true,
        });
    }

    handleCloseModal() {
        this.isModal = false;
        this.redirectSupportPage();
    }

    redirectSupportPage() {
        this.routerService.gotoSupportPageRoot({ replaceUrl: true });
    }

    selfServePage() {
        this.location.back();
    }

    ngOnDestroy() {
        if (this.subscribe$) {
            this.subscribe$.unsubscribe();
        }
    }

    private handleResponseTypes(data) {
        switch (data && data.type) {
            case 1:
                // delivery popup
                this.analyticsService.trackImpression({
                    objectName:
                        SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_DELIVERY_POPUP,
                    objectValue: JSON.stringify(data),
                });
                this.isModal = true;
                break;
            case 2:
                // issue tranfer to agent
                this.agentTransfer(data);
                break;
            default:
                this.analyticsService.trackImpression({
                    objectName:
                        SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_COMPLAINT_ERROR,
                    objectValue: JSON.stringify(data),
                });
                this.agentTransfer(data);
                break;
        }
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
