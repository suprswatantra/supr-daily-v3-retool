import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnDestroy,
} from "@angular/core";
import { Router } from "@angular/router";
import { take } from "rxjs/operators";

import { OrderDeliverySlotMap } from "@models";

import { RouterService } from "@services/util/router.service";
import { SelfServeStore } from "@store/self-serve";
import { DateService } from "@services/date/date.service";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SelfServe } from "@types";
import { Subscription } from "rxjs";
import { SELF_SERVE } from "@constants";

@Component({
    selector: "supr-self-serve-layout",
    template: ` <div class="suprContainer">
        <supr-page-header>
            <supr-text type="subheading">{{ formattedDate }}</supr-text>
        </supr-page-header>
        <div class="suprScrollContent" *mobxReaction="errorHandler.bind(this)">
            <div class="selfServe">
                <supr-order-issues
                    [orderHistory]="orderHistory"
                ></supr-order-issues>
                <supr-service-issues></supr-service-issues>
                <supr-notes></supr-notes>
            </div>
        </div>
        <supr-page-footer>
            <supr-page-footer-v2
                [disabled]="!selfServeStore.isComplaint"
                buttonIcon="chevron_right"
                [buttonTitle]="
                    selfServeConfig?.selfServePage?.proceedButtonTitle
                "
                [buttonSubtitle]="selfServeConfig?.selfServePage?.proceedButton"
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_PROCEED}"
                [saObjectValue]="date"
                (handleButtonClick)="proceedHandler()"
            >
                <supr-text type="action14">{{
                    selfServeConfig?.selfServePage?.otherQueriesTitle
                }}</supr-text>
                <ion-row
                    class="link"
                    saClick
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_OTHER_QUERIES}"
                    (click)="handleOtherQueriesClick()"
                >
                    <supr-text type="caption">{{
                        selfServeConfig?.selfServePage?.otherQueries
                    }}</supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </ion-row>
            </supr-page-footer-v2>
        </supr-page-footer>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../styles/self-serve.page.scss"],
})
export class SelfServeLayout implements OnInit, OnDestroy {
    @Input() date: string;
    @Input() orderHistory: OrderDeliverySlotMap;

    formattedDate: string;
    selfServeConfig: SelfServe;
    subscribe$: Subscription;

    constructor(
        private routerService: RouterService,
        private dateService: DateService,
        public selfServeStore: SelfServeStore,
        private router: Router,
        private settingsService: SettingsService,
        private analyticsService: AnalyticsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();

        this.formattedDate = this.dateService.formatDate(
            this.date,
            "dd LLLL yyyy"
        );
        const state = this.router.getCurrentNavigation().extras.state;

        const slot = (state && state.slot) || "";

        this.selfServeStore.setDateSlot(this.date, slot);
        this.selfServeStore.selfServeGet();
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }

    errorHandler() {
        const { isAgentTransfer, errorResponse } = this.selfServeStore;
        if (isAgentTransfer) {
            this.analyticsService.trackImpression({
                objectName:
                    SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_ERROR_INTERUPT,
                objectValue: JSON.stringify(errorResponse),
            });
            this.routerService.goToSupportPage({
                state: {
                    agentTransfer:
                        this.selfServeConfig &&
                        this.selfServeConfig.agentFallbackMessage,
                },
                replaceUrl: true,
            });
        }
        return isAgentTransfer;
    }

    navigate() {
        this.subscribe$ = this.selfServeStore
            .isImageUpload()
            .pipe(take(1))
            .subscribe(
                (data: any) => {
                    if (data && data.data && data.data.image_upload_req) {
                        this.selfServeStore.setImageUploadIds(
                            data.data.order_ids
                        );
                        this.routerService.goToImageUploadPage();
                    } else {
                        this.routerService.goToSummaryPage();
                    }
                },
                () => {
                    this.routerService.goToSummaryPage();
                }
            );
    }

    proceedHandler() {
        this.analyticsService.trackClick({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_PROCEED,
            objectValue: JSON.stringify({
                interaction_id: this.selfServeStore.interaction_id,
                date: this.selfServeStore.date,
                orders: this.selfServeStore.selectedOrdersForInstumentation,
                service: this.selfServeStore.selectedServiceIssues,
            }),
        });
        if (
            !this.utilService.isEmpty(
                this.selfServeStore.selectedOrdersWithIssue
            )
        ) {
            const isError = this.selfServeStore.validateOrders();
            if (isError) {
                this.scrollToError();
            } else {
                this.navigate();
            }
        } else {
            this.navigate();
        }
    }

    handleOtherQueriesClick() {
        this.routerService.gotoSupportPageRoot({
            state: {
                scrollId: "nonOrderFlows",
            },
            replaceUrl: true,
        });
    }

    private scrollToError() {
        this.utilService.scrollToElementId("errormsg");
    }

    ngOnDestroy() {
        if (this.subscribe$) {
            this.subscribe$.unsubscribe();
        }
    }
}
