import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    OnDestroy,
    Output,
    ChangeDetectorRef,
    OnInit,
} from "@angular/core";
import { Platform } from "@ionic/angular";

import { Subscription } from "rxjs";
import { take } from "rxjs/operators";

import { filter } from "rxjs/operators";

import { Router, NavigationEnd } from "@angular/router";

import { COMPLAINTS, ANALYTICS_OBJECT_NAMES, SELF_SERVE } from "@constants";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

import { AnalyticsService } from "@services/integration/analytics.service";
import { User, SupportOptions, ComplaintCreatedList } from "@models";
import { TodayOrder } from "@pages/support/types";

import { SettingsService } from "@services/shared/settings.service";
import { DateService } from "@services/date/date.service";
import { SupportService } from "@services/layout/support.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { FreshChatService } from "@services/integration/freshchat.service";
import { FreshBotService } from "@services/integration/freshbot.service";
import { UtilService } from "@services/util/util.service";
import { SelfServeStore } from "@store/self-serve";
import { ScheduleAdapter as Adapter } from "@shared/adapters/schedule.adapter";

import { Complaintv2Store } from "@store/complaintv2";

import { FetchOrdersActionPayload, SelfServe } from "@types";

@Component({
    selector: "supr-support-layout",
    template: `
        <div class="suprContainer" *mobxAutorun>
            <supr-page-header>
                <supr-text type="subheading">Support & FAQs</supr-text>
            </supr-page-header>
            <div class="suprScrollContent" id="supportContentWrapper">
                <supr-list-item
                    class="successTicker"
                    id="successTicker"
                    islast="true"
                    selected="true"
                    [disabled]="true"
                    *ngIf="issueRaised && !tickerAb"
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_COMPLAINT_TICKER}"
                >
                    <div class="listItem">
                        <supr-text type="subheading">
                            {{ selfServeConfig?.issueRaised }}</supr-text
                        >
                    </div>
                </supr-list-item>

                <div
                    class="successTicker"
                    id="successTicker"
                    *ngIf="issueRaised && tickerAb"
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_COMPLAINT_TICKER}"
                >
                    <supr-cart-banner
                        [config]="selfServeConfig?.bannerConfig"
                    ></supr-cart-banner>
                </div>

                <supr-resume-chat
                    [user]="user"
                    [isChatResume]="isChatResume | async"
                    [title]="selfServeConfig?.resumeChat?.resumeChatText"
                    [subtitle]="
                        selfServeConfig?.resumeChat?.resumeChatCountMessage
                    "
                    [count]="count | async"
                ></supr-resume-chat>

                <supr-active-complaints
                    [user]="user"
                    [newComplaints]="complaints"
                ></supr-active-complaints>

                <supr-support-order-calendar
                    (fetchOrders)="fetchSuportOrders($event)"
                ></supr-support-order-calendar>

                <supr-support-content
                    id="nonOrderFlows"
                    [isArchive]="complaintv2Store?.complaintsListV2?.length > 0"
                ></supr-support-content>
                <div class="divider16"></div>

                <supr-support-help
                    [user]="user"
                    *ngIf="!isAnonymousUser"
                    (callHandler)="handleCallUsClick()"
                ></supr-support-help>

                <supr-support-ivr
                    [isModal]="isModal"
                    [todayData]="todayData"
                    (closeModal)="handleModalClose()"
                    (scrollTo)="scrollToId($event)"
                ></supr-support-ivr>
            </div>
        </div>
    `,
    styleUrls: ["../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SupportLayoutComponent implements OnInit, OnDestroy {
    @Input() user: User;
    @Input() isAnonymousUser: boolean;

    @Output() fetchOrders: EventEmitter<
        FetchOrdersActionPayload
    > = new EventEmitter();

    count: Promise<number>;
    issueRaised: boolean = false;
    tickerAb: boolean = true;
    routeSubscriber: Subscription;
    selfServeConfig: SelfServe;
    isChatResume: Promise<boolean>;

    isModal = false;
    todayData: TodayOrder = {
        isTodayOrders: false,
        todayDate: "",
        slot: "",
    };
    complaints: ComplaintCreatedList[];

    private resumeSubscriber: Subscription;

    constructor(
        private settingsService: SettingsService,
        private feedbackService: FeedbackService,
        private router: Router,
        private freshChatService: FreshChatService,
        private freshBotService: FreshBotService,
        public complaintv2Store: Complaintv2Store,
        private dateService: DateService,
        private utilService: UtilService,
        private cd: ChangeDetectorRef,
        public selfServeStore: SelfServeStore,
        private analyticsService: AnalyticsService,
        private adapter: Adapter,
        private supportService: SupportService,
        public platform: Platform
    ) {
        this.subscribeToRouterEvents();
        this.subscribeToResumeEvent();
    }

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
        this.feedbackService.fetchOrderFeedbackSettings();
        this.initializeSupport();
        this.init();
    }

    handleModalClose() {
        this.isModal = false;
    }

    handleCallUsClick() {
        this.isModal = true;
        const today = this.dateService.getTodayDateText();

        this.adapter
            .getOrderByDate(today)
            .pipe(take(1))
            .subscribe((data) => {
                let orders = this.utilService.getNestedValue(data, "orders");
                if (orders && orders.length) {
                    this.todayData = this.supportService.getIvrCallData(
                        orders,
                        today
                    );
                }
            });
    }

    fetchSuportOrders(payload: FetchOrdersActionPayload) {
        this.fetchOrders.emit(payload);
    }

    ngOnDestroy() {
        this.resumeSubscriber.unsubscribe();
        this.routeSubscriber.unsubscribe();
    }

    scrollToId(id) {
        setTimeout(() => {
            this.utilService.scrollToElementId(id, false);
            this.cd.markForCheck();
        }, 100);
    }

    private subscribeToResumeEvent() {
        this.resumeSubscriber = this.platform.resume.subscribe(async () => {
            this.initializeSupport();
        });
    }

    private subscribeToRouterEvents() {
        this.routeSubscriber = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isSupportPageUrl(event.url)
                )
            )
            .subscribe(() => {
                this.handleSelfServeEvents();
            });
    }

    private getComplaintsList() {
        const complaintConfig = this.fetchComplaintConfig();

        this.complaintv2Store.fetchComplaintsListV2(
            this.dateService.textFromDate(new Date()),
            complaintConfig.ACTIVE_COMPLAINT_DAYS
        );
    }

    private init() {
        const complaintConfig = this.fetchComplaintConfig();
        if (!this.isAnonymousUser && complaintConfig.ENABLED) {
            this.getComplaintsList();
        }
        this.tickerAb =
            this.utilService.isLengthyArray(this.selfServeConfig.experiments) &&
            this.selfServeConfig.experiments.includes("ticker");
        this.fetchFaqList();
        this.getSupportOptions();
        this.initFreshbots();
        this.freshChatService.setChatRouting();
    }

    private initializeSupport() {
        if (!this.isAnonymousUser) {
            this.checkResumeChat();
            this.count = this.freshChatService.getChatCount();
        }
    }

    private checkResumeChat() {
        if (
            this.selfServeConfig &&
            this.selfServeConfig.resumeChat &&
            this.selfServeConfig.resumeChat.resumeChatEnabled &&
            this.selfServeConfig.resumeChat.agentTransferResumeMinutes
        ) {
            this.isChatResume = this.freshChatService.isResumeChat(
                this.selfServeConfig.resumeChat.agentTransferResumeMinutes
            );
            this.cd.markForCheck();
        }
    }

    private getSupportOptions() {
        this.complaintv2Store
            .fetchSupportOptions()
            .pipe(take(1))
            .subscribe(
                (supportOptions: SupportOptions) => {
                    this.complaintv2Store.setSupportOptions(supportOptions);
                },
                (err) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION.SUPPORT_OPTIONS_ERROR,
                        objectValue: `${JSON.stringify(err)}`,
                    });
                }
            );
    }

    private isSupportPageUrl(url: string): boolean {
        return url && url === "/support";
    }

    private handleSelfServeEvents() {
        const state = this.router.getCurrentNavigation().extras.state;
        this.issueRaised = (state && state.issueRaised) || false;
        this.complaints = (state && state.complaints) || [];
        const agentTransfer = (state && state.agentTransfer) || "";
        const scrollId = (state && state.scrollId) || "successTicker";

        this.scrollToId(scrollId);

        const complaintConfig = this.fetchComplaintConfig();
        if (this.issueRaised && complaintConfig.ENABLED) {
            this.getComplaintsList();
        }
        if (agentTransfer) {
            this.freshChatService.openChat(this.user);
            this.freshChatService.sendFreshchatMessage(agentTransfer);
        }

        this.initializeSupport();
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }

    private fetchComplaintConfig() {
        const complaintConfig = this.settingsService.getSettingsValue(
            "complaintConfig",
            COMPLAINTS
        );
        this.analyticsService.trackImpression({
            objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.COMPLAINT_CONFIG,
            objectValue: `${JSON.stringify(complaintConfig)}`,
        });
        const selfServeConfig = this.fetchSelfServeConfig();
        const ENABLED = selfServeConfig.enabled || false;

        return { ...complaintConfig, ENABLED };
    }

    private initFreshbots() {
        if (
            !window.Freshbots &&
            !this.isAnonymousUser &&
            !this.freshBotService.getFreshbotScriptLoaded()
        ) {
            this.freshBotService.initialize(this.user);
        }
    }

    private fetchFaqList() {
        if (
            this.utilService.isEmpty(this.selfServeStore.staticData) &&
            this.selfServeConfig.isStaticFlowEnabled
        ) {
            this.selfServeStore
                .getSelfServeStaticFlows()
                .pipe(take(1))
                .subscribe((data: any) => {
                    if (!this.utilService.isEmpty(data)) {
                        this.selfServeStore.setStaticData(
                            JSON.parse(data.value)
                        );
                    }
                });
        }
    }
}
