import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { SelfServeStore } from "@store/self-serve";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SettingsService } from "@services/shared/settings.service";
import { RouterService } from "@services/util/router.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SelfServe } from "@types";
import { SELF_SERVE } from "@constants";

@Component({
    selector: "supr-self-serve-images-layout",
    template: ` <div class="suprContainer">
        <supr-page-header>
            <supr-text type="subheading">{{
                selfServeConfig?.imageUploadPage?.header
            }}</supr-text>
        </supr-page-header>
        <div class="suprScrollContent">
            <supr-add-image (supportPage)="supportPage()"></supr-add-image>
        </div>
        <supr-page-footer>
            <supr-button
                (click)="summaryPage()"
                [disabled]="selfServeStore.uploadedImages?.length < 1"
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                    .SELF_SERVE_IMAGE_PROCEED}"
                >{{
                    selfServeConfig?.imageUploadPage?.proceedButton
                }}</supr-button
            >
        </supr-page-footer>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../styles/self-serve.page.scss"],
})
export class SelfServeImageLayout implements OnInit {
    selfServeConfig: SelfServe;

    constructor(
        public selfServeStore: SelfServeStore,
        private settingsService: SettingsService,
        private routerService: RouterService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    summaryPage() {
        this.analyticsService.trackClick({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_IMAGE_PROCEED,
            objectValue: JSON.stringify({
                interaction_id: this.selfServeStore.interaction_id,
                date: this.selfServeStore.date,
                images: this.selfServeStore.uploadedImages,
                orders: this.selfServeStore.selectedOrdersForInstumentation,
                service: this.selfServeStore.selectedServiceIssues,
            }),
        });
        this.routerService.goToSummaryPage();
    }

    supportPage() {
        this.routerService.goToSupportPage({
            state: {
                agentTransfer:
                    this.selfServeConfig &&
                    this.selfServeConfig.agentFallbackMessage,
            },
            replaceUrl: true,
        });
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
