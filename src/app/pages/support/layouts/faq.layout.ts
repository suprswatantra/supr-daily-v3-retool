import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    ChangeDetectorRef,
} from "@angular/core";
import { take } from "rxjs/operators";
import { Router } from "@angular/router";

import { ActivatedRoute } from "@angular/router";
import { TOAST_MESSAGES, SELF_SERVE } from "@constants";
import { Faq } from "@models";
import { SelfServe } from "@types";

import { ToastService } from "@services/layout/toast.service";
import { SelfServeStore } from "@store/self-serve";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Component({
    selector: "supr-support-faq-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    {{ selfServeConfig?.faq?.header }}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <ng-container *ngIf="loading; else faq_list">
                    <supr-loader></supr-loader>
                </ng-container>

                <ng-template #faq_list>
                    <supr-accordion
                        [title]="header"
                        [subTitle]="selfServeConfig?.faq?.subTitle"
                        [disabled]="true"
                        [accordionToggle]="false"
                        icon="QUESTION"
                    >
                        <supr-support-faq-item-list
                            [faqList]="selfServeStore.faqList"
                            (setFaqChatModal)="setFaqChatModal($event)"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_STATIC_FAQ_TREE}"
                            [saObjectValue]="id"
                        ></supr-support-faq-item-list>
                    </supr-accordion>
                    <supr-support-faq-chat
                        [isModal]="isModal"
                        [transferToAgent]="transferToAgent"
                        (setFaqChatModal)="setFaqChatModal($event)"
                    ></supr-support-faq-chat>
                </ng-template>
            </div>
            <supr-page-footer *ngIf="selfServeConfig?.faq?.chatEnabled">
                <supr-text type="body">{{
                    selfServeConfig?.faq?.footerTitle
                }}</supr-text>
                <div class="divider4"></div>
                <supr-support-button
                    [show]="true"
                    [displayText]="selfServeConfig?.faq?.chat"
                    (handleClick)="chatActionButton()"
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_STATIC_FAQ_CHAT}"
                ></supr-support-button>
            </supr-page-footer>
        </div>
    `,
    styleUrls: ["../styles/faq.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqLayoutComponent implements OnInit {
    faqList: Faq[] = [];
    loading = true;
    isModal = false;
    selfServeConfig: SelfServe;
    id: number;
    transferToAgent = false;
    header: string;

    constructor(
        private toastService: ToastService,
        public selfServeStore: SelfServeStore,
        private activatedRoute: ActivatedRoute,
        private cd: ChangeDetectorRef,
        private settingsService: SettingsService,
        private utilService: UtilService,
        private router: Router
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
        const id = this.utilService.getNestedValue(
            this.activatedRoute,
            "snapshot.params.id"
        );

        const state = this.router.getCurrentNavigation().extras.state;

        const transferToAgent = (state && state.transferToAgent) || false;
        const header =
            (state && state.header) || this.selfServeConfig.faq.title;
        this.isModal = transferToAgent;
        this.transferToAgent = transferToAgent;
        this.header = header;

        this.id = id;
        this.fetchFaqList(parseInt(id));
    }

    chatActionButton() {
        this.setFaqChatModal(true);
    }

    setFaqChatModal(value: boolean = false) {
        this.isModal = value;
    }

    private fetchFaqList(id: number) {
        if (
            this.utilService.isEmpty(this.selfServeStore.staticData) &&
            this.selfServeConfig.isStaticFlowEnabled
        ) {
            this.selfServeStore
                .getSelfServeStaticFlows()
                .pipe(take(1))
                .subscribe(
                    (data: any) => {
                        this.loading = false;
                        if (!this.utilService.isEmpty(data)) {
                            this.selfServeStore.setStaticData(
                                JSON.parse(data && data.value)
                            );
                            this.selfServeStore.setFaqById(id);
                        }
                        this.cd.markForCheck();
                    },
                    () => {
                        this.loading = false;
                        this.showError();
                        this.cd.markForCheck();
                    }
                );
        } else {
            this.loading = false;
            this.selfServeStore.setFaqById(id);
        }
    }

    private showError() {
        this.toastService.present(TOAST_MESSAGES.SOME_THING_WENT_WRONG);
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
