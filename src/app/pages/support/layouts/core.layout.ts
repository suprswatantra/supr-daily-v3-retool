import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-support-core-layout",
    template: `
        <ion-router-outlet
            [animated]="true"
            [swipeGesture]="false"
        ></ion-router-outlet>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SupportCoreLayoutComponent {}
