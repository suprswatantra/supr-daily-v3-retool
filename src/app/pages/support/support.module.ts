import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MobxAngularModule } from "mobx-angular";

import { IonicModule } from "@ionic/angular";
import { CallNumber } from "@ionic-native/call-number/ngx";

import { SharedModule } from "@shared/shared.module";

import { SupportRoutingModule } from "./support-routing.module";
import { SupportPageAdapter } from "./services/support.adapter";
import { SupportCoreLayoutComponent } from "./layouts/core.layout";
import { SupportLayoutComponent } from "./layouts/support.layout";
import { FaqLayoutComponent } from "./layouts/faq.layout";
import { supportComponents } from "./components";
import { SupportPageContainer } from "./containers/support.container";
import { SelfServeContainer } from "./containers/self-serve.container";
import { SelfServeSummaryContainer } from "./containers/self-serve-summary.container";
import { SelfServeImageContainer } from "./containers/self-serve-images.container";
import { SelfServeLayout } from "./layouts/self-serve.layout";
import { SelfServeSummaryLayout } from "./layouts/self-serve-summary.layout";
import { SelfServeImageLayout } from "./layouts/self-serve-image.layout";

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        SupportRoutingModule,
        SharedModule,
        MobxAngularModule,
    ],
    declarations: [
        SupportCoreLayoutComponent,
        SupportLayoutComponent,
        FaqLayoutComponent,
        SupportPageContainer,
        SelfServeContainer,
        SelfServeLayout,
        ...supportComponents,
        SelfServeSummaryContainer,
        SelfServeSummaryLayout,
        SelfServeImageContainer,
        SelfServeImageLayout,
    ],

    providers: [SupportPageAdapter, CallNumber],
})
export class SupportPageModule {}
