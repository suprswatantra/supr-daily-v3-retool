import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { SupportCoreLayoutComponent } from "./layouts/core.layout";
import { FaqLayoutComponent } from "./layouts/faq.layout";
import { SupportPageContainer } from "./containers/support.container";
import { SelfServeContainer } from "./containers/self-serve.container";
import { SelfServeImageContainer } from "./containers/self-serve-images.container";
import { SelfServeSummaryContainer } from "./containers/self-serve-summary.container";
import { SCREEN_NAMES } from "@constants";

const supportRoutes: Routes = [
    {
        path: "",
        component: SupportCoreLayoutComponent,
        children: [
            {
                path: "",
                component: SupportPageContainer,
            },
            {
                path: "faq/:id",
                component: FaqLayoutComponent,
                data: { screenName: SCREEN_NAMES.FAQ },
            },
            {
                path: "self-serve/summary",
                component: SelfServeSummaryContainer,
                data: {
                    screenName: SCREEN_NAMES.SELF_SERVE_SUMMARY,
                },
            },
            {
                path: "self-serve/image-upload",
                component: SelfServeImageContainer,
                data: {
                    screenName: SCREEN_NAMES.SELF_SERVE_IMAGES,
                },
            },
            {
                path: "self-serve/:id",
                component: SelfServeContainer,
                data: { screenName: SCREEN_NAMES.SELF_SERVE },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(supportRoutes)],
    exports: [RouterModule],
})
export class SupportRoutingModule {}
