import { FaqItemComponent } from "./faq/item/item.component";
import { FaqItemListComponent } from "./faq/item-list/item-list.component";
import { FaqChatComponent } from "./faq/faq-chat/faq-chat.component";
import { FaqHeaderComponent } from "./faq/faq-header/faq-header.component";
import { FaqTemplateComponent } from "./faq/faq-template/faq-template.component";

import { HelpComponent } from "./support/help/help.component";
import { ListComponent } from "./support/list/list.component";
import { ContentComponent } from "./support/content.component";
import { IvrCallComponent } from "./support/ivr/ivr-call.component";

import { ActiveComplaints } from "./complaints/active-complaints.component";
import { SelfServeOrderIssues } from "./self-serve/order-issues/order-issues.component";
import { SelfServeOrderDetails } from "./self-serve/order-issues/order-details/order-details.component";
import { ServiceIssues } from "./self-serve/service-issues/service-issues.component";
import { NotesComponent } from "./self-serve/notes/notes.component";
import { SupportOrderCalendar } from "./calendar/order-calendar.component";
import { AddImageComponent } from "./self-serve/add-image/add-image.component";
import { ResumeChatComponent } from "./resume-chat/resume-chat.component";

export const supportComponents = [
    ContentComponent,
    HelpComponent,
    ListComponent,
    FaqItemComponent,
    FaqItemListComponent,
    ActiveComplaints,
    SelfServeOrderIssues,
    ServiceIssues,
    NotesComponent,
    SupportOrderCalendar,
    SelfServeOrderDetails,
    AddImageComponent,
    ResumeChatComponent,
    FaqChatComponent,
    FaqHeaderComponent,
    FaqTemplateComponent,
    IvrCallComponent,
];
