import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";

import { CallNumber } from "@ionic-native/call-number/ngx";

import { FreshChatService } from "@services/integration/freshchat.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";
import { SupportService } from "@services/layout/support.service";
import { SettingsService } from "@services/shared/settings.service";

import { SupportConfig } from "@pages/support/types";
import { ComplaintSetting } from "@types";
import { Complaintv2Store } from "@store/complaintv2";
import {
    SETTINGS_KEYS_DEFAULT_VALUE,
    MODAL_NAMES,
    MODAL_TYPES,
    COMPLAINTS,
} from "@constants";
import { ComplaintViewV2, User, ComplaintCreatedList } from "@models";

@Component({
    selector: "supr-active-complaints",
    template: `
        <div *mobxReaction="handleComplaintsList.bind(this)">
            <div class="activeComplaints" *ngIf="complaintsListV2?.length">
                <div class="divider16"></div>

                <supr-text type="bold24">Recent issues</supr-text>

                <supr-complaints-list
                    [ComplaintsListV2]="complaintsListV2"
                    (handleClick)="clickAction($event)"
                    (complaintClick)="complaintAction($event)"
                    saObjectName="active"
                ></supr-complaints-list>

                <supr-modal
                    *ngIf="showSupport"
                    modalName="${MODAL_NAMES.SUPPORT_FAQ_COMPLAINTS}"
                    modalType="${MODAL_TYPES.BOTTOM_SHEET}"
                    (handleClose)="handleSupportModal()"
                >
                    <div class="supportModal">
                        <supr-support-options
                            [chat]="chat"
                            [call]="call"
                            [help]="help"
                            [subTitle]="subTitle"
                            [isChat]="isChat"
                            [isCall]="isCall"
                            saObjectName="active"
                            [saObjectValue]="complaintId"
                            [saContext]="
                                complaint?.complaint_status_timeline
                                    .complaint_status
                            "
                            (chatActionButton)="chatActionButton()"
                            (callActionButton)="callActionButton()"
                        ></supr-support-options>
                    </div>
                </supr-modal>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActiveComplaints implements OnInit {
    @Input() user: User;
    @Input() newComplaints: ComplaintCreatedList[];

    complaintsListV2: ComplaintViewV2[] = [];

    showSupport: boolean = false;
    isChat: boolean = false;
    isCall: boolean = false;
    chat: string;
    call: string;
    help: string;
    subTitle: string;

    complaintConfig: ComplaintSetting;
    supportConfig: SupportConfig;
    complaintId: number;
    complaintV2: ComplaintViewV2;

    ngOnInit() {
        this.supportConfig = this.fetchsupportConfig();
        this.complaintConfig = this.fetchComplaintConfig();

        this.chat = this.complaintConfig.CHAT;
        this.call = this.complaintConfig.CALL;
        this.help = this.complaintConfig.COMPLAINT_SUPPORT_TITLE;
        this.subTitle = this.complaintConfig.COMPLAINT_SUPPORT_SUBTITLE;
    }

    constructor(
        private callNumber: CallNumber,
        private settingsService: SettingsService,
        private freshChatService: FreshChatService,
        private routerService: RouterService,
        private dateService: DateService,
        private complaintv2Store: Complaintv2Store,
        private supportService: SupportService
    ) {}

    handleComplaintsList() {
        const { complaintsListV2 = [] } = this.complaintv2Store;

        if (complaintsListV2 && complaintsListV2.length > 0) {
            this.complaintsListV2 =
                this.getFilteredComplaints(complaintsListV2) || [];
        }
        return this.complaintsListV2;
    }

    private fetchsupportConfig() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        return supportConfig;
    }

    private fetchComplaintConfig() {
        const complaintConfig = this.settingsService.getSettingsValue(
            "complaintConfig",
            COMPLAINTS
        );
        return complaintConfig;
    }

    callActionButton() {
        this.handleSupportModal(false);
        this.openDialer(this.supportConfig.number);
    }

    private openDialer(phoneNo: string) {
        this.callNumber.callNumber(phoneNo, false).catch(() => {
            // Open the dialer - this doesn't need to seek any permission
            window.location.href = `tel:${phoneNo}`;
        });
    }

    private getFilteredComplaints(complaintsList: ComplaintViewV2[]) {
        const complaintConfig = this.fetchComplaintConfig();

        return this.supportService.getActiveComplaints(
            complaintsList,
            complaintConfig,
            this.newComplaints
        );
    }

    chatActionButton() {
        this.handleSupportModal(false);
        this.openChatService();
    }

    private openChatService() {
        this.freshChatService.openChat(this.user);
        this.freshChatService.sendMessage(this.complaintId);
    }

    clickAction(complaint: ComplaintViewV2) {
        const days = this.dateService.daysFromToday(complaint.complaintDate);
        this.complaintV2 = complaint;
        this.complaintId = complaint.complaint_id;

        if (Math.abs(days) > this.complaintConfig.COMPLAINT_SUPPORT.DAYS) {
            this.showSupport = true;
            this.isCall = this.complaintConfig.COMPLAINT_SUPPORT.POST_DATE.IS_CALL;
            this.isChat = this.complaintConfig.COMPLAINT_SUPPORT.POST_DATE.IS_CHAT;
        } else {
            this.showSupport = true;
            this.isCall = this.complaintConfig.COMPLAINT_SUPPORT.PRE_DATE.IS_CALL;
            this.isChat = this.complaintConfig.COMPLAINT_SUPPORT.PRE_DATE.IS_CHAT;
        }
    }

    complaintAction(complaint: ComplaintViewV2) {
        this.complaintv2Store.setComplaint(complaint);
        this.routerService.goToComplaintsDetailsPage(
            {},
            complaint.complaint_id
        );
    }

    handleSupportModal(value: boolean = false) {
        this.showSupport = value;
    }
}
