import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";
import { take } from "rxjs/operators";

import { MODAL_NAMES, MODAL_TYPES, SELF_SERVE } from "@constants";
import { SelfServe } from "@types";

import { SelfServeStore } from "@store/self-serve";
import { ToastService } from "@services/layout/toast.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Component({
    selector: "supr-support-faq-chat",
    template: `
        <div class="faqSupport">
            <supr-modal
                *ngIf="isModal"
                modalName="${MODAL_NAMES.SELF_SERVE_STATIC_CHAT_MODAL}"
                modalType="${MODAL_TYPES.NO_WRAPPER}"
                [fullScreen]="true"
                (handleClose)="handleModalClose()"
            >
                <supr-page-header (handleBackButtonClick)="handleModalClose()">
                    <supr-text type="subheading">{{
                        selfServeConfig?.faq?.chatheader
                    }}</supr-text>
                </supr-page-header>

                <div class="suprScrollContent">
                    <div class="faqChatForm">
                        <div class="chatContent">
                            <supr-accordion
                                [title]="selfServeConfig?.faq?.chatTitle"
                                [subTitle]="selfServeConfig?.faq?.chatSubTitle"
                                icon="bag_cart"
                            >
                                <supr-list-item
                                    [islast]="true"
                                    [isExpand]="isExpand"
                                    [selected]="true"
                                    [disabled]="disabled"
                                >
                                    <div class="listItem">
                                        <supr-text type="body">{{
                                            selfServeConfig?.faq?.description
                                        }}</supr-text>
                                    </div>

                                    <div class="listContent">
                                        <textarea
                                            rows="5"
                                            (blur)="setNotes($event)"
                                            (keydown)="setValidate()"
                                            type="text"
                                            saImpression
                                            saClick
                                            saObjectName="${SA_OBJECT_NAMES
                                                .IMPRESSION
                                                .SELF_SERVE_STATIC_FAQ_COMMENT}"
                                        ></textarea>
                                        <supr-form-error
                                            *ngIf="isInvalid"
                                            [errMsg]="
                                                selfServeConfig?.faq?.error
                                            "
                                        ></supr-form-error>
                                    </div>
                                </supr-list-item>

                                <div class="imageUpload">
                                    <div class="suprRow block">
                                        <div
                                            class="suprColumn left icon stretch"
                                        >
                                            <supr-svg></supr-svg>
                                        </div>

                                        <div class="suprColumn left text">
                                            <supr-text type="body">{{
                                                selfServeConfig?.faq
                                                    ?.uploadTitle
                                            }}</supr-text>

                                            <div class="upload">
                                                <supr-image-upload
                                                    [loading]="imageLoader"
                                                    [imageArray]="imageArray"
                                                    (fileChange)="
                                                        fileUpload($event)
                                                    "
                                                    (deleteImage)="
                                                        deleteImage($event)
                                                    "
                                                ></supr-image-upload>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </supr-accordion>
                        </div>
                    </div>
                </div>

                <supr-page-footer>
                    <supr-button
                        (click)="submitHandler()"
                        saImpression
                        saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                            .SELF_SERVE_STATIC_FAQ_SUBMIT}"
                        >{{ selfServeConfig?.faq?.submit }}</supr-button
                    >
                </supr-page-footer>
            </supr-modal>
        </div>
    `,
    styleUrls: [
        "../../../styles/faq.page.scss",
        "../../../styles/self-serve.page.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqChatComponent implements OnInit {
    @Input() isModal = false;
    @Input() transferToAgent = false;

    @Output()
    setFaqChatModal: EventEmitter<boolean> = new EventEmitter();

    selfServeConfig: SelfServe;
    disabled = true;
    isExpand = true;
    imageLoader: boolean = false;
    imageArray: string[] = [];
    userIssue: string = "";
    isInvalid = false;

    constructor(
        public selfServeStore: SelfServeStore,
        private toastService: ToastService,
        private cd: ChangeDetectorRef,
        private routerService: RouterService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    trackByFn(index: number): number {
        return index;
    }

    handleModalClose() {
        this.setFaqChatModal.emit(false);
        if (this.transferToAgent) {
            this.goToSupportPage();
        }
    }

    fileUpload(file: File) {
        this.imageLoader = true;

        this.selfServeStore
            .imageUpload(file)
            .pipe(take(1))
            .subscribe(
                (data: any) => {
                    this.imageLoader = false;
                    this.imageArray.push(data && data.url);

                    this.toastService.present(
                        this.selfServeConfig &&
                            this.selfServeConfig.imageUploadPage &&
                            this.selfServeConfig.imageUploadPage.success
                    );

                    this.cd.markForCheck();
                },
                () => {
                    this.imageLoader = false;
                    this.toastService.present(
                        this.selfServeConfig &&
                            this.selfServeConfig.imageUploadPage &&
                            this.selfServeConfig.imageUploadPage.error
                    );

                    this.cd.markForCheck();
                }
            );
    }

    deleteImage(image: string) {
        const { imageArray } = this;
        const updatedImages = imageArray.filter((img) => image !== img);
        Object.assign(this, { imageArray: updatedImages });
    }

    submitHandler() {
        if (this.validate()) {
            let agentMessage = `
            FAQ Issue 
            description: ${this.userIssue}`;
            if (this.imageArray && this.imageArray.length) {
                agentMessage += `
                Uploaded Images:`;
                agentMessage = this.imageArray.reduce((acc, img) => {
                    acc += `
                    "${img}",   `;
                    return acc;
                }, agentMessage);
            }
            this.handleModalClose();
            this.routerService.goToSupportPage({
                state: {
                    agentTransfer: agentMessage,
                },
                replaceUrl: true,
            });
        }
    }

    setValidate() {
        this.isInvalid = false;
    }

    setNotes(event: any) {
        const { value = "" } = event && event.target;
        this.userIssue = value.trim();
    }

    goToSupportPage() {
        this.routerService.goToSupportPage({
            replaceUrl: true,
        });
    }

    private validate() {
        if (!this.userIssue) {
            this.isInvalid = true;
            return false;
        }
        return true;
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
