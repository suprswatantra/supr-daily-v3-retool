import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { SELF_SERVE } from "@constants";
import { SelfServe } from "@types";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

import { SelfServeStore } from "@store/self-serve";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-support-faq-header",
    template: `
        <div class="faqSupport" *mobxAutorun>
            <div *ngIf="selfServeStore?.staticData?.expand_template?.length">
                <div class="divider24"></div>

                <supr-text
                    type="bold24"
                    saImpression="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_STATIC_OTHER_QUERIES}"
                    >{{
                        selfServeConfig?.supportPage?.otherQueriesHeader
                    }}</supr-text
                >

                <div
                    *ngFor="
                        let faqNav of selfServeStore?.staticData
                            ?.expand_template;
                        trackBy: trackByFn
                    "
                >
                    <div class="divider16"></div>
                    <div class="separator"></div>
                    <div class="divider16"></div>
                    <supr-support-list
                        [faqNav]="faqNav"
                        (handleListClick)="listClick($event)"
                    ></supr-support-list>
                </div>
                <div class="divider16"></div>
                <div class="separator"></div>
            </div>
        </div>
    `,
    styleUrls: [
        "../../../styles/faq.page.scss",
        "../../../styles/self-serve.page.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqHeaderComponent implements OnInit {
    selfServeConfig: SelfServe;

    constructor(
        public selfServeStore: SelfServeStore,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    listClick(nav) {
        try {
            const data = this.utilService.getNestedValue(nav, "data");
            if (
                !this.utilService.isEmpty(data) &&
                !this.utilService.isEmpty(data.nav)
            ) {
                if (data.nav.transferToAgent) {
                    this.routerService.goToFaqPage(
                        {
                            state: {
                                transferToAgent: true,
                                header: nav.name,
                            },
                        },
                        nav.id
                    );
                    return;
                }
                const { type, url } = data.nav;
                this.routerService.navigateUrlByType(url, type, {
                    state: {
                        header: nav.name,
                    },
                });
                return;
            }
            this.routerService.goToFaqPage(
                {
                    state: {
                        header: nav.name,
                    },
                },
                nav.id
            );
        } catch (err) {
            this.routerService.goToFaqPage({}, nav.id);
        }
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
