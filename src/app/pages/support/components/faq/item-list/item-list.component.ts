import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SELFSERVE } from "@models";

@Component({
    selector: "supr-support-faq-item-list",
    template: `
        <div
            class="faqList"
            *ngFor="let faq of faqList; trackBy: trackByFn; last as isLast"
        >
            <supr-support-faq-item [faq]="faq"></supr-support-faq-item>
            <div class="divider16"></div>
            <div class="separator" *ngIf="!isLast"></div>
        </div>
    `,
    styleUrls: [
        "../../../styles/faq.page.scss",
        "../../../styles/self-serve.page.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemListComponent {
    @Input() faqList: SELFSERVE.STATIC_NODE;

    trackByFn(index: number): number {
        return index;
    }
}
