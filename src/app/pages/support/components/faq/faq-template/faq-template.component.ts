import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SELFSERVE } from "@models";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-support-faq-template",
    template: `
        <div class="faqTemplate">
            <div
                class="component"
                *ngFor="let component of faqTemplate; trackBy: trackByFn"
                [ngSwitch]="component?.nodeType"
            >
                <div *ngSwitchCase="'text'" class="text">
                    <supr-text-fragment
                        [textFragment]="component?.data"
                    ></supr-text-fragment>
                </div>
                <div *ngSwitchCase="'link'" class="text">
                    <ion-row (click)="handleClick(component)" class="link">
                        <supr-text> {{ component.data.text }}</supr-text>
                    </ion-row>
                </div>
                <div *ngSwitchCase="'list'" class="list">
                    <supr-text-fragment
                        [textFragment]="component?.data"
                    ></supr-text-fragment>

                    <supr-ordered-list
                        [list]="component?.data?.listItems"
                    ></supr-ordered-list>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../../styles/faq.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqTemplateComponent {
    @Input() faqTemplate: SELFSERVE.STATIC_NODE[];

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    handleClick(component) {
        const data = this.utilService.getNestedValue(component, "data");
        if (
            !this.utilService.isEmpty(data) &&
            !this.utilService.isEmpty(data.nav)
        ) {
            const { type, url } = data.nav;
            this.routerService.navigateUrlByType(url, type);
            return;
        }
    }
}
