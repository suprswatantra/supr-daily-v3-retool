import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ICONS } from "@pages/support/constants";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SELFSERVE } from "@models";

@Component({
    selector: "supr-support-faq-item",
    template: `
        <div
            (click)="toggleItem()"
            saImpression
            saClick
            [saObjectName]="getSaObjectName()"
            [saObjectValue]="faq?.name"
        >
            <div class="suprRow spaceBetween item">
                <div class="heading">
                    <supr-text type="body"> {{ faq?.name }}</supr-text>
                </div>
                <supr-icon
                    [class.up]="!folded"
                    name="${ICONS.CHEVRON_DOWN}"
                ></supr-icon>
            </div>

            <div class="description" [class.suprHide]="folded">
                <supr-support-faq-template
                    [faqTemplate]="faq?.expand_template"
                ></supr-support-faq-template>
            </div>
        </div>
    `,
    styleUrls: ["../../../styles/faq.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemComponent {
    @Input() faq: SELFSERVE.STATIC_NODE;

    folded = true;

    toggleItem() {
        this.folded = !this.folded;
    }

    getSaObjectName(): string {
        return !this.folded
            ? SA_OBJECT_NAMES.CLICK.QUESTION_CLOSE
            : SA_OBJECT_NAMES.CLICK.QUESTION_OPEN;
    }
}
