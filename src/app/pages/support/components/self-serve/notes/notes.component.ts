import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SelfServeStore } from "@store/self-serve";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Component({
    selector: "supr-notes",
    template: `
        <div *mobxAutorun>
            <supr-accordion
                [title]="store.notesComputedData?.name"
                [subTitle]="store.notesComputedData?.description"
                icon="EDIT"
                [disabled]="isDisabled"
                *ngIf="isNotesComponent"
            >
                <supr-list-item
                    islast="true"
                    [isExpand]="isExpand"
                    [selected]="selected"
                    [disabled]="isDisabled"
                >
                    <div class="listItem">
                        <supr-text>{{
                            store.notesComputedData?.childName
                        }}</supr-text>
                    </div>

                    <div class="listContent">
                        <textarea
                            [value]="store.notes"
                            (blur)="setNotes($event)"
                            rows="5"
                            type="text"
                            [attr.disabled]="_disabled"
                            saImpression
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_NOTES}"
                            [saObjectValue]="store.notes"
                        ></textarea>
                    </div>
                </supr-list-item>
            </supr-accordion>
        </div>
    `,
    styleUrls: ["../../../styles/self-serve.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotesComponent {
    @Input() set disabled(_disabled: boolean) {
        this.isDisabled = _disabled;
        this._disabled = _disabled ? "true" : null;
    }

    isDisabled: boolean;
    _disabled: string;
    isExpand = true;
    selected = true;

    constructor(private store: SelfServeStore) {}

    get isNotesComponent() {
        if (this.isDisabled && !this.store.notes) {
            return false;
        }
        return this.store.notesComputedData;
    }

    setNotes(event: any) {
        const { value = "" } = event && event.target;
        this.store.setNotes(value.trim());
    }
}
