import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SelfServeStore } from "@store/self-serve";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SELFSERVE } from "@shared/models";

@Component({
    selector: "supr-service-issues",
    template: `
        <div class="serviceIssues" *mobxReaction="setServiceData.bind(this)">
            <supr-accordion
                [title]="name"
                [subTitle]="description"
                icon="SETTINGS"
                [disabled]="disabled"
                *ngIf="serviceRelatedIssues && serviceRelatedIssues.length > 0"
            >
                <supr-list-item
                    *ngFor="let issue of serviceRelatedIssues; last as isLast"
                    [islast]="isLast"
                    [disabled]="disabled || issue.disabled"
                    [selected]="selected"
                    (clickHandler)="issueSelect($event)"
                    [data]="issue"
                    saImpression
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_SERVICE_ISSUES}"
                    [saObjectValue]="issue.name"
                >
                    <div class="listItem">
                        <supr-text [class.disabled]="issue.disabled">{{
                            issue?.name
                        }}</supr-text>

                        <div class="serviceComplaint">
                            <supr-text
                                *ngIf="issue.disabled && issue.complaint_id"
                                >Issue id #{{ issue.complaint_id }} already
                                exists
                            </supr-text>
                        </div>
                    </div>
                </supr-list-item>
            </supr-accordion>
        </div>
    `,
    styleUrls: ["../../../styles/self-serve.page.scss"],

    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceIssues {
    @Input() disabled: boolean = false;

    selected = false;
    serviceRelatedIssues: SELFSERVE.Node_TYPE[] = [];
    name: string;
    description: string;

    constructor(private selfServeStore: SelfServeStore) {}

    setServiceData() {
        const {
            selectedServiceIssues = [],
            name = "",
            description = "",
            expand_template = "",
        } = this.selfServeStore.serviceComputedData;
        this.name = name;
        this.description = description;
        if (this.disabled) {
            this.selected = true;
            this.serviceRelatedIssues = selectedServiceIssues || [];
        } else {
            this.serviceRelatedIssues = expand_template || [];
        }
        return {
            selectedServiceIssues,
            name,
            description,
            expand_template,
        };
    }

    issueSelect(issue) {
        this.selfServeStore.setServiceIssue(issue);
    }
}
