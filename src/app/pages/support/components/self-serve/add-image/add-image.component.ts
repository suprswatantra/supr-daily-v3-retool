import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    ChangeDetectorRef,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

import { SELFSERVE } from "@shared/models";
import { Subscription } from "rxjs";
import { take } from "rxjs/operators";

import { SelfServeStore } from "@store/self-serve";
import { SettingsService } from "@services/shared/settings.service";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { ToastService } from "@services/layout/toast.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SelfServe } from "@types";
import { SELF_SERVE } from "@constants";

@Component({
    selector: "supr-add-image",
    template: ` <div class="imageUpload">
        <supr-accordion
            [title]="selfServeConfig?.imageUploadPage?.title"
            [subTitle]="selfServeConfig?.imageUploadPage?.subTitle"
            icon="RECEIPT"
            [disabled]="summary"
        >
            <div class="suprRow block">
                <div class="suprColumn left icon stretch">
                    <supr-svg></supr-svg>
                </div>

                <div class="suprColumn left text">
                    <supr-text type="body">{{
                        selfServeConfig?.imageUploadPage?.textTitle
                    }}</supr-text>
                    <div class="divider4"></div>
                    <supr-text type="caption" class="subtitle">{{
                        selfServeConfig?.imageUploadPage?.textSubtitle
                    }}</supr-text>

                    <div class="list">
                        <div
                            *ngFor="let order of selectedOrders; index as count"
                            class="suprRow spaceBetween order"
                        >
                            <div
                                class="suprColumn left stretch"
                                saImpression
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .SELF_SERVE_IMAGE_ORDER}"
                                [saObjectValue]="order?.orderData?.id"
                            >
                                <supr-text type="caption" class="caption">
                                    {{ count + 1 }}.
                                    {{ order?.sku_name }}</supr-text
                                >
                            </div>
                            <div class="suprColumn right stretch quantity">
                                <supr-text type="caption" class="caption">{{
                                    order?.quantity
                                }}</supr-text>
                            </div>
                        </div>
                    </div>

                    <div class="upload">
                        <supr-image-upload
                            [loading]="imageLoader"
                            [imageArray]="selfServeStore.uploadedImages"
                            [disabled]="summary"
                            (fileChange)="fileUpload($event)"
                            (deleteImage)="deleteImage($event)"
                        ></supr-image-upload>
                    </div>
                </div></div
        ></supr-accordion>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../../styles/self-serve.page.scss"],
})
export class AddImageComponent implements OnInit {
    @Input() summary: boolean = false;

    @Output() supportPage: EventEmitter<void> = new EventEmitter();

    selfServeConfig: SelfServe;
    isExpand: boolean = true;
    selected: boolean = true;
    selectedOrders: SELFSERVE.OrderSelfServe[];
    imageLoader: boolean = false;
    subscribe$: Subscription;

    constructor(
        public selfServeStore: SelfServeStore,
        private settingsService: SettingsService,
        private cd: ChangeDetectorRef,
        private analyticsService: AnalyticsService,
        private toastService: ToastService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
        this.selectedOrders = this.selfServeStore.imageUploadOrders;
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }

    fileUpload(file: File) {
        this.imageLoader = true;
        this.analyticsService.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_IMAGE_UPLOAD,
            objectValue: `${file.name}`,
        });
        this.subscribe$ = this.selfServeStore
            .imageUpload(file)
            .pipe(take(1))
            .subscribe(
                (data: any) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_IMAGE_UPLOAD_SUCCESS,
                        objectValue: `${file.name}`,
                    });
                    this.imageLoader = false;
                    this.pushImage(data && data.url);
                    this.toastService.present(
                        this.selfServeConfig &&
                            this.selfServeConfig.imageUploadPage &&
                            this.selfServeConfig.imageUploadPage.success
                    );

                    this.cd.markForCheck();
                },
                (err) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_IMAGE_UPLOAD_FAILURE,
                        objectValue: `${file.name}`,
                    });
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_ERROR_INTERUPT,
                        objectValue: JSON.stringify(err),
                    });
                    this.imageLoader = false;
                    this.toastService.present(
                        this.selfServeConfig &&
                            this.selfServeConfig.imageUploadPage &&
                            this.selfServeConfig.imageUploadPage.error
                    );
                    setTimeout(() => {
                        this.supportPage.emit();
                    }, 2000);
                    this.cd.markForCheck();
                }
            );
    }

    pushImage(url: string) {
        this.selfServeStore.setUploadedImages(url);
    }

    deleteImage(image: string) {
        this.selfServeStore.deleteUploadedImages(image);
    }

    ngOnDestroy() {
        if (this.subscribe$) {
            this.subscribe$.unsubscribe();
        }
    }
}
