import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { SelfServeStore } from "@store/self-serve";
import { OrderDeliverySlotMap, Order } from "@models";
import { SELFSERVE } from "@shared/models";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

import { SelfServe } from "@types";
import { SELF_SERVE } from "@constants";

import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-order-issues",
    template: `
        <div *mobxReaction="ordersArray.bind(this)">
            <supr-accordion
                [title]="name"
                [subTitle]="description"
                icon="bag_cart"
                [disabled]="disabled"
                *ngIf="computedOrders && computedOrders.length > 0"
            >
                <supr-list-item
                    (clickHandler)="ondSelect($event)"
                    [data]="ond_template"
                    *ngIf="showOnd"
                    [selected]="ond_template?.isOndSelected"
                    [disabled]="disabled"
                    saImpression
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_OND}"
                    [saObjectValue]="ond_template?.isOndSelected"
                >
                    <div class="listItem">
                        <supr-text>{{ ond_template?.name }}</supr-text>
                    </div>
                </supr-list-item>

                <supr-list-item
                    *ngFor="let _order of computedOrders; last as isLast"
                    [islast]="isLast"
                    [isExpand]="isExpand"
                    [selected]="_order.selected"
                    [disabled]="_order.disabled || disabled"
                    (clickHandler)="orderSelect($event)"
                    [data]="_order"
                >
                    <div class="listItem">
                        <supr-order-history-container
                            [class.disabled]="_order.disabled"
                            [date]="selfServeStore.date"
                            [order]="_order.orderData"
                            saImpression
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_ORDERS}"
                            [saObjectValue]="_order.orderData?.id"
                            [saContext]="_order.orderData?.statusNew"
                        ></supr-order-history-container>
                    </div>
                    <div
                        class="pseudoDropdown suprRow spaceBetween listBuffer"
                        *ngIf="
                            !_order.isOrderDetails &&
                            !disabled &&
                            !_order.disabled &&
                            selfServeConfig?.experiments?.includes(
                                'pseudoDropdown'
                            )
                        "
                    >
                        <supr-text
                            >{{ selfServeConfig.pseudoDropdown }}
                        </supr-text>

                        <supr-icon
                            class="secondary"
                            name="chevron_down"
                            *ngIf="!disabled"
                        ></supr-icon>
                    </div>
                    <div class="listContent">
                        <supr-order-details
                            [id]="_order.orderData?.id"
                            [disabled]="disabled"
                        >
                        </supr-order-details>
                    </div>
                </supr-list-item>
            </supr-accordion>
        </div>
    `,
    styleUrls: ["../../../styles/self-serve.page.scss"],

    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelfServeOrderIssues implements OnInit {
    @Input() summary: boolean = false;

    @Input() set orderHistory(history: OrderDeliverySlotMap) {
        this.setOrderHistory(history);
    }

    selfServeConfig: SelfServe;
    disabled = false;
    _orderHistory: Order[] = [];
    name: string;
    description: string;
    computedOrders: SELFSERVE.OrderSelfServe[];
    ond_template: SELFSERVE.OND_Template;
    isExpand: boolean = true;

    constructor(
        private selfServeStore: SelfServeStore,
        private settingsService: SettingsService
    ) {}

    get showOnd() {
        const { disabled, ond_template } = this;
        const { isOndSelected, disabled: ondDisabled, is_ond } = ond_template;
        if (disabled && ond_template) {
            return isOndSelected;
        } else {
            return is_ond && !ondDisabled;
        }
    }

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    ordersArray() {
        const {
            orderRelatedIssues,
            orderData,
            isOndSelected,
        } = this.selfServeStore;
        const { name = "", description = "", ond_template } = orderData;

        this.name = name;
        this.description = description;
        this.ond_template = { ...ond_template, isOndSelected };

        const ordersArray = [];
        if (this.summary) {
            this.disabled = true;

            orderRelatedIssues.forEach((value) => {
                if (value && value.selected) {
                    ordersArray.push(value);
                }
            });
        } else {
            orderRelatedIssues.forEach((value, key) => {
                const orderData = this._orderHistory.find(
                    (historyOrder) => historyOrder.id === key
                );
                if (orderData && orderData.id)
                    ordersArray.push({
                        orderData,
                        ...value,
                    });
            });
        }

        this.computedOrders = ordersArray;
        return this.computedOrders;
    }

    setOrderHistory(history: OrderDeliverySlotMap) {
        let arr = [];
        if (
            history &&
            (history.REGULAR_DELIVERY || history.ALTERNATE_DELIVERY)
        ) {
            const { REGULAR_DELIVERY = [], ALTERNATE_DELIVERY = [] } = history;
            arr = [...REGULAR_DELIVERY, ...ALTERNATE_DELIVERY] || [];
        }
        this._orderHistory = arr || [];
    }

    orderSelect(order: SELFSERVE.OrderSelfServe) {
        this.selfServeStore.setSelectedOrders(order);
    }

    ondSelect(ond) {
        this.selfServeStore.setOnd(ond, this._orderHistory);
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
