import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { SelfServeStore } from "@store/self-serve";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SettingsService } from "@services/shared/settings.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { DropdownOptions, SelfServe } from "@types";
import { SELF_SERVE } from "@constants";
import { Feedback } from "@models";

@Component({
    selector: "supr-order-details",
    template: `
        <div *mobxReaction="orderDetails.bind(this)">
            <div class="orderDetails" *ngIf="isOrderDetails">
                <supr-dropdown
                    [dropDownOptions]="dropdownList"
                    classSelector="issueDropdown"
                    [title]="
                        selfServeConfig?.selfServePage
                            ?.complaintTypesOptionsTitle
                    "
                    [isModal]="true"
                    (select)="issueSelection($event)"
                    (deSelect)="deselectIssue($event)"
                    [disabled]="disabled"
                    [default]="selfServeConfig?.defaultDropdown"
                    *ngIf="dropdownList?.length"
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .SELF_SERVE_COMPLAINTS}"
                    [saObjectValue]="id"
                ></supr-dropdown>

                <div class="suprRow quantitySelection" *ngIf="maxQuantity">
                    <div class="suprColumn left">
                        <supr-text>{{ quantityHeader }}</supr-text>
                    </div>
                    <div class="suprColumn right">
                        <supr-number-counter
                            [quantity]="quantity"
                            [thresholdQty]="-1"
                            (handleClick)="updateQty($event)"
                            [plusDisabled]="
                                disabled || quantity === maxQuantity
                            "
                            [minusDisabled]="disabled"
                            [class.disabled]="disabled"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_QUANTITY}"
                            [saObjectValue]="id"
                        >
                        </supr-number-counter>
                    </div>
                </div>

                <div class="feedback" *ngIf="feedbackParams.length > 0">
                    <supr-text>{{
                        selfServeConfig?.selfServePage?.feedbackTitle
                    }}</supr-text>
                    <div class="divider16"></div>
                    <div class="buttonList">
                        <supr-button
                            *ngFor="let param of feedbackParams"
                            [class.selected]="
                                feedbackParamsObject &&
                                feedbackParamsObject[param]
                            "
                            [class.disabled]="disabled"
                            (click)="handleSelectParams(param)"
                            saClick
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_FEEDBACK_PARAMS}"
                            [saObjectValue]="param"
                            >{{ param }}</supr-button
                        >
                    </div>
                    <supr-form-error
                        id="errormsg"
                        [errMsg]="error"
                        *ngIf="error"
                    ></supr-form-error>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../../../styles/self-serve.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelfServeOrderDetails implements OnInit {
    @Input() disabled: boolean = false;
    @Input() id: number;

    quantity: number;
    feedbackParamsObject: Feedback.ParamsObject;
    feedbackParams: string[] = [];
    dropdownList: DropdownOptions[] = [];
    maxQuantity: number;
    isOrderDetails: boolean = false;
    error: string = "";
    quantityHeader: string = "";
    selfServeConfig: SelfServe;

    constructor(
        private selfServeStore: SelfServeStore,
        private settingsService: SettingsService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.selfServeConfig = this.fetchSelfServeConfig();
    }

    orderDetails() {
        const { orderRelatedIssues, quantityData } = this.selfServeStore;

        const orderDetails = orderRelatedIssues.get(this.id);
        this.quantityHeader = (quantityData && quantityData.name) || "";
        if (orderDetails && orderDetails.isOrderDetails) {
            const {
                feedbackParamsObject,
                issue,
                quantity,
                dropdownList = [],
                maxQuantity,
                feedbackParams = [],
                isOrderDetails,
                error,
            } = orderDetails;
            this.maxQuantity = maxQuantity;
            this.dropdownList = dropdownList;
            this.feedbackParams = feedbackParams;
            this.isOrderDetails = isOrderDetails;
            this.error = error;
            this.feedbackParamsObject = feedbackParamsObject;
            this.quantity = quantity;

            if (this.disabled) {
                this.feedbackParams = this.getFeedbackParams(
                    feedbackParamsObject
                );
            }

            return {
                feedbackParamsObject,
                issue,
                quantity,
                dropdownList,
                maxQuantity,
                feedbackParams,
                isOrderDetails,
            };
        }
        return;
    }

    getFeedbackParams(feedbackParamsObject) {
        const feedbackParams = [];
        for (const [key, val] of Object.entries(feedbackParamsObject)) {
            if (val) {
                feedbackParams.push(key);
            }
        }
        return feedbackParams;
    }

    updateQty(direction: number) {
        this.selfServeStore.setQuantity(direction, this.id);
        this.analyticsService.trackClick({
            objectName:
                direction > 0
                    ? SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_QUANTITY_PLUS
                    : SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_QUANTITY_MINUS,
            objectValue: `${this.quantity}`,
            context: `${this.id}`,
        });
    }

    handleSelectParams(param: string) {
        if (this.disabled) {
            return;
        }
        this.selfServeStore.toggleFeedbackParams(param, this.id);
    }

    issueSelection(issue?: string) {
        if (!issue) {
            this.selfServeStore.deselectSelectedOrders(this.id);
            return;
        }
        this.analyticsService.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_COMPLAINT_TYPE,
            objectValue: issue,
            context: `${this.id}`,
        });
        this.selfServeStore.setIssue(issue, this.id);
    }

    deselectIssue() {
        this.selfServeStore.deselectIssue(this.id);
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
