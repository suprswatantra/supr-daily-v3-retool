import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

import { SupportConfig, TodayOrder } from "@pages/support/types";
import { Complaintv2Store } from "@store/complaintv2";
import {
    SETTINGS_KEYS_DEFAULT_VALUE,
    ANALYTICS_OBJECT_NAMES,
    MODAL_NAMES,
    MODAL_TYPES,
} from "@constants";
import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

import { SettingsService } from "@services/shared/settings.service";
import { SegmentService } from "@services/integration/segment.service";
import { RouterService } from "@services/util/router.service";
import { FreshChatService } from "@services/integration/freshchat.service";

@Component({
    selector: "supr-support-ivr",
    template: `
        <div class="ivr">
            <supr-modal
                *ngIf="isModal"
                modalName="${MODAL_NAMES.IVR_CALL_MODAL}"
                modalType="${MODAL_TYPES.NO_WRAPPER}"
                [fullScreen]="true"
                (handleClose)="handleModalClose()"
            >
                <supr-page-header (handleBackButtonClick)="handleModalClose()">
                    <supr-text type="subheading">{{
                        supportConfig?.ivr_popup?.header
                    }}</supr-text>
                </supr-page-header>

                <div class="suprScrollContent">
                    <div class="ivrCall">
                        <supr-text type="subtitle">{{
                            supportConfig?.ivr_popup?.title
                        }}</supr-text>
                        <div class="divider12"></div>
                        <supr-text type="body">{{
                            supportConfig?.ivr_popup?.subTitle
                        }}</supr-text>
                        <div class="divider12"></div>
                        <supr-text type="body" class="listHeader">{{
                            supportConfig?.ivr_popup?.listHeader
                        }}</supr-text>

                        <supr-ordered-list
                            [list]="supportConfig?.ivr_popup?.list"
                        ></supr-ordered-list>
                    </div>
                </div>

                <supr-page-footer>
                    <div>
                        <supr-button
                            class="secondary"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_IVR_OTHER_QUERIES}"
                            (click)="navigateToNonOrdersFlow()"
                            >{{
                                supportConfig?.ivr_popup?.nonOrderButton
                            }}</supr-button
                        >
                    </div>
                    <div>
                        <supr-button
                            [class.secondary]="todayData?.isTodayOrders"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_IVR_OLDER_ORDERS}"
                            (click)="navigateToOrderCalendar()"
                            >{{
                                supportConfig?.ivr_popup?.previousOrderButton
                            }}</supr-button
                        >
                    </div>

                    <div *ngIf="todayData?.isTodayOrders">
                        <supr-button
                            (click)="navigateToSelfServe()"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_IVR_TODAY_ORDER}"
                            >{{ supportConfig?.ivr_popup?.todayOrderButton }}
                        </supr-button>
                    </div>
                </supr-page-footer>
            </supr-modal>
        </div>
    `,
    styleUrls: ["../../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IvrCallComponent implements OnInit {
    @Input() isModal: boolean = false;
    @Input() todayData: TodayOrder;

    @Output() closeModal: EventEmitter<void> = new EventEmitter();
    @Output() scrollTo: EventEmitter<string> = new EventEmitter();

    supportConfig: SupportConfig;
    constructor(
        private settingsService: SettingsService,
        private segmentService: SegmentService,
        public complaintv2Store: Complaintv2Store,
        private routerService: RouterService,
        private freshChatService: FreshChatService
    ) {}

    ngOnInit() {
        this.supportConfig = this.fetchsupportConfig();
    }

    navigateToNonOrdersFlow() {
        this.handleModalClose();
        this.scrollTo.emit("nonOrderFlows");
    }

    navigateToOrderCalendar() {
        this.handleModalClose();
        this.routerService.goToSchedulePage();
    }

    navigateToSelfServe() {
        this.handleModalClose();
        this.freshChatService.chatActionButton(
            this.todayData.todayDate,
            this.todayData.slot
        );
    }

    handleModalClose() {
        this.closeModal.emit();
    }

    private fetchsupportConfig() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        this.segmentService.trackImpression({
            objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.SUPPORT_CONFIG,
            objectValue: `${JSON.stringify(supportConfig)}`,
        });
        return supportConfig;
    }
}
