import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SELFSERVE } from "@models";

@Component({
    selector: "supr-support-list",
    template: `
        <div
            class="suprRow list"
            (click)="handleListClick.emit(faqNav)"
            *ngIf="faqNav.nodeType === 'button'"
            saClick
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_STATIC_FAQ}"
            [saObjectValue]="faqNav?.name"
            [saContext]="faqNav.id"
        >
            <div class="suprColumn left col1">
                <supr-icon name="{{ faqNav?.data?.icon }}"></supr-icon>
            </div>
            <div class="suprColumn left">
                <supr-text type="body">
                    {{ faqNav?.name }}
                </supr-text>
            </div>
            <div class="suprColumn right col2">
                <supr-icon name="chevron_right"></supr-icon>
            </div>
        </div>
    `,
    styleUrls: ["../../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent {
    @Output() handleListClick: EventEmitter<any> = new EventEmitter();
    @Input() faqNav: SELFSERVE.STATIC_NODE;
}
