import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { SELFSERVE } from "@models";
import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { SupportConfig } from "@pages/support/types";

import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-support-content",
    template: `
        <div class="wrapper">
            <supr-support-faq-header></supr-support-faq-header>

            <div *ngIf="isArchive">
                <div class="divider24"></div>
                <supr-text type="bold24">{{
                    supportList?.data?.title
                }}</supr-text>
                <div class="divider16"></div>

                <div class="saperator"></div>
                <div class="divider16"></div>
                <supr-support-list
                    [faqNav]="supportList"
                    (handleListClick)="issuesClick($event)"
                    saClick
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ARCHIVE_ISSUES}"
                ></supr-support-list>
                <div class="divider16"></div>
                <div class="saperator"></div>
            </div>

            <div class="divider24"></div>
        </div>
    `,
    styleUrls: ["../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit {
    @Input() isArchive: boolean = true;

    supportList: SELFSERVE.STATIC_NODE;
    supportConfig: SupportConfig;

    constructor(
        private routerService: RouterService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.supportConfig = this.fetchsupportConfig();
        this.supportList = this.supportConfig.archive;
    }

    private fetchsupportConfig() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );

        return supportConfig;
    }

    issuesClick() {
        this.routerService.goToComplaintsPage();
    }
}
