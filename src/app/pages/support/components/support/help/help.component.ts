import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

import { take } from "rxjs/operators";
import { CallNumber } from "@ionic-native/call-number/ngx";

import { SupportConfig } from "@pages/support/types";
import { User, SupportOptions } from "@models";
import { Complaintv2Store } from "@store/complaintv2";
import {
    SETTINGS_KEYS_DEFAULT_VALUE,
    ANALYTICS_OBJECT_NAMES,
    FRESHBOT_SCRIPT_V3,
} from "@constants";

import { SettingsService } from "@services/shared/settings.service";
import { FreshChatService } from "@services/integration/freshchat.service";
import { FreshBotService } from "@services/integration/freshbot.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { AnalyticsService } from "@services/integration/analytics.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-support-help",
    template: `
        <div *mobxReaction="getOptions.bind(this)">
            <supr-support-options
                [chat]="chat"
                [call]="call"
                [help]="help"
                [isChat]="supportOptions?.chat_access"
                [isCall]="supportOptions?.call_access"
                (chatActionButton)="chatActionButton()"
                (callActionButton)="callActionButton()"
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.SUPPORT}"
                [saContext]="isSuprAccess"
            ></supr-support-options>
        </div>
    `,
    styleUrls: ["../../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelpComponent implements OnInit {
    @Input() user: User;

    @Output() callHandler: EventEmitter<void> = new EventEmitter();

    supportOptions: SupportOptions;
    supportConfig: SupportConfig;
    isSuprAccess = false;

    chat: string;
    call: string;
    help: string;

    constructor(
        private callNumber: CallNumber,
        private settingsService: SettingsService,
        private freshChatService: FreshChatService,
        private freshBotService: FreshBotService,
        private suprPassService: SuprPassService,
        public complaintv2Store: Complaintv2Store,
        private analyticsService: AnalyticsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.isSuprAccess = this.suprPassService.getIsSuprAccessMember();
        this.supportConfig = this.fetchsupportConfig();
        this.chat = this.supportConfig.chat;
        this.call = this.supportConfig.call;
        this.help = this.supportConfig.help;
    }

    getOptions() {
        const { supportOptions } = this.complaintv2Store;
        const { call_access, chat_access, ivr_popup } = supportOptions;
        const { isCall, isChat } = this.supportConfig;
        this.supportOptions = {
            chat_access: isChat && chat_access,
            call_access: isCall && call_access,
            ivr_popup,
        };
        return supportOptions;
    }

    callActionButton() {
        this.complaintv2Store
            .fetchSupportOptions()
            .pipe(take(1))
            .subscribe((supportOptions: SupportOptions) => {
                this.complaintv2Store.setSupportOptions(supportOptions);
                const ivrPopupOption = this.utilService.getNestedValue(
                    supportOptions,
                    "ivr_popup"
                );
                const ivrPopupConfig = this.utilService.getNestedValue(
                    this.supportConfig,
                    "ivr_popup"
                );
                if (
                    ivrPopupOption &&
                    ivrPopupConfig &&
                    ivrPopupConfig.enabled
                ) {
                    this.callHandler.emit();
                    return;
                }
                this.openDialer(this.supportConfig.number);
            });
    }

    chatActionButton() {
        this.openChatService();
    }

    private fetchsupportConfig() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        this.analyticsService.trackImpression({
            objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.SUPPORT_CONFIG,
            objectValue: `${JSON.stringify(supportConfig)}`,
        });
        return supportConfig;
    }

    private fetchFreshBotConfig(): boolean {
        const freshBotConfig = this.settingsService.getSettingsValue(
            "freshbotConfigV3",
            FRESHBOT_SCRIPT_V3
        );
        return freshBotConfig && freshBotConfig.ENABLED;
    }

    private openDialer(phoneNo: string) {
        this.callNumber
            .callNumber(this.supportConfig.number, false)
            .catch(() => {
                // Open the dialer - this doesn't need to seek any permission
                window.location.href = `tel:${phoneNo}`;
            });
    }

    private openChatService() {
        if (
            this.fetchFreshBotConfig() &&
            window &&
            window.Freshbots &&
            !this.freshBotService.getFreshbotError()
        ) {
            // Open FreshBot
            this.freshBotService.initBotChat();
        } else {
            this.analyticsService.trackImpression({
                objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.FRESHCHAT_LOAD,
                objectValue: `${JSON.stringify({
                    isBotEnabled: this.fetchFreshBotConfig(),
                    isBotScriptLoaded: window.Freshbots,
                    isScriptError: this.freshBotService.getFreshbotError(),
                })}`,
            });
            this.freshChatService.openChat(this.user);
        }
    }
}
