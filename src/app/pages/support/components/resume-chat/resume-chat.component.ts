import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";
import { FreshChatService } from "@services/integration/freshchat.service";
import { User } from "@models";

@Component({
    selector: "supr-resume-chat",
    template: `
        <div class="resumeChat" *ngIf="isChatResume">
            <div class="divider16"></div>

            <supr-text type="bold24">Active chat</supr-text>

            <div
                class="suprRow resumeChatContent"
                saClick
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.RESUME_CHAT}"
                (click)="handleClick()"
            >
                <div class="suprColumn left image">
                    <supr-svg></supr-svg>
                </div>

                <div class="suprColumn left textbox">
                    <supr-text type="bold16">{{ title }}</supr-text>
                    <supr-text type="caption" *ngIf="count; else default"
                        >{{ count }} {{ subtitle }}</supr-text
                    >
                    <ng-template #default>
                        <supr-text type="caption">Online</supr-text>
                    </ng-template>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResumeChatComponent {
    @Input() isChatResume: boolean = false;
    @Input() user: User;
    @Input() subtitle: string;
    @Input() title: string;
    @Input() count: number;

    constructor(private freshChatService: FreshChatService) {}

    handleClick() {
        this.freshChatService.openChat(this.user);
    }
}
