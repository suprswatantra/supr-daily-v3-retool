import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    EventEmitter,
    Output,
    ChangeDetectorRef,
} from "@angular/core";
import { take } from "rxjs/operators";

import { DateService } from "@services/date/date.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { SelfServeStore } from "@store/self-serve";

import { SETTINGS_KEYS_DEFAULT_VALUE, SELF_SERVE } from "@constants";
import { OrderHistory } from "@models";
import { FetchOrdersActionPayload, SuprApi } from "@types";
import { SupportConfig } from "@pages/support/types";
import { SupportPageAdapter as Adapter } from "@pages/support/services/support.adapter";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Component({
    selector: "supr-support-order-calendar",
    template: ` <div
        class="supportCalendar"
        *ngIf="config?.enabled && datesArray.length > 0"
    >
        <div class="divider24"></div>

        <supr-text type="bold24">{{ config?.title }}</supr-text>

        <div class="divider16"></div>
        <div class="orderCalendar">
            <supr-order-delivery-container
                [date]="date"
                [id]="date"
                *ngFor="let date of datesArray; let last = last"
                [islastAccordion]="last"
                [supportCalendarConfig]="config"
            >
            </supr-order-delivery-container>
        </div>

        <div class="divider16"></div>

        <div class="suprRow calendarLink">
            <div class="suprColumn left">
                <supr-text type="action14">{{
                    config?.calendarNavigationText
                }}</supr-text>
            </div>

            <div class="suprColumn right link">
                <ion-row
                    (click)="handleClick()"
                    class="link"
                    saClick
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION.OLDER_ORDERS}"
                >
                    <supr-text type="caption">{{
                        config?.calendarNavigationLinkText
                    }}</supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </ion-row>
            </div>
        </div>
    </div>`,
    styleUrls: ["../../styles/support.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SupportOrderCalendar implements OnInit {
    datesArray = [];
    supportConfig: SupportConfig;

    @Output() fetchOrders: EventEmitter<
        FetchOrdersActionPayload
    > = new EventEmitter();

    constructor(
        private dateService: DateService,
        private settingsService: SettingsService,
        private utilService: UtilService,
        private routerService: RouterService,
        public selfServeStore: SelfServeStore,
        private cd: ChangeDetectorRef,
        private adapter: Adapter
    ) {}

    get config() {
        const { calendar, number } = this.fetchsupportConfig();
        return {
            isAccordion: true,
            ...calendar,
            number,
            saObjectNameCall: SA_OBJECT_NAMES.IMPRESSION.SUPPORT_CALENDAR_CALL,
            saObjectNameChat: SA_OBJECT_NAMES.IMPRESSION.SUPPORT_CALENDAR_CHAT,
        };
    }

    ngOnInit() {
        this.supportConfig = this.fetchsupportConfig();
        this.fetchSuportOrders();
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }

    private fetchsupportConfig() {
        if (!this.utilService.isEmpty(this.supportConfig)) {
            return this.supportConfig;
        }
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        const { enabled } = this.fetchSelfServeConfig();
        if (supportConfig && supportConfig.calendar) {
            supportConfig.calendar.enabled = enabled;
        }
        this.supportConfig = supportConfig;
        return supportConfig;
    }

    fetchOrderDates(orders: OrderHistory[]) {
        try {
            const supportConfig = this.fetchsupportConfig();

            if (
                this.utilService.isLengthyArray(orders) &&
                !this.utilService.isEmpty(supportConfig)
            ) {
                const { calendar } = supportConfig;

                const dates = orders.slice(0, calendar.orderDateNumber) || [];
                this.datesArray =
                    (this.utilService.isLengthyArray(dates) &&
                        dates.reverse().map((date) => date.date)) ||
                    [];

                this.cd.markForCheck();
            }
        } catch (err) {
            this.datesArray = [];
        }
    }

    fetchSuportOrders() {
        const { calendar } = this.fetchsupportConfig();
        const today = this.dateService.getTodayDate();
        this.selfServeStore
            .getOrders(
                this.dateService.textFromDate(today),
                calendar.orderDateLimit
            )
            .pipe(take(1))
            .subscribe((data: SuprApi.OrderHistoryData) => {
                this.adapter.setOrders(data);
                this.fetchOrderDates(data.order_history);
            });
    }

    handleClick() {
        this.routerService.goToSchedulePage();
    }
}
