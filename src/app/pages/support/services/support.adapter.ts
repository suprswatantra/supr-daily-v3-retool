import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import { OrderDeliverySlotMap } from "@models";
import {
    StoreState,
    UserStoreSelectors,
    OrderStoreActions,
    OrderStoreSelectors,
} from "@supr/store";

import { FetchOrdersActionPayload, SuprApi } from "@types";

@Injectable()
export class SupportPageAdapter {
    userState$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    isAnonymousUser$ = this.store.pipe(
        select(UserStoreSelectors.isAnonymousUser)
    );

    constructor(private store: Store<StoreState>) {}

    getOrders(payload: FetchOrdersActionPayload) {
        this.store.dispatch(
            new OrderStoreActions.GetOrdersDataAction({
                toDate: payload.toDate,
                firstLoad: payload.firstLoad,
                days: payload.days,
            })
        );
    }

    getOrderDeliveryByDate(date: string): Observable<OrderDeliverySlotMap> {
        return this.store.pipe(
            select(OrderStoreSelectors.selectOrderDeliveryByDate, { date })
        );
    }

    selectOrderList$ = this.store.pipe(
        select(OrderStoreSelectors.selectOrderList)
    );

    setOrders(payload: SuprApi.OrderHistoryData) {
        this.store.dispatch(
            new OrderStoreActions.GetOrdersDataSuccessAction({
                response: payload,
            })
        );
    }
}
