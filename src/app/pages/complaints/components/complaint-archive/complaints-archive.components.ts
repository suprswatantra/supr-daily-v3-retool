import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";
import { CallNumber } from "@ionic-native/call-number/ngx";

import { FreshChatService } from "@services/integration/freshchat.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

import {
    MODAL_NAMES,
    MODAL_TYPES,
    COMPLAINTS,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";
import { ComplaintSetting } from "@types";
import { SupportConfig } from "@pages/support/types";
import { ComplaintsListWithStatusV2, User, ComplaintViewV2 } from "@models";
import { Complaintv2Store } from "@store/complaintv2";

@Component({
    selector: "supr-complaints-archive",
    template: `
        <div
            class="complaintsArchive"
            *mobxReaction="handleComplaintsList.bind(this)"
        >
            <div *ngIf="complaintsListStatusV2['Reopened']?.length > 0">
                <supr-text>Reopened</supr-text>

                <supr-complaints-list
                    [ComplaintsListV2]="complaintsListStatusV2['Reopened']"
                    (handleClick)="clickAction($event)"
                    (complaintClick)="complaintAction($event)"
                    saObjectName="archive"
                ></supr-complaints-list>
                <div class="divider8"></div>
            </div>

            <div *ngIf="complaintsListStatusV2['Open']?.length > 0">
                <supr-text>Open</supr-text>

                <supr-complaints-list
                    [ComplaintsListV2]="complaintsListStatusV2['Open']"
                    (handleClick)="clickAction($event)"
                    (complaintClick)="complaintAction($event)"
                    saObjectName="archive"
                ></supr-complaints-list>
                <div class="divider8"></div>
            </div>

            <div *ngIf="complaintsListStatusV2['In-progress']?.length > 0">
                <supr-text>In-progress</supr-text>

                <supr-complaints-list
                    [ComplaintsListV2]="complaintsListStatusV2['In-progress']"
                    (handleClick)="clickAction($event)"
                    (complaintClick)="complaintAction($event)"
                    saObjectName="archive"
                ></supr-complaints-list>
                <div class="divider8"></div>
            </div>

            <div *ngIf="complaintsListStatusV2['Resolved']?.length > 0">
                <supr-text>Resolved</supr-text>

                <supr-complaints-list
                    [ComplaintsListV2]="complaintsListStatusV2['Resolved']"
                    (handleClick)="clickAction($event)"
                    (complaintClick)="complaintAction($event)"
                    saObjectName="archive"
                ></supr-complaints-list>
                <div class="divider8"></div>
            </div>

            <supr-modal
                *ngIf="showSupport"
                modalName="${MODAL_NAMES.COMPLAINTS_SUPPORT}"
                modalType="${MODAL_TYPES.BOTTOM_SHEET}"
                (handleClose)="handleSupportModal()"
            >
                <div class="supportModal">
                    <supr-support-options
                        [chat]="chat"
                        [call]="call"
                        [help]="help"
                        [subTitle]="subTitle"
                        [isChat]="isChat"
                        [isCall]="isCall"
                        saObjectName="archive"
                        [saObjectValue]="complaintId"
                        [saContext]="
                            complaintV2?.complaint_status_timeline
                                .complaint_status
                        "
                        (chatActionButton)="chatActionButton()"
                        (callActionButton)="callActionButton()"
                    ></supr-support-options>
                </div>
            </supr-modal>
        </div>
    `,
    styleUrls: ["../../styles/complaints.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintsArchiveComponent implements OnInit {
    @Input() user: User;

    isChat: boolean = false;
    isCall: boolean = false;
    chat: string;
    call: string;
    help: string;
    subTitle: string;

    showSupport: boolean = false;
    complaintConfig: ComplaintSetting;

    complaintsListStatusV2: ComplaintsListWithStatusV2 = {};
    supportConfig: SupportConfig;
    complaintId: number;
    complaintV2: ComplaintViewV2;

    ngOnInit() {
        this.supportConfig = this.fetchsupportConfig();
        this.complaintConfig = this.fetchComplaintConfig();

        this.chat = this.complaintConfig.CHAT;
        this.call = this.complaintConfig.CALL;
        this.help = this.complaintConfig.COMPLAINT_SUPPORT_TITLE;
        this.subTitle = this.complaintConfig.COMPLAINT_SUPPORT_SUBTITLE;
    }

    handleComplaintsList() {
        const { complaintsListV2 } = this.complaintv2Store;
        if (complaintsListV2 && complaintsListV2.length > 0) {
            const key = "complaint_status";

            this.complaintsListStatusV2 =
                complaintsListV2.reduce((result, currentValue) => {
                    const status =
                        currentValue &&
                        currentValue.complaint_status_timeline[key];
                    if (status) {
                        const value = result[status] || [];
                        value.push(currentValue);
                        result[status] = value;
                    }
                    return result;
                }, {}) || {};
        }
        return this.complaintsListStatusV2;
    }

    constructor(
        private callNumber: CallNumber,
        private settingsService: SettingsService,
        private freshChatService: FreshChatService,
        private routerService: RouterService,
        private dateService: DateService,
        private complaintv2Store: Complaintv2Store
    ) {}

    private fetchsupportConfig() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        return supportConfig;
    }

    private fetchComplaintConfig() {
        const complaintConfig = this.settingsService.getSettingsValue(
            "complaintConfig",
            COMPLAINTS
        );
        return complaintConfig;
    }

    callActionButton() {
        this.handleSupportModal(false);
        this.openDialer(this.supportConfig.number);
    }

    private openDialer(phoneNo: string) {
        this.callNumber
            .callNumber(this.supportConfig.number, false)
            .catch(() => {
                // Open the dialer - this doesn't need to seek any permission
                window.location.href = `tel:${phoneNo}`;
            });
    }

    chatActionButton() {
        this.handleSupportModal(false);
        this.openChatService();
    }

    private openChatService() {
        this.freshChatService.openChat(this.user);
        this.freshChatService.sendMessage(this.complaintId);
    }

    clickAction(complaint: ComplaintViewV2) {
        const days = this.dateService.daysFromToday(complaint.complaintDate);
        this.complaintId = complaint.complaint_id;
        this.complaintV2 = complaint;

        if (Math.abs(days) > this.complaintConfig.COMPLAINT_SUPPORT.DAYS) {
            this.showSupport = true;
            this.isCall = this.complaintConfig.COMPLAINT_SUPPORT.POST_DATE.IS_CALL;
            this.isChat = this.complaintConfig.COMPLAINT_SUPPORT.POST_DATE.IS_CHAT;
        } else {
            this.showSupport = true;
            this.isCall = this.complaintConfig.COMPLAINT_SUPPORT.PRE_DATE.IS_CALL;
            this.isChat = this.complaintConfig.COMPLAINT_SUPPORT.PRE_DATE.IS_CHAT;
        }
    }

    complaintAction(complaint: ComplaintViewV2) {
        this.routerService.goToComplaintsDetailsPage(
            {},
            complaint.complaint_id
        );
    }

    handleSupportModal(value: boolean = false) {
        this.showSupport = value;
    }
}
