import { ComplaintDetailsComponent } from "./complaint-details/complaint-details.component";
import { ComplaintTimelineComponent } from "./complaint-timeline/complaint-timeline.component";
import { ComplaintsArchiveComponent } from "./complaint-archive/complaints-archive.components";

export const complaintComponents = [
    ComplaintDetailsComponent,
    ComplaintTimelineComponent,
    ComplaintsArchiveComponent,
];
