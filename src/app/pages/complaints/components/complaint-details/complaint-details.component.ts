import { Component, ChangeDetectionStrategy } from "@angular/core";

import { Complaintv2Store } from "@store/complaintv2";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Component({
    selector: "supr-complaint-details",
    template: `
        <div *mobxReaction="getComplaintDetails.bind(this)">
            <supr-accordion
                [title]="store.complaintDetails?.title"
                [subTitle]="store.complaintDetails?.subTitle"
                icon="bag_cart"
                [disabled]="disabled"
            >
                <div *ngIf="store.complaintDetails?.orders?.length > 0">
                    <supr-list-item
                        *ngFor="
                            let _order of store.complaintDetails.orders;
                            last as isLast
                        "
                        [isExpand]="isExpand"
                        [selected]="_order.selected"
                        [disabled]="_order.disabled || disabled"
                        [data]="_order"
                    >
                        <div class="listItem">
                            <supr-order-history-container
                                [class.disabled]="_order.disabled"
                                [date]="store.complaint?.order_date"
                                [order]="_order"
                                saImpression
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .SELF_SERVE_ORDERS}"
                                [saObjectValue]="_order.id"
                                [saContext]="_order.statusNew"
                            ></supr-order-history-container>
                        </div>

                        <div class="listContent">
                            <div class="orderDetails">
                                <supr-dropdown
                                    [dropDownOptions]="_order.complaint"
                                    classSelector="issueDropdown"
                                    [isModal]="false"
                                    [disabled]="disabled"
                                    *ngIf="_order.complaint?.length > 0"
                                    saImpression
                                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                        .SELF_SERVE_COMPLAINTS}"
                                    [saObjectValue]="_order.id"
                                ></supr-dropdown>

                                <div class="suprRow quantitySelection">
                                    <div class="suprColumn left">
                                        <supr-text>{{
                                            quantityHeader
                                        }}</supr-text>
                                    </div>
                                    <div class="suprColumn right">
                                        <supr-number-counter
                                            [quantity]="_order.quantity"
                                            [thresholdQty]="-1"
                                            [plusDisabled]="disabled"
                                            [minusDisabled]="disabled"
                                            [class.disabled]="disabled"
                                            saImpression
                                            saObjectName="${SA_OBJECT_NAMES
                                                .IMPRESSION
                                                .SELF_SERVE_QUANTITY}"
                                            [saObjectValue]="_order.id"
                                        >
                                        </supr-number-counter>
                                    </div>
                                </div>

                                <div
                                    class="feedback"
                                    *ngIf="_order.feedbackParams.length > 0"
                                >
                                    <!-- <supr-text>{{
                                selfServeConfig?.selfServePage?.feedbackTitle
                            }}</supr-text> -->
                                    <supr-text>{{ feedbackTitle }}</supr-text>
                                    <div class="divider16"></div>
                                    <div class="buttonList">
                                        <supr-button
                                            *ngFor="
                                                let param of _order.feedbackParams
                                            "
                                            [class.selected]="true"
                                            [class.disabled]="disabled"
                                            saClick
                                            saImpression
                                            saObjectName="${SA_OBJECT_NAMES
                                                .IMPRESSION
                                                .SELF_SERVE_FEEDBACK_PARAMS}"
                                            [saObjectValue]="param"
                                            >{{ param }}</supr-button
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </supr-list-item>
                </div>

                <div *ngIf="store.complaintDetails?.service?.length > 0">
                    <supr-list-item
                        *ngFor="
                            let issue of store.complaintDetails?.service;
                            last as isLast
                        "
                        [disabled]="disabled"
                        [selected]="true"
                        [data]="issue"
                        saImpression
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                            .SELF_SERVE_SERVICE_ISSUES}"
                        [saObjectValue]="issue"
                    >
                        <div class="listItem">
                            <supr-text [class.disabled]="disabled">{{
                                issue
                            }}</supr-text>
                        </div>
                    </supr-list-item>
                </div>

                <supr-list-item
                    [isExpand]="isExpand"
                    [selected]="true"
                    [disabled]="disabled"
                    *ngIf="store.complaintDetails?.uploadedImages?.length > 0"
                >
                    <div class="listItem">
                        <supr-text>{{ imageUpload }}</supr-text>
                    </div>

                    <div class="listContent">
                        <div class="upload">
                            <supr-image-upload
                                [imageArray]="
                                    store.complaintDetails?.uploadedImages
                                "
                                [disabled]="disabled"
                            ></supr-image-upload>
                        </div>
                    </div>
                </supr-list-item>

                <supr-list-item
                    [islast]="true"
                    [isExpand]="isExpand"
                    [selected]="true"
                    [disabled]="disabled"
                    *ngIf="store.complaintDetails?.notes"
                >
                    <div class="listItem">
                        <supr-text>{{ notes }}</supr-text>
                    </div>

                    <div class="listContent">
                        <textarea
                            [value]="store.complaintDetails?.notes"
                            rows="5"
                            type="text"
                            [attr.disabled]="disabled"
                            saImpression
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .SELF_SERVE_NOTES}"
                            [saObjectValue]="store.complaintDetails?.notes"
                        ></textarea>
                    </div>
                </supr-list-item>
            </supr-accordion>
            <supr-complaint-timeline
                [complaintTimeline]="
                    store.complaintDetails?.complaint_timeline?.timelineItems
                "
            ></supr-complaint-timeline>
        </div>
    `,
    styleUrls: ["../../styles/complaint-details.page.scss"],

    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintDetailsComponent {
    disabled = true;
    name: string;
    description: string;
    isExpand: boolean = true;

    quantityHeader = "Qty with issue";
    feedbackTitle = "What quality issue did you face";
    notes: "Additional Notes(Optional)";
    imageUpload: "Uploaded images";

    constructor(public store: Complaintv2Store) {}

    getComplaintDetails() {
        const { complaintDetails } = this.store;
        this.notes = "Additional Notes(Optional)";
        this.imageUpload = "Uploaded images";
        return complaintDetails;
    }
}
