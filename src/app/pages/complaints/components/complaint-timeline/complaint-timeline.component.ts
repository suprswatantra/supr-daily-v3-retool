import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SuprTimelineProps } from "@types";

@Component({
    selector: "supr-complaint-timeline",
    template: `
        <div class="complaintTimeline">
            <supr-accordion
                [title]="title"
                [subTitle]="subTitle"
                icon="ERROR"
                [disabled]="disabled"
            >
            </supr-accordion>
            <supr-timeline
                [isHeader]="isHeader"
                [timelineItems]="complaintTimeline"
            >
            </supr-timeline>
        </div>
    `,
    styleUrls: ["../../styles/complaint-details.page.scss"],

    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintTimelineComponent {
    disabled = false;
    isExpand: boolean = true;
    title: string = "Complaint status";
    subTitle: string = "Updates on the issues";
    isHeader: boolean = false;
    collapsed: boolean = false;

    @Input() complaintTimeline: SuprTimelineProps.LineItem[];

    constructor() {}
}
