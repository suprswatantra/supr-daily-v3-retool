import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { MobxAngularModule } from "mobx-angular";

import { SharedModule } from "@shared/shared.module";
import { ComplaintsRoutingModule } from "./complaints-routing.module";
import { ComplaintsContainer } from "./containers/complaints.container";
import { ComplaintsDetailsContainer } from "./containers/complaints-details.container";
import { ComplaintsLayoutComponent } from "./layouts/complaints.layout";
import { ComplaintsDetailsLayoutComponent } from "./layouts/complaints-details.layout";
import { complaintComponents } from "./components";

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        SharedModule,
        ComplaintsRoutingModule,
        MobxAngularModule,
    ],
    declarations: [
        ComplaintsContainer,
        ComplaintsDetailsContainer,
        ComplaintsLayoutComponent,
        ComplaintsDetailsLayoutComponent,
        ...complaintComponents,
    ],
    providers: [],
})
export class ComplaintsModule {}
