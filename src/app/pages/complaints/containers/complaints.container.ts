import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { User } from "@models";
import { UserAdapter as Adapter } from "@shared/adapters/user.adapter";
@Component({
    selector: "supr-complaints-container",
    template: `<supr-layout-complaints
        [user]="user$ | async"
    ></supr-layout-complaints>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintsContainer {
    user$: Observable<User>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.user$ = this.adapter.user$;
    }
}
