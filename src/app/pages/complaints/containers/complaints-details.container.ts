import { Component, ChangeDetectionStrategy } from "@angular/core";

import { UtilService } from "@services/util/util.service";
import { ActivatedRoute } from "@angular/router";
import { Complaintv2Store } from "@store/complaintv2";

@Component({
    selector: "supr-complaints-details-container",
    template: `<supr-complaints-details-layout></supr-complaints-details-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintsDetailsContainer {
    complaintId: number;
    constructor(
        private utilService: UtilService,
        private activatedRoute: ActivatedRoute,
        private store: Complaintv2Store
    ) {}

    ngOnInit() {
        this.complaintId = this.utilService.getNestedValue(
            this.activatedRoute,
            "snapshot.params.id"
        );
        this.fetchComplaintDetails();
    }

    private fetchComplaintDetails() {
        this.store.fetchComplaintDetails(this.complaintId);
    }
}
