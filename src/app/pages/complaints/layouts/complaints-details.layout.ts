import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-complaints-details-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">Issue status</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <!--<supr-complaint-details></supr-complaint-details>-->
            </div>
        </div>
    `,
    styleUrls: ["../styles/complaint-details.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintsDetailsLayoutComponent {
    constructor() {}
}
