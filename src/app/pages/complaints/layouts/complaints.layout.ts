import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { COMPLAINTS } from "@constants";
import { User } from "@models";
import { ComplaintSetting } from "@types";

import { SettingsService } from "@services/shared/settings.service";
import { DateService } from "@services/date/date.service";
import { Complaintv2Store } from "@store/complaintv2";

@Component({
    selector: "supr-layout-complaints",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">All Issues</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <div class="divider12"></div>

                <supr-complaints-archive
                    [user]="user"
                ></supr-complaints-archive>
            </div>
        </div>
    `,
    styleUrls: ["../styles/complaints.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComplaintsLayoutComponent implements OnInit {
    @Input() user: User;

    complaintConfig: ComplaintSetting;

    constructor(
        private complaintv2Store: Complaintv2Store,
        private settingsService: SettingsService,
        private dateService: DateService
    ) {}

    ngOnInit() {
        this.complaintConfig = this.fetchComplaintConfig();
        this.complaintv2Store.fetchComplaintsListV2(
            this.dateService.textFromDate(new Date()),
            this.complaintConfig.ARCHIVE_COMPLAINT_COUNT
        );
    }

    private fetchComplaintConfig() {
        const complaintConfig = this.settingsService.getSettingsValue(
            "complaintConfig",
            COMPLAINTS
        );
        return complaintConfig;
    }
}
