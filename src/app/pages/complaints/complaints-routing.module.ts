import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ComplaintsContainer } from "./containers/complaints.container";
import { ComplaintsDetailsContainer } from "./containers/complaints-details.container";
import { SCREEN_NAMES } from "@constants";

const complaintsRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "",
                component: ComplaintsContainer,
            },
            {
                path: "details/:id",
                component: ComplaintsDetailsContainer,
                data: {
                    screenName: SCREEN_NAMES.COMPLAINTS_DETAIL,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(complaintsRoutes)],
    exports: [RouterModule],
})
export class ComplaintsRoutingModule {}
