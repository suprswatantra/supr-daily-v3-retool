import {
    Input,
    OnInit,
    Output,
    Component,
    OnChanges,
    ViewChild,
    ElementRef,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    MODAL_NAMES,
    SA_IMPRESSIONS_IS_ENABLED,
    FEATURED_CARD_CONSTANTS,
} from "@constants";

import { Category } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";

import { Color } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "@pages/category/constants/analytics.constants";
import {
    HEADER_BG_COLOR_CSS_VARIABLE,
    HEADER_TITLE_COLOR_CSS_VARIABLE,
    HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
} from "@pages/category/constants/category.constants";

@Component({
    selector: "supr-category-layout",
    template: `
        <div class="suprContainer">
            <div class="header" #header>
                <supr-page-header>
                    <supr-search-box
                        (click)="goToSearchPage()"
                        saClick
                        saImpression
                        [saImpressionEnabled]="
                            ${SA_IMPRESSIONS_IS_ENABLED.SEARCH_BOX}
                        "
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .CATALOG_HEADER_SEARCH}"
                    ></supr-search-box>
                </supr-page-header>
            </div>
            <div
                class="suprScrollContent"
                id="categoryScrollContent"
                [class.noScroll]="!_category"
            >
                <supr-category-header
                    [category]="_category"
                    [hsl]="hslData"
                ></supr-category-header>

                <ng-container>
                    <div class="divider16"></div>
                    <div class="feature">
                        <supr-feature-card
                            componentName="${FEATURED_CARD_CONSTANTS.CATEGORY_DETAIL}"
                            [componentId]="categoryId"
                        ></supr-feature-card>
                    </div>
                </ng-container>

                <ng-container *ngIf="_category; else loader">
                    <div class="suprScrollContentWrapper">
                        <supr-category-product-group-list
                            [category]="_category"
                            [navigateId]="navigateId"
                            [skuNavigationId]="skuNavigationId"
                        ></supr-category-product-group-list>
                    </div>
                </ng-container>

                <ng-template #loader>
                    <supr-category-body-skeleton></supr-category-body-skeleton>
                </ng-template>
            </div>

            <ng-container *ngIf="_category">
                <supr-cart></supr-cart>
            </ng-container>

            <ng-container *ngIf="showModal">
                <supr-category-product-unavailable-modal
                    modalName="${MODAL_NAMES.CATEGORY_SKUS_UNAVAILABLE_MODAL}"
                >
                </supr-category-product-unavailable-modal>
            </ng-container>
        </div>
        <supr-loader-overlay *ngIf="updatingSkuStore"></supr-loader-overlay>
    `,
    styleUrls: ["../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryPageLayoutComponent implements OnInit, OnChanges {
    @Input() category: Category;
    @Input() categoryId: number;
    @Input() navigateId: number;
    @Input() skuNavigationId: number;
    @Input() updatingSkuStore: boolean;
    @Output() fetchCategory: EventEmitter<number> = new EventEmitter();

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    hslData: Color.HSL;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService,
        private appsFlyerService: AppsFlyerService
    ) {}

    showModal = false;
    _category = null;

    ngOnInit() {
        if (!this.category) {
            this.fetchCategory.emit(this.categoryId);
        } else {
            this.initData();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["category"];
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initData();
        }
    }

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }

    private initData() {
        if (!this.category) {
            return;
        }

        if (!this.checkIfListEmpty(this.category)) {
            this.openModal();
        }

        this._category = this.category;

        this.convertColorToHSL();
        this.setBgColor();

        this.setTitleColor();
        this.setItemCountColor();

        this.sendAnalyticsEvents();
    }

    private convertColorToHSL() {
        this.hslData = this.utilService.hexToHSL(this.category.image.bgColor);
    }

    private setBgColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 95%)`
        );
    }

    private setTitleColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_TITLE_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 40%)`
        );
    }

    private setItemCountColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 70%)`
        );
    }

    private sendAnalyticsEvents() {
        this.appsFlyerService.trackCatalogListView(
            this.categoryId,
            this.category.name
        );

        // this.moengageService.trackProductListView(
        //     this.categoryId,
        //     this.category.name
        // );
    }

    private checkIfListEmpty(category: Category): boolean {
        return (
            !this.utilService.isEmpty(category.default) ||
            !this.utilService.isEmpty(category.filtered)
        );
    }

    private openModal() {
        this.showModal = true;
    }
}
