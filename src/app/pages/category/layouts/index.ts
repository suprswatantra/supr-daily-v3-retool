import { CategoryPageLayoutComponent } from "./category.layout";
import { SubCategoryPageLayoutComponent } from "./subcategory.layout";
import { CollectionPageLayoutComponent } from "./collection.layout";

export const categoryLayouts = [
    CategoryPageLayoutComponent,
    SubCategoryPageLayoutComponent,
    CollectionPageLayoutComponent,
];
