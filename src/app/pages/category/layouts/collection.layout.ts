import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { MODAL_NAMES, SA_IMPRESSIONS_IS_ENABLED } from "@constants";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

import {
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_TEXTS,
} from "@pages/category/constants/analytics.constants";
import { HEADER_BG_COLOR_CSS_VARIABLE } from "@pages/category/constants/category.constants";
import { CollectionDetail } from "@shared/models";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-layout",
    template: `
        <div class="suprContainer">
            <div class="header" #header>
                <supr-page-header>
                    <supr-search-box
                        (click)="goToSearchPage()"
                        saClick
                        saImpression
                        [saImpressionEnabled]="
                            ${SA_IMPRESSIONS_IS_ENABLED.SEARCH_BOX}
                        "
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .CATALOG_HEADER_SEARCH}"
                    ></supr-search-box>
                </supr-page-header>
            </div>
            <div class="suprScrollContent" [class.noScroll]="!collection">
                <supr-collection-details-header
                    [collection]="collection"
                ></supr-collection-details-header>

                <ng-container *ngIf="collection; else loader">
                    <div class="suprScrollContentWrapper">
                        <supr-collection-product-group-list
                            [collection]="collection"
                            [saContextList]="saContextList"
                        ></supr-collection-product-group-list>
                    </div>
                </ng-container>

                <ng-template #loader>
                    <supr-category-body-skeleton></supr-category-body-skeleton>
                </ng-template>
            </div>

            <ng-container *ngIf="collection">
                <supr-cart></supr-cart>
            </ng-container>

            <ng-container *ngIf="showModal">
                <supr-category-product-unavailable-modal
                    modalName="${MODAL_NAMES.COLLECTION_SKUS_UNAVAILABLE_MODAL}"
                ></supr-category-product-unavailable-modal>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageLayoutComponent implements OnInit, OnChanges {
    @Input() collection: CollectionDetail;
    @Input() viewId: number;

    @Output() handleFetchCollection: EventEmitter<number> = new EventEmitter();

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    showModal = false;
    saContextList: Segment.ContextListItem[] = [];

    ngOnInit() {
        if (!this.collection) {
            this.handleFetchCollection.emit(this.viewId);
        } else {
            this.initData();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["collection"];
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initData();
        }
    }

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }

    private initData() {
        if (!this.collection) {
            return;
        }

        if (!this.checkIfListEmpty(this.collection)) {
            this.openModal();
        }

        this.setBgColor();
        this.setAnalyticsData();
    }

    private setBgColor() {
        if (!this.collection.background) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            this.collection.background.color
        );
    }

    private checkIfListEmpty(collection: CollectionDetail): boolean {
        return (
            !this.utilService.isEmpty(collection.default) ||
            !this.utilService.isEmpty(collection.filtered) ||
            !this.utilService.isEmpty(collection.skuIdList)
        );
    }

    private openModal() {
        this.showModal = true;
    }

    private setAnalyticsData() {
        this.saContextList = [
            {
                name: ANALYTICS_TEXTS.COLLECTION,
                value: this.collection && this.collection.viewId,
            },
        ];
    }
}
