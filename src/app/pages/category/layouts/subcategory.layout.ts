import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { Category, SubCategory } from "@models";

import {
    HEADER_BG_COLOR_CSS_VARIABLE,
    HEADER_TITLE_COLOR_CSS_VARIABLE,
    HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
} from "@pages/category/constants/category.constants";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

import { Color } from "@types";

@Component({
    selector: "supr-sub-category-layout",
    template: `
        <div class="suprContainer">
            <div class="header" #header>
                <supr-page-header>
                    <supr-search-box
                        (click)="goToSearchPage()"
                    ></supr-search-box>
                </supr-page-header>
            </div>

            <div class="suprScrollContent" *ngIf="subCategory">
                <supr-category-header
                    [category]="subCategory"
                    [parentCategoryName]="category?.name"
                    [hsl]="hslData"
                ></supr-category-header>
                <div class="suprScrollContentWrapper">
                    <div class="groupItemList">
                        <supr-category-product-item-list
                            [skuIdList]="subCategory?.skuIdList"
                            [saContext]="category?.id"
                        ></supr-category-product-item-list>
                    </div>
                </div>
            </div>
            <supr-cart></supr-cart>

            <ng-container *ngIf="showModal">
                <supr-category-product-unavailable-modal
                    modalName="${MODAL_NAMES.SUB_CATEGORY_SKUS_UNAVAILABLE_MODAL}"
                ></supr-category-product-unavailable-modal>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubCategoryPageLayoutComponent implements OnInit {
    @Input() category: Category;
    @Input() subCategoryId: number;

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    hslData: Color.HSL;
    subCategory: SubCategory;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    showModal = false;

    ngOnInit() {
        if (!this.category || !this.category.filtered) {
            return;
        }

        this.subCategory = this.category.filtered.find(
            subCat => subCat.id === this.subCategoryId
        );

        this.initData();
    }

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }

    private initData() {
        if (!this.category) {
            return;
        }

        if (!this.checkIfListEmpty(this.category)) {
            this.openModal();
        }

        this.convertColorToHSL();
        this.setBgColor();

        this.setTitleColor();
        this.setItemCountColor();
    }

    private convertColorToHSL() {
        this.hslData = this.utilService.hexToHSL(this.category.image.bgColor);
    }

    private setBgColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 95%)`
        );
    }

    private setTitleColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_TITLE_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 40%)`
        );
    }

    private setItemCountColor() {
        const { h, s } = this.hslData;
        this.headerEl.nativeElement.style.setProperty(
            HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 70%)`
        );
    }

    private checkIfListEmpty(category: Category): boolean {
        return (
            !this.utilService.isEmpty(category.default) ||
            !this.utilService.isEmpty(category.filtered)
        );
    }

    private openModal() {
        this.showModal = true;
    }
}
