import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Category } from "@models";
import { CategoryPageAdapter as Adapter } from "../services/category.adapter";

@Component({
    selector: "supr-category-container",
    template: `
        <supr-category-layout
            [category]="category$ | async"
            [categoryId]="categoryId$ | async"
            [navigateId]="navigateId$ | async"
            [updatingSkuStore]="skuLoadingState$ | async"
            [skuNavigationId]="skuNavigationId$ | async"
            (fetchCategory)="fetchCategoryDetails($event)"
        ></supr-category-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryPageContainer implements OnInit {
    category$: Observable<Category>;
    categoryId$: Observable<number>;
    navigateId$: Observable<number>;
    skuNavigationId$: Observable<number>;
    skuLoadingState$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.category$ = this.adapter.category$;
        this.categoryId$ = this.adapter.urlParams$.pipe(
            map((params: Params) => +params["categoryId"])
        );
        this.navigateId$ = this.adapter.queryParams$.pipe(
            map((params: Params) => +params["filterId"])
        );
        this.skuLoadingState$ = this.adapter.skuLoadingState$;
        this.skuNavigationId$ = this.adapter.queryParams$.pipe(
            map((params: Params) => +params["skuId"])
        );
    }

    fetchCategoryDetails(categoryId: number) {
        this.adapter.fetchCategoryDetails(categoryId);
    }
}
