import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Category } from "@models";
import { CategoryPageAdapter as Adapter } from "@pages/category/services/category.adapter";

@Component({
    selector: "supr-subcategory-container",
    template: `
        <supr-sub-category-layout
            [category]="category$ | async"
            [subCategoryId]="subCategoryId$ | async"
        ></supr-sub-category-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubCategoryPageContainer implements OnInit {
    category$: Observable<Category>;
    subCategoryId$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.category$ = this.adapter.category$;
        this.subCategoryId$ = this.adapter.urlParams$.pipe(
            map((params: Params) => +params["filterId"])
        );
    }
}
