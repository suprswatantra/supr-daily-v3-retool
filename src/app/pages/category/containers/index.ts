import { CategoryPageContainer } from "./category.container";
import { SubCategoryPageContainer } from "./subcategory.container";
import { EssentialsPageContainer } from "./essentials.container";
import { CollectionPageContainer } from "./collection.container";
import { ItemContainer } from "./item.container";

export const categoryContainers = [
    CategoryPageContainer,
    EssentialsPageContainer,
    SubCategoryPageContainer,
    CollectionPageContainer,
    ItemContainer,
];
