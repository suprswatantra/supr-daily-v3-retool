import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Category } from "@models";
import { CategoryPageAdapter as Adapter } from "@pages/category/services/category.adapter";

@Component({
    selector: "supr-category-container",
    template: `
        <supr-category-layout
            [category]="category$ | async"
            [categoryId]="categoryId$ | async"
            [navigateId]="navigateId$ | async"
        ></supr-category-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EssentialsPageContainer implements OnInit {
    category$: Observable<Category>;
    categoryId$: Observable<number>;
    navigateId$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.category$ = this.adapter.essentialCategory$;
        this.categoryId$ = this.adapter.urlParams$.pipe(
            map((params: Params) => +params["categoryId"])
        );
        this.navigateId$ = this.adapter.queryParams$.pipe(
            map((params: Params) => +params["filterId"])
        );
    }
}
