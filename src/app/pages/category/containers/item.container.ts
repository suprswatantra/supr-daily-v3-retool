import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { CartAdapter } from "@shared/adapters/cart.adapter";

import { Sku, CartItem, SkuAttributes } from "@models";

import { Segment } from "@types";

import { CategoryPageAdapter as Adapter } from "@pages/category/services/category.adapter";

@Component({
    selector: "supr-category-item-container",
    template: `
        <supr-product-tile
            [skuMeta]="skuMeta"
            [sku]="sku$ | async"
            [navigateId]="navigateId"
            [cartItem]="cartItem$ | async"
            [skuNavigationId]="skuNavigationId"
            [skuNotified]="skuNotified$ | async"
            [skuAttributes]="skuAttributes$ | async"
            [alternatesV2Experiment]="alternatesV2Experiment"
            [saContext]="saContext"
            [saContextList]="saContextList"
        ></supr-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemContainer implements OnInit {
    @Input() skuMeta: Sku;
    @Input() skuId: number;
    @Input() navigateId: number;
    @Input() skuNavigationId: number;
    @Input() alternatesV2Experiment = true;

    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];

    sku$: Observable<Sku>;
    cartItem$: Observable<CartItem>;
    skuNotified$: Observable<string>;
    skuAttributes$: Observable<SkuAttributes>;

    constructor(
        private adapter: Adapter,
        private skuAdapter: SkuAdapter,
        private cartAdapter: CartAdapter
    ) {}

    ngOnInit() {
        this.sku$ = this.adapter.getSkuById(this.skuId);
        this.skuNotified$ = this.skuAdapter.isNotified(this.skuId);
        this.cartItem$ = this.cartAdapter.getCartItemById(this.skuId);
        this.skuAttributes$ = this.skuAdapter.getSkuAttributes(this.skuId);
    }
}
