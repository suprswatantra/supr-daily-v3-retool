import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { CollectionDetail } from "@models";
import { CategoryPageAdapter as Adapter } from "../services/category.adapter";

@Component({
    selector: "supr-collection-container",
    template: `
        <supr-collection-layout
            [collection]="collection$ | async"
            [viewId]="viewId$ | async"
            (handleFetchCollection)="fetchCollectionDetails($event)"
        ></supr-collection-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionPageContainer implements OnInit {
    collection$: Observable<CollectionDetail>;
    viewId$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.collection$ = this.adapter.collection$;
        this.viewId$ = this.adapter.urlParams$.pipe(
            map((params: Params) => +params["viewId"])
        );
    }

    fetchCollectionDetails(viewId: number) {
        this.adapter.fetchCollectionDetails(viewId);
    }
}
