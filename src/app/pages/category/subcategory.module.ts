import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { CategoryCommonModule } from "./common.module";

import { SubCategoryPageContainer } from "./containers/subcategory.container";

const routes: Routes = [
    {
        path: "",
        component: SubCategoryPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        SharedModule,
        RouterModule.forChild(routes),
        CategoryCommonModule,
    ],
    declarations: [SubCategoryPageContainer],
})
export class SubCategoryPageModule {}
