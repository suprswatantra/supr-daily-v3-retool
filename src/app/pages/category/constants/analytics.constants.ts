export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        SUGGESTIONS: "suggestions",
        SUBCATEGORY_OPEN: "subcategory_open",
        SUBCATEGORY_CLOSE: "subcategory_close",
        CATALOG_HEADER_SEARCH: "search",
    },
    IMPRESSION: {
        SUBCATEGORY: "subcategory",
    },
};

export const ANALYTICS_TEXTS = {
    COLLECTION: "collection",
};
