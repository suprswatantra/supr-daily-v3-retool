export const HEADER_BG_COLOR_CSS_VARIABLE = "--header-bg-color";
export const HEADER_TITLE_COLOR_CSS_VARIABLE = "--header-title-color";
export const HEADER_SUB_TITLE_COLOR_CSS_VARIABLE = "--header-sub-title-color";
