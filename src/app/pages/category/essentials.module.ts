import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { CategoryCommonModule } from "./common.module";

import { EssentialsPageContainer } from "./containers/essentials.container";

const routes: Routes = [
    {
        path: "",
        component: EssentialsPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        SharedModule,
        RouterModule.forChild(routes),
        CategoryCommonModule,
    ],
    declarations: [EssentialsPageContainer],
})
export class CategoryEssentialsPageModule {}
