import { CategoryPageAdapter } from "./category.adapter";
import { CategoryPageService } from "./category.service";

export const categoryServices = [CategoryPageService, CategoryPageAdapter];
