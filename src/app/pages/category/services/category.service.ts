import { Injectable } from "@angular/core";

import { SubCategory } from "@models";

@Injectable()
export class CategoryPageService {
    getCategoryTotalSkuCount(list: SubCategory[]): number {
        return list.reduce((acc: number, subCategory: any) => {
            acc += subCategory.skuIdList.length;
            return acc;
        }, 0);
    }
}
