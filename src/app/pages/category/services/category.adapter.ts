import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Sku } from "@models";
import {
    StoreState,
    CategoryStoreActions,
    CollectionStoreActions,
    CategoryStoreSelectors,
    RouterStoreSelectors,
    SkuStoreSelectors,
    EssentialsStoreSelectors,
    CollectionStoreSelectors,
} from "@supr/store";

@Injectable()
export class CategoryPageAdapter {
    category$ = this.store.pipe(
        select(CategoryStoreSelectors.selectCategoryDetailsFromUrlParams)
    );

    skuLoadingState$ = this.store.pipe(
        select(SkuStoreSelectors.selectSkuLoading)
    );

    essentialCategory$ = this.store.pipe(
        select(EssentialsStoreSelectors.selectEssentialDetailsById)
    );

    collection$ = this.store.pipe(
        select(CollectionStoreSelectors.selectCollectionDetailsById)
    );

    urlParams$ = this.store.pipe(select(RouterStoreSelectors.selectUrlParams));

    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    constructor(private store: Store<StoreState>) {}

    fetchCategoryDetails(categoryId: number) {
        this.store.dispatch(
            new CategoryStoreActions.FetchCategoryDetailsRequestAction({
                categoryId,
            })
        );
    }

    fetchCollectionDetails(viewId: number) {
        this.store.dispatch(
            new CollectionStoreActions.FetchCollectionDetailsRequestAction({
                viewId,
            })
        );
    }

    getSkuById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, { skuId })
        );
    }
}
