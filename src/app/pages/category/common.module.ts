import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { categoryComponents } from "./components";
import { ItemContainer } from "./containers/item.container";
import { categoryLayouts } from "./layouts";
import { categoryServices } from "./services";

@NgModule({
    imports: [CommonModule, IonicModule, SharedModule],
    declarations: [ItemContainer, ...categoryComponents, ...categoryLayouts],
    exports: [ItemContainer, ...categoryComponents, ...categoryLayouts],
    providers: [...categoryServices],
})
export class CategoryCommonModule {}
