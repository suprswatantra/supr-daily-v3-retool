import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { Color } from "@types";

import { HEADER_BG_COLOR_CSS_VARIABLE } from "@pages/category/constants/category.constants";

@Component({
    selector: "supr-category-oos-banner",
    template: `
        <div class="oosBanner" #oosBanner>
            <supr-banner [banner]="category?.oosBanner?.banner"></supr-banner>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryOosBannerComponent implements OnInit {
    @Input() category: any;
    @Input() hsl: Color.HSL;

    @ViewChild("oosBanner", { static: true }) bannerEl: ElementRef;

    ngOnInit() {
        if (this.hsl) {
            this.setBgColor();
        }
    }

    private setBgColor() {
        const { h, s } = this.hsl;
        if (h && s) {
            this.bannerEl.nativeElement.style.setProperty(
                HEADER_BG_COLOR_CSS_VARIABLE,
                `hsl(${h}, ${s}%, 95%)`
            );
        }
    }
}
