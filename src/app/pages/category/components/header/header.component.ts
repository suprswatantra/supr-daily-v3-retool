import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { Category } from "@models";

import { Color } from "@types";

@Component({
    selector: "supr-category-header",
    template: `
        <div class="header">
            <ng-container *ngIf="!category; else info">
                <supr-category-header-skeleton></supr-category-header-skeleton>
            </ng-container>

            <ng-template #info>
                <ng-container *ngIf="category?.oosBanner?.isEnabled">
                    <supr-category-oos-banner
                        [category]="category"
                        [hsl]="hsl"
                    ></supr-category-oos-banner>
                </ng-container>
                <supr-category-info
                    [category]="category"
                    [parentCategoryName]="parentCategoryName"
                    [hsl]="hsl"
                ></supr-category-info>
                <supr-category-filter-grid
                    [category]="category"
                    [hsl]="hsl"
                ></supr-category-filter-grid>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    @Input() category: Category;
    @Input() parentCategoryName: string;
    @Input() hsl: Color.HSL;
}
