import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { Color } from "@types";

import { CategoryPageService } from "@pages/category/services/category.service";
import { HEADER_BG_COLOR_CSS_VARIABLE } from "@pages/category/constants/category.constants";

@Component({
    selector: "supr-category-info",
    template: `
        <div class="info" #info>
            <div class="divider16"></div>
            <div class="suprRow">
                <div
                    class="infoImage"
                    [class.suprHide]="!category?.image?.fullUrl"
                >
                    <supr-image
                        [src]="category?.image?.fullUrl"
                        [image]="category?.image"
                        [withWrapper]="false"
                    ></supr-image>
                </div>
                <div class="spacer16"></div>
                <div class="suprColumn left center">
                    <supr-text class="title" type="heading">
                        {{ category?.name }}
                    </supr-text>
                    <ng-container *ngIf="parentCategoryName">
                        <supr-text class="subTitle" type="caption">
                            in {{ parentCategoryName }}
                        </supr-text>
                    </ng-container>
                    <supr-text class="itemCount" type="caption">
                        {{ totalSkuCount }} items
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent implements OnInit {
    @Input() category: any;
    @Input() parentCategoryName: string;
    @Input() hsl: Color.HSL;

    @ViewChild("info", { static: true }) infoEl: ElementRef;

    totalSkuCount: number;

    constructor(private categoryService: CategoryPageService) {}

    ngOnInit() {
        this.caculateItemCount();
        this.setBgColor();
    }

    private caculateItemCount() {
        if (this.parentCategoryName) {
            this.totalSkuCount = this.category.skuIdList.length;
        } else {
            this.totalSkuCount = this.categoryService.getCategoryTotalSkuCount(
                this.category.default
            );
        }
    }

    private setBgColor() {
        const { h, s } = this.hsl;
        this.infoEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 95%)`
        );
    }
}
