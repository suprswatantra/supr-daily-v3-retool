import { CategoryOosBannerComponent } from "./header/oos-banner.component";
import { HeaderComponent } from "./header/header.component";

import { FilterGridComponent } from "./filter/grid.component";
import { FilterItemComponent } from "./filter/item.component";

import { InfoComponent } from "./info/info.component";

import { ProductGroupListComponent } from "./products/group-list.component";
import { ProductGroupComponent } from "./products/group.component";
import { ProductItemListComponent } from "./products/item-list.component";
import { ProductGroupHeaderComponent } from "./products/group-header.component";

import {
    HeaderSkeletonComponent,
    HeaderFilterSkeletonComponent,
} from "./loaders/header-skeleton.component";
import { BodySkeletonComponent } from "./loaders/body-skeleton.component";

import { CollectionHeaderComponent } from "./collection/header.component";
import { CollectionInfoComponent } from "./collection/info.component";
import { CollectionProductGroupListComponent } from "./collection/products-group-list.component";
import { ProductNotAvailableContent } from "./product_not_available/content.component";
import { ProductNotAvailableModal } from "./product_not_available/modal.component";

export const categoryComponents = [
    FilterGridComponent,
    FilterItemComponent,
    HeaderComponent,
    InfoComponent,
    ProductGroupListComponent,
    ProductGroupComponent,
    ProductItemListComponent,
    ProductGroupHeaderComponent,
    HeaderSkeletonComponent,
    HeaderFilterSkeletonComponent,
    BodySkeletonComponent,
    CollectionHeaderComponent,
    CollectionInfoComponent,
    CollectionProductGroupListComponent,
    ProductNotAvailableModal,
    ProductNotAvailableContent,
    CategoryOosBannerComponent,
];
