import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-category-product-unavailable-content",
    template: `
        <div class="modal">
            <supr-text type="subtitle">
                {{ texts?.title }}
            </supr-text>

            <div class="divider16"></div>
            <supr-text type="paragraph" class="paragraph">
                {{ texts?.subtitle1 }}
            </supr-text>

            <div class="divider16"></div>

            <supr-text type="paragraph" class="paragraph">
                {{ texts?.subtitle2 }}
            </supr-text>

            <div class="divider32"></div>

            <supr-button (handleClick)="handleGoBack.emit()">
                {{ texts?.btnText }}
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductNotAvailableContent implements OnInit {
    @Output() handleGoBack: EventEmitter<void> = new EventEmitter();

    constructor(
        private ms: ModalService,
        private settingsService: SettingsService
    ) {}

    texts: any;

    ngOnInit() {
        this.setTexts();
    }

    _closeModal() {
        this.ms.closeModal();
    }

    private setTexts() {
        this.texts = this.settingsService.getSettingsValue(
            "skusProductNotAvailableTexts",
            SETTINGS_KEYS_DEFAULT_VALUE.skusProductNotAvailableTexts
        );
    }
}
