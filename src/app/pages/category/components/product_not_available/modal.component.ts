import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-category-product-unavailable-modal",
    template: `
        <supr-modal
            *ngIf="showModal"
            [animate]="modalAnimate"
            [backdropDismiss]="backdropDismiss"
            (handleClose)="_closeModal()"
            [modalName]="modalName"
        >
            <supr-category-product-unavailable-content
                (handleGoBack)="goBack()"
            ></supr-category-product-unavailable-content>
        </supr-modal>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductNotAvailableModal {
    @Input() modalName: string;

    constructor(private routerService: RouterService) {}

    modalAnimate = true;
    backdropDismiss = false;
    showModal = true;

    _closeModal() {
        this.showModal = false;
    }

    goBack() {
        this.routerService.goBack();
    }
}
