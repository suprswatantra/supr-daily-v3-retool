import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { CollectionDetail } from "@models";
import { UtilService } from "@services/util/util.service";

import { CategoryPageService } from "@pages/category/services/category.service";
import {
    HEADER_BG_COLOR_CSS_VARIABLE,
    HEADER_TITLE_COLOR_CSS_VARIABLE,
    HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
} from "@pages/category/constants/category.constants";

@Component({
    selector: "supr-collection-info",
    template: `
        <div class="info" #info>
            <div class="divider16"></div>
            <div class="suprRow">
                <div class="infoImage" *ngIf="collection?.image?.fullUrl">
                    <supr-image
                        [src]="collection?.image?.fullUrl"
                        [image]="collection?.image"
                        [withWrapper]="false"
                    ></supr-image>
                </div>
                <div class="spacer16"></div>
                <div class="suprColumn left center">
                    <supr-text class="title" type="subtitle">
                        {{ collection?.title?.text }}
                    </supr-text>
                    <ng-container *ngIf="collection?.subtitle?.text">
                        <div class="divider4"></div>
                        <supr-text class="subTitle" type="regular">
                            {{ collection?.subtitle?.text }}
                        </supr-text>
                    </ng-container>

                    <div class="divider8"></div>
                    <supr-text class="itemCount" type="regular">
                        {{ totalSkuCount }} items
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionInfoComponent implements OnInit {
    @Input() collection: CollectionDetail;

    @ViewChild("info", { static: true }) infoEl: ElementRef;

    totalSkuCount: number;

    constructor(
        private categoryService: CategoryPageService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.caculateItemCount();
        this.setBgColor();
        this.setTitleColor();
        this.setSubtitleColor();
    }

    private caculateItemCount() {
        if (!this.utilService.isEmpty(this.collection.default)) {
            this.totalSkuCount = this.categoryService.getCategoryTotalSkuCount(
                this.collection.default
            );
        } else {
            this.totalSkuCount = this.collection.skuIdList
                ? this.collection.skuIdList.length
                : 0;
        }
    }

    private setBgColor() {
        if (!this.collection.background) {
            return;
        }

        this.infoEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            this.collection.background.color
        );
    }

    private setTitleColor() {
        if (!this.collection.title) {
            return;
        }

        this.infoEl.nativeElement.style.setProperty(
            HEADER_TITLE_COLOR_CSS_VARIABLE,
            this.collection.title.color
        );
    }

    private setSubtitleColor() {
        if (!this.collection.subtitle) {
            return;
        }

        this.infoEl.nativeElement.style.setProperty(
            HEADER_SUB_TITLE_COLOR_CSS_VARIABLE,
            this.collection.subtitle.color
        );
    }
}
