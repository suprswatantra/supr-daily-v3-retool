import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { CollectionDetail } from "@models";

@Component({
    selector: "supr-collection-details-header",
    template: `
        <div class="header">
            <ng-container *ngIf="!collection; else info">
                <supr-category-header-skeleton></supr-category-header-skeleton>
            </ng-container>

            <ng-template #info>
                <supr-collection-info
                    [collection]="collection"
                ></supr-collection-info>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionHeaderComponent {
    @Input() collection: CollectionDetail;
}
