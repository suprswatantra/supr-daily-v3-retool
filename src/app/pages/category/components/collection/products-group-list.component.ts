import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { ANALYTICS_OBJECT_NAMES } from "@pages/category/constants/analytics.constants";

import { CollectionDetail } from "@models";
import { SkuDict, Segment } from "@types";

@Component({
    selector: "supr-collection-product-group-list",
    template: `
        <ng-container *ngIf="isNested; else linear">
            <ng-container
                *ngFor="
                    let subCategory of collection?.default;
                    trackBy: trackByFn;
                    last as isLast;
                    index as position
                "
            >
                <supr-category-product-group
                    id="group-{{ subCategory.id }}"
                    productGroupId="group-{{ subCategory.id }}"
                    [subCategory]="subCategory"
                    [skuDict]="skuDict"
                    [isLast]="isLast"
                    saImpression
                    [saImpressionEnabled]="
                        ${SA_IMPRESSIONS_IS_ENABLED.CATEGORY_PRODUCT_GROUP}
                    "
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .SUBCATEGORY}"
                    [saObjectValue]="subCategory.id"
                    [saContext]="collection?.viewId"
                    [saContextList]="saContextList"
                    [saPosition]="position + 1"
                >
                </supr-category-product-group>
            </ng-container>
        </ng-container>

        <ng-template #linear>
            <supr-category-product-item-list
                [skuIdList]="collection?.skuIdList"
                [skuDict]="skuDict"
                [saContext]="collection?.viewId"
                [saContextList]="saContextList"
            ></supr-category-product-item-list>
        </ng-template>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionProductGroupListComponent implements OnInit {
    @Input() collection: CollectionDetail;
    @Input() saContextList: Segment.ContextListItem[] = [];

    isNested: boolean;
    skuDict: SkuDict = {};

    ngOnInit() {
        this.setType();
        this.setupSkuDict();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private setType() {
        const { skuIdList } = this.collection;
        const _default = this.collection.default;

        this.isNested = !!_default && _default.length && !skuIdList.length;
    }

    private setupSkuDict() {
        if (!this.collection || !this.collection.skuList) {
            return;
        }

        this.skuDict = this.collection.skuList.reduce((dict, item) => {
            dict[item.id] = item;
            return dict;
        }, {});
    }
}
