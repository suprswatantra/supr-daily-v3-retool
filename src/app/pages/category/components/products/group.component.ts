import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SubCategory } from "@models";
import { SkuDict, Segment } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "@pages/category/constants/analytics.constants";

@Component({
    selector: "supr-category-product-group",
    template: `
        <div class="groupWrapper" [class.last]="isLast">
            <supr-category-product-group-header
                [subCategory]="subCategory"
                [showList]="_showList"
                (click)="toggleList()"
                saClick
                [saObjectName]="
                    _showList
                        ? saClickObjectNames.SUBCATEGORY_CLOSE
                        : saClickObjectNames.SUBCATEGORY_OPEN
                "
                [saObjectValue]="productGroupId"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            >
            </supr-category-product-group-header>

            <div class="groupItemList" *ngIf="_showList">
                <supr-category-product-item-list
                    [skuNavigationId]="skuNavigationId"
                    [navigateId]="navigateId"
                    [skuIdList]="subCategory?.skuIdList"
                    [skuDict]="skuDict"
                    [saContext]="saContext"
                    [saContextList]="saContextList"
                ></supr-category-product-item-list>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductGroupComponent implements OnInit {
    @Input() categoryId: number;
    @Input() categoryName: string;
    @Input() subCategory: SubCategory;
    @Input() skuDict: SkuDict = {};
    @Input() showList = false;
    @Input() isLast: boolean;
    @Input() productGroupId: string;
    @Input() saContext: number;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;
    @Input() skuNavigationId: number;
    @Input() navigateId: number;

    _showList: boolean;
    saClickObjectNames = ANALYTICS_OBJECT_NAMES.CLICK;

    constructor() {}

    ngOnInit() {
        this._showList = this.showList;
        if (this._showList) {
            this.sendAnalyticsEvents();
        }
    }

    toggleList() {
        this._showList = !this._showList;
        if (this._showList) {
            this.sendAnalyticsEvents();
        }
    }

    private sendAnalyticsEvents() {
        // this.moengageService.trackProductListView(
        //     this.categoryId,
        //     this.categoryName,
        //     this.subCategory.id,
        //     this.subCategory.name,
        //     (this.subCategory && this.subCategory.skuIdList) || []
        // );
    }
}
