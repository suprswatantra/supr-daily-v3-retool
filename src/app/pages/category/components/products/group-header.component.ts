import { Component, Input, ChangeDetectionStrategy } from "@angular/core";
import { SubCategory } from "@models";

@Component({
    selector: "supr-category-product-group-header",
    template: `
        <div class="groupHeader">
            <div class="divider16"></div>
            <div class="suprRow">
                <supr-text type="subheading">{{ categoryName }}</supr-text>
                <supr-icon
                    [class.up]="showList"
                    name="chevron_down"
                ></supr-icon>
            </div>
            <supr-text class="caption" type="caption">
                {{ itemQtyStr }}
            </supr-text>
            <div class="divider16"></div>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductGroupHeaderComponent {
    @Input()
    set subCategory(cat: SubCategory) {
        this.init(cat);
    }

    @Input() showList: boolean;

    iconName = "chevron_down";
    itemQtyStr: string;
    categoryName: string;

    private init(cat: SubCategory) {
        try {
            const len = cat.skuIdList.length;
            this.itemQtyStr = `${len} item${len > 1 ? "s" : ""}`;

            this.categoryName = cat.name;
        } catch (e) {
            /** */
        }
    }
}
