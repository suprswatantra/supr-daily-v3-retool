import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SettingsService } from "@services/shared/settings.service";

import { SkuDict, Segment } from "@types";

@Component({
    selector: "supr-category-product-item-list",
    template: `
        <div class="groupItemListWrapper">
            <ng-container
                *ngFor="
                    let skuId of skuIdList;
                    trackBy: trackByFn;
                    last as _lastItem
                "
            >
                <supr-category-item-container
                    id="sku-{{ skuId }}"
                    [navigateId]="navigateId"
                    [skuNavigationId]="skuNavigationId"
                    [skuId]="skuId"
                    [skuMeta]="skuDict[skuId]"
                    [alternatesV2Experiment]="_alternatesV2Experiment"
                    [saContext]="saContext"
                    [saContextList]="saContextList"
                ></supr-category-item-container>
                <ng-container *ngIf="_alternatesV2Experiment">
                    <supr-oos-v5-alternates-container
                        [skuId]="skuId"
                        id="sku-alternates-{{ skuId }}"
                    ></supr-oos-v5-alternates-container>
                </ng-container>
                <ng-container *ngIf="!_lastItem">
                    <div class="divider32"></div>
                </ng-container>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductItemListComponent implements OnInit {
    @Input() saContext: any;
    @Input() skuIdList: number[];
    @Input() skuDict: SkuDict = {};
    @Input() saContextList: Segment.ContextListItem[];
    @Input() skuNavigationId: number;
    @Input() navigateId: number;

    _alternatesV2Experiment = true;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this._alternatesV2Experiment = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
