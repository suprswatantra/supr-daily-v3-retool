import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { SubCategory } from "@models";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-category-product-group-list",
    template: `
        <ng-container
            *ngFor="
                let subCategory of category?.default;
                trackBy: trackByFn;
                last as isLast;
                index as position
            "
        >
            <supr-category-product-group
                id="group-{{ subCategory.id }}"
                productGroupId="group-{{ subCategory.id }}"
                [categoryId]="category?.id"
                [categoryName]="category?.name"
                [subCategory]="subCategory"
                [showList]="canShowList(subCategory, position)"
                [skuNavigationId]="skuNavigationId"
                [navigateId]="navigateId"
                [isLast]="isLast"
                saImpression
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.SUBCATEGORY}"
                [saObjectValue]="subCategory.id"
                [saContext]="category?.id"
                [saPosition]="position + 1"
            >
            </supr-category-product-group>
        </ng-container>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductGroupListComponent implements OnInit {
    @Input() category: any;
    @Input() navigateId: number;
    @Input() skuNavigationId: number;

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        if (this.navigateId && !this.skuNavigationId) {
            this.utilService.scrollTo(this.navigateId, "catNavigateElem");
        }
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    canShowList(subCategory: SubCategory, position: number): boolean {
        if (
            position === 0 &&
            !this.settingsService.getSettingsValue(
                SETTINGS.HIDE_FIRST_SUBCATEGORY,
                SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.HIDE_FIRST_SUBCATEGORY]
            )
        ) {
            return true;
        }
        return (
            (this.navigateId && subCategory.id === this.navigateId) ||
            (this.category &&
                this.category.default &&
                this.category.default.length === 1)
        );
    }
}
