import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
    SimpleChanges,
    OnChanges,
    SimpleChange,
} from "@angular/core";

import { Category, SubCategory } from "@models";

import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { RouterService } from "@services/util/router.service";

import { Color } from "@types";

import {
    HEADER_TITLE_COLOR_CSS_VARIABLE,
    HEADER_BG_COLOR_CSS_VARIABLE,
} from "@pages/category/constants/category.constants";

import { ANALYTICS_OBJECT_NAMES } from "@pages/category/constants/analytics.constants";

@Component({
    selector: "supr-category-filter-grid",
    template: `
        <div class="filter" #filter>
            <ng-container *ngIf="category?.filtered?.length">
                <supr-text type="subheading">Suggestions</supr-text>
                <div class="divider8"></div>

                <div class="filterSlider suprRow top">
                    <ng-container
                        *ngFor="
                            let subCategory of category?.filtered;
                            let position = index;
                            trackBy: trackByFn
                        "
                    >
                        <supr-category-filter-item
                            [subCategory]="subCategory"
                            (click)="goToFilterPage(subCategory)"
                            saClick
                            saImpression
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .SUGGESTIONS}"
                            [saObjectValue]="subCategory?.id"
                            [saContext]="category?.id"
                            [saPosition]="position + 1"
                        ></supr-category-filter-item>
                    </ng-container>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterGridComponent implements OnInit, OnChanges {
    @Input() category: Category;
    @Input() hsl: Color.HSL;

    @ViewChild("filter", { static: true }) filterEl: ElementRef;

    constructor(
        private routerService: RouterService,
        private appsFlyerService: AppsFlyerService
    ) {}

    ngOnInit() {
        this.setBgColor();
        this.setTitleColor();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleHslChange(changes["hsl"]);
    }

    trackByFn(category: SubCategory): number {
        return category.id;
    }

    goToFilterPage(filterCategory: SubCategory) {
        this.sendAppsFlyerEvent(filterCategory);

        this.routerService.goToSubCategoryPage(
            this.category.id,
            filterCategory.id
        );
    }

    private handleHslChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setBgColor();
            this.setTitleColor();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private setBgColor() {
        const { h, s } = this.hsl;
        this.filterEl.nativeElement.style.setProperty(
            HEADER_BG_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 95%)`
        );
    }

    private setTitleColor() {
        const { h, s } = this.hsl;
        this.filterEl.nativeElement.style.setProperty(
            HEADER_TITLE_COLOR_CSS_VARIABLE,
            `hsl(${h}, ${s}%, 40%)`
        );
    }

    private sendAppsFlyerEvent(filterCategory: SubCategory) {
        const { id, name } = filterCategory;

        this.appsFlyerService.trackCatalogListView(id, name);
        // this.moengageService.trackProductListView(
        //     this.category.id,
        //     this.category.name,
        //     id,
        //     name
        // );
    }
}
