import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { SubCategory } from "@models";

@Component({
    selector: "supr-category-filter-item",
    template: `
        <div class="filterItem suprColumn">
            <div class="filterItemImage">
                <supr-image
                    [src]="subCategory?.image?.fullUrl"
                    [image]="subCategory?.image"
                ></supr-image>
            </div>
            <div class="divider4"></div>
            <supr-text type="regular14">{{ subCategory?.name }}</supr-text>
        </div>
    `,
    styleUrls: ["../../styles/category.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterItemComponent {
    @Input() subCategory: SubCategory;
}
