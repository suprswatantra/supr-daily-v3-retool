import { Component, ChangeDetectionStrategy } from "@angular/core";

const COUNT = 5;

@Component({
    selector: "supr-category-body-skeleton",
    template: `
        <div class="body">
            <div
                class="suprColumn left bodyItem"
                *ngFor="let number of numbers; trackBy: trackByFn"
            >
                <div class="divider16"></div>

                <ion-skeleton-text class="title" animated></ion-skeleton-text>
                <ion-skeleton-text class="subtext" animated></ion-skeleton-text>
                <div class="divider16"></div>
            </div>
        </div>
    `,
    styleUrls: ["./../../styles/skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BodySkeletonComponent {
    numbers = [];

    constructor() {
        this.numbers = Array(COUNT).fill("");
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
