import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-category-header-filter-skeleton",
    template: `
        <div class="headerFilterTile suprColumn">
            <ion-skeleton-text animated class="image"></ion-skeleton-text>
            <div class="divider4"></div>
            <ion-skeleton-text animated class="title"></ion-skeleton-text>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderFilterSkeletonComponent {}

@Component({
    selector: "supr-category-header-skeleton",
    template: `
        <div class="header">
            <div class="suprRow headerTitle">
                <ion-skeleton-text animated class="round"></ion-skeleton-text>

                <div class="spacer8"></div>

                <div class="suprColumn left">
                    <ion-skeleton-text
                        animated
                        class="first"
                    ></ion-skeleton-text>
                    <ion-skeleton-text
                        animated
                        class="second"
                    ></ion-skeleton-text>
                </div>
            </div>
            <div class="headerFilter">
                <ion-skeleton-text
                    class="headerFilterTitle"
                    animated
                ></ion-skeleton-text>
                <div class="divider8"></div>
                <div class="suprRow headerFilterRow">
                    <ng-container *ngFor="let tile of tiles">
                        <supr-category-header-filter-skeleton></supr-category-header-filter-skeleton>
                    </ng-container>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./../../styles/skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderSkeletonComponent {
    tiles = [0, 1, 2, 3, 4, 5, 6, 7];
}
