import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { ScratchCard, ScratchedCardDetails, TextFragment } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { ConfigurableIcon } from "@types";

import { RewardsService } from "../services/rewards.service";

import { TEXTS, DEFAULT_SCRATCH_CARD_URL } from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-rewards-scratch-card-details-layout",
    template: `
        <div class="suprContainer">
            <div class="suprScrollContent scratchCardContainer">
                <div class="suprColumn center">
                    <div
                        class="suprColumn center scratchCardWrapper"
                        [class.celebration]="
                            isCelebration && !isFetchingScratchCardBenefit
                        "
                    >
                        <div class="cardWrapper center">
                            <ng-container>
                                <div class="suprRow center">
                                    <supr-scratch-card
                                        [imageUrl]="getScratchCardImgUrl()"
                                        [icon]="errorIcon"
                                        [textFragment]="scratchCardTextFragment"
                                        [isFetchingScratchCardBenefit]="
                                            isFetchingScratchCardBenefit
                                        "
                                        (handleFetchScratchCardBenefit)="
                                            fetchScratchCardBenefit()
                                        "
                                    ></supr-scratch-card>
                                </div>
                            </ng-container>

                            <div class="divider32"></div>
                            <div class="infoAndAction">
                                <ng-container
                                    *ngIf="actionBtnText; else infoText"
                                >
                                    <div class="suprRow center actionBtn">
                                        <supr-button
                                            (handleClick)="onActionBtnClick()"
                                            [disabled]="
                                                isFetchingScratchCardBenefit
                                            "
                                            [class.hasError]="errorIcon"
                                            [saObjectName]="
                                                actionButtonSaObjectName
                                            "
                                        >
                                            <div class="suprRow">
                                                <supr-text type="body">
                                                    {{ actionBtnText }}
                                                </supr-text>
                                                <supr-icon
                                                    name="chevron_right"
                                                ></supr-icon>
                                            </div>
                                        </supr-button>
                                    </div>
                                </ng-container>
                                <ng-template #infoText>
                                    <ng-container
                                        *ngIf="!isFetchingScratchCardBenefit"
                                    >
                                        <div class="infoTexts">
                                            <div class="suprRow center">
                                                <supr-text type="bold16">
                                                    ${TEXTS.REDEEM_REWARDS_HEADER}
                                                </supr-text>
                                            </div>
                                            <div class="divider4"></div>
                                            <div class="suprRow">
                                                <supr-text type="paragraph">
                                                    ${TEXTS.REDEEM_REWARDS_INFO}
                                                </supr-text>
                                            </div>
                                        </div>
                                    </ng-container>
                                </ng-template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsScratchCardDetailsLayoutComponent
    implements OnInit, OnChanges {
    @Input() scratchCard: ScratchCard;
    @Input() scratchedCardDetails: ScratchedCardDetails;
    @Input() isFetchingScratchCardBenefit: boolean;

    @Output()
    handleFetchScratchCardBenefit: EventEmitter<number> = new EventEmitter();

    constructor(
        private utilService: UtilService,
        private routerService: RouterService,
        private rewardsService: RewardsService
    ) {}

    defaultImageUrl = DEFAULT_SCRATCH_CARD_URL;
    scratchCardTextFragment: TextFragment;
    actionBtnText: string;
    errorIcon: ConfigurableIcon;
    isCelebration: boolean;
    actionButtonSaObjectName: string;

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change =
            changes["scratchCard"] || changes["scratchedCardDetails"];

        if (this.canHandleChange(change)) {
            this.initialize();
        }
    }

    fetchScratchCardBenefit() {
        const id = this.utilService.getNestedValue(
            this.scratchCard,
            "id",
            null
        );

        if (!id) {
            return;
        }

        this.handleFetchScratchCardBenefit.emit(id);
    }

    getScratchCardImgUrl() {
        const id = this.utilService.getNestedValue(this.scratchCard, "id", 1);

        return this.rewardsService.getScratchCardImgUrl(id);
    }

    private initialize() {
        this.resetAttributes();

        this.scratchCardTextFragment = this.rewardsService.getScratchCardTextFragment(
            this.scratchedCardDetails
        );
        this.errorIcon = this.rewardsService.getErrorIcon(
            this.scratchedCardDetails
        );
        this.actionBtnText = this.rewardsService.getActionBtnText(
            this.scratchedCardDetails
        );
        this.isCelebration = this.rewardsService.getIsCelebration(
            this.scratchedCardDetails
        );

        this.setActionButtonSaObjectName();
    }

    resetAttributes() {
        this.scratchCardTextFragment = null;
        this.actionBtnText = "";
        this.errorIcon = null;
        this.isCelebration = false;
    }

    setActionButtonSaObjectName() {
        if (!this.actionBtnText) {
            return;
        }

        switch (this.actionBtnText) {
            case TEXTS.OK:
                this.actionButtonSaObjectName = SA_OBJECT_NAMES.CLICK.OK;
                break;
            case TEXTS.TRY_AGAIN:
                this.actionButtonSaObjectName = SA_OBJECT_NAMES.CLICK.TRY_AGAIN;
                break;
            case TEXTS.SEE_ALL_REWARDS:
                this.actionButtonSaObjectName =
                    SA_OBJECT_NAMES.CLICK.VIEW_REWARDS;
                break;

            default:
                this.actionButtonSaObjectName =
                    SA_OBJECT_NAMES.CLICK.VIEW_REWARDS;
                break;
        }
    }

    onActionBtnClick() {
        if (!this.actionBtnText) {
            return;
        }

        switch (this.actionBtnText) {
            case TEXTS.OK:
                this.routerService.goBack();
                break;
            case TEXTS.TRY_AGAIN:
                this.fetchScratchCardBenefit();
                break;
            case TEXTS.SEE_ALL_REWARDS:
                this.routerService.goToRewardsPage();
                break;

            default:
                break;
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
