import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ActivityBlock } from "@models";

import { TEXTS } from "../constants";

@Component({
    selector: "supr-rewards-activity-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.ACTIVITY_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-rewards-activity
                    [rewardsActivity]="activity"
                    [showFullList]="true"
                ></supr-rewards-activity>
            </div>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsActivityLayoutComponent {
    @Input() activity: ActivityBlock[];
}
