import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { TEXTS } from "../constants";

import { FaqItem } from "@models";

@Component({
    selector: "supr-rewards-faq-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.FAQ_SECTION_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-rewards-faq
                    [rewardsFaq]="faqs"
                    [showFullList]="true"
                    [hasHeader]="true"
                ></supr-rewards-faq>
            </div>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsFaqLayoutComponent {
    @Input() faqs: FaqItem[];
}
