import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import {
    RewardsInfo,
    BenefitsBannerCtaAction,
    ScratchCard,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { RewardsService } from "../services/rewards.service";
import {
    TEXTS,
    ACTIVITIES_TO_SHOW,
    DEFAULT_SCRATCH_CARD_URL,
} from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-rewards-layout",
    template: `
        <div class="suprContainer">
            <!-- Top Bar -->

            <supr-rewards-top-bar></supr-rewards-top-bar>
            <div class="suprScrollContent rewardsInfoWrapper">
                <ng-container *ngIf="isFetchingRewardsInfo; else content">
                    <div class="loaderContainer suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-container>
                <ng-template #content>
                    <ng-container *ngIf="rewardsInfo; else error">
                        <!-- Header -->
                        <ng-container *ngIf="rewardsInfo?.header">
                            <supr-rewards-header
                                [headerData]="rewardsInfo?.header"
                            ></supr-rewards-header>
                        </ng-container>

                        <!-- Scratch card horizonal list -->
                        <ng-container *ngIf="rewardsInfo?.scratchCards.length">
                            <div class="divider28"></div>
                            <div
                                class="suprRow"
                                saImpression
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .SCRATCH_CARDS}"
                            >
                                <supr-text
                                    type="subheading"
                                    class="sectionTitleLight"
                                >
                                    ${TEXTS.CLAIM_REWARDS}
                                </supr-text>

                                <ng-container *ngIf="scratchCradSeeAllText">
                                    <div
                                        class="suprRow sectionLink"
                                        (click)="goToScratchCardsPage()"
                                        saClick
                                        saObjectName="${SA_OBJECT_NAMES.CLICK
                                            .SEE_ALL_SCRATCH_CARDS}"
                                    >
                                        <supr-text type="paragraph">
                                            {{ scratchCradSeeAllText }}
                                        </supr-text>
                                        <supr-icon
                                            name="chevron_right"
                                        ></supr-icon>
                                    </div>
                                </ng-container>
                            </div>

                            <div class="scratchCardSlider">
                                <supr-horizontal>
                                    <div
                                        class="suprRow container center horizontalImageSlider"
                                    >
                                        <ng-container
                                            *ngFor="
                                                let scratchCard of rewardsInfo?.scratchCards
                                            "
                                        >
                                            <div
                                                class="scratchCardSliderTile"
                                                (click)="
                                                    goToScratchCardsDetailsPage(
                                                        scratchCard?.id
                                                    )
                                                "
                                            >
                                                <img
                                                    [src]="
                                                        getScratchCardImgUrl(
                                                            scratchCard
                                                        )
                                                    "
                                                />
                                            </div>
                                        </ng-container>
                                    </div>
                                </supr-horizontal>
                            </div>
                        </ng-container>

                        <!-- Benefits -->
                        <ng-container *ngIf="rewardsInfo?.benefits">
                            <div class="divider28"></div>
                            <supr-benefits-banner
                                [benefitsBanner]="rewardsInfo?.benefits"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick($event)
                                "
                            ></supr-benefits-banner>
                        </ng-container>

                        <!-- Activity -->
                        <ng-container *ngIf="rewardsInfo?.pastRewards">
                            <div class="divider28"></div>
                            <div class="suprRow">
                                <supr-text
                                    type="subheading"
                                    class="sectionTitleLight"
                                >
                                    ${TEXTS.ACTIVITY_TITLE}
                                </supr-text>

                                <div
                                    class="suprRow sectionLink"
                                    (click)="goToActivityPage()"
                                    saClick
                                    saObjectName="${SA_OBJECT_NAMES.CLICK
                                        .SEE_ALL_PAST_REWARDS}"
                                >
                                    <supr-text type="paragraph">
                                        ${TEXTS.SEE_ALL_PAST_REWARDS}
                                    </supr-text>
                                    <supr-icon name="chevron_right"></supr-icon>
                                </div>
                            </div>

                            <div class="divider12"></div>
                            <supr-rewards-activity
                                [rewardsActivity]="rewardsInfo?.pastRewards"
                                activityCount="${ACTIVITIES_TO_SHOW}"
                            ></supr-rewards-activity>
                        </ng-container>

                        <!-- FAQs -->
                        <ng-container *ngIf="rewardsInfo?.faqs">
                            <div class="divider28"></div>
                            <supr-rewards-faq
                                [rewardsFaq]="rewardsInfo?.faqs"
                            ></supr-rewards-faq>
                        </ng-container>
                    </ng-container>

                    <!-- Error -->
                    <ng-template #error></ng-template>
                </ng-template>
            </div>

            <!-- Footer -->
            <ng-container *ngIf="!isFetchingRewardsInfo && rewardsInfo?.footer">
                <supr-rewards-footer
                    [footerData]="rewardsInfo?.footer"
                    (handleButtonClick)="handleFooterButtonClick()"
                ></supr-rewards-footer>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardsLayout implements OnInit, OnChanges {
    @Input() rewardsInfo: RewardsInfo;
    @Input() isFetchingRewardsInfo: boolean;

    @Output() handleFetchRewardsInfo: EventEmitter<void> = new EventEmitter();

    hasError = false;
    defaultScratchCardUrl = DEFAULT_SCRATCH_CARD_URL;
    scratchCradSeeAllText: string;

    constructor(
        private utilService: UtilService,
        private rewardsService: RewardsService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        if (this.utilService.isEmpty(this.rewardsInfo)) {
            this.handleFetchRewardsInfo.emit();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["rewardsInfo"];
        if (this.canHandleChange(change)) {
            this.initialize();
        }
    }

    handleBenefitCtaClick(ctaAction: BenefitsBannerCtaAction) {
        this.rewardsService.handleBenefitCtaClick(ctaAction);
    }

    handleFaqClick() {
        this.rewardsService.handleFaqClick(this.rewardsInfo);
    }

    goToActivityPage() {
        this.routerService.goToRewardsActivityPage();
    }

    goToScratchCardsPage() {
        this.routerService.goToRewardsScratchCardsPage();
    }

    goToScratchCardsDetailsPage(id: number) {
        if (!id) {
            return;
        }

        this.routerService.goToRewardsScratchCardDetailsPage(id);
    }

    handleFooterButtonClick() {
        this.rewardsService.handleFooterButtonClick(this.rewardsInfo);
    }

    getScratchCardImgUrl(scratchCard: ScratchCard) {
        const id = this.utilService.getNestedValue(scratchCard, "id", 1);

        return this.rewardsService.getScratchCardImgUrl(id);
    }

    private initialize() {
        this.scratchCradSeeAllText = this.rewardsService.getScratchCardSeeAllText(
            this.rewardsInfo
        );
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
