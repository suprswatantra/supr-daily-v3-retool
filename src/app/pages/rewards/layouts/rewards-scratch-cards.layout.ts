import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ScratchCard } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { RewardsService } from "../services/rewards.service";
import { TEXTS, DEFAULT_SCRATCH_CARD_URL } from "../constants";

@Component({
    selector: "supr-rewards-scratch-cards-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.CLAIM_REWARDS}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent scratchCardsContainer">
                <ng-container *ngIf="isFetchingRewardsInfo; else content">
                    <div class="loaderContainer suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-container>
                <ng-template #content>
                    <ng-container
                        *ngIf="
                            scratchCards && scratchCards.length;
                            else noCardsLeft
                        "
                    >
                        <ng-container *ngFor="let scratchCard of scratchCards">
                            <div class="divider12"></div>
                            <div
                                class="suprRow scractchCardImgWrapper center"
                                (click)="
                                    goToScratchCardsDetailsPage(scratchCard?.id)
                                "
                            >
                                <img
                                    [src]="getScratchCardImgUrl(scratchCard)"
                                />
                            </div>
                            <div class="divider12"></div>
                        </ng-container>
                    </ng-container>
                    <ng-template #noCardsLeft>
                        <div class="suprColumn center allRewardsClaimed">
                            <supr-svg class="allRewardsClaimedImage"></supr-svg>
                            <div class="divider20"></div>
                            <supr-text
                                type="subtitle"
                                class="allRewardsClaimedText"
                                >${TEXTS.ALL_REWARDS_CLAIMED}</supr-text
                            >
                        </div>
                    </ng-template>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsScratchCardsLayoutComponent {
    @Input() scratchCards: ScratchCard[];
    @Input() isFetchingRewardsInfo: boolean;

    defaultScratchCardImgUrl = DEFAULT_SCRATCH_CARD_URL;

    constructor(
        private routerService: RouterService,
        private rewardsService: RewardsService,
        private utilService: UtilService
    ) {}

    goToScratchCardsDetailsPage(id: number) {
        if (!id) {
            return;
        }

        this.routerService.goToRewardsScratchCardDetailsPage(id);
    }

    getScratchCardImgUrl(scratchCard: ScratchCard) {
        const id = this.utilService.getNestedValue(scratchCard, "id", 1);

        return this.rewardsService.getScratchCardImgUrl(id);
    }
}
