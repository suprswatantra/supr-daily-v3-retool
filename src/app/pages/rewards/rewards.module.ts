import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SCREEN_NAMES } from "@constants";

import { RewardsLayout } from "./layouts/rewards.layout";
import { SuprRewardsFaqLayoutComponent } from "./layouts/rewards-faq.layout";
import { SuprRewardsActivityLayoutComponent } from "./layouts/rewards-activity.layout";
import { RewardsComponents } from "./components";
import { RewardsContainer } from "./containers/rewards.container";
import { SuprRewardsFaqPageContainer } from "./containers/rewards-faq.container";
import { SuprRewardsActivityContainer } from "./containers/rewards-activity.container";
import { SuprRewardsScratchCardsContainer } from "./containers/rewards-scratch-cards.container";
import { SuprRewardsScratchCardDetailsContainer } from "./containers/rewards-scratch-card-details.container";
import { SuprRewardsScratchCardsLayoutComponent } from "./layouts/rewards-scratch-cards.layout";
import { SuprRewardsScratchCardDetailsLayoutComponent } from "./layouts/rewards-scratch-card-details.layout";

import { RewardsService } from "./services/rewards.service";

import { RewardsAdapter } from "./services/rewards.adapter";

const routes: Routes = [
    {
        path: "",
        component: RewardsContainer,
    },
    {
        path: "faq",
        component: SuprRewardsFaqPageContainer,
        data: { screenName: SCREEN_NAMES.REWARDS_FAQ },
    },
    {
        path: "activity",
        component: SuprRewardsActivityContainer,
        data: { screenName: SCREEN_NAMES.REWARDS_ACTIVITY },
    },
    {
        path: "scratch-cards",
        component: SuprRewardsScratchCardsContainer,
        data: { screenName: SCREEN_NAMES.REWARDS_SCRATCH_CARDS },
    },
    {
        path: "scratch-card/details/:scratchCardId",
        component: SuprRewardsScratchCardDetailsContainer,
        data: { screenName: SCREEN_NAMES.REWARDS_SCRATCH_CARD },
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...RewardsComponents,
        RewardsContainer,
        SuprRewardsFaqPageContainer,
        SuprRewardsActivityContainer,
        SuprRewardsScratchCardsContainer,
        SuprRewardsScratchCardDetailsContainer,
        RewardsLayout,
        SuprRewardsFaqLayoutComponent,
        SuprRewardsActivityLayoutComponent,
        SuprRewardsScratchCardsLayoutComponent,
        SuprRewardsScratchCardDetailsLayoutComponent,
    ],
    providers: [RewardsAdapter, RewardsService],
})
export class RewardsModule {}
