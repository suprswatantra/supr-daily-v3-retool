import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { RewardsInfo } from "@shared/models";

import { RewardsAdapter as Adapter } from "../services/rewards.adapter";

@Component({
    selector: "supr-rewards-container",
    template: `
        <supr-rewards-layout
            [rewardsInfo]="rewardsInfo$ | async"
            [isFetchingRewardsInfo]="isFetchingRewardsInfo$ | async"
            (handleFetchRewardsInfo)="handleFetchRewardsInfo()"
        ></supr-rewards-layout>
    `,
})
export class RewardsContainer implements OnInit {
    rewardsInfo$: Observable<RewardsInfo>;
    isFetchingRewardsInfo$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.rewardsInfo$ = this.adapter.rewardsInfo$;
        this.isFetchingRewardsInfo$ = this.adapter.isFetchingRewardsInfo$;
    }

    handleFetchRewardsInfo() {
        this.adapter.fetchRewardsInfo();
    }
}
