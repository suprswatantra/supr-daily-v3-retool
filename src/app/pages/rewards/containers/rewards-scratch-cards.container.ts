import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ScratchCard } from "@shared/models";

import { RewardsAdapter as Adapter } from "../services/rewards.adapter";

@Component({
    selector: "supr-rewards-scratch-cards-container",
    template: `
        <supr-rewards-scratch-cards-layout
            [scratchCards]="scratchCards$ | async"
            [isFetchingRewardsInfo]="isFetchingRewardsInfo$ | async"
        ></supr-rewards-scratch-cards-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsScratchCardsContainer implements OnInit {
    scratchCards$: Observable<ScratchCard[]>;
    isFetchingRewardsInfo$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.scratchCards$ = this.adapter.scratchCards$;
        this.isFetchingRewardsInfo$ = this.adapter.isFetchingRewardsInfo$;
    }
}
