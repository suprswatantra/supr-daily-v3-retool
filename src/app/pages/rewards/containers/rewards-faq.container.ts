import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { FaqItem } from "@shared/models";

import { RewardsAdapter as Adapter } from "../services/rewards.adapter";

@Component({
    selector: "supr-rewards-faq-container",
    template: `
        <supr-rewards-faq-layout
            [faqs]="rewardsFaqs$ | async"
        ></supr-rewards-faq-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsFaqPageContainer implements OnInit {
    rewardsFaqs$: Observable<FaqItem[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.rewardsFaqs$ = this.adapter.rewardsFaqs$;
    }
}
