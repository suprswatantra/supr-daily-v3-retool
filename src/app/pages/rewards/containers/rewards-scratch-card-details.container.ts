import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ScratchCard, ScratchedCardDetails } from "@shared/models";

import { RewardsAdapter as Adapter } from "../services/rewards.adapter";

@Component({
    selector: "supr-rewards-scratch-card-details-container",
    template: `
        <supr-rewards-scratch-card-details-layout
            [scratchCard]="scratchCard$ | async"
            [scratchedCardDetails]="scratchedCardDetails$ | async"
            [isFetchingScratchCardBenefit]="
                isFetchingScratchCardBenefit$ | async
            "
            (handleFetchRewardsInfo)="fetchRewardsInfo()"
            (handleFetchScratchCardBenefit)="fetchScratchCardBenefit($event)"
        ></supr-rewards-scratch-card-details-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsScratchCardDetailsContainer implements OnInit {
    scratchCard$: Observable<ScratchCard>;
    scratchedCardDetails$: Observable<ScratchedCardDetails>;
    isFetchingScratchCardBenefit$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.scratchCard$ = this.adapter.scratchCard$;
        this.isFetchingScratchCardBenefit$ = this.adapter.isFetchingScratchCardBenefit$;
    }

    fetchRewardsInfo() {
        this.adapter.fetchRewardsInfo();
    }

    fetchScratchCardBenefit(id: number) {
        this.adapter.fetchScratchCardBenefit(id);
        this.scratchedCardDetails$ = this.adapter.getScratchedCardDetailsById(
            id
        );
    }
}
