import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ActivityBlock } from "@shared/models";

import { RewardsAdapter as Adapter } from "../services/rewards.adapter";

@Component({
    selector: "supr-rewards-activity-container",
    template: `
        <supr-rewards-activity-layout
            [activity]="rewardsActivity$ | async"
        ></supr-rewards-activity-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprRewardsActivityContainer implements OnInit {
    rewardsActivity$: Observable<ActivityBlock[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.rewardsActivity$ = this.adapter.rewardsActivity$;
    }
}
