export const SA_OBJECT_NAMES = {
    CLICK: {
        FOOTER_BTN_CLICK: "rewards-footer-btn",
        SEE_ALL_PAST_REWARDS: "see-all-past-rewards",
        SEE_ALL_SCRATCH_CARDS: "see-all-scratch-cards",
        VIEW_ALL_FAQS: "view-all-faqs",
        VIEW_REWARDS: "see-all-rewards",
        TRY_AGAIN: "try-again",
        OK: "ok",
    },
    IMPRESSION: {
        SCRATCH_CARDS: "supr-scratch-cards-block",
    },
};
