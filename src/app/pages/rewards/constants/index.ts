export const TEXTS = {
    HEADER_TEXT: "Rewards",
    FAQ_SECTION_TITLE: "Frequently asked questions",
    VIEW_ALL_FAQS: "VIEW ALL FAQS",
    ACTIVITY_TITLE: "Past Rewards",
    CREDITS: "credits",
    EXPIRING: "expiring on",
    AVAILABLE: "Available",
    REDEEM: "REDEEM NOW",
    SEE_ALL_PAST_REWARDS: "SEE ALL PAST REWARDS",
    CLAIM_REWARDS: "Claim rewards",
    SEE_ALL_TEXT: "SEE ALL",
    REDEEM_REWARDS_HEADER: "To redeem rewards",
    REDEEM_REWARDS_INFO:
        "Scratch the card above to reveal the prize and follow instructions to redeem",
    TRY_AGAIN: "TRY AGAIN",
    OK: "OK GOT IT",
    SEE_ALL_REWARDS: "SEE ALL REWARDS",
    ALL_REWARDS_CLAIMED: "Yay, you have claimed all the rewards!",
};

export const DEFAULT_IMG_URL = "assets/images/app/supr-rewards.svg";
export const DEFAULT_SCRATCH_CARD_URL =
    "./assets/images/app/supr-scratch-card.png";

export const SCRATCH_CARD_URLS = [
    "./assets/images/scratch-card/scratch-card-1.png",
    "./assets/images/scratch-card/scratch-card-2.png",
    "./assets/images/scratch-card/scratch-card-3.png",
    "./assets/images/scratch-card/scratch-card-4.png",
    "./assets/images/scratch-card/scratch-card-5.png",
    "./assets/images/scratch-card/scratch-card-6.png",
];

export const SCRATCH_CARD_ERROR_ICON = {
    iconName: "error_triangle",
    iconSize: "20px",
    iconColor: "#BC321F",
};

export const SCRATCH_CARD_ERROR_GENERIC = {
    text: "Oops! something went wrong",
    textColor: "#BC321F",
    fontSize: "20px",
};

export const SCRATCH_CARD_ERROR_EXPIRY = {
    text: "This scratch card expired",
    textColor: "#BC321F",
    fontSize: "20px",
};

export const CELEBRATION_IMG_URL = "assets/images/app/supr-celebration.svg";

export const ACTIVITIES_TO_SHOW = 3;

export const FAQ_ITEMS_TO_SHOW = 2;

export const ACTIVITY_PAGE_URL = "/rewards/activity";

export const STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-rewards-header-bg",
    },
    {
        attributeName: "warningText.textColor",
        attributeStyleVariableName: "--supr-rewards-warning-icon-color",
    },
    {
        attributeName: "warningText.fontSize",
        attributeStyleVariableName: "--supr-rewards-warning-icon-font-size",
    },
];

export const FOOTER_BTN_ICON = {
    ENABLED: "chevron_right",
    DISABLED: "lock",
};

export const SUPR_CREDITS_ICON_CONFIG = {
    iconType: "svg",
    iconName: "supr-credits-img",
    iconSize: "12px",
};
