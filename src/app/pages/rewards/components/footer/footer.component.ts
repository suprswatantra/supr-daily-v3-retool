import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { RewardsInfoFooter } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import {
    TEXTS,
    FOOTER_BTN_ICON,
    SUPR_CREDITS_ICON_CONFIG,
} from "@pages/rewards/constants";

import { SA_OBJECT_NAMES } from "@pages/rewards/constants/analytics.constants";

@Component({
    selector: "supr-rewards-footer",
    template: `
        <supr-page-footer-v2
            [buttonIcon]="btnIconName"
            [buttonTitle]="btnUpperFoldText"
            [buttonTitleIcon]="
                footerData?.scAvailable ? suprCreditsIconConfig : ''
            "
            [buttonSubtitle]="btnLowerFoldText"
            saObjectName="${SA_OBJECT_NAMES.CLICK.FOOTER_BTN_CLICK}"
            (handleButtonClick)="handleButtonClick.emit()"
        >
            <div class="suprColumn left leftText">
                <ng-container
                    *ngIf="footerData?.scExpiry?.amount; else defaultTextBlock"
                >
                    <div class="suprRow">
                        <supr-text type="paragraph">
                            {{ footerData?.scExpiry?.amount }}
                        </supr-text>
                        <supr-configurable-icon
                            [config]="suprCreditsIconConfig"
                        ></supr-configurable-icon>
                    </div>
                    <div class="suprRow">
                        <supr-text type="paragraph">
                            ${TEXTS.EXPIRING}
                        </supr-text>
                    </div>
                    <div class="suprRow">
                        <supr-text type="paragraph">
                            {{
                                footerData?.scExpiry?.date | date: "d LLL, yyyy"
                            }}
                        </supr-text>
                    </div>
                </ng-container>
                <ng-template #defaultTextBlock>
                    <div class="suprRow">
                        <supr-text type="paragraph" class="disabled">
                            {{ footerData?.leftSectionDefaultText }}
                        </supr-text>
                    </div>
                </ng-template>
            </div>
        </supr-page-footer-v2>
    `,
    styleUrls: ["../../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
    @Input() footerData: RewardsInfoFooter;

    @Output() handleButtonClick: EventEmitter<void> = new EventEmitter();

    btnIconName = FOOTER_BTN_ICON.ENABLED;
    btnUpperFoldText: string;
    btnLowerFoldText: string;
    suprCreditsIconConfig = SUPR_CREDITS_ICON_CONFIG;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    private initialize() {
        const availableSc = this.utilService.getNestedValue(
            this.footerData,
            "scAvailable",
            0
        );

        if (!availableSc) {
            this.btnUpperFoldText = this.utilService.getNestedValue(
                this.footerData,
                "buttonUpperFoldDefaultText",
                null
            );

            this.btnLowerFoldText = this.utilService.getNestedValue(
                this.footerData,
                "buttonLowerFoldDefaultText",
                null
            );

            return;
        }

        this.btnUpperFoldText = `${TEXTS.AVAILABLE} ${availableSc}`;
        this.btnLowerFoldText = TEXTS.REDEEM;
    }
}
