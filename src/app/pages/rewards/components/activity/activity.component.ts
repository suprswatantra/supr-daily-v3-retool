import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ActivityBlock } from "@shared/models";

@Component({
    selector: "supr-rewards-activity",
    template: `
        <div
            class="activityBlock"
            *ngIf="rewardsActivity && rewardsActivity.length"
            [class.activityBlockPadding]="showFullList"
        >
            <ng-container *ngIf="activityCount; else fullActivityList">
                <div
                    *ngFor="
                        let activity of rewardsActivity;
                        trackBy: trackByFn;
                        index as activityIndex
                    "
                >
                    <ng-container *ngIf="activityIndex < activityCount">
                        <supr-activity-block
                            [activityItem]="activity"
                        ></supr-activity-block>
                        <div
                            class="divider16"
                            *ngIf="activityIndex < activityCount - 1"
                        ></div>
                    </ng-container>
                </div>
            </ng-container>
            <ng-template #fullActivityList>
                <div
                    *ngFor="let activity of rewardsActivity; trackBy: trackByFn"
                >
                    <supr-activity-block
                        [activityItem]="activity"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityComponent {
    @Input() rewardsActivity: ActivityBlock[] = [];
    @Input() showFullList: boolean;
    @Input() activityCount: number;

    trackByFn(_: any, index: number): number {
        return index;
    }
}
