import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-rewards-top-bar",
    template: `
        <supr-page-header>
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="body" class="pageHeader">
                        ${TEXTS.HEADER_TEXT}
                    </supr-text>
                </div>
            </div>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {}
