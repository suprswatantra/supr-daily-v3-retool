import { FooterComponent } from "./footer/footer.component";
import { FaqComponent } from "./faqs/faq.component";
import { TopBarComponent } from "./top-bar/top-bar.component";
import { HeaderComponent } from "./header/header.component";
import { ActivityComponent } from "./activity/activity.component";

export const RewardsComponents = [
    TopBarComponent,
    HeaderComponent,
    FaqComponent,
    FooterComponent,
    ActivityComponent,
];
