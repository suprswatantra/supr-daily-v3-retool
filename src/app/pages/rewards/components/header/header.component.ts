import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { RewardsInfoHeader } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { STYLE_LIST, DEFAULT_IMG_URL } from "@pages/rewards/constants";

@Component({
    selector: "supr-rewards-header",
    template: `
        <div class="rewardsHeaderContainer" #headerWrapper>
            <div class="rewardsHeader">
                <div class="divider20"></div>
                <div class="imgWrapper">
                    <img [src]="headerData?.logo || defaultImgUrl" />
                </div>
                <ng-container *ngIf="headerData?.title?.text">
                    <div class="divider12"></div>
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.title"
                            class="boldText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container *ngIf="headerData?.subTitle?.text">
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.subTitle"
                            class="semiBoldText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container *ngIf="headerData?.infoText?.text">
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.infoText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    @Input() headerData: RewardsInfoHeader;

    @ViewChild("headerWrapper", { static: true }) wrapperEl: ElementRef;

    defaultImgUrl = DEFAULT_IMG_URL;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.headerData) {
            return;
        }

        this.utilService.setStyles(this.headerData, STYLE_LIST, this.wrapperEl);
    }
}
