import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    RewardsStoreSelectors,
    RewardsStoreActions,
} from "@supr/store";
import { ScratchedCardDetails } from "@shared/models";
import { Observable } from "rxjs";

@Injectable()
export class RewardsAdapter {
    rewardsInfo$ = this.store.pipe(
        select(RewardsStoreSelectors.selectRewardsInfo)
    );

    scratchCards$ = this.store.pipe(
        select(RewardsStoreSelectors.selectRewardsScratchCards)
    );

    scratchCard$ = this.store.pipe(
        select(RewardsStoreSelectors.selectScratchCardByUrlParams)
    );

    rewardsFaqs$ = this.store.pipe(
        select(RewardsStoreSelectors.selectRewardsFaqs)
    );

    rewardsActivity$ = this.store.pipe(
        select(RewardsStoreSelectors.selectRewardsActivity)
    );

    isFetchingScratchCardBenefit$ = this.store.pipe(
        select(RewardsStoreSelectors.selectIsFetchingScratchCardBenefit)
    );

    isFetchingRewardsInfo$ = this.store.pipe(
        select(RewardsStoreSelectors.selectIsFetchingRewardsInfo)
    );

    constructor(private store: Store<StoreState>) {}

    fetchRewardsInfo() {
        this.store.dispatch(
            new RewardsStoreActions.FetchRewardsRequestAction()
        );
    }

    getScratchedCardDetailsById(
        scratchCardId: number
    ): Observable<ScratchedCardDetails> {
        return this.store.pipe(
            select(
                RewardsStoreSelectors.selectRewardsScratchedCardDetailsById,
                {
                    scratchCardId,
                }
            )
        );
    }

    fetchScratchCardBenefit(id: number) {
        this.store.dispatch(
            new RewardsStoreActions.FetchScratchCardBenefitRequestAction({
                id,
            })
        );
    }
}
