import { TestBed } from "@angular/core/testing";

import { RewardsInfo, ScratchedCardDetails } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { RewardsService } from "./rewards.service";

import {
    BaseUnitTestImportsWithRouterIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../../unitTest.conf";

class RouterServiceMock {}

describe("RewardsService", () => {
    let rewardsService: RewardsService;
    let utilService: UtilService;
    let routerService: RouterService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithRouterIonic],

            providers: [
                UtilService,
                { provide: RouterService, useClass: RouterServiceMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        utilService = TestBed.get(UtilService);
        routerService = TestBed.get(RouterService);

        rewardsService = new RewardsService(utilService, routerService);
    });

    it("#should create rewrads service instance", () => {
        expect(rewardsService).toBeTruthy();
    });

    it("#should return ScratchCardSeeAllText", () => {
        const rewardsInfo: RewardsInfo = {
            scratchCards: [
                { id: 1, imgUrl: "" },
                { id: 1, imgUrl: "" },
                { id: 1, imgUrl: "" },
            ],
            header: { title: { text: "" } },
            footer: { redirectionUrl: "" },
        };

        const rewardsInfo1: RewardsInfo = {
            scratchCards: [{ id: 1, imgUrl: "" }],
            header: { title: { text: "" } },
            footer: { redirectionUrl: "" },
        };

        const rewardsInfo2: RewardsInfo = {
            scratchCards: [],
            header: { title: { text: "" } },
            footer: { redirectionUrl: "" },
        };

        const rewardsInfo3: RewardsInfo = {
            scratchCards: [
                { id: 1, imgUrl: "" },
                { id: 1, imgUrl: "" },
            ],
            header: { title: { text: "" } },
            footer: { redirectionUrl: "" },
        };

        expect(rewardsService.getScratchCardSeeAllText(rewardsInfo)).toBe(
            "SEE ALL (3)"
        );

        expect(rewardsService.getScratchCardSeeAllText(rewardsInfo1)).toBe("");

        expect(rewardsService.getScratchCardSeeAllText(rewardsInfo2)).toBe("");

        expect(rewardsService.getScratchCardSeeAllText(rewardsInfo3)).toBe(
            "SEE ALL (2)"
        );
    });

    it("#should return correct ActionBtnText", () => {
        const scratchedCardDetails: ScratchedCardDetails = {
            id: 1,
            isExpired: true,
        };

        const scratchedCardDetails1: ScratchedCardDetails = {
            id: 1,
            isError: true,
        };

        const scratchedCardDetails2: ScratchedCardDetails = {
            id: 1,
            benefitInfo: { text: "Hola" },
        };

        expect(rewardsService.getActionBtnText(scratchedCardDetails)).toBe(
            "OK GOT IT"
        );

        expect(rewardsService.getActionBtnText(scratchedCardDetails1)).toBe(
            "TRY AGAIN"
        );

        expect(rewardsService.getActionBtnText(scratchedCardDetails2)).toBe(
            "SEE ALL REWARDS"
        );
    });
});
