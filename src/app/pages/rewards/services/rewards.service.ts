import { Injectable } from "@angular/core";

import { BENEFITS_BANNER_CTA_ACTION_TYPES } from "@constants";

import {
    RewardsInfo,
    BenefitsBannerCtaAction,
    TextFragment,
    ScratchedCardDetails,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import {
    TEXTS,
    SCRATCH_CARD_ERROR_EXPIRY,
    SCRATCH_CARD_ERROR_GENERIC,
    SCRATCH_CARD_ERROR_ICON,
    SCRATCH_CARD_URLS,
} from "../constants";
import { ConfigurableIcon } from "@types";

@Injectable()
export class RewardsService {
    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    handleBenefitCtaClick(ctaAction: BenefitsBannerCtaAction) {
        const ctaActionType = this.utilService.getNestedValue(
            ctaAction,
            "action"
        );

        if (!ctaActionType) {
            return;
        }

        switch (ctaActionType) {
            case BENEFITS_BANNER_CTA_ACTION_TYPES.ROUTE:
                const route = this.utilService.getNestedValue(
                    ctaAction,
                    "appUrl"
                );

                if (!route) {
                    return;
                }

                this.routerService.goToUrl(route);
                return;

            default:
                break;
        }
    }

    handleFaqClick(rewardsInfo: RewardsInfo) {
        const hasFaq = this.utilService.getNestedValue(rewardsInfo, "faqs");

        if (!hasFaq) {
            return;
        }

        this.routerService.goToRewardsFaqPage();
    }

    handleFooterButtonClick(rewardsInfo: RewardsInfo) {
        const redirectionUrl = this.utilService.getNestedValue(
            rewardsInfo,
            "footer.redirectionUrl",
            null
        );

        if (!redirectionUrl) {
            return;
        }
        this.routerService.goToUrl(redirectionUrl);
    }

    getScratchCardSeeAllText(rewardsInfo: RewardsInfo): string {
        const scratchCards = this.utilService.getNestedValue(
            rewardsInfo,
            "scratchCards",
            null
        );

        if (!scratchCards) {
            return "";
        }

        if (!this.utilService.isLengthyArray(scratchCards)) {
            return "";
        }

        if (scratchCards.length === 1) {
            return "";
        }

        return `${TEXTS.SEE_ALL_TEXT} (${scratchCards.length})`;
    }

    getScratchCardTextFragment(
        scratchedCardDetails: ScratchedCardDetails
    ): TextFragment {
        const { isExpired, isError, benefitInfo } = this.getScratchedCardFields(
            scratchedCardDetails
        );

        if (isExpired) {
            return SCRATCH_CARD_ERROR_EXPIRY;
        }

        if (isError) {
            return SCRATCH_CARD_ERROR_GENERIC;
        }

        if (benefitInfo) {
            return benefitInfo;
        }

        return null;
    }

    getErrorIcon(scratchedCardDetails: ScratchedCardDetails): ConfigurableIcon {
        const { isExpired, isError } = this.getScratchedCardFields(
            scratchedCardDetails
        );

        if (isExpired || isError) {
            return SCRATCH_CARD_ERROR_ICON;
        }

        return null;
    }

    getActionBtnText(scratchedCardDetails: ScratchedCardDetails): string {
        const { isExpired, isError, benefitInfo } = this.getScratchedCardFields(
            scratchedCardDetails
        );

        if (isExpired) {
            return TEXTS.OK;
        }

        if (isError) {
            return TEXTS.TRY_AGAIN;
        }

        if (benefitInfo) {
            return TEXTS.SEE_ALL_REWARDS;
        }

        return "";
    }

    getIsCelebration(scratchedCardDetails: ScratchedCardDetails): boolean {
        const { benefitInfo } = this.getScratchedCardFields(
            scratchedCardDetails
        );

        if (benefitInfo) {
            return true;
        }

        return false;
    }

    getScratchedCardFields(
        scratchedCardDetails: ScratchedCardDetails
    ): { isExpired: boolean; isError: boolean; benefitInfo: TextFragment } {
        const isExpired = this.utilService.getNestedValue(
            scratchedCardDetails,
            "isExpired",
            null
        );

        const isError = this.utilService.getNestedValue(
            scratchedCardDetails,
            "isError",
            null
        );

        const benefitInfo = this.utilService.getNestedValue(
            scratchedCardDetails,
            "benefitInfo",
            null
        );

        return { isExpired, isError, benefitInfo };
    }

    getScratchCardImgUrl(id = 1) {
        const imageUrlIndex = id % SCRATCH_CARD_URLS.length;
        return SCRATCH_CARD_URLS[imageUrlIndex];
    }
}
