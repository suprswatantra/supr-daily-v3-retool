import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

import { deliveryProofComponents } from "./components";
import { ReportImagesContainer } from "./containers/report-images.container";
import { ReportImagesLayoutComponent } from "./layouts/report-images.layout";
import { DeliveryProofLayoutComponent } from "./layouts/delivery-proof.layout";
import { DeliveryProofContainer } from "./containers/delivery-proof.container";

import { deliveryProofServices } from "./services";

const routes: Routes = [
    {
        path: "",
        component: DeliveryProofContainer,
    },
    {
        component: ReportImagesContainer,
        path: PAGE_ROUTES.DELIVERY_PROOF_V2.CHILDREN.REPORT.PATH,
        data: {
            screenName: SCREEN_NAMES.REPORT_DELIVERY_PROOF,
        },
    },
];

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        ReportImagesContainer,
        DeliveryProofContainer,
        ReportImagesLayoutComponent,
        DeliveryProofLayoutComponent,
        ...deliveryProofComponents,
    ],
    providers: [...deliveryProofServices],
})
export class DeliveryProofV2Module {}
