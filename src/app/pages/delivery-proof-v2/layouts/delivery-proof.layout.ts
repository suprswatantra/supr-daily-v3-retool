import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DeliveryProof } from "@models";

import { RouterService } from "@services/util/router.service";

import {
    SuprApi,
    Segment,
    DeliveryProofPageQueryParams,
    PODFeedbackClickHandlerInfo,
} from "@types";

import { SA_CONTEXT_NAMES, SA_OBJECT_NAMES } from "../constants";
import { DeliveryProofPageService } from "../services/delivery-proof.service";

@Component({
    selector: "supr-delivery-proof-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    {{ textConstants.FEEDBACK_PAGE_HEADER }}
                </supr-text>
            </supr-page-header>

            <div class="suprScrollContent">
                <ng-container *ngIf="deliveryProof?.length">
                    <div
                        class="contentWrapper"
                        *ngFor="
                            let proofObject of deliveryProof;
                            trackBy: trackDeliveryProof;
                            index as _i
                        "
                    >
                        <supr-delivery-proof
                            [saPosition]="_i + 1"
                            [deliveryProof]="proofObject"
                            [textConstants]="textConstants"
                            [deliveryDate]="queryParams?.delivery_date"
                            [deliverySlot]="queryParams?.delivery_slot"
                            (handleFeedbackClick)="
                                handleFeedbackClick($event, _i)
                            "
                        >
                        </supr-delivery-proof>
                    </div>
                </ng-container>
            </div>

            <supr-page-footer>
                <div class="suprRow spaceBetween">
                    <supr-button
                        class="back"
                        (handleClick)="goBack()"
                        [saContextList]="footerContextList"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.CLOSE}"
                    >
                        <supr-text type="action14">
                            {{ textConstants.FEEDBACK_PAGE_CLOSE }}
                        </supr-text>
                    </supr-button>
                    <supr-button
                        (handleClick)="goToSupportPage()"
                        [saContextList]="footerContextList"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.GET_HELP}"
                    >
                        <supr-text type="action14">
                            {{ textConstants.FEEDBACK_PAGE_HELP }}
                        </supr-text>
                    </supr-button>
                </div>
            </supr-page-footer>
        </div>
    `,
    styleUrls: ["../styles/delivery-proof.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProofLayoutComponent implements OnInit {
    @Input() textConstants: any;
    @Input() deliveryProof: DeliveryProof[];
    @Input() queryParams: DeliveryProofPageQueryParams;

    @Output()
    postPodFeedback: EventEmitter<SuprApi.DeliveryProofFeedbackPayload> = new EventEmitter();
    @Output()
    fetchDeliveryProof: EventEmitter<SuprApi.DeliveryProofQueryParams> = new EventEmitter();

    footerContextList: Segment.ContextListItem[];

    constructor(
        private routerService: RouterService,
        private deliveryProofService: DeliveryProofPageService
    ) {}

    ngOnInit() {
        this.setAnalyticsData(this.queryParams);
        this.fetchDeliveryProof.emit(this.queryParams);
    }

    trackDeliveryProof(_: number, deliveryProof: DeliveryProof): string {
        return deliveryProof.image_type;
    }

    handleFeedbackClick(
        feedbackInfo: PODFeedbackClickHandlerInfo,
        selectedProof: number
    ) {
        const deliveryProofFeedback = this.deliveryProofService.getDeliveryFeedbackPayload(
            selectedProof,
            this.deliveryProof,
            this.queryParams,
            feedbackInfo
        );

        this.postPodFeedback.emit(deliveryProofFeedback);
    }

    goBack() {
        this.deliveryProofService.setSchedulePageScrollToDate(this.queryParams);
        this.routerService.goBack();
    }

    goToSupportPage() {
        this.routerService.goToSupportPage();
    }

    private setAnalyticsData(queryParams: DeliveryProofPageQueryParams) {
        this.footerContextList = [
            {
                name: SA_CONTEXT_NAMES.DELIVERY_DATE,
                value: queryParams && queryParams.delivery_date,
            },
        ];
    }
}
