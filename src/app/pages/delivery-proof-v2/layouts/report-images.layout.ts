import {
    Input,
    OnInit,
    Output,
    OnDestroy,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";
import { FormControl } from "@angular/forms";

import { Subscription } from "rxjs";
import { debounceTime } from "rxjs/operators";

import { SETTINGS, TOAST_MESSAGES } from "@constants";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

import { DeliveryProofCurrentState } from "@store/delivery-proof/delivery-proof.state";

import {
    SuprApi,
    Segment,
    ReportPODIssue,
    ReportPODTextConfig,
    ReportPODImageQueryParams,
} from "@types";

import { SA_CONTEXT_NAMES, SA_OBJECT_NAMES } from "../constants";

import { DeliveryProofPageService } from "../services/delivery-proof.service";

@Component({
    selector: "supr-delivery-proof-report-images-layout",
    template: ` <div class="suprContainer">
        <supr-page-header>
            <supr-text type="subheading">
                {{ textConstants.REPORT_PAGE_HEADER }}
            </supr-text>
        </supr-page-header>

        <div class="suprScrollContent">
            <supr-section-header
                icon="error_octagon"
                subTitleType="paragraph"
                [title]="textConstants.REPORT_PHOTO_TITLE"
                [subtitle]="textConstants.REPORT_PHOTO_SUBTITLE"
            ></supr-section-header>

            <div class="contentWrapper">
                <div class="headerTextWrapper" *ngIf="textConfig?.header">
                    <supr-text type="body">{{ textConfig.header }}</supr-text>
                </div>

                <div class="issuesWrapper" *ngIf="textConfig?.issues.length">
                    <supr-delivery-proof-report-issues
                        [issues]="textConfig.issues"
                        [textConstants]="textConstants"
                        (handleIssueClick)="handleIssueClick($event)"
                    >
                    </supr-delivery-proof-report-issues>
                </div>

                <div class="descriptionWrapper">
                    <supr-text type="subheading">
                        {{ textConstants.REPORT_PAGE_DESCRIBE }}
                    </supr-text>
                    <textarea
                        rows="5"
                        type="text"
                        [formControl]="textAreaFormCtrl"
                    ></textarea>
                </div>
            </div>
        </div>

        <supr-page-footer>
            <supr-button
                [loading]="loading"
                [saContext]="description"
                [saContextList]="saContextList"
                [saObjectValue]="saObjectValue"
                [disabled]="loading || selectedIssues < 1"
                saObjectName="${SA_OBJECT_NAMES.CLICK.SUBMIT_REPORT}"
                (handleClick)="handleSubmit()"
            >
                <supr-text type="action14"> SUBMIT </supr-text>
            </supr-button>
        </supr-page-footer>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../styles/report-images.page.scss"],
})
export class ReportImagesLayoutComponent
    implements OnInit, OnChanges, OnDestroy {
    @Input() textConstants: any;
    @Input() queryParams: ReportPODImageQueryParams;
    @Input() currentState: DeliveryProofCurrentState;

    @Output()
    reportDeliveryImage: EventEmitter<SuprApi.DeliveryProofReportPayload> = new EventEmitter();

    loading = false;
    description = "";
    selectedIssues = 0;
    saObjectValue: string[];
    textConfig: ReportPODTextConfig;
    textAreaFormCtrlSub: Subscription;
    textAreaFormCtrl = new FormControl();
    saContextList: Segment.ContextListItem[];

    constructor(
        private utilService: UtilService,
        private toastService: ToastService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private deliveryProofService: DeliveryProofPageService
    ) {}

    ngOnInit() {
        this.textConfig = this.settingsService.getSettingsValue(
            SETTINGS.REPORT_POD_CONFIG
        );

        this.textAreaFormCtrlSub = this.textAreaFormCtrl.valueChanges
            .pipe(debounceTime(1000))
            .subscribe((newValue) => {
                this.description = newValue;
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleReportingPODChanges(changes["currentState"]);
    }

    ngOnDestroy() {
        if (this.textAreaFormCtrlSub && !this.textAreaFormCtrlSub.closed) {
            this.textAreaFormCtrlSub.unsubscribe();
        }
    }

    handleIssueClick(index: number) {
        const issueToBeUpdated = this.utilService.getNestedValue(
            this.textConfig,
            `issues.${index}`
        );

        if (!this.utilService.isEmpty(issueToBeUpdated)) {
            this.textConfig.issues[index].selected = !this.textConfig.issues[
                index
            ].selected;
            this.textConfig.issues[index].icon = this.textConfig.issues[index]
                .selected
                ? ICON_NAMES.APPROVE_FILLED
                : ICON_NAMES.APPROVE;

            this.setAnalyticsData();
            this.setButtonState(this.textConfig.issues[index].selected);
        }
    }

    handleSubmit() {
        const reportImagePayload = this.deliveryProofService.getReportImagePayload(
            this.description,
            this.textConfig.issues,
            this.queryParams
        );

        if (!reportImagePayload) {
            return;
        }

        this.loading = true;
        this.reportDeliveryImage.emit(reportImagePayload);
    }

    private setAnalyticsData() {
        const issues: ReportPODIssue[] = this.utilService.getNestedValue(
            this.textConfig,
            "issues"
        );

        this.saContextList = [
            {
                name: SA_CONTEXT_NAMES.DELIVERY_DATE,
                value: this.queryParams && this.queryParams.delivery_date,
            },
        ];

        if (this.utilService.isLengthyArray(issues)) {
            this.saObjectValue = issues
                .filter((issue) => issue.selected)
                .map((issue) => issue.key);
        }
    }

    private handleReportingPODChanges(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            change.previousValue ===
                DeliveryProofCurrentState.REPORTING_POD_IMAGES &&
            change.currentValue === DeliveryProofCurrentState.IDLE
        ) {
            this.toastService.present(
                TOAST_MESSAGES.DELIVERY_PROOF.REPORT_IMAGE_SUCCESS,
                { duration: 4000 }
            );
            this.deliveryProofService.setSchedulePageScrollToDate(
                this.queryParams
            );
            this.routerService.goToSchedulePage();
        }
    }

    private setButtonState(selected: boolean) {
        if (selected) {
            this.selectedIssues += 1;
        } else {
            this.selectedIssues -= 1;
        }
    }
}
