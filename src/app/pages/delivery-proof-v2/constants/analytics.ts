export const SA_OBJECT_NAMES = {
    CLICK: {
        CLOSE: "close",
        TAG: "attribute",
        GET_HELP: "get-help",
        REPORT_CTA: "report-photo",
        SUBMIT_REPORT: "submit-report-photo",
    },

    IMPRESSION: {
        POD_FEEDBACK: "pod-feedback",
        FOLLOWUP_QN: "followup-question",
        FEEDBACK_GIVEN: "thank-you-pod-feedback",
    },
};

export const SA_CONTEXT_NAMES = {
    ATTRIBUTES: "attributes",
    DELIVERY_DATE: "delivery-date",
    FEEDBACK_STATE: "feedback-state",
    FOLLOWUP_QN: "followup-question",
};
