export * from "./analytics";

export const TEXTS = {
    FEEDBACK_PAGE_CLOSE: "CLOSE",
    FEEDBACK_PAGE_HELP: "GET HELP",
    REPORT_PAGE_HEADER: "Report photo",
    IMAGE_REPORT_PHOTO: "REPORT PHOTO",
    REPORT_SELECT_ISSUE: "Select issue*",
    FEEDBACK_PAGE_HEADER: "Delivery photo",
    REPORT_PHOTO_TITLE: "Report this photo(s)",
    REPORT_PAGE_DESCRIBE: "Describe issue (optional)",
    REPORT_ISSUE_MANDATORY: "(This is a mandatory field)",
    REPORT_PHOTO_SUBTITLE: "All reports are strictly confidential.",
};
