import { Injectable } from "@angular/core";

import { select, Store } from "@ngrx/store";

import { SuprApi } from "@types";

import { RouterStoreSelectors, StoreState } from "@supr/store";

import {
    DeliveryProofStoreActions,
    DeliveryProofStoreSelectors,
} from "@store/delivery-proof";

@Injectable()
export class DeliveryProofPageAdapter {
    constructor(private store: Store<StoreState>) {}

    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    currentState$ = this.store.pipe(
        select(DeliveryProofStoreSelectors.selectStoreCurrentState)
    );

    deliveryProof$ = this.store.pipe(
        select(DeliveryProofStoreSelectors.selectDeliveryProof)
    );

    fetchDeliveryProof(params: SuprApi.DeliveryProofQueryParams) {
        this.store.dispatch(
            new DeliveryProofStoreActions.FetchDeliveryProofsAction(params)
        );
    }

    postPodFeedback(feedback: SuprApi.DeliveryProofFeedbackPayload) {
        this.store.dispatch(
            new DeliveryProofStoreActions.PostDeliveryProofFeedbackAction(
                feedback
            )
        );
    }

    reportDeliveryImage(payload: SuprApi.DeliveryProofReportPayload) {
        this.store.dispatch(
            new DeliveryProofStoreActions.ReportDeliveryProofImagesAction(
                payload
            )
        );
    }
}
