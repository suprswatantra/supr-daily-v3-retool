import { DeliveryProofPageAdapter } from "./delivery-proof.adapter";
import { DeliveryProofPageService } from "./delivery-proof.service";

export const deliveryProofServices = [
    DeliveryProofPageAdapter,
    DeliveryProofPageService,
];
