import { TestBed } from "@angular/core/testing";

import { DeliveryProofFeedbackTag, DeliveryProofFeedbackAnswer } from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { ReportPODImageQueryParams, ReportPODIssue, SuprApi } from "@types";

import {
    BaseUnitTestProvidersIonic,
    BaseUnitTestImportsWithStoreIonic,
} from "../../../../unitTest.conf";
import { DeliveryProofPageService } from "./delivery-proof.service";

describe("DeliveryProofPageService", () => {
    let utilService: UtilService;
    let dateService: DateService;
    let toastService: ToastService;
    let scheduleService: ScheduleService;
    let deliveryProofPageService: DeliveryProofPageService;

    const podIssues: ReportPODIssue[] = [
        {
            key: "spam",
            selected: false,
            icon: "approve",
            value: "Spam or scam",
        },
        {
            icon: "approve",
            selected: false,
            key: "hate_speech",
            value: "Contains hate speech",
        },
        {
            key: "violence",
            icon: "approve",
            selected: false,
            value: "Violence, crime, or self-harm",
        },
        {
            key: "nudity",
            icon: "approve",
            selected: false,
            value: "Nudity, pornography, or sexually explicit content",
        },
    ];

    const podIssuesSelected: ReportPODIssue[] = [
        {
            key: "spam",
            selected: false,
            icon: "approve",
            value: "Spam or scam",
        },
        {
            icon: "approve",
            selected: true,
            key: "hate_speech",
            value: "Contains hate speech",
        },
        {
            key: "violence",
            icon: "approve",
            selected: false,
            value: "Violence, crime, or self-harm",
        },
        {
            key: "nudity",
            icon: "approve",
            selected: true,
            value: "Nudity, pornography, or sexually explicit content",
        },
    ];

    const feedbackAnswerNoTags: DeliveryProofFeedbackAnswer = {
        tags: [],
        key: "test",
        value: "Test",
        icon: "test_icon",
    };

    const feedbackAnswerUndefinedTags: DeliveryProofFeedbackAnswer = {
        key: "test",
        value: "Test",
        icon: "test_icon",
        tags: undefined,
    };

    const feedbackAnswerWithSelectedTags: DeliveryProofFeedbackAnswer = {
        key: "test",
        value: "Test",
        icon: "test_icon",
        tags: podIssuesSelected,
    };

    const feedbackAnswerWithResetTags: DeliveryProofFeedbackAnswer = {
        key: "test",
        value: "Test",
        tags: podIssues,
        icon: "test_icon",
    };

    const reportPODQueryParams: ReportPODImageQueryParams = {
        image_type: "PRODUCTS",
        delivery_date: "2020-12-01",
        delivery_slot: "03:00-07:00",
    };

    const reportImagesPayload: SuprApi.DeliveryProofReportPayload = {
        ...reportPODQueryParams,
        description: "Test Description",
        issues: ["hate_speech", "nudity"],
    };

    const feedbackTags: DeliveryProofFeedbackTag[] = [
        {
            key: "CLEAR",
            value: "clear image",
            selected: true,
        },
        {
            key: "COVER_PRODUCT",
            value: "cover all products",
        },
        {
            key: "NOT_CLEAR",
            value: "photo not clear",
            selected: true,
        },
        {
            key: "NOT_RECEIVED",
            value: "Items not received",
        },
    ];

    const selectedFeedbackTags: string[] = ["CLEAR", "NOT_CLEAR"];

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                UtilService,
                DateService,
                ToastService,
                ScheduleService,

                ...BaseUnitTestProvidersIonic,
            ],
        });

        utilService = TestBed.get(UtilService);
        dateService = TestBed.get(DateService);
        toastService = TestBed.get(ToastService);
        scheduleService = TestBed.get(ScheduleService);

        deliveryProofPageService = new DeliveryProofPageService(
            utilService,
            dateService,
            toastService,
            scheduleService
        );
    });

    it("#should create error service instance", () => {
        expect(deliveryProofPageService).toBeTruthy();
    });

    it("#should return the feedbackAnswer param as passed if it is empty or tags are undefined/empty", () => {
        expect(deliveryProofPageService.resetFeedbackTagStates(undefined)).toBe(
            undefined
        );

        expect(
            deliveryProofPageService.resetFeedbackTagStates(
                feedbackAnswerNoTags
            )
        ).toEqual(feedbackAnswerNoTags);

        expect(
            deliveryProofPageService.resetFeedbackTagStates(
                feedbackAnswerUndefinedTags
            )
        ).toEqual(feedbackAnswerUndefinedTags);
    });

    it("#should return the feedback answer with tag states reset to unselected", () => {
        expect(
            deliveryProofPageService.resetFeedbackTagStates(
                feedbackAnswerWithSelectedTags
            )
        ).toEqual(feedbackAnswerWithResetTags);
    });

    it("#should return undefined when issues is undefined or an empty array or query params are empty", () => {
        expect(
            deliveryProofPageService.getReportImagePayload(
                "Test Description",
                undefined,
                reportPODQueryParams
            )
        ).toBe(undefined);

        expect(
            deliveryProofPageService.getReportImagePayload(
                "Test Description",
                [],
                reportPODQueryParams
            )
        ).toBe(undefined);

        expect(
            deliveryProofPageService.getReportImagePayload(
                "Test Description",
                podIssuesSelected,
                undefined
            )
        ).toBe(undefined);
    });

    it("#should return undefined when no issues are selected", () => {
        expect(
            deliveryProofPageService.getReportImagePayload(
                "Test Description",
                podIssues,
                reportPODQueryParams
            )
        ).toBe(undefined);
    });

    it("#should return a valid payload when at least one pod issue is selected and valid query params are passed", () => {
        expect(
            deliveryProofPageService.getReportImagePayload(
                "Test Description",
                podIssuesSelected,
                reportPODQueryParams
            )
        ).toEqual(reportImagesPayload);
    });

    it("#should return selected feedback tags", () => {
        expect(
            (deliveryProofPageService as any).getSelectedFeedbackTags(
                feedbackTags
            )
        ).toEqual(selectedFeedbackTags);
    });
});
