import { Injectable } from "@angular/core";

import { TOAST_MESSAGES } from "@constants";

import {
    DeliveryProof,
    DeliveryProofFeedbackTag,
    DeliveryProofFeedbackAnswer,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { ScheduleService } from "@services/shared/schedule.service";

import {
    SuprApi,
    ReportPODIssue,
    ReportPODImageQueryParams,
    PODFeedbackClickHandlerInfo,
    DeliveryProofPageQueryParams,
} from "@types";

@Injectable()
export class DeliveryProofPageService {
    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private toastService: ToastService,
        private scheduleService: ScheduleService
    ) {}

    /**
     * Generate payload for Delivery Proof feedback
     *
     * @param {number} selectedProofIndex
     * @param {Array<DeliveryProof>} deliveryProofs
     * @param {DeliveryProofPageQueryParams} queryParams
     * @param {PODFeedbackClickHandlerInfo} feedbackInfo
     * @returns {SuprApi.DeliveryProofFeedbackPayload}
     * @memberof DeliveryProofPageService
     */
    getDeliveryFeedbackPayload(
        selectedProofIndex: number,
        deliveryProofs: DeliveryProof[],
        queryParams: DeliveryProofPageQueryParams,
        feedbackInfo: PODFeedbackClickHandlerInfo
    ): SuprApi.DeliveryProofFeedbackPayload {
        if (
            isNaN(selectedProofIndex) ||
            this.utilService.isEmpty(queryParams) ||
            this.utilService.isEmpty(feedbackInfo) ||
            !this.utilService.isLengthyArray(deliveryProofs) ||
            !this.dateService.isValidDateString(queryParams.delivery_date)
        ) {
            return;
        }

        const selectedProof = deliveryProofs[selectedProofIndex];

        if (this.utilService.isEmpty(selectedProof)) {
            return;
        }

        const primaryAnswer = feedbackInfo.answer;

        if (this.utilService.isEmpty(primaryAnswer)) {
            return;
        }

        const payload: SuprApi.DeliveryProofFeedbackPayload = {
            ...queryParams,
            image_type: selectedProof.image_type,
            answer: {
                key: primaryAnswer.key,
            },
        };

        const selectedTags = this.getSelectedFeedbackTags(primaryAnswer.tags);

        /* only primary answer selected */
        if (!this.utilService.isLengthyArray(selectedTags)) {
            return payload;
        }

        return {
            ...payload,
            answer: { ...payload.answer, tags: selectedTags },
        };
    }

    /**
     * Reset feedback tag states when user toggles between +ve and -ve feedback
     *
     * @param {DeliveryProofFeedbackAnswer} feedbackAnswer
     * @returns {DeliveryProofFeedbackAnswer}
     * @memberof DeliveryProofPageService
     */
    resetFeedbackTagStates(
        feedbackAnswer: DeliveryProofFeedbackAnswer
    ): DeliveryProofFeedbackAnswer {
        if (
            this.utilService.isEmpty(feedbackAnswer) ||
            !this.utilService.isLengthyArray(feedbackAnswer.tags)
        ) {
            return feedbackAnswer;
        }

        const tags: DeliveryProofFeedbackTag[] = feedbackAnswer.tags.map(
            (tag) => ({ ...tag, selected: false })
        );

        return {
            ...feedbackAnswer,
            tags,
        };
    }

    /**
     * Generate payload for Delivery Proof feedback
     *
     * @param {string} description
     * @param {Array<ReportPODIssue>} issues
     * @returns {SuprApi.DeliveryProofReportPayload}
     * @memberof DeliveryProofPageService
     */
    getReportImagePayload(
        description: string,
        issues: ReportPODIssue[],
        queryParams: ReportPODImageQueryParams
    ): SuprApi.DeliveryProofReportPayload {
        if (
            !this.utilService.isLengthyArray(issues) ||
            this.utilService.isEmpty(queryParams)
        ) {
            this.toastService.present(TOAST_MESSAGES.SOME_THING_WENT_WRONG);
            return;
        }

        const selectedIssues = issues.filter((issue) => issue.selected);

        if (!this.utilService.isLengthyArray(selectedIssues)) {
            this.toastService.present(
                TOAST_MESSAGES.DELIVERY_PROOF.REPORT_IMAGE_ISSUE_MANDATORY
            );
            return;
        }

        return {
            ...queryParams,
            description: description || "",
            issues: selectedIssues.map((issue) => issue.key),
        };
    }

    /**
     * Set custom schedule start date and scroll date for navigation to the calendar page
     * @param {DeliveryProofPageQueryParams} queryParams
     * @memberof DeliveryProofPageService
     */
    setSchedulePageScrollToDate(queryParams: DeliveryProofPageQueryParams) {
        const deliveryDateString = this.utilService.getNestedValue(
            queryParams,
            "delivery_date"
        );

        if (
            !deliveryDateString ||
            !this.dateService.isValidDateString(deliveryDateString)
        ) {
            return;
        }

        const deliveryDate = this.dateService.dateFromText(deliveryDateString);
        const weekStartDate = this.dateService.startDateOfWeek(deliveryDate);
        const startDateString = this.dateService.textFromDate(weekStartDate);

        this.scheduleService.setCustomScheduleStartDate(startDateString);
        this.scheduleService.setCustomScheduleScrollDate(deliveryDateString);
    }

    /**
     * Filter selected feedback tags
     *
     * @param {Array<DeliveryProofFeedbackTag>} tags
     * @returns {Array<string>}
     * @memberof DeliveryProofPageService
     */
    private getSelectedFeedbackTags(
        tags: DeliveryProofFeedbackTag[]
    ): string[] {
        if (!this.utilService.isLengthyArray(tags)) {
            return;
        }

        return tags
            .filter((tag: DeliveryProofFeedbackTag) => tag.selected)
            .map((tag: DeliveryProofFeedbackTag) => tag.key);
    }
}
