import { Params } from "@angular/router";
import { OnInit, Component, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SETTINGS } from "@constants";

import { DeliveryProof } from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { SuprApi, DeliveryProofPageQueryParams } from "@types";

import { TEXTS } from "../constants";
import { DeliveryProofPageAdapter } from "../services/delivery-proof.adapter";

@Component({
    selector: "supr-delivery-proof-container",
    template: `<supr-delivery-proof-layout
        [textConstants]="textConstants"
        [queryParams]="queryParams$ | async"
        [deliveryProof]="deliveryProof$ | async"
        (postPodFeedback)="postPodFeedback($event)"
        (fetchDeliveryProof)="fetchDeliveryProof($event)"
    ></supr-delivery-proof-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProofContainer implements OnInit {
    textConstants = TEXTS;
    deliveryProof$: Observable<DeliveryProof[]>;
    queryParams$: Observable<DeliveryProofPageQueryParams>;

    constructor(
        private settingsService: SettingsService,
        private pageAdapter: DeliveryProofPageAdapter
    ) {}

    ngOnInit() {
        this.queryParams$ = this.pageAdapter.queryParams$.pipe(
            map((params: Params) => ({
                delivery_date: params["date"],
                delivery_slot: params["slot"],
            }))
        );
        this.deliveryProof$ = this.pageAdapter.deliveryProof$;
        this.textConstants = this.settingsService.getSettingsValue(
            SETTINGS.POD_FEEDBACK_TEXT_CONFIG
        );
    }

    postPodFeedback(feedback: SuprApi.DeliveryProofFeedbackPayload) {
        this.pageAdapter.postPodFeedback(feedback);
    }

    fetchDeliveryProof(params: SuprApi.DeliveryProofQueryParams) {
        this.pageAdapter.fetchDeliveryProof(params);
    }
}
