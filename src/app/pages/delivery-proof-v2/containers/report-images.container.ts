import { Params } from "@angular/router";
import { OnInit, Component, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SETTINGS } from "@constants";

import { SettingsService } from "@services/shared/settings.service";

import { DeliveryProofCurrentState } from "@store/delivery-proof/delivery-proof.state";

import { DeliveryProofPageQueryParams, SuprApi } from "@types";

import { TEXTS } from "../constants";
import { DeliveryProofPageAdapter } from "../services/delivery-proof.adapter";

@Component({
    selector: "supr-delivery-proof-report-container",
    template: `<supr-delivery-proof-report-images-layout
        [textConstants]="textConstants"
        [queryParams]="queryParams$ | async"
        [currentState]="currentState$ | async"
        (reportDeliveryImage)="reportDeliveryImage($event)"
    ></supr-delivery-proof-report-images-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportImagesContainer implements OnInit {
    textConstants = TEXTS;
    currentState$: Observable<DeliveryProofCurrentState>;
    queryParams$: Observable<DeliveryProofPageQueryParams>;

    constructor(
        private settingsService: SettingsService,
        private pageAdapter: DeliveryProofPageAdapter
    ) {}

    ngOnInit() {
        this.queryParams$ = this.pageAdapter.queryParams$.pipe(
            map((params: Params) => ({
                delivery_slot: params["slot"],
                image_type: params["imageType"],
                delivery_date: params["deliveryDate"],
            }))
        );

        this.currentState$ = this.pageAdapter.currentState$;
        this.textConstants = this.settingsService.getSettingsValue(
            SETTINGS.POD_FEEDBACK_TEXT_CONFIG
        );
    }

    reportDeliveryImage(payload: SuprApi.DeliveryProofReportPayload) {
        this.pageAdapter.reportDeliveryImage(payload);
    }
}
