import { FeedbackTagsComponent } from "./feedback-tags/feedback-tags.component";
import { ReportIssuesComponent } from "./report-issues/report-issues.component";
import { DeliveryProofComponent } from "./delivery-proof/delivery-proof.component";
import { DeliveryFeedbackComponent } from "./delivery-feedback/delivery-feedback.component";

export const deliveryProofComponents = [
    FeedbackTagsComponent,
    ReportIssuesComponent,
    DeliveryProofComponent,
    DeliveryFeedbackComponent,
];
