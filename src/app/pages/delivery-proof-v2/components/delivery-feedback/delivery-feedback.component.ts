import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DeliveryProof, DeliveryProofFeedbackAnswer } from "@models";

import { UtilService } from "@services/util/util.service";

import { PODFeedbackClickHandlerInfo, Segment } from "@types";

import { DeliveryProofPageService } from "@pages/delivery-proof-v2/services/delivery-proof.service";

import { SA_CONTEXT_NAMES, SA_OBJECT_NAMES } from "../../constants";

@Component({
    selector: "supr-delivery-feedback",
    template: `
        <ng-container
            *ngIf="
                deliveryProof?.primary_question &&
                    deliveryProof?.possible_answers?.length;
                else postFeedbackTemplate
            "
        >
            <div class="feedbackWrapper">
                <div class="header">
                    <supr-text type="subheading">
                        {{ deliveryProof.primary_question }}
                    </supr-text>
                </div>
                <div class="suprRow center iconsWrapper">
                    <div
                        saClick
                        [saPosition]="saPosition"
                        class="iconWrapper suprColumn center"
                        [saObjectName]="answer?.sa_object_name"
                        [saContextList]="feedbackButtonsContextList[_i]"
                        [saObjectValue]="deliveryProof.primary_question"
                        (click)="_handleAnswerClick(answer)"
                        *ngFor="
                            let answer of deliveryProof.possible_answers;
                            trackBy: trackByFn;
                            index as _i
                        "
                        [class.selected]="
                            selectedAnswer && selectedAnswer.key === answer.key
                        "
                    >
                        <supr-icon
                            *ngIf="answer?.icon"
                            [name]="answer.icon"
                        ></supr-icon>
                    </div>
                </div>
                <ng-container
                    *ngFor="
                        let answer of deliveryProof.possible_answers;
                        trackBy: trackByFn
                    "
                >
                    <div
                        class="tagsWrapper"
                        *ngIf="answer.key === selectedAnswer?.key"
                    >
                        <!-- duplicating component using ngFor with same props to force re-init and
                        send the impression event with right data on toggle -->

                        <supr-delivery-proof-feedback-tags
                            [saPosition]="saPosition"
                            [tags]="selectedAnswer.tags"
                            [deliveryDate]="deliveryDate"
                            [question]="selectedAnswer?.followup_question"
                            (handleTagClick)="handleTagClick($event)"
                        >
                        </supr-delivery-proof-feedback-tags>
                    </div>
                </ng-container>
            </div>
        </ng-container>

        <ng-template #postFeedbackTemplate>
            <ng-container
                *ngIf="
                    deliveryProof?.primary_answer?.icon &&
                    deliveryProof?.feedback_note?.title
                "
            >
                <div
                    saImpression
                    class="savedFeedbackWrapper"
                    [saPosition]="saPosition"
                    [saContextList]="saContextList"
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION.FEEDBACK_GIVEN}"
                    [saObjectValue]="
                        deliveryProof.primary_answer?.sa_object_value
                    "
                >
                    <div class="suprRow center">
                        <div class="iconWrapper suprColumn center">
                            <supr-icon
                                *ngIf="deliveryProof.primary_answer?.icon"
                                [name]="deliveryProof.primary_answer.icon"
                            ></supr-icon>
                        </div>
                    </div>

                    <div class="divider8"></div>
                    <supr-text class="title" type="subheading">
                        {{ deliveryProof.feedback_note.title }}
                    </supr-text>

                    <ng-container
                        *ngIf="deliveryProof?.feedback_note?.subtitle"
                    >
                        <div class="divider4"></div>
                        <supr-text class="subtitle" type="paragraph">
                            {{ deliveryProof.feedback_note.subtitle }}
                        </supr-text>
                    </ng-container>
                </div>
            </ng-container>
        </ng-template>
    `,
    styleUrls: ["./delivery-feedback.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryFeedbackComponent implements OnInit {
    @Input() saPosition: number;
    @Input() deliveryDate: string;
    @Input() deliveryProof: DeliveryProof;

    @Output()
    handleFeedbackClick: EventEmitter<PODFeedbackClickHandlerInfo> = new EventEmitter();

    saContextList: Segment.ContextListItem[];
    selectedAnswer: DeliveryProofFeedbackAnswer;
    feedbackButtonsContextList: Array<Segment.ContextListItem[]>;

    constructor(
        private utilService: UtilService,
        private deliveryProofService: DeliveryProofPageService
    ) {}

    ngOnInit() {
        this.setAnalyticsData();
    }

    _handleAnswerClick(answer: DeliveryProofFeedbackAnswer) {
        if (
            this.utilService.getNestedValue(this.selectedAnswer, "key") ===
            answer.key
        ) {
            return;
        }

        this.selectedAnswer = this.deliveryProofService.resetFeedbackTagStates(
            answer
        );
        this.handleFeedbackClick.emit({
            answer: this.selectedAnswer,
        });
    }

    trackByFn(_: number, answer: DeliveryProofFeedbackAnswer): string {
        return answer.key;
    }

    handleTagClick(tagInfo: PODFeedbackClickHandlerInfo) {
        if (
            tagInfo &&
            tagInfo.hasOwnProperty("tagIndex") &&
            !this.utilService.isEmpty(tagInfo.tag)
        ) {
            this.selectedAnswer.tags.splice(tagInfo.tagIndex, 1, tagInfo.tag);
        }

        this.handleFeedbackClick.emit({
            ...tagInfo,
            answer: this.selectedAnswer,
        });
    }

    private setAnalyticsData() {
        this.saContextList = [
            { name: SA_CONTEXT_NAMES.DELIVERY_DATE, value: this.deliveryDate },
        ];

        const answers: DeliveryProofFeedbackAnswer[] = this.utilService.getNestedValue(
            this.deliveryProof,
            "possible_answers"
        );

        this.feedbackButtonsContextList =
            (this.utilService.isLengthyArray(answers) &&
                answers.map((answer) => {
                    return [
                        {
                            value: this.deliveryDate,
                            name: SA_CONTEXT_NAMES.DELIVERY_DATE,
                        },

                        {
                            value: answer.followup_question,
                            name: SA_CONTEXT_NAMES.FOLLOWUP_QN,
                        },
                    ];
                })) ||
            [];
    }
}
