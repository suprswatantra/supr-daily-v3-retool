import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { UtilService } from "@services/util/util.service";

import {
    Segment,
    PODFeedbackTagComponentInfo,
    PODFeedbackClickHandlerInfo,
} from "@types";

import { SA_CONTEXT_NAMES, SA_OBJECT_NAMES } from "../../constants";

@Component({
    selector: "supr-delivery-proof-feedback-tags",
    template: `
        <div class="contentWrapper" *ngIf="tags?.length">
            <supr-text
                class="headerText"
                type="body"
                saImpression
                *ngIf="question"
                [saPosition]="saPosition"
                [saObjectValue]="question"
                [saContextList]="qnContextList"
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.FOLLOWUP_QN}"
            >
                {{ question }}
            </supr-text>

            <div class="tagsWrapper suprRow wrap">
                <supr-chip
                    [saObjectValue]="tag?.key"
                    [saContextList]="tagContextList"
                    [class.selected]="tag?.selected"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.TAG}"
                    (handleClick)="_handleTagClick(tag, _i)"
                    *ngFor="let tag of tags; index as _i; trackBy: trackByFn"
                >
                    <supr-text type="caption">{{ tag.value }}</supr-text>
                </supr-chip>
            </div>
        </div>
    `,
    styleUrls: ["./feedback-tags.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackTagsComponent implements OnInit {
    @Input() question: string;
    @Input() saPosition: number;
    @Input() deliveryDate: string;
    @Input() tags: PODFeedbackTagComponentInfo[];

    @Output()
    handleTagClick: EventEmitter<PODFeedbackClickHandlerInfo> = new EventEmitter();

    qnContextList: Segment.ContextListItem[];
    tagContextList: Segment.ContextListItem[];

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setAnalyticsData();
    }

    trackByFn(_: number, tag: PODFeedbackTagComponentInfo): string {
        return tag.key;
    }

    _handleTagClick(tag: PODFeedbackTagComponentInfo, tagIndex: number) {
        const currentTag =
            this.utilService.isLengthyArray(this.tags) && this.tags[tagIndex];

        if (!currentTag || currentTag.selected !== tag.selected) {
            return;
        }

        this.handleTagClick.emit({
            tagIndex,
            tag: {
                ...tag,
                selected: !tag.selected,
            },
        });
    }

    private setAnalyticsData() {
        const tags = this.utilService.isLengthyArray(this.tags)
            ? this.tags.map((tag) => tag.key)
            : [];

        this.qnContextList = [
            {
                name: SA_CONTEXT_NAMES.DELIVERY_DATE,
                value: this.deliveryDate,
            },
            {
                name: SA_CONTEXT_NAMES.ATTRIBUTES,
                value: tags,
            },
        ];

        this.tagContextList = [
            {
                value: this.deliveryDate,
                name: SA_CONTEXT_NAMES.DELIVERY_DATE,
            },

            {
                value: this.question,
                name: SA_CONTEXT_NAMES.FOLLOWUP_QN,
            },
        ];
    }
}
