import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ReportPODIssue } from "@types";

@Component({
    selector: "supr-delivery-proof-report-issues",
    template: ` <ng-container *ngIf="issues?.length">
        <div class="suprRow headerTextWrapper">
            <supr-text type="subheading">
                {{ textConstants.REPORT_SELECT_ISSUE }}
            </supr-text>
            <supr-text type="caption" class="caution">
                {{ textConstants.REPORT_ISSUE_MANDATORY }}
            </supr-text>
        </div>

        <ng-container
            *ngFor="let issue of issues; trackBy: trackIssues; index as _i"
        >
            <div class="issueWrapper">
                <supr-line-item-variant-A
                    textType="body"
                    [text]="issue?.value"
                    [iconOnTheLeft]="issue?.icon"
                    [class.selected]="issue?.selected"
                    (handleClick)="handleIssueClick.emit(_i)"
                ></supr-line-item-variant-A>
            </div>
        </ng-container>
    </ng-container>`,
    styleUrls: ["./report-issues.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportIssuesComponent {
    @Input() textConstants: any;
    @Input() issues: Array<ReportPODIssue>;

    @Output() handleIssueClick: EventEmitter<number> = new EventEmitter();

    constructor() {}

    trackIssues(_: number, issue: ReportPODIssue): string {
        return issue.key;
    }
}
