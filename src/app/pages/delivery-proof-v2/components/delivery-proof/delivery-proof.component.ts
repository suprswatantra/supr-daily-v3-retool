import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DeliveryProof } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { PODFeedbackClickHandlerInfo, Segment } from "@types";

import { SA_CONTEXT_NAMES, SA_OBJECT_NAMES } from "../../constants";

@Component({
    selector: "supr-delivery-proof",
    template: `
        <div class="imageWrapper" *ngIf="deliveryProof?.image_url">
            <supr-image
                saImpression
                [useDirectUrl]="true"
                [src]="deliveryProof.image_url"
                [saPosition]="saPosition"
                [saContextList]="saContextList"
                [saObjectValue]="deliveryProof?.primary_question || 'NA'"
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.POD_FEEDBACK}"
            ></supr-image>
        </div>

        <div class="feedbackWrapper">
            <supr-delivery-feedback
                [saPosition]="saPosition"
                [deliveryDate]="deliveryDate"
                [deliveryProof]="deliveryProof"
                (handleFeedbackClick)="handleFeedbackClick.emit($event)"
            >
            </supr-delivery-feedback>
            <div
                saClick
                [saPosition]="saPosition"
                [saContextList]="saContextList"
                class="suprRow reportWrapper center"
                saObjectName="${SA_OBJECT_NAMES.CLICK.REPORT_CTA}"
                (click)="goToReportPage()"
            >
                <supr-text type="paragraph">
                    {{ textConstants.IMAGE_REPORT_PHOTO }}
                </supr-text>
                <supr-icon name="chevron_right"></supr-icon>
            </div>
        </div>
    `,
    styleUrls: ["./delivery-proof.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProofComponent implements OnInit {
    @Input() textConstants: any;
    @Input() saPosition: number;
    @Input() deliveryDate: string;
    @Input() deliverySlot: string;
    @Input() deliveryProof: DeliveryProof;

    @Output()
    handleFeedbackClick: EventEmitter<PODFeedbackClickHandlerInfo> = new EventEmitter();

    saContextList: Segment.ContextListItem[];

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.setAnalyticsData();
    }

    goToReportPage() {
        this.routerService.goToDeliveryProofReportPage({
            queryParams: {
                deliveryDate: this.deliveryDate,
                imageType: this.utilService.getNestedValue(
                    this.deliveryProof,
                    "image_type"
                ),
                slot: this.deliverySlot,
            },
        });
    }

    private setAnalyticsData() {
        this.saContextList = [
            {
                value: this.deliveryDate,
                name: SA_CONTEXT_NAMES.DELIVERY_DATE,
            },
            {
                name: SA_CONTEXT_NAMES.FEEDBACK_STATE,
                value:
                    this.deliveryProof &&
                    this.deliveryProof.primary_question &&
                    this.utilService.isLengthyArray(
                        this.deliveryProof.possible_answers
                    )
                        ? "on"
                        : "off",
            },
        ];
    }
}
