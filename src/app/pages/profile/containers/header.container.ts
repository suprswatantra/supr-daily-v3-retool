import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { ProfilePageAdapter as Adapter } from "@pages/profile/services/profile.adapter";

@Component({
    selector: "supr-profile-header-container",
    template: `
        <supr-profile-header
            [fullName]="fullName$ | async"
            [balance]="walletBalance$ | async"
            [subscriptionCount]="subscriptionCount$ | async"
        >
        </supr-profile-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderContainer implements OnInit {
    fullName$: Observable<string>;
    walletBalance$: Observable<number>;
    subscriptionCount$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.fullName$ = this.adapter.fullName$;
        this.walletBalance$ = this.adapter.walletBalance$;
        this.subscriptionCount$ = this.adapter.subscriptionCount$;
    }
}
