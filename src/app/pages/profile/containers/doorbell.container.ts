import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { UserCurrentState } from "@store/user/user.state";

import { ProfilePageAdapter as Adapter } from "@pages/profile/services/profile.adapter";

@Component({
    selector: "supr-profile-doorbell-container",
    template: `
        <supr-profile-doorbell
            [doorBell]="doorBell$ | async"
            [userState]="userState$ | async"
            [doorBellUpdateError]="userError$ | async"
            (handleUpdateDoorBell)="updateDoorBell($event)"
        >
        </supr-profile-doorbell>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoorBellContainer implements OnInit {
    doorBell$: Observable<boolean>;
    userState$: Observable<UserCurrentState>;
    userError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.doorBell$ = this.adapter.doorBell$;
        this.userState$ = this.adapter.userState$;
        this.userError$ = this.adapter.userError$;
    }

    updateDoorBell(bellRing: boolean) {
        this.adapter.updateUser({ bellRing });
    }
}
