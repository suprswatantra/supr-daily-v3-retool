import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { User } from "@models";

import { UserCurrentState } from "@store/user/user.state";

import { ProfilePageAdapter as Adapter } from "@pages/profile/services/profile.adapter";

@Component({
    selector: "supr-profile-billing-container",
    template: `
        <supr-profile-billing
            [user]="user$ | async"
            [fullName]="fullName$ | async"
            [profileUpdateError]="userError$ | async"
            [userState]="userState$ | async"
            (handleUpdateUser)="updateUser($event)"
        >
        </supr-profile-billing>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BillingContainer implements OnInit {
    user$: Observable<User>;
    userState$: Observable<UserCurrentState>;
    fullName$: Observable<string>;
    userError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.user$ = this.adapter.user$;
        this.fullName$ = this.adapter.fullName$;
        this.userError$ = this.adapter.userError$;
        this.userState$ = this.adapter.userState$;
    }

    updateUser(user: User) {
        this.adapter.updateUser(user);
    }
}
