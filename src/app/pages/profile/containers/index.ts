import { HeaderContainer } from "./header.container";
import { BillingContainer } from "./billing.container";
import { AddressContainer } from "./address.container";
import { DoorBellContainer } from "./doorbell.container";
import { WhatsappContainer } from "./whatsapp.container";

export const profileContainers = [
    HeaderContainer,
    BillingContainer,
    AddressContainer,
    DoorBellContainer,
    WhatsappContainer,
];
