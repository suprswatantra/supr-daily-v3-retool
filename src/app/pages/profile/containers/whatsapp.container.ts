import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { UserCurrentState } from "@store/user/user.state";

import { ProfilePageAdapter as Adapter } from "@pages/profile/services/profile.adapter";

@Component({
    selector: "supr-profile-whatsapp-container",
    template: `
        <supr-profile-whatsapp
            [whatsAppOptIn]="whatsAppOptIn$ | async"
            [userState]="userState$ | async"
            [whatsAppUpdateError]="userError$ | async"
            (handleUpdateWhatsApp)="updateWhatsAppStatus($event)"
        >
        </supr-profile-whatsapp>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhatsappContainer implements OnInit {
    whatsAppOptIn$: Observable<boolean>;
    userState$: Observable<UserCurrentState>;
    userError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.whatsAppOptIn$ = this.adapter.whatsAppOptIn$;
        this.userState$ = this.adapter.userState$;
        this.userError$ = this.adapter.userError$;
    }

    updateWhatsAppStatus(isWhatsAppOptIn: boolean) {
        this.adapter.updateUser({ isWhatsAppOptIn });
    }
}
