import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Address } from "@models";
import { ProfilePageAdapter as Adapter } from "@pages/profile/services/profile.adapter";

@Component({
    selector: "supr-profile-address-container",
    template: `
        <supr-profile-address
            [address]="address$ | async"
            [hasAddress]="hasAddress$ | async"
        ></supr-profile-address>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressContainer implements OnInit {
    address$: Observable<Address>;
    hasAddress$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.address$ = this.adapter.address$;
        this.hasAddress$ = this.adapter.hasAddress$;
    }
}
