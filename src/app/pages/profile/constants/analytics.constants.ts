export const SA_OBJECT_NAMES = {
    CLICK: {
        LOGOUT_BUTTON: "logout-button",
        LOGOUT_CONFIRM_BUTTON: "logout-confirm-button",
        DOOR_BELL_SETTING_UPDATE: "door-bell-setting-update",
        DOOR_BELL_SETTING_TOGGLE: "door-bell-setting-toggle",
        UPDATE_ADDRESS: "update-address",
    },
};
