export const TITLE_TEXTS = {
    BULDING: "Profile details",
    ADDRESS: "My address",
    PRIVACY: "Privacy policy",
    COOKIE: "Cookie policy",
    LOGOUT: "Logout",
    DOOR_BELL: "Doorbell settings",
    WHATSAPP: "WhatsApp notifications",
};

export const POLICY_URL = {
    PRIVACY: "http://www.suprdaily.com/policies#privacy-policy",
    COOKIE: "http://www.suprdaily.com/policies#cookie-policy",
};

export const TEXTS = {
    NO_ADDRESS_TEXT: "No address present",
    NO_ADDRESS_SUUBTEXT: "Add your address to help us deliver",
    NO_INSTRUCTION_TEXT: "No instruction added",
    RESIDENCE_TYPE_TEXT: "Apartment / Flat",
    PROVIDE_MORE_DETAILS:
        "Please provide us with a few more details about yourself.",
    COMPLETE_SIGN_UP: "Complete Sign-Up",
    individualHouse: "Independent House",
    knownSociety: "Apartment or Flat",
    unknownSociety: "Apartment or Flat",
    DO_NOT_RING_BELL: "Do not ring the doorbell.",
    RING_BELL: "Ring the doorbell.",
    DOOR_BELL_CONTENT: "Should we ring the doorbell every time we deliver?",
    UPDATE: "Update",
    YES: "YES",
    NO: "NO",
    RECEIVE_UPDATE_ON_WHATSAPP: "Receive updates on WhatsApp",
    DO_NOT_RECEIVE_UPDATE_ON_WHATSAPP: "Do not send updates on WhatsApp",
};

export const STATIC_MAP = {
    MARKER_COLOR: "red",
    URL: "https://maps.googleapis.com/maps/api/staticmap",
    ZOOM: 15,
    PADDING: 48,
    RELATIVE_DIVIDE_VALUE: 3.25,
    MAP_TYPE: "roadmap",
};

export const DISPLAY_TEXTS = {
    CHANGE_RESIDENCE_TYPE:
        "Changing residence type will clear your address and location.",
    CHANGE_RESIDENCE_TYPE_CONFIRM:
        "Are you sure you want to change your residence type?",
};
