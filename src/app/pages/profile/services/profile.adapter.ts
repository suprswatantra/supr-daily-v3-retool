import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { User } from "@models";

import {
    StoreState,
    WalletStoreSelectors,
    SubscriptionStoreSelectors,
    AddressStoreSelectors,
    UserStoreSelectors,
    UserStoreActions,
} from "@supr/store";

@Injectable()
export class ProfilePageAdapter {
    userState$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCurrentState)
    );

    fullName$ = this.store.pipe(select(UserStoreSelectors.selectUserFullName));

    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    address$ = this.store.pipe(select(AddressStoreSelectors.selectAddress));

    userError$ = this.store.pipe(select(UserStoreSelectors.selectUserError));

    hasAddress$ = this.store.pipe(
        select(AddressStoreSelectors.selectHasAddress)
    );

    doorBell$ = this.store.pipe(select(UserStoreSelectors.selectDoorBell));

    whatsAppOptIn$ = this.store.pipe(
        select(UserStoreSelectors.selectWhatsAppOptIn)
    );

    walletBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletBalance)
    );

    subscriptionCount$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectActiveSubscriptionCount)
    );

    constructor(private store: Store<StoreState>) {}

    updateUser(user: User) {
        this.store.dispatch(
            new UserStoreActions.UpdateProfileRequestAction({ user })
        );
    }
}
