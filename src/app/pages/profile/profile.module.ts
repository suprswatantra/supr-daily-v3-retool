import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { CallNumber } from "@ionic-native/call-number/ngx";
import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { ProfilePageAdapter } from "./services/profile.adapter";

import { profileComponents } from "./components";
import { profileContainers } from "./containers";
import { ProfilePageLayoutComponent } from "./layouts/profile.layout";

const routes: Routes = [
    {
        path: "",
        component: ProfilePageLayoutComponent,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...profileComponents,
        ...profileContainers,
        ProfilePageLayoutComponent,
    ],
    providers: [CallNumber, ProfilePageAdapter],
})
export class ProfilePageModule {}
