import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-profile-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="body">My account</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-profile-header-container></supr-profile-header-container>
                <supr-profile-content></supr-profile-content>
            </div>
        </div>
    `,
    styleUrls: ["../styles/profile.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilePageLayoutComponent {}
