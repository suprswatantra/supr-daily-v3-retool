import { HeaderComponent } from "./header/header.component";

import { ContentComponent } from "./content/content.component";
import { ContentItemComponent } from "./content/content-item.component";

import { AddressComponent } from "./address/address.component";
import { AddressDetailsComponent } from "./address/address.details.component";
import { AddressDetailsInputComponent } from "./address/address.details-input.component";

import { BillingComponent } from "./billing/billing.component";
import { BillingModalComponent } from "./billing/billing-modal.component";

import { LogoutComponent } from "./logout/logout.component";
import { LogoutModalComponent } from "./logout/logout-modal.component";

import { PolicyComponent } from "./policy/policy.component";

import { DoorBellModalComponent } from "./doorbell/doorbell-modal.component";
import { DoorBellComponent } from "./doorbell/doorbell.component";
import { WhatsAppComponent } from "./whatsapp/whatsapp.component";

export const profileComponents = [
    ContentComponent,
    HeaderComponent,
    ContentItemComponent,
    AddressComponent,
    BillingComponent,
    LogoutComponent,
    LogoutModalComponent,
    PolicyComponent,
    BillingModalComponent,
    AddressDetailsComponent,
    AddressDetailsInputComponent,
    DoorBellModalComponent,
    DoorBellComponent,
    WhatsAppComponent,
];
