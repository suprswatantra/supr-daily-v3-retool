import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
    OnInit,
} from "@angular/core";

import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-profile-header",
    template: `
        <div class="header">
            <div class="suprColumn center">
                <div class="headerIcon">
                    <supr-icon name="profile"></supr-icon>
                </div>
                <div class="divider8"></div>

                <supr-text type="subheading">{{ fullName }}</supr-text>
                <div class="divider16"></div>

                <div class="suprRow">
                    <supr-chip (handleClick)="goToSubscriptionListPage()">
                        <supr-text type="paragraph">
                            {{ subscriptionCountStr }}
                        </supr-text>
                    </supr-chip>

                    <div class="spacer16"></div>

                    <supr-chip (handleClick)="goToWalletPage()">
                        <supr-text type="paragraph">
                            {{ balance || 0 | rupee }} Balance
                        </supr-text>
                    </supr-chip>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnChanges {
    @Input() fullName: string;
    @Input() balance: number;
    @Input() subscriptionCount: number;

    constructor(private routerService: RouterService) {}

    subscriptionCountStr: string;

    ngOnInit() {
        this.setSubscriptionCountStr();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["subscriptionCount"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.setSubscriptionCountStr();
        }
    }

    goToWalletPage() {
        this.routerService.goToWalletPage();
    }

    goToSubscriptionListPage() {
        this.routerService.goToSubscriptionListPage();
    }

    private setSubscriptionCountStr() {
        const pluralStr = this.subscriptionCount > 1 ? "s" : "";
        this.subscriptionCountStr = `${this.subscriptionCount} Subscription${pluralStr}`;
    }
}
