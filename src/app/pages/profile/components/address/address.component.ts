import {
    Input,
    OnInit,
    OnChanges,
    Component,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    OnDestroy,
} from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { MODAL_NAMES, PAGE_ROUTES } from "@constants";

import { Address } from "@models";

import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import { TITLE_TEXTS, TEXTS } from "@pages/profile/constants";

@Component({
    selector: "supr-profile-address",
    template: `
        <supr-profile-content-item
            leftIcon="location"
            rightIcon="chevron_right"
            [title]="TEXTS?.ADDRESS"
            (handleItemClick)="itemClick()"
        >
            <supr-text type="paragraph">
                {{ addressText }}
            </supr-text>
        </supr-profile-content-item>

        <supr-modal
            *ngIf="showModal"
            #suprModal
            [animate]="modalAnimate"
            (handleClose)="closeModal()"
            modalName="${MODAL_NAMES.EDIT_ADDRESS}"
        >
            <address-modal-content
                *ngIf="!isAlertShow"
                [address]="address"
                (closeModal)="suprModal.closeModal()"
                (handleUpdateAddress)="updateAddress()"
            ></address-modal-content>
        </supr-modal>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressComponent implements OnInit, OnChanges, OnDestroy {
    @Input() set address(data: Address) {
        this._address = data;
    }
    get address(): Address {
        return this._address;
    }
    @Input() hasAddress: boolean;

    showModal = false;
    modalAnimate = true;

    TEXTS = TITLE_TEXTS;
    addressText: string;
    addressSubtext: string;

    private _address: Address;
    private routerSub: Subscription;

    constructor(
        private router: Router,
        private routerService: RouterService,
        private addressService: AddressService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.setAddress(this.address);
        this.subscribeToRouterEvents();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["address"];
        if (change) {
            if (!change.firstChange && change.currentValue) {
                this.setAddress(change.currentValue);
                this.addressService.setInitialAddressData(change.currentValue);
                this.addressService.setInitialFormData(change.currentValue);
            }
        }
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    itemClick(animate = true) {
        if (this.hasAddress) {
            this.modalAnimate = animate;
            this.showModal = true;
        } else {
            this.goToAddressPage();
        }
    }

    goToAddressPage() {
        this.closeModal();
        this.routerService.goToAddressMapPage();
    }

    updateAddress() {
        this.closeModal();
        this.routerService.goToAddressMapPage();
    }

    closeModal() {
        this.showModal = false;
        this.cdr.detectChanges();
    }

    private setAddress(address: Address) {
        if (this.hasAddress) {
            this.addressText = this.addressService.getAddressText(address);
        } else {
            this.addressText = TEXTS.NO_ADDRESS_TEXT;
            this.addressSubtext = TEXTS.NO_ADDRESS_SUUBTEXT;
        }
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isProfilePageUrl(event.url)
                )
            )
            .subscribe(() => {
                if (this.address) {
                    this.addressService.setInitialAddressData(this.address);
                    this.addressService.setInitialFormData(this.address);
                }
            });
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    private isProfilePageUrl(url: string): boolean {
        return url && url.includes(PAGE_ROUTES.PROFILE.PATH);
    }
}
