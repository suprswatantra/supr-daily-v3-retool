import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";
import { Address } from "@shared/models";
import { SA_OBJECT_NAMES } from "@pages/profile/constants/analytics.constants";

@Component({
    selector: "address-modal-content",
    template: `
        <div class="modal">
            <supr-address-summary [address]="address"></supr-address-summary>
            <div class="divider24"></div>
            <supr-button
                (handleClick)="handleUpdateAddress.emit()"
                saObjectName="${SA_OBJECT_NAMES.CLICK.UPDATE_ADDRESS}"
            >
                <supr-text type="body">
                    Update
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/address.details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressDetailsComponent {
    @Input() address: Address;
    @Output() handleUpdateAddress: EventEmitter<string> = new EventEmitter();
}
