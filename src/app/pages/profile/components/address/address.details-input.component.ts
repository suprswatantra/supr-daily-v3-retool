import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "address-modal-input-content",
    template: `
        <div class="input" [class.isBorderBottom]="isLast">
            <div class="suprRow spaceBetween">
                <supr-text type="body" class="header">
                    {{ title }}
                </supr-text>
                <supr-icon [name]="icon"></supr-icon>
            </div>
            <div class="suprRow inputContent">
                <supr-text type="body"> {{ content }} </supr-text>
            </div>

            <div class="suprRow">
                <ng-content></ng-content>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/address.details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressDetailsInputComponent {
    @Input() title: string;
    @Input() icon: string;
    @Input() content: string;
    @Input() isLast = false;
}
