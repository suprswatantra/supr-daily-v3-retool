import {
    Component,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    OnInit,
    ChangeDetectorRef,
    SimpleChange,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { CallNumber } from "@ionic-native/call-number/ngx";

import {
    SETTINGS_KEYS_DEFAULT_VALUE,
    PROFILE_VALIDATION_TEXTS,
    TOAST_MESSAGES,
} from "@constants";

import { User } from "@models";

import { ModalService } from "@services/layout/modal.service";
import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { MoengageService } from "@services/integration/moengage.service";
import { SettingsService } from "@services/shared/settings.service";

import { UserState, UserCurrentState } from "@store/user/user.state";

import { ProfileFormError } from "@types";

@Component({
    selector: "supr-profile-billing-modal",
    template: `
        <div class="modal">
            <supr-text type="subtitle" class="title">
                Profile details
            </supr-text>
            <div class="divider16"></div>

            <supr-profile-form
                [user]="user"
                [formErrors]="formErrors"
                (handleUpdateFormData)="updateFormData($event)"
            ></supr-profile-form>

            <div class="divider16"></div>

            <div class="modalInput">
                <supr-input-label
                    label="Phone number"
                    placeholder="Mobile number"
                    disabled="true"
                    [value]="user?.mobileNumber"
                ></supr-input-label>
            </div>

            <div class="modalInfo">
                <supr-text type="body">
                    To change your phone number,
                    <span (click)="openDialer()">Call us</span>
                </supr-text>
            </div>

            <supr-button
                [disabled]="isBtnDisabled"
                [loading]="isLoading"
                (handleClick)="updateProfile()"
            >
                <supr-text type="body">
                    Update
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/billing.details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BillingModalComponent implements OnInit, OnChanges {
    @Input() user: User;
    @Input() userState: UserState;
    @Input() name: string;
    @Input() profileUpdateError: any;

    @Output() closeModal: EventEmitter<TouchEvent> = new EventEmitter();
    @Output() handleUpdateUser: EventEmitter<User> = new EventEmitter();

    VALIDATION_TEXTS = PROFILE_VALIDATION_TEXTS;
    formErrors: ProfileFormError = {};
    formData: User = {};
    isBtnDisabled = true;
    isLoading = false;

    constructor(
        private ms: ModalService,
        private cdr: ChangeDetectorRef,
        private callNumber: CallNumber,
        private utilService: UtilService,
        private toastService: ToastService,
        private moengageService: MoengageService,
        private settingsService: SettingsService
    ) {}

    _closeModal() {
        this.ms.closeModal();
    }

    ngOnInit() {
        if (this.user) {
            this.formData = this.user;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["userState"];

        if (this.canHandleProfileStateChange(change)) {
            this.handleProfileChange(change);
        }
    }

    private fetchsupportNumber() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        return (
            (supportConfig && supportConfig.number) ||
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig.number
        );
    }

    openDialer() {
        const number = this.fetchsupportNumber();

        const phoneNo = number;
        this.callNumber.callNumber(phoneNo, false).catch(() => {
            // Open the dialer - this doesn't need to seek any permission
            window.location.href = `tel:${phoneNo}`;
        });
    }

    updateProfile() {
        if (!this.isFormValid()) {
            return;
        }

        this.isLoading = true;
        this.handleUpdateUser.emit(this.formData);
    }

    updateFormData(formData: User) {
        this.formData = { ...this.formData, ...formData };
        this.setBtnDisabled(false);
    }

    private isFormValid(): boolean {
        const nameErrMsg = this.checkNameField();
        const emailErrMsg = this.checkEmailField();

        this.formErrors = {
            name: nameErrMsg,
            email: emailErrMsg,
        };

        this.cdr.detectChanges();

        return !nameErrMsg.length && !emailErrMsg.length;
    }

    private checkNameField(): string {
        const { firstName = "", lastName = "" } = this.formData;

        const name = `${firstName} ${lastName}`.trim();
        if (!firstName.length) {
            return this.VALIDATION_TEXTS.NAME_EMPTY;
        } else if (!this.utilService.isValidName(name)) {
            return this.VALIDATION_TEXTS.NAME_INVALID;
        }

        return "";
    }

    private checkEmailField(): string {
        const email = this.formData.email;
        if (!email || !email.length) {
            return this.VALIDATION_TEXTS.EMAIL_EMPTY;
        } else if (!this.utilService.isValidEmail(email)) {
            return this.VALIDATION_TEXTS.EMAIL_INVALID;
        }

        return "";
    }

    private setBtnDisabled(value: boolean) {
        this.isBtnDisabled = value;
    }

    private canHandleProfileStateChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private handleProfileChange(change: SimpleChange) {
        if (
            change.previousValue === UserCurrentState.UPDATING_USER &&
            change.currentValue === UserCurrentState.NO_ACTION
        ) {
            this.hideLoader();

            if (!this.profileUpdateError) {
                this.moengageService.setUserContext(this.user);
                this.handleUpdateUserSuccess();
            } else {
                this.handleUpdateUserError();
            }
        }
    }

    private handleUpdateUserSuccess() {
        this.showSuccessToast();
        this._closeModal();
    }

    private handleUpdateUserError() {
        this.showErrorToast();
    }

    private hideLoader() {
        this.isLoading = false;
    }

    private showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.PROFILE.SUCCESS);
    }

    private showErrorToast() {
        this.toastService.present(TOAST_MESSAGES.PROFILE.ERROR);
    }
}
