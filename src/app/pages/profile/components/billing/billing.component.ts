import {
    Component,
    Output,
    EventEmitter,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CallNumber } from "@ionic-native/call-number/ngx";

import { MODAL_NAMES, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { User } from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { UserCurrentState } from "@store/user/user.state";

import { TITLE_TEXTS } from "@pages/profile/constants";

@Component({
    selector: "supr-profile-billing",
    template: `
        <div class="billing">
            <supr-profile-content-item
                leftIcon="profile"
                rightIcon="chevron_right"
                [title]="TEXTS?.BULDING"
                (handleItemClick)="billingItemClick()"
            >
                <div class="divider8"></div>
                <supr-text type="paragraph">
                    {{ fullName }}, {{ user?.mobileNumber }}
                </supr-text>
                <div class="divider8"></div>
                <supr-text type="paragraph">
                    {{ user?.email }}
                </supr-text>
            </supr-profile-content-item>

            <supr-modal
                *ngIf="showModal"
                [animate]="modalAnimate"
                (handleClose)="_closeModal()"
                (handleOpenDialer)="openDialer()"
                modalName="${MODAL_NAMES.BILLING}"
            >
                <supr-profile-billing-modal
                    (closeModal)="suprModal.closeModal()"
                    (handleUpdateUser)="updateUser($event)"
                    [user]="user"
                    [name]="fullName"
                    [profileUpdateError]="profileUpdateError"
                    [userState]="userState"
                ></supr-profile-billing-modal>
            </supr-modal>
        </div>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BillingComponent {
    @Input() user: User;
    @Input() userState: UserCurrentState;
    @Input() fullName: string;
    @Input() profileUpdateError: any;

    @Output() handleBillingItemClick: EventEmitter<void> = new EventEmitter();
    @Output() handleUpdateUser: EventEmitter<User> = new EventEmitter();

    constructor(
        private callNumber: CallNumber,
        private settingsService: SettingsService
    ) {}

    TEXTS = TITLE_TEXTS;

    showModal = false;
    modalAnimate = true;

    billingItemClick(animate = true) {
        this.modalAnimate = animate;
        this.showModal = true;
    }

    _closeModal() {
        this.showModal = false;
    }

    private fetchsupportNumber() {
        const supportConfig = this.settingsService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        return (
            (supportConfig && supportConfig.number) ||
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig.number
        );
    }

    openDialer() {
        const number = this.fetchsupportNumber();

        const phoneNo = number;
        this.callNumber.callNumber(phoneNo, false).catch(() => {
            // Open the dialer - this doesn't need to seek any permission
            window.location.href = `tel:${phoneNo}`;
        });
    }

    updateUser(user: User) {
        this.handleUpdateUser.emit(user);
    }
}
