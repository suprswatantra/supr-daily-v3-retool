import { Component, ChangeDetectionStrategy } from "@angular/core";
import { TITLE_TEXTS, POLICY_URL } from "@pages/profile/constants";

@Component({
    selector: "supr-profile-policy",
    template: `
        <supr-profile-content-item
            leftIcon="error"
            rightIcon="chevron_right"
            [title]="TEXTS?.PRIVACY"
            (handleItemClick)="privacyItemClick()"
        >
        </supr-profile-content-item>

        <supr-profile-content-item
            leftIcon="error"
            rightIcon="chevron_right"
            [title]="TEXTS?.COOKIE"
            (handleItemClick)="cookieItemClick()"
        >
        </supr-profile-content-item>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PolicyComponent {
    TEXTS = TITLE_TEXTS;

    privacyItemClick() {
        window.open(POLICY_URL.PRIVACY, "_system", "location=yes");
    }

    cookieItemClick() {
        window.open(POLICY_URL.COOKIE, "_system", "location=yes");
    }
}
