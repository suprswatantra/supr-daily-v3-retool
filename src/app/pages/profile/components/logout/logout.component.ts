import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TOAST_MESSAGES } from "@constants";

import { AuthService } from "@services/data/auth.service";
import { ModalService } from "@services/layout/modal.service";
import { ToastService } from "@services/layout/toast.service";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-profile-logout",
    template: `
        <div class="logout">
            <supr-profile-content-item
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.LOGOUT_BUTTON}"
                leftIcon="logout"
                title="Logout"
                (handleItemClick)="confirmLogout()"
            >
            </supr-profile-content-item>
        </div>

        <supr-modal *ngIf="showModal" (handleClose)="closeModal()">
            <supr-profile-logout-modal
                (handleLogout)="logout()"
            ></supr-profile-logout-modal>
        </supr-modal>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoutComponent {
    showModal = false;

    constructor(
        private authService: AuthService,
        private modalService: ModalService,
        private toastService: ToastService
    ) {}

    confirmLogout() {
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    logout() {
        this.modalService.closeModal();
        this.authService.logout();
        this.toastService.present(TOAST_MESSAGES.AUTH.LOGOUT_SUCCESS_TEXT, {
            duration: 1000,
        });
    }
}
