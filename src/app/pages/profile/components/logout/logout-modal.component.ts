import {
    Component,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-profile-logout-modal",
    template: `
        <div class="logout modal">
            <supr-text type="subtitle">Logout</supr-text>
            <div class="divider4"></div>

            <supr-text class="body" type="body">
                Are you sure you want to logout?
            </supr-text>
            <div class="divider16"></div>

            <supr-button
                saObjectName="${SA_OBJECT_NAMES.CLICK.LOGOUT_CONFIRM_BUTTON}"
                (handleClick)="handleLogout.emit()"
            >
                <supr-text type="body">
                    Logout
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoutModalComponent {
    @Output() handleLogout: EventEmitter<void> = new EventEmitter();
}
