import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-profile-content-item",
    template: `
        <div class="suprRow item" (click)="handleItemClick.emit()">
            <div class="suprColumn stretch top left itemLeft">
                <supr-icon [name]="leftIcon"></supr-icon>
            </div>

            <div class="spacer16"></div>

            <div class="suprColumn stretch top left itemContent">
                <supr-text type="body" class="title">{{ title }}</supr-text>
                <ng-content></ng-content>
            </div>

            <div class="suprColumn stretch top right itemRight">
                <supr-icon [name]="rightIcon"></supr-icon>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentItemComponent {
    @Input() leftIcon: string;
    @Input() rightIcon: string;
    @Input() title: string;

    @Output() handleItemClick: EventEmitter<void> = new EventEmitter();
}
