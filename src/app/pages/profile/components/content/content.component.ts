import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-profile-content",
    template: `
        <div class="wrapper">
            <supr-profile-billing-container></supr-profile-billing-container>
            <supr-profile-address-container></supr-profile-address-container>
            <supr-profile-doorbell-container></supr-profile-doorbell-container>
            <supr-profile-whatsapp-container></supr-profile-whatsapp-container>
            <supr-profile-policy></supr-profile-policy>
            <supr-profile-logout></supr-profile-logout>
        </div>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent {}
