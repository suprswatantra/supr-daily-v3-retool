import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { TITLE_TEXTS, TEXTS } from "@pages/profile/constants";

import { UserCurrentState } from "@store/user/user.state";

@Component({
    selector: "supr-profile-doorbell",
    template: `
        <supr-profile-content-item
            [leftIcon]="doorbellIcon"
            rightIcon="chevron_right"
            title="${TITLE_TEXTS.DOOR_BELL}"
            (handleItemClick)="openModal()"
        >
            <supr-text type="paragraph">
                {{ doorBell ? TEXTS.RING_BELL : TEXTS.DO_NOT_RING_BELL }}
            </supr-text>
        </supr-profile-content-item>

        <supr-modal
            *ngIf="showModal"
            (handleClose)="closeModal()"
            modalName="${MODAL_NAMES.DOOR_BELL}"
        >
            <supr-profile-modal-doorbell
                [doorBell]="doorBell"
                [userState]="userState"
                [doorBellUpdateError]="doorBellUpdateError"
                (handleUpdateDoorBell)="handleUpdateDoorBell.emit($event)"
            ></supr-profile-modal-doorbell>
        </supr-modal>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoorBellComponent {
    @Input()
    set doorBell(val: boolean) {
        this._doorBell = val;
        this.setIcon();
    }

    get doorBell(): boolean {
        return this._doorBell;
    }

    @Input() userState: UserCurrentState;
    @Input() doorBellUpdateError: any;
    @Output() handleUpdateDoorBell: EventEmitter<boolean> = new EventEmitter();

    TEXTS = TEXTS;

    doorbellIcon = "no_bell";
    showModal = false;

    private _doorBell = false;

    closeModal() {
        this.showModal = false;
    }

    openModal() {
        this.showModal = true;
    }

    private setIcon() {
        this.doorbellIcon = this.doorBell ? "bell" : "no_bell";
    }
}
