import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    SimpleChanges,
    OnChanges,
    SimpleChange,
} from "@angular/core";
import { TOAST_MESSAGES } from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { ToastService } from "@services/layout/toast.service";

import { UserState, UserCurrentState } from "@store/user/user.state";
import { TITLE_TEXTS, TEXTS } from "@pages/profile/constants";
import { SA_OBJECT_NAMES } from "@pages/profile/constants/analytics.constants";

@Component({
    selector: "supr-profile-modal-doorbell",
    template: `
        <div class="modal">
            <supr-text type="subtitle">${TITLE_TEXTS.DOOR_BELL}</supr-text>
            <div class="divider16"></div>

            <div class="suprRow spaceBetween">
                <supr-text type="body" class="description">
                    {{ TEXTS.DOOR_BELL_CONTENT }}
                </supr-text>

                <div
                    class="suprRow modalToggle"
                    (click)="toggleRingBell()"
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.CLICK
                        .DOOR_BELL_SETTING_TOGGLE}"
                    [saObjectValue]="!ringBell"
                >
                    <div
                        class="suprColumn center modalToggleBtn"
                        [class.active]="ringBell"
                    >
                        <supr-text type="paragraph">{{ TEXTS.YES }}</supr-text>
                    </div>

                    <div
                        class="suprColumn center modalToggleBtn modalToggleBtnRight"
                        [class.active]="!ringBell"
                    >
                        <supr-text type="paragraph">{{ TEXTS.NO }}</supr-text>
                    </div>
                </div>
            </div>
            <div class="divider24"></div>

            <supr-button
                [loading]="isLoading"
                [disabled]="doorBell === ringBell"
                (handleClick)="updateDoorBell()"
                saObjectName="${SA_OBJECT_NAMES.CLICK.DOOR_BELL_SETTING_UPDATE}"
                [saObjectValue]="ringBell"
            >
                <supr-text type="body">{{ TEXTS.UPDATE }}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/doorbell.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoorBellModalComponent implements OnInit, OnChanges {
    @Input() doorBell: boolean;
    @Input() userState: UserState;
    @Input() doorBellUpdateError: any;
    @Output() handleUpdateDoorBell: EventEmitter<boolean> = new EventEmitter();

    TEXTS = TEXTS;
    isLoading = false;
    ringBell = false;

    constructor(
        private modalService: ModalService,
        private toastService: ToastService
    ) {}

    ngOnInit() {
        this.ringBell = this.doorBell;
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["userState"];

        if (this.canHandleProfileStateChange(change)) {
            this.handleProfileChange(change);
        }
    }

    toggleRingBell() {
        this.ringBell = !this.ringBell;
    }

    updateDoorBell() {
        this.showLoader();
        this.handleUpdateDoorBell.emit(this.ringBell);
    }

    private canHandleProfileStateChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private handleProfileChange(change: SimpleChange) {
        if (
            change.previousValue === UserCurrentState.UPDATING_USER &&
            change.currentValue === UserCurrentState.NO_ACTION
        ) {
            if (!this.doorBellUpdateError) {
                this.handleUpdateDoorBellSuccess();
            } else {
                this.hideLoader();
                this.handleUpdateDoorBellError();
            }
        }
    }

    private handleUpdateDoorBellSuccess() {
        this.showSuccessToast();
        this.modalService.closeModal();
    }

    private handleUpdateDoorBellError() {
        this.showErrorToast();
    }

    private showLoader() {
        this.isLoading = true;
    }

    private hideLoader() {
        this.isLoading = false;
    }

    private showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.DOORBELL.SUCCESS);
    }

    private showErrorToast() {
        this.toastService.present(TOAST_MESSAGES.DOORBELL.ERROR);
    }
}
