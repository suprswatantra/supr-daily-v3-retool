import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { MODAL_NAMES, TOAST_MESSAGES } from "@constants";

import { ToastService } from "@services/layout/toast.service";
import { UserCurrentState } from "@store/user/user.state";

import { TITLE_TEXTS, TEXTS } from "@pages/profile/constants";

@Component({
    selector: "supr-profile-whatsapp",
    template: `
        <supr-profile-content-item
            [leftIcon]="optInIcon"
            rightIcon="chevron_right"
            title="${TITLE_TEXTS.WHATSAPP}"
            (handleItemClick)="openModal()"
        >
            <supr-text type="paragraph">
                {{
                    whatsAppOptIn
                        ? TEXTS.RECEIVE_UPDATE_ON_WHATSAPP
                        : TEXTS.DO_NOT_RECEIVE_UPDATE_ON_WHATSAPP
                }}
            </supr-text>
        </supr-profile-content-item>

        <supr-modal
            *ngIf="showModal"
            (handleClose)="closeModal()"
            modalName="${MODAL_NAMES.WHATSAPP_OPT_IN}"
        >
            <supr-whatsapp
                [whatsAppOptIn]="whatsAppOptIn"
                [isLoading]="isLoading"
                (handleUpdateWhatsApp)="updateWhatsApp($event)"
            ></supr-whatsapp>
        </supr-modal>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhatsAppComponent implements OnChanges {
    @Input()
    set whatsAppOptIn(val: boolean) {
        this._whatsAppOptIn = val;
        this.setIcon();
    }

    get whatsAppOptIn(): boolean {
        return this._whatsAppOptIn;
    }

    @Input() userState: UserCurrentState;
    @Input() whatsAppUpdateError: any;
    @Output() handleUpdateWhatsApp: EventEmitter<boolean> = new EventEmitter();

    readonly TEXTS = TEXTS;

    constructor(private toastService: ToastService) {}

    optInIcon = "comment";
    showModal = false;
    isLoading = false;
    tempWhatsUpStatus = null;

    private _whatsAppOptIn = false;

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["userState"];

        if (this.canHandleProfileStateChange(change)) {
            this.handleProfileChange(change);
        }
    }

    closeModal() {
        this.showModal = false;
    }

    openModal() {
        this.showModal = true;
    }

    updateWhatsApp(status: boolean) {
        this.tempWhatsUpStatus = status;
        this.handleUpdateWhatsApp.emit(status);
    }

    private setIcon() {
        this.optInIcon = this.whatsAppOptIn ? "comment" : "comment_slash";
    }

    private canHandleProfileStateChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private handleProfileChange(change: SimpleChange) {
        if (
            change.previousValue === UserCurrentState.UPDATING_USER &&
            change.currentValue === UserCurrentState.NO_ACTION
        ) {
            if (this._whatsAppOptIn === this.tempWhatsUpStatus) {
                if (!this.whatsAppUpdateError) {
                    this.handleUpdateWhatsappOptInSuccess();
                } else {
                    this.hideLoader();
                    this.handleUpdateWhatsappOptInError();
                }

                this.tempWhatsUpStatus = null;
            }
        }
    }

    private hideLoader() {
        this.isLoading = false;
    }

    private handleUpdateWhatsappOptInSuccess() {
        this.showSuccessToast();
        this.showModal = false;
    }

    private handleUpdateWhatsappOptInError() {
        this.showErrorToast();
    }

    private showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.WHATSAPP_OPT_IN.SUCCESS);
    }

    private showErrorToast() {
        this.toastService.present(TOAST_MESSAGES.WHATSAPP_OPT_IN.ERROR);
    }
}
