export const TEXTS = {
    WELCOME_TO_SUPR: "Welcome to Supr!",
    PROVIDE_MORE_DETAILS:
        "Please provide us with a few more details about yourself.",
    COMPLETE_SIGN_UP: "Complete sign-up",
};

export const COPY_BLOCK = {
    copyText: {
        text: null,
        textColor: "var(--primary-140)",
        fontSize: "16px",
    },
    bgColor: "#ECFBFA",
    iconColor: "#ABE1DE",
    borderColor: "#50C1BA",
};
