import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
    SimpleChange,
    ChangeDetectorRef,
} from "@angular/core";

import {
    PROFILE_VALIDATION_TEXTS,
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_OBJECT_VALUES,
} from "@constants";

import { User } from "@models";

import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { MoengageService } from "@services/integration/moengage.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { UserCurrentState } from "@store/user/user.state";

import { ProfileFormError } from "@types";

import { TEXTS, COPY_BLOCK } from "../constants";

@Component({
    selector: "supr-register-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-svg></supr-svg>
            </supr-page-header>

            <div class="suprScrollContent">
                <div class="divider24"></div>
                <supr-text type="heading">
                    {{ TEXTS.WELCOME_TO_SUPR }}
                </supr-text>
                <div class="divider4"></div>

                <supr-text type="caption" class="subHeading">
                    {{ TEXTS.PROVIDE_MORE_DETAILS }}
                </supr-text>
                <div class="divider16"></div>

                <supr-profile-form
                    [user]="user"
                    [formErrors]="formErrors"
                    [showReferralBlock]="true"
                    [prePopulatedReferralCode]="prePopulatedReferralCode"
                    (handleResetPrePopulatedReferralCode)="
                        handleResetPrePopulatedReferralCode.emit()
                    "
                    (handleUpdateFormData)="updateFormData($event)"
                ></supr-profile-form>
            </div>

            <supr-page-footer>
                <supr-button
                    [loading]="updating"
                    (handleClick)="updateProfile()"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.CONTINUE}"
                    saObjectValue="${ANALYTICS_OBJECT_VALUES.ACTIVE}"
                    [saContext]="formData?.referralCode"
                >
                    <supr-text type="body">
                        {{ TEXTS.COMPLETE_SIGN_UP }}
                    </supr-text>
                </supr-button>
            </supr-page-footer>

            <!-- Referral Success modal -->
            <ng-container>
                <supr-referral-success
                    [showModal]="showReferralSuccessModal"
                    [imgUrl]="user?.referralBenefit?.imgUrl"
                    [title]="user?.referralBenefit?.title"
                    [description]="user?.referralBenefit?.description"
                    [copyBlock]="copyBlock"
                    (handleClose)="closeReferralSuccessModal()"
                ></supr-referral-success>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/register.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterPageLayoutComponent implements OnChanges {
    @Input() user: User;
    @Input() userState: UserCurrentState;
    @Input() userUpdateError: any;
    @Input() prePopulatedReferralCode: string;

    @Output() handleUpdateUser: EventEmitter<User> = new EventEmitter();
    @Output() handleResetPrePopulatedReferralCode: EventEmitter<
        void
    > = new EventEmitter();

    TEXTS = TEXTS;
    VALIDATION_TEXTS = PROFILE_VALIDATION_TEXTS;
    updating = false;
    formErrors: ProfileFormError = {};
    formData: User = {};
    showReferralSuccessModal = false;

    copyBlock = COPY_BLOCK;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService,
        private appsFlyerService: AppsFlyerService,
        private moengageService: MoengageService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["userState"];
        if (this.canHandleUserStateChange(change)) {
            if (change.currentValue === UserCurrentState.UPDATING_USER) {
                this.updating = true;
            } else if (!this.userUpdateError) {
                this.sendAppsFlyerEvent();
                this.moengageService.trackProfileCompleted({
                    referral_successful: this.isReferralSuccessful(),
                    referral_code: this.user && this.user.referralCode,
                });

                if (this.isReferralSuccessful()) {
                    this.openReferralSuccessModal();
                    return;
                }

                this.routerService.goToCitySelectionPage();
            }
        }
    }

    updateFormData(formData: User) {
        this.formData = { ...this.formData, ...formData };
    }

    updateProfile() {
        if (!this.isFormValid()) {
            return;
        }

        this.handleUpdateUser.emit(this.formData);
    }

    openReferralSuccessModal() {
        const promoCode = this.utilService.getNestedValue(
            this.user,
            "referralBenefit.promoCode"
        );

        this.setCopyBlock(promoCode);
        this.showReferralSuccessModal = true;
    }

    closeReferralSuccessModal() {
        this.showReferralSuccessModal = false;
        this.routerService.goToCitySelectionPage();
    }

    private setCopyBlock(promoCode: string) {
        this.copyBlock.copyText.text = promoCode;
    }

    private isReferralSuccessful(): boolean {
        const promoCode = this.utilService.getNestedValue(
            this.user,
            "referralBenefit.promoCode"
        );

        if (!promoCode) {
            return false;
        }

        return true;
    }

    private isFormValid(): boolean {
        const nameErrMsg = this.checkNameField();
        const emailErrMsg = this.checkEmailField();

        this.formErrors = {
            name: nameErrMsg,
            email: emailErrMsg,
        };

        this.cdr.detectChanges();

        return !nameErrMsg.length && !emailErrMsg.length;
    }

    private checkNameField(): string {
        const { firstName = "", lastName = "" } = this.formData;

        const name = `${firstName} ${lastName}`.trim();

        if (!firstName.length) {
            return this.VALIDATION_TEXTS.NAME_EMPTY;
        } else if (!this.utilService.isValidName(name)) {
            return this.VALIDATION_TEXTS.NAME_INVALID;
        }

        return "";
    }

    private checkEmailField(): string {
        const email = this.formData.email;
        if (!email || !email.length) {
            return this.VALIDATION_TEXTS.EMAIL_EMPTY;
        } else if (!this.utilService.isValidEmail(email)) {
            return this.VALIDATION_TEXTS.EMAIL_INVALID;
        }

        return "";
    }

    private canHandleUserStateChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private sendAppsFlyerEvent() {
        this.appsFlyerService.trackRegistration(
            this.user && this.user.mobileNumber
        );
    }
}
