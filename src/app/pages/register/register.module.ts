import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { CallNumber } from "@ionic-native/call-number/ngx";

import { SharedModule } from "@shared/shared.module";

import { RegisterPageContainer } from "./containers/register.container";
import { RegisterPageLayoutComponent } from "./layouts/register.layout";
import { RegisterPageAdapter } from "./services/register.adapter";

const routes: Routes = [
    {
        path: "",
        component: RegisterPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [RegisterPageLayoutComponent, RegisterPageContainer],
    providers: [CallNumber, RegisterPageAdapter],
})
export class RegisterPageModule {}
