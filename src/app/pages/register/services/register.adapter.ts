import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { User } from "@models";

import {
    StoreState,
    UserStoreSelectors,
    UserStoreActions,
    ReferralStoreSelectors,
    ReferralStoreActions,
} from "@supr/store";

@Injectable()
export class RegisterPageAdapter {
    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    userState$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCurrentState)
    );

    userError$ = this.store.pipe(select(UserStoreSelectors.selectUserError));

    prePopulatedReferralCode$ = this.store.pipe(
        select(ReferralStoreSelectors.selectPrePopulatedReferralCode)
    );

    constructor(private store: Store<StoreState>) {}

    updateUser(user: User) {
        this.store.dispatch(
            new UserStoreActions.UpdateProfileRequestAction({ user })
        );
    }

    resetReferralCode() {
        this.store.dispatch(new ReferralStoreActions.ResetReferralCode());
    }
}
