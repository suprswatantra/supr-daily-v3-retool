import { Component } from "@angular/core";
import { Observable } from "rxjs";

import { User } from "@models";
import { UserCurrentState } from "@store/user/user.state";

import { RegisterPageAdapter as Adapter } from "@pages/register/services/register.adapter";

@Component({
    selector: "supr-register-container",
    template: `
        <supr-register-layout
            [user]="user$ | async"
            [userState]="userState$ | async"
            [userUpdateError]="userError$ | async"
            [prePopulatedReferralCode]="prePopulatedReferralCode$ | async"
            (handleUpdateUser)="updateProfile($event)"
            (handleResetPrePopulatedReferralCode)="resetReferralCode()"
        ></supr-register-layout>
    `,
})
export class RegisterPageContainer {
    user$: Observable<User>;
    userState$: Observable<UserCurrentState>;
    userError$: Observable<any>;
    prePopulatedReferralCode$: Observable<string>;

    constructor(private adapter: Adapter) {
        this.user$ = this.adapter.user$;
        this.userState$ = this.adapter.userState$;
        this.userError$ = this.adapter.userError$;
        this.prePopulatedReferralCode$ = this.adapter.prePopulatedReferralCode$;
    }

    updateProfile(user: User) {
        this.adapter.updateUser(user);
    }

    resetReferralCode() {
        this.adapter.resetReferralCode();
    }
}
