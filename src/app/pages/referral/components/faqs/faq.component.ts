import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { FaqItem } from "@shared/models";

@Component({
    selector: "supr-referral-faq",
    template: `
        <div class="faqsBlock" *ngIf="referralFaq && referralFaq.length > 0">
            <div class="faqContentWrapper">
                <div *ngFor="let faq of referralFaq">
                    <supr-faq-item [faq]="faq"></supr-faq-item>
                    <div class="divider16"></div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqComponent {
    @Input() referralFaq: FaqItem[] = [];
}
