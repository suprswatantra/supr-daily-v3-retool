import { FooterComponent } from "./footer/footer.component";
import { FaqComponent } from "./faqs/faq.component";
import { TopBarComponent } from "./top-bar/top-bar.component";
import { HeaderComponent } from "./header/header.component";
import { RewardsComponent } from "./rewards/rewards.component";
import { ReferralCodeComponent } from "./coupon/referral-code.component";
import { ContactCardComponent } from "./contact-card/contact-card.component";

export const ReferralComponents = [
    TopBarComponent,
    HeaderComponent,
    FaqComponent,
    FooterComponent,
    RewardsComponent,
    ReferralCodeComponent,
    ContactCardComponent,
];
