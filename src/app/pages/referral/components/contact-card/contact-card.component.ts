import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { PlatformService } from "@services/util/platform.service";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-referral-contact-card",
    template: `
        <ng-container *ngIf="phoneNumber || name">
            <div class="contactCard">
                <div class="suprRow center">
                    <ng-container>
                        <div
                            class="contactCircle"
                            [class.shareCardCircle]="isGenericShareCard"
                        >
                            <supr-text
                                type="body"
                                *ngIf="!isGenericShareCard; else shareIcon"
                            >
                                {{ contactInitial }}
                            </supr-text>
                            <ng-template #shareIcon>
                                <supr-icon name="share"></supr-icon>
                            </ng-template>
                        </div>
                    </ng-container>

                    <div class="spacer12"></div>

                    <div class="contactBlock">
                        <div class="contactName" *ngIf="name">
                            <supr-text
                                type="body"
                                [class.shareCardName]="isGenericShareCard"
                            >
                                {{ name }}
                            </supr-text>
                        </div>
                        <div class="contactNumber" *ngIf="phoneNumber">
                            <supr-text
                                type="caption"
                                [class.shareCardNumber]="isGenericShareCard"
                            >
                                {{ phoneNumber }}
                            </supr-text>
                        </div>
                    </div>

                    <div class="contactAction">
                        <ng-container
                            *ngIf="!isGenericShareCard; else shareBlock"
                        >
                            <div
                                class="suprRow inviteText"
                                (click)="onInviteClick()"
                                *ngIf="!isInvited; else invited"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .REFERRAL_CONTACTS_SYNC_INVITE}"
                                [saObjectValue]="phoneNumber"
                            >
                                <supr-text type="paragraph">
                                    ${TEXTS.INVITE}
                                </supr-text>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                            <ng-template #invited>
                                <div class="suprRow invitedText">
                                    <supr-text type="paragraph">
                                        ${TEXTS.INVITED}
                                    </supr-text>
                                </div>
                            </ng-template>
                        </ng-container>
                        <ng-template #shareBlock>
                            <div
                                class="suprRow inviteText"
                                (click)="onInviteClick()"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .REFERRAL_CONTACTS_SYNC_SHARE}"
                            >
                                <supr-text type="paragraph">
                                    ${TEXTS.SHARE}
                                </supr-text>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                        </ng-template>
                    </div>
                </div>
            </div>
        </ng-container>
    `,
    styleUrls: ["../../styles/contact-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactCardComponent implements OnInit {
    @Input() name: string;
    @Input() phoneNumber: string;
    @Input() isInvited: boolean;
    @Input() isGenericShareCard: boolean;
    @Input() abid: string;

    @Output() handleInviteClick: EventEmitter<string> = new EventEmitter();

    contactInitial: string;

    constructor(private platformService: PlatformService) {}

    ngOnInit() {
        this.setContactInitial();
    }

    onInviteClick() {
        if (this.platformService.isIOS()) {
            this.handleInviteClick.emit(this.abid);
            return;
        }

        this.handleInviteClick.emit(this.phoneNumber);
    }

    private setContactInitial() {
        if (!this.name) {
            return;
        }

        this.contactInitial = this.name.charAt(0);
    }
}
