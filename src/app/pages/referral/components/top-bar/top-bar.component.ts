import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

import { TEXTS } from "../../constants";
import { FaqItem } from "@shared/models";

@Component({
    selector: "supr-referral-top-bar",
    template: `
        <supr-page-header>
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="body" class="pageHeader">
                        ${TEXTS.HEADER_TEXT}
                    </supr-text>
                </div>

                <div
                    class="suprColumn right"
                    *ngIf="!isFetchingReferralInfo && faq && faq.length"
                >
                    <div
                        class="suprRow faqTopBar"
                        (click)="handleFaqClick.emit()"
                    >
                        <supr-text type="caption" class="pageHeader">
                            ${TEXTS.FAQ}
                        </supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
            </div>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {
    @Input() isFetchingReferralInfo: boolean;
    @Input() faq: FaqItem[];

    @Output() handleFaqClick: EventEmitter<void> = new EventEmitter();
}
