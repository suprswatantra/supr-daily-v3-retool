import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ReferralRewards } from "@shared/models";

import { REWARDS_TEXT } from "@pages/referral/constants";

@Component({
    selector: "supr-referral-rewards",
    template: `
        <div class="rewardsBlock" *ngIf="rewards">
            <ng-container *ngIf="rewards?.upcoming && rewards?.upcoming.length">
                <supr-text type="regular14">${REWARDS_TEXT.UPCOMING}</supr-text>
                <div class="divider12"></div>
                <div
                    *ngFor="
                        let activity of rewards.upcoming;
                        trackBy: trackByFn
                    "
                >
                    <supr-activity-block
                        [activityItem]="activity"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </ng-container>
            <ng-container *ngIf="rewards?.past && rewards?.past.length">
                <div
                    class="divider12"
                    *ngIf="rewards?.upcoming && rewards?.upcoming.length"
                ></div>
                <supr-text type="regular14">${REWARDS_TEXT.PAST}</supr-text>
                <div class="divider12"></div>
                <div *ngFor="let activity of rewards.past; trackBy: trackByFn">
                    <supr-activity-block
                        [activityItem]="activity"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardsComponent {
    @Input() rewards: ReferralRewards;

    constructor() {}

    trackByFn(_: any, index: number): number {
        return index;
    }
}
