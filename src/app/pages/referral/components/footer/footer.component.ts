import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { CopyBlock } from "@shared/models";

import {
    FOOTER_COPY_BLOCK,
    TEXTS,
    FOOTER_BTN_ICON,
} from "@pages/referral/constants";
import { SA_OBJECT_NAMES } from "@pages/referral/constants/analytics.constants";

@Component({
    selector: "supr-referral-footer",
    template: `
        <supr-page-footer-v2
            [buttonIcon]="btnIconName"
            buttonTitle="${TEXTS.UNLOCK_SAVINGS}"
            buttonSubtitle="${TEXTS.START_INVITING}"
            [footNoteText]="footNote"
            [disabled]="!isEnabled"
            footNoteLinkText="${TEXTS.FOOTNOTE_LINK_TEXT}"
            saObjectName="${SA_OBJECT_NAMES.CLICK.START_INVITING}"
            (handleButtonClick)="handleShareClick.emit()"
            (handleFooterNoteClick)="handleFooterNoteClick.emit()"
        >
            <div class="suprColumn left leftText">
                <ng-container *ngIf="isEnabled; else disabledTextBlock">
                    <div class="suprRow">
                        <supr-text type="caption">
                            ${TEXTS.TAP_TO_COPY}
                        </supr-text>
                    </div>
                    <div class="divider4"></div>
                    <supr-copy-block
                        [copyText]="copyBlock?.copyText"
                        [iconColor]="copyBlock?.iconColor"
                        [hideBorder]="true"
                        [showHelpText]="false"
                        [copyCodeClickSaObjectName]="copyCodeClickSaObjectName"
                    ></supr-copy-block>
                </ng-container>
                <ng-template #disabledTextBlock>
                    <div class="suprRow" *ngIf="disabledText">
                        <supr-text type="caption" class="disabled">
                            {{ disabledText }}
                        </supr-text>
                    </div>
                </ng-template>
            </div>
        </supr-page-footer-v2>
    `,
    styleUrls: ["../../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
    @Input() activeReferralCode: string;
    @Input() footNote: string;
    @Input() isEnabled: boolean;
    @Input() disabledText: string;

    @Output() handleShareClick: EventEmitter<void> = new EventEmitter();
    @Output() handleFooterNoteClick: EventEmitter<void> = new EventEmitter();

    copyBlock: CopyBlock;
    copyCodeClickSaObjectName =
        SA_OBJECT_NAMES.CLICK.COPY_CODE_FROM_REFERRAL_FOOTER;
    btnIconName: string;

    ngOnInit() {
        this.initialize();
    }

    private initialize() {
        this.copyBlock = FOOTER_COPY_BLOCK;
        this.copyBlock.copyText.text = this.activeReferralCode;

        this.btnIconName = this.isEnabled
            ? FOOTER_BTN_ICON.ENABLED
            : FOOTER_BTN_ICON.DISABLED;
    }
}
