import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { INPUT } from "@constants";

import { ReferralCouponBlock } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { REFERRAL_CODE_BLOCK_TEXTS } from "@pages/referral/constants";
import { SA_OBJECT_NAMES } from "@pages/referral/constants/analytics.constants";

@Component({
    selector: "supr-referral-code-block",
    template: `
        <div class="referralCodeBlockWrapper">
            <div class="suprRow">
                <supr-text type="subheading" class="referralTitle">
                    ${REFERRAL_CODE_BLOCK_TEXTS.GOT_CODE}
                </supr-text>
            </div>

            <ng-container *ngIf="referralCouponBlock?.benefitText">
                <div class="suprRow">
                    <supr-text type="caption" class="referralCaption">
                        {{ referralCouponBlock?.benefitText }}
                    </supr-text>
                </div>
            </ng-container>

            <div class="divider12"></div>
            <div class="formBlock">
                <div class="suprRow form">
                    <supr-input-label
                        placeholder="${REFERRAL_CODE_BLOCK_TEXTS.PLACEHOLDER}"
                        [type]="type"
                        [value]="referralCode"
                        [error]="errorMessage"
                        [maxLength]="10"
                        (handleInputChange)="updateReferralCode($event)"
                        saObjectName="referral-page-coupon-block"
                    ></supr-input-label>
                </div>
                <ng-container *ngIf="showSubmitButton">
                    <div
                        class="submitFloater"
                        (click)="sendReferralCode()"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK.CODE_SUBMIT}"
                    >
                        <div class="suprColumn center">
                            <supr-text type="regular14">
                                ${REFERRAL_CODE_BLOCK_TEXTS.SUBMIT}
                            </supr-text>
                        </div>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/referral-code.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralCodeComponent implements OnInit, OnChanges {
    @Input() referralCouponBlock: ReferralCouponBlock;
    @Input() prePopulatedReferralCode: string;

    @Output() handleSumbitClick: EventEmitter<string> = new EventEmitter();
    @Output() handleResetPrePopulatedReferralCode: EventEmitter<
        void
    > = new EventEmitter();

    type = INPUT.TYPES.NUMBER;

    referralCode: string;
    errorMessage: string;
    showSubmitButton = false;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change =
            changes["referralCouponBlock"] ||
            changes["prePopulatedReferralCode"];

        if (this.canHandleChange(change)) {
            this.initialize();
        }
    }

    updateReferralCode(code) {
        this.errorMessage = "";
        this.referralCode = code;

        if (!this.referralCode) {
            this.showSubmitButton = false;
            return;
        }

        this.showSubmitButton = true;
    }

    sendReferralCode() {
        const referralString = "" + this.referralCode;

        if (!referralString) {
            return;
        }

        this.handleSumbitClick.emit(referralString);
    }

    private initialize() {
        this.resetFlags();

        if (this.prePopulatedReferralCode) {
            this.updateReferralCode(this.prePopulatedReferralCode);
            this.handleResetPrePopulatedReferralCode.emit();
        }

        this.errorMessage = this.utilService.getNestedValue(
            this.referralCouponBlock,
            "errorMessage",
            ""
        );

        if (this.errorMessage) {
            this.showSubmitButton = false;
            return;
        }
    }

    private resetFlags() {
        this.errorMessage = "";
        this.referralCode = "";
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
