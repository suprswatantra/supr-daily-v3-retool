import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    ReferralStoreSelectors,
    ReferralStoreActions,
} from "@supr/store";

@Injectable()
export class ReferralAdapter {
    referralInfo$ = this.store.pipe(
        select(ReferralStoreSelectors.selectReferralInfo)
    );

    referralFaqs$ = this.store.pipe(
        select(ReferralStoreSelectors.selectReferralFaqs)
    );

    referralRewards$ = this.store.pipe(
        select(ReferralStoreSelectors.selectReferralRewards)
    );

    isSyncingContacts$ = this.store.pipe(
        select(ReferralStoreSelectors.selectIsSyncingContacts)
    );

    isFetchingReferralInfo$ = this.store.pipe(
        select(ReferralStoreSelectors.selectIsFetchingReferralInfo)
    );

    isApplyingReferralCode$ = this.store.pipe(
        select(ReferralStoreSelectors.selectIsApplyingReferralCode)
    );

    prePopulatedReferralCode$ = this.store.pipe(
        select(ReferralStoreSelectors.selectPrePopulatedReferralCode)
    );

    filteredPhoneNumbers$ = this.store.pipe(
        select(ReferralStoreSelectors.selectFilteredContacts)
    );

    constructor(private store: Store<StoreState>) {}

    fetchReferralInfo() {
        this.store.dispatch(
            new ReferralStoreActions.FetchReferralRequestAction({
                silent: false,
            })
        );
    }

    applyReferralCode(referralCode: string) {
        this.store.dispatch(
            new ReferralStoreActions.ApplyReferralCodeRequestAction({
                referralCode,
            })
        );
    }

    resetReferralCode() {
        this.store.dispatch(new ReferralStoreActions.ResetReferralCode());
    }

    syncContacts(contacts: string[]) {
        this.store.dispatch(
            new ReferralStoreActions.ReferralFilterContactsRequestAction({
                contacts,
            })
        );
    }
}
