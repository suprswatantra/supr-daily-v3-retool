import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SocialSharing } from "@ionic-enterprise/social-sharing/ngx";
import { OpenNativeSettings } from "@ionic-native/open-native-settings/ngx";
import { Contacts } from "@ionic-enterprise/contacts/ngx";

import { SharedModule } from "@shared/shared.module";

import { SCREEN_NAMES } from "@constants";

import { ReferralLayout } from "./layouts/referral.layout";
import { ReferralFaqLayoutComponent } from "./layouts/referral-faq.layout";
import { ReferralRewardsLayoutComponent } from "./layouts/referral-rewards.layout";
import { ReferralInviteLayoutComponent } from "./layouts/referral-invite.layout";
import { ReferralComponents } from "./components";
import { ReferralContainer } from "./containers/referral.container";
import { ReferralFaqPageContainer } from "./containers/referral-faq.container";
import { ReferralRewardsContainer } from "./containers/referral-rewards.container";
import { ReferralInvitePageContainer } from "./containers/referral-invite.container";

import { ReferralAdapter } from "./services/referral.adapter";
const routes: Routes = [
    {
        path: "",
        component: ReferralContainer,
    },
    {
        path: "faq",
        component: ReferralFaqPageContainer,
        data: { screenName: SCREEN_NAMES.REFERRAL_FAQ },
    },
    {
        path: "rewards",
        component: ReferralRewardsContainer,
        data: { screenName: SCREEN_NAMES.REFERRAL_REWARDS },
    },
    {
        path: "invite",
        component: ReferralInvitePageContainer,
        data: { screenName: SCREEN_NAMES.REFERRAL_INVITE },
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...ReferralComponents,
        ReferralContainer,
        ReferralFaqPageContainer,
        ReferralRewardsContainer,
        ReferralInvitePageContainer,
        ReferralLayout,
        ReferralFaqLayoutComponent,
        ReferralRewardsLayoutComponent,
        ReferralInviteLayoutComponent,
    ],
    providers: [ReferralAdapter, Contacts, OpenNativeSettings, SocialSharing],
})
export class ReferralModule {}
