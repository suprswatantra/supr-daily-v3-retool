import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    OnChanges,
    ChangeDetectorRef,
} from "@angular/core";
import {
    Contact,
    ContactFindOptions,
} from "@ionic-enterprise/contacts/ngx/IonicContacts";
import { Contacts } from "@ionic-enterprise/contacts/ngx";
import { OpenNativeSettings } from "@ionic-native/open-native-settings/ngx";

import { ContactCard, ReferralInfo } from "@shared/models";

import { ReferralService } from "@services/shared/referral.service";
import { PlatformService } from "@services/util/platform.service";
import { ToastService } from "@services/layout/toast.service";

import { TEXTS, DEFAULT_IMG_URL, APPLICATION_SETTINGS } from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-referral-invite-layout",
    template: `
        <div class="suprContainer">
            <!-- Header -->
            <supr-page-header>
                <supr-text
                    type="subheading"
                    *ngIf="
                        filteredContacts && filteredContacts.length;
                        else syncContacts
                    "
                >
                    ${TEXTS.INVITE_VIA_WHATSAPP}
                </supr-text>
                <ng-template #syncContacts>
                    <supr-text type="subheading">
                        ${TEXTS.SYNC_CONTACTS}
                    </supr-text>
                </ng-template>
            </supr-page-header>

            <!-- Content -->
            <div class="suprScrollContent">
                <ng-container
                    *ngIf="
                        isSyncingContacts || isloadingContactsFromPhone;
                        else content
                    "
                >
                    <div class="loaderContainer suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-container>
                <ng-template #content>
                    <ng-container
                        *ngIf="
                            filteredContacts && filteredContacts.length;
                            else askPermission
                        "
                    >
                        <!-- Filtered contact list -->
                        <div
                            class="filteredContactsWrapper"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .REFERRAL_CONTACTS_SYNC}"
                        >
                            <div class="divider12"></div>
                            <supr-input-label
                                placeholder="${TEXTS.CONTACTS_SEARCH_PLACEHOLDER}"
                                (handleInputChange)="
                                    onSearchInputChange($event)
                                "
                            ></supr-input-label>
                            <div class="divider16"></div>
                            <div class="genericReferralBlock">
                                <supr-referral-contact-card
                                    name="${TEXTS.SHARE_REFERRAL_CODE}"
                                    [phoneNumber]="
                                        referralInfo?.activeReferralCode
                                    "
                                    [isGenericShareCard]="true"
                                    (handleInviteClick)="
                                        inviteViaWhatsApp($event)
                                    "
                                ></supr-referral-contact-card>
                            </div>
                            <div class="divider12"></div>
                            <ng-container
                                *ngFor="
                                    let contact of filteredContacts
                                        | SearchFilter: searchText
                                "
                            >
                                <ng-container *ngIf="contact">
                                    <supr-referral-contact-card
                                        [name]="contact.name"
                                        [phoneNumber]="contact.phone"
                                        [abid]="contact?.id"
                                        (handleInviteClick)="
                                            inviteViaWhatsAppContact($event)
                                        "
                                    ></supr-referral-contact-card>
                                    <div class="divider16"></div>
                                </ng-container>
                            </ng-container>
                        </div>
                    </ng-container>

                    <!-- Could not fetch contacts -->
                    <ng-template #askPermission>
                        <div class="syncContactsWrapper">
                            <div class="suprRow center syncImg">
                                <img [src]="defaultImgUrl" />
                            </div>
                            <div class="divider24"></div>
                            <div class="suprColumn center">
                                <supr-text type="subtitle" class="syncText">
                                    ${TEXTS.CONTACT_SYNC_DEFAULT}
                                </supr-text>
                            </div>
                        </div>
                        <supr-page-footer>
                            <supr-button
                                (handleClick)="grantPermission()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .REFERRAL_GRANT_PERMISSION}"
                            >
                                <supr-text type="body">
                                    ${TEXTS.GRANT_PERMISSION}
                                </supr-text>
                            </supr-button>
                            <div class="divider8"></div>
                            <div
                                class="suprRow center alternateBlock"
                                (click)="inviteViaWhatsApp()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .REFERRAL_INVITE_WHATSAPP}"
                            >
                                <supr-text type="caption">
                                    ${TEXTS.INVITE_ALTERNATE} ${TEXTS.WHATSAPP}
                                </supr-text>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                        </supr-page-footer>
                    </ng-template>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralInviteLayoutComponent implements OnInit, OnChanges {
    @Input() filteredPhoneNumbers: string[];
    @Input() isSyncingContacts: boolean;
    @Input() referralInfo: ReferralInfo;

    @Output() handleSyncContacts: EventEmitter<string[]> = new EventEmitter();

    filteredContacts: ContactCard[] = [];
    searchText: string;
    defaultImgUrl = DEFAULT_IMG_URL;
    isloadingContactsFromPhone = false;

    private phoneContacts: Contact[] = [];

    constructor(
        private contacts: Contacts,
        private referralService: ReferralService,
        private platformService: PlatformService,
        private toastService: ToastService,
        private openNativeSettings: OpenNativeSettings,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.initialize();
    }

    /**
     * Listen to changes on filtered phone numbers from BE
     *
     * @param {SimpleChanges} changes
     * @memberof ReferralInviteLayoutComponent
     */
    ngOnChanges(changes: SimpleChanges) {
        const change = changes["filteredPhoneNumbers"];

        if (this.canHandleChange(change)) {
            this.filterContacts();
        }
    }

    onSearchInputChange(text: string) {
        this.searchText = text;
    }

    inviteViaWhatsAppContact(reciever: string) {
        this.referralService.shareViaWhatsAppToContact(
            this.referralInfo,
            reciever
        );
    }

    inviteViaWhatsApp() {
        this.referralService.shareViaWhatsApp(this.referralInfo);
    }

    /**
     * Try to load contacts from Phone
     * If successful, sync with BE to check if contact doesnot exist on our platform
     * Only those contacts would have share functions
     * Handles error gracefully
     *
     * @returns
     * @memberof ReferralInviteLayoutComponent
     */
    loadContacts() {
        if (!this.platformService.isCordova()) {
            return;
        }

        const options: ContactFindOptions = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true,
        };

        this.isloadingContactsFromPhone = true;

        this.contacts
            .find(["*"], options)
            .then((contacts: Contact[]) => {
                this.isloadingContactsFromPhone = false;
                this.cdr.detectChanges();
                this.syncContacts(contacts);
            })
            .catch(() => {
                this.isloadingContactsFromPhone = false;
                this.cdr.detectChanges();
                this.toastService.present(TEXTS.LOAD_CONTACTS_ERROR);
            });
    }

    grantPermission() {
        this.openNativeSettings.open(APPLICATION_SETTINGS);
    }

    private initialize() {
        this.loadContacts();
    }

    /**
     * Sorts contact list alphabetiocally, creates a new staructure of contacts to be used by contact-card
     * Filter out contacts already present in our platform
     *
     * @private
     * @memberof ReferralInviteLayoutComponent
     */
    private filterContacts() {
        this.filteredContacts = this.referralService.filterAndSortContacts(
            this.phoneContacts,
            this.filteredPhoneNumbers
        );
    }

    /**
     * getRawPrimaryPhoneNumbers trims out all the special characters and spaces from phone numbers
     * BE needs clean numbers for processing
     *
     * @private
     * @returns
     * @memberof ReferralInviteLayoutComponent
     */
    private syncContacts(contacts: Contact[]) {
        this.phoneContacts = contacts;

        const rawPhoneNumbers = this.referralService.getRawPrimaryPhoneNumbers(
            this.phoneContacts
        );

        if (!rawPhoneNumbers) {
            return;
        }

        this.handleSyncContacts.emit(rawPhoneNumbers);
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
