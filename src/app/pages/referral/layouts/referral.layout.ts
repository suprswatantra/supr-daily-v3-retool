import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { RATING_NUDGE_TRIGGERS } from "@constants";

import {
    ReferralInfo,
    BenefitsBannerCtaAction,
    ImageBannerContent,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { ReferralService } from "@services/shared/referral.service";
import { RouterService } from "@services/util/router.service";
import { NudgeService } from "@services/layout/nudge.service";

import { TEXTS, IMAGE_BANNER_ACTIONS } from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-referral-layout",
    template: `
        <div class="suprContainer">
            <!-- Top Bar -->
            <supr-referral-top-bar
                [isFetchingReferralInfo]="isFetchingReferralInfo"
                [faq]="referralInfo?.faqs"
                (handleFaqClick)="handleFaqClick()"
            ></supr-referral-top-bar>
            <div
                class="suprScrollContent referralInfoWrapper"
                [class.hasFootNote]="referralInfo?.referralRewardInfoText"
            >
                <ng-container *ngIf="isFetchingReferralInfo; else content">
                    <div class="loaderContainer suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-container>
                <ng-template #content>
                    <ng-container *ngIf="referralInfo; else error">
                        <!-- Apply Coupon Block -->
                        <ng-container
                            *ngIf="
                                referralInfo?.referralCouponBlock
                                    ?.showCouponBlock
                            "
                        >
                            <div
                                saImpression
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .APPLY_REFERRAL_CODE_BLOCK}"
                            >
                                <div class="divider20"></div>
                                <supr-referral-code-block
                                    [referralCouponBlock]="
                                        referralInfo?.referralCouponBlock
                                    "
                                    [prePopulatedReferralCode]="
                                        prePopulatedReferralCode
                                    "
                                    (handleResetPrePopulatedReferralCode)="
                                        handleResetPrePopulatedReferralCode.emit()
                                    "
                                    (handleSumbitClick)="
                                        handleApplyReferralCode.emit($event)
                                    "
                                ></supr-referral-code-block>
                                <div class="divider24"></div>
                            </div>
                        </ng-container>

                        <!-- Coupn Applied Banner -->
                        <ng-container
                            *ngIf="
                                referralInfo?.referralCouponBlock
                                    ?.referralCodeAppliedBanner
                            "
                        >
                            <div
                                saImpression
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .REFERRAL_CODE_SUCCESS_BANNER}"
                            >
                                <div class="divider20"></div>
                                <supr-banner
                                    [banner]="
                                        referralInfo?.referralCouponBlock
                                            ?.referralCodeAppliedBanner
                                    "
                                ></supr-banner>
                                <div class="divider20"></div>
                            </div>
                        </ng-container>

                        <!-- Header -->
                        <ng-container *ngIf="referralInfo?.header">
                            <supr-referral-header
                                [headerData]="referralInfo?.header"
                            ></supr-referral-header>
                        </ng-container>

                        <!-- Alert Banner -->
                        <ng-container *ngIf="referralInfo?.alertBanner">
                            <div class="divider28"></div>
                            <supr-banner
                                [banner]="referralInfo?.alertBanner"
                            ></supr-banner>
                        </ng-container>

                        <!-- Benefits -->
                        <ng-container *ngIf="referralInfo?.benefits">
                            <div class="divider28"></div>
                            <supr-benefits-banner
                                [benefitsBanner]="referralInfo?.benefits"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick($event)
                                "
                            ></supr-benefits-banner>
                        </ng-container>

                        <!-- Image Banner -->
                        <ng-container *ngIf="referralInfo?.imgBanner">
                            <div class="divider28"></div>
                            <supr-img-banner
                                [imgBanner]="referralInfo?.imgBanner"
                                (handleImgBannerClick)="
                                    handleImgBannerClick($event)
                                "
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .IMG_BANNER}"
                            ></supr-img-banner>
                        </ng-container>

                        <!-- Banner -->
                        <ng-container *ngIf="referralInfo?.banner">
                            <div class="divider28"></div>
                            <supr-banner
                                [banner]="referralInfo?.banner"
                            ></supr-banner>
                        </ng-container>

                        <!-- Common Benefits -->
                        <ng-container *ngIf="referralInfo?.commonBenefits">
                            <div class="divider28"></div>
                            <supr-benefits-banner
                                [benefitsBanner]="referralInfo?.commonBenefits"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick($event)
                                "
                            ></supr-benefits-banner>
                        </ng-container>
                    </ng-container>

                    <!-- Error -->
                    <ng-template #error></ng-template>
                </ng-template>
            </div>

            <!-- Footer -->
            <ng-container *ngIf="referralInfo?.activeReferralCode">
                <supr-referral-footer
                    [activeReferralCode]="referralInfo?.activeReferralCode"
                    [footNote]="referralInfo?.referralRewardInfoText"
                    [isEnabled]="referralInfo?.eligibleToRefer"
                    [disabledText]="referralInfo?.referralDisabledInfoText"
                    (handleShareClick)="handleShareClick()"
                    (handleFooterNoteClick)="handleFaqClick()"
                ></supr-referral-footer>
            </ng-container>

            <!-- Referral Success modal -->
            <ng-container *ngIf="referralInfo?.successfulReferralMessage">
                <supr-referral-success
                    [showModal]="showReferralSuccessModal"
                    [imgUrl]="referralInfo?.successfulReferralMessage?.imgUrl"
                    [title]="referralInfo?.successfulReferralMessage?.title"
                    [description]="
                        referralInfo?.successfulReferralMessage?.description
                    "
                    (handleClose)="closeReferralSuccessModal()"
                ></supr-referral-success>
            </ng-container>

            <!-- Referral share bottomsheet -->
            <ng-container *ngIf="referralInfo?.shareBlock?.message">
                <supr-referral-bottomsheet
                    [showModal]="showReferralShareBottomsheet"
                    (handleClose)="closeReferralShareBottomsheet()"
                ></supr-referral-bottomsheet>
            </ng-container>

            <!-- Overlay -->
            <supr-loader-overlay *ngIf="isApplyingReferralCode">
                ${TEXTS.APPLYING_REFERRAL_CODE}
            </supr-loader-overlay>
        </div>
    `,
    styleUrls: ["../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralLayout implements OnInit, OnChanges {
    @Input() referralInfo: ReferralInfo;
    @Input() isFetchingReferralInfo: boolean;
    @Input() isApplyingReferralCode: boolean;
    @Input() prePopulatedReferralCode: string;

    @Output() handleFetchReferralInfo: EventEmitter<void> = new EventEmitter();
    @Output() handleApplyReferralCode: EventEmitter<
        string
    > = new EventEmitter();
    @Output() handleResetPrePopulatedReferralCode: EventEmitter<
        void
    > = new EventEmitter();

    hasError = false;
    showReferralSuccessModal = false;
    showReferralShareBottomsheet = false;

    constructor(
        private utilService: UtilService,
        private referralService: ReferralService,
        private routerService: RouterService,
        private nudgeService: NudgeService
    ) {}

    ngOnInit() {
        if (this.utilService.isEmpty(this.referralInfo)) {
            this.handleFetchReferralInfo.emit();
        }

        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["referralInfo"];

        if (this.canHandleChange(change)) {
            this.initialize();
        }
    }

    handleImgBannerClick(bannerImage: ImageBannerContent) {
        if (bannerImage && bannerImage.action === IMAGE_BANNER_ACTIONS.ROUTE) {
            if (bannerImage.appUrl) {
                this.routerService.goToUrl(bannerImage.appUrl);
            }
            if (bannerImage.outsideUrl) {
                this.routerService.openExternalUrl(bannerImage.outsideUrl);
            }
        }
    }

    closeReferralShareBottomsheet() {
        this.showReferralShareBottomsheet = false;
    }

    openReferralSuccessModal() {
        this.showReferralSuccessModal = true;
    }

    closeReferralSuccessModal() {
        this.showReferralSuccessModal = false;

        this.referralService.setSuccessfulReferralsToDb(this.referralInfo);

        this.sendRatingNudge();
    }

    handleBenefitCtaClick(ctaAction: BenefitsBannerCtaAction) {
        this.referralService.handleBenefitCtaClick(
            this.referralInfo,
            ctaAction
        );
    }

    handleFaqClick() {
        this.referralService.handleFaqClick(this.referralInfo);
    }

    handleShareClick() {
        this.showReferralShareBottomsheet = true;
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.SUCCESSFUL_REFERRAL
        );
    }

    private async initialize() {
        if (!this.referralInfo) {
            return;
        }

        const canOpenReferralSuccessModal = await this.referralService.canOpenReferralSuccessModal(
            this.referralInfo
        );

        if (canOpenReferralSuccessModal) {
            this.openReferralSuccessModal();
        }
    }
}
