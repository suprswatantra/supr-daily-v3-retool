import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { TEXTS } from "../constants";

import { ReferralRewards } from "@models";

@Component({
    selector: "supr-referral-rewards-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.REWARDS_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-referral-rewards
                    [rewards]="rewards"
                ></supr-referral-rewards>
            </div>
        </div>
    `,
    styleUrls: ["../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralRewardsLayoutComponent {
    @Input() rewards: ReferralRewards;
}
