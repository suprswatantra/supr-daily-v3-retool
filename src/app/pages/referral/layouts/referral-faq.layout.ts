import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { FaqItem } from "@models";

import { TEXTS } from "../constants";

@Component({
    selector: "supr-referral-faq-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.FAQ_SECTION_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-referral-faq [referralFaq]="faqs"></supr-referral-faq>
            </div>
        </div>
    `,
    styleUrls: ["../styles/referral.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralFaqLayoutComponent {
    @Input() faqs: FaqItem[];
}
