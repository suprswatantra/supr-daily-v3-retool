import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ReferralRewards } from "@shared/models";

import { ReferralAdapter as Adapter } from "../services/referral.adapter";

@Component({
    selector: "supr-referral-rewards-container",
    template: `
        <supr-referral-rewards-layout
            [rewards]="referralRewards$ | async"
        ></supr-referral-rewards-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralRewardsContainer implements OnInit {
    referralRewards$: Observable<ReferralRewards>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.referralRewards$ = this.adapter.referralRewards$;
    }
}
