import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { FaqItem } from "@shared/models";

import { ReferralAdapter as Adapter } from "../services/referral.adapter";

@Component({
    selector: "supr-referral-faq-container",
    template: `
        <supr-referral-faq-layout
            [faqs]="referralFaqs$ | async"
        ></supr-referral-faq-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralFaqPageContainer implements OnInit {
    referralFaqs$: Observable<FaqItem[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.referralFaqs$ = this.adapter.referralFaqs$;
    }
}
