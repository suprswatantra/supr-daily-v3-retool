import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { ReferralInfo } from "@shared/models";

import { ReferralAdapter as Adapter } from "../services/referral.adapter";

@Component({
    selector: "supr-referral-container",
    template: `
        <supr-referral-layout
            [referralInfo]="referralInfo$ | async"
            [isFetchingReferralInfo]="isFetchingReferralInfo$ | async"
            [isApplyingReferralCode]="isApplyingReferralCode$ | async"
            [prePopulatedReferralCode]="prePopulatedReferralCode$ | async"
            (handleFetchReferralInfo)="handleFetchReferralInfo()"
            (handleApplyReferralCode)="applyReferralCode($event)"
            (handleResetPrePopulatedReferralCode)="resetReferralCode()"
        ></supr-referral-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralContainer implements OnInit {
    referralInfo$: Observable<ReferralInfo>;
    prePopulatedReferralCode$: Observable<string>;
    isFetchingReferralInfo$: Observable<boolean>;
    isApplyingReferralCode$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.referralInfo$ = this.adapter.referralInfo$;
        this.isFetchingReferralInfo$ = this.adapter.isFetchingReferralInfo$;
        this.isApplyingReferralCode$ = this.adapter.isApplyingReferralCode$;
        this.prePopulatedReferralCode$ = this.adapter.prePopulatedReferralCode$;
    }

    handleFetchReferralInfo() {
        this.adapter.fetchReferralInfo();
    }

    applyReferralCode(referralCode: string) {
        this.adapter.applyReferralCode(referralCode);
    }

    resetReferralCode() {
        this.adapter.resetReferralCode();
    }
}
