import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { ReferralAdapter as Adapter } from "../services/referral.adapter";
import { ReferralInfo } from "@shared/models";

@Component({
    selector: "supr-referral-faq-container",
    template: `
        <supr-referral-invite-layout
            [filteredPhoneNumbers]="filteredPhoneNumbers$ | async"
            [referralInfo]="referralInfo$ | async"
            [isSyncingContacts]="isSyncingContacts$ | async"
            (handleSyncContacts)="syncContacts($event)"
        ></supr-referral-invite-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralInvitePageContainer implements OnInit {
    filteredPhoneNumbers$: Observable<string[]>;
    referralInfo$: Observable<ReferralInfo>;
    isSyncingContacts$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.filteredPhoneNumbers$ = this.adapter.filteredPhoneNumbers$;
        this.referralInfo$ = this.adapter.referralInfo$;
        this.isSyncingContacts$ = this.adapter.isSyncingContacts$;
    }

    syncContacts(contacts: string[]) {
        this.adapter.syncContacts(contacts);
    }
}
