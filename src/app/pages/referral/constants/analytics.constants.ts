export const SA_OBJECT_NAMES = {
    IMPRESSION: {
        REFERRAL_CODE_SUCCESS_BANNER: "referral-code-success-banner",
        APPLY_REFERRAL_CODE_BLOCK: "apply-referral-code-block",
        REFERRAL_CONTACTS_SYNC: "referral-contacts-sync",
    },
    CLICK: {
        START_INVITING: "referral-start-inviting",
        CODE_SUBMIT: "referral-code-submit",
        COPY_CODE_FROM_REFERRAL_FOOTER: "copy-code-from-referral-footer",
        COPY_CODE_FROM_REFERRAL_CODE_SUCCESS_BANNER:
            "copy-code-from-referral-code-success-banner",
        IMG_BANNER: "referral-img-banner",
        REFERRAL_GRANT_PERMISSION: "referral-grant-permission",
        REFERRAL_INVITE_WHATSAPP: "referral-invite-whatsapp",
        REFERRAL_CONTACTS_SYNC_INVITE: "referral-contacts-sync-invite",
        REFERRAL_CONTACTS_SYNC_SHARE: "referral-contacts-sync-share",
    },
};
