export const TEXTS = {
    HEADER_TEXT: "Refer and save",
    FAQ_SECTION_TITLE: "Frequently asked questions",
    REWARDS_TITLE: "Referral Rewards",
    FAQ: "FAQs",
    APPLYING_REFERRAL_CODE: "Applying referral code...",
    TAP_TO_COPY: "Tap to copy referral code",
    UNLOCK_SAVINGS: "Unlock savings",
    START_INVITING: "START INVITING",
    INVITE_VIA_WHATSAPP: "Invite via WhatsApp",
    SYNC_CONTACTS: "Sync contacts",
    GRANT_PERMISSION: "Grant permission",
    CONTACT_SYNC_DEFAULT: "Sync your contacts to refer your friends easily",
    SHARE_REFERRAL_CODE: "Share your referral code",
    SHARE: "SHARE",
    INVITE: "INVITE",
    INVITED: "INVITED",
    INVITE_ALTERNATE: "OR INVITE USING",
    WHATSAPP: "WHATSAPP",
    CONTACTS_SEARCH_PLACEHOLDER: "Search contacts",
    SYNC: "SYNC",
    LOAD_CONTACTS_ERROR: "Error fetching contacts from phone",
    FOOTNOTE_LINK_TEXT: "View T&Cs",
};

export const IMAGE_BANNER_ACTIONS = {
    ROUTE: "route",
};

export const STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-referral-header-bg",
    },
    {
        attributeName: "warningText.textColor",
        attributeStyleVariableName: "--supr-referral-warning-icon-color",
    },
    {
        attributeName: "warningText.fontSize",
        attributeStyleVariableName: "--supr-referral-warning-icon-font-size",
    },
];

export const REWARDS_TEXT = {
    UPCOMING: "Upcoming rewards",
    PAST: "Past rewards",
};

export const FOOTER_COPY_BLOCK = {
    copyText: {
        text: null,
        textColor: "var(--primary-100)",
        fontSize: "14px",
    },
    iconColor: "var(--primary-100)",
};

export const REFERRAL_CODE_BLOCK_TEXTS = {
    GOT_CODE: "Got a code?",
    SUBMIT: "SUBMIT",
    PLACEHOLDER: "Enter 10 digit referral code",
};

export const DEFAULT_IMG_URL = "assets/images/app/supr-sync-contacts.svg";

export const APPLICATION_SETTINGS = "application_details";

export const FOOTER_BTN_ICON = {
    ENABLED: "chevron_right",
    DISABLED: "lock",
};
