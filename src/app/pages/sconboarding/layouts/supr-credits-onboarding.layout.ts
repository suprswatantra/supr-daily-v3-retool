import { Component, ChangeDetectionStrategy } from "@angular/core";

import { LOCAL_DB_DATA_KEYS } from "@constants";

import { DbService } from "@services/data/db.service";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-credits-onboarding-layout",
    template: `
        <supr-credits-onboarding
            *ngIf="firstScreen"
            (showCoinsEarned)="showCoinsEarned()"
        ></supr-credits-onboarding>
        <supr-credits-onboarding-balance-container
            *ngIf="secondScreen"
        ></supr-credits-onboarding-balance-container>
    `,
    styleUrls: ["../styles/supr-credits-onboarding.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCreditsOnboardingLayoutComponent {
    firstScreen = true;
    secondScreen = false;

    constructor(
        private dbService: DbService,
        private routerService: RouterService
    ) {}

    showCoinsEarned() {
        this.secondScreen = true;
        this.firstScreen = false;
        this.setOnboardedFlagInDB();
    }

    private setOnboardedFlagInDB() {
        setTimeout(() => {
            this.dbService.setData(
                LOCAL_DB_DATA_KEYS.SUPR_CREDITS_ONBOARDING_DONE,
                true
            );
            this.routerService.goToHomePage();
        }, 3000);
    }
}
