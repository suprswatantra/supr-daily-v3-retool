import { SuprCreditsOnboardingBalanceComponent } from "./supr-credits-onboarding-balance.component";
import { SuprCreditsOnboardingComponent } from "./supr-credits-onboarding.component";

export const scOnboardingComponents = [
    SuprCreditsOnboardingComponent,
    SuprCreditsOnboardingBalanceComponent,
];
