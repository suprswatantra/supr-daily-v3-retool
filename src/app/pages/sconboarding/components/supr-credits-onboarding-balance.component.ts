import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { TEXTS } from "../constants/onboarding.constants";
@Component({
    selector: "supr-credits-onboarding-balance",
    template: `
        <div class="scWrapper balanceScreen">
            <supr-svg class="bgIllustration"></supr-svg>
            <div class="suprColumn center">
                <supr-svg class="suprCreditsBg"></supr-svg>
                <div class="suprColumn center absolute">
                    <supr-text type="action14">{{
                        texts.YOU_EARNED | uppercase
                    }}</supr-text>
                    <div class="divider8"></div>
                    <supr-text type="title">+ {{ scBalance }}</supr-text>
                    <supr-text type="subtitle">Supr Credits</supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-credits-onboarding.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCreditsOnboardingBalanceComponent {
    @Input() scBalance: number;

    texts = TEXTS;
}
