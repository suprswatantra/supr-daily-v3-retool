import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
} from "@angular/core";

import { ANALYTIC_OBJECT_NAMES } from "../constants/analytics.constants";
import { TEXTS } from "../constants/onboarding.constants";
@Component({
    selector: "supr-credits-onboarding",
    template: `
        <div class="scWrapper">
            <div
                class="suprColumn center"
                saImpression
                saObjectName="${ANALYTIC_OBJECT_NAMES.INTRODUCING_SUPRCREDITS}"
            >
                <supr-svg class="scImg"></supr-svg>
                <div class="divider24"></div>
                <supr-text type="title">Introducing</supr-text>
                <supr-text type="title">Supr Credits</supr-text>
                <div class="divider16"></div>
                <div class="desc">
                    <supr-text type="action14">
                        {{ texts.DESCRIPTION }}
                    </supr-text>
                </div>
                <div class="divider16"></div>
                <supr-text type="subheading600">
                    {{ texts.SC_RUPEE_CONVERSION }}
                </supr-text>
                <div class="divider24"></div>
                <supr-button
                    (handleClick)="showCoinsEarned.emit()"
                    saClick
                    saObjectName="${ANALYTIC_OBJECT_NAMES.CLAIM_CREDITS}"
                >
                    <supr-text type="action14">{{
                        texts.CLAIM_CREDITS | uppercase
                    }}</supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-credits-onboarding.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCreditsOnboardingComponent {
    @Output() showCoinsEarned: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;
}
