export const TEXTS = {
    DESCRIPTION:
        "Earn credits for every product you buy, wallet recharges and much more. Redeem the credits on your orders and increase your monthly savings.",
    CLAIM_CREDITS: "Claim credits",
    YOU_EARNED: "You earned",
    SC_RUPEE_CONVERSION: "1 SuprCredit = 1 Rupee",
};
