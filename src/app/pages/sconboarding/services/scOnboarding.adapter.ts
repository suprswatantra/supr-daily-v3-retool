import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { StoreState, WalletStoreSelectors } from "@supr/store";

@Injectable()
export class SuprCreditsOnboardingPageAdapter {
    suprCreditsBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectSuprCreditsBalance)
    );

    constructor(private store: Store<StoreState>) {}
}
