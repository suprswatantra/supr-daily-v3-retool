import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SuprCreditsOnboardingLayoutComponent } from "./layouts/supr-credits-onboarding.layout";
import { scOnboardingComponents } from "./components";
import { SuprCreditsOnboardingBalanceContainer } from "./containers/supr-credits-onboarding-balance.container";
import { SuprCreditsOnboardingPageAdapter } from "./services/scOnboarding.adapter";

const routes: Routes = [
    {
        path: "",
        component: SuprCreditsOnboardingLayoutComponent,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...scOnboardingComponents,
        SuprCreditsOnboardingBalanceContainer,
        SuprCreditsOnboardingLayoutComponent,
    ],
    providers: [SuprCreditsOnboardingPageAdapter],
})
export class SuperCreditsOnboardingPageModule {}
