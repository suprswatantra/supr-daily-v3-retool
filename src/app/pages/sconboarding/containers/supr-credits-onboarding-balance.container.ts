import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { SuprCreditsOnboardingPageAdapter as Adapter } from "../services/scOnboarding.adapter";

@Component({
    selector: "supr-credits-onboarding-balance-container",
    template: `
        <supr-credits-onboarding-balance
            [scBalance]="suprCreditsBalance$ | async"
        ></supr-credits-onboarding-balance>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCreditsOnboardingBalanceContainer {
    suprCreditsBalance$: Observable<number>;

    constructor(private adapter: Adapter) {
        this.suprCreditsBalance$ = this.adapter.suprCreditsBalance$;
    }
}
