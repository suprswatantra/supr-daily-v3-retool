import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { MileStoneHome } from "@models";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-milestone-container",
    template: `
        <supr-home-milestone
            [mileStone]="mileStone$ | async"
            [startCurrentMilestoneState]="startCurrentMilestoneState$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            [selectMilestoneState]="selectMilestoneState$ | async"
            (handleFetchMileStone)="fetchMileStone()"
            (handleFetchUserStartMileStone)="postUserMilestone($event)"
        ></supr-home-milestone>
    `,
    changeDetection: ChangeDetectionStrategy.Default,
})
export class MileStoneContainer implements OnInit {
    mileStone$: Observable<MileStoneHome>;
    startCurrentMilestoneState$: Observable<string>;
    appLaunchDone$: Observable<boolean>;
    selectMilestoneState$: Observable<string>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.mileStone$ = this.adapter.mileStone$;
        this.startCurrentMilestoneState$ = this.adapter.startMilestoneCurrentState$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
        this.selectMilestoneState$ = this.adapter.selectMilestoneState$;
    }

    fetchMileStone() {
        this.adapter.fetchMileStone();
    }

    postUserMilestone(userMileStoneId: number) {
        this.adapter.postUserMilestone(userMileStoneId);
    }
}
