import {
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { PauseAdapter } from "@shared/adapters/pause.adapter";

import { DeliveryStatus, Vacation, SystemVacation } from "@models";

import { VacationService } from "@services/shared/vacation.service";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-delivery-status-container",
    template: `
        <supr-home-delivery-status
            [vacation]="vacation$ | async"
            [isFetching]="isFetching$ | async"
            [refreshStatus]="refreshStatus$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            [systemVacation]="systemVacation$ | async"
            [deliveryStatus]="deliveryStatus$ | async"
            [selectCurrentDeliveryStatusStoreState]="
                selectCurrentDeliveryStatusStoreState$ | async
            "
            (handleModifyVacation)="openVacationModal()"
            (handlefetchDeliveryStatus)="fetchDeliveryStatus()"
        >
        </supr-home-delivery-status>

        <supr-home-delivery-notification
            [isFetching]="isFetching$ | async"
            [refreshStatus]="refreshStatus$ | async"
            [deliveryStatus]="deliveryStatus$ | async"
        >
        </supr-home-delivery-notification>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusContainer implements OnInit {
    @Output() handleNotice: EventEmitter<boolean> = new EventEmitter();

    vacation$: Observable<Vacation>;
    isFetching$: Observable<boolean>;
    refreshStatus$: Observable<boolean>;
    appLaunchDone$: Observable<boolean>;
    systemVacation$: Observable<SystemVacation>;
    deliveryStatus$: Observable<DeliveryStatus>;
    selectCurrentDeliveryStatusStoreState$: Observable<string>;

    constructor(
        private adapter: Adapter,
        private vacationAdapter: PauseAdapter,
        private vacationService: VacationService
    ) {}

    ngOnInit() {
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
        this.deliveryStatus$ = this.adapter.deliveryStatus$;
        this.selectCurrentDeliveryStatusStoreState$ = this.adapter.selectCurrentDeliveryStatusStoreState$;
        this.refreshStatus$ = this.adapter.refreshDeliveryStatus$;
        this.isFetching$ = this.adapter.isFetchingDeliveryStatus$;
        this.systemVacation$ = this.vacationAdapter.systemVacation$;
        this.vacation$ = this.vacationAdapter.vacation$.pipe(
            map((vacation: Vacation) =>
                this.vacationService.isTomorrowInVacation(vacation)
                    ? vacation
                    : null
            )
        );
        this.systemVacation$ = this.vacationAdapter.systemVacation$.pipe(
            map((vacation: SystemVacation) =>
                this.vacationService.isTomorrowInVacation(vacation)
                    ? vacation
                    : null
            )
        );
    }

    fetchDeliveryStatus() {
        this.adapter.fetchDeliveryStatus();
    }

    openVacationModal() {
        this.vacationAdapter.togglePauseModal();
    }
}
