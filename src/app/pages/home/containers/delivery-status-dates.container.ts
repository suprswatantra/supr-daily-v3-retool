import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { PauseAdapter } from "@shared/adapters/pause.adapter";

import { Vacation, SystemVacation, DelivereyStatusCurrentWeek } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { ScheduleDict } from "@types";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-delivery-status-dates-container",
    template: `
        <supr-home-delivery-status-dates
            [currentWeek]="currentWeek"
            [vacation]="vacation$ | async"
            [dateToHighlight]="dateToHighlight"
            [scheduleDict]="scheduleDict$ | async"
            [systemVacation]="systemVacation$ | async"
            (routeToSchedulePage)="routeToSchedulePage($event)"
        ></supr-home-delivery-status-dates>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusDatesContainer implements OnInit {
    @Input() dateToHighlight: string;
    @Input() currentWeek: Array<DelivereyStatusCurrentWeek> = [];

    vacation$: Observable<Vacation>;
    scheduleDict$: Observable<ScheduleDict>;
    systemVacation$: Observable<SystemVacation>;

    constructor(
        private adapter: Adapter,
        private utilService: UtilService,
        private pauseAdapter: PauseAdapter,
        private routerService: RouterService,
        private scheduleService: ScheduleService
    ) {}

    ngOnInit() {
        this.vacation$ = this.pauseAdapter.vacation$;
        this.scheduleDict$ = this.adapter.scheduleDict$;
        this.systemVacation$ = this.pauseAdapter.systemVacation$;
    }

    routeToSchedulePage(d: DelivereyStatusCurrentWeek) {
        if (
            this.utilService.isLengthyArray(this.currentWeek) &&
            !this.utilService.isEmpty(this.currentWeek[0])
        ) {
            this.scheduleService.setCustomScheduleStartDate(
                this.currentWeek[0].formattedDate
            );
            this.scheduleService.setCustomScheduleScrollDate(d.formattedDate);
        }

        this.routerService.goToSchedulePage();
    }
}
