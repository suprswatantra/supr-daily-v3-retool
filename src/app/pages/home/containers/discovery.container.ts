import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { CartMini } from "@types";

import { HomePageAdapter as Adapter } from "@pages/home/services/home.adapter";

@Component({
    selector: "supr-home-discovery-container",
    template: `
        <supr-home-discovery
            [cartPresent]="cartPresent$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [isExperimentsFetched]="isExperimentsFetched$ | async"
        ></supr-home-discovery>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiscoveryContainer implements OnInit {
    cartPresent$: Observable<boolean>;
    appLaunchDone$: Observable<boolean>;
    isAnonymousUser$: Observable<boolean>;
    isExperimentsFetched$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.cartPresent$ = this.adapter.miniCart$.pipe(
            map((miniCart: CartMini) => miniCart.itemCount > 0)
        );

        this.appLaunchDone$ = this.adapter.appLaunchDone$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
        this.isExperimentsFetched$ = this.adapter.isExperimentsFetched$;
    }
}
