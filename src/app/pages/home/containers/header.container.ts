import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { Subscription } from "@models";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-header-container",
    template: `
        <supr-home-header
            [balance]="walletBalance$ | async"
            [subscriptionList]="subscriptionList$ | async"
        ></supr-home-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderContainer implements OnInit {
    walletBalance$: Observable<number>;
    subscriptionList$: Observable<Subscription[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.walletBalance$ = this.adapter.walletBalance$;
        this.subscriptionList$ = this.adapter.subscriptionList$;
    }
}
