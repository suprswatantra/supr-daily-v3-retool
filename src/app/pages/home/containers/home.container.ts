import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { RewardsDetails } from "@shared/models";

import { ErrorCurrentState } from "@store/error/error.state";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

import { Address, User } from "@models";
import { UserCurrentState } from "@store/user/user.state";
import { GlobalError } from "@types";

import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-home-container",
    template: `
        <supr-home-layout-new
            [refreshDeliveryStatus]="refreshDeliveryStatus$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            [errorState]="errorState$ | async"
            [suprCreditsEnabled]="suprCreditsEnabled$ | async"
            [suprCreditsBalance]="suprCreditsBalance$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [rewardsEnabled]="rewardsEnabled$ | async"
            [rewardsDetails]="rewardsDetails$ | async"
            [user]="user$ | async"
            [address]="address$ | async"
            [userState]="userState$ | async"
            [error]="error$ | async"
            [settingsFetched]="settingsFetched$ | async"
            [experimentsFetched]="experimentsFetched$ | async"
            (handleDeliveryStatusRefresh)="disableDeliveryStatusRefresh()"
            (handleToggleAppLaunchDone)="toggleAppLaunchDone()"
            (setAppInitDone)="setAppInitDone()"
            (handleLoadProfile)="fetchProfile()"
            (handleFetchSettings)="fetchSettings()"
        ></supr-home-layout-new>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeContainer implements OnInit {
    refreshDeliveryStatus$: Observable<boolean>;
    appLaunchDone$: Observable<boolean>;
    errorState$: Observable<ErrorCurrentState>;
    suprCreditsEnabled$: Observable<boolean>;
    suprCreditsBalance$: Observable<number>;
    isAnonymousUser$: Observable<boolean>;
    rewardsEnabled$: Observable<boolean>;
    rewardsDetails$: Observable<RewardsDetails>;

    user$: Observable<User>;
    address$: Observable<Address>;
    userState$: Observable<UserCurrentState>;
    error$: Observable<GlobalError>;
    settingsFetched$: Observable<boolean>;
    experimentsFetched$: Observable<boolean>;

    constructor(
        private adapter: Adapter,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.refreshDeliveryStatus$ = this.adapter.refreshDeliveryStatus$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
        this.errorState$ = this.adapter.errorState$;
        this.suprCreditsEnabled$ = this.adapter.suprCreditsEnabled$;
        this.suprCreditsBalance$ = this.adapter.suprCreditsBalance$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
        this.rewardsEnabled$ = this.adapter.rewardsEnabled$;
        this.rewardsDetails$ = this.adapter.rewardsDetails$;

        this.user$ = this.adapter.user$;
        this.address$ = this.adapter.address$;
        this.userState$ = this.adapter.userState$;
        this.error$ = this.adapter.error$;
        this.settingsFetched$ = this.adapter.settingsFetched$;
        this.experimentsFetched$ = this.adapter.experimentsFetched$;
    }

    disableDeliveryStatusRefresh() {
        this.adapter.disableRefreshDeliveryStatus();
    }

    toggleAppLaunchDone() {
        this.adapter.toggleAppLaunchDone();
    }

    setAppInitDone() {
        this.adapter.setAppInitDone();
    }

    fetchProfile() {
        this.adapter.fetchProfileWithAddress();
    }

    fetchSettings() {
        this.settingsService.fetchSettings();
    }
}
