import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { V3Layout } from "@models";

import { HomePageAdapter as Adapter } from "@pages/home/services/home.adapter";
import { HomeLayoutCurrentState } from "@store/home-layout/home-layout.state";

@Component({
    selector: "supr-home-layout-container",
    template: `
        <supr-home-layouts
            [homeLayoutList]="homeLayoutList$ | async"
            [homeLayoutState]="homeLayoutState$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            (handleFetchHomeLayouts)="fetchHomeLayouts()"
        ></supr-home-layouts>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeLayoutContainer implements OnInit {
    @Input() position: string;

    homeLayoutList$: Observable<V3Layout[]>;
    homeLayoutState$: Observable<HomeLayoutCurrentState>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.homeLayoutList$ = this.adapter.fetchHomeLayoutListForPosition(
            this.position
        );
        this.homeLayoutState$ = this.adapter.homeLayoutCurrentState$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }

    fetchHomeLayouts() {
        this.adapter.fetchHomeLayoutList();
    }
}
