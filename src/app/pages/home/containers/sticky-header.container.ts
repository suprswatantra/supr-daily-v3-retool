import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { Subscription } from "@models";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-sticky-header-container",
    template: `
        <supr-home-sticky-header
            [scrollTop]="scrollTop"
            [balance]="walletBalance$ | async"
            [subscriptionList]="subscriptionList$ | async"
            [suprCreditsEnabled]="suprCreditsEnabled$ | async"
            [suprCreditsBalance]="suprCreditsBalance$ | async"
            [subscriptionCurrentState]="subscriptionCurrentState$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [appLaunchDone]="appLaunchDone"
            (handleFetchSubscriptions)="handleFetchSubscriptions()"
        ></supr-home-sticky-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StickyHeaderContainer implements OnInit {
    @Input() scrollTop: number;
    @Input() appLaunchDone: boolean;

    walletBalance$: Observable<number>;
    subscriptionList$: Observable<Subscription[]>;
    suprCreditsEnabled$: Observable<boolean>;
    suprCreditsBalance$: Observable<number>;
    subscriptionCurrentState$: Observable<SubscriptionCurrentState>;
    isAnonymousUser$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.walletBalance$ = this.adapter.walletBalance$;
        this.subscriptionList$ = this.adapter.subscriptionList$;
        this.suprCreditsEnabled$ = this.adapter.suprCreditsEnabled$;
        this.suprCreditsBalance$ = this.adapter.suprCreditsBalance$;
        this.subscriptionCurrentState$ = this.adapter.subscriptionCurrentState$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
    }

    handleFetchSubscriptions() {
        this.adapter.fetchSubscriptionList();
    }
}
