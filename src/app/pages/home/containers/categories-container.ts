import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-categories-container",
    template: `
        <supr-home-categories
            [isFetching]="isFetching$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
        ></supr-home-categories>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesContainer implements OnInit {
    isFetching$: Observable<boolean>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isFetching$ = this.adapter.isFetchingCategories$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }
}
