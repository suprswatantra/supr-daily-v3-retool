import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { Collection } from "@models";
import { CollectionCurrentState } from "@store/collection/collection.state";

import { HomePageAdapter as Adapter } from "@pages/home/services/home.adapter";

@Component({
    selector: "supr-home-collections-container",
    template: `
        <supr-home-collections
            [collectionList]="collectionList$ | async"
            [collectionState]="collectionState$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            (handleFetchCollections)="fetchCollections()"
        ></supr-home-collections>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionsContainer implements OnInit {
    @Input() position: string;

    collectionList$: Observable<Collection[]>;
    collectionState$: Observable<CollectionCurrentState>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.collectionList$ = this.adapter.fetchCollectionListForPosition(
            this.position
        );
        this.collectionState$ = this.adapter.collectionCurrentState$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }

    fetchCollections() {
        this.adapter.fetchCollectionList();
    }
}
