import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { Category } from "@models";
import { EssentialsCurrentState } from "@store/essentials/essentials.state";

import { HomePageAdapter as Adapter } from "../services/home.adapter";

@Component({
    selector: "supr-home-essentials-container",
    template: `
        <supr-home-essentials
            [essentialList]="essentialList$ | async"
            [essentialsState]="essentialsState$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            (handleFetchEssentials)="fetchEssentialList()"
        ></supr-home-essentials>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EssentialsContainer implements OnInit {
    essentialList$: Observable<Category[]>;
    essentialsState$: Observable<EssentialsCurrentState>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.essentialList$ = this.adapter.essentialList$;
        this.essentialsState$ = this.adapter.essentialsCurrentState$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }

    fetchEssentialList() {
        this.adapter.fetchEssentialList();
    }
}
