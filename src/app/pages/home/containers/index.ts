import { HomeContainer } from "./home.container";
import { EssentialsContainer } from "./essentials.container";
import { HeaderContainer } from "./header.container";
import { DeliveryStatusContainer } from "./delivery-status.container";
import { DiscoveryContainer } from "./discovery.container";
import { CategoriesContainer } from "./categories-container";
import { CollectionsContainer } from "./collections-container";
import { StickyHeaderContainer } from "./sticky-header.container";
import { DeliveryStatusDatesContainer } from "./delivery-status-dates.container";
import { HomeLayoutContainer } from "./home-layout-container";
import { MileStoneContainer } from "./milestone-container";

export const homePageContainers = [
    HomeContainer,
    EssentialsContainer,
    HeaderContainer,
    DeliveryStatusContainer,
    DeliveryStatusDatesContainer,
    DiscoveryContainer,
    CategoriesContainer,
    CollectionsContainer,
    StickyHeaderContainer,
    HomeLayoutContainer,
    MileStoneContainer,
];
