import {
    AfterViewInit,
    Component,
    OnInit,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    SimpleChange,
    OnDestroy,
} from "@angular/core";

import { NavigationEnd, Router } from "@angular/router";

import { Subscription, of } from "rxjs";
import { filter, catchError } from "rxjs/operators";

import {
    PAGE_ROUTES,
    SCROLL_TYPE,
    AUTO_SCROLL_INFO,
    STORAGE_DB_DATA_KEYS,
} from "@constants";

import { CronService } from "@services/shared/cron.service";
import { DbService } from "@services/data/db.service";
import { StoreService } from "@services/data/store.service";
import { InitializeService } from "@services/util/initialize.service";
import { PlatformService } from "@services/util/platform.service";
import { RouterService } from "@services/util/router.service";
import { NudgeService } from "@services/layout/nudge.service";
import { HomePageService } from "@pages/home/services/home.service";
import { UtilService } from "@services/util/util.service";
import { AuthService } from "@services/data/auth.service";

import { User, Address, RewardsDetails } from "@models";
import { ErrorCurrentState } from "@store/error/error.state";
import { UserCurrentState } from "@store/user/user.state";
import { GlobalError } from "@types";

import {
    DEFAULT_REWARDS_ONBOARDING_IMG_URL,
    TEXTS,
} from "../constants/home.constants";

@Component({
    selector: "supr-home-layout-new",
    template: `
        <div class="suprContainer">
            <ion-menu
                side="start"
                menuId="home-menu"
                contentId="home-content"
                type="overlay"
                [swipeGesture]="false"
            >
                <supr-sidemenu-home></supr-sidemenu-home>
            </ion-menu>

            <ion-content id="home-content">
                <div
                    class="scrollable"
                    id="homeScrollContent"
                    (scroll)="onScroll($event)"
                >
                    <!-- <supr-home-sticky-header-container
                        [appLaunchDone]="appLaunchDone"
                        [scrollTop]="scrollTop"
                    ></supr-home-sticky-header-container>

                    <supr-home-delivery-status-container
                        *ngIf="
                            !appLaunchDone ||
                            (appLaunchDone && !isAnonymousUser)
                        "
                    ></supr-home-delivery-status-container> -->

                    <supr-home-discovery-container></supr-home-discovery-container>
                </div>
            </ion-content>
            <supr-cart *ngIf="appLaunchDone"></supr-cart>
        </div>

        <!-- Rewards onboarding modal -->
        <supr-rewards-onboarding
            [showModal]="showRewardsOnboarding"
            [imgUrl]="
                rewardsDetails?.onboardingImgUrl || rewardsOnboardingDefaultImg
            "
            btnText="${TEXTS.REWARDS_ONBOARDING_BTN}"
            (handleClose)="closeRewardsOnboardingModal()"
            (handleBtnClick)="closeModalAndGoToRewardsPage()"
        ></supr-rewards-onboarding>
    `,
    styleUrls: ["../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageLayoutComponentNew
    implements OnInit, OnChanges, OnDestroy, AfterViewInit {
    @Input() refreshDeliveryStatus: boolean;
    @Input() appLaunchDone: boolean;
    @Input() errorState: ErrorCurrentState;
    @Input() suprCreditsBalance: number;
    @Input() suprCreditsEnabled: boolean;
    @Input() isAnonymousUser: boolean;
    @Input() rewardsEnabled: boolean;
    @Input() rewardsDetails: RewardsDetails;

    @Output()
    handleDeliveryStatusRefresh: EventEmitter<void> = new EventEmitter();

    @Output()
    handleToggleAppLaunchDone: EventEmitter<void> = new EventEmitter();

    @Input() user: User;
    @Input() userState: UserCurrentState;
    @Input() address: Address;
    @Input() error: GlobalError;
    @Input() settingsFetched: boolean;
    @Input() experimentsFetched: boolean;

    @Output() handleLoadProfile: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchSettings: EventEmitter<void> = new EventEmitter();
    @Output() setAppInitDone: EventEmitter<void> = new EventEmitter();

    isInitData = false;
    scrollTop = 0;
    showRewardsOnboarding = false;
    rewardsOnboardingDefaultImg = DEFAULT_REWARDS_ONBOARDING_IMG_URL;

    private sessionLoading = true;
    private routerSub: Subscription;

    constructor(
        private platformService: PlatformService,
        private initializeService: InitializeService,
        private routerService: RouterService,
        private storeService: StoreService,
        private cronService: CronService,
        private dbService: DbService,
        private router: Router,
        private nudgeService: NudgeService,
        private homePageService: HomePageService,
        private utilService: UtilService,
        private authService: AuthService
    ) {
        this.subscribeToRouterEvents();
    }

    ngOnInit() {
        this.hideSplashScreen();

        this.loadSession();
        if (!this.settingsFetched) {
            this.handleFetchSettings.emit();
        }
    }

    ngAfterViewInit() {
        this.homePageService.trackInstalledApps();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleRefreshDeliveryStatus(changes["refreshDeliveryStatus"]);
        this.handleAppLaunchChange(changes["appLaunchDone"]);

        this.handleUserStateChange(changes["userState"]);
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    ionViewWillEnter() {
        this.platformService.setStatusBarForHomePage();
    }

    closeRewardsOnboardingModal() {
        this.showRewardsOnboarding = false;
        this.setScratchCardsEarnedToDb();
    }

    closeModalAndGoToRewardsPage() {
        this.closeRewardsOnboardingModal();
        this.setScratchCardsEarnedToDb();
        this.routerService.goToRewardsPage();
    }

    onScroll(e) {
        this.scrollTop = e.target.scrollTop;
    }

    private initData() {
        if (this.isInitData) {
            return;
        }
        this.isInitData = true;
        // Set the flags on
        if (!this.appLaunchDone) {
            this.handleToggleAppLaunchDone.emit();
        }
        this.platformService.setStatusBarForHomePage();
        this.setAppInitDone.emit();

        // Make API calls
        const launchSubscription = this.initializeService
            .getLaunchData(this.isAnonymousUser)
            .subscribe(
                () => this.setLaunchDone(), // setup done, update the flag
                () => {
                    /** Do nothing, global error component takes care this */
                },
                () => {
                    if (launchSubscription && !launchSubscription.closed) {
                        launchSubscription.unsubscribe();
                    }
                }
            );
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isHomePageUrl(event.url))
            )
            .subscribe((event) => {
                // Check for SuprCredits onboarding
                // Removing Supr Credits Onboarding [Temporary]
                // this.handleSuprCreditsOnboarding();
                this.setShowRewardsOnboarding();

                if (event instanceof NavigationEnd) {
                    // Send nudge
                    this.nudgeService.sendCampaignPopupNudge("home");
                    this.autoScrollIfAvailable();
                }
            });
    }

    private isHomePageUrl(url: string): boolean {
        return url && url.indexOf(PAGE_ROUTES.HOME.PATH) > -1;
    }

    private handleRefreshDeliveryStatus(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            setTimeout(() => {
                this.handleDeliveryStatusRefresh.emit();
            }, 100);
        }
    }

    private handleAppLaunchChange(change: SimpleChange) {
        if (change && !change.previousValue && change.currentValue) {
            this.initData();
        }
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    private setLaunchDone() {
        // Check for auto pushing old dates in cart
        this.storeService.checkCartDates();

        // Start the home page crons
        this.cronService.initHomePageCrons();

        // Check for SuprCredits onboarding
        // Removing Supr Credits Onboarding [Temporary]
        // this.handleSuprCreditsOnboarding();
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }

    private scrollToContent(elemId: string) {
        setTimeout(() => {
            this.utilService.scrollToPageSection(elemId);
            this.replaceHomePageUrl();
        }, 200);
    }

    private replaceHomePageUrl() {
        this.router.navigate([PAGE_ROUTES.HOME.PATH], { replaceUrl: true });
    }

    private autoScrollIfAvailable() {
        const scrollPage = this.getQueryParamByName(
            AUTO_SCROLL_INFO.query_param.page
        );

        if (scrollPage === AUTO_SCROLL_INFO.pages.home) {
            const vScrollType = this.getQueryParamByName(
                AUTO_SCROLL_INFO.query_param.type
            );

            if (vScrollType === SCROLL_TYPE.collection) {
                const scrollId = this.getQueryParamByName(
                    AUTO_SCROLL_INFO.query_param.id
                );

                if (scrollId) {
                    this.scrollToContent(
                        AUTO_SCROLL_INFO.prefix_collection + scrollId
                    );
                }
            } else if (vScrollType === SCROLL_TYPE.category) {
                this.scrollToContent(AUTO_SCROLL_INFO.prefix_home_category);
            } else if (vScrollType === SCROLL_TYPE.feature) {
                this.scrollToContent(AUTO_SCROLL_INFO.prefix_home_feature);
            }
        }
    }

    private getQueryParamByName(param_name = "") {
        if (!param_name) {
            return;
        }

        const match = RegExp("[?&]" + param_name + "=([^&]*)").exec(
            window.location.search
        );
        return match && decodeURIComponent(match[1].replace(/\+/g, " "));
    }

    private async setShowRewardsOnboarding() {
        if (!this.rewardsEnabled) {
            this.showRewardsOnboarding = false;
            return;
        }

        const scratchCardsEarned = this.utilService.getNestedValue(
            this.rewardsDetails,
            "scratchCardsEarned",
            0
        );

        if (!scratchCardsEarned) {
            this.showRewardsOnboarding = false;
            return;
        }

        this.showRewardsOnboarding = await this.canOpenrewardsOnboardingModal(
            scratchCardsEarned
        );
    }

    private async canOpenrewardsOnboardingModal(
        scratchCardsEarned: number
    ): Promise<boolean> {
        const scratchCardsEarnedCountFromDb = await this.getScratchCardsEarnedCountFromDb();

        if (scratchCardsEarned > scratchCardsEarnedCountFromDb) {
            return true;
        }

        return false;
    }

    private async getScratchCardsEarnedCountFromDb(): Promise<number> {
        const scratchCardsEarnedCount = await this.dbService.getData(
            STORAGE_DB_DATA_KEYS.SCRATCH_CARDS_EARNED_COUNT
        );

        return scratchCardsEarnedCount || 0;
    }

    private async setScratchCardsEarnedToDb() {
        const scratchCardsEarned = this.utilService.getNestedValue(
            this.rewardsDetails,
            "scratchCardsEarned",
            0
        );

        if (!scratchCardsEarned) {
            return;
        }

        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.SCRATCH_CARDS_EARNED_COUNT,
            scratchCardsEarned
        );
    }

    private handleUserStateChange(change: SimpleChange) {
        if (
            change &&
            change.previousValue === UserCurrentState.FETCHING_USER &&
            change.currentValue === UserCurrentState.NO_ACTION
        ) {
            this.handleChange();
        }
    }

    private handleChange() {
        if (
            this.userState === UserCurrentState.FETCHING_USER ||
            this.sessionLoading
        ) {
            return;
        }

        if (this.userState === UserCurrentState.NO_ACTION && this.user) {
            this.handleUserFlow(this.user, this.address);
        }
    }

    private handleUserFlow(user?: User, address?: Address) {
        if (this.globalErrorPresent()) {
            return;
        }

        this.initializeService.handleUserFlow(user, address, true);
        this.initData();
    }

    private globalErrorPresent(): boolean {
        return this.errorState !== ErrorCurrentState.NO_ERROR;
    }

    private loadSession() {
        const subscription = this.authService
            .loadSession()
            .pipe(catchError(() => of("")))
            .subscribe((token: string) => {
                this.sessionLoading = false;

                if (subscription && !subscription.closed) {
                    subscription.unsubscribe();
                }

                if (token) {
                    this.handleLoadProfile.emit();
                } else {
                    this.setAppInitDone.emit();
                    this.routerService.goToLoginPage();
                }
            });
    }

    private hideSplashScreen(timeout = 300) {
        setTimeout(() => {
            this.platformService.hideSplashScreen();
        }, timeout);
    }
}
