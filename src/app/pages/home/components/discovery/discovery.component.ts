import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import {
    FEATURED_CARD_CONSTANTS,
    SETTINGS,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { CollectionPositions, HomeLayoutPositions } from "@shared/models";

import { SettingsService } from "@services/shared/settings.service";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-home-discovery",
    template: `
        <div class="discovery" [class.cartPresent]="cartPresent">
            <!-- <supr-home-milestone-container></supr-home-milestone-container> -->

            <supr-home-layout-container
                position="${HomeLayoutPositions.HOME_SECTION_4}"
            ></supr-home-layout-container>

            <!-- <ng-container *ngIf="appLaunchDone && showSuprPassCard">
                <supr-pass-card
                    [showSmallCard]="true"
                    saCardContext="${ANALYTICS_OBJECT_NAMES.CONTEXT.DISCOVERY}"
                ></supr-pass-card>
            </ng-container> -->

            <!-- <supr-feature-card
                *ngIf="!appLaunchDone || (appLaunchDone && !isAnonymousUser)"
                componentName="${FEATURED_CARD_CONSTANTS.HOME}"
            ></supr-feature-card>

            <supr-home-collections-container
                position="${CollectionPositions.HOME_SECTION_1}"
            ></supr-home-collections-container> -->

            <supr-home-layout-container
                position="${HomeLayoutPositions.HOME_SECTION_1}"
            ></supr-home-layout-container>

            <!-- <supr-home-categories-container></supr-home-categories-container> -->

            <!-- <supr-home-collections-container
                position="${CollectionPositions.HOME_SECTION_2}"
            ></supr-home-collections-container> -->
            <supr-home-layout-container
                position="${HomeLayoutPositions.HOME_SECTION_2}"
            ></supr-home-layout-container>

            <!-- <supr-home-essentials-container></supr-home-essentials-container> -->

            <!-- <supr-home-collections-container
                position="${CollectionPositions.HOME_SECTION_3}"
            ></supr-home-collections-container> -->
            <supr-home-layout-container
                position="${HomeLayoutPositions.HOME_SECTION_3}"
            ></supr-home-layout-container>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiscoveryComponent implements OnInit, OnChanges {
    @Input() cartPresent: boolean;
    @Input() appLaunchDone: boolean;
    @Input() isAnonymousUser: boolean;
    @Input() isExperimentsFetched: boolean;

    showSuprPassCard: boolean;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this.setShowSuprPassCard();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["isExperimentsFetched"];

        if (this.canHandleChange(change)) {
            this.setShowSuprPassCard();
        }
    }

    private setShowSuprPassCard() {
        this.showSuprPassCard = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_SUPR_PASS_HOMEPAGE_CARD,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.SHOW_SUPR_PASS_HOMEPAGE_CARD]
        );
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
