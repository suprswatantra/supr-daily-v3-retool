import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
} from "@angular/core";

@Component({
    selector: "supr-home-sticky-section",
    template: `
        <div class="sticky" [class.withNotice]="showNotice">
            <div class="stickyContent">
                <supr-home-header-container></supr-home-header-container>
                <supr-home-delivery-status-container
                    (handleNotice)="updateNotice($event)"
                ></supr-home-delivery-status-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StickySectionComponent {
    @Output() handleNotice: EventEmitter<boolean> = new EventEmitter();

    showNotice = false;

    updateNotice(showNotice: boolean) {
        this.showNotice = showNotice;
        this.handleNotice.emit(this.showNotice);
    }
}
