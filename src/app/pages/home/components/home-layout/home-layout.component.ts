import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { V3Layout, HomeLayoutPositions } from "@models";

import { HomeLayoutCurrentState } from "@store/home-layout/home-layout.state";

@Component({
    selector: "supr-home-layouts",
    template: `
        <ng-container *ngIf="!loading && appLaunchDone">
            <!-- <div class="discoverySpacing" *ngIf="homeLayoutList?.length"></div> -->
            <ng-container
                *ngFor="
                    let layout of homeLayoutList;
                    trackBy: trackByFn;
                    index as position;
                    last as isLast
                "
            >
                <supr-v3-widget-container
                    [layout]="layout"
                    saImpression
                    [saImpressionEnabled]="
                        layout?.data?.analytics?.saImpression
                    "
                    [saObjectName]="
                        layout?.data?.analytics?.saImpression?.objectName
                    "
                    [saObjectTag]="
                        layout?.data?.analytics?.saImpression?.objectTag
                    "
                    [saObjectValue]="
                        layout?.data?.analytics?.saImpression?.objectValue
                    "
                    [saPosition]="position + 1"
                ></supr-v3-widget-container>

                <div class="divider24" *ngIf="!isLast"></div>
                <div class="divider24" *ngIf="isLast"></div>
            </ng-container>
        </ng-container>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeLayoutsComponent implements OnInit, OnChanges {
    @Input() homeLayoutList: V3Layout[];
    @Input() homeLayoutState: HomeLayoutCurrentState;
    @Input() appLaunchDone: boolean;

    @Output() handleFetchHomeLayouts: EventEmitter<void> = new EventEmitter();

    loading = true;
    section4 = HomeLayoutPositions.HOME_SECTION_4;

    ngOnInit() {
        this.fetchHomeLayoutList();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleHomeLayoutStateChange(changes["homeLayoutState"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    trackByFn(homeLayout: V3Layout): string {
        return homeLayout.widgetType;
    }

    private fetchHomeLayoutList() {
        if (
            this.homeLayoutState === HomeLayoutCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.handleFetchHomeLayouts.emit();
        } else {
            this.loading = false;
        }
    }

    private handleHomeLayoutStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        const { previousValue, currentValue } = change;
        if (
            previousValue !== HomeLayoutCurrentState.NO_ACTION &&
            currentValue === HomeLayoutCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.fetchHomeLayoutList();
        } else if (this.appLaunchDone && this.fetchHomeLayoutListDone()) {
            this.loading = false;
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            if (this.appLaunchDone && this.fetchHomeLayoutListDone()) {
                this.loading = false;
            } else if (this.appLaunchDone && !this.fetchHomeLayoutListDone()) {
                this.fetchHomeLayoutList();
            }
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchHomeLayoutListDone(): boolean {
        return (
            this.homeLayoutState ===
            HomeLayoutCurrentState.FETCHING_HOME_LAYOUT_DONE
        );
    }
}
