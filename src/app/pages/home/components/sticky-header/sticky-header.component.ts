import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    OnInit,
    OnChanges,
    Output,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import {
    SEARCH_HEADER_TEXT,
    SUB_HEADER_HEIGHT,
    SEARCH_BAR_FIXED_CLASS_NAME,
} from "@pages/home/constants/home.constants";

import { Subscription } from "@models";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { RouterService } from "@services/util/router.service";
import { SubscriptionService } from "@services/shared/subscription.service";
import { UtilService } from "@services/util/util.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";
import { WalletService } from "@services/shared/wallet.service";
import { DateService } from "@services/date/date.service";

@Component({
    selector: "supr-home-sticky-header",
    template: `
        <div class="stickyHeaderWrapper">
            <div class="stickyHeader">
                <supr-side-menu-opener name="home-menu"></supr-side-menu-opener>

                <ng-container *ngIf="isFetchingSubscriptions(); else content">
                    <ion-row class="ion-justify-content-end shimmer">
                        <ion-skeleton-text
                            animated
                            class="shimmerChip"
                        ></ion-skeleton-text>
                        <ion-skeleton-text
                            animated
                            class="shimmerChip"
                        ></ion-skeleton-text>
                    </ion-row>
                </ng-container>

                <ng-template #content>
                    <ng-container *ngIf="showLogo && !hideChips">
                        <div class="stickyHeaderLogoWrapper">
                            <ion-row class="logo ion-justify-content-center">
                                <supr-svg></supr-svg>
                                <supr-button
                                    class="addMoney"
                                    (handleClick)="goToWalletPage()"
                                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                        .ADD_MONEY}"
                                    *ngIf="!isAnonymousUser"
                                >
                                    <div class="suprRow">
                                        <supr-icon name="add"></supr-icon>
                                        <supr-text type="paragraph"
                                            >Add money</supr-text
                                        >
                                    </div>
                                </supr-button>
                            </ion-row>
                        </div>
                    </ng-container>

                    <ng-container *ngIf="!showLogo && !hideChips">
                        <div class="stickyHeaderChipWrapper">
                            <ion-row class="ion-justify-content-end">
                                <!-- <ng-container *ngIf="subscriptionCountStr"> -->
                                <supr-chip
                                    (handleClick)="goToSubscriptionsPage()"
                                    [ngClass]="subscriptionChipClass"
                                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                        .HOME_SUBSCRIPTION_CHIP}"
                                    [saObjectValue]="subscriptionChipClass"
                                >
                                    <supr-icon
                                        *ngIf="subscriptionChipIcon"
                                        [name]="subscriptionChipIcon"
                                    ></supr-icon>
                                    <div class="spacer4"></div>
                                    <supr-text type="paragraph">
                                        {{
                                            subscriptionCountStr ||
                                                "My Subscriptions"
                                        }}
                                    </supr-text>
                                </supr-chip>
                                <!-- </ng-container> -->

                                <ng-container
                                    *ngIf="
                                        !suprCreditsEnabled;
                                        else suprCredits
                                    "
                                >
                                    <supr-chip
                                        (handleClick)="goToWalletPage()"
                                        saObjectName="${ANALYTICS_OBJECT_NAMES
                                            .CLICK.HOME_WALLET_BALANCE}"
                                    >
                                        <supr-text type="paragraph">
                                            {{ balance | rupee }} Balance
                                        </supr-text>
                                    </supr-chip>
                                </ng-container>
                                <ng-template #suprCredits>
                                    <supr-chip
                                        class="scChip"
                                        (handleClick)="goToWalletPage()"
                                        saObjectName="${ANALYTICS_OBJECT_NAMES
                                            .CLICK.HOME_WALLET_BALANCE}"
                                    >
                                        <div class="walletBalance">
                                            <supr-text type="paragraph">
                                                {{ balance | rupee }}
                                            </supr-text>
                                        </div>
                                        <div class="scBalance suprRow center">
                                            <supr-text type="paragraph">{{
                                                suprCreditsBalance || 0
                                            }}</supr-text>
                                            <supr-svg class="scIcon"></supr-svg>
                                        </div>
                                    </supr-chip>
                                </ng-template>
                            </ion-row>
                        </div>
                    </ng-container>
                </ng-template>
            </div>
            <div class="stickyHeaderSpacing"></div>
            <div class="scrollableHeader">
                <div class="search">
                    <supr-text type="subheading"
                        >{{ headerText.heading }}
                        {{ headerText.subHeading }}</supr-text
                    >
                    <div class="divider12"></div>
                    <div
                        class="scrollableHeaderSearch"
                        [ngClass]="searchBarClassName"
                    >
                        <supr-search-box
                            (click)="goToSearchPage()"
                            saClick
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .HOME_SEARCH}"
                        ></supr-search-box>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/sticky-header.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StickyHeaderComponent implements OnInit, OnChanges {
    @Input() balance: number;
    @Input() subscriptionList: Subscription[] = [];
    @Input() scrollTop: number;
    @Input() suprCreditsBalance: number;
    @Input() suprCreditsEnabled: boolean;
    @Input() subscriptionCurrentState: SubscriptionCurrentState;
    @Input() isAnonymousUser: boolean;
    @Input() appLaunchDone: boolean;

    @Output() handleFetchSubscriptions: EventEmitter<void> = new EventEmitter();

    showLogo = true;
    searchBarClassName: string;
    hideChips = false;
    subscriptionCountStr: string;
    subscriptionChipClass: string;
    subscriptionChipIcon: string;
    headerText = {
        heading: "",
        subHeading: "",
    };

    constructor(
        private routerService: RouterService,
        private subscriptionService: SubscriptionService,
        private utilService: UtilService,
        private walletService: WalletService,
        private dateService: DateService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    initialize() {
        if (
            !this.utilService.isLengthyArray(this.subscriptionList) &&
            this.subscriptionCurrentState ===
                SubscriptionCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.handleFetchSubscriptions.emit();
        } else {
            this.initData();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["balance"] || changes["subscriptionList"];
        const scrollChange = changes["scrollTop"];
        const appLaunchChange = changes["appLaunchDone"];

        if (this.canHandleChange(appLaunchChange)) {
            this.initialize();
        }

        if (this.canHandleChange(change)) {
            this.initData();
        }

        if (this.canHandleChange(scrollChange)) {
            this.handleScrollChange();
        }
    }

    goToSubscriptionsPage() {
        this.routerService.goToSubscriptionListPage();
    }

    goToWalletPage() {
        this.routerService.goToWalletPage();
    }

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }

    isFetchingSubscriptions() {
        return (
            !this.isAnonymousUser &&
            this.subscriptionCurrentState ===
                SubscriptionCurrentState.FETCHING_SUBSCRIPTIONS
        );
    }

    private handleScrollChange() {
        if (this.scrollTop > SUB_HEADER_HEIGHT) {
            this.hideChips = true;
            this.searchBarClassName = SEARCH_BAR_FIXED_CLASS_NAME;
            return;
        }

        this.hideChips = false;
        this.searchBarClassName = "";
        return;
    }

    private initData() {
        if (
            !this.isAnonymousUser &&
            (this.balance > 0 ||
                this.suprCreditsBalance > 0 ||
                this.utilService.isLengthyArray(this.subscriptionList))
        ) {
            this.balance = this.roundOffAmount(this.balance);
            this.showLogo = false;
            this.hydrateSubscriptionChipData();
            if (this.suprCreditsBalance) {
                this.suprCreditsBalance = this.roundOffAmount(
                    this.suprCreditsBalance
                );
            }
            this.walletService.setWalletBalance(this.balance);
            this.walletService.setSuprCreditsBalance(this.suprCreditsBalance);
        } else {
            this.showLogo = true;
        }

        this.setHeaderText();
    }

    private roundOffAmount(value = 0) {
        return Math.floor(value);
    }

    private setHeaderText() {
        if (this.dateService.hasTodaysCutOffTimePassed()) {
            this.headerText = SEARCH_HEADER_TEXT.DURING_CUTOFF;
        } else {
            this.headerText = SEARCH_HEADER_TEXT.DEFAULT;
        }
    }

    private hydrateSubscriptionChipData() {
        if (this.utilService.isLengthyArray(this.subscriptionList)) {
            this.setSubscriptionChipInfo();
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setSubscriptionChipInfo() {
        const split = this.subscriptionService.splitSubscriptionsBasedOnStatus(
            this.subscriptionList
        );

        const subscriptionChipInfo = this.subscriptionService.getPrioritySubcriptionInfoChip(
            split
        );

        if (subscriptionChipInfo) {
            const { type, message, iconName } = subscriptionChipInfo;
            this.subscriptionChipClass = type;
            this.subscriptionCountStr = message;
            this.subscriptionChipIcon = iconName;
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setEmptySubscriptionValues() {
        this.subscriptionCountStr = null;
        this.subscriptionChipIcon = null;
        this.subscriptionChipClass = "";
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
