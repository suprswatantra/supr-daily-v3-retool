import { Component } from "@angular/core";

@Component({
    selector: "supr-home-delivery-info-skeleton",
    template: `
        <div class="deliveryInfo">
            <ion-skeleton-text class="heading"></ion-skeleton-text>
            <ion-skeleton-text class="subHeading"></ion-skeleton-text>
            <div class="productsWrapper">
                <div style="productsWrapperProducts">
                    <ion-skeleton-text class="round"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text2"></ion-skeleton-text>
                </div>
                <div style="productsWrapperProducts">
                    <ion-skeleton-text class="round"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text2"></ion-skeleton-text>
                </div>
                <div style="productsWrapperProducts">
                    <ion-skeleton-text class="round"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text1"></ion-skeleton-text>
                    <ion-skeleton-text class="text2"></ion-skeleton-text>
                </div>
                <div class="productsWrapperNext">
                    <ion-skeleton-text class="roundSmall"></ion-skeleton-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class DeliveryInfoSkeletonComponent {}
