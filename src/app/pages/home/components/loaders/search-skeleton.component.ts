import { Component } from "@angular/core";

@Component({
    selector: "supr-home-search-skeleton",
    template: `
        <div class="search">
            <ion-skeleton-text animated class="searchTitle"></ion-skeleton-text>
            <div class="divider8"></div>
            <ion-skeleton-text animated class="searchBox"></ion-skeleton-text>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class SearchSkeletonComponent {}
