import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "supr-home-launch-loader",
    template: `
        <div class="suprColumn launch center">
            <ng-container *ngIf="!retry; else showRetry">
                <supr-loader></supr-loader>
            </ng-container>

            <ng-template #showRetry>
                <supr-text type="subtitle">
                    Oops! Something went wrong
                </supr-text>
                <div class="divider16"></div>
                <supr-button *ngIf="retry" (handleClick)="handleRetry.emit()">
                    Retry
                </supr-button>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class LaunchLoaderComponent {
    @Input() retry = false;
    @Output() handleRetry: EventEmitter<void> = new EventEmitter();
}
