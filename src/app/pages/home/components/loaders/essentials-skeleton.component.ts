import { Component } from "@angular/core";

@Component({
    selector: "supr-home-essentials-skeleton",
    template: `
        <div class="essentials">
            <ion-skeleton-text animated class="heading"></ion-skeleton-text>

            <div class="suprRow row" *ngFor="let elem of rows">
                <div class="essentialsItem suprRow">
                    <ion-skeleton-text
                        animated
                        class="image"
                    ></ion-skeleton-text>
                    <div class="essentialsItemInfo">
                        <ion-skeleton-text
                            animated
                            class="text1"
                        ></ion-skeleton-text>
                        <ion-skeleton-text
                            animated
                            class="text2"
                        ></ion-skeleton-text>
                    </div>
                </div>

                <div class="essentialsItem suprRow">
                    <ion-skeleton-text
                        animated
                        class="image"
                    ></ion-skeleton-text>
                    <div class="essentialsItemInfo">
                        <ion-skeleton-text
                            animated
                            class="text1"
                        ></ion-skeleton-text>
                        <ion-skeleton-text
                            animated
                            class="text2"
                        ></ion-skeleton-text>
                    </div>
                </div>
            </div>
            <div class="divider16"></div>
            <div class="suprRow row">
                <div class="essentialsItem">
                    <ion-skeleton-text
                        animated
                        class="tile"
                    ></ion-skeleton-text>
                </div>

                <div class="essentialsItem">
                    <ion-skeleton-text
                        animated
                        class="tile"
                    ></ion-skeleton-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class EssentialsSkeletonComponent {
    rows: number[];

    constructor() {
        this.rows = Array(4)
            .fill(4)
            .map((_, i) => i);
    }
}
