import { Component } from "@angular/core";

@Component({
    selector: "supr-home-categories-skeleton",
    template: `
        <div class="categories">
            <ion-skeleton-text animated class="subHeading"></ion-skeleton-text>
            <supr-tile-grid-skeleton></supr-tile-grid-skeleton>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class CategoriesSkeletonComponent {}
