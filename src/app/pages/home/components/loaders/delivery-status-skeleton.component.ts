import { Component } from "@angular/core";

@Component({
    selector: "supr-home-delivery-status-skeleton",
    template: `
        <div class="deliveryStatus">
            <ion-skeleton-text animated class="heading"></ion-skeleton-text>
            <div class="datesWrapper">
                <div class="datesWrapperItem" *ngFor="let i of daysArray">
                    <ion-skeleton-text
                        animated
                        class="round"
                    ></ion-skeleton-text>
                </div>
            </div>
            <ion-skeleton-text animated class="subHeading"></ion-skeleton-text>
            <div class="productsWrapper">
                <div
                    class="productsWrapperProducts suprColumn center"
                    *ngFor="let i of productsCountArray"
                >
                    <ion-skeleton-text
                        animated
                        class="round"
                    ></ion-skeleton-text>
                    <ion-skeleton-text
                        animated
                        class="text1"
                    ></ion-skeleton-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class DeliveryStatusSkeletonComponent {
    daysArray = [0, 1, 2, 3, 4, 5, 6];
    productsCountArray = [0, 1, 2, 3];
}
