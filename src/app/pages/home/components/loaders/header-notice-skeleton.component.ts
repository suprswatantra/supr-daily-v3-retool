import { Component } from "@angular/core";

@Component({
    selector: "supr-home-header-notice-skeleton",
    template: `
        <div class="suprColumn left notice">
            <ion-skeleton-text class="noticeText"></ion-skeleton-text>
            <ion-skeleton-text class="noticeTextSmall"></ion-skeleton-text>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class HeaderNoticeSkeletonComponent {}
