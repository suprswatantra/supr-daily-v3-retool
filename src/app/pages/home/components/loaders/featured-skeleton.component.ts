import { Component } from "@angular/core";

@Component({
    selector: "supr-home-featured-skeleton",
    template: `
        <div class="featured">
            <div class="featuredContainer">
                <ion-skeleton-text animated class="tile"></ion-skeleton-text>
                <ion-skeleton-text animated class="tile"></ion-skeleton-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
})
export class FeaturedSkeletonComponent {}
