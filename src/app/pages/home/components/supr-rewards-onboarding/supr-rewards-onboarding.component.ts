import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-rewards-onboarding",
    template: `
        <ng-container *ngIf="showModal">
            <div class="fullScreenModal">
                <supr-modal
                    class="suprColumn center"
                    [modalType]="type"
                    (handleClose)="handleClose.emit()"
                    modalName="${MODAL_NAMES.REWARDS_ONBOARDING}"
                >
                    <div class="wrapper scratchCardContainer">
                        <div class="suprColumn center">
                            <div class="suprColumn center scratchCardWrapper">
                                <div class="cardWrapper center">
                                    <div class="suprRow center">
                                        <div class="imgWrapper">
                                            <img [src]="imgUrl" />
                                        </div>
                                    </div>
                                    <div class="divider16"></div>
                                    <div class="suprRow center actionBtn">
                                        <supr-button
                                            (handleClick)="
                                                handleBtnClick.emit()
                                            "
                                            saObjectName="${ANALYTICS_OBJECT_NAMES
                                                .CLICK.REWARDS_ONBOARDING_CTA}"
                                        >
                                            <div class="suprRow">
                                                <supr-text type="body">
                                                    {{ btnText }}
                                                </supr-text>
                                                <supr-icon
                                                    name="chevron_right"
                                                ></supr-icon>
                                            </div>
                                        </supr-button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </supr-modal>
            </div>
        </ng-container>
    `,
    styleUrls: ["../../styles/supr-rewards-onboardingcomponent.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardsOnboardingComponent {
    @Input() showModal: boolean;
    @Input() imgUrl: string;
    @Input() btnText: string;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output() handleBtnClick: EventEmitter<void> = new EventEmitter();
}
