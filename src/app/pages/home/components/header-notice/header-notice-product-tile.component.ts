import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { Sku } from "@models";

@Component({
    selector: "supr-home-header-notice-product-tile",
    template: `
        <div class="headerNoticeProductsWrapper" #headerNoticeProductCircle>
            <supr-image
                [src]="sku?.image?.fullUrl"
                [image]="sku?.image"
            ></supr-image>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNoticeProductTileComponent {
    @Input() sku: Sku;
}
