import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SlotInfo } from "@models";

import { TEXTS } from "@pages/home/constants/home.constants";

@Component({
    selector: "supr-home-header-notice-content",
    template: `
        <div class="suprRow spaceBetween headerNoticeText">
            <ion-row class="align-items-top">
                <supr-icon [name]="noticeContent?.icon || ''"></supr-icon>
                <div>
                    <supr-text type="body">
                        {{ noticeContent?.heading || "" }}
                    </supr-text>
                    <supr-text type="paragraph">
                        {{ noticeContent?.subHeading || "" }}
                    </supr-text>
                    <supr-text
                        *ngIf="noticeContent?.showNeedHelp"
                        type="paragraph"
                        class="needHelp"
                        (click)="handleClick($event)"
                    >
                        ${TEXTS.NEED_HELP_TEXT}
                    </supr-text>
                </div>
            </ion-row>
            <supr-home-header-notice-products
                [productsList]="noticeContent?.deliveredProducts"
            ></supr-home-header-notice-products>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNoticeContentComponent {
    @Input() noticeContent: SlotInfo;

    @Output() handleNeedHelpClick: EventEmitter<void> = new EventEmitter();

    handleClick(event?: MouseEvent) {
        if (event) {
            event.stopImmediatePropagation();
        }
        this.handleNeedHelpClick.emit();
    }
}
