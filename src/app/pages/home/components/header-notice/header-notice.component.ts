import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SlotInfo } from "@models";

@Component({
    selector: "supr-home-header-notice",
    template: `
        <div class="headerNotice">
            <supr-home-header-notice-content
                [noticeContent]="noticeContent"
                (handleNeedHelpClick)="handleNeedHelpClick.emit($event)"
            >
            </supr-home-header-notice-content>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNoticeComponent {
    @Input() noticeContent: SlotInfo;

    @Output() handleNeedHelpClick: EventEmitter<void> = new EventEmitter();
}
