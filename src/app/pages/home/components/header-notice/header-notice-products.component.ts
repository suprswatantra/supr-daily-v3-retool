import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { DeliveryStatusProductInfo } from "@models";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-home-header-notice-products",
    template: `
        <ng-container *ngIf="showProducts">
            <div class="headerNoticeProducts">
                <div
                    class="headerNoticeProductsWrapper"
                    *ngIf="extraProductsCount > 0"
                >
                    <supr-text type="paragraph">
                        +{{ extraProductsCount }}
                    </supr-text>
                </div>
                <ng-container
                    *ngFor="
                        let productInfo of productsToDisplay;
                        trackBy: trackByFn
                    "
                >
                    <supr-home-header-notice-product-tile
                        [sku]="productInfo.sku"
                    ></supr-home-header-notice-product-tile>
                </ng-container>
            </div>
        </ng-container>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNoticeProductComponent {
    @Input()
    set productsList(productsList: DeliveryStatusProductInfo[]) {
        if (this.utilService.isLengthyArray(productsList)) {
            this.hydrateComponent(productsList);
        } else {
            this.showProducts = false;
        }
    }

    showProducts: boolean;
    productsToDisplay: DeliveryStatusProductInfo[];
    extraProductsCount: number;

    constructor(private utilService: UtilService) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    private hydrateComponent(productsList: DeliveryStatusProductInfo[]) {
        this.showProducts = true;
        this.productsToDisplay = productsList.slice(0, 3);
        const extraProductsCount = productsList.length - 3;
        this.extraProductsCount =
            extraProductsCount > 0 ? extraProductsCount : 0;
    }
}
