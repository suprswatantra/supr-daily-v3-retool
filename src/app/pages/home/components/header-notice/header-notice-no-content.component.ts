import { Component, ChangeDetectionStrategy } from "@angular/core";

import { NO_DELIVERY_SCHEDULED_NOTICE_TEXT, SEE_CALENDAR_TEXT } from "./../../constants/delivery-status.constants";

@Component({
    selector: "supr-home-header-notice-nocontent",
    template: `
        <div class="suprRow center spaceBetween headerNoticeNoContent">
            <div class="headerNoticeNoContentText">
                <supr-text type="subheading">
                    {{ displayText }}
                </supr-text>
            </div>
            <div class="seeSchedule">
                <supr-text type="subtext10" class="seeScheduleText">
                    ${SEE_CALENDAR_TEXT}
                </supr-text>
                <div class="deliveryInfoProductsMore">
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderNoticeNoContentComponent {
    displayText = NO_DELIVERY_SCHEDULED_NOTICE_TEXT;
}
