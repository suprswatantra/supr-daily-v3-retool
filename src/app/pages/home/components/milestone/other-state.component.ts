import { UtilService } from "@services/util/util.service";
import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
    ViewChild,
    ElementRef,
} from "@angular/core";
import {
    MILESTONES_TEXTS,
    MILESTONE_STYLE_CONSTANT,
} from "@pages/home/constants/home.constants";
import { MileStoneHome, MileStoneState } from "@shared/models";

@Component({
    selector: "supr-home-milestone-other-state",
    template: `
        <div class="containerOtherState" #card>
            <div class="containerOtherStateHeader suprRow spaceBetween">
                <div class="containerOtherStateHeaderContent suprRow">
                    <supr-svg></supr-svg>
                    <div class="spacer8"></div>

                    <supr-text-fragment
                        type="body"
                        [textFragment]="mileStone?.details?.header_text"
                    ></supr-text-fragment>
                </div>
            </div>

            <div class="containerOtherStateInfo">
                <div class="suprRow spaceBetween">
                    <div class="suprRow containerOtherStateInfoText">
                        <ng-container
                            *ngIf="mileStone?.details?.image?.fullUrl"
                        >
                            <supr-image
                                [src]="mileStone?.details?.image?.fullUrl"
                                [image]="mileStone?.details?.image"
                            ></supr-image>
                            <div class="spacer8"></div>
                        </ng-container>

                        <supr-text-fragment
                            type="body"
                            [textFragment]="mileStone?.details?.title"
                        ></supr-text-fragment>
                    </div>
                    <div
                        class="containerOtherStateInfoIndicator suprRow center"
                        *ngIf="mileStone?.details?.sub_title?.text"
                    >
                        <supr-text-fragment
                            type="subtext10"
                            [textFragment]="mileStone?.details?.sub_title"
                        ></supr-text-fragment>
                    </div>
                </div>

                <div class="divider16"></div>
                <div class="containerOtherStateInfoBar">
                    <ng-container
                        *ngIf="mileStone?.state === inProgress; else pending"
                    >
                        <supr-progress-bar
                            *ngIf="
                                mileStone?.details?.completion_percentage !==
                                '0'
                            "
                            [percentage]="
                                mileStone?.details?.completion_percentage
                            "
                        ></supr-progress-bar>
                    </ng-container>

                    <ng-template #pending>
                        <supr-button>
                            <div class="suprRow">
                                <supr-text type="paragraph">
                                    {{ btnTextName }}
                                </supr-text>
                                <div class="spacer4"></div>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                        </supr-button>
                    </ng-template>
                </div>
            </div>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestones.component.scss"],
})
export class OtherStateComponent implements OnChanges {
    @Input() mileStone: MileStoneHome;
    @ViewChild("card", { static: true }) cardEl: ElementRef;

    btnTextName: string;
    inProgress = MileStoneState.IN_PROGRESS;

    constructor(private utilService: UtilService) {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["mileStone"];
        if (change) {
            this.setBtnTextName();
            this.setBgColor();
        }
    }

    setBtnTextName() {
        this.btnTextName =
            this.mileStone.state === MileStoneState.START
                ? MILESTONES_TEXTS.START_NOW
                : MILESTONES_TEXTS.VIEW_NOW;
    }

    setBgColor() {
        const subTitleBgColor = this.utilService.getNestedValue(
            this.mileStone.details,
            "sub_title_bg_color",
            null
        );

        if (this.utilService.isEmpty(subTitleBgColor)) {
            return;
        }

        this.cardEl.nativeElement.style.setProperty(
            MILESTONE_STYLE_CONSTANT.INDICATOR_BG_COLOR,
            subTitleBgColor
        );
    }
}
