import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { MileStoneHome } from "@shared/models";

@Component({
    selector: "supr-home-milestone-lock",
    template: `<div class="containerLock">
        <supr-image
            [src]="mileStone?.image.fullUrl"
            [image]="mileStone?.image"
            [imgHeight]="CLODUINARY_IMAGE_SIZE.PREVIEW.HEIGHT"
            [imgWidth]="CLODUINARY_IMAGE_SIZE.PREVIEW.WIDTH"
        ></supr-image>
    </div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestones.component.scss"],
})
export class LockedComponent {
    @Input() mileStone: MileStoneHome;

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;
}
