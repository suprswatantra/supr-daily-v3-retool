import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
    Input,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { MileStoneHome, MileStoneState } from "@shared/models";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { MileStoneStartCurrentState } from "@store/milestone/milestone.state";
import {
    ANALYTICS_OBJECT_NAMES,
    MILESTONE_ANALYTICS,
} from "../../constants/analytics.constants";

import { MileStoneCurrentState } from "@store/milestone/milestone.state";

@Component({
    selector: "supr-home-milestone",
    template: `
        <ng-container *ngIf="appLaunchDone">
            <div
                class="container"
                *ngIf="mileStone?.state"
                (click)="handleCtaEvent()"
                saClick
                saImpression
                [saObjectName]="
                    ANALYTICS_OBJECT_NAMES?.IMPRESSION?.MILESTONE_GROUP
                "
                [saObjectValue]="mileStone?.milestone_group_id"
                [saContextList]="getContextList()"
                [saContext]="mileStone?.state"
            >
                <ng-container
                    *ngIf="mileStone?.state === LOCKED; else inProgress"
                >
                    <supr-home-milestone-lock
                        [mileStone]="mileStone"
                    ></supr-home-milestone-lock>
                </ng-container>
                <ng-template #inProgress>
                    <supr-home-milestone-other-state
                        [mileStone]="mileStone"
                    ></supr-home-milestone-other-state>
                </ng-template>

                <div class="divider24"></div>
            </div>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MileStoneComponent implements OnChanges {
    @Input() mileStone: MileStoneHome;
    @Input() startCurrentMilestoneState: string;
    @Input() appLaunchDone: boolean;
    @Input() selectMilestoneState: string;

    @Output() handleFetchMileStone: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchUserStartMileStone: EventEmitter<
        number
    > = new EventEmitter();

    ANALYTICS_OBJECT_NAMES = ANALYTICS_OBJECT_NAMES;
    LOCKED = MileStoneState.LOCKED;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.handleMilestoneStateChange(changes["selectMilestoneState"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
        const change = changes["startCurrentMilestoneState"];
        if (change && !change.firstChange) {
            if (
                change.currentValue ===
                MileStoneStartCurrentState.POSTING_MILESTONE_START_DONE
            ) {
                this.routerService.goToMileStonePage();
            }
        }
    }

    handleCtaEvent() {
        if (this.mileStone.state === MileStoneState.START) {
            this.postUserMilestone();
        } else {
            this.redirect();
        }
    }

    getContextList() {
        const list = [
            {
                name: MILESTONE_ANALYTICS.MILESTONE_GROUP,
                value: this.mileStone.milestone_group_id,
            },
            {
                name: MILESTONE_ANALYTICS.MILESTONE,
                value: this.mileStone.milestone_id,
            },
            {
                name: MILESTONE_ANALYTICS.MILESTONE_TASK,
                value: this.mileStone.task_id,
            },
        ];

        return list;
    }

    private handleMilestoneStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        const { previousValue, currentValue } = change;
        if (
            previousValue !== MileStoneCurrentState.NO_ACTION &&
            currentValue === MileStoneCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.init();
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            if (this.appLaunchDone && !this.fetchMilestoneStateDone()) {
                this.init();
            }
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchMilestoneStateDone(): boolean {
        return (
            this.selectMilestoneState ===
            MileStoneCurrentState.FETCHING_MILESTONE_DONE
        );
    }

    private postUserMilestone() {
        this.handleFetchUserStartMileStone.emit(
            this.mileStone.user_milestone_id
        );
    }

    private redirect() {
        const url = this.utilService.getNestedValue(
            this.mileStone.cta,
            "link",
            null
        );
        if (url) {
            this.routerService.goToUrl(url);
        }
    }

    private init() {
        this.handleFetchMileStone.emit();
    }
}
