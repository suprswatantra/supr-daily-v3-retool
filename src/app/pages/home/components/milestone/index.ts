import { MileStoneComponent } from "./milestone.component";

import { OtherStateComponent } from "./other-state.component";
import { LockedComponent } from "./locked.component";

export const milestonesComponents = [
    LockedComponent,
    OtherStateComponent,
    MileStoneComponent,
];
