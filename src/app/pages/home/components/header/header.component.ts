import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { Subscription } from "@models";

import { RouterService } from "@services/util/router.service";
import { SubscriptionService } from "@services/shared/subscription.service";
import { UtilService } from "@services/util/util.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";
import { WalletService } from "@services/shared/wallet.service";

@Component({
    selector: "supr-home-header",
    template: `
        <div class="header">
            <supr-side-menu-opener name="home-menu"></supr-side-menu-opener>

            <ng-container *ngIf="showLogo; else chips">
                <ion-row class="headerLogo ion-justify-content-center">
                    <supr-svg></supr-svg>
                </ion-row>
            </ng-container>

            <ng-template #chips>
                <ion-row class="ion-justify-content-end">
                    <ng-container *ngIf="subscriptionCountStr">
                        <supr-chip
                            (handleClick)="goToSubscriptionsPage()"
                            [ngClass]="subscriptionChipClass"
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .HOME_SUBSCRIPTION_CHIP}"
                            [saObjectValue]="subscriptionChipClass"
                        >
                            <supr-icon
                                *ngIf="subscriptionChipIcon"
                                [name]="subscriptionChipIcon"
                            ></supr-icon>
                            <div class="spacer4"></div>
                            <supr-text type="paragraph">
                                {{ subscriptionCountStr }}
                            </supr-text>
                        </supr-chip>
                    </ng-container>

                    <supr-chip
                        (handleClick)="goToWalletPage()"
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .HOME_WALLET_BALANCE}"
                    >
                        <supr-text type="paragraph">
                            {{ balance | rupee }} Balance
                        </supr-text>
                    </supr-chip>
                </ion-row>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnChanges {
    @Input() balance: number;
    @Input() subscriptionList: Subscription[] = [];

    showLogo = true;
    subscriptionCountStr: string;
    subscriptionChipClass: string;
    subscriptionChipIcon: string;

    constructor(
        private routerService: RouterService,
        private subscriptionService: SubscriptionService,
        private utilService: UtilService,
        private walletService: WalletService
    ) {}

    ngOnInit() {
        this.initData();
        this.walletService.setWalletBalance(this.balance);
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["balance"] || changes["subscriptionList"];

        if (this.canHandleChange(change)) {
            this.initData();
        }
    }

    goToSubscriptionsPage() {
        this.routerService.goToSubscriptionListPage();
    }

    goToWalletPage() {
        this.routerService.goToWalletPage();
    }

    private initData() {
        if (
            this.balance > 0 ||
            this.utilService.isLengthyArray(this.subscriptionList)
        ) {
            this.showLogo = false;
            this.hydrateSubscriptionChipData();
        } else {
            this.showLogo = true;
        }
    }

    private hydrateSubscriptionChipData() {
        if (this.subscriptionList.length > 0) {
            this.setSubscriptionChipInfo();
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setSubscriptionChipInfo() {
        const split = this.subscriptionService.splitSubscriptionsBasedOnStatus(
            this.subscriptionList
        );

        const subscriptionChipInfo = this.subscriptionService.getPrioritySubcriptionInfoChip(
            split
        );

        if (subscriptionChipInfo) {
            const { type, message, iconName } = subscriptionChipInfo;
            this.subscriptionChipClass = type;
            this.subscriptionCountStr = message;
            this.subscriptionChipIcon = iconName;
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setEmptySubscriptionValues() {
        this.subscriptionCountStr = null;
        this.subscriptionChipIcon = null;
        this.subscriptionChipClass = "";
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
