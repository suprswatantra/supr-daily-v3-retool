import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Vacation, SystemVacation } from "@models";

import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-home-delivery-status-vacation",
    template: `
        <ng-container *ngIf="systemVacation; else vacationView">
            <div class="suprRow systemVacation">
                <div class="suprColumn stretch top iconWrapper">
                    <supr-icon name="pause"></supr-icon>
                </div>

                <div class="suprColumn left">
                    <supr-text type="caption">
                        {{ textsConfig.title }}
                        {{ systemVacation?.end_date | date: "MMM dd, yyyy" }}
                    </supr-text>

                    <div class="divider12"></div>

                    <div class="suprRow" (click)="handleSystemVacation.emit()">
                        <supr-text class="footer" type="captionSemiBold">
                            {{ textsConfig.subtitle }}
                        </supr-text>
                        <supr-icon
                            class="chevron"
                            name="chevron_right"
                        ></supr-icon>
                    </div>
                </div>
            </div>
        </ng-container>

        <ng-template #vacationView>
            <div class="suprRow vacation">
                <div class="suprColumn stretch top iconWrapper">
                    <supr-icon name="pause"></supr-icon>
                </div>

                <div class="suprColumn left">
                    <supr-text type="caption">
                        {{ textsConfig.title }}
                        {{ vacation?.end_date | date: "MMM dd, yyyy" }}
                    </supr-text>

                    <div class="divider12"></div>

                    <div class="suprRow" (click)="handleModifyVacation.emit()">
                        <supr-text class="footer" type="captionSemiBold">
                            {{ textsConfig.subtitle }}
                        </supr-text>
                        <supr-icon
                            class="chevron"
                            name="chevron_right"
                        ></supr-icon>
                    </div>
                </div>
            </div>
        </ng-template>
    `,
    styleUrls: ["./delivery-status-vacation.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusVacationComponent implements OnInit {
    @Input() vacation: Vacation;
    @Input() systemVacation: SystemVacation;

    @Output() handleModifyVacation: EventEmitter<void> = new EventEmitter();
    @Output() handleSystemVacation: EventEmitter<void> = new EventEmitter();

    textsConfig: any;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this.setTexts();
    }

    private setTexts() {
        const vacationType = this.systemVacation
            ? "systemVacation"
            : "vacation";
        const config = this.settingsService.getSettingsValue(
            "deliverySummaryVacationTexts"
        );

        this.textsConfig = config[vacationType];
    }
}
