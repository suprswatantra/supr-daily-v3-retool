import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DeliveryStatusProductInfo } from "@models";

import { DateService } from "@services/date/date.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-delivery-status-products",
    template: `
        <div class="suprRow top deliveryStatusProducts">
            <supr-home-delivery-status-add-more-product
                [disabled]="postHubCutOff"
            ></supr-home-delivery-status-add-more-product>
            <ng-container
                *ngFor="let productInfo of productsList; trackBy: trackByFn"
            >
                <supr-home-delivery-status-product-tile
                    [sku]="productInfo?.sku"
                    [skuMaskDetails]="productInfo?.sku_mask_details"
                    saClick
                    saObjectName="
                        ${ANALYTICS_OBJECT_NAMES.CLICK
                        .HOME_ORDER_CALENDAR_PRODUCTS}
                    "
                ></supr-home-delivery-status-product-tile>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusProductsComponent implements OnInit {
    @Input() productsList: Array<DeliveryStatusProductInfo> = [];

    postHubCutOff = false;

    constructor(private dateService: DateService) {}

    ngOnInit() {
        this.postHubCutOff = this.dateService.hasTodaysCutOffTimePassed();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
