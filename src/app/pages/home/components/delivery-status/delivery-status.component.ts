import {
    Input,
    Output,
    OnInit,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    Vacation,
    DeliveryStatus,
    SystemVacation,
    DeliveryStatusProductInfo,
    DelivereyStatusCurrentWeek,
    DeliveryStatusNewHeaderContent,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";
import { DeliveryStatusService } from "@pages/home/services/delivery-status.service";
import { DeliveryStatusCurrentState } from "@store/delivery-status/delivery-status.state";

@Component({
    selector: "supr-home-delivery-status",
    template: `
        <ng-container *ngIf="systemVacation && appLaunchDone">
            <div class="suprRow systemVacationBanner">
                <div class="suprColumn stretch top left iconWrapper">
                    <supr-icon name="error"></supr-icon>
                </div>
                <div class="suprColumn top left">
                    <supr-text type="body">
                        {{ vacationBannerInfo.title }}
                    </supr-text>
                    <div class="divider8"></div>
                    <supr-text type="caption">
                        {{ vacationBannerInfo.subtitle }}
                    </supr-text>
                </div>
            </div>
        </ng-container>
        <ng-container *ngIf="isFetching || !appLaunchDone; else content">
            <supr-home-delivery-status-skeleton></supr-home-delivery-status-skeleton>
        </ng-container>
        <ng-template #content>
            <div class="deliveryStatus" [ngClass]="getClasses()">
                <supr-home-delivery-status-dates-container
                    [currentWeek]="currentWeekInfo"
                    [dateToHighlight]="
                        deliveryStatusHeaderContent?.dateToHighlight
                    "
                ></supr-home-delivery-status-dates-container>

                <div class="divider12"></div>

                <ng-container
                    *ngIf="!vacation && !systemVacation; else pausedView"
                >
                    <supr-home-delivery-status-title
                        saClick
                        [deliveryStatusHeaderContent]="
                            deliveryStatusHeaderContent
                        "
                        [saObjectName]="getOrderStatusEventName()"
                    ></supr-home-delivery-status-title>

                    <div class="divider16"></div>

                    <supr-home-delivery-status-products
                        [productsList]="productsList"
                        (click)="routeToSchedulePage()"
                    ></supr-home-delivery-status-products>
                </ng-container>

                <ng-template #pausedView>
                    <supr-home-delivery-status-vacation
                        [vacation]="vacation"
                        [systemVacation]="systemVacation"
                        (handleSystemVacation)="routeToSchedulePage()"
                        (handleModifyVacation)="handleModifyVacation.emit()"
                    >
                    </supr-home-delivery-status-vacation>
                </ng-template>
            </div>
        </ng-template>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusComponent implements OnInit, OnChanges {
    @Input() vacation: Vacation;
    @Input() isFetching: boolean;
    @Input() refreshStatus: boolean;
    @Input() appLaunchDone: boolean;
    @Input() systemVacation: SystemVacation;
    @Input() deliveryStatus: DeliveryStatus;
    @Input() selectCurrentDeliveryStatusStoreState: string;

    @Output() handlefetchDeliveryStatus: EventEmitter<
        void
    > = new EventEmitter();
    @Output() handleModifyVacation: EventEmitter<void> = new EventEmitter();

    vacationBannerInfo: any;
    productsList: DeliveryStatusProductInfo[] = [];
    currentWeekInfo: Array<DelivereyStatusCurrentWeek> = [];
    deliveryStatusHeaderContent: DeliveryStatusNewHeaderContent;

    constructor(
        private dateService: DateService,
        private utilService: UtilService,
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private settingsService: SettingsService,
        private deliveryStatusService: DeliveryStatusService
    ) {}

    ngOnInit() {
        if (!this.utilService.isEmpty(this.deliveryStatus)) {
            this.hydrateComponentData();
        } else {
            this.fetchDeliveryStatus();
        }
        this.setCurrentWeekInfo();
        this.setVacationBannerInfo();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleDeliveryStatusChange(changes["deliveryStatus"]);
        this.handleRefreshStatusChange(changes["refreshStatus"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    hydrateComponentData() {
        this.setProductsList();
        this.setOrderStatusText();
    }

    fetchDeliveryStatus() {
        if (
            this.appLaunchDone &&
            this.selectCurrentDeliveryStatusStoreState !==
                DeliveryStatusCurrentState.FETCHING
        ) {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    routeToSchedulePage() {
        if (Array.isArray(this.currentWeekInfo) && this.currentWeekInfo[0]) {
            const scrollToDate = this.dateService.getTomorrowDate();
            const scrollToDateText = this.dateService.textFromDate(
                scrollToDate
            );

            this.scheduleService.setCustomScheduleStartDate(
                this.currentWeekInfo[0].formattedDate
            );
            this.scheduleService.setCustomScheduleScrollDate(scrollToDateText);
        }
        this.routerService.goToSchedulePage();
    }

    getOrderStatusEventName() {
        if (this.deliveryStatusHeaderContent) {
            const saEventNameSuffix = this.deliveryStatusHeaderContent.title
                .toLowerCase()
                .replace(" ", "-");
            return `${ANALYTICS_OBJECT_NAMES.CLICK.HOME_ORDERS_PREFIX}${saEventNameSuffix}`;
        }
    }

    getClasses() {
        return {
            systemVacation: this.systemVacation,
            vacation: !this.systemVacation && this.vacation,
        };
    }

    private setVacationBannerInfo() {
        this.vacationBannerInfo = this.settingsService.getSettingsValue(
            "deliverySummaryVacationBanner"
        );
    }

    private setCurrentWeekInfo() {
        this.currentWeekInfo = this.deliveryStatusService.getCurrentWeekDateAndDay();
    }

    private setOrderStatusText() {
        this.deliveryStatusHeaderContent = this.deliveryStatusService.getDeliveryStatusNewHeaderContent(
            this.deliveryStatus
        );
    }

    private setProductsList() {
        this.productsList = this.deliveryStatusService.getProductsToDisplay(
            this.deliveryStatus
        );
    }

    private handleDeliveryStatusChange(change: SimpleChange) {
        if (!change) {
            return;
        }

        const { currentValue } = change;
        if (this.utilService.isEmpty(currentValue) && this.appLaunchDone) {
            this.handlefetchDeliveryStatus.emit();
        } else if (
            !change.firstChange &&
            this.shouldReRenderComponent(change)
        ) {
            this.hydrateComponentData();
        }
    }

    private handleRefreshStatusChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.appLaunchDone) {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (
            change &&
            !change.previousValue &&
            change.currentValue &&
            this.appLaunchDone
        ) {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }

    private shouldReRenderComponent(change: SimpleChange): boolean {
        const { isEmpty } = this.utilService;
        const { currentValue, previousValue } = change;

        if (isEmpty(currentValue) && isEmpty(previousValue)) {
            return false;
        } else if (
            (isEmpty(currentValue) && !isEmpty(previousValue)) ||
            (!isEmpty(currentValue) && isEmpty(previousValue))
        ) {
            return true;
        } else {
            const { orderSummary, scheduleSummary } = currentValue;
            const {
                orderSummary: prevOrderSummary,
                scheduleSumary: prevScheduleSummary,
            } = previousValue;

            const orderSummaryStatus = orderSummary ? orderSummary.status : "";
            const orderSummaryDate = orderSummary ? orderSummary.date : "";
            const scheduleSummaryStatus = scheduleSummary
                ? scheduleSummary.status
                : "";
            const scheduleSummaryDate = scheduleSummary
                ? scheduleSummary.date
                : "";

            const prevOrderSummaryStatus = prevOrderSummary
                ? prevOrderSummary.status
                : "";
            const prevOrderSummaryDate = prevOrderSummary
                ? prevOrderSummary.date
                : "";
            const prevScheduleSummaryStatus = prevScheduleSummary
                ? prevScheduleSummary.status
                : "";
            const prevScheduleSummaryDate = prevScheduleSummary
                ? prevScheduleSummary.date
                : "";

            return (
                orderSummaryStatus !== prevOrderSummaryStatus ||
                orderSummaryDate !== prevOrderSummaryDate ||
                scheduleSummaryStatus !== prevScheduleSummaryStatus ||
                scheduleSummaryDate !== prevScheduleSummaryDate
            );
        }
    }
}
