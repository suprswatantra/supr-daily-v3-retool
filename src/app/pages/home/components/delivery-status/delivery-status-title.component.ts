import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "./../../constants/analytics.constants";

import { DeliveryStatusNewHeaderContent } from "@models";

@Component({
    selector: "supr-home-delivery-status-title",
    template: `
        <div
            class="deliveryStatusTitle"
            saImpression
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.HOME_PAGE_BANNER}"
            [saObjectValue]="deliveryStatusHeaderContent?.title"
        >
            <supr-text type="caption">
                {{ deliveryStatusHeaderContent?.title || "" }}{{ " "
                }}{{ deliveryStatusHeaderContent?.subTitle || "" }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusTitleComponent {
    @Input() deliveryStatusHeaderContent: DeliveryStatusNewHeaderContent;
}
