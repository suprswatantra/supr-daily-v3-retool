import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { RouterService } from "@services/util/router.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-delivery-status-add-more-product",
    template: `
        <div
            class="suprColumn center deliveryStatusAddMore"
            [class.disabled]="disabled"
            saClick
            saObjectName="
            ${ANALYTICS_OBJECT_NAMES.CLICK.HOME_ORDER_CALENDAR_ADD_TO_ORDER}
            "
            (click)="routeToSearchPage($event)"
        >
            <div class="suprColumn center addMore">
                <supr-icon name="add"></supr-icon>
            </div>
            <supr-text type="paragraph" [truncate]="true">
                ADD ITEM
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/delivery-status-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusAddMoreProductComponent {
    @Input() disabled: boolean;

    constructor(private routerService: RouterService) {}

    routeToSearchPage(event: MouseEvent) {
        event.stopPropagation();

        if (!this.disabled) {
            this.routerService.goToSearchPage();
        }
    }
}
