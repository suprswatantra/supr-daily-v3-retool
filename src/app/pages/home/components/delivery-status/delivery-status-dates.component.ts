import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Vacation, SystemVacation, DelivereyStatusCurrentWeek } from "@models";

import { DateService } from "@services/date/date.service";
import { VacationService } from "@services/shared/vacation.service";

import { ScheduleDict } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-delivery-status-dates",
    template: `
        <div class="suprRow deliveryStatusDates">
            <div
                *ngFor="let d of currentWeek"
                [ngClass]="getClasses(d.dateObject)"
                class="suprColumn deliveryStatusDatesWrapper"
                [class.activeDate]="d.formattedDate === dateToHighlight"
                (click)="routeToSchedulePage.emit(d)"
                saClick
                saObjectName="
                    ${ANALYTICS_OBJECT_NAMES.CLICK.HOME_ORDER_CALENDAR_DATES}
                "
                [saContext]="d.formattedDate"
            >
                <div class="suprRow">
                    <supr-text type="paragraph">{{ d.date }}</supr-text>
                    <div
                        class="deliveryStatusDatesOrderCircle"
                        *ngIf="areOrdersScheduled(d)"
                    ></div>
                </div>
                <supr-text type="paragraph">
                    {{
                        d.formattedDate === formattedTodayDate ? "Today" : d.day
                    }}
                </supr-text>
                <div
                    class="downArrow"
                    *ngIf="d.formattedDate === dateToHighlight"
                ></div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusDatesComponent {
    @Input() vacation: Vacation;
    @Input() dateToHighlight: string;
    @Input() scheduleDict: ScheduleDict;
    @Input() systemVacation: SystemVacation;
    @Input() currentWeek: Array<DelivereyStatusCurrentWeek> = [];

    @Output()
    routeToSchedulePage: EventEmitter<
        DelivereyStatusCurrentWeek
    > = new EventEmitter();

    formattedTodayDate = this.dateService.dateFnsFormatDate(new Date());

    constructor(
        private dateService: DateService,
        private vacationService: VacationService
    ) {}

    areOrdersScheduled(date: DelivereyStatusCurrentWeek): boolean {
        const { formattedDate, dateObject } = date;

        if (
            this.vacationService.isDateInVacation(
                dateObject,
                this.systemVacation || this.vacation
            )
        ) {
            return false;
        }

        const orderDateInfo =
            this.scheduleDict && this.scheduleDict[formattedDate];
        if (orderDateInfo) {
            if (orderDateInfo.addons && orderDateInfo.addons.length > 0) {
                return true;
            }

            if (
                orderDateInfo.subscriptions &&
                orderDateInfo.subscriptions.length > 0
            ) {
                const orderScheduled =
                    orderDateInfo.subscriptions.filter(
                        (item) => item.quantity > 0
                    ).length > 0;
                return orderScheduled;
            }

            return false;
        }
        return false;
    }

    getClasses(date: Date) {
        return {
            vacation: this.vacationService.isDateInVacation(
                date,
                this.vacation
            ),
            systemVacation: this.vacationService.isDateInVacation(
                date,
                this.systemVacation
            ),
        };
    }
}
