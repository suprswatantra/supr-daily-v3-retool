import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { Sku } from "@models";
import { SkuMaskDetails } from "@shared/models/schedule.model";

@Component({
    selector: "supr-home-delivery-status-product-tile",
    template: `
        <div class="suprColumn center deliveryStatusProductsTile">
            <supr-image
                [src]="
                    skuMaskDetails
                        ? skuMaskDetails?.image?.fullUrl
                        : sku?.image?.fullUrl
                "
                [image]="skuMaskDetails ? skuMaskDetails?.image : sku?.image"
            ></supr-image>
            <supr-text type="paragraph" [truncate]="true">
                {{ skuMaskDetails ? skuMaskDetails?.sku_name : sku?.sku_name }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/delivery-status-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusProductTileComponent {
    @Input() sku: Sku;
    @Input() skuMaskDetails: SkuMaskDetails;
}
