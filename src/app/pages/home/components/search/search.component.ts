import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { SEARCH_HEADER_TEXT } from "@pages/home/constants/home.constants";

import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";
import { RouterService } from "@services/util/router.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-search",
    template: `
        <ng-container *ngIf="!appLaunchDone; else content">
            <supr-home-search-skeleton></supr-home-search-skeleton>
        </ng-container>

        <ng-template #content>
            <div class="search suprVisibleDelay">
                <supr-text type="title">{{ headerText.heading }}</supr-text>
                <supr-text
                    class="colored"
                    type="title"
                    *ngIf="headerText.subHeading"
                >
                    {{ headerText.subHeading }}
                </supr-text>
                <div class="divider16"></div>
                <supr-search-box
                    (click)="goToSearchPage()"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.HOME_SEARCH}"
                ></supr-search-box>
            </div>
        </ng-template>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit {
    @Input() appLaunchDone: boolean;

    headerText = {
        heading: "",
        subHeading: "",
    };

    private tpEnabled = false;

    constructor(
        private routerService: RouterService,
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    ngOnInit() {
        this.init();
    }

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }

    private init() {
        this.setTpStatus();
        this.setHeaderText();
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private setHeaderText() {
        if (this.dateService.hasTodaysCutOffTimePassed() || this.tpEnabled) {
            this.headerText = SEARCH_HEADER_TEXT.DURING_CUTOFF;
        } else {
            this.headerText = SEARCH_HEADER_TEXT.DEFAULT;
        }
    }
}
