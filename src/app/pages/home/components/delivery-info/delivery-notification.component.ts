import {
    Input,
    OnInit,
    OnChanges,
    Component,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SlotInfo, DeliveryStatus, DelivereyStatusCurrentWeek } from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";
import { CalendarService } from "@services/date/calendar.service";
import { SettingsService } from "@services/shared/settings.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

import { DeliveryStatusService } from "@pages/home/services/delivery-status.service";

@Component({
    selector: "supr-home-delivery-notification",
    template: `
        <div
            class="suprRow deliveryNotification"
            *ngIf="showNotice && notificationContent"
        >
            <div class="suprColumn left" (click)="goToSchedulePage()">
                <ng-container *ngFor="let slot of deliverySlots">
                    <div
                        class="suprRow top slotWrapper"
                        *ngIf="notificationContent[slot]"
                    >
                        <supr-svg [ngClass]="slot"></supr-svg>
                        <supr-text type="caption">
                            {{ notificationContent[slot].subtitle }}
                            <supr-text
                                *ngIf="notificationContent[slot].otp"
                                class="otp"
                                [inline]="true"
                                type="captionBold"
                            >
                                {{ notificationContent[slot].otp }}
                            </supr-text>
                        </supr-text>
                    </div>
                </ng-container>
            </div>
            <div class="suprColumn stretch bottom right helpWrapper">
                <div
                    saClick
                    class="suprRow"
                    (click)="routeToSupportPage()"
                    [saObjectName]="
                        ${ANALYTICS_OBJECT_NAMES.CLICK
                            .HOME_ORDER_CALENDAR_NEED_HELP}
                    "
                >
                    <supr-text type="action14"> GET HELP </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryNotificationComponent implements OnInit, OnChanges {
    @Input() isFetching: boolean;
    @Input() refreshStatus: boolean;
    @Input() deliveryStatus: DeliveryStatus;

    showNotice = false;
    deliverySlots: string[];
    notificationContent: SlotInfo;
    currentWeekInfo: Array<DelivereyStatusCurrentWeek> = [];

    constructor(
        private dateService: DateService,
        private utilService: UtilService,
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private calendarService: CalendarService,
        private settingsService: SettingsService,
        private deliveryStatusService: DeliveryStatusService
    ) {}

    ngOnInit() {
        if (!this.utilService.isEmpty(this.deliveryStatus)) {
            this.handleNotification();
        }

        this.setDeliverySlots();
        this.setWeekInfo();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleDeliveryStatusChange(changes["deliveryStatus"]);
    }

    routeToSupportPage() {
        this.routerService.goToSupportPage();
    }

    goToSchedulePage() {
        if (Array.isArray(this.currentWeekInfo) && this.currentWeekInfo[0]) {
            const scrollToDate = this.dateService.getTodayDate();
            const scrollToDateText = this.dateService.textFromDate(
                scrollToDate
            );

            this.scheduleService.setCustomScheduleStartDate(
                this.currentWeekInfo[0].formattedDate
            );
            this.scheduleService.setCustomScheduleScrollDate(scrollToDateText);
        }
        this.routerService.goToSchedulePage();
    }

    private setWeekInfo() {
        const isLastDayOfWeek = this.calendarService.isTodayLastDayOfWeek();

        /* If it's a Sunday, we need to load current week in schedule page and scroll to Sunday
        as getCurrentWeekDayAndDay returns upcoming week info on a Sunday. We need to set this week's
        monday as customScheduleStartDate to render the right days in the schedule page. Else it would
        start from next week's monday and the user won't see today's order on first load of schedule page */
        if (isLastDayOfWeek) {
            this.currentWeekInfo = this.deliveryStatusService.getPreviousWeekDateAndDay();
        } else {
            this.currentWeekInfo = this.deliveryStatusService.getCurrentWeekDateAndDay();
        }
    }

    private setDeliverySlots() {
        this.deliverySlots = this.settingsService.getSettingsValue(
            "deliverySlotTypes"
        );
    }

    private handleDeliveryStatusChange(change: SimpleChange) {
        if (change && !change.firstChange) {
            this.handleNotification();
        }
    }

    private handleNotification() {
        const notificationContent = this.deliveryStatusService.getDeliveryStatusNotificationContent(
            this.deliveryStatus
        );

        if (notificationContent) {
            this.setNotificationContent(notificationContent);
        } else {
            this.unsetShowNotice();
        }
    }

    private setNotificationContent(notificationContent: SlotInfo) {
        this.notificationContent = notificationContent;
        this.setShowNotice();
    }

    private setShowNotice() {
        this.showNotice = true;
    }

    private unsetShowNotice() {
        this.showNotice = false;
    }
}
