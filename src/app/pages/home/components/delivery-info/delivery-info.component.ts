import {
    Component,
    ChangeDetectionStrategy,
    Output,
    Input,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import {
    SlotInfo,
    DeliveryStatus,
    DeliveryStatusProductInfo,
    DeliveryStatusHeaderContent,
} from "@models";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

import { DeliveryStatusService } from "@pages/home/services/delivery-status.service";
import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-delivery-info",
    template: `
        <ng-container *ngIf="isFetching || !appLaunchDone; else content">
            <supr-home-delivery-info-skeleton></supr-home-delivery-info-skeleton>
        </ng-container>
        <ng-template #content>
            <div class="deliveryInfo">
                <div (click)="routeToSchedulePage()">
                    <ng-container *ngIf="noOrders; else deliveryInfo">
                        <supr-home-no-delivery></supr-home-no-delivery>
                    </ng-container>

                    <ng-template #deliveryInfo>
                        <supr-home-delivery-status-text
                            [deliveryStatusHeaderContent]="
                                deliveryStatusHeaderContent
                            "
                            saClick
                            [saObjectName]="getOrderStatusEventName()"
                        ></supr-home-delivery-status-text>
                        <div class="divider16"></div>
                        <supr-home-delivery-products
                            [productsList]="productsList"
                        ></supr-home-delivery-products>
                    </ng-template>
                </div>
            </div>
        </ng-template>
        <supr-home-header-notice
            *ngIf="showNotice"
            [noticeContent]="notificationContent"
            (click)="routeToSchedulePage()"
            (handleNeedHelpClick)="goToSupportPage()"
        ></supr-home-header-notice>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryInfoComponent implements OnInit, OnChanges {
    @Input() isFetching: boolean;
    @Input() refreshStatus: boolean;
    @Input() deliveryStatus: DeliveryStatus;
    @Input() appLaunchDone: boolean;
    @Output()
    handleShowNotice: EventEmitter<boolean> = new EventEmitter();
    @Output()
    handlefetchDeliveryStatus: EventEmitter<void> = new EventEmitter();

    noOrders = false;
    showNotice = false;
    notificationContent: SlotInfo;
    productsList: DeliveryStatusProductInfo[] = [];
    deliveryStatusHeaderContent: DeliveryStatusHeaderContent;

    constructor(
        private deliveryStatusService: DeliveryStatusService,
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        if (!this.utilService.isEmpty(this.deliveryStatus)) {
            this.hydrateComponentData();
        } else {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleDeliveryStatusChange(changes["deliveryStatus"]);
        this.handleRefreshStatusChange(changes["refreshStatus"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    hydrateComponentData() {
        this.noOrders = !this.deliveryStatusService.showProducts(
            this.deliveryStatus
        );

        if (!this.noOrders) {
            this.setOrderStatusText();
            this.setProductsList();
        }

        this.handleNotification();
    }

    routeToSchedulePage() {
        this.routerService.goToSchedulePage();
    }

    goToSupportPage() {
        this.routerService.goToSupportPage();
    }

    getOrderStatusEventName() {
        if (this.deliveryStatusHeaderContent) {
            const saEventNameSuffix = this.deliveryStatusHeaderContent.title
                .toLowerCase()
                .replace(" ", "-");
            return `${ANALYTICS_OBJECT_NAMES.CLICK.HOME_ORDERS_PREFIX}${saEventNameSuffix}`;
        }
    }

    private setOrderStatusText() {
        this.deliveryStatusHeaderContent = this.deliveryStatusService.getDeliveryStatusHeaderContent(
            this.deliveryStatus
        );
    }

    private setProductsList() {
        this.productsList = this.deliveryStatusService.getProductsToDisplay(
            this.deliveryStatus
        );
    }

    private handleNotification() {
        const notificationContent = this.deliveryStatusService.getDeliveryStatusNotificationContent(
            this.deliveryStatus
        );

        if (notificationContent) {
            this.setNotificationContent(notificationContent);
        } else {
            this.unsetShowNotice();
        }
    }

    private setNotificationContent(notificationContent: SlotInfo) {
        this.notificationContent = notificationContent;
        this.setShowNotice();
    }

    private setShowNotice() {
        this.showNotice = true;
        setTimeout(() => this.handleShowNotice.emit(true));
    }

    private unsetShowNotice() {
        this.showNotice = false;
        setTimeout(() => this.handleShowNotice.emit(false));
    }

    private handleDeliveryStatusChange(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            this.shouldReRenderComponent(change)
        ) {
            this.hydrateComponentData();
        }
    }

    private handleRefreshStatusChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            !change.currentValue &&
            change.currentValue !== change.previousValue
        ) {
            this.handlefetchDeliveryStatus.emit();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }

    private shouldReRenderComponent(change: SimpleChange): boolean {
        const { isEmpty } = this.utilService;
        const { currentValue, previousValue } = change;

        if (isEmpty(currentValue) && isEmpty(previousValue)) {
            return false;
        } else if (
            (isEmpty(currentValue) && !isEmpty(previousValue)) ||
            (!isEmpty(currentValue) && isEmpty(previousValue))
        ) {
            return true;
        } else {
            const { orderSummary, scheduleSummary } = currentValue;
            const {
                orderSummary: prevOrderSummary,
                scheduleSumary: prevScheduleSummary,
            } = previousValue;

            const orderSummaryStatus = orderSummary ? orderSummary.status : "";
            const orderSummaryDate = orderSummary ? orderSummary.date : "";
            const scheduleSummaryStatus = scheduleSummary
                ? scheduleSummary.status
                : "";
            const scheduleSummaryDate = scheduleSummary
                ? scheduleSummary.date
                : "";

            const prevOrderSummaryStatus = prevOrderSummary
                ? prevOrderSummary.status
                : "";
            const prevOrderSummaryDate = prevOrderSummary
                ? prevOrderSummary.date
                : "";
            const prevScheduleSummaryStatus = prevScheduleSummary
                ? prevScheduleSummary.status
                : "";
            const prevScheduleSummaryDate = prevScheduleSummary
                ? prevScheduleSummary.date
                : "";

            return (
                orderSummaryStatus !== prevOrderSummaryStatus ||
                orderSummaryDate !== prevOrderSummaryDate ||
                scheduleSummaryStatus !== prevScheduleSummaryStatus ||
                scheduleSummaryDate !== prevScheduleSummaryDate
            );
        }
    }
}
