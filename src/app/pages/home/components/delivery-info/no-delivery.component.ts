import { Component, ChangeDetectionStrategy } from "@angular/core";

import {
    NO_DELIVERY_SCHEDULED_TEXT,
    ORDER_CALENDAR,
} from "./../../constants/delivery-status.constants";
import { ANALYTICS_OBJECT_NAMES } from "./../../constants/analytics.constants";

@Component({
    selector: "supr-home-no-delivery",
    template: `
        <div
            class="suprColumn noDelivery"
            saImpression
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.HOME_PAGE_BANNER}"
            saObjectValue="${NO_DELIVERY_SCHEDULED_TEXT.heading}"
        >
            <supr-svg></supr-svg>
            <supr-text type="heading">
                ${NO_DELIVERY_SCHEDULED_TEXT.heading}
            </supr-text>
            <supr-text class="caption" type="regular14">
                ${NO_DELIVERY_SCHEDULED_TEXT.subHeading}
            </supr-text>
            <div class="divider16"></div>
            <div class="suprRow orderCalendar">
                <supr-icon name="calendar"></supr-icon>
                <div class="spacer8"></div>
                <supr-text type="regular14">
                    ${ORDER_CALENDAR}
                </supr-text>
                <supr-icon
                    name="chevron_right"
                    class="calendarChevron"
                ></supr-icon>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoDeliveryComponent {}
