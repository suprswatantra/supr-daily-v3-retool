import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { DeliveryStatusProductInfo } from "@models";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";
import { SEE_CALENDAR_TEXT } from "./../../constants/delivery-status.constants";

@Component({
    selector: "supr-home-delivery-products",
    template: `
        <div class="suprRow top spaceBetween deliveryInfoProducts">
            <ion-row class="align-items-top">
                <ng-container
                    *ngFor="
                        let productInfo of productsToDisplay;
                        trackBy: trackByFn
                    "
                >
                    <supr-home-delivery-product-tile
                        [sku]="productInfo?.sku"
                        [quantity]="productInfo?.quantity"
                        [skuMaskDetails]="productInfo?.sku_mask_details"
                    ></supr-home-delivery-product-tile>
                </ng-container>
            </ion-row>
            <ng-container *ngIf="isCenterAligned; else bottomArrow">
                <div class="deliveryInfoProductsMore">
                    <supr-icon
                        name="chevron_right"
                        *ngIf="showIcon"
                    ></supr-icon>
                    <supr-text type="body" *ngIf="!showIcon">
                        +{{ extraProductsCount }}
                    </supr-text>
                </div>
            </ng-container>
            <ng-template #bottomArrow>
                <div
                    class="seeSchedule"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .HOME_VIEW_SCHEDULE}"
                >
                    <supr-text type="subtext10" class="seeScheduleText">
                        ${SEE_CALENDAR_TEXT}
                    </supr-text>
                    <div class="deliveryInfoProductsMore">
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProductsComponent {
    @Input()
    set productsList(productsList: DeliveryStatusProductInfo[]) {
        this._productsToDisplay = productsList.slice(0, 3);
        this._productsListLength = productsList.length;
        this._extraProductsCount =
            this._productsListLength - this._productsToDisplay.length;
        this._showIcon = this._extraProductsCount <= 0 ? true : false;
        this._centerAligned = this._productsListLength >= 3;
    }

    get productsToDisplay(): DeliveryStatusProductInfo[] {
        return this._productsToDisplay;
    }

    get productArrayLength(): number {
        return this._productsListLength;
    }

    get extraProductsCount(): number {
        return this._extraProductsCount;
    }

    get showIcon(): boolean {
        return this._showIcon;
    }

    get isCenterAligned(): boolean {
        return this._centerAligned;
    }

    private _productsToDisplay: DeliveryStatusProductInfo[] = [];
    private _productsListLength = 0;
    private _extraProductsCount = 0;
    private _showIcon = true;
    private _centerAligned = true;

    trackByFn(_: any, index: number): number {
        return index;
    }
}
