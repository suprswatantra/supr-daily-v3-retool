import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { Sku } from "@models";
import { SkuMaskDetails } from "@shared/models/schedule.model";

import { QuantityService } from "@services/util/quantity.service";

@Component({
    selector: "supr-home-delivery-product-tile",
    template: `
        <div class="suprColumn center tile">
            <supr-image
                [src]="
                    skuMaskDetails
                        ? skuMaskDetails?.image?.fullUrl
                        : sku?.image?.fullUrl
                "
                [image]="skuMaskDetails ? skuMaskDetails?.image : sku?.image"
            ></supr-image>
            <supr-text class="title" type="paragraph" [truncate]="true">
                {{ skuMaskDetails ? skuMaskDetails?.sku_name : sku?.sku_name }}
            </supr-text>
            <supr-text
                class="quantity"
                type="paragraph"
                *ngIf="!skuMaskDetails"
            >
                {{ displayQty }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/delivery-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProductTileComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() quantity: number;
    @Input() skuMaskDetails: SkuMaskDetails;

    displayQty: string;

    constructor(private qtyService: QuantityService) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }
        this.setDisplayQuantity();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["sku"])) {
            this.setDisplayQuantity();
        }
    }

    private setDisplayQuantity() {
        this.displayQty = this.qtyService.toText(this.sku, this.quantity);
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
