import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "./../../constants/analytics.constants";

import { DeliveryStatusHeaderContent } from "@models";

@Component({
    selector: "supr-home-delivery-status-text",
    template: `
        <div
            class="deliveryInfoStatus"
            saImpression
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.HOME_PAGE_BANNER}"
            [saObjectValue]="deliveryStatusHeaderContent?.title"
        >
            <ion-row class="ion-align-items-center">
                <supr-text class="deliveryInfoStatusTitle" type="title">
                    {{ deliveryStatusHeaderContent?.title || "" }}
                </supr-text>
                <supr-icon
                    class="deliveryInfoStatusIcon"
                    [name]="deliveryStatusHeaderContent?.icon || ''"
                ></supr-icon>
            </ion-row>
            <supr-text class="deliveryInfoStatusSubTitle" type="body">
                {{ deliveryStatusHeaderContent?.subTitle || "" }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryStatusTextComponent {
    @Input() deliveryStatusHeaderContent: DeliveryStatusHeaderContent;
}
