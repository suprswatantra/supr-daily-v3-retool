import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { Category } from "@models";
import { RouterService } from "@services/util/router.service";
import { EssentialsCurrentState } from "@store/essentials/essentials.state";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-essentials",
    template: `
        <ng-container *ngIf="loading; else content">
            <div class="discoverySpacing"></div>
            <supr-home-essentials-skeleton></supr-home-essentials-skeleton>
        </ng-container>

        <ng-template #content>
            <div class="essentials" *ngIf="essentialsAvbl">
                <supr-home-essentials-sub-category-grid
                    [category]="openCategory"
                ></supr-home-essentials-sub-category-grid>

                <ng-container *ngIf="closedCategoriesAvbl">
                    <div class="discoveryHalfSpacing"></div>
                    <div class="suprRow essentialsNonDailyRow">
                        <ng-container
                            *ngFor="
                                let category of closedCategories;
                                trackBy: trackByFn
                            "
                        >
                            <supr-home-essentials-closed-component
                                [essentialCategory]="category"
                                (click)="goToCategoryPage(category)"
                                saClick
                                [saObjectName]="getSaEventName(category)"
                            ></supr-home-essentials-closed-component>
                        </ng-container>
                    </div>
                </ng-container>
                <div class="discoverySpacing"></div>
            </div>
        </ng-template>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EssentialsComponent implements OnInit, OnChanges {
    @Input() essentialList: Category[] = [];
    @Input() essentialsState: EssentialsCurrentState;
    @Input() appLaunchDone: boolean;

    @Output() handleFetchEssentials: EventEmitter<void> = new EventEmitter();

    loading = true;
    essentialsAvbl: boolean;
    closedCategoriesAvbl: boolean;
    openCategory: Category;
    closedCategories: Category[] = [];

    constructor(private routerService: RouterService) {}

    ngOnInit() {
        this.fetchEssentialsList();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleEssentialsStateChange(changes["essentialsState"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    trackByFn(category: Category): number {
        return category.id;
    }

    goToCategoryPage(category?: Category) {
        this.routerService.goToCategoryEssentialsPage(category.id);
    }

    getSaEventName(category: Category): string {
        const categorySaName = category.name.toLowerCase().replace(" ", "-");
        return `${categorySaName}${ANALYTICS_OBJECT_NAMES.CLICK.HOME_NONDAILY_ESSENTIALS_SUFFIX}`;
    }

    private fetchEssentialsList() {
        if (
            this.essentialsState === EssentialsCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.handleFetchEssentials.emit();
        } else if (this.appLaunchDone) {
            this.loading = false;
        }
    }

    private handleEssentialsStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        const { previousValue, currentValue } = change;
        if (
            previousValue !== EssentialsCurrentState.NO_ACTION &&
            currentValue === EssentialsCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.fetchEssentialsList();
        } else if (this.appLaunchDone && this.fetchEssentialsListDone()) {
            this.loading = false;
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            if (this.appLaunchDone && this.fetchEssentialsListDone()) {
                this.loading = false;
            } else if (this.appLaunchDone && !this.fetchEssentialsListDone()) {
                this.fetchEssentialsList();
            }
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchEssentialsListDone(): boolean {
        // Setup initial data once the essentials load is done
        this.initData();

        return (
            this.essentialsState ===
            EssentialsCurrentState.FETCHING_ESSENTIALS_DONE
        );
    }

    private initData() {
        if (!this.essentialList.length) {
            return;
        }

        this.setOpenCategories();
        this.setClosedCategories();
        this.checkAvailability();
    }

    private setOpenCategories() {
        this.openCategory = this.essentialList[0];
    }

    private setClosedCategories() {
        const closedList = this.essentialList.slice(1);
        this.closedCategories = closedList.filter(
            (category) => category.default && category.default.length
        );
    }

    private checkAvailability() {
        this.closedCategoriesAvbl = !!this.closedCategories.find(
            (cat) => cat.default && cat.default.length
        );

        this.essentialsAvbl =
            this.closedCategoriesAvbl ||
            !!(this.openCategory && this.openCategory.default.length);
    }
}
