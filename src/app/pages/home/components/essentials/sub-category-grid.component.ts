import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { Category, SubCategory } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import {
    MAX_NO_OF_ESSENTIALS_SUB_CATEGORIES_DISPLAY,
    TEXTS,
} from "@pages/home/constants/home.constants";
import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-essentials-sub-category-grid",
    template: `
        <supr-text type="subtitle">{{ category?.name }}</supr-text>
        <div class="discoveryHalfSpacing"></div>

        <div class="essentialsSubCategory">
            <div
                class="suprRow essentialsSubCategoryRow"
                *ngFor="
                    let rowData of displayCategories;
                    trackBy: trackByFn;
                    last as _lastRow
                "
            >
                <supr-sub-category-tile
                    [subCategory]="subCategory"
                    (click)="goToCategoryPage(subCategory.id)"
                    *ngFor="let subCategory of rowData; trackBy: trackByFn"
                    saClick
                    saImpression
                    [saImpressionEnabled]="
                        ${SA_IMPRESSIONS_IS_ENABLED.SUB_CATEGORY_TILE}
                    "
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .HOME_DAILY_ESSENTIALS}"
                    [saObjectValue]="subCategory?.name"
                ></supr-sub-category-tile>

                <div
                    class="suprRow essentialsSubCategoryMore"
                    (click)="goToCategoryPage()"
                    *ngIf="_lastRow && moreCategoriesCount > 0"
                >
                    <div class="essentialsSubCategoryMoreTile">
                        <supr-text type="subheading">
                            +{{ moreCategoriesCount + 1 }}
                        </supr-text>
                    </div>
                    <div
                        class="essentialsSubCategoryMoreText suprColumn top left"
                    >
                        <supr-text type="body">
                            ${TEXTS.MORE_ESSENTIALS_LINE_ONE}
                        </supr-text>
                        <supr-text type="body">
                            ${TEXTS.MORE_ESSENTIALS_LINE_TWO}
                        </supr-text>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EssentialsSubCategoryGridComponent implements OnInit {
    @Input() category: Category;

    displayCategories: SubCategory[] = [];
    moreCategoriesCount = 0;
    moreCategoriesFirstCategoryId: number;

    private subCategories: SubCategory[] = [];

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.initData();
    }

    trackByFn(subCategory: SubCategory): number {
        return subCategory.id;
    }

    goToCategoryPage(filterId?: number) {
        const queryParams = {};
        if (filterId) {
            queryParams["filterId"] = filterId;
        } else if (this.moreCategoriesFirstCategoryId) {
            queryParams["filterId"] = this.moreCategoriesFirstCategoryId;
        }

        this.routerService.goToCategoryEssentialsPage(this.category.id, {
            queryParams,
        });
    }

    private initData() {
        if (this.utilService.isEmpty(this.category)) {
            return;
        }

        this.setSubCategories();
        this.calculateMoreCount();
        this.splitCategoriesIntoGroups();
    }

    private setSubCategories() {
        this.subCategories = this.category.default;
    }

    private calculateMoreCount() {
        const moreCount =
            this.subCategories.length -
            MAX_NO_OF_ESSENTIALS_SUB_CATEGORIES_DISPLAY;
        this.moreCategoriesCount = Math.max(0, moreCount);
    }

    private splitCategoriesIntoGroups() {
        let displayCategories = [...this.subCategories];

        if (this.moreCategoriesCount > 0) {
            displayCategories = this.subCategories.slice(
                0,
                MAX_NO_OF_ESSENTIALS_SUB_CATEGORIES_DISPLAY - 1
            );
            this.setMoreCategoriesCategoryId();
        }

        this.displayCategories = this.utilService.chunkArray(
            displayCategories,
            2
        );
    }

    private setMoreCategoriesCategoryId() {
        const category = this.subCategories[
            MAX_NO_OF_ESSENTIALS_SUB_CATEGORIES_DISPLAY - 1
        ];
        if (category) {
            this.moreCategoriesFirstCategoryId = category.id;
        }
    }
}
