import {
    Component,
    ChangeDetectionStrategy,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
} from "@angular/core";

import { UtilService } from "@services/util/util.service";
import { Color } from "@types";
import { Category } from "@models";

@Component({
    selector: "supr-home-essentials-closed-component",
    template: `
        <div class="essentialsNonDailyWrapper suprRow" #essentialsNonDaily>
            <div class="suprColumn stretchItems center">
                <supr-text type="subheading">{{
                    categoryNameLineOne
                }}</supr-text>
                <div class="suprRow spaceBetween">
                    <supr-text type="subheading">{{
                        categoryNameLineTwo
                    }}</supr-text>
                    <supr-icon
                        class="deliveryInfoStatusIcon"
                        name="chevron_right"
                    ></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EssentialsClosedComponent implements OnInit {
    @Input() essentialCategory: Category;

    @ViewChild("essentialsNonDaily", { static: true }) wrapperEl: ElementRef;

    hslData: Color.HSL;
    categoryNameLineOne: string;
    categoryNameLineTwo: string;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.convertColorToHSL();

        this.setBgColor();

        this.splitCategoryName();
    }

    private convertColorToHSL() {
        this.hslData = this.utilService.hexToHSL(
            this.essentialCategory.image.bgColor
        );
    }

    private setBgColor() {
        const { h, s } = this.hslData;
        this.wrapperEl.nativeElement.style.setProperty(
            "--home-essentials-closed-background-color",
            `hsl(${h}, ${s}%, 95%)`
        );
    }

    private splitCategoryName() {
        if (this.essentialCategory.name) {
            const categoryNameSplit = this.essentialCategory.name.split(" ");
            if (categoryNameSplit.length === 2) {
                this.categoryNameLineOne = categoryNameSplit[0];
                this.categoryNameLineTwo = categoryNameSplit[1];
            }
        }
    }
}
