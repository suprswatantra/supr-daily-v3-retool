import { StickySectionComponent } from "./sticky-section/sticky-section.component";
import { DiscoveryComponent } from "./discovery/discovery.component";
import { HeaderComponent } from "./header/header.component";
import { StickyHeaderComponent } from "./sticky-header/sticky-header.component";
import { HeaderNoticeComponent } from "./header-notice/header-notice.component";
import { DeliveryInfoComponent } from "./delivery-info/delivery-info.component";
import { DeliveryStatusTextComponent } from "./delivery-info/delivery-status-text.component";
import { DeliveryProductsComponent } from "./delivery-info/delivery-products.component";
import { SearchComponent } from "./search/search.component";
import { EssentialsComponent } from "./essentials/essentials.component";
import { EssentialsSubCategoryGridComponent } from "./essentials/sub-category-grid.component";
import { EssentialsClosedComponent } from "./essentials/essentials-closed.component";
import { CategoriesComponent } from "./categories/categories.component";
import { CollectionsComponent } from "./collections/collections.component";
import { NoDeliveryComponent } from "./delivery-info/no-delivery.component";
import { DeliveryProductTileComponent } from "./delivery-info/delivery-product-tile.component";
import { HeaderNoticeProductTileComponent } from "./header-notice/header-notice-product-tile.component";
import { HeaderNoticeProductComponent } from "./header-notice/header-notice-products.component";
import { HeaderNoticeContentComponent } from "./header-notice/header-notice-content.component";
import { HeaderNoticeNoContentComponent } from "./header-notice/header-notice-no-content.component";
import { DeliveryNotificationComponent } from "./delivery-info/delivery-notification.component";

import { LaunchLoaderComponent } from "./loaders/launch.component";
import { DeliveryInfoSkeletonComponent } from "./loaders/delivery-info-skeleton.component";
import { FeaturedSkeletonComponent } from "./loaders/featured-skeleton.component";
import { EssentialsSkeletonComponent } from "./loaders/essentials-skeleton.component";
import { SearchSkeletonComponent } from "./loaders/search-skeleton.component";
import { CategoriesSkeletonComponent } from "./loaders/categories-skeleton.component";
import { HeaderNoticeSkeletonComponent } from "./loaders/header-notice-skeleton.component";
import { DeliveryStatusComponent } from "./delivery-status/delivery-status.component";
import { DeliveryStatusDatesComponent } from "./delivery-status/delivery-status-dates.component";
import { DeliveryStatusTitleComponent } from "./delivery-status/delivery-status-title.component";
import { DeliveryStatusProductsComponent } from "./delivery-status/delivery-status-products.component";
import { DeliveryStatusProductTileComponent } from "./delivery-status/delivery-status-product-tile.component";
import { DeliveryStatusAddMoreProductComponent } from "./delivery-status/delivery-status-add-more-product.component";
import { DeliveryStatusSkeletonComponent } from "./loaders/delivery-status-skeleton.component";
import { DeliveryStatusVacationComponent } from "./delivery-status/delivery-status-vacation/delivery-status-vacation.component";
import { HomeLayoutsComponent } from "./home-layout/home-layout.component";
import { milestonesComponents } from "./milestone";
import { RewardsOnboardingComponent } from "./supr-rewards-onboarding/supr-rewards-onboarding.component";

export const homePageComponents = [
    StickySectionComponent,
    DiscoveryComponent,
    HeaderComponent,
    StickyHeaderComponent,
    HeaderNoticeComponent,
    DeliveryInfoComponent,
    DeliveryStatusComponent,
    DeliveryStatusTextComponent,
    DeliveryProductsComponent,
    DeliveryStatusDatesComponent,
    DeliveryStatusTitleComponent,
    DeliveryStatusProductsComponent,
    DeliveryStatusProductTileComponent,
    DeliveryStatusAddMoreProductComponent,
    SearchComponent,
    EssentialsComponent,
    EssentialsClosedComponent,
    EssentialsSubCategoryGridComponent,
    CategoriesComponent,
    CollectionsComponent,
    NoDeliveryComponent,
    DeliveryProductTileComponent,
    HeaderNoticeProductTileComponent,
    HeaderNoticeProductComponent,
    HeaderNoticeContentComponent,
    HeaderNoticeNoContentComponent,
    DeliveryNotificationComponent,
    DeliveryInfoSkeletonComponent,
    LaunchLoaderComponent,
    FeaturedSkeletonComponent,
    EssentialsSkeletonComponent,
    SearchSkeletonComponent,
    CategoriesSkeletonComponent,
    HeaderNoticeSkeletonComponent,
    DeliveryStatusSkeletonComponent,
    DeliveryStatusVacationComponent,
    HomeLayoutsComponent,
    milestonesComponents,
    RewardsOnboardingComponent,
];
