import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { Collection } from "@models";
import { CollectionCurrentState } from "@store/collection/collection.state";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-collections",
    template: `
        <ng-container *ngIf="!loading && appLaunchDone">
            <ng-container
                *ngFor="
                    let collection of collectionList;
                    trackBy: trackByFn;
                    index as position;
                    last as isLast
                "
            >
                <supr-collection-widget
                    [collection]="collection"
                    saImpression
                    saClick
                    [saImpressionEnabled]="
                        ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_WIDGET}
                    "
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .HOME_COLLECTION_WIDGET}"
                    [saObjectValue]="collection?.viewId"
                    [saPosition]="position + 1"
                ></supr-collection-widget>
                <div class="divider24" *ngIf="!isLast"></div>
            </ng-container>
            <!-- <div class="discoverySpacing" *ngIf="collectionList?.length"></div> -->
        </ng-container>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionsComponent implements OnInit, OnChanges {
    @Input() collectionList: Collection[];
    @Input() collectionState: CollectionCurrentState;
    @Input() appLaunchDone: boolean;

    @Output() handleFetchCollections: EventEmitter<void> = new EventEmitter();

    loading = true;

    ngOnInit() {
        this.fetchCollectionList();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCollectionStateChange(changes["collectionState"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    trackByFn(collection: Collection): number {
        return collection && collection.viewId;
    }

    private fetchCollectionList() {
        if (
            this.collectionState === CollectionCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.handleFetchCollections.emit();
        } else {
            this.loading = false;
        }
    }

    private handleCollectionStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        const { previousValue, currentValue } = change;
        if (
            previousValue !== CollectionCurrentState.NO_ACTION &&
            currentValue === CollectionCurrentState.NO_ACTION &&
            this.appLaunchDone
        ) {
            this.fetchCollectionList();
        } else if (this.appLaunchDone && this.fetchCollectionListDone()) {
            this.loading = false;
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            if (this.appLaunchDone && this.fetchCollectionListDone()) {
                this.loading = false;
            } else if (this.appLaunchDone && !this.fetchCollectionListDone()) {
                this.fetchCollectionList();
            }
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchCollectionListDone(): boolean {
        return (
            this.collectionState ===
            CollectionCurrentState.FETCHING_COLLECTIONS_DONE
        );
    }
}
