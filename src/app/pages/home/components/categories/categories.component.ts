import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-home-categories",
    template: `
        <div class="categories" id="home-category_section">
            <ng-container *ngIf="!appLaunchDone || isFetching; else content">
                <supr-home-categories-skeleton></supr-home-categories-skeleton>
            </ng-container>

            <ng-template #content>
                <supr-text type="subtitle">Categories</supr-text>
                <div class="discoveryHalfSpacing"></div>
                <supr-category-grid
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .HOME_CATEGORIES}"
                ></supr-category-grid>
                <div class="discoverySpacing"></div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/home.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesComponent {
    @Input() appLaunchDone: boolean;
    @Input() isFetching: boolean;
}
