export const TEXTS = {
    DEFAULT_SEARCH_PLACEHOLDER: "Search for milk & groceries...",
    MORE_ESSENTIALS_LINE_ONE: "More",
    MORE_ESSENTIALS_LINE_TWO: "essentials",
    NEED_HELP_TEXT: "Need help?",
    REWARDS_ONBOARDING_BTN: "TAKE ME THERE",
};

export const MAX_NO_OF_ESSENTIALS_SUB_CATEGORIES_DISPLAY = 8;

export const FEATURED_ITEM = {
    ACTION_TYPES: {
        STATIC: "static",
        NAVIGATION: "navigation",
    },
    NAVIGATION_PAGES: {
        ESSENTIAL: "essential",
        CATEGORY: "category",
        COLLECTION: "collection",
        WALLET_RECHARGE: "wallet_recharge",
        INAPP_URL: "inapp_url",
        OUTSIDE_URL: "outside_url",
    },
};

export const SEARCH_HEADER_TEXT = {
    DEFAULT: {
        heading: "What can we get you?",
        subHeading: "",
    },
    DURING_CUTOFF: {
        heading: "What can we get you?",
        subHeading: "",
    },
};

export const SUB_HEADER_HEIGHT = 100;

export const SEARCH_BAR_FIXED_CLASS_NAME = "fixed";

export const COLLECTIONS = {
    HOME_SECTION_1: "home_section_1",
    HOME_SECTION_2: "home_section_2",
    HOME_SECTION_3: "home_section_3",
    HOME_SECTION_4: "home_section_4",
};

export const DEFAULT_REWARDS_ONBOARDING_IMG_URL =
    "assets/images/app/supr-rewards-onboarding.svg";

export const MILESTONES_TEXTS = {
    START_NOW: "START NOW",
    VIEW_NOW: "VIEW NOW",
};

export const MILESTONE_STYLE_CONSTANT = {
    INDICATOR_BG_COLOR: "--milestone-indicator-bg-color",
};
