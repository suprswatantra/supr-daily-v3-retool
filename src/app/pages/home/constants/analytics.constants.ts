export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        HOME_SUBSCRIPTION_CHIP: "home-subscription",
        HOME_WALLET_BALANCE: "home-wallet-balance",
        HOME_ORDERS_PREFIX: "orders-",
        HOME_VIEW_SCHEDULE: "view-schedule",
        HOME_SEARCH: "search",
        HOME_SIDE_MENU: "side-menu",
        HOME_FEATURED_CARD_PREFIX: "featured-card-",
        HOME_DAILY_ESSENTIALS: "daily-essentials",
        HOME_NONDAILY_ESSENTIALS_SUFFIX: "-card",
        HOME_CATEGORIES: "category",
        HOME_ORDER_CALENDAR_ADD_TO_ORDER: "add-to-order",
        HOME_ORDER_CALENDAR_NEED_HELP: "need-help-v2",
        HOME_ORDER_CALENDAR_PRODUCTS: "upcoming-order",
        HOME_ORDER_CALENDAR_DATES: "date",
        ADD_MONEY: "add-money",
        REWARDS_ONBOARDING_CTA: "rewards-onboarding-cta",
    },
    IMPRESSION: {
        HOME_PAGE_BANNER: "home-page-banner",
        HOME_COLLECTION_WIDGET: "collection-widget",
        MILESTONE_GROUP: "milestone-group",
    },
    CONTEXT: {
        DISCOVERY: "discovery",
    },
};

export const MILESTONE_ANALYTICS = {
    MILESTONE_GROUP: "milestone-group",
    MILESTONE_STATE: "milestone-state",
    MILESTONE: "milestone",
    MILESTONE_TASK: "milestone-task",
};
