export const DeliveryOrderStatusState = {
    ORDER_SCHEDULED: "ORDER_SCHEDULED",
    PICKED_UP: "PICKED_UP",
    DELIVERED_PARTIAL: "DELIVERED_PARTIAL",
    DELIVERED: "DELIVERED",
    DELAYED: "DELAYED",
    REFUND_PARTIAL: "REFUND_PARTIAL",
    REFUND_FULL: "REFUND_FULL",
};

export const DeliveryStatusTerminalState = [
    DeliveryOrderStatusState.DELIVERED,
    DeliveryOrderStatusState.REFUND_PARTIAL,
    DeliveryOrderStatusState.REFUND_FULL,
];

export const DeliveryScheduleState = {
    SCHEDULED: "SCHEDULED",
    NOTHING_SCHEDULED: "NOTHING_SCHEDULED",
};

export const DeliveryStatusHeaderContent = {
    ORDER_SCHEDULED: null,
    PICKED_UP: null,
    DELIVERED_PARTIAL: {
        heading: "We have missed some items in your order.",
        subHeading: "We are on our way to deliver them soon.",
        icon: "stop_watch",
    },
    DELIVERED: {
        heading: " delivered this morning.",
    },
    DELAYED: {
        heading: "Apologies for the delay in today's delivery.",
        subHeading: "We are looking into it.",
        icon: "error",
    },
    REFUND_PARTIAL: {
        heading: "Sorry we couldn't deliver some items today.",
        subHeading: "Click here to view details.",
        icon: "error",
    },
    REFUND_FULL: {
        heading: "We're sorry, we couldn't deliver your orders for today.",
        subHeading: "Click here to view details.",
        icon: "error",
    },
};

export const DeliveryStatusHeaderText = {
    ORDER_SCHEDULED: {
        title: "Arriving tomorrow",
        subTitle: "At your doorstep",
    },
    PICKED_UP: {
        title: "Arriving soon",
        subTitle: "Your delivery is on the way.",
    },
    DELAYED: {
        title: "Arriving soon",
        subTitle: "Your order is still on the way",
    },
    DELIVERED_PARTIAL: {
        title: "Arriving soon",
        subTitle: "Your order is still on the way",
    },
};

export const NO_DELIVERY_SCHEDULED_TEXT = {
    heading: "No orders for tomorrow.",
    subHeading: "",
};

export const NO_DELIVERY_SCHEDULED_NOTICE_TEXT = "Next delivery";
export const ORDER_CALENDAR = "Order calendar";
export const SEE_CALENDAR_TEXT = "See order calendar";
