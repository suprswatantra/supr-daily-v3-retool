import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { homePageContainers } from "./containers";
import { homePageComponents } from "./components";
import { homePageServices } from "./services";

import { HomePageLayoutComponentNew } from "./layouts/home.layout-new";
import { HomeContainer } from "./containers/home.container";

const routes: Routes = [
    {
        path: "",
        component: HomeContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...homePageContainers,
        ...homePageComponents,
        HomePageLayoutComponentNew,
    ],
    providers: [...homePageServices],
})
export class HomePageModule {}
