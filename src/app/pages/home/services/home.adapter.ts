import { SubscriptionStoreActions } from "@store/subscription";
import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Sku, Collection, V3Layout } from "@models";
import {
    StoreState,
    EssentialsStoreSelectors,
    EssentialsStoreActions,
    WalletStoreSelectors,
    SubscriptionStoreSelectors,
    SkuStoreSelectors,
    CartStoreSelectors,
    MiscStoreSelectors,
    MiscStoreActions,
    DeliveryStatusStoreSelectors,
    DeliveryStatusStoreActions,
    ErrorStoreSelectors,
    CategoryStoreSelectors,
    CollectionStoreSelectors,
    CollectionStoreActions,
    ScheduleStoreSelectors,
    UserStoreSelectors,
    HomeLayoutStoreSelectors,
    HomeLayoutStoreActions,
    MileStoneStoreSelectors,
    MileStoneStoreActions,
    AddressStoreSelectors,
    UserStoreActions,
    ErrorStoreActions,
} from "@supr/store";

@Injectable()
export class HomePageAdapter {
    essentialList$ = this.store.pipe(
        select(EssentialsStoreSelectors.selectEssentialList)
    );

    essentialsCurrentState$ = this.store.pipe(
        select(EssentialsStoreSelectors.selectEssentialsCurrentState)
    );

    startMilestoneCurrentState$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectStartMilestoneState)
    );

    mileStone$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectMileStone)
    );

    collectionList$ = this.store.pipe(
        select(CollectionStoreSelectors.selectCollectionList)
    );

    collectionCurrentState$ = this.store.pipe(
        select(CollectionStoreSelectors.selectCollectionCurrentState)
    );

    homeLayoutCurrentState$ = this.store.pipe(
        select(HomeLayoutStoreSelectors.selectHomeLayoutCurrentState)
    );

    walletBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletBalance)
    );

    subscriptionList$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    subscriptionCurrentState$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionState)
    );

    isFetchingDeliveryStatus$ = this.store.pipe(
        select(DeliveryStatusStoreSelectors.selectIsFetchingDeliveryStatus)
    );

    isFetchingCategories$ = this.store.pipe(
        select(CategoryStoreSelectors.selectIsFetchingCategories)
    );

    deliveryStatus$ = this.store.pipe(
        select(DeliveryStatusStoreSelectors.selectDeliveryStatus)
    );

    selectCurrentDeliveryStatusStoreState$ = this.store.pipe(
        select(
            DeliveryStatusStoreSelectors.selectCurrentDeliveryStatusStoreState
        )
    );

    refreshDeliveryStatus$ = this.store.pipe(
        select(DeliveryStatusStoreSelectors.selectShouldRefreshDeliveryStatus)
    );

    miniCart$ = this.store.pipe(select(CartStoreSelectors.selectMiniCart));

    appLaunchDone$ = this.store.pipe(
        select(MiscStoreSelectors.selectAppLaunchDone)
    );

    errorState$ = this.store.pipe(
        select(ErrorStoreSelectors.selectErrorCurrentState)
    );

    scheduleDict$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleDict)
    );

    suprCreditsEnabled$ = this.store.pipe(
        select(UserStoreSelectors.selectIsSuprCreditsEnabled)
    );

    suprCreditsBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectSuprCreditsBalance)
    );

    isAnonymousUser$ = this.store.pipe(
        select(UserStoreSelectors.isAnonymousUser)
    );

    rewardsEnabled$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForRewards)
    );

    rewardsDetails$ = this.store.pipe(
        select(UserStoreSelectors.selectRewardsDetails)
    );

    isExperimentsFetched$ = this.store.pipe(
        select(MiscStoreSelectors.selectExperimentsFetched)
    );

    selectMilestoneState$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectMilestoneState)
    );

    constructor(private store: Store<StoreState>) {}

    fetchEssentialList() {
        this.store.dispatch(
            new EssentialsStoreActions.FetchEssentialListRequestAction()
        );
    }

    fetchCollectionList() {
        this.store.dispatch(
            new CollectionStoreActions.FetchCollectionListRequestAction()
        );
    }

    fetchHomeLayoutList() {
        this.store.dispatch(
            new HomeLayoutStoreActions.FetchHomeLayoutListRequestAction()
        );
    }

    fetchDeliveryStatus(silent = false) {
        this.store.dispatch(
            new DeliveryStatusStoreActions.FetchDeliveryStatusAction({
                silent,
            })
        );
    }

    fetchCollectionListForPosition(position: string): Observable<Collection[]> {
        return this.store.pipe(
            select(CollectionStoreSelectors.selectCollectionListByPosition, {
                position,
            })
        );
    }

    fetchHomeLayoutListForPosition(position: string): Observable<V3Layout[]> {
        return this.store.pipe(
            select(HomeLayoutStoreSelectors.selectHomeLayoutListByPosition, {
                position,
            })
        );
    }

    getSkuById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, { skuId })
        );
    }

    disableRefreshDeliveryStatus() {
        this.store.dispatch(
            new DeliveryStatusStoreActions.DisableRefreshDeliveryStatus()
        );
    }

    toggleAppLaunchDone() {
        this.store.dispatch(new MiscStoreActions.ToggleAppLaunchDone());
    }

    setAppInitDone() {
        this.store.dispatch(new MiscStoreActions.SetAppInitDone());
    }

    fetchSubscriptionList() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }

    fetchMileStone() {
        this.store.dispatch(
            new MileStoneStoreActions.FetchMileStoneRequestAction()
        );
    }

    postUserMilestone(userMilestoneId: number) {
        this.store.dispatch(
            new MileStoneStoreActions.PostMileStoneStartRequestAction({
                user_milestone_id: userMilestoneId,
            })
        );
    }

    // landing content

    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    userState$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCurrentState)
    );

    address$ = this.store.pipe(select(AddressStoreSelectors.selectAddress));

    error$ = this.store.pipe(select(ErrorStoreSelectors.selectError));

    settingsFetched$ = this.store.pipe(
        select(MiscStoreSelectors.selectSettingsFetched)
    );

    experimentsFetched$ = this.store.pipe(
        select(MiscStoreSelectors.selectExperimentsFetched)
    );

    fetchProfileWithAddress() {
        this.store.dispatch(
            new UserStoreActions.LoadProfileWithAddressRequestAction({
                address: true,
                wallet: true,
                t_plus_one: true,
            })
        );
    }

    clearGlobalError() {
        this.store.dispatch(new ErrorStoreActions.ClearGlobalError());
    }
}
