import { HomePageService } from "./home.service";
import { HomePageAdapter } from "./home.adapter";
import { DeliveryStatusService } from "./delivery-status.service";

export const homePageServices = [
    HomePageAdapter,
    HomePageService,
    DeliveryStatusService,
];
