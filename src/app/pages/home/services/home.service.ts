import { Injectable } from "@angular/core";

import { EVENT_TYPES, SETTINGS, STORAGE_DB_DATA_KEYS } from "@constants";
import { Featured } from "@models";
import { AppTrackingMeta, AppTrackingData } from "@types";

import { environment } from "@environments/environment";

import { RouterService } from "@services/util/router.service";
import { PlatformService } from "@services/util/platform.service";
import { SettingsService } from "@services/shared/settings.service";
import { DbService } from "@services/data/db.service";
import { ErrorService } from "@services/integration/error.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { FEATURED_ITEM } from "@pages/home/constants/home.constants";

@Injectable()
export class HomePageService {
    constructor(
        private routerService: RouterService,
        private platformService: PlatformService,
        private dbService: DbService,
        private settingsService: SettingsService,
        private errorService: ErrorService,
        private analyticsService: AnalyticsService
    ) {}

    handleFeaturedItemClick(item: Featured.Item) {
        const action = item.action;
        if (!action || action.type === FEATURED_ITEM.ACTION_TYPES.STATIC) {
            return;
        }

        if (action.type === FEATURED_ITEM.ACTION_TYPES.NAVIGATION) {
            this.getFeaturedItemNavigationUrl(item);
        }
    }

    private getFeaturedItemNavigationUrl(item: Featured.Item) {
        const {
            pageType,
            paramValue,
            filterValue,
        } = item.action.navigationParams;

        switch (pageType) {
            case FEATURED_ITEM.NAVIGATION_PAGES.CATEGORY:
                this.goToCategoryPage(paramValue, filterValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.ESSENTIAL:
                this.goToEssentialPage(paramValue, filterValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.COLLECTION:
                // Param Value is string and need to send Int value so that used +paramValue
                this.goToCollectionPage(+paramValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.WALLET_RECHARGE:
                this.goToWalletPage();
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.INAPP_URL:
                // paramValue is url in this case
                this.routerService.goToUrl(String(paramValue));
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.OUTSIDE_URL:
                // paramValue is url in this case
                window.open(String(paramValue), "_system", "location=yes");
                break;
        }
    }

    private goToCategoryPage(categoryId: number, filterValue?: number) {
        const queryParams = {};
        if (filterValue) {
            queryParams["filterId"] = filterValue;
        }

        this.routerService.goToCategoryPage(categoryId, {
            queryParams,
        });
    }

    private goToEssentialPage(categoryId: number, filterValue?: number) {
        const queryParams = {};
        if (filterValue) {
            queryParams["filterId"] = filterValue;
        }

        this.routerService.goToCategoryEssentialsPage(categoryId, {
            queryParams,
        });
    }

    private goToCollectionPage(viewId: number) {
        this.routerService.goToCollectionPage(viewId);
    }

    private goToWalletPage() {
        this.routerService.goToWalletPage();
    }

    /* Log information about installed apps */
    async trackInstalledApps() {
        const appTrackingEnabled = await this.isAppTrackingEnabled();
        if (appTrackingEnabled) {
            const { version, apps: appsToTrack } = this.getAppsToTrack();
            const appTrackingInfoSent = await this.isAppTrackingInfoSent(
                version
            );
            if (!appTrackingInfoSent) {
                const appTrackingPromises = appsToTrack.map((appData) =>
                    this.platformService.getAppAvailability(appData)
                );

                if (appTrackingPromises.length > 0) {
                    Promise.all(appTrackingPromises)
                        .then((appTrackingInfo: Array<any>) => {
                            this.sendAppTrackingAnalytics(
                                appTrackingInfo,
                                version
                            );
                        })
                        .catch((err) => {
                            this.errorService.logSentryError(err, {
                                message: "Installed apps tracking error",
                            });
                        });
                }
            }
        }
    }

    /* Check if app tracking service is enabled */
    private async isAppTrackingEnabled(): Promise<boolean> {
        const appTrackingEnabled = this.settingsService.getSettingsValue(
            SETTINGS.APPS_TRACKING_ENABLED,
            false
        );
        return (
            this.platformService.isCordova() &&
            environment.production &&
            appTrackingEnabled
        );
    }

    /* Check if the apps tracking info is already sent for the user for the current version of app set */
    private async isAppTrackingInfoSent(
        trackingVersion: number
    ): Promise<boolean> {
        if (!trackingVersion) {
            return false;
        }

        const appTrackingInfoVersion = await this.dbService.getData(
            STORAGE_DB_DATA_KEYS.APPS_TRACKING_VERSION
        );

        return appTrackingInfoVersion === trackingVersion;
    }

    private getAppsToTrack(): AppTrackingMeta {
        const defaultTrackingData = {
            version: 0,
            apps: [],
        };
        const trackingData = this.platformService.isCordova()
            ? this.settingsService.getSettingsValue(
                  SETTINGS.TRACKED_APPS,
                  defaultTrackingData
              )
            : defaultTrackingData;

        const appIDKey = this.platformService.isAndroid()
            ? "androidAppId"
            : "iOSAppId";

        trackingData.apps = trackingData.apps.filter(
            (appData: AppTrackingData) => {
                return appData[appIDKey];
            }
        );

        return trackingData;
    }

    private sendAppTrackingAnalytics(
        appTrackingInfo: Array<any>,
        trackingVersion: number
    ) {
        this.analyticsService.trackCustom(EVENT_TYPES.APPS_TRACKED, {
            version: trackingVersion,
            appTrackingInfo,
        });

        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.APPS_TRACKING_VERSION,
            trackingVersion
        );
    }
}
