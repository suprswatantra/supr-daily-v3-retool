import { Injectable } from "@angular/core";

import {
    Sku,
    SlotInfo,
    OrderSummary,
    DeliveryStatus,
    OrderSummaryItem,
    ScheduleSubscription,
    DeliveryStatusProductInfo,
    DelivereyStatusCurrentWeek,
    DeliveryStatusNewHeaderContent,
    DeliveryStatusHeaderContent as DeliveryStatusHeaderContentModel,
} from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";

import {
    DeliveryScheduleState,
    DeliveryStatusHeaderText,
    DeliveryOrderStatusState,
    NO_DELIVERY_SCHEDULED_TEXT,
    DeliveryStatusTerminalState,
} from "../constants/delivery-status.constants";

@Injectable()
export class DeliveryStatusService {
    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    showProducts(deliveryData: DeliveryStatus): boolean {
        if (this.isNothingScheduled(deliveryData)) {
            return false;
        } else {
            return true;
        }
    }

    getDeliveryStatusHeaderContent(
        deliveryData: DeliveryStatus
    ): DeliveryStatusHeaderContentModel {
        const deliveryStatusHeaderText: DeliveryStatusHeaderContentModel = {
            title: "",
            subTitle: "",
            icon: "",
        };

        const { orderSummary, scheduleSummary } = deliveryData;

        if (this.isNothingScheduled(deliveryData)) {
            return deliveryStatusHeaderText;
        } else if (
            scheduleSummary &&
            (!orderSummary ||
                (orderSummary &&
                    DeliveryStatusTerminalState.indexOf(orderSummary.status) >
                        -1))
        ) {
            return {
                ...DeliveryStatusHeaderText.ORDER_SCHEDULED,
                icon: "arrow_right",
            };
        } else {
            return this.getDeliveryStatusHeaderTextFromOrderSummary(
                deliveryData
            );
        }
    }

    getDeliveryStatusNewHeaderContent(
        deliveryData: DeliveryStatus
    ): DeliveryStatusNewHeaderContent {
        if (!deliveryData || !deliveryData.scheduleSummary) {
            return {
                title: NO_DELIVERY_SCHEDULED_TEXT.heading,
                subTitle: NO_DELIVERY_SCHEDULED_TEXT.subHeading,
                dateToHighlight: this.dateService.dateFnsFormatDate(
                    this.dateService.getTomorrowDate()
                ),
            };
        }

        const { scheduleSummary } = deliveryData;
        let dateToHighlight: string = this.dateService.dateFnsFormatDate(
            this.dateService.getTomorrowDate()
        );

        if (this.dateService.hasTodaysCutOffTimePassed()) {
            const today = this.dateService.getTodayDate();
            dateToHighlight = this.dateService.dateFnsFormatDate(
                this.dateService.addDays(today, 2)
            );
        }

        return {
            title: this.utilService.getNestedValue(
                scheduleSummary,
                "message.title",
                ""
            ),
            subTitle: this.utilService.getNestedValue(
                scheduleSummary,
                "message.subtitle",
                ""
            ),

            dateToHighlight,
        };
    }

    getDeliveryStatusNotificationContent(
        deliveryData: DeliveryStatus
    ): SlotInfo {
        if (!deliveryData || !deliveryData.orderSummary) {
            return;
        }

        const { orderSummary } = deliveryData;
        let deliveryStatusNotificationContent = {};

        if (orderSummary && orderSummary.orders && orderSummary.slot_info) {
            // tslint:disable-next-line: forin
            for (const slot in orderSummary.slot_info) {
                const slotHasOrders = orderSummary.orders.find(
                    (order) => order.delivery_type === slot
                );

                if (slotHasOrders) {
                    deliveryStatusNotificationContent = {
                        ...deliveryStatusNotificationContent,
                        [slot]: orderSummary.slot_info[slot],
                    };
                }
            }

            return deliveryStatusNotificationContent;
        }

        return null;
    }

    getDeliveredProductsInfo(
        orderSummary: OrderSummary,
        skuList: Sku[]
    ): DeliveryStatusProductInfo[] {
        return orderSummary.orders.map((orderItem: OrderSummaryItem) => {
            return {
                quantity: orderItem.delivered_quantity,
                sku: this.getSkuDataFromId(orderItem.sku_id, skuList),
            };
        });
    }

    getProductsToDisplay(
        deliveryData: DeliveryStatus
    ): DeliveryStatusProductInfo[] {
        const { scheduleSummary, skuData = [] } = deliveryData;

        if (this.isNothingScheduled(deliveryData)) {
            return [];
        }

        if (scheduleSummary) {
            const { subscriptions = [], addons = [] } = scheduleSummary;

            const subscriptionProducts: DeliveryStatusProductInfo[] = subscriptions.map(
                (subscriptionItem: ScheduleSubscription) => {
                    return {
                        quantity: subscriptionItem.quantity,
                        sku: this.getSkuDataFromId(
                            subscriptionItem.sku_id,
                            skuData
                        ),
                        sku_mask_details:
                            subscriptionItem &&
                            subscriptionItem.meta &&
                            subscriptionItem.meta.sku_mask_details,
                    };
                }
            );

            const addonProducts: DeliveryStatusProductInfo[] = addons.map(
                (subscriptionItem: ScheduleSubscription) => {
                    return {
                        quantity: subscriptionItem.quantity,
                        sku: this.getSkuDataFromId(
                            subscriptionItem.sku_id,
                            skuData
                        ),
                        sku_mask_details:
                            subscriptionItem &&
                            subscriptionItem.meta &&
                            subscriptionItem.meta.sku_mask_details,
                    };
                }
            );

            return [...subscriptionProducts, ...addonProducts];
        }

        return [];
    }

    getPreviousWeekDateAndDay(): Array<DelivereyStatusCurrentWeek> {
        const currentWeekDates = this.dateService.getEachDayOfWeek();

        return currentWeekDates.map((d: Date) => {
            return {
                dateObject: d,
                date: d.getDate(),
                day: this.dateService.getDayOfWeek(d, "iii"),
                formattedDate: this.dateService.dateFnsFormatDate(d),
            };
        });
    }

    getCurrentWeekDateAndDay(): Array<DelivereyStatusCurrentWeek> {
        let currentWeekDates = this.dateService.getEachDayOfWeek();
        const isLastDayOfWeek = this.calendarService.isTodayLastDayOfWeek(
            currentWeekDates
        );

        /* If it's the last day of the week, show next week's calendar */
        if (isLastDayOfWeek) {
            const tomorrow = this.dateService.getTomorrowDate();

            currentWeekDates = this.dateService.getEachDayOfWeek(1, tomorrow);
        }

        return currentWeekDates.map((d: Date) => {
            return {
                dateObject: d,
                date: d.getDate(),
                day: this.dateService.getDayOfWeek(d, "iii"),
                formattedDate: this.dateService.dateFnsFormatDate(d),
            };
        });
    }

    private isNothingScheduled(deliveryData: DeliveryStatus): boolean {
        if (!deliveryData) {
            return;
        }

        const { scheduleSummary } = deliveryData;

        return (
            !scheduleSummary ||
            (scheduleSummary &&
                scheduleSummary.status ===
                    DeliveryScheduleState.NOTHING_SCHEDULED)
        );
    }

    private getDeliveryStatusHeaderTextFromOrderSummary(
        deliveryData: DeliveryStatus
    ): DeliveryStatusHeaderContentModel {
        let deliveryStatusHeaderText = { title: "", subTitle: "", icon: "" };
        const { orderSummary } = deliveryData;
        const { status, date } = orderSummary;

        const daysDifferenceBetweenNowAndScheduledDay = this.dateService.daysFromToday(
            this.dateService.dateFromText(date)
        );
        if (daysDifferenceBetweenNowAndScheduledDay === 1) {
            deliveryStatusHeaderText = {
                ...DeliveryStatusHeaderText.ORDER_SCHEDULED,
                icon: "arrow_right",
            };
        } else if (
            daysDifferenceBetweenNowAndScheduledDay === 0 ||
            daysDifferenceBetweenNowAndScheduledDay === -1
        ) {
            if (status === DeliveryOrderStatusState.PICKED_UP) {
                deliveryStatusHeaderText = {
                    title: DeliveryStatusHeaderText.PICKED_UP.title,
                    subTitle:
                        status === DeliveryOrderStatusState.DELAYED ||
                        status === DeliveryOrderStatusState.DELIVERED_PARTIAL
                            ? DeliveryStatusHeaderText.DELAYED.subTitle
                            : DeliveryStatusHeaderText.PICKED_UP.subTitle,
                    icon: "delivery",
                };
            } else {
                deliveryStatusHeaderText = {
                    title: "Arriving today",
                    subTitle: DeliveryStatusHeaderText.ORDER_SCHEDULED.subTitle,
                    icon: "lock",
                };
            }
        } else {
            const deliveryDateObj = new Date(date);
            const deliveryDate = String(deliveryDateObj.getDate());
            deliveryStatusHeaderText = {
                title: `Arriving ${this.dateService.getDayNameOfDate(
                    deliveryDateObj
                )}`,
                subTitle: `At your doorstep on ${deliveryDate}${this.dateService.getDateSuffix(
                    deliveryDate
                )}`,
                icon: "arrow_right",
            };
        }
        return deliveryStatusHeaderText;
    }

    private getSkuDataFromId(skuId: number, skuList: Sku[]) {
        const requiredSku = skuList.filter((item: Sku) => item.id === skuId);
        return requiredSku.length > 0 ? requiredSku[0] : null;
    }
}
