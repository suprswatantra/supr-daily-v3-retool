export const SA_OBJECT_NAMES = {
    CLICK: {
        PHONE_NUMBER: "phone-number",
        ENTER_OTP: "enter-otp",
        GET_OPT_IN_SMS: "get-sms",
        GET_OPT_IN_WHATSAPP: "get-whatsapp",
        WHATS_APP_CHECK: "whatsapp-check",
    },
    IMPRESSION: {
        PHONE_VERIFIED: "phone-verified",
        INCORRECT_OTP: "incorrect-otp",
        WHATS_APP_CHECK: "whatsapp-check",
    },
};

export const SA_OBJECT_VALUES = {
    ACTIVE: "active",
    INACTIVE: "inactive",
};
