export * from "./analytics.constants";

export const TEXTS = {
    OTP_INCORRECT: "This OTP is incorrect. Please try again.",
    NOT_VALID_NUMBER: "Please enter valid number.",
    MOBILE_NUMBER_VERIFIED: "Mobile number verified",
    OTP_NOT_RECIEVED: "Haven’t received your OTP yet?",
    SENT_OTP: "OTP has been sent to",
    RESEND: "RESEND",
    SENDING_OTP: "Sending OTP",
    OTP_DIGIT: "Enter 6 digit OTP",
    LOGIN_TEXT: "We will send you OTP on this number",
    OTP_NOT_SENT: "OTP is not sent, please retry",
    LOGIN_TO_CONTINUE: "Login to continue",
    CONTINUE: "Continue",
    YOUR_PHONE_NUMBER: "Your phone number",
    GET_OPT_IN_SMS: "OTP on SMS",
    GET_OPT_IN_WHATSAPP: "OTP on WhatsApp",
    OTP_INFO_MESSAGE:
        "Receive OTP and other important communication through WhatsApp.",
};

export const ALERT_TYPE = {
    RESEND: "resend",
    ERROR: "error",
    CLIENT: "client",
};

export const COUNTER = 30;
export const INTERVAL_TIME_MS = 1000;
export const ALERT_INTERVAL_TIME_MS = 10000;

export const OTP_INPUT_LENGTH = 6;
export const VALID_NUMBER_LENGTH = 10;

export const VERIFIED_NAVIGATE_DELAY = 1400;

export const LOGIN_PAGE_DATA = {
    CONTENT: [
        {
            className: "banner1",
            titleText: "Milk & groceries delivered daily.",
            subTitleText: "Choose from a wide range of products!",
        },
        {
            className: "banner2",
            titleText: "Order by 11 pm, get it tomorrow morning.",
            subTitleText:
                "Take your time & order groceries as late as 11 pm & we'll deliver it tomorrow morning.",
        },
        {
            className: "banner3",
            titleText: "Buy fresh daily, eat fresh daily.",
            subTitleText:
                "Order just what you need - not more, not less & we'll deliver it to your doorstep!",
        },
        {
            className: "banner4",
            titleText: "Need just 1 apple or a packet of chips? We’ll deliver.",
            subTitleText: "No minimum order.",
        },
        {
            className: "banner5",
            titleText: "Contact-less deliveries.",
            subTitleText: "No interaction, no doorbell rings.",
        },
    ],
};
