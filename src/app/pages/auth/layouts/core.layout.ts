import { Component, OnInit } from "@angular/core";
import { PlatformService } from "@services/util/platform.service";

@Component({
    selector: "supr-auth-layout",
    template: `
        <div class="suprContainer">
            <supr-auth-rtb></supr-auth-rtb>
            <supr-sticky-wrapper>
                <div class="wrapper">
                    <ion-router-outlet
                        animated="false"
                        [swipeGesture]="false"
                    ></ion-router-outlet>
                </div>
            </supr-sticky-wrapper>
        </div>
    `,
    styleUrls: ["../styles/login.page.scss"],
})
export class AuthPageLayoutComponent implements OnInit {
    constructor(private platformService: PlatformService) {}

    ngOnInit() {
        this.platformService.setStatusBarForLoginPage();
    }

    ionViewWillEnter() {
        this.platformService.setStatusBarForLoginPage();
    }
}
