import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { SCREEN_NAMES, PAGE_ROUTES } from "@constants";
import { LoginPageContainer } from "./containers/login.container";
import { AuthPageLayoutComponent } from "./layouts/core.layout";
import { OtpPageContainer } from "./containers/otp.container";

const authRoutes: Routes = [
    {
        path: "",
        component: AuthPageLayoutComponent,
        children: [
            {
                path: "",
                redirectTo: PAGE_ROUTES.AUTH.CHILDREN.LOGIN.PATH,
                pathMatch: "full",
            },
            {
                path: PAGE_ROUTES.AUTH.CHILDREN.LOGIN.PATH,
                component: LoginPageContainer,
                data: {
                    screenName: SCREEN_NAMES.AUTH_LOGIN,
                },
            },
            {
                path: PAGE_ROUTES.AUTH.CHILDREN.OTP.PATH,
                component: OtpPageContainer,
                data: {
                    screenName: SCREEN_NAMES.AUTH_OTP,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(authRoutes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
