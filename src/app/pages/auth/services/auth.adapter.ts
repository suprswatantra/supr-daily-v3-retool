import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { StoreState } from "app/store";

import { SuprApi } from "@types";

import {
    AuthStoreActions,
    AuthStoreSelectors,
    UserStoreSelectors,
    AddressStoreSelectors,
    UserStoreActions,
    MiscStoreActions,
} from "@supr/store";
import { User } from "@shared/models";

@Injectable()
export class AuthPageAdapter {
    mobile$ = this.store.pipe(select(AuthStoreSelectors.selectLoginMobile));

    isWhatsAppOptIn$ = this.store.pipe(
        select(AuthStoreSelectors.selectWhatsAppStatus)
    );

    sendingOtp$ = this.store.pipe(select(AuthStoreSelectors.selectSendingOtp));

    verifyingOtp$ = this.store.pipe(
        select(AuthStoreSelectors.selectVerifyingOtp)
    );

    authStateWithError$ = this.store.pipe(
        select(AuthStoreSelectors.authStateWithError)
    );

    errMsg$ = this.store.pipe(
        select(AuthStoreSelectors.selectAuthErrorMessage)
    );

    isLogoutFlag$ = this.store.pipe(select(AuthStoreSelectors.logoutFlag));

    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    address$ = this.store.pipe(select(AddressStoreSelectors.selectAddress));

    constructor(private store: Store<StoreState>) {}

    sendOtp(payload: SuprApi.PasswordReqBody) {
        this.store.dispatch(new AuthStoreActions.SendOtpRequestAction(payload));
    }

    verifyOtp(payload: { mobile: string; otp: string }) {
        this.store.dispatch(
            new AuthStoreActions.VerifyOtpRequestAction(payload)
        );
    }

    fetchProfileWithAddress() {
        this.store.dispatch(
            new UserStoreActions.LoadProfileWithAddressRequestAction({
                address: true,
                wallet: true,
                t_plus_one: true,
            })
        );
    }

    updateWhatsappStatus(user: User) {
        this.store.dispatch(
            new UserStoreActions.UpdateProfileRequestAction({ user })
        );
    }

    setWhatsAppOptInEnabled() {
        this.store.dispatch(new UserStoreActions.SetWhatsAppOptInEnabled());
    }

    toggleAppLaunchDone() {
        this.store.dispatch(new MiscStoreActions.ToggleAppLaunchDone());
    }
}
