import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { Address, User } from "@models";
import { AuthPageAdapter as Adapter } from "../services/auth.adapter";

import { InitializeService } from "@services/util/initialize.service";

@Component({
    selector: "supr-auth-verified-container",
    template: `
        <supr-auth-verified
            [user]="user$ | async"
            [address]="address$ | async"
            [isLogoutFlag]="isLogoutFlag$ | async"
            (loadProfile)="fetchProfile()"
            (toggleAppLaunchDone)="toggleAppLaunchDone()"
            (getLaunchData)="getLaunchData()"
        ></supr-auth-verified>
    `,
    styleUrls: ["../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerifiedContainer implements OnInit {
    @Input() isWhatsAppOptIn: boolean;

    user$: Observable<User>;
    address$: Observable<Address>;
    isLogoutFlag$: Observable<boolean>;

    constructor(
        private adapter: Adapter,
        private initializeService: InitializeService
    ) {}

    ngOnInit() {
        this.user$ = this.adapter.user$;
        this.address$ = this.adapter.address$;
        this.isLogoutFlag$ = this.adapter.isLogoutFlag$;
    }

    fetchProfile() {
        this.adapter.fetchProfileWithAddress();

        if (this.isWhatsAppOptIn) {
            this.setWhatsAppOptInEnabled();
        }
    }

    toggleAppLaunchDone() {
        this.adapter.toggleAppLaunchDone();
    }

    getLaunchData() {
        const launchSubscription = this.initializeService
            .getLaunchData()
            .subscribe(
                () => {
                    /** Do nothing, global error component takes care this */
                },
                () => {
                    if (launchSubscription && !launchSubscription.closed) {
                        launchSubscription.unsubscribe();
                    }
                }
            );
    }

    private setWhatsAppOptInEnabled() {
        // Dispatch Action to update store for isWhatsAppOptin
        this.adapter.setWhatsAppOptInEnabled();
    }
}
