import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { AuthPageAdapter as Adapter } from "../services/auth.adapter";

@Component({
    selector: "supr-otp-container",
    template: `
        <supr-auth-otp
            [mobile]="mobile$ | async"
            [errorMsg]="errorMsg$ | async"
            [verifyingOtp]="verifyingOtp$ | async"
            [isWhatsAppOptIn]="isWhatsAppOptIn$ | async"
            (handleVerifyOtp)="verifyOtp($event)"
            (handleUpdateWhatsappStatus)="updateWhatsappStatus($event)"
        ></supr-auth-otp>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpPageContainer implements OnInit {
    mobile$: Observable<string>;
    errorMsg$: Observable<string>;
    verifyingOtp$: Observable<any>;
    isWhatsAppOptIn$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.mobile$ = this.adapter.mobile$;
        this.isWhatsAppOptIn$ = this.adapter.isWhatsAppOptIn$;
        this.errorMsg$ = this.adapter.errMsg$;
        this.verifyingOtp$ = this.adapter.verifyingOtp$;
    }

    verifyOtp(payload: { mobile: string; otp: string }) {
        this.adapter.verifyOtp(payload);
    }

    updateWhatsappStatus(isWhatsAppOptIn: boolean) {
        this.adapter.updateWhatsappStatus({ isWhatsAppOptIn });
    }
}
