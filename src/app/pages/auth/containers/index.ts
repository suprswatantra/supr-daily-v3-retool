import { LoginPageContainer } from "./login.container";
import { VerifiedContainer } from "./verified.container";
import { OtpResendContainer } from "./resend.container";
import { OtpPageContainer } from "./otp.container";

export const authContainers = [
    LoginPageContainer,
    OtpPageContainer,
    OtpResendContainer,
    VerifiedContainer,
];
