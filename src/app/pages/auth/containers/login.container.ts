import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { AuthPageAdapter as Adapter } from "../services/auth.adapter";
import { SuprApi } from "@types";

@Component({
    selector: "supr-login-container",
    template: `
        <supr-auth-login
            [errorMsg]="errorMsg$ | async"
            [sendingOtp]="sendingOtp$ | async"
            (handleSendOtp)="sendOtp($event)"
        >
        </supr-auth-login>
    `,
    styleUrls: ["../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPageContainer implements OnInit {
    errorMsg$: Observable<string>;
    sendingOtp$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.errorMsg$ = this.adapter.errMsg$;
        this.sendingOtp$ = this.adapter.sendingOtp$;
    }

    sendOtp(PasswordReqBody: SuprApi.PasswordReqBody) {
        this.adapter.sendOtp(PasswordReqBody);
    }
}
