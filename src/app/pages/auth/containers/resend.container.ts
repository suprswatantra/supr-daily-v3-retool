import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";

import { Observable } from "rxjs";

import { AuthPageAdapter as Adapter } from "../services/auth.adapter";

@Component({
    selector: "supr-auth-otp-resend-container",
    template: `
        <supr-auth-otp-resend
            [mobile]="mobile$ | async"
            [errorMsg]="errorMsg$ | async"
            [incorrectOtp]="incorrectOtp"
            [sendingOtp]="sendingOtp$ | async"
            (handleResendOtp)="sendOtp($event)"
        ></supr-auth-otp-resend>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpResendContainer implements OnInit {
    @Input() incorrectOtp: boolean;

    mobile$: Observable<string>;
    errorMsg$: Observable<string>;
    sendingOtp$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.mobile$ = this.adapter.mobile$;
        this.errorMsg$ = this.adapter.errMsg$;
        this.sendingOtp$ = this.adapter.sendingOtp$;
    }

    sendOtp(mobile: string) {
        this.adapter.sendOtp({ phone_number: mobile, resend: true });
    }
}
