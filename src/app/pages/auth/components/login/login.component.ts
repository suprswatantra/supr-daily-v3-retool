import {
    Component,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

import { SuprApi } from "@types";

import {
    VALID_NUMBER_LENGTH,
    ALERT_TYPE,
    TEXTS,
    SA_OBJECT_NAMES,
    SA_OBJECT_VALUES,
} from "@pages/auth/constants";

@Component({
    selector: "supr-auth-login",
    template: `
        <div class="login">
            <div class="divider16"></div>
            <supr-text type="subtitle" class="title">
                {{ TEXTS.LOGIN_TO_CONTINUE }}
            </supr-text>
            <div class="divider16"></div>

            <supr-login-input
                (handleInputChange)="inputChange($event)"
                [isError]="_isError"
            ></supr-login-input>
            <div class="divider16"></div>

            <supr-text
                class="loginNote"
                type="paragraph"
                [class.errorText]="_isError"
            >
                {{ message }}
            </supr-text>
            <div class="divider16"></div>

            <div class="suprRow loginActionButton">
                <ng-container *ngIf="!_showLoader; else loader">
                    <supr-button
                        class="message"
                        (handleClick)="goToOtpPage()"
                        [loading]="_showLoader"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.GET_OPT_IN_SMS}"
                        saObjectValue="${SA_OBJECT_VALUES.ACTIVE}"
                    >
                        <supr-text type="body">
                            {{ TEXTS.GET_OPT_IN_SMS }}
                        </supr-text>
                    </supr-button>

                    <div class="spacer16"></div>

                    <supr-button
                        (handleClick)="goToOtpPage(true)"
                        [loading]="_showLoader"
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .GET_OPT_IN_WHATSAPP}"
                        saObjectValue="${SA_OBJECT_VALUES.ACTIVE}"
                    >
                        <div class="suprRow">
                            <supr-text type="body">
                                {{ TEXTS.GET_OPT_IN_WHATSAPP }}
                            </supr-text>
                            <div class="spacer8"></div>

                            <supr-icon name="whatsapp"></supr-icon>
                        </div>
                    </supr-button>
                </ng-container>

                <ng-template #loader>
                    <supr-button [loading]="_showLoader"> </supr-button>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnChanges {
    @Input() errorMsg: string;
    @Input() sendingOtp: boolean;
    @Output() handleSendOtp: EventEmitter<
        SuprApi.PasswordReqBody
    > = new EventEmitter();

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    mobileNumber = "";
    alertType: string;
    isValidNumber = false;
    _showLoader = false;
    _isError = false;
    message = TEXTS.LOGIN_TEXT;
    TEXTS = TEXTS;

    ngOnChanges(changes: SimpleChanges) {
        this.handleAuthStateChange(changes["sendingOtp"]);
    }

    goToOtpPage(isWhatsAppOptIn: boolean) {
        if (!this.isNumber(this.mobileNumber)) {
            this.isValidNumber = false;
            this.alertType = ALERT_TYPE.CLIENT;
        }

        if (this.isValidNumber) {
            this.checkError(false);
            this._showLoader = true;
            this.handleSendOtp.emit(this.getPasswordReqBody(isWhatsAppOptIn));
        } else {
            this.checkError(true);
        }
    }

    inputChange(number: string) {
        this.checkError(false);

        const _number = number.trim();
        if (_number.length === VALID_NUMBER_LENGTH) {
            this.isValidNumber = true;
            this.mobileNumber = _number;
        } else {
            this.isValidNumber = false;
        }
    }

    private isNumber(number: string): boolean {
        return this.utilService.isNumber(+number);
    }

    private getPasswordReqBody(
        isWhatsAppOptIn: boolean
    ): SuprApi.PasswordReqBody {
        return {
            phone_number: this.mobileNumber,
            is_whatsapp_optin: !!isWhatsAppOptIn,
        };
    }

    private handleAuthStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue === true && change.currentValue === false) {
            this._showLoader = false;
            this.isValidNumber = false;
            if (this.errorMsg) {
                this.checkError(true);
                this.alertType = ALERT_TYPE.CLIENT;
            } else {
                this.checkError(false);
                this.routerService.goToPasswordPage();
            }
        }
    }

    private checkError(error: boolean) {
        this._isError = error;

        if (error) {
            this.message = TEXTS.NOT_VALID_NUMBER;
        } else {
            this.message = TEXTS.LOGIN_TEXT;
        }
    }
}
