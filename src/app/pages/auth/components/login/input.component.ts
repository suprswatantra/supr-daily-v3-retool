import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
    Input,
    OnChanges,
    SimpleChanges,
    ElementRef,
    ViewChild,
    OnDestroy,
    ChangeDetectorRef,
} from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { INPUT } from "@constants";

import { InputComponent } from "@shared/components/supr-input/supr-input.component";

import { TEXTS } from "@pages/auth/constants";
import { SA_OBJECT_NAMES } from "@pages/auth/constants/analytics.constants";

@Component({
    selector: "supr-login-input",
    template: `
        <div class="suprRow loginInput" [class.errorInput]="isError">
            <div class="loginInputPrefix">
                <supr-text type="subheading" [class.light]="!inputEmpty">
                    +91
                </supr-text>
            </div>

            <supr-input
                [type]="type"
                [placeholder]="TEXTS.YOUR_PHONE_NUMBER"
                [maxLength]="10"
                (handleInputChange)="updateNumber($event)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.PHONE_NUMBER}"
            ></supr-input>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginInputComponent implements OnInit, OnChanges, OnDestroy {
    @Input() isError: boolean;
    @Output() handleInputChange: EventEmitter<string> = new EventEmitter();

    @ViewChild(InputComponent, { static: true }) inputComponent: InputComponent;

    type = INPUT.TYPES.NUMBER;
    TEXTS = TEXTS;
    inputEmpty = true;

    private init = false;
    private inputEl: ElementRef;
    private routerSub: Subscription;

    constructor(private router: Router, private cdr: ChangeDetectorRef) {
        this.routerSub = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isLoginPageUrl(event.url))
            )
            .subscribe(() => {
                if (this.init) {
                    this.resetMobile();
                }
            });
    }

    ngOnInit() {
        this.inputEl = this.inputComponent.getInputElement();
        this.init = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["isError"];
        if (change && !change.firstChange && change.currentValue) {
            this.resetMobile();
        }
    }

    ngOnDestroy() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    updateNumber(number: string) {
        this.inputEmpty = !number;
        this.cdr.detectChanges();

        this.handleInputChange.emit(number);
    }

    private isLoginPageUrl(url: string): boolean {
        return url && url.indexOf("login") > -1;
    }

    private resetMobile() {
        this.inputEl.nativeElement.value = "";
    }
}
