import { RtbComponent } from "./rtb/rtb.component";
import { LoginComponent } from "./login/login.component";
import { OtpComponent } from "./otp/otp.component";
import { LoginInputComponent } from "./login/input.component";
import { ResendComponent } from "./otp/resend.component";
import { VerifiedComponent } from "./otp/verified.component";
import { OtpFooterComponent } from "./otp/footer.component";
import { WhatsAppOptInfoComponent } from "./otp/whatsapp-opt-in-info.component";

import { ResendTimerComponent } from "./otp/resend-timer.component";

import { RtbBannerComponent } from "./rtb/rtb-banner.component";

export const authComponents = [
    RtbComponent,
    LoginComponent,
    OtpComponent,
    LoginInputComponent,
    ResendComponent,
    VerifiedComponent,
    OtpFooterComponent,
    WhatsAppOptInfoComponent,

    ResendTimerComponent,

    RtbBannerComponent,
];
