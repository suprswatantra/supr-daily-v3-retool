import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { RouterService } from "@services/util/router.service";
import { PlatformService } from "@services/util/platform.service";
import { LOGIN_PAGE_DATA } from "@pages/auth/constants";

@Component({
    selector: "supr-auth-rtb",
    templateUrl: "./rtb.component.html",
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RtbComponent implements OnInit {
    slideOpts = {
        initialSlide: 0,
        speed: 600,
        autoplay: true,
    };

    isIos: boolean;
    loginData = LOGIN_PAGE_DATA.CONTENT;

    constructor(
        private routerService: RouterService,
        private platformService: PlatformService
    ) {}

    ngOnInit() {
        this.isIos = this.platformService.isIOS();
    }

    handleSkipLogin() {
        this.routerService.goToCitySelectionPage();
    }
}
