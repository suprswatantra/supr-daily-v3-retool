import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "rtb-banner",
    template: `
        <div class="rtbBanner">
            <supr-svg class="suprLogo"></supr-svg>
            <supr-svg class="banner" [ngClass]="className"></supr-svg>
            <div class="rtbBannerText">
                <div class="suprRow">
                    <supr-text class="titleText"> {{ title }}</supr-text>
                </div>
                <div class="divider4"></div>
                <div class="suprRow">
                    <supr-text class="helpText">
                        {{ subTitle }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RtbBannerComponent {
    @Input() title: string;
    @Input() subTitle: string;
    @Input() className: string;
}
