import {
    Component,
    ChangeDetectorRef,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { TOAST_MESSAGES } from "@constants";

import { ToastService } from "@services/layout/toast.service";

import { TEXTS } from "@pages/auth/constants";
import { SA_OBJECT_NAMES } from "@pages/auth/constants/analytics.constants";

@Component({
    selector: "supr-auth-otp-resend",
    template: `
        <div class="suprRow otpResend bottom">
            <div class="suprColumn left">
                <div class="suprRow">
                    <supr-text type="caption">{{ TEXTS?.SENT_OTP }}</supr-text>
                    <div class="spacer4"></div>
                    <supr-text type="caption" class="mobile">
                        {{ mobile }}
                    </supr-text>
                </div>
                <div class="divider4"></div>

                <ng-container *ngIf="incorrectOtp; else notReceived">
                    <supr-text
                        type="caption"
                        class="error"
                        saImpression
                        saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                            .INCORRECT_OTP}"
                    >
                        {{ TEXTS?.OTP_INCORRECT }}
                    </supr-text>
                </ng-container>

                <ng-template #notReceived>
                    <supr-text type="caption">
                        {{ TEXTS?.OTP_NOT_RECIEVED }}
                    </supr-text>
                </ng-template>
            </div>

            <div class="suprRow otpResendTimer">
                <ng-container *ngIf="resendActive; else timer">
                    <supr-text
                        type="subtext10"
                        class="active"
                        (click)="resendOtp()"
                    >
                        {{ resending ? TEXTS.SENDING_OTP : TEXTS.RESEND }}
                    </supr-text>
                    <supr-icon
                        name="chevron_right"
                        *ngIf="!resending"
                    ></supr-icon>
                </ng-container>

                <ng-template #timer>
                    <div class="suprRow center base">
                        <supr-text type="caption" class="otpResendTimerLink">
                            Wait
                        </supr-text>
                        <div class="spacer4"></div>
                        <supr-auth-otp-resend-timer
                            (handleTimerFinish)="enableResend()"
                        ></supr-auth-otp-resend-timer>
                    </div>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResendComponent implements OnChanges {
    @Input() mobile: string;
    @Input() sendingOtp: boolean;
    @Input() errorMsg: boolean;
    @Input() incorrectOtp: boolean;
    @Output() handleResendOtp: EventEmitter<string> = new EventEmitter();

    resendActive = false;
    resending = false;
    TEXTS = TEXTS;

    constructor(
        private cdr: ChangeDetectorRef,
        private toastService: ToastService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["sendingOtp"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue === true && change.currentValue === false) {
            this.disableResending();
            if (this.errorMsg) {
                this.showError();
            } else {
                this.disableResend();
            }
        }
    }

    resendOtp() {
        if (this.resendActive && !this.resending) {
            this.resending = true;
            this.handleResendOtp.emit(this.mobile);
            this.cdr.detectChanges();
        }
    }

    enableResend() {
        this.resendActive = true;
    }

    private disableResend() {
        this.resendActive = false;
    }

    private disableResending() {
        this.resending = false;
    }

    private showError() {
        this.toastService.present(TOAST_MESSAGES.AUTH.OTP_NOT_SENT);
    }
}
