import {
    Component,
    Output,
    EventEmitter,
    OnInit,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
} from "@angular/core";

import { COUNTER, INTERVAL_TIME_MS } from "@pages/auth/constants";

@Component({
    selector: "supr-auth-otp-resend-timer",
    template: `
        <span class="otpResend">
            <supr-text type="caption">{{ counter }}s</supr-text>
        </span>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResendTimerComponent implements OnInit, OnDestroy {
    @Output() handleTimerFinish: EventEmitter<void> = new EventEmitter();

    counter = COUNTER;
    private _interval: number;

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnInit() {
        this.setTimer();
    }

    ngOnDestroy() {
        this.clearInterval();
    }

    private setTimer() {
        this._interval = window.setInterval(() => {
            this.counter--;

            // require view to be updated
            this.cdr.markForCheck();

            if (!this.counter) {
                this.clearInterval();
                this.handleTimerFinish.emit();
            }
        }, INTERVAL_TIME_MS);
    }

    private clearInterval() {
        if (this._interval !== null) {
            clearInterval(this._interval);
            this._interval = null;
        }
    }
}
