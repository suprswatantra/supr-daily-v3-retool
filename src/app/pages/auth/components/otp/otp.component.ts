import {
    Component,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    OnInit,
    OnChanges,
    ChangeDetectionStrategy,
    SimpleChange,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { SmsService } from "@services/integration/sms.service";
import { RouterService } from "@services/util/router.service";

import { OTP_INPUT_LENGTH, TEXTS } from "@pages/auth/constants";
import { SA_OBJECT_NAMES } from "@pages/auth/constants/analytics.constants";

@Component({
    selector: "supr-auth-otp",
    template: `
        <div class="otp" [class.extraTopPadding]="isWhatsAppOptIn">
            <ng-container *ngIf="!isVerified; else verified">
                <div class="suprRow">
                    <supr-icon
                        name="chevron_left"
                        (click)="goToLoginPage()"
                        saClick
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .BACK_BUTTON}"
                    ></supr-icon>
                    <div class="spacer4"></div>
                    <supr-text type="subtitle" class="title">
                        {{ TEXTS?.OTP_DIGIT }}
                    </supr-text>
                </div>
                <div class="divider16"></div>

                <supr-otp-input
                    [length]="otpInputLength"
                    [value]="autoReadOtp"
                    (updateOtp)="onOtpUpdate($event)"
                    [error]="_isError"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ENTER_OTP}"
                >
                </supr-otp-input>
                <div class="divider16"></div>

                <supr-auth-otp-resend-container
                    [incorrectOtp]="_isError"
                ></supr-auth-otp-resend-container>
                <div class="divider16"></div>

                <supr-auth-otp-footer
                    [btnDisabled]="isDisabledButton"
                    [isLoading]="verifyingOtp"
                    [isWhatsAppOptIn]="isWhatsAppOptIn"
                    (handleOtpClick)="verifyOtp($event)"
                    (handleChangeNumberClick)="goToLoginPage()"
                >
                </supr-auth-otp-footer>
            </ng-container>

            <ng-template #verified>
                <supr-auth-verified-container
                    [isWhatsAppOptIn]="_isWhatsAppOptIn"
                ></supr-auth-verified-container>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpComponent implements OnInit, OnChanges {
    @Input() mobile: string;
    @Input() errorMsg: string;
    @Input() verifyingOtp: boolean;
    @Input() isWhatsAppOptIn: boolean;

    @Output() handleVerifyOtp: EventEmitter<{
        mobile: string;
        otp: string;
    }> = new EventEmitter();

    @Output() handleUpdateWhatsappStatus: EventEmitter<
        boolean
    > = new EventEmitter();

    _isError = false;
    isVerified = false;
    isDisabledButton = true;
    alertType: string;
    TEXTS = TEXTS;
    otpInputLength = OTP_INPUT_LENGTH;
    autoReadOtp: string;
    _isWhatsAppOptIn = false;

    private otpInput: string;

    constructor(
        private routerService: RouterService,
        private smsService: SmsService
    ) {}

    ngOnInit() {
        this.smsService.setListener((otp: string) => this.setAndVerify(otp));
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleVerifyOtpChange(changes["verifyingOtp"]);
    }

    goToLoginPage() {
        this.routerService.goToLoginPage();
    }

    verifyOtp(isWhatsAppOptIn?: boolean) {
        this._isWhatsAppOptIn = isWhatsAppOptIn;
        this.handleVerifyOtp.emit({ mobile: this.mobile, otp: this.otpInput });
    }

    onOtpUpdate(input: string) {
        this._isError = false;

        if (input.length === this.otpInputLength) {
            this.isDisabledButton = false;
            this.otpInput = input;
        } else {
            this.isDisabledButton = true;
        }
    }

    private handleVerifyOtpChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue === true && change.currentValue === false) {
            if (this.errorMsg) {
                this._isError = true;
            } else {
                this.updateWhatsappStatus();
                this._isError = false;
                this.isVerified = true;
            }
        }
    }

    private setAndVerify(otp: string) {
        this.autoReadOtp = otp;
        this.otpInput = otp;
        this.verifyOtp();
    }

    private updateWhatsappStatus() {
        if (!this._isWhatsAppOptIn) {
            return;
        }

        this.handleUpdateWhatsappStatus.emit(this._isWhatsAppOptIn);
    }
}
