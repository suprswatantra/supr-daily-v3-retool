import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { User, Address } from "@models";

import { RouterService } from "@services/util/router.service";
import { InitializeService } from "@services/util/initialize.service";
import { MoengageService } from "@services/integration/moengage.service";

import { TEXTS, VERIFIED_NAVIGATE_DELAY } from "@pages/auth/constants";
import { SA_OBJECT_NAMES } from "@pages/auth/constants/analytics.constants";

@Component({
    selector: "supr-auth-verified",
    template: `
        <div
            class="otpVerified suprColumn center suprVisibleDelay"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.PHONE_VERIFIED}"
        >
            <supr-svg></supr-svg>
            <div class="divider16"></div>
            <supr-text type="subheading">
                {{ TEXTS?.MOBILE_NUMBER_VERIFIED }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerifiedComponent implements OnInit, OnChanges {
    @Input() user: User;
    @Input() address: Address;
    @Input() isLogoutFlag: boolean;
    @Output() loadProfile: EventEmitter<void> = new EventEmitter();
    @Output() toggleAppLaunchDone: EventEmitter<void> = new EventEmitter();
    @Output() getLaunchData: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
    private timerOver = false;
    private apiCallOver = false;

    constructor(
        private initService: InitializeService,
        private moengageService: MoengageService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.setBackBtnExit(true);
        this.setTimer();
        this.loadProfile.emit();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["user"];
        if (change && !change.previousValue && change.currentValue) {
            this.apiCallOver = true;
            this.handleUserFlow();
        }
    }

    private setTimer() {
        setTimeout(() => {
            this.timerOver = true;
            this.handleUserFlow();
        }, VERIFIED_NAVIGATE_DELAY);
    }

    private handleUserFlow() {
        if (this.timerOver && this.apiCallOver) {
            this.setBackBtnExit(false);
            this.initService.handleUserFlow(this.user, this.address);
            this.toggleAppLaunchDone.emit();
            this.sendAnalyticsEvents();
            if (this.isLogoutFlag) {
                this.getLaunchData.emit();
            }
        }
    }

    private setBackBtnExit(exit = true) {
        this.routerService.setAppExitFlag(exit);
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackLogin();
    }
}
