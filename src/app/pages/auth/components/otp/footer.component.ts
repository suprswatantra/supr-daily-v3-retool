import {
    Component,
    Output,
    EventEmitter,
    Input,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES, ANALYTICS_OBJECT_VALUES } from "@constants";

@Component({
    selector: "supr-auth-otp-footer",
    template: `
        <div class="otpFooter">
            <supr-otp-whats-app-opt-in-info
                *ngIf="!isWhatsAppOptIn"
                [isWhatsAppOptIn]="_isWhatsAppOptIn"
                (handleToggle)="toggle($event)"
            ></supr-otp-whats-app-opt-in-info>

            <div class="divider16"></div>

            <supr-button
                (handleClick)="otpClick()"
                [disabled]="btnDisabled"
                [loading]="isLoading"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.CONTINUE}"
                saObjectValue="${ANALYTICS_OBJECT_VALUES.ACTIVE}"
            >
                <supr-text type="body">Continue</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpFooterComponent implements OnInit {
    @Input() btnDisabled: boolean;
    @Input() isLoading: boolean;
    @Input() isWhatsAppOptIn: boolean;

    @Output() handleOtpClick: EventEmitter<boolean> = new EventEmitter();
    @Output() handleChangeNumberClick: EventEmitter<void> = new EventEmitter();

    _isWhatsAppOptIn = false;

    ngOnInit() {
        this._isWhatsAppOptIn = this.isWhatsAppOptIn;
    }

    otpClick() {
        if (!this.btnDisabled) {
            this.handleOtpClick.emit(this._isWhatsAppOptIn);
        }
    }

    toggle(status: boolean) {
        this._isWhatsAppOptIn = status;
    }
}
