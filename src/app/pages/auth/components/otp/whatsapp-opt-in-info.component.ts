import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TEXTS, SA_OBJECT_NAMES } from "@pages/auth/constants";

@Component({
    selector: "supr-otp-whats-app-opt-in-info",
    template: `
        <div
            class="suprRow otpFooterWhatsApp"
            (click)="toggle()"
            saImpression
            saClick
            saObjectName="${SA_OBJECT_NAMES.CLICK.WHATS_APP_CHECK}"
            [saObjectValue]="!isWhatsAppOptIn"
        >
            <supr-svg></supr-svg>

            <div class="spacer12"></div>

            <supr-text type="caption">
                ${TEXTS.OTP_INFO_MESSAGE}
            </supr-text>

            <div class="spacer8"></div>

            <supr-icon
                [name]="isWhatsAppOptIn ? 'approve_filled' : 'unchecked'"
            ></supr-icon>
        </div>
    `,
    styleUrls: ["../../styles/login.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhatsAppOptInfoComponent {
    @Input() isWhatsAppOptIn: boolean;
    @Output() handleToggle: EventEmitter<boolean> = new EventEmitter();

    toggle() {
        this.isWhatsAppOptIn = !this.isWhatsAppOptIn;
        this.handleToggle.emit(this.isWhatsAppOptIn);
    }
}
