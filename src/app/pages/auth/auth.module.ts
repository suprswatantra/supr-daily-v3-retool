import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { AuthRoutingModule } from "./auth-routing.module";

import { AuthPageLayoutComponent } from "./layouts/core.layout";
import { authComponents } from "./components";
import { authContainers } from "./containers";

import { AuthPageAdapter } from "./services/auth.adapter";

@NgModule({
    imports: [CommonModule, IonicModule, AuthRoutingModule, SharedModule],
    declarations: [
        AuthPageLayoutComponent,
        ...authComponents,
        ...authContainers,
    ],
    providers: [AuthPageAdapter],
})
export class AuthPageModule {}
