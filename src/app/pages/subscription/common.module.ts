import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SubscriptionPageAdapter } from "./services/subscription.adapter";

@NgModule({
    imports: [CommonModule, IonicModule, SharedModule],
    declarations: [],
    providers: [SubscriptionPageAdapter],
})
export class SubscriptionCommonModule {}
