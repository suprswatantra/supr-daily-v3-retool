export const SUBSCRIPTION_EXPIRY_DAYS_LIMIT = -7;
export const SUBSCRIPTION_RECHARGE_DAYS_LIMIT = 7;

export const TEXTS = {
    CONTINUE: "Continue",
    SOME_ISSUES: "Some issues in the cart",
    RESOLVE_ISSUES: "Please resolve the issues to continue",
    CANCEL_SUBSCRIPTION: "Cancel subscription",
    CANCEL_SUBSCRIPTION_CONFIRM:
        "Are you sure you want to cancel the subscription?",
    CANCEL_SUBSCRIPTION_HELPER:
        "Unused amount will be refunded to your Supr Wallet.",
};
