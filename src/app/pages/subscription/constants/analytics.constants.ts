export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        SUBSCRIPTION_RECHARGE_BUTTON: "recharge-button",
        SUBSCRIPTION_RESTART_BUTTON: "restart-button",
        SUBSCRIPTION_SEE_DETAILS_BUTTON: "see-details-button",
        SUBSCRIPTION_CARD: "subscription-card",
        CANCEL_SUBSCRIPTION: "cancel-subscription",
        SUBSCRIPTION_DETAILS_RECHARGE_BUTTON: "recharge-button",
        EDIT_SUBSCRIPTION: "edit-subscription",
        DELIVERY_SCHEDULE: "delivery-schedule",
    },
    IMPRESSION: {},
};
