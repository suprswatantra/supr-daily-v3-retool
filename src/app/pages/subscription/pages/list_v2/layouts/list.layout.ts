import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DEVICE_WIDTHS } from "@constants";

import { Subscription } from "@models";

import { UtilService } from "@services/util/util.service";
import { SubscriptionService } from "@services/shared/subscription.service";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { SUBSCRIPTION_LIST_TYPES } from "../constants";

import { SubscriptionSplit } from "@pages/subscription/types/subscription.types";

@Component({
    selector: "supr-subscription-list-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">My subscriptions</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <ng-container *ngIf="isFetchingSubscriptions(); else content">
                    <div class="loader"><supr-loader></supr-loader></div>
                </ng-container>
                <ng-template #content>
                    <ng-container
                        *ngIf="!isSubscriptionListEmpty; else noSubscriptions"
                    >
                        <supr-subscription-list
                            [smallScreen]="_smallScreen"
                            [subscriptions]="split?.active"
                        ></supr-subscription-list>
                        <supr-subscription-list
                            [smallScreen]="_smallScreen"
                            [subscriptions]="split?.expired"
                            [type]="subscriptionListTypes?.EXPIRED"
                        ></supr-subscription-list>
                    </ng-container>

                    <ng-template #noSubscriptions>
                        <supr-empty-subscription></supr-empty-subscription>
                    </ng-template>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../styles/list.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionListPageLayoutComponent implements OnInit, OnChanges {
    @Input() subscriptions: Subscription[];
    @Input() subscriptionCurrentState: SubscriptionCurrentState;

    @Output() handleFetchSubscriptions: EventEmitter<void> = new EventEmitter();

    _smallScreen = false;
    split: SubscriptionSplit;
    isSubscriptionListEmpty = true;
    subscriptionListTypes = SUBSCRIPTION_LIST_TYPES;

    constructor(
        private subscriptionService: SubscriptionService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        if (
            !this.utilService.isLengthyArray(this.subscriptions) &&
            this.subscriptionCurrentState === SubscriptionCurrentState.NO_ACTION
        ) {
            this.handleFetchSubscriptions.emit();
        } else {
            this.splitSubscriptionList();
        }

        if (screen.width < DEVICE_WIDTHS[360]) {
            this._smallScreen = true;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["subscriptions"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.splitSubscriptionList();
        }
    }

    isFetchingSubscriptions() {
        return (
            this.subscriptionCurrentState ===
            SubscriptionCurrentState.FETCHING_SUBSCRIPTIONS
        );
    }

    private splitSubscriptionList() {
        this.split = this.subscriptionService.splitSubscriptionsIntoActiveAndPast(
            this.subscriptions
        );

        this.checkSubscriptionListEmpty(this.split);
    }

    private checkSubscriptionListEmpty(split: SubscriptionSplit) {
        if (split.active.length > 0 || split.expired.length > 0) {
            this.isSubscriptionListEmpty = false;
        } else {
            this.isSubscriptionListEmpty = true;
        }
    }
}
