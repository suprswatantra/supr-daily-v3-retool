import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { subscriptionListComponents } from "./components";
import { subscriptionListContainers } from "./containers";
import { SubscriptionListPageLayoutComponent } from "./layouts/list.layout";
import { SubscriptionListPageContainer } from "./containers/subscription-list.container";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionListPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        ...subscriptionListComponents,
        ...subscriptionListContainers,
        SubscriptionListPageLayoutComponent,
    ],
})
export class SubscriptionListV2PageModule {}
