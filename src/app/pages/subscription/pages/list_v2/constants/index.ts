export const TEXTS = {
    RESTART_NOW: "RESTART NOW",
    RECHARGE_NOW: "RECHARGE NOW",
    DELIVERIES_LEFT: "Deliveries left",
    QUANTITY_REMAINING: "Quantity remaining",
    DAYS_LEFT: "Days left",
    BALANCE: "Subscription balance",
    VALID_TILL: "Valid till",
    DELIVERIES_APPROX: "Approx.",
    QUANTITY_PER_DAY: "Quantity per day",
};
export const SUBSCRIPTION_LIST_TYPES = {
    ACTIVE: "active",
    EXPIRED: "expired",
    RECHARGE: "recharge",
};

export const SUBSCRIPTION_LIST_TITLES = {
    [SUBSCRIPTION_LIST_TYPES.ACTIVE]: "Active subscriptions",
    [SUBSCRIPTION_LIST_TYPES.EXPIRED]: "Past subscriptions",
};

export const EMPTY_TEXTS = {
    NO_ACTIVE_SUBSCRIPTION: "No active subscriptions yet",
    FIRST_LINE: "Start a subscription now to get Supr grocery",
    SECOND_LINE: "deliveries daily to your doorstep.",
};
