import { SubscriptionTileComponent } from "./subscription-tile/subscription-tile.component";
import { SubscriptionListComponent } from "./subscription-list/subscription-list.component";

import { TileFooterComponent } from "./tile-footer/tile-footer.component";

import { EmptySubscriptionComponent } from "./empty-subscription/empty-subscription.component";

export const subscriptionListComponents = [
    SubscriptionTileComponent,
    SubscriptionListComponent,
    TileFooterComponent,
    EmptySubscriptionComponent,
];
