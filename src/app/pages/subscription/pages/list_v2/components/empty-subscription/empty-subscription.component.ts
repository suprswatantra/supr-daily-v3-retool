import { Component, ChangeDetectionStrategy } from "@angular/core";
import { EMPTY_TEXTS } from "../../constants";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-empty-subscription",
    template: `
        <div class="suprColumn empty center">
            <supr-svg></supr-svg>

            <div class="divider16"></div>

            <supr-text type="heading">
                {{ TEXTS.NO_ACTIVE_SUBSCRIPTION }}
            </supr-text>

            <div class="divider8"></div>

            <supr-text type="caption" class="subText">
                {{ TEXTS.FIRST_LINE }}
            </supr-text>
            <supr-text type="caption" class="subText">
                {{ TEXTS.SECOND_LINE }}
            </supr-text>

            <div class="divider16"></div>

            <supr-button (handleClick)="goToSearchPage()">
                <supr-text type="body">
                    Explore Supr
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./../../styles/empty-subscription.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptySubscriptionComponent {
    TEXTS = EMPTY_TEXTS;

    constructor(private routerService: RouterService) {}

    goToSearchPage() {
        this.routerService.goToSearchPage();
    }
}
