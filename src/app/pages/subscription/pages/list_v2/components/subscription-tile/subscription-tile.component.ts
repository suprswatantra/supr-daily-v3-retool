import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import classNames from "classnames";

import { Sku, Subscription } from "@models";

import { ErrorService } from "@services/integration/error.service";
import { RouterService } from "@services/util/router.service";
import { SubscriptionService } from "@services/shared/subscription.service";
import { SkuService } from "@services/shared/sku.service";

import { Segment } from "@types";

import { SUBSCRIPTION_LIST_TYPES, TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "@pages/subscription/constants/analytics.constants";

@Component({
    selector: "supr-subscription-tile",
    template: `
        <div class="tile" [ngClass]="tileClass" (click)="goToDetailsPage()">
            <supr-card
                saClick
                saImpression
                [saContext]="sku?.id"
                [saContextList]="saContextList"
                [saObjectValue]="subscription?.id"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.SUBSCRIPTION_CARD}"
            >
                <div class="suprRow top">
                    <div class="suprColumn tileImage">
                        <div>
                            <supr-image
                                [src]="sku?.image?.fullUrl"
                                [image]="sku?.image"
                            ></supr-image>
                        </div>
                    </div>
                    <div class="suprColumn stretch top left tileContent">
                        <supr-text type="subheading600" class="tileContentName">
                            {{ sku?.sku_name }}
                        </supr-text>
                        <div class="divider4"></div>
                        <div class="suprRow tileContentFrequency">
                            <supr-icon name="refresh"></supr-icon>
                            <div class="spacer8"></div>
                            <supr-week-days-small
                                [selectedDays]="subscription.frequency"
                            ></supr-week-days-small>
                        </div>
                        <div class="divider16"></div>

                        <div
                            class="tileContentBalance"
                            [class.suprRow]="!smallScreen"
                            *ngIf="!isSuprPassSku"
                        >
                            <supr-text type="action14" class="label">
                                ${TEXTS.QUANTITY_PER_DAY}
                            </supr-text>

                            <div *ngIf="!smallScreen" class="spacer6"></div>
                            <div *ngIf="smallScreen" class="divider4"></div>

                            <div class="suprRow">
                                <supr-text type="action14" class="value">
                                    {{ subscription?.quantity }}
                                </supr-text>
                            </div>
                        </div>
                        <div class="divider6"></div>

                        <div
                            class="tileContentBalance"
                            [class.suprRow]="!smallScreen"
                        >
                            <supr-text
                                type="action14"
                                class="label"
                                *ngIf="isSuprPassSku"
                            >
                                ${TEXTS.DAYS_LEFT}
                            </supr-text>

                            <supr-text
                                type="action14"
                                class="label"
                                *ngIf="!isSuprPassSku"
                            >
                                ${TEXTS.DELIVERIES_LEFT}
                            </supr-text>

                            <div *ngIf="!smallScreen" class="spacer6"></div>
                            <div *ngIf="smallScreen" class="divider4"></div>

                            <div class="suprRow">
                                <supr-text type="action14" class="value">
                                    {{ subscription?.deliveries_remaining }}
                                </supr-text>
                                <div
                                    *ngIf="subscription?.deliveries_remaining"
                                    class="spacer4"
                                ></div>
                                <supr-text
                                    *ngIf="
                                        subscription?.deliveries_remaining &&
                                        !isSuprPassSku
                                    "
                                    type="paragraph"
                                >
                                    ${TEXTS.DELIVERIES_APPROX}
                                </supr-text>
                            </div>
                        </div>
                        <div class="divider6"></div>

                        <div
                            *ngIf="!isSuprPassSku"
                            class="tileContentBalance"
                            [class.suprRow]="!smallScreen"
                        >
                            <supr-text type="action14" class="label">
                                ${TEXTS.QUANTITY_REMAINING}
                            </supr-text>

                            <div *ngIf="!smallScreen" class="spacer6"></div>
                            <div *ngIf="smallScreen" class="divider4"></div>

                            <div class="suprRow">
                                <supr-text type="action14" class="value">
                                    {{ subscription?.balance }}
                                </supr-text>
                                <div
                                    *ngIf="subscription?.balance"
                                    class="spacer4"
                                ></div>
                                <supr-text
                                    type="paragraph"
                                    *ngIf="subscription?.balance"
                                >
                                    ${TEXTS.DELIVERIES_APPROX}
                                </supr-text>
                            </div>
                        </div>
                        <div class="divider6"></div>

                        <div
                            class="tileContentBalance"
                            [class.suprRow]="!smallScreen"
                            *ngIf="!isSuprPassSku"
                        >
                            <supr-text type="action14" class="label">
                                ${TEXTS.BALANCE}
                            </supr-text>

                            <div *ngIf="!smallScreen" class="spacer6"></div>
                            <div *ngIf="smallScreen" class="divider4"></div>

                            <supr-text type="action14" class="value">
                                {{ subscription?.balance_amount | rupee }}
                            </supr-text>
                        </div>
                        <div
                            class="tileContentBalance"
                            [class.suprRow]="!smallScreen"
                            *ngIf="
                                isSuprPassSku &&
                                subscription?.delivery_dates?.last
                            "
                        >
                            <supr-text type="action14" class="label">
                                ${TEXTS.VALID_TILL}
                            </supr-text>

                            <div *ngIf="!smallScreen" class="spacer6"></div>
                            <div *ngIf="smallScreen" class="divider4"></div>

                            <supr-text type="action14" class="value">
                                {{
                                    subscription?.delivery_dates?.last
                                        | date: "d LLL, yyy"
                                }}
                            </supr-text>
                        </div>
                        <supr-subscription-tile-footer
                            [type]="tileType"
                            [subscription]="subscription"
                            [saContext]="sku?.id"
                            [saContextList]="saContextList"
                            [isSuprPassSku]="isSuprPassSku"
                        ></supr-subscription-tile-footer>
                    </div>
                </div>
            </supr-card>
        </div>
    `,
    styleUrls: ["./subscription-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionTileComponent implements OnInit {
    @Input() sku: Sku;
    @Input() type: string;
    @Input() smallScreen: boolean;
    @Input() subscription: Subscription;

    tileType: string;
    isActive: boolean;
    tileClass: string;
    saContextList: Segment.ContextListItem[] = [];
    isSuprPassSku: boolean;

    constructor(
        private routerService: RouterService,
        private subscriptionService: SubscriptionService,
        private skuService: SkuService,
        private errorService: ErrorService
    ) {}

    ngOnInit() {
        this.initData();
    }

    goToDetailsPage() {
        if (this.isSuprPassSku) {
            this.routerService.goToSuprPassPage();
            return;
        }

        if (!this.subscription) {
            this.errorService.logSentryError(new Error(), {
                context: { sku: this.sku, subscription: this.subscription },
                message:
                    "Unable to route to subscription details. Subscription id missing",
            });
            return;
        }
        this.routerService.goToSubscriptionDetailsPage(this.subscription.id);
    }

    private initData() {
        this.isSuprPassSku = this.skuService.isSuprPassSku(this.sku);

        this.setIsActive();
        this.setTileType();
        this.setTileClass();
        this.setContextList();
    }

    private setIsActive() {
        this.isActive = this.type === SUBSCRIPTION_LIST_TYPES.ACTIVE;
    }

    private setTileClass() {
        this.tileClass = classNames({
            tileActive: this.isActive,
            tileExpired: !this.isActive,
            tileRecharge: this.isRechargeModeEnabled(),
        });
    }

    private setTileType() {
        if (this.isActive) {
            this.tileType = this.isRechargeModeEnabled()
                ? "recharge"
                : "active";

            return;
        }

        this.tileType = "expired";
    }

    private setContextList() {
        if (this.subscription) {
            this.saContextList.push(
                {
                    name: "deliveries",
                    value: this.subscription.deliveries_remaining,
                },
                {
                    name: "balance",
                    value: this.subscription.balance_amount,
                }
            );

            if (this.isSuprPassSku) {
                this.saContextList.push({
                    name: "skuType",
                    value: "supr-pass",
                });
            }
        }
    }

    private isRechargeModeEnabled() {
        if (!this.isActive) {
            return false;
        }

        return this.subscriptionService.isRechargeReadySubscription(
            this.subscription
        );
    }
}
