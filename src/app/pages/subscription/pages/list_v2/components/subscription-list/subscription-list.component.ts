import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription } from "@models";

import {
    SUBSCRIPTION_LIST_TYPES,
    SUBSCRIPTION_LIST_TITLES,
} from "../../constants";

@Component({
    selector: "supr-subscription-list",
    template: `
        <div
            class="list"
            [class.listActive]="isActive"
            *ngIf="subscriptions?.length"
        >
            <div class="divider16"></div>

            <ng-container
                *ngFor="
                    let subscription of subscriptions;
                    trackBy: trackByFn;
                    last as _lastItem
                "
            >
                <supr-subscription-tile
                    [type]="type"
                    [smallScreen]="smallScreen"
                    [subscription]="subscription"
                    [sku]="subscription?.sku_data"
                ></supr-subscription-tile>

                <div class="divider16" *ngIf="!_lastItem"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/list.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionListComponent implements OnInit {
    @Input() smallScreen: boolean;
    @Input() subscriptions: Subscription[];
    @Input() type = SUBSCRIPTION_LIST_TYPES.ACTIVE;

    isActive: boolean;
    listTitles = SUBSCRIPTION_LIST_TITLES;

    ngOnInit() {
        if (!this.subscriptions) {
            return;
        }

        this.initData();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private initData() {
        this.isActive = this.type === SUBSCRIPTION_LIST_TYPES.ACTIVE;
    }
}
