import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription } from "@models";

import { RouterService } from "@services/util/router.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { Segment } from "@types";

import { TEXTS, SUBSCRIPTION_LIST_TYPES } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "@pages/subscription/constants/analytics.constants";

@Component({
    selector: "supr-subscription-tile-footer",
    template: `
        <ng-container
            *ngIf="
                (subscription?.sku_data?.active || isSuprPassSku) &&
                type !== '${SUBSCRIPTION_LIST_TYPES.ACTIVE}'
            "
        >
            <div class="divider16"></div>
            <div class="footer suprRow" [ngClass]="type">
                <supr-button
                    (click)="stopBubbling($event)"
                    (handleClick)="goToRechargePage()"
                    [saContext]="saContext"
                    [saObjectName]="_saObjectName"
                    [saContextList]="saContextList"
                    [saObjectValue]="subscription?.id"
                >
                    <div class="suprRow">
                        <supr-text type="paragraph">{{ _btnText }}</supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </supr-button>
            </div>
        </ng-container>
    `,
    styleUrls: ["./tile-footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileFooterComponent implements OnInit {
    @Input() type: string;
    @Input() subscription: Subscription;
    @Input() saContext: number;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() isSuprPassSku: boolean;

    _btnText: string;
    _saObjectName: string;

    constructor(
        private routerService: RouterService,
        private blockAccessService: BlockAccessService
    ) {}

    ngOnInit() {
        this.setButtonData();
    }

    goToRechargePage() {
        /* Block recharge page route in case of access restrictions */
        if (this.blockAccessService.isSubscriptionAccessRestricted()) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.routerService.goToSubscriptionRechargePage(this.subscription.id);
    }

    stopBubbling(event: MouseEvent | TouchEvent) {
        event.stopPropagation();
    }

    private setButtonData() {
        if (this.type !== SUBSCRIPTION_LIST_TYPES.ACTIVE) {
            this._btnText =
                this.type === SUBSCRIPTION_LIST_TYPES.RECHARGE
                    ? TEXTS.RECHARGE_NOW
                    : TEXTS.RESTART_NOW;

            this._saObjectName =
                this.type === SUBSCRIPTION_LIST_TYPES.RECHARGE
                    ? ANALYTICS_OBJECT_NAMES.CLICK.SUBSCRIPTION_RECHARGE_BUTTON
                    : ANALYTICS_OBJECT_NAMES.CLICK.SUBSCRIPTION_RESTART_BUTTON;
        }
    }
}
