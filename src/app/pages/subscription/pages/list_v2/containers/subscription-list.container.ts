import { Component, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { SubscriptionPageAdapter as Adapter } from "@pages/subscription/services/subscription.adapter";

import { Subscription } from "@models";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

@Component({
    selector: "supr-subscription-list-container",
    template: `
        <supr-subscription-list-layout
            [subscriptions]="subscriptions$ | async"
            [subscriptionCurrentState]="subscriptionCurrentState$ | async"
            (handleFetchSubscriptions)="handleFetchSubscriptions()"
        ></supr-subscription-list-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionListPageContainer {
    subscriptions$: Observable<Subscription[]>;
    subscriptionCurrentState$: Observable<SubscriptionCurrentState>;

    constructor(private adapter: Adapter) {
        this.subscriptions$ = this.adapter.subscriptions$;
        this.subscriptionCurrentState$ = this.adapter.subscriptionState$;
    }

    handleFetchSubscriptions() {
        this.adapter.fetchSubscriptions();
    }
}
