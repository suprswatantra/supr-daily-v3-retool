export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        DELIVERY_FREQUENCY: "delivery-frequency",
        SUPR_PASS_PLAN_CLICK_RECHARGE_SUBSCRIPTION:
            "supr-pass-plan-click-recharge-subscription",
        RECHARGE_SUPR_PASS: "supr-pass-recharge",
        SUPR_PASS_GO_TO_CART: "supr-pass-go-to-cart",
        SUBSCRIPTION_GO_TO_CART: "proceed-to-cart",
    },
};
