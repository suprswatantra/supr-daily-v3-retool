export const TEXTS = {
    DELIVERY_FEE: "Service fee",
    FINAL_AMOUNT: "Amount to pay",
    WALLET_BALANCE: "Wallet balance available",
    AMOUNT_TO_ADD: "Amount to add",
    TOTAL_NUMBER_OF_DELIVERIES: "Total number of deliveries",
    PRICE_PER_DELIVERY: "Price per delivery",
    CART_TOTAL: "Cart total",
    CREDITS_USED: "Supr Credits used",
    CREDITS_GAINED: "You will earn cashback on this order",
    FREE: "Free",
    DEL_FEE_REFUND_MESSAGE:
        "Congrats you’re getting service fee refunded as Supr Credits",
    PROCEED_TO_CART: "Proceed to cart",
};

export const SUPR_PASS_TEXTS = {
    SUPR_PASS_APPLIED: "Supr Access Membership Applied!",
    SUPR_PASS: "Supr Access",
    APPLY_SUPR_PASS: "Want zero service fees? Get ",
    SUPR_PASS_PLANS: "Select the plan to recharge",
    MEMBERSHIP: "Your membership will extend till",
};
