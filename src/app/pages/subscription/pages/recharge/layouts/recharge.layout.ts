import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    OnDestroy,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Subscription as RxSubscription } from "rxjs";
import { filter } from "rxjs/operators";

import {
    DEFAULT_RECHARGE_OPTION,
    RATING_NUDGE_TRIGGERS,
    TOAST_MESSAGES,
    PAGE_ROUTES,
    SETTINGS,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";
import {
    Subscription,
    CartItem,
    CartMeta,
    SuprPassInfo,
    SuprPassPlan,
    AutoDebitPaymentMode,
    AutoDebitPlan,
} from "@models";

import { SubscriptionService } from "@services/shared/subscription.service";
import { CartService } from "@services/shared/cart.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";
import { MoengageService } from "@services/integration/moengage.service";
import { NudgeService } from "@services/layout/nudge.service";
import { SkuService } from "@services/shared/sku.service";
import { ErrorService } from "@services/integration/error.service";
import { SettingsService } from "@services/shared/settings.service";

import { CartCurrentState } from "@store/cart/cart.state";
import { SuprApi } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-subscription-recharge-layout",
    template: `
        <div class="suprContainer">
            <ng-container
                *ngIf="
                    (_cartUpdating && !rechargeThroughCart) || _dataLoading;
                    else layoutContent
                "
            >
                <supr-recharge-loader-skeleton></supr-recharge-loader-skeleton>
            </ng-container>

            <ng-template #layoutContent>
                <supr-page-header>
                    <supr-text type="subheading">
                        Recharge subscription
                    </supr-text>
                </supr-page-header>
                <div class="suprScrollContent">
                    <supr-subscription-recharge-header-container
                        [subscription]="subscription"
                    ></supr-subscription-recharge-header-container>

                    <div class="body">
                        <ng-container
                            *ngIf="!_isSuprPassSku; else plansWrapper"
                        >
                            <supr-subscription-recharge-deliveries
                                [selectedQty]="_rechargeQty"
                                (handleQtyChange)="updateRechargeQty($event)"
                            >
                            </supr-subscription-recharge-deliveries>
                        </ng-container>

                        <ng-template #plansWrapper>
                            <supr-pass-recharge-plans
                                [suprPassInfo]="suprPassInfo"
                                [selectedPlan]="selectedPlan"
                                (handlePlanSelect)="onPlanSelect($event)"
                            ></supr-pass-recharge-plans>
                        </ng-template>

                        <ng-container
                            *ngIf="!_isSuprPassSku && !rechargeThroughCart"
                        >
                            <div class="divider24"></div>
                            <supr-discount-block-container
                                [fromRechargeSubscriptionPage]="true"
                            ></supr-discount-block-container>
                            <div class="divider24"></div>

                            <supr-subscription-recharge-summary-container
                                [skuId]="subscription?.sku_id"
                                [isSuprPassSku]="_isSuprPassSku"
                                [rechargeQty]="_rechargeQty"
                                [unitQty]="subscription?.quantity"
                            ></supr-subscription-recharge-summary-container>
                        </ng-container>
                    </div>
                </div>

                <supr-auto-debit-payment-container
                    [texts]="suprPassInfo?.autoDebitData?.texts"
                    [startPaymentProcess]="showAutoDebitPaymentScreen"
                    [selectedSuprPassPlan]="selectedPlan"
                    [selectedAutoDebitPlan]="selectedAutoDebitPlan"
                    [isActiveSuprPassMember]="false"
                    (cancelPaymentProcess)="toggleAutoDebitPaymentScreen(false)"
                    (handlePaymentSuccess)="handleAutoDebitPaymentSuccess()"
                    (handlePaymentFailure)="handleAutoDebitPaymentFailure()"
                    (proceedToCart)="goToCartPage()"
                    *ngIf="showAutoDebitPaymentScreen"
                ></supr-auto-debit-payment-container>

                <supr-subscription-recharge-footer-container
                    (handlePay)="placeOrder()"
                    *ngIf="!_isSuprPassSku && !rechargeThroughCart"
                ></supr-subscription-recharge-footer-container>

                <supr-subscription-recharge-footer-v2
                    [saObjectName]="
                        _isSuprPassSku
                            ? analyticsObjectNames.CLICK.SUPR_PASS_GO_TO_CART
                            : analyticsObjectNames.CLICK.SUBSCRIPTION_GO_TO_CART
                    "
                    (handleFooterClick)="handleFooterClick()"
                    *ngIf="rechargeThroughCart"
                >
                </supr-subscription-recharge-footer-v2>

                <ng-container *ngIf="_placingOrder">
                    <supr-loader-overlay>Recharging</supr-loader-overlay>
                </ng-container>

                <ng-container *ngIf="_validatingCart">
                    <supr-recharge-loader-overlay></supr-recharge-loader-overlay>
                </ng-container>
            </ng-template>
        </div>
    `,
    styleUrls: ["../styles/recharge.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})

/** TODO: Cleanup in place recharge methods and components once "rechargeThroughCart"
 * experiment has conclude @Manish */
export class SubscriptionRechargePageLayoutComponent
    implements OnInit, OnChanges, OnDestroy {
    @Input() subscription: Subscription;
    @Input() cartCurrentState: CartCurrentState;
    @Input() cartMeta: CartMeta;
    @Input() cartError: any;
    @Input() suprPassInfo: SuprPassInfo;
    @Input() cartItems: CartItem[];
    @Input() autoDebitPaymentModes: AutoDebitPaymentMode[];

    @Output()
    handleValidateCart: EventEmitter<SuprApi.CartBody> = new EventEmitter();

    @Output()
    handleFetchSubscriptions: EventEmitter<void> = new EventEmitter();
    @Output()
    handleFetchSuprPassInfo: EventEmitter<void> = new EventEmitter();

    @Output()
    handleUpdateCart: EventEmitter<{
        cartItem: CartItem;
        validateCart: boolean;
    }> = new EventEmitter();

    @Output()
    handleCheckoutCart: EventEmitter<void> = new EventEmitter();

    @Output() fetchSubscriptionSchedule: EventEmitter<{
        subscriptionId: number;
    }> = new EventEmitter();

    @Output() handleFetchAutoDebitPaymentModes: EventEmitter<
        void
    > = new EventEmitter();

    _rechargeQty = DEFAULT_RECHARGE_OPTION;
    _couponCode: string;
    _cartUpdating = false;
    _validatingCart = false;
    _isCheckingOut = false;
    _placingOrder = false;
    _isSuprPassSku = false;
    _dataLoading = false;
    selectedPlan: SuprPassPlan;
    isAutoDebitEnabled = true;
    showAutoDebitPaymentScreen = false;
    selectedAutoDebitPlan: AutoDebitPlan = null;
    rechargeThroughCart = false;
    analyticsObjectNames = ANALYTICS_OBJECT_NAMES;

    private init = false;
    private routerSub: RxSubscription;

    constructor(
        private subscriptionService: SubscriptionService,
        private cartService: CartService,
        private utilService: UtilService,
        private routerService: RouterService,
        private toastService: ToastService,
        private moengageService: MoengageService,
        private nudgeService: NudgeService,
        private router: Router,
        private skuService: SkuService,
        private errorService: ErrorService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCartStateChange(changes["cartCurrentState"]);
        this.handleSubscriptionChange(changes["subscription"]);
        this.handleSuprPassInfoChange(changes["suprPassInfo"]);
    }

    ngOnDestroy() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    updateRechargeQty(qty: number) {
        this._rechargeQty = qty;
        if (!this.rechargeThroughCart) {
            this._validateCart();
        }
    }

    updateCoupon(coupon?: string) {
        this._couponCode = coupon;
    }

    _validateCart() {
        if (this._isSuprPassSku) {
            return;
        }

        const cartBody = this.subscriptionService.getSubscriptionRechargeValidateCartBody(
            this.subscription,
            this._rechargeQty,
            this._couponCode,
            this._isSuprPassSku,
            this.selectedPlan
        );
        setTimeout(() => this.handleValidateCart.emit(cartBody), 0);
    }

    placeOrder() {
        if (this._isSuprPassSku) {
            return;
        }

        this._isCheckingOut = true;
        setTimeout(() => this.handleValidateCart.emit(), 0);
    }

    handleFooterClick() {
        const hasActiveAutoDebit: boolean = this.utilService.getNestedValue(
            this.suprPassInfo,
            "autoDebitData.isActive",
            false
        );

        if (
            this._isSuprPassSku &&
            !hasActiveAutoDebit &&
            this.isAutoDebitEnabled &&
            this.selectedAutoDebitPlan
        ) {
            this.handleSuprPassRechargeWithAutoDebit();
            return;
        }

        this.goToCartPage();
    }

    goToCartPage() {
        this._updateItemInCart();
        this.resetUpdatingFlags();
        setTimeout(() => {
            this.routerService.goToCartPage();
        }, 0);
    }

    onPlanSelect(plan: SuprPassPlan) {
        if (plan && plan.durationDays) {
            this._rechargeQty = plan.durationDays;
            this.selectedPlan = plan;
            this.setAutoDebitPlan();
        }
    }

    toggleAutoDebitPaymentScreen(show: boolean) {
        this.showAutoDebitPaymentScreen = show;
    }

    handleAutoDebitPaymentSuccess() {
        this.toggleAutoDebitPaymentScreen(false);
        this.handleFetchSuprPassInfo.emit();
        setTimeout(() => {
            this.routerService.goToSuprPassPage();
        }, 0);
    }

    handleAutoDebitPaymentFailure() {
        this.toggleAutoDebitPaymentScreen(false);
    }

    handleSuprPassRechargeWithAutoDebit() {
        this.toggleAutoDebitPaymentScreen(true);
    }

    setAutoDebitPlan() {
        const autoDebitPlans: AutoDebitPlan[] = this.utilService.getNestedValue(
            this.suprPassInfo,
            "autoDebitData.autoDebitPlans",
            []
        );
        const { id: planId } = this.selectedPlan || { id: null };
        if (autoDebitPlans && autoDebitPlans.length && planId) {
            this.selectedAutoDebitPlan = autoDebitPlans.find(
                (item) => item.suprPassPlanId === planId
            );
        } else {
            this.selectedAutoDebitPlan = null;
        }
    }

    private initialize() {
        this._dataLoading = false;

        if (!this.subscription) {
            this._dataLoading = true;
            this.handleFetchSubscriptions.emit();
        }

        if (!this.autoDebitPaymentModes) {
            this.handleFetchAutoDebitPaymentModes.emit();
        }

        this.setSuprPassInfo();

        this.rechargeThroughCart = this.settingsService.getSettingsValue(
            SETTINGS.RECHARGE_THROUGH_CART,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.RECHARGE_THROUGH_CART]
        );

        this.isAutoDebitEnabled = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_SUPR_PASS_AUTO_DEBIT,
            true
        );

        this.init = true;

        if (this._isSuprPassSku) {
            return;
        }

        this._updateItemInCart();
        this.initRouterEvents();
        this.handleAutoCheckout();
    }

    private initRouterEvents() {
        this.routerSub = this.router.events
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                if (this.isRechargePageUrl(event.url)) {
                    if (this.init) {
                        this.handleAutoCheckout();
                    }
                } else if (!this.isWalletPageUrl(event.url)) {
                    this.cartService.restoreCartFromDb();
                }
            });
    }

    private _updateItemInCart() {
        if (!this.subscription) {
            return;
        }

        const cartItem = this.subscriptionService.getSubscriptionRechargeCartItem(
            this.subscription,
            this._rechargeQty,
            this._isSuprPassSku,
            this.selectedPlan
        );

        /** Check added for extra info in sentry as error was coming from  getSubscriptionRechargeCartItem */
        if (!cartItem) {
            this.errorService.logSentryError(
                new Error(),
                {
                    context: {
                        subscription: this.subscription,
                        qty: this._rechargeQty,
                        isSuprPassSku: this._isSuprPassSku,
                        selectedPlan: this.selectedPlan,
                        cartMeta: this.cartMeta,
                        cartItems: this.cartItems,
                    },
                    message:
                        "error fetching cart item during itemUpdate in subscription recharge layout",
                },
                false
            );
            return;
        }

        const validate = !this._isSuprPassSku && !this.rechargeThroughCart;
        this._cartUpdating = true;
        setTimeout(
            () =>
                this.handleUpdateCart.emit({
                    cartItem: cartItem,
                    validateCart: validate,
                }),
            0
        );
    }

    private checkoutCart() {
        const canCheckout = this.utilService.getNestedValue(
            this.cartMeta,
            "wallet_info.is_sufficient",
            false
        );

        if (canCheckout) {
            this.sendAnalyticsEvents();
            setTimeout(() => this.handleCheckoutCart.emit(), 0);
        } else {
            this.resetUpdatingFlags();
        }
    }

    private handleAutoCheckout() {
        const isAutoCheckoutSet = this.cartService.getAutoCheckoutFlag();
        if (isAutoCheckoutSet) {
            this.cartService.clearAutoCheckoutFlag();
            this.placeOrder();
        }
    }

    private handlePlaceOrderSuccess() {
        this.sendRatingNudge();
        this.fetchSubscriptionSchedules();
        this.toastService.present(TOAST_MESSAGES.RECHARGE.SUCCESS);
        this.routerService.goBack();
    }

    private fetchSubscriptionSchedules() {
        /* Fetch subscription schedule if user came from subscription details page */
        const previousUrl = this.routerService.getPreviousUrl();

        if (
            previousUrl.indexOf(PAGE_ROUTES.SUBSCRIPTION_DETAILS_V2.BASE) >
                -1 ||
            previousUrl.indexOf(PAGE_ROUTES.SUBSCRIPTION_SUMMARY.BASE) > -1
        ) {
            this.fetchSubscriptionSchedule.emit({
                subscriptionId: this.subscription.id,
            });
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (!this.canHandleChange(change)) {
            return;
        }

        if (this.isValidatingCart(change)) {
            this._validatingCart = true;
        } else if (this.isValidationDone(change)) {
            if (this._isCheckingOut) {
                if (this.canProceedToPlaceOrder(change)) {
                    this.checkoutCart();
                } else {
                    this._isCheckingOut = false;
                    this.resetUpdatingFlags();
                }
            } else {
                this.resetUpdatingFlags();
            }
        } else if (this.isPlacingOrder(change)) {
            this._placingOrder = true;
        } else if (this.isCheckoutDone(change)) {
            if (!this.cartError) {
                this.handlePlaceOrderSuccess();
            } else {
                this.resetUpdatingFlags();
            }
        }
    }

    private handleSubscriptionChange(change: SimpleChange) {
        if (!this.canHandleSubscriptionChange(change)) {
            return;
        }

        this._dataLoading = false;
        this.setSuprPassInfo();
        this._validateCart();
    }

    private setSuprPassInfo() {
        const skuData = this.utilService.getNestedValue(
            this.subscription,
            "sku_data"
        );
        if (!skuData) {
            this._isSuprPassSku = false;
            return;
        }

        this._isSuprPassSku = this.skuService.isSuprPassSku(skuData);

        if (!this.utilService.isEmpty(this.suprPassInfo)) {
            const { plans = [] } = this.suprPassInfo;

            const suprPassObj = this.cartItems.find((obj) => {
                return obj && obj.hasOwnProperty("plan_id");
            });

            if (suprPassObj && typeof suprPassObj.plan_id === "number") {
                this.selectedPlan = plans.find(
                    (plan: SuprPassPlan) => plan.id === suprPassObj.plan_id
                );
                this.setAutoDebitPlan();
                return;
            }

            this.setAutoDebitPlan();
            this.selectedPlan = plans.find(
                (plan: SuprPassPlan) => plan.isRecommended
            );
        }

        if (
            this._isSuprPassSku &&
            this.utilService.isEmpty(this.suprPassInfo)
        ) {
            this._dataLoading = true;
            this.handleFetchSuprPassInfo.emit();
        }
    }

    private handleSuprPassInfoChange(change: SimpleChange) {
        if (!this.canHandleSubscriptionChange(change)) {
            return;
        }

        this._dataLoading = false;
        this.setSuprPassInfo();
    }

    private canProceedToPlaceOrder(change: SimpleChange) {
        if (this.cartError || this.isValidationFailed(change)) {
            return false;
        } else {
            return true;
        }
    }

    private resetUpdatingFlags() {
        this._validatingCart = false;
        this._cartUpdating = false;
        this._dataLoading = false;
        this._placingOrder = false;
        this._isCheckingOut = false;
    }

    private isRechargePageUrl(url: string): boolean {
        return (
            url &&
            url.indexOf("recharge") > -1 &&
            url.indexOf("subscription") > -1
        );
    }

    private isWalletPageUrl(url: string): boolean {
        return url && url.indexOf("wallet") > -1;
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private canHandleSubscriptionChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private isValidatingCart(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.UPDATING_CART;
    }

    private isValidationDone(change: SimpleChange): boolean {
        return (
            change.previousValue === CartCurrentState.UPDATING_CART &&
            (change.currentValue === CartCurrentState.NO_ACTION ||
                change.currentValue === CartCurrentState.VALIDATION_FAILED ||
                change.currentValue === CartCurrentState.CHECKOUT_FAILED)
        );
    }

    private isValidationFailed(change: SimpleChange): boolean {
        return (
            change.currentValue === CartCurrentState.VALIDATION_FAILED ||
            change.currentValue === CartCurrentState.CHECKOUT_FAILED
        );
    }

    private isPlacingOrder(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.PLACING_ORDER;
    }

    private isCheckoutDone(change: SimpleChange): boolean {
        return (
            change.previousValue === CartCurrentState.PLACING_ORDER &&
            change.currentValue === CartCurrentState.NO_ACTION
        );
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackInitiateRecharge(
            this.subscription,
            this.cartMeta,
            this._rechargeQty
        );
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.RECHARGE_SUBSCRIPTION
        );
    }
}
