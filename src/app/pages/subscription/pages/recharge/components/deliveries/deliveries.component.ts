import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-recharge-deliveries",
    template: `
        <div class="deliveries">
            <supr-text type="subheading">
                How many deliveries do you want?
            </supr-text>
            <div class="divider16"></div>
            <supr-recharge-qty-select
                [selectedQty]="selectedQty"
                (handleOptionChange)="handleQtyChange.emit($event)"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .DELIVERY_FREQUENCY}"
            ></supr-recharge-qty-select>
        </div>
    `,
    styleUrls: ["../../styles/recharge.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeDeliveriesComponent {
    @Input() selectedQty: number;
    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();
}
