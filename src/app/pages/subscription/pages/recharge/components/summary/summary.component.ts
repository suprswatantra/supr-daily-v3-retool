import {
    Component,
    OnInit,
    Input,
    SimpleChanges,
    OnChanges,
    SimpleChange,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CART_DISCOUNT_TYPES, ANALYTICS_OBJECT_NAMES } from "@constants";
import { CartMeta, Sku, DeliveryFeeMessageInfo } from "@models";
import { QuantityService } from "@services/util/quantity.service";

import { TEXTS, SUPR_PASS_TEXTS } from "../../constants";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-subscription-recharge-summary",
    templateUrl: "./summary.component.html",
    styleUrls: ["../../styles/summary.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeSummaryComponent implements OnInit, OnChanges {
    @Input() cartMeta: CartMeta;
    @Input() sku: Sku;
    @Input() rechargeQty: number;
    @Input() unitQty: number;
    @Input() isSuprPassSku: boolean;
    @Input() isEligibleForSuprPass: boolean;

    _itemPrice: number;
    _discountAmount: number;
    _couponCode: string;
    _totalAmount: number;
    _finalAmount: number;
    _balance: number;
    _amountToAdd: number;
    _suprCreditsUsed: number;
    _suprCreditsGainedAmount: number;
    _deliveryFee: number;
    _preDeliveryFee: number;
    _isSuprPassApplied: boolean;
    discountApplied: boolean;
    instantDiscount: boolean;
    displayQty: string;
    delFeeInfo: DeliveryFeeMessageInfo;
    showDeliveryFeeInfoModal: boolean;
    appliedSuprPassText: string;

    saObjectNames = ANALYTICS_OBJECT_NAMES;

    TEXTS = TEXTS;

    constructor(
        private qtyService: QuantityService,
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCartMetaChange(changes["cartMeta"]);
        this.handleUnitQtyChange(changes["unitQty"]);
    }

    showDeliveryFeeInfo() {
        this.showDeliveryFeeInfoModal = true;
    }

    closeDeliveryFeeInfo() {
        this.showDeliveryFeeInfoModal = false;
    }

    private handleCartMetaChange(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        ) {
            this.initData();
        }
    }

    private handleUnitQtyChange(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        ) {
            this.setDisplayQuantity();
        }
    }

    private initData() {
        if (!this.cartMeta) {
            return;
        }

        this.setItemPrice();
        this.setTotalAmount();
        this.setDiscountAmount();
        this.setSuprCreditsUsed();
        this.setSuprCreditsGainedAmount();
        this.setDeliveryFee();
        this.setDelFeeInfo();
        this.setAmountToAdd();
        this.setDisplayQuantity();
        this.setSuprPassInfo();
    }

    private setItemPrice() {
        this._itemPrice = this.utilService.getNestedValue(
            this.sku,
            "unit_price"
        );
    }

    private setTotalAmount() {
        const { payment_data } = this.cartMeta;
        this._totalAmount = payment_data.pre_discount;

        const scUsed = this.utilService.getNestedValue(
            payment_data,
            "supr_credits_used",
            0
        );

        this._finalAmount = this.getAmountToPay(scUsed);
    }

    private getAmountToPay(scUsed): number {
        const {
            real_amount = 0,
            post_discount,
            supr_credits_used,
            delivery_fee = 0,
        } = this.cartMeta.payment_data;

        if (real_amount) {
            return real_amount;
        }

        if (scUsed) {
            return post_discount + delivery_fee - supr_credits_used;
        }

        return post_discount + delivery_fee;
    }

    private setDiscountAmount() {
        this.discountApplied = false;

        const { discount_info } = this.cartMeta;
        if (discount_info) {
            const {
                is_applied,
                discount_amount,
                coupon_code,
                discount_type,
            } = discount_info;

            if (is_applied) {
                this.discountApplied = true;
                this.instantDiscount =
                    discount_type === CART_DISCOUNT_TYPES.DISCOUNT;

                this._discountAmount = discount_amount * -1;
                this._couponCode = coupon_code;
            } else {
                this._discountAmount = 0;
                this._couponCode = null;
            }
        }
    }

    private setSuprCreditsGainedAmount() {
        this._suprCreditsGainedAmount = this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.supr_credits_awarded",
            0
        );
    }

    private setDeliveryFee() {
        this._deliveryFee = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.delivery_fee",
            0
        );

        this._preDeliveryFee = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.pre_delivery_fee",
            0
        );
    }

    private setSuprPassInfo() {
        this._isSuprPassApplied = this.utilService.getNestedValue(
            this.cartMeta,
            "supr_pass_applied",
            false
        );

        const suprPassTexts = this.settingsService.getSettingsValue(
            "suprPassTexts",
            SUPR_PASS_TEXTS
        );

        this.appliedSuprPassText = suprPassTexts.SUPR_PASS_APPLIED;
    }

    private setSuprCreditsUsed() {
        this._suprCreditsUsed = 0;

        const { payment_data } = this.cartMeta;
        const scUsed = this.utilService.getNestedValue(
            payment_data,
            "supr_credits_used",
            0
        );

        if (scUsed) {
            this._suprCreditsUsed = payment_data.supr_credits_used * -1;
        }
    }

    private setAmountToAdd() {
        const {
            wallet_info: { balance, extra_required },
        } = this.cartMeta;

        this._balance = balance;
        this._amountToAdd = Math.ceil(extra_required);
    }

    private setDisplayQuantity() {
        this.displayQty = this.qtyService.toText(this.sku, this.unitQty);
    }

    private setDelFeeInfo() {
        this.delFeeInfo = this.utilService.getNestedValue(
            this.cartMeta,
            "delivery_fee_message_info"
        );
    }
}
