import { SubscriptionRechargeHeaderComponent } from "./header/header.component";
import { SubscriptionRechargeDeliveriesComponent } from "./deliveries/deliveries.component";
import { SuprPassRechargePlansComponent } from "./supr-pass-plans/supr-pass-plans.component";
import { SubscriptionRechargeSummaryComponent } from "./summary/summary.component";

import { SubscriptionRechargeOverlayComponent } from "./loader/overlay.component";
import { SubscriptionRechargeSkeletonComponent } from "./loader/skeleton.component";

import { SubscriptionRechargeFooterComponent } from "./footer/footer.component";
import { SubscriptionRechargeFooterErrorComponent } from "./footer/error.component";
import { SubscriptionRechargeFooterV2Component } from "./footer/recharge-footer.component";

export const subscriptionRechargeComponents = [
    SubscriptionRechargeHeaderComponent,
    SubscriptionRechargeDeliveriesComponent,
    SuprPassRechargePlansComponent,
    SubscriptionRechargeSummaryComponent,

    SubscriptionRechargeOverlayComponent,
    SubscriptionRechargeSkeletonComponent,

    SubscriptionRechargeFooterComponent,
    SubscriptionRechargeFooterErrorComponent,
    SubscriptionRechargeFooterV2Component,
];
