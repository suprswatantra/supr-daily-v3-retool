import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-recharge-loader-overlay",
    template: `
        <supr-backdrop></supr-backdrop>
    `,
    styleUrls: ["../../styles/loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeOverlayComponent {}
