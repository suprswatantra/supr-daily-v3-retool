import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-subscription-recharge-footer-v2",
    template: `
        <supr-sticky-wrapper>
            <div class="footerWrapper">
                <div class="suprRow">
                    <supr-button
                        (handleClick)="handleFooterClick.emit()"
                        [saObjectName]="saObjectName"
                    >
                        <supr-text type="body">
                            ${TEXTS.PROCEED_TO_CART}
                        </supr-text>
                    </supr-button>
                </div>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["../../styles/recharge-footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeFooterV2Component {
    @Input() saObjectName: string;
    @Output() handleFooterClick: EventEmitter<void> = new EventEmitter();
}
