import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { CartMeta } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";
import { UtilService } from "@services/util/util.service";
import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-subscription-recharge-footer",
    template: `
        <ng-container *ngIf="hasError; else pay">
            <supr-subscription-recharge-footer-error></supr-subscription-recharge-footer-error>
        </ng-container>

        <ng-template #pay>
            <supr-cart-footer-dep
                [amountToAdd]="amountToAdd"
                [amountToPay]="amountToPay"
                [suprCreditsUsed]="suprCreditsUsed"
                [userCheckoutInfo]="userCheckoutInfo"
                [cartMeta]="cartMeta"
                [fromRechargeSubscriptionPage]="isRechargeSubscriptionPage"
                (handlePay)="handlePay.emit()"
            ></supr-cart-footer-dep>
        </ng-template>
    `,
    styleUrls: ["../../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeFooterComponent implements OnInit, OnChanges {
    @Input() cartMeta: CartMeta;
    @Input() cartState: CartCurrentState;
    @Input() userCheckoutInfo: CheckoutInfo;

    @Output() handlePay: EventEmitter<void> = new EventEmitter();
    hasError = false;
    isRechargeSubscriptionPage = true;
    amountToPay: number;
    amountToAdd: number;
    suprCreditsUsed: number;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCartMetaChange(changes["cartMeta"]);
        this.handleCartStateChange(changes["cartState"]);
    }

    private handleCartMetaChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setTotals();
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setError();
        }
    }

    private initData() {
        this.setError();
        this.setTotals();
    }

    private setError() {
        this.hasError =
            this.cartState === CartCurrentState.VALIDATION_FAILED ||
            this.cartState === CartCurrentState.CHECKOUT_FAILED;
    }

    private setTotals() {
        if (this.utilService.isEmpty(this.cartMeta)) {
            return;
        }

        this.setAmountToPay();
        this.setAmountToAdd();
        this.setSuprCreditsUsed();
    }

    private setAmountToPay() {
        const scUsed = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.supr_credits_used",
            0
        );

        this.amountToPay = this.getAmountToPay(scUsed);
    }

    private getAmountToPay(scUsed): number {
        const {
            real_amount = 0,
            post_discount,
            supr_credits_used,
            delivery_fee = 0,
        } = this.cartMeta.payment_data;

        if (real_amount) {
            return real_amount;
        }

        if (scUsed) {
            return post_discount + delivery_fee - supr_credits_used;
        }

        return post_discount + delivery_fee;
    }

    private setAmountToAdd() {
        this.amountToAdd = Math.ceil(this.cartMeta.wallet_info.extra_required);
    }

    private setSuprCreditsUsed() {
        this.suprCreditsUsed = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.supr_credits_used",
            0
        );
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
