import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

import { Sku, Subscription } from "@models";
import { SkuService } from "@services/shared/sku.service";

@Component({
    selector: "supr-subscription-recharge-header",
    template: `
        <div class="header">
            <div class="headerContent">
                <supr-subscription-product-tile
                    [subscription]="subscription"
                    showPrice="true"
                ></supr-subscription-product-tile>
                <ng-container *ngIf="!_isSuprPassSku">
                    <div class="divider12"></div>
                    <div
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .EDIT_SUBSCRIPTION}"
                        class="suprRow edit"
                        (click)="openEditModal()"
                    >
                        <supr-text type="subtext10">
                            Edit subscription
                        </supr-text>
                        <div class="spacer4"></div>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </ng-container>
            </div>
        </div>

        <ng-container *ngIf="_showEditModal">
            <supr-modal
                modalName="${MODAL_NAMES.EDIT_SUBSCRIPTION}"
                (handleClose)="closeEditModal()"
            >
                <supr-edit-subscription
                    [subscription]="subscription"
                    [sku]="sku"
                ></supr-edit-subscription>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeHeaderComponent implements OnInit {
    @Input() subscription: Subscription;
    @Input() sku: Sku;

    _showEditModal = false;
    _isSuprPassSku = false;

    constructor(private skuService: SkuService) {}

    ngOnInit() {
        this._isSuprPassSku = this.skuService.isSuprPassSku(this.sku);
    }

    openEditModal() {
        this._showEditModal = true;
    }

    closeEditModal() {
        this._showEditModal = false;
    }
}
