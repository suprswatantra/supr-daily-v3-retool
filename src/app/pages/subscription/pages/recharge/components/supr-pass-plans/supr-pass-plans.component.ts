import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import {
    SuprPassInfo,
    SuprPassPlan,
    CartItem,
    BenefitsBannerContent,
} from "@shared/models";
import { SUPR_PASS_TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-pass-recharge-plans",
    template: `
        <div class="suprPassPlansWrapper">
            <ng-container *ngIf="suprPassInfo?.benefits && showBenefits">
                <supr-benefits-banner
                    [benefitsBanner]="suprPassInfo?.benefits"
                    [hideDefaults]="true"
                    [hideCta]="true"
                ></supr-benefits-banner>
                <div class="divider20"></div>
            </ng-container>

            <supr-text type="subheading">
                {{ suprPassTexts.SUPR_PASS_PLANS }}
            </supr-text>
            <supr-pass-plans
                [suprPassPlans]="suprPassInfo?.plans"
                [selectedPlan]="selectedPlan"
                [saObjNamePlanClick]="saObjNamePlanClick"
                (handlePlanSelect)="onPlanSelect($event)"
            ></supr-pass-plans>
        </div>
    `,
    styleUrls: ["../../styles/recharge.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassRechargePlansComponent implements OnInit {
    @Input() suprPassInfo: SuprPassInfo;
    @Input() selectedPlan: SuprPassPlan;
    @Input() cartItems: CartItem[];

    @Output() handlePlanSelect: EventEmitter<SuprPassPlan> = new EventEmitter();

    planEndDate: string;
    suprPassTexts = SUPR_PASS_TEXTS;
    saObjNamePlanClick =
        ANALYTICS_OBJECT_NAMES.CLICK.SUPR_PASS_PLAN_CLICK_RECHARGE_SUBSCRIPTION;
    showBenefits = false;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    onPlanSelect(plan: SuprPassPlan) {
        this.handlePlanSelect.emit(plan);
    }

    private initialize() {
        const benefitContentArray: BenefitsBannerContent[] = this.utilService.getNestedValue(
            this.suprPassInfo,
            "benefits.content",
            null
        );

        if (!benefitContentArray) {
            this.showBenefits = false;
            return;
        }

        this.showBenefits = benefitContentArray.some(
            (benefit) => !benefit.isDefault
        );
    }
}
