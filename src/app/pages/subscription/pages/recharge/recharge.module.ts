import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { subscriptionRechargeComponents } from "./components";
import { subscriptionRechargeContainers } from "./containers";

import { SubscriptionRechargePageLayoutComponent } from "./layouts/recharge.layout";
import { SubscriptionRechargePageContainer } from "./containers/recharge.container";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionRechargePageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        ...subscriptionRechargeComponents,
        ...subscriptionRechargeContainers,
        SubscriptionRechargePageLayoutComponent,
    ],
})
export class SubscriptionRechargePageModule {}
