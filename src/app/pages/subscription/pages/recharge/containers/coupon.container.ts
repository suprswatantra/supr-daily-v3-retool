import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";

@Component({
    selector: "supr-subscription-recharge-coupon-container",
    template: `
        <supr-coupon
            [couponCode]="couponCode$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            (applyCoupon)="updateCoupon($event)"
            (removeCoupon)="updateCoupon()"
        ></supr-coupon>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeCouponContainer implements OnInit {
    couponCode$: Observable<CouponCode>;
    cartCurrentState$: Observable<CartCurrentState>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.couponCode$ = this.adapter.cartCouponCode$;
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
    }

    updateCoupon(couponCode?: string) {
        this.adapter.updateCouponInCart(couponCode);
    }
}
