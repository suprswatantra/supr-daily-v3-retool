import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { Sku, Subscription } from "@models";

@Component({
    selector: "supr-subscription-recharge-header-container",
    template: `
        <supr-subscription-recharge-header
            [subscription]="subscription"
            [sku]="sku"
        ></supr-subscription-recharge-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeHeaderContainer implements OnInit {
    @Input() subscription: Subscription;

    sku: Sku;

    ngOnInit() {
        if (this.subscription && this.subscription.sku_data) {
            this.sku = this.subscription.sku_data;
        }
    }
}
