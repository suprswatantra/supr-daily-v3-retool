import { SubscriptionRechargeHeaderContainer } from "./header.container";
import { SubscriptionRechargePageContainer } from "./recharge.container";
import { SubscriptionRechargeCouponContainer } from "./coupon.container";
import { SubscriptionRechargeSummaryContainer } from "./summary.container";
import { SubscriptionRechargeFooterContainer } from "./footer.container";

export const subscriptionRechargeContainers = [
    SubscriptionRechargeHeaderContainer,
    SubscriptionRechargePageContainer,
    SubscriptionRechargeCouponContainer,
    SubscriptionRechargeSummaryContainer,
    SubscriptionRechargeFooterContainer,
];
