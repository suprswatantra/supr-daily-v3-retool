import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Observable } from "rxjs";

import { CartMeta } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";
import { CartCurrentState } from "@store/cart/cart.state";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";

@Component({
    selector: "supr-subscription-recharge-footer-container",
    template: `
        <supr-subscription-recharge-footer
            [cartState]="cartState$ | async"
            [cartMeta]="cartMeta$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            (handlePay)="handlePay.emit()"
        ></supr-subscription-recharge-footer>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeFooterContainer implements OnInit {
    @Output() handlePay: EventEmitter<void> = new EventEmitter();

    cartState$: Observable<CartCurrentState>;
    cartMeta$: Observable<CartMeta>;
    userCheckoutInfo$: Observable<CheckoutInfo>;

    constructor(private adapter: Adapter, private userAdapter: UserAdapter) {}

    ngOnInit() {
        this.cartState$ = this.adapter.cartCurrentState$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.userCheckoutInfo$ = this.userAdapter.userCheckoutInfo$;
    }
}
