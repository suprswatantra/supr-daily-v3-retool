import {
    Input,
    Component,
    OnInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter } from "@shared/adapters/cart.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import { Sku, CartMeta } from "@models";

@Component({
    selector: "supr-subscription-recharge-summary-container",
    template: `
        <supr-subscription-recharge-summary
            [sku]="sku$ | async"
            [cartMeta]="cartMeta$ | async"
            [rechargeQty]="rechargeQty"
            [unitQty]="unitQty"
            [isEligibleForSuprPass]="isEligibleForSuprPass$ | async"
            [isSuprPassSku]="isSuprPassSku"
        ></supr-subscription-recharge-summary>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargeSummaryContainer implements OnInit {
    @Input() skuId: number;
    @Input() isSuprPassSku: boolean;
    @Input() rechargeQty: number;
    @Input() unitQty: number;

    cartMeta$: Observable<CartMeta>;
    sku$: Observable<Sku>;
    isEligibleForSuprPass$: Observable<boolean>;

    constructor(
        private adapter: SubscriptionAdapter,
        private cartAdapter: CartAdapter
    ) {}

    ngOnInit() {
        this.sku$ = this.adapter.getSkuDetailsById(this.skuId);
        this.cartMeta$ = this.cartAdapter.cartMeta$;
        this.isEligibleForSuprPass$ = this.cartAdapter.isSuprPassEligible$;
    }
}
