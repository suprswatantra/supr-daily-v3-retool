import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import {
    Subscription,
    CartItem,
    CartMeta,
    SuprPassInfo,
    AutoDebitPaymentMode,
} from "@models";

import { CartCurrentState } from "@store/cart/cart.state";

import { SuprApi, FetchScheduleActionPayload } from "@types";

import { CartAdapter } from "@shared/adapters/cart.adapter";
import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { SubscriptionPageAdapter as Adapter } from "@pages/subscription/services/subscription.adapter";

@Component({
    selector: "supr-subscription-details-container",
    template: `
        <supr-subscription-recharge-layout
            [subscription]="subscription$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [cartError]="cartError$ | async"
            [cartMeta]="cartMeta$ | async"
            [suprPassInfo]="suprPassInfo$ | async"
            [cartItems]="cartItems$ | async"
            [autoDebitPaymentModes]="autoDebitPaymentModes$ | async"
            (handleUpdateCart)="updateCart($event)"
            (handleValidateCart)="validateCart($event)"
            (handleCheckoutCart)="checkoutCart()"
            (handleFetchSubscriptions)="fetchSubscriptionList()"
            (handleFetchSuprPassInfo)="fetchSuprPassInfo()"
            (fetchSubscriptionSchedule)="fetchSubscriptionSchedule($event)"
            (handleFetchAutoDebitPaymentModes)="fetchAutoDebitPaymentModes()"
        ></supr-subscription-recharge-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionRechargePageContainer implements OnInit {
    subscription$: Observable<Subscription>;
    cartCurrentState$: Observable<CartCurrentState>;
    cartMeta$: Observable<CartMeta>;
    cartError$: Observable<any>;
    cartItems$: Observable<CartItem[]>;
    suprPassInfo$: Observable<SuprPassInfo>;
    autoDebitPaymentModes$: Observable<AutoDebitPaymentMode[]>;

    constructor(
        private adapter: Adapter,
        private cartAdapter: CartAdapter,
        private scheduleAdapter: ScheduleAdapter
    ) {}

    ngOnInit() {
        this.subscription$ = this.adapter.subscription$;
        this.cartCurrentState$ = this.cartAdapter.cartCurrentState$;
        this.cartMeta$ = this.cartAdapter.cartMeta$;
        this.cartError$ = this.cartAdapter.cartError$;
        this.cartItems$ = this.cartAdapter.cartItems$;
        this.suprPassInfo$ = this.adapter.suprPassInfo$;
        this.autoDebitPaymentModes$ = this.adapter.autoDebitPaymentModes$;
    }

    validateCart(cartBody: SuprApi.CartBody) {
        this.cartAdapter.validateCart(cartBody);
    }

    fetchSubscriptionList() {
        this.adapter.fetchSubscriptions();
    }

    fetchSuprPassInfo() {
        this.adapter.fetchSuprPassInfo();
    }

    updateCart(obj: { cartItem: CartItem; validateCart: boolean }) {
        this.cartAdapter.updateItemInCart(obj.cartItem, obj.validateCart);
    }

    checkoutCart() {
        this.cartAdapter.checkoutCart();
    }

    fetchSubscriptionSchedule(data: FetchScheduleActionPayload) {
        this.scheduleAdapter.getSchedule(data);
    }

    fetchAutoDebitPaymentModes() {
        this.adapter.fetchAutoDebitPaymentModes();
    }
}
