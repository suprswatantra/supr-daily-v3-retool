import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { SubscriptionSchedulePageContainer } from "./containers/schedule.container";
import { SubscriptionSchedulePageLayoutComponent } from "./layouts/schedule.layout";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionSchedulePageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        SubscriptionSchedulePageContainer,
        SubscriptionSchedulePageLayoutComponent,
    ],
})
export class SubscriptionSchedulePageModule {}
