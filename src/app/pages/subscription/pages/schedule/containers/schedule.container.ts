import { OnDestroy, Component } from "@angular/core";

import { Observable, Subscription } from "rxjs";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { UtilService } from "@services/util/util.service";

import { SubscriptionPageAdapter } from "@pages/subscription/services/subscription.adapter";

import { ScheduleCurrentState } from "@store/schedule/schedule.state";
import {
    Subscription as SubscriptionModel,
    Schedule,
    Vacation,
    Sku,
} from "@models";
import { FetchScheduleActionPayload, ScheduleDict } from "@types";

@Component({
    selector: "supr-subscription-schedule-container",
    template: `
        <supr-subscription-schedule-layout
            [schedules]="schedules$ | async"
            [subscription]="subscription$ | async"
            [refreshSchedule]="refreshSchedule$ | async"
            [scheduleCurrentState]="scheduleCurrentState$ | async"
            [vacation]="vacation$ | async"
            [sku]="sku$ | async"
            [scheduleDict]="scheduleDict$ | async"
            (fetchSchedule)="fetchSchedule($event)"
        >
        </supr-subscription-schedule-layout>
    `,
    styleUrls: ["../styles/schedule.page.scss"],
})
export class SubscriptionSchedulePageContainer implements OnDestroy {
    schedules$: Observable<Schedule[]>;
    subscriptionObservable: Subscription;
    refreshSchedule$: Observable<boolean>;
    subscription$: Observable<SubscriptionModel>;
    scheduleCurrentState$: Observable<ScheduleCurrentState>;
    vacation$: Observable<Vacation>;
    scheduleDict$: Observable<ScheduleDict>;
    sku$: Observable<Sku>;

    constructor(
        private utilService: UtilService,
        private scheduleAdapter: ScheduleAdapter,
        private subscriptionPageAdapter: SubscriptionPageAdapter
    ) {
        this.refreshSchedule$ = this.scheduleAdapter.refreshSchedule$;
        this.subscription$ = this.subscriptionPageAdapter.subscription$;
        this.schedules$ = this.subscriptionPageAdapter.getSubscriptionSchedule();
        this.scheduleCurrentState$ = this.scheduleAdapter.scheduleCurrentState$;
        this.vacation$ = this.scheduleAdapter.vacation$;
        this.scheduleDict$ = this.scheduleAdapter.scheduleDict$;

        this.subscribeToSubscriptionData();
    }

    ngOnDestroy() {
        if (this.subscriptionObservable) {
            this.subscriptionObservable.unsubscribe();
            this.subscriptionObservable = null;
        }
    }

    subscribeToSubscriptionData() {
        this.subscriptionObservable = this.subscription$.subscribe(data => {
            const skuId = this.utilService.getNestedValue(data, "sku_id");
            if (skuId) {
                this.sku$ = this.subscriptionPageAdapter.getSkuById(skuId);
            }
        });
    }

    fetchSchedule(data: FetchScheduleActionPayload) {
        this.scheduleAdapter.getSchedule(data);
    }
}
