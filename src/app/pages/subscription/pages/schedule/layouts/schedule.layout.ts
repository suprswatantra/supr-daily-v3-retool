import {
    Input,
    Output,
    OnInit,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription, Schedule, Vacation, Sku } from "@models";
import { ScheduleCurrentState } from "@store/schedule/schedule.state";

import { TEXTS } from "../constants/schedule.constants";
import { ANALYTICS_OBJECT_NAMES } from "../constants/analytics.constants";

import { ScheduleDict } from "@types";

@Component({
    selector: "supr-subscription-schedule-layout",
    template: `
        <div class="container">
            <supr-page-header [saObjectValueBack]="subscription?.id">
                <div class="suprRow spaceBetween">
                    <supr-text type="subheading">
                        ${TEXTS.DELIVERY_SCHEDULE_TITLE}
                    </supr-text>
                    <supr-chip
                        (handleClick)="openSubBalanceModal()"
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .SCHEDULE_DELIVERY}"
                        class="headerChip"
                    >
                        <supr-text type="paragraph"
                            >${TEXTS.SCHEDULE_DELIVERY}</supr-text
                        >
                    </supr-chip>
                </div>
            </supr-page-header>
            <div class="suprScrollContent" *ngIf="showShimmers; else content">
                <supr-schedule-skeleton></supr-schedule-skeleton>
            </div>
            <ng-template #content>
                <div class="suprScrollContent">
                    <div *ngIf="filteredSchedules.length > 0">
                        <ng-container
                            *ngFor="
                                let schedule of filteredSchedules;
                                trackBy: trackByFn
                            "
                        >
                            <supr-schedule-item
                                [locked]="true"
                                [schedule]="schedule"
                                [date]="schedule?.date"
                                [supportAction]="false"
                                [subscriptionView]="true"
                            ></supr-schedule-item>
                        </ng-container>
                    </div>
                </div>
            </ng-template>
            <supr-subscription-balance-modal
                [activeSubscription]="subscription"
                [activeSku]="sku"
                [vacation]="vacation"
                [scheduleData]="scheduleDict"
                [showSubBalanceModal]="showSubBalanceModal"
                (handleShowModalChange)="onShowModalChange($event)"
                (handleScheduleUpdate)="handleScheduleUpdate()"
            ></supr-subscription-balance-modal>
        </div>
    `,
    styleUrls: ["../styles/schedule.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionSchedulePageLayoutComponent
    implements OnInit, OnChanges {
    showShimmers = true;
    filteredSchedules = [];

    @Input() refreshSchedule: boolean;
    @Input() subscription: Subscription;
    @Input() set scheduleCurrentState(state: ScheduleCurrentState) {
        this.setLoadingState(state);
    }
    @Input() set schedules(schedules: Schedule[]) {
        this.filteredSchedules = this.filterEmptySchedules(schedules);
    }
    @Input() vacation: Vacation;
    @Input() scheduleDict: ScheduleDict;
    @Input() sku: Sku;

    @Output() fetchSchedule: EventEmitter<{
        subscriptionId: number;
    }> = new EventEmitter();

    showSubBalanceModal = false;

    ngOnInit() {
        if (this.subscription) {
            this.fetchSchedule.emit({ subscriptionId: this.subscription.id });
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const refreshScheduleChange = changes["refreshSchedule"];
        if (
            this.subscription &&
            refreshScheduleChange &&
            !refreshScheduleChange.firstChange &&
            refreshScheduleChange.currentValue
        ) {
            setTimeout(() => {
                this.fetchSchedule.emit({
                    subscriptionId: this.subscription.id,
                });
            }, 0);
        }
    }

    trackByFn(schedule: Schedule): string {
        return schedule.date;
    }

    openSubBalanceModal() {
        this.showSubBalanceModal = true;
    }

    onShowModalChange(showModal: boolean) {
        this.showSubBalanceModal = showModal;
    }

    handleScheduleUpdate() {
        // fetch schedule again to show updated info
        this.fetchSchedule.emit({ subscriptionId: this.subscription.id });
    }

    private filterEmptySchedules(allSchedules: Schedule[]): Schedule[] {
        return allSchedules.filter((schedule) => {
            const hasAddons =
                schedule.addons &&
                schedule.addons.find((addon) => addon.quantity > 0);

            const hasSubscriptions =
                schedule.subscriptions &&
                schedule.subscriptions.find((sub) => sub.quantity > 0);

            return hasAddons || hasSubscriptions;
        });
    }

    private setLoadingState(state: ScheduleCurrentState) {
        this.showShimmers = state === ScheduleCurrentState.FETCHING_SCHEDULE;
    }
}
