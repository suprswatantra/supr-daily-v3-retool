import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { SubscriptionCreatePageContainer } from "./containers/create.container";
import { SubscriptionCreatePageLayoutComponent } from "./layouts/create.layout";
import { SubscriptionComponents } from "./components/";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionCreatePageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        SubscriptionCreatePageContainer,
        SubscriptionCreatePageLayoutComponent,
        ...SubscriptionComponents,
    ],
})
export class SubscriptionCreatePageModule {}
