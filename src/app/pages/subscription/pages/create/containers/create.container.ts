import { OnInit, Component } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { CartAdapter } from "@shared/adapters/cart.adapter";
import { PauseAdapter } from "@shared/adapters/pause.adapter";
import { SearchAdapter } from "@shared/adapters/search.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import { Sku, Vacation, CartItem } from "@models";

@Component({
    selector: "supr-subscription-create-container",
    template: `
        <supr-subscription-create-layout
            [sku]="sku$ | async"
            [item]="cartItem$ | async"
            [vacation]="vacation$ | async"
            [fromCatalog]="fromCatalog"
            (updateItemInCart)="updateItemInCart($event)"
        ></supr-subscription-create-layout>
    `,
    styleUrls: ["../styles/create.page.scss"],
})
export class SubscriptionCreatePageContainer implements OnInit {
    sku$: Observable<Sku>;
    cartItem$: Observable<CartItem>;
    vacation$: Observable<Vacation>;
    fromCatalog: boolean;

    constructor(
        private skuAdapter: SkuAdapter,
        private cartAdapter: CartAdapter,
        private searchAdapter: SearchAdapter,
        private vacationAdapter: PauseAdapter,
        private subscriptionAdapter: SubscriptionAdapter
    ) {}

    ngOnInit() {
        this.vacation$ = this.vacationAdapter.vacation$;

        this.sku$ = this.subscriptionAdapter.urlParams$.pipe(
            switchMap((params: Params) => {
                const skuId = params["skuId"];
                this.fromCatalog = params["from"] === "catalog";
                this.cartItem$ = this.cartAdapter.getCartItemById(skuId);

                return this.skuAdapter.getSkuDetails(skuId);
            })
        );
    }

    updateItemInCart(cartItem: CartItem) {
        this.cartAdapter.updateItemInCart(cartItem, true);
        this.sendSearchSubscribeInstrumentation(cartItem);
    }

    private sendSearchSubscribeInstrumentation(cartItem: CartItem) {
        if (cartItem && cartItem.sku_id) {
            this.searchAdapter.checkSearchSubInstrumentation(cartItem.sku_id);
        }
    }
}
