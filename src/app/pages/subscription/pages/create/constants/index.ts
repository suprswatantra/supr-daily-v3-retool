export const TEXTS = {
    DESCRIPTION: {
        HEADER_1: "Get your groceries on",
        HEADER_2: "auto-pilot with subscriptions",
        DESC_HEADING_1: "Unlimited free deliveries",
        DESC_CONTENT_1:
            "No delivery fee for any items, no minimum order required.",
        DESC_HEADING_2: "Cancel anytime",
        DESC_CONTENT_2: "Cancel anytime and get your money back.",
        DESC_HEADING_3: "Set vacation days easily",
        DESC_CONTENT_3: "Going on a vacation? Set flexible vacation days.",
    },
    SUBSCRIBE_SUPR_PASS: "Subscribe",
};
