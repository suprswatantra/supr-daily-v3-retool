export const LATE_START_TEXTS = {
    DESCRIPTION:
        "Due to surge in demand we have set the earliest available start date for this item.",
    NEED_SOMETHING_EARLIER: "Need something earlier?",
    VIEW_AVAILABLE_PRODUCTS: "view available products",
};
