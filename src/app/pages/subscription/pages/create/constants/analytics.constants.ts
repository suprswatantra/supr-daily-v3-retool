export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        REPEAT: "repeat",
        DELIVERIES: "top-up",
        START_DATE: "start-date",
        DELIVER_ONCE: "deliver-once",
        CONFIRM_SUBSCRIPTION: "confirm-subscription",
        DELIVERY_QTY: "delivery-quantity",
        CONFIRM_DELIVERY_QTY: "confirm-deliveries",
        CONFIRM_START_DATE: "confirm-start-date",
        SUBSCRIBE_SUPR_PASS_CLICK:
            "subscription-start-date-calendar-subscribe-supr-pass-click",
    },
    IMPRESSION: {
        TPONE_BANNER_SUBSCRIPTION: "tplus1-banner-subscription",
        ALTERNATE_SKU_MSG: "alternate-sku-msg",
    },
};
