import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    OOS_TEXT,
    MODAL_NAMES,
    CART_ITEM_TYPES,
    FREQUENCY_OPTIONS,
    DEFAULT_RECHARGE_OPTION,
} from "@constants";

import { Sku, Vacation, CartItem } from "@shared/models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { CartService } from "@services/shared/cart.service";
import { RouterService } from "@services/util/router.service";
import { CalendarService } from "@services/date/calendar.service";
import { SettingsService } from "@services/shared/settings.service";
import { MoengageService } from "@services/integration/moengage.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { Frequency } from "@types";

import { TEXTS } from "../constants";
import { ANALYTICS_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-subscription-create-layout",
    templateUrl: "./create.layout.html",
    styleUrls: ["../styles/create.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionCreatePageLayoutComponent implements OnInit {
    @Input() vacation: Vacation;
    @Input() fromCatalog: boolean;
    @Input() set sku(data: Sku) {
        this._sku = data;

        /* if it isn't a subscription item but initSubscriptionItem was called
        by item setter before sku prop was received, set the _subscriptionItem
        once sku data is received */
        if (!this._subscriptionItem) {
            this.initSubscriptionItem();
        }
    }
    get sku(): Sku {
        return this._sku;
    }
    @Input() set item(cartItem: CartItem) {
        this._item = cartItem;
        /* if it's a subscription item, but initSubscriptionItem was called by sku
        setter before item prop was received, overwrite _subscriptionItem using the
        initSubscriptionItem method */
        this.initSubscriptionItem();
    }
    get item(): CartItem {
        return this._item;
    }

    @Output() updateItemInCart: EventEmitter<CartItem> = new EventEmitter();

    oosDates = [];
    tpEnabled = false;
    deliveries: number;
    startDateText: string;
    frequencyText: string;
    showDateModal = false;
    oosDateCalendarText = "";
    showDeliveryModal = false;
    showFrequencyModal = false;
    _TEXTS = TEXTS.DESCRIPTION;
    _subscriptionItem: CartItem;
    _alternatesV2Experiment = true;

    readonly modalNames = MODAL_NAMES;
    readonly saClickObjectNames = ANALYTICS_OBJECT_NAMES.CLICK;
    readonly saImpressionObjectName =
        ANALYTICS_OBJECT_NAMES.IMPRESSION.TPONE_BANNER_SUBSCRIPTION;

    private _sku: Sku;
    private _item: CartItem;

    constructor(
        private skuService: SkuService,
        private cartService: CartService,
        private dateService: DateService,
        private utilService: UtilService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private calendarService: CalendarService,
        private moengageService: MoengageService,
        private blockAccessService: BlockAccessService
    ) {}

    ngOnInit() {
        this.setTpStatus();
        this.setOOSDates();
        this._alternatesV2Experiment = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );
    }

    deliverOnce() {
        let item: CartItem;

        if (this.isSubscriptionItem()) {
            item = this.cartService.getAddonItemFromSubscriptionItem(this.item);
        } else if (this.fromCatalog) {
            const qty = this._subscriptionItem.quantity;
            item = this.cartService.getCartItemFromSku(
                this.sku,
                qty,
                this.vacation
            );
        } else {
            return this.routerService.goBack();
        }

        this.updateItemInCart.emit(item);
        this.routerService.goBack();
    }

    confirmSubscription() {
        /* Block adding to cart in case of access restrictions */
        if (this.blockAccessService.isSubscriptionAccessRestricted()) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.updateItemInCart.emit(this._subscriptionItem);
        if (this.sku) {
            this.moengageService.trackSubscriptionAddToCart({
                sku_id: this.sku.id,
                sku_price: this.sku.unit_price,
                qty: this._subscriptionItem && this._subscriptionItem.quantity,
                recharge_quantity:
                    this._subscriptionItem &&
                    this._subscriptionItem.recharge_quantity,
                product_identifier: this.sku.product_identifier,
            });
        }
        this.routerService.goBack();
    }

    openFrequencySelectionModal() {
        this.showFrequencyModal = true;
    }

    closeFrequencySelectionModal() {
        this.showFrequencyModal = false;
    }

    openDeliverySelectionModal() {
        this.showDeliveryModal = true;
    }

    closeDeliverySelectionModal() {
        this.showDeliveryModal = false;
    }

    openDateSelectionModal() {
        /* Block future date selection in case of access restrictions */
        if (this.blockAccessService.isFutureDeliveryAccessRestricted()) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.showDateModal = true;
    }

    closeDateSelectionModal() {
        this.showDateModal = false;
    }

    onDeliveryChange(deliveries: number) {
        this.deliveries = deliveries;

        this.setRechargeQty();
        this.closeDeliverySelectionModal();
    }

    onFrequencyChange(frequency: Frequency) {
        this._subscriptionItem.frequency = frequency;

        this.setFrequencyText();
        this.closeFrequencySelectionModal();
    }

    onDateChange(date: string) {
        this._subscriptionItem.start_date = date;

        this.setStartDateText();
        this.closeDateSelectionModal();
    }

    onQtyUpdate(direction: number) {
        if (direction > 0) {
            this._subscriptionItem.quantity++;
        } else if (direction < 0 && this._subscriptionItem.quantity > 0) {
            this._subscriptionItem.quantity--;
        }

        this.setRechargeQty();
    }

    private initSubscriptionItem() {
        const isSubscriptionItem = this.isSubscriptionItem();

        if (isSubscriptionItem) {
            /* Edit flow from Cart/Catalog */
            this._subscriptionItem = { ...this.item };
        } else if (this.sku) {
            /* Create new subscription flow */
            this._subscriptionItem = {
                type: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
                quantity: 1,
                sku_id: this.sku.id,
                frequency: FREQUENCY_OPTIONS[0].FREQUENCY,
                recharge_quantity: DEFAULT_RECHARGE_OPTION,
                start_date: this.getStartDate(),
            };
        }

        this.setDeliveries();
        this.setStartDateText();
        this.setFrequencyText();
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private setDeliveries() {
        const rechargeQty = this.utilService.getNestedValue(
            this._subscriptionItem,
            "recharge_quantity",
            null
        );
        const qty = this.utilService.getNestedValue(
            this._subscriptionItem,
            "quantity",
            null
        );

        if (rechargeQty && qty) {
            this.deliveries = rechargeQty / qty;
            return;
        }

        this.deliveries = 0;
    }

    private setStartDateText() {
        const startDate = this.utilService.getNestedValue(
            this._subscriptionItem,
            "start_date",
            null
        );

        if (!startDate) {
            return;
        }

        this.startDateText = this.dateService.getDeliveryDateText(startDate);
    }

    private setFrequencyText() {
        const frequency = this.utilService.getNestedValue(
            this._subscriptionItem,
            "frequency"
        );

        this.frequencyText = this.utilService
            .getWeekDaysFormatFromDays(frequency)
            .toUpperCase();
    }

    private setRechargeQty() {
        this._subscriptionItem.recharge_quantity =
            this._subscriptionItem.quantity * this.deliveries;
    }

    private isSubscriptionItem(): boolean {
        return this.item && this.item.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW;
    }

    private getStartDate(): string {
        const date = this.calendarService.getNextAvailableDate(
            this.vacation,
            this.sku
        );
        return this.dateService.textFromDate(date);
    }

    private setOOSDates() {
        const oosDates = this.skuService.getOOSDates(this.sku);
        if (oosDates) {
            this.oosDates = oosDates;
            this.oosDateCalendarText = OOS_TEXT;
        }
    }
}
