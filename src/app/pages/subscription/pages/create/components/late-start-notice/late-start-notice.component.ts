import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnChanges,
    OnInit,
} from "@angular/core";

import { CART_ITEM_TYPES, OOS_ANALYTICS_TEXTS } from "@constants";

import { Sku, Vacation } from "@models";

import { Segment } from "@types";

import { CalendarService } from "@services/date/calendar.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";

import { LATE_START_TEXTS } from "./../../constants/text.constants";
import { ANALYTICS_OBJECT_NAMES } from "./../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-late-start-notice",
    template: `
        <ng-container *ngIf="showBlock">
            <div
                class="wrapper"
                saClick
                saImpression
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                    .ALTERNATE_SKU_MSG}"
                [saObjectValue]="sku?.id"
                [saContextList]="saContextList"
                (click)="goToAlternateProductsPage($event)"
            >
                <div class="suprRow infoText">
                    <supr-text type="caption">
                        {{ TEXTS.DESCRIPTION }}
                    </supr-text>
                </div>
                <div class="divider8"></div>
                <div class="innerWrapper">
                    <supr-text type="captionSemiBold">{{
                        TEXTS.NEED_SOMETHING_EARLIER
                    }}</supr-text>
                    <div class="suprRow availableProducts">
                        <supr-text type="captionSemiBold" class="">{{
                            TEXTS.VIEW_AVAILABLE_PRODUCTS
                        }}</supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
            </div>
        </ng-container>
    `,
    styleUrls: ["./late-start-notice.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LateStartNoticeComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() startDateText: string;
    @Input() quantity: number;
    @Input() vacation: Vacation;

    TEXTS = LATE_START_TEXTS;
    saContextList: Segment.ContextListItem[] = [];
    showBlock = true;

    constructor(
        private routerService: RouterService,
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    ngOnInit() {
        this.showLateStartBlock();
    }

    ngOnChanges() {
        this.setContextList();
    }

    goToAlternateProductsPage(event: MouseEvent | TouchEvent) {
        event.stopPropagation();
        if (this.sku && this.sku.id) {
            return this.routerService.goToOOSAlternatesPage(this.sku.id, {
                queryParams: {
                    intent: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
                },
            });
        }
    }

    private showLateStartBlock() {
        const subStartDate =
            this.startDateText || (this.sku && this.sku.next_available_date);
        if (subStartDate) {
            /* if subStartDate is same as next available date without considering sku information
             then don't show this block  */
            if (
                this.dateService.textFromDate(
                    this.calendarService.getNextAvailableDate(this.vacation)
                ) === subStartDate
            ) {
                this.showBlock = false;
            }
        }
    }

    private setContextList() {
        const contextList: Segment.ContextListItem[] = [];
        const { sku } = this;
        if (sku) {
            contextList.push(
                {
                    name: OOS_ANALYTICS_TEXTS.UNIT_MRP,
                    value: sku.unit_mrp,
                },
                {
                    name: OOS_ANALYTICS_TEXTS.UNIT_PRICE,
                    value: sku.unit_price,
                },
                {
                    name: OOS_ANALYTICS_TEXTS.TYPE,
                    value: "subscription_new",
                },
                {
                    name: OOS_ANALYTICS_TEXTS.START_DATE,
                    value: this.startDateText || sku.next_available_date,
                },
                {
                    name: OOS_ANALYTICS_TEXTS.QTY_PER_DELIVERY,
                    value: this.quantity,
                },
                {
                    name: OOS_ANALYTICS_TEXTS.RECHARGE_QTY,
                    value: 0,
                },
                {
                    name: OOS_ANALYTICS_TEXTS.OOS_PREFERRED_FLOW,
                    value: "subscription_new",
                },
                {
                    name: OOS_ANALYTICS_TEXTS.OOS_AVAILABLE_DATE,
                    value: sku.next_available_date,
                }
            );
        }
        this.saContextList = contextList;
    }
}
