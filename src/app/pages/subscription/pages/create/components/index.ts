import { DeliveryModalComponent } from "./deliveries-modal/deliveries-modal.component";
import { StartDateModalComponent } from "./start-date-modal/start-date-modal.component";
import { LateStartNoticeComponent } from "./late-start-notice/late-start-notice.component";

export const SubscriptionComponents = [
    DeliveryModalComponent,
    StartDateModalComponent,
    LateStartNoticeComponent,
];
