import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { RECHARGE_OPTIONS, RECHARGE_OPTIONS_MAP } from "@constants";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-subscription-create-delivery-modal",
    template: `
        <div class="wrapper">
            <div class="headerText">
                <supr-text type="body">Choose deliveries</supr-text>
            </div>
            <div class="divider4"></div>
            <div
                saClick
                class="optionWrapper"
                [saContext]="skuId"
                [saObjectValue]="option"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.DELIVERY_QTY}"
                [class.firstOption]="_i === 0"
                [class.selected]="deliveries === option"
                *ngFor="
                    let option of rechargeOptions;
                    index as _i;
                    trackBy: trackByFn
                "
                (click)="onDeliverySelection(option)"
            >
                <div class="option suprRow">
                    <supr-text type="regular14"
                        >{{ option }} Deliveries</supr-text
                    >
                    <supr-text
                        type="regular14"
                        class="label {{ rechargeOptionsMap[option].labelType }}"
                        *ngIf="rechargeOptionsMap[option]"
                    >
                        {{ rechargeOptionsMap[option].label }}
                    </supr-text>
                </div>
            </div>
            <div class="divider16"></div>
            <div class="btnWrapper">
                <supr-button
                    [saContext]="skuId"
                    [saObjectValue]="deliveries"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .CONFIRM_DELIVERY_QTY}"
                    (click)="handleDeliveryConfirmation.emit(deliveries)"
                >
                    Select deliveries
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["./deliveries-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryModalComponent implements OnInit {
    @Input() skuId: number;
    @Input() deliveries: number;

    rechargeOptions: number[];
    disabledQuantities: number[] = [];
    readonly rechargeOptionsMap = RECHARGE_OPTIONS_MAP;

    constructor(
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    @Output() handleDeliveryConfirmation: EventEmitter<
        number
    > = new EventEmitter();

    ngOnInit() {
        this.initialize();
    }

    onDeliverySelection(deliveries: number) {
        this.deliveries = deliveries;
    }

    trackByFn(index: number) {
        return index;
    }

    private initialize() {
        const subscriptionInfo = this.settingsService.getSettingsValue(
            "subscriptionInfo",
            null
        );

        if (subscriptionInfo) {
            this.disabledQuantities = this.utilService.getNestedValue(
                subscriptionInfo,
                "DISABLED_QUANTITIES",
                []
            );
        }

        this.rechargeOptions = RECHARGE_OPTIONS.filter(
            (op) => !this.isDisabledQty(op)
        );
    }

    private isDisabledQty(qty: number): boolean {
        return this.disabledQuantities.indexOf(qty) > -1;
    }
}
