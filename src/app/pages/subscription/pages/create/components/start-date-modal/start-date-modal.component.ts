import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { Vacation } from "@shared/models";

import { SuprDateService } from "@services/date/supr-date.service";
import { BlockAccessService } from "@services/shared/block-access.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";

import { Calendar } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";
import { TEXTS } from "../../constants";

@Component({
    selector: "supr-subscription-create-date-modal",
    template: `
        <div class="wrapper">
            <supr-calendar
                [vacation]="vacation"
                [disablePastDays]="true"
                [selectedDate]="selectedDate"
                [customDisabledDates]="customDisabledDates"
                [customDisabledDateText]="customDisabledDateText"
                (handleSelectedDate)="onDateSelection($event)"
            ></supr-calendar>
            <div class="divider24"></div>

            <!-- Rendered only when supr access cutoff time experiment is active -->
            <ng-container *ngIf="suprPassInfoText">
                <div class="suprRow suprPassInfo top">
                    <supr-icon name="error"></supr-icon>
                    <div class="spacer4"></div>
                    <supr-text type="caption">
                        {{ suprPassInfoText }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <div class="subscribeText">
                        <div
                            class="suprRow"
                            (click)="gotoSuprPassPage()"
                            saClick
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .SUBSCRIBE_SUPR_PASS_CLICK}"
                        >
                            <supr-text type="paragraph">
                                ${TEXTS.SUBSCRIBE_SUPR_PASS}
                            </supr-text>
                            <supr-icon name="chevron_right"></supr-icon>
                        </div>
                    </div>
                </div>
                <div class="divider12"></div>
            </ng-container>

            <supr-button
                [saContext]="skuId"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .CONFIRM_START_DATE}"
                (handleClick)="onDateConfirmation()"
            >
                Set start date ({{
                    selectedDate?.jsDate | date: "d LLL, yyyy"
                }})
            </supr-button>
        </div>
    `,
    styleUrls: ["./start-date-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartDateModalComponent implements OnInit {
    @Input() skuId: number;
    @Input() vacation: Vacation;
    @Input() customDisabledDates: Array<string> = [];
    @Input() customDisabledDateText = "";
    @Input() set startDate(date: string) {
        this.setDateInfo(date);
    }

    @Output() handleDateConfirmation: EventEmitter<string> = new EventEmitter();

    dateText: string;
    selectedDate: Calendar.DayData;
    suprPassInfoText: string;

    constructor(
        private suprDateService: SuprDateService,
        private blockAccessService: BlockAccessService,
        private dateService: DateService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.setSuprPassInfoText();
    }

    onDateSelection(date: Calendar.DayData) {
        this.selectedDate = date;
    }

    onDateConfirmation() {
        this.handleDateConfirmation.emit(this.selectedDate.dateText);
    }

    gotoSuprPassPage() {
        this.routerService.goToSuprPassPage();
    }

    /**
     * Only sets text in case supr pass cutoff time experiment is active for the current user
     *
     * @private
     * @returns
     * @memberof ItemRescheduleModalComponent
     */
    private setSuprPassInfoText() {
        if (!this.dateService.hasTodaysCutOffTimePassed()) {
            this.suprPassInfoText = "";
            return;
        }

        this.suprPassInfoText = this.blockAccessService.getCalendarInfoText();
    }

    private setDateInfo(date: string) {
        if (date) {
            this.selectedDate = this.suprDateService.suprDateFromText(date);
        }
    }
}
