export const TEXTS = {
    HEADER_BALANCE: {
        title: "Subscription",
        subtitle: "balance",
    },
    HEADER_DELIVERIES_LEFT: {
        title: "Deliveries",
        subtitle: "remaining",
    },
    HEADER_BALANCE_REMAINING: {
        title: "Quantity",
        subtitle: "remaining",
    },
    HEADER_DELIVERIES_APPROX: "Approx.",
    QTY_PER_DAY: "Quantity per day",
    REPEAT: "Repeat",
    CANCEL_SUB: "CANCEL SUBSCRIPTION",
    SCHEDULE_DELIVERY: "SCHEDULE DELIVERY",
    QUANTIY_MODAL_HEADER: "Change quantity preference",
    QUANTIY_MODAL_LABEL: "Quantity per day",
    QUANTIY_MODAL_ACTION: "UPDATE SUBSCRIPTION",
    UPCOMING_HEADER: "Upcoming",
    TRANSACTIONS_HEADER: "Past transactions",
    SEE_ALL_PRETEXT: "SEE ALL ",
    SEE_ALL_UPCOMING: " UPCOMING ORDERS",
    SEE_ALL_TRANSACTIONS: "PAST TRANSACTIONS",
    SUBSCRIPTION_ENDING_MSG:
        "Your subscription is about to end, please recharge now for uninterrupted deliveries.",
    SUBSCRIPTION_RESTART_MSG_1: "This subscription has ended.",
    SUBSCRIPTION_RESTART_MSG_2:
        "Please recharge to continue your subscription.",
    MESSAGE_MODAL_HEADER: "Total deliveries are based on market rate",
    MESSAGE_MODAL_BODY:
        "Deliveries remaining are adjusted as per the current market rate of the item.",
    MESSAGE_MODAL_ACTION: "OKAY",
};
