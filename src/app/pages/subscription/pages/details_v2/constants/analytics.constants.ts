export const SA_OBJECT_NAMES = {
    CLICK: {
        REPEAT: "repeat",
        CANCEL: "cancel",
        RESTART: "restart",
        RECHARGE: "recharge",
        SCHEDULE: "schedule",
        VIEW_PAST: "view-past",
        QTY_PER_DAY: "qty-per-day",
        TOOLTIP_MODAL_OK: "ok-button",
        VIEW_UPCOMING: "view-upcoming",
        EDIT_DELIVERY: "edit-delivery",
        CONFIRM_QTY: "confirm-quantity",
        DELIVERY_TOOLTIP: "subscription-delivery-info",
    },
};

export const MODAL_NAMES = {
    QUANTITY: "update-qty",
    REPEAT: "update-repeat",
    DELIVERY_INFO: "delivery-estimate-info",
};
