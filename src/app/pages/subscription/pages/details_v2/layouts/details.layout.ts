import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Schedule, Subscription, SubscriptionTransaction } from "@models";

import { RouterService } from "@services/util/router.service";
import { SubscriptionService } from "@services/shared/subscription.service";

import { ScheduleCurrentState } from "@store/schedule/schedule.state";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { Frequency, SuprApi, Segment } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";
import { SUBSCRIPTION_LIST_TYPES } from "@pages/subscription/pages/list_v2/constants";

@Component({
    selector: "supr-subscription-details-layout",
    templateUrl: "./details.layout.html",
    styleUrls: ["../styles/details.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsLayoutComponent implements OnInit, OnChanges {
    @Input() schedules: Schedule[];
    @Input() subscription: Subscription;
    @Input() scheduleState: ScheduleCurrentState;
    @Input() transactions: SubscriptionTransaction[];
    @Input() subscriptionState: SubscriptionCurrentState;

    @Output() fetchSchedule: EventEmitter<{
        subscriptionId: number;
    }> = new EventEmitter();
    @Output() fetchSubscriptionTransactions: EventEmitter<
        number
    > = new EventEmitter();
    @Output() handleSubscriptionUpdate: EventEmitter<{
        subscriptionId: number;
        instantRefreshSchedule?: boolean;
        subscriptionData: SuprApi.SubscriptionBody;
    }> = new EventEmitter();
    @Output() handleFetchSubscriptions: EventEmitter<void> = new EventEmitter();

    _showLoader = false;
    _showQtyModal = false;
    _showEditModal = false;
    _loadingSchedule = true;
    _showCancelModal = false;
    _subscriptionType: string;
    _showMessageModal = false;
    _showScheduleModal = false;
    _loadingTransactions = true;
    _showFrequencyModal = false;
    _saObjectNames = SA_OBJECT_NAMES;
    _saContextList: Segment.ContextListItem[] = [];
    SUBSCRIPTION_LIST_TYPES = SUBSCRIPTION_LIST_TYPES;

    constructor(
        private routerService: RouterService,
        private subscriptionService: SubscriptionService
    ) {}

    ngOnInit() {
        if (!this.subscription) {
            this.handleFetchSubscriptions.emit();
            return;
        }

        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSubscriptionChange(changes["subscription"]);
        this.handleScheduleStateChange(changes["scheduleState"]);
        this.handleSubscriptionStateChange(changes["subscriptionState"]);
    }

    openQuantityModal() {
        if (this._subscriptionType !== SUBSCRIPTION_LIST_TYPES.EXPIRED) {
            this._showQtyModal = true;
        }
    }

    closeQuantityModal() {
        this._showQtyModal = false;
    }

    openScheduleModal() {
        this._showScheduleModal = true;
    }

    closeScheduleModal() {
        this._showScheduleModal = false;
    }

    openCancelModal() {
        this._showCancelModal = true;
    }

    closeCancelModal() {
        this._showCancelModal = false;
    }

    openFrequencyModal() {
        if (this._subscriptionType !== SUBSCRIPTION_LIST_TYPES.EXPIRED) {
            this._showFrequencyModal = true;
        }
    }

    closeFrequencyModal() {
        this._showFrequencyModal = false;
    }

    toggleEditModal(state: boolean) {
        this._showEditModal = state;
    }

    openEditModal() {
        this._showEditModal = true;
    }

    closeEditModal() {
        this._showEditModal = false;
    }

    onQuantityChange(quantity: number) {
        if (this.subscription) {
            this.subscription = { ...this.subscription, quantity };

            this.handleSubscriptionUpdate.emit({
                instantRefreshSchedule: true,
                subscriptionData: this.subscription,
                subscriptionId: this.subscription.id,
            });
        }
    }

    onFrequencyChange(frequency: Frequency) {
        if (this.subscription) {
            this.subscription = { ...this.subscription, frequency };

            this.handleSubscriptionUpdate.emit({
                instantRefreshSchedule: true,
                subscriptionData: this.subscription,
                subscriptionId: this.subscription.id,
            });
        }
    }

    openMessageModal() {
        this._showMessageModal = true;
    }

    closeMessageModal() {
        this._showMessageModal = false;
    }

    private initialize() {
        this.setType();
        this.setAnalyticsData();
        this.fetchSubscriptionTransactions.emit(this.subscription.id);
        this.fetchSchedule.emit({ subscriptionId: this.subscription.id });
        this.subscriptionService.setSubscriptionDetailsInfo(this.subscription);
    }

    private setType() {
        this._subscriptionType = this.subscriptionService.getSubscriptionType(
            this.subscription
        );
    }

    private handleSubscriptionChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            /* [TODO_RITESH]
            Currently subscriptions are refetched post recharge/restart and hence
            we need to refetch transactions to reflect the recharge transaction.
            But when subscription instruction is updated, the subscription data gets refreshed as well to get
            latest balance and no. of deliveries.
            But this forces a transaction fetch (we have no way to identify what triggered the subscripion refetch)
            which isn't necessary after subscription instruction update. Need to handle this */
            this.initialize();
        }
    }

    private handleScheduleStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue === ScheduleCurrentState.FETCHING_SCHEDULE) {
            this._loadingSchedule = true;
        } else if (
            change.currentValue ===
                ScheduleCurrentState.FETCHING_SCHEDULE_DONE ||
            change.currentValue === ScheduleCurrentState.UPDATING_CALENDAR_DONE
        ) {
            this._loadingSchedule = false;
        }
    }

    private handleSubscriptionStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue === SubscriptionCurrentState.NO_ACTION) {
            if (
                change.previousValue ===
                SubscriptionCurrentState.UPDATING_SUBSCRIPTION
            ) {
                this.closeQuantityModal();
                this.closeFrequencyModal();
            }

            if (
                change.previousValue ===
                SubscriptionCurrentState.CANCELLING_SUBSCRIPTION
            ) {
                return this.routerService.goBack();
            }
        }

        switch (change.currentValue) {
            case SubscriptionCurrentState.UPDATING_SUBSCRIPTION:
                this._showLoader = true;
                this._loadingSchedule = true;
                break;
            case SubscriptionCurrentState.FETCHING_SUBSCRIPTIONS:
                this._loadingSchedule = true;
                this._loadingTransactions = true;
                break;
            case SubscriptionCurrentState.CANCELLING_SUBSCRIPTION:
                this._showLoader = true;
                break;
            case SubscriptionCurrentState.FETCHING_SUB_TRANSACTIONS:
                this._loadingTransactions = true;
                break;
            case SubscriptionCurrentState.FETCHING_SUB_TRANSACTIONS_DONE:
                this._loadingTransactions = false;
                break;
            default:
                this._showLoader = false;
                break;
        }
    }

    private setAnalyticsData() {
        this._saContextList.push(
            {
                name: "deliveries",
                value: this.subscription.deliveries_remaining,
            },
            {
                name: "balance",
                value: this.subscription.balance_amount,
            }
        );
    }
}
