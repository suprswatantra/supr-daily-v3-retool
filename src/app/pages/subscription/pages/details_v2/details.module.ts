import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { subscriptionDetailsComponents } from "./components";
import { subscriptionDetailsContainers } from "./containers";
import { SubscriptionDetailsLayoutComponent } from "./layouts/details.layout";
import { SubscriptionDetailsContainer } from "./containers/details.container";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionDetailsContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        ...subscriptionDetailsComponents,
        ...subscriptionDetailsContainers,
        SubscriptionDetailsLayoutComponent,
    ],
})
export class SubscriptionDetailsV2PageModule {}
