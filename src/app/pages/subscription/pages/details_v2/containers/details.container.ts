import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { Subscription, Schedule, SubscriptionTransaction } from "@models";

import { ScheduleCurrentState } from "@store/schedule/schedule.state";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { SuprApi, FetchScheduleActionPayload } from "@types";

import { SubscriptionPageAdapter as Adapter } from "@pages/subscription/services/subscription.adapter";

@Component({
    selector: "supr-subscription-details-container",
    template: `
        <supr-subscription-details-layout
            [schedules]="schedules$ | async"
            [subscription]="subscription$ | async"
            [transactions]="transactions$ | async"
            [scheduleState]="scheduleCurrentState$ | async"
            [subscriptionState]="subscriptionState$ | async"
            (fetchSchedule)="fetchSchedule($event)"
            (handleFetchSubscriptions)="fetchSubscriptionList()"
            (handleSubscriptionUpdate)="updateSubscription($event)"
            (fetchSubscriptionTransactions)="
                fetchSubscriptionTransactions($event)
            "
        ></supr-subscription-details-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsContainer implements OnInit {
    schedules$: Observable<Schedule[]>;
    subscription$: Observable<Subscription>;
    transactions$: Observable<SubscriptionTransaction[]>;
    scheduleCurrentState$: Observable<ScheduleCurrentState>;
    subscriptionState$: Observable<SubscriptionCurrentState>;

    constructor(
        private adapter: Adapter,
        private scheduleAdapter: ScheduleAdapter
    ) {}

    ngOnInit() {
        this.transactions$ = this.adapter.transactions$;
        this.subscription$ = this.adapter.subscription$;
        this.schedules$ = this.adapter.getSubscriptionSchedule();
        this.subscriptionState$ = this.adapter.subscriptionState$;
        this.scheduleCurrentState$ = this.scheduleAdapter.scheduleCurrentState$;
    }

    updateSubscription(data: {
        subscriptionId: number;
        instantRefreshSchedule?: boolean;
        subscriptionData: SuprApi.SubscriptionBody;
    }) {
        this.adapter.updateSubscription(
            data.subscriptionId,
            data.subscriptionData,
            data.instantRefreshSchedule
        );
    }

    fetchSubscriptionList() {
        this.adapter.fetchSubscriptions();
    }

    fetchSchedule(data: FetchScheduleActionPayload) {
        this.scheduleAdapter.getSchedule(data);
    }

    fetchSubscriptionTransactions(subscriptionId: number) {
        this.adapter.fetchSubscriptionTransactions(subscriptionId);
    }
}
