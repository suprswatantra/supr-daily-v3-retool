import { SubscriptionDetailsContainer } from "./details.container";
import { SubscriptionDetailsCancelContainer } from "./cancel.container";

export const subscriptionDetailsContainers = [
    SubscriptionDetailsContainer,
    SubscriptionDetailsCancelContainer,
];
