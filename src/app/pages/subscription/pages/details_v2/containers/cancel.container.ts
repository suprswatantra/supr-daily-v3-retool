import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";
import { SubscriptionPageAdapter as Adapter } from "@pages/subscription/services/subscription.adapter";

@Component({
    selector: "supr-subscription-details-cancel-confirm-container",
    template: `
        <supr-subscription-details-cancel-modal-content
            [subscriptionState]="subscriptionState$ | async"
            (cancelSubscription)="cancelSubscription()"
        ></supr-subscription-details-cancel-modal-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsCancelContainer implements OnInit {
    @Input() subscriptionId: number;

    subscriptionState$: Observable<SubscriptionCurrentState>;
    subscriptionError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.subscriptionState$ = this.adapter.subscriptionState$;
        this.subscriptionError$ = this.adapter.subscriptionError$;
    }

    cancelSubscription() {
        this.adapter.cancelSubscription(this.subscriptionId);
    }
}
