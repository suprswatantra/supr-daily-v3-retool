import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DEVICE_WIDTHS } from "@constants";

import { Segment } from "@types";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SUBSCRIPTION_LIST_TYPES } from "@pages/subscription/pages/list_v2/constants";

@Component({
    selector: "supr-subscription-details-action",
    template: `
        <div [class.suprRow]="!_stackedBtns" class="wrapper spaceBetween">
            <supr-button
                class="cancelBtn"
                [class.stacked]="_stackedBtns"
                [saContext]="saContext"
                [saObjectValue]="saObjectValue"
                [saContextList]="saContextList"
                saObjectName="${SA_OBJECT_NAMES.CLICK.CANCEL}"
                (handleClick)="handleCancelClick.emit()"
            >
                <supr-text type="paragraph">${TEXTS.CANCEL_SUB}</supr-text>
            </supr-button>
            <div *ngIf="!_stackedBtns" class="spacer12"></div>
            <div *ngIf="_stackedBtns" class="divider12"></div>
            <supr-button
                class="scheduleBtn"
                [class.stacked]="_stackedBtns"
                [disabled]="type === '${SUBSCRIPTION_LIST_TYPES.EXPIRED}'"
                [class.disabled]="type === '${SUBSCRIPTION_LIST_TYPES.EXPIRED}'"
                [saContext]="saContext"
                [saObjectValue]="saObjectValue"
                [saContextList]="saContextList"
                saObjectName="${SA_OBJECT_NAMES.CLICK.SCHEDULE}"
                (handleClick)="handleScheduleClick.emit()"
            >
                <supr-text type="paragraph">
                    ${TEXTS.SCHEDULE_DELIVERY}
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./actions.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionComponent implements OnInit {
    @Input() type: string;
    @Input() saContext: number;
    @Input() saObjectValue: number;
    @Input() saContextList: Segment.ContextListItem[];

    @Output() handleCancelClick: EventEmitter<void> = new EventEmitter();
    @Output() handleScheduleClick: EventEmitter<void> = new EventEmitter();

    _stackedBtns = false;

    ngOnInit() {
        if (screen.width < DEVICE_WIDTHS[375]) {
            this._stackedBtns = true;
        }
    }
}
