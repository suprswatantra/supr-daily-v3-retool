import { ActionComponent } from "./actions/actions.component";
import { SubscriptionDetailsCancelModalComponents } from "./cancel-modal/";
import { SubscriptionDetailsStatusFooterComponent } from "./footer/footer.component";
import { SubscriptionDetailsScheduleLoader } from "./details-schedule/loader.component";
import { SubscriptionDetailsSchedule } from "./details-schedule/details-schedule.component";
import { SubscriptionMessageModalComponent } from "./message-modal/message-modal.component";
import { SubscriptionDetailsQuantityComponent } from "./quantity-card/quantity-card.component";
import { SubscriptionDetailsProductInfoComponent } from "./product-info/product-info.component";
import { SubscriptionDetailsFrequencyComponent } from "./frequency-card/frequency-card.component";
import { SubscriptionDetailsQuantityModalComponent } from "./quantity-modal/quantity-modal.component";
import { SubscriptionDetailsFrequencyModalComponent } from "./frequency-modal/frequency-modal.component";

export const subscriptionDetailsComponents = [
    ActionComponent,
    SubscriptionDetailsSchedule,
    SubscriptionDetailsScheduleLoader,
    SubscriptionMessageModalComponent,
    SubscriptionDetailsQuantityComponent,
    SubscriptionDetailsFrequencyComponent,
    SubscriptionDetailsProductInfoComponent,
    SubscriptionDetailsStatusFooterComponent,
    SubscriptionDetailsQuantityModalComponent,
    SubscriptionDetailsFrequencyModalComponent,
    ...SubscriptionDetailsCancelModalComponents,
];
