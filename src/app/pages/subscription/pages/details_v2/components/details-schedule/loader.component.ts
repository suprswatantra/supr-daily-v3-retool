import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-subscription-details-schedule-loader",
    template: `
        <ion-skeleton-text animated class="header"></ion-skeleton-text>
        <div class="divider16"></div>
        <ng-container *ngFor="let loader of loaders">
            <ion-skeleton-text animated class="first"></ion-skeleton-text>
            <ion-skeleton-text animated class="second"></ion-skeleton-text>
            <ion-skeleton-text animated class="third"></ion-skeleton-text>
            <div class="divider16"></div>
        </ng-container>
    `,
    styleUrls: ["./details-schedule.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsScheduleLoader {
    loaders = [1, 2, 3];
}
