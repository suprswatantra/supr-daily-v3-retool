import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import {
    Schedule,
    Subscription,
    ScheduleItem,
    SubscriptionTransaction,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { ErrorService } from "@services/integration/error.service";

import { Segment } from "@types";

import { TEXTS } from "../../constants";
import { TABS } from "@pages/subscription/pages/summary/constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

const MAX_ITEMS = 3;

@Component({
    selector: "supr-subscription-details-schedule",
    template: `
        <ng-container *ngIf="history; else scheduleView">
            <div
                class="wrapper"
                *ngIf="!loading && transactions && transactions.length"
            >
                <supr-text type="subheading" class="wrapperHeaderText">
                    ${TEXTS.TRANSACTIONS_HEADER}
                </supr-text>

                <div class="divider16"></div>

                <ng-container
                    *ngFor="
                        let transaction of transactions;
                        trackBy: trackByFn;
                        last as isLast
                    "
                >
                    <supr-subscription-schedule-tile
                        [history]="history"
                        [transaction]="transaction"
                        [subscription]="subscription"
                    ></supr-subscription-schedule-tile>

                    <div class="divider16" *ngIf="!isLast || _actionText"></div>
                </ng-container>

                <div
                    saClick
                    class="suprRow"
                    *ngIf="_actionText"
                    (click)="goToSummaryPage()"
                    [saContextList]="saContextList"
                    [saObjectValue]="subscription?.id"
                    [saContext]="subscription?.sku_data?.id"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.VIEW_PAST}"
                >
                    <supr-text type="paragraph" class="wrapperFooterText">
                        {{ _actionText }}
                    </supr-text>
                    <supr-icon
                        name="chevron_right"
                        class="wrapperFooterIcon"
                    ></supr-icon>
                </div>
            </div>
        </ng-container>

        <ng-template #scheduleView>
            <div
                class="wrapper"
                *ngIf="!loading && schedules && schedules.length"
            >
                <supr-text type="subheading" class="wrapperHeaderText">
                    ${TEXTS.UPCOMING_HEADER}
                </supr-text>

                <div class="divider16"></div>

                <ng-container
                    *ngFor="
                        let schedule of schedules;
                        trackBy: trackByFn;
                        last as isLast
                    "
                >
                    <supr-subscription-schedule-tile
                        [history]="history"
                        [schedule]="schedule"
                        [subscription]="subscription"
                        [saObjectValue]="subscription?.id"
                        [saContext]="subscription?.sku_data?.id"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.EDIT_DELIVERY}"
                        (handleEditClick)="handleEditClick(schedule)"
                    ></supr-subscription-schedule-tile>

                    <div class="divider16" *ngIf="!isLast || _actionText"></div>
                </ng-container>

                <div
                    saClick
                    class="suprRow"
                    *ngIf="_actionText"
                    (click)="goToSummaryPage()"
                    [saContextList]="saContextList"
                    [saObjectValue]="subscription?.id"
                    [saContext]="subscription?.sku_data?.id"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.VIEW_UPCOMING}"
                >
                    <supr-text type="paragraph" class="wrapperFooterText">
                        {{ _actionText }}
                    </supr-text>
                    <supr-icon
                        name="chevron_right"
                        class="wrapperFooterIcon"
                    ></supr-icon>
                </div>
            </div>
        </ng-template>

        <ng-container *ngIf="loading">
            <supr-subscription-details-schedule-loader>
            </supr-subscription-details-schedule-loader>
        </ng-container>

        <supr-modal
            *ngIf="showModal && _activeScheduleItem"
            (handleClose)="toggleEditModal.emit(false)"
            [saObjectValue]="subscription?.id"
            modalName="${MODAL_NAMES.EDIT_SUBSCRIPTION_DELIVERY}"
        >
            <supr-subscription-edit-card-container
                [date]="_activeDate"
                [subscription]="subscription"
                [sku]="subscription?.sku_data"
                [instantRefreshSchedule]="true"
                [schedule]="_activeScheduleItem"
            ></supr-subscription-edit-card-container>
        </supr-modal>
    `,
    styleUrls: ["./details-schedule.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsSchedule {
    @Input() loading = true;
    @Input() history = false;
    @Input() showModal = false;
    @Input() subscription: Subscription;

    @Input() saContextList: Segment.ContextListItem[] = [];

    @Input() set schedules(data: Schedule[]) {
        if (data && data.length) {
            this._totalItems = data.length;
            this._schedules = this.setVisibleSchedules(data);

            this.setActionText();
        }
    }
    get schedules(): Schedule[] {
        return this._schedules;
    }

    @Input() set transactions(data: SubscriptionTransaction[]) {
        if (data && data.length) {
            this._totalItems = data.length;
            this._transactions = data.slice(0, MAX_ITEMS);

            this.setActionText();
        }
    }
    get transactions(): SubscriptionTransaction[] {
        return this._transactions;
    }

    @Output() toggleEditModal: EventEmitter<boolean> = new EventEmitter();

    _activeDate: string;
    _actionText: string;
    _totalItems: number;
    _activeScheduleItem: ScheduleItem;

    private _schedules: Schedule[] = [];
    private _transactions: SubscriptionTransaction[] = [];

    constructor(
        private utilService: UtilService,
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private errorService: ErrorService
    ) {}

    goToSummaryPage() {
        const subscriptionId = this.utilService.getNestedValue(
            this.subscription,
            "id"
        );
        if (subscriptionId) {
            this.routerService.goToSubscriptionSummaryPage(subscriptionId, {
                queryParams: {
                    active: !this.history
                        ? TABS.SCHEDULE.type
                        : TABS.TRANSACTIONS.type,
                },
            });
        } else {
            this.errorService.logSentryError(
                new Error(),
                {
                    context: {
                        subscription: this.subscription,
                        schedules: this._schedules,
                    },
                    message:
                        "Not able to route to subscription summary page from details-schedule.component",
                },
                false
            );
        }
    }

    handleEditClick(schedule: Schedule) {
        this._activeDate = schedule && schedule.date;
        this._activeScheduleItem = this.utilService.getNestedValue(
            schedule,
            "subscriptions.0"
        );

        this.toggleEditModal.emit(true);
    }

    private setActionText() {
        if (this._totalItems > MAX_ITEMS) {
            this._actionText = !this.history
                ? `${TEXTS.SEE_ALL_PRETEXT} ${this._totalItems} ${TEXTS.SEE_ALL_UPCOMING}`
                : `${TEXTS.SEE_ALL_PRETEXT} ${this._totalItems} ${TEXTS.SEE_ALL_TRANSACTIONS}`;
        } else {
            this._actionText = null;
        }
    }

    private setVisibleSchedules(schedules: Schedule[]): Schedule[] {
        let filteredSchedules: Schedule[] = [];

        if (this.utilService.isLengthyArray(schedules)) {
            filteredSchedules = schedules.filter((schedule: Schedule) =>
                this.scheduleService.hasScheduledDeliveries(schedule)
            );
        }

        return filteredSchedules.slice(0, MAX_ITEMS);
    }
}
