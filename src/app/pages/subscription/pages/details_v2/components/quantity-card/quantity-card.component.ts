import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants/";

@Component({
    selector: "supr-subscription-details-quantity",
    template: `
        <div class="card quantity">
            <div class="cardHeader">
                <supr-text type="paragraph">${TEXTS.QTY_PER_DAY}</supr-text>
            </div>
            <div class="suprColumn bottom cardContent">
                <div class="suprRow left center">
                    <supr-text class="quantity" type="subheading">{{
                        quantity
                    }}</supr-text>
                    <supr-icon
                        *ngIf="!disabled"
                        name="chevron_right"
                    ></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/info-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsQuantityComponent {
    @Input() disabled = false;
    @Input() quantity: number;
}
