import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Segment } from "@types";

import { TEXTS } from "../../constants";
import {
    SA_OBJECT_NAMES,
    MODAL_NAMES,
} from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-details-quantity-modal",
    template: `
        <supr-modal
            *ngIf="showModal"
            modalName=${MODAL_NAMES.QUANTITY}
            (handleClose)="handleModalClose.emit()"
        >
            <div class="wrapper">
                <supr-text class="wrapperHeader" type="subtitle">
                    ${TEXTS.QUANTIY_MODAL_HEADER}
                </supr-text>
                <div class="divider32"></div>
                <div class="suprRow left center wrapperContent">
                    <supr-icon name="cart"></supr-icon>
                    <div class="spacer14"></div>
                    <div class="wrapperContentLabel">
                        <supr-text type="action14">
                            ${TEXTS.QUANTIY_MODAL_LABEL}
                        </supr-text>
                    </div>

                    <supr-number-counter
                        [thresholdQty]="0"
                        [quantity]="quantity"
                        [minusDisabled]="quantity === 1"
                        (handleClick)="onQtyUpdate($event)"
                    ></supr-number-counter>
                </div>
                <supr-button
                    [loading]="loading"
                    [saContext]="saContext"
                    [saObjectValue]="saObjectValue"
                    [saContext]="saContext"
                    [saObjectValue]="saObjectValue"
                    [saContextList]="_saContextList"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.CONFIRM_QTY}"
                    (handleClick)="handleQuantityChange.emit(quantity)"
                >
                    <supr-text type="body">
                        ${TEXTS.QUANTIY_MODAL_ACTION}
                    </supr-text>
                </supr-button>
            </div>
        </supr-modal>
    `,
    styleUrls: ["./quantity-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsQuantityModalComponent implements OnInit {
    @Input() loading = false;
    @Input() quantity: number;
    @Input() showModal = false;

    @Input() saContext: number;
    @Input() saObjectValue: number;

    @Output() handleModalClose: EventEmitter<void> = new EventEmitter();
    @Output() handleQuantityChange: EventEmitter<number> = new EventEmitter();

    _saContextList: Segment.ContextListItem[];

    ngOnInit() {
        this.setAnalyticsData();
    }

    onQtyUpdate(direction: number) {
        if (direction > 0) {
            this.quantity++;
        } else if (direction < 0 && this.quantity > 0) {
            this.quantity--;
        }

        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        this._saContextList = [
            {
                value: this.quantity,
                name: SA_OBJECT_NAMES.CLICK.QTY_PER_DAY,
            },
        ];
    }
}
