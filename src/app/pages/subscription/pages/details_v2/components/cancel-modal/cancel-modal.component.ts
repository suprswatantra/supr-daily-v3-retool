import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { Subscription } from "@models";

@Component({
    selector: "supr-subscription-details-cancel-modal",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                modalName="${MODAL_NAMES.CANCEL_CONFIRM_SUBSCRIPTION}"
                (handleClose)="closeCancelModal()"
            >
                <supr-subscription-details-cancel-confirm-container
                    [subscriptionId]="subscription?.id"
                ></supr-subscription-details-cancel-confirm-container>
            </supr-modal>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalWrapperComponent {
    @Input() showModal = false;
    @Input() subscription: Subscription;

    @Output() handleModalClose: EventEmitter<void> = new EventEmitter();

    closeCancelModal() {
        this.showModal = false;
        this.handleModalClose.emit();
    }
}
