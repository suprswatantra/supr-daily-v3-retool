import { ModalContentComponent } from "./modal-content.component";
import { ModalWrapperComponent } from "./cancel-modal.component";

export const SubscriptionDetailsCancelModalComponents = [
    ModalContentComponent,
    ModalWrapperComponent,
];
