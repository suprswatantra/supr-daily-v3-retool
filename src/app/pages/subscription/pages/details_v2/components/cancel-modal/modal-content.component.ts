import {
    Input,
    Output,
    Component,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    EventEmitter,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { RATING_NUDGE_TRIGGERS, TOAST_MESSAGES } from "@constants";
import { TEXTS } from "@pages/subscription/constants/subscription.constants";

import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";
import { NudgeService } from "@services/layout/nudge.service";
import { MoengageService } from "@services/integration/moengage.service";
import { SubscriptionService } from "@services/shared/subscription.service";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";
@Component({
    selector: "supr-subscription-details-cancel-modal-content",
    template: `
        <div class="modal cancel">
            <supr-text type="subtitle">
                ${TEXTS.CANCEL_SUBSCRIPTION}
            </supr-text>
            <div class="divider16"></div>

            <supr-text class="body" type="body">
                ${TEXTS.CANCEL_SUBSCRIPTION_CONFIRM}
            </supr-text>
            <div class="divider20"></div>

            <supr-text class="body" type="body">
                ${TEXTS.CANCEL_SUBSCRIPTION_HELPER}
            </supr-text>

            <supr-button
                (handleClick)="_cancelSubscription()"
                [loading]="showLoader"
            >
                <supr-text type="body">
                    ${TEXTS.CANCEL_SUBSCRIPTION}
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./cancel-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalContentComponent implements OnChanges {
    @Input() subscriptionError: any;
    @Input() subscriptionState: SubscriptionCurrentState;
    @Output() cancelSubscription: EventEmitter<MouseEvent> = new EventEmitter();

    showLoader = false;

    constructor(
        private cdr: ChangeDetectorRef,
        private nudgeService: NudgeService,
        private toastService: ToastService,
        private routerService: RouterService,
        private moengageService: MoengageService,
        private subscriptionService: SubscriptionService
    ) {}

    _cancelSubscription() {
        this.showLoader = true;
        this.cdr.detectChanges();
        this.cancelSubscription.emit();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSubscriptionStateChange(changes["subscriptionState"]);
    }

    private handleSubscriptionStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.previousValue ===
                SubscriptionCurrentState.CANCELLING_SUBSCRIPTION &&
            change.currentValue === SubscriptionCurrentState.NO_ACTION
        ) {
            if (!this.subscriptionError) {
                this.toastService.present(
                    TOAST_MESSAGES.SUBSCRIPTIONS.CANCELLED
                );
                this.sendAnalyticsEvents();
                this.sendRatingNudge();
                this.routerService.goBack();
            }
        }
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.CANCEL_SUBSCRIPTION
        );
    }

    private sendAnalyticsEvents() {
        const subscriptionDetails = this.subscriptionService.getSubscriptionDetailsInfo();
        this.moengageService.trackSubscriptionCancellation(subscriptionDetails);
    }
}
