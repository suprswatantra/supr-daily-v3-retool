import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TEXTS } from "../../constants";
import {
    MODAL_NAMES,
    SA_OBJECT_NAMES,
} from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-message-modal",
    template: `
        <supr-modal
            modalName="${MODAL_NAMES.DELIVERY_INFO}"
            *ngIf="showModal"
            (handleClose)="handleModalClose.emit()"
        >
            <div class="wrapper">
                <supr-text type="subtitle" class="header">
                    ${TEXTS.MESSAGE_MODAL_HEADER}
                </supr-text>
                <div class="divider12"></div>
                <supr-text type="body" class="content">
                    ${TEXTS.MESSAGE_MODAL_BODY}
                </supr-text>
                <div class="divider24"></div>
                <supr-button
                    saObjectName="${SA_OBJECT_NAMES.CLICK.TOOLTIP_MODAL_OK}"
                    (handleClick)="handleModalClose.emit()"
                >
                    <supr-text type="body">
                        ${TEXTS.MESSAGE_MODAL_ACTION}
                    </supr-text>
                </supr-button>
            </div>
        </supr-modal>
    `,
    styleUrls: ["./message-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionMessageModalComponent {
    @Input() showModal = false;

    @Output() handleModalClose: EventEmitter<void> = new EventEmitter();
}
