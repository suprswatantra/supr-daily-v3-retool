import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Sku, Subscription } from "@shared/models";

import { TEXTS } from "../../constants/";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SUBSCRIPTION_LIST_TYPES } from "@pages/subscription/pages/list_v2/constants";

@Component({
    selector: "supr-subscription-details-product-info",
    template: `
        <div class="product suprColumn center">
            <div class="divider24"></div>
            <supr-image
                [src]="sku?.image?.fullUrl"
                [image]="sku?.image"
            ></supr-image>
            <div class="divider20"></div>
            <supr-text class="productName" type="heading">
                {{ sku?.sku_name }}
            </supr-text>
            <div class="divider18"></div>
            <div class="suprRow center top productInfo">
                <div class="suprColumn productInfoBalance">
                    <div class="inlineWrapper">
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_BALANCE.title}
                        </supr-text>
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_BALANCE.subtitle}
                        </supr-text>
                        <div class="divider4"></div>
                        <supr-text type="subheading600">
                            {{ subscription?.balance_amount | rupee: 2 }}
                        </supr-text>
                    </div>
                </div>
                <div class="suprColumn  productInfoDeliveries">
                    <div class="inlineWrapper">
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_DELIVERIES_LEFT.title}
                        </supr-text>
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_DELIVERIES_LEFT.subtitle}
                        </supr-text>
                        <div class="divider4"></div>
                        <div class="suprRow">
                            <supr-text
                                [class.caution]="
                                    type ===
                                    '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                "
                                type="subheading600"
                            >
                                {{ subscription?.deliveries_remaining }}
                            </supr-text>
                            <div class="spacer4"></div>
                            <div
                                class="suprRow"
                                *ngIf="subscription?.deliveries_remaining"
                                (click)="handleTooltipClick.emit()"
                            >
                                <supr-text
                                    class="label approx"
                                    [class.caution]="
                                        type ===
                                        '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                    "
                                    type="subtext10"
                                >
                                    ${TEXTS.HEADER_DELIVERIES_APPROX}
                                </supr-text>
                                <div class="spacer4"></div>
                                <supr-icon
                                    saClick
                                    name="error"
                                    class="tooltip"
                                    [class.caution]="
                                        type ===
                                        '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                    "
                                    saObjectName="${SA_OBJECT_NAMES.CLICK
                                        .DELIVERY_TOOLTIP}"
                                ></supr-icon>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="suprColumn productInfoQuantity">
                    <div class="inlineWrapper">
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_BALANCE_REMAINING.title}
                        </supr-text>
                        <supr-text class="label" type="paragraph">
                            ${TEXTS.HEADER_BALANCE_REMAINING.subtitle}
                        </supr-text>
                        <div class="divider4"></div>
                        <div class="suprRow">
                            <supr-text
                                [class.caution]="
                                    type ===
                                    '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                "
                                type="subheading600"
                            >
                                {{ subscription?.balance }}
                            </supr-text>
                            <div class="spacer4"></div>
                            <div
                                class="suprRow"
                                *ngIf="subscription?.balance"
                                (click)="handleTooltipClick.emit()"
                            >
                                <supr-text
                                    class="label approx"
                                    [class.caution]="
                                        type ===
                                        '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                    "
                                    type="subtext10"
                                >
                                    ${TEXTS.HEADER_DELIVERIES_APPROX}
                                </supr-text>
                                <div class="spacer4"></div>
                                <supr-icon
                                    saClick
                                    name="error"
                                    class="tooltip"
                                    [class.caution]="
                                        type ===
                                        '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                                    "
                                    saObjectName="${SA_OBJECT_NAMES.CLICK
                                        .DELIVERY_TOOLTIP}"
                                ></supr-icon>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./product-info.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsProductInfoComponent {
    @Input() sku: Sku;
    @Input() type: string;
    @Input() subscription: Subscription;

    @Output() handleTooltipClick: EventEmitter<void> = new EventEmitter();
}
