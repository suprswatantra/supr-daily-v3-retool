import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Frequency } from "@types";

import { MODAL_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-details-frequency-modal",
    template: `
        <supr-modal
            *ngIf="showModal"
            modalName="${MODAL_NAMES.REPEAT}"
            (handleClose)="handleModalClose.emit()"
        >
            <supr-subscription-create-frequency-modal
                [skuId]="skuId"
                [loading]="loading"
                [frequency]="frequency"
                [saObjectValue]="saObjectValue"
                (handleFrequencyConfirmation)="
                    handleFrequencyChange.emit($event)
                "
            ></supr-subscription-create-frequency-modal>
        </supr-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsFrequencyModalComponent {
    @Input() skuId: number;
    @Input() loading = false;
    @Input() showModal = false;
    @Input() frequency: Frequency;

    @Input() saObjectValue: number;

    @Output() handleFrequencyChange: EventEmitter<
        Frequency
    > = new EventEmitter();

    @Output() handleModalClose: EventEmitter<void> = new EventEmitter();
}
