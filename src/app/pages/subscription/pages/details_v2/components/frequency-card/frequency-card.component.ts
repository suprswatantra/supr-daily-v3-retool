import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { WEEK_DAYS_SELECTION_FORMAT, DAY_NAMES_MAP } from "@constants";

import { UtilService } from "@services/util/util.service";

import { Frequency } from "@types";

import { TEXTS } from "../../constants/";

@Component({
    selector: "supr-subscription-details-frequency",
    template: `
        <div class="card">
            <div class="cardHeader">
                <supr-text type="paragraph">${TEXTS.REPEAT}</supr-text>
            </div>
            <div class="suprColumn left bottom cardContent" *ngIf="frequency">
                <ng-container *ngIf="!_isDaily">
                    <supr-text class="frequencyLabel" type="subheading">
                        {{ _frequencyText }}
                    </supr-text>
                    <div class="divider4"></div>
                </ng-container>
                <div class="suprRow left center">
                    <supr-text
                        class="frequencyLabel"
                        [class.daily]="_isDaily"
                        *ngIf="_isDaily"
                        type="subheading"
                    >
                        {{ _frequencyText }}
                    </supr-text>
                    <ng-container *ngIf="!_isDaily">
                        <ng-container
                            *ngFor="
                                let day of _selectedDays;
                                trackBy: trackByFn
                            "
                        >
                            <supr-text class="frequencyDetail" type="paragraph">
                                {{ day | titlecase }}
                            </supr-text>
                            <div class="spacer8"></div>
                        </ng-container>
                    </ng-container>
                    <supr-icon
                        *ngIf="!disabled"
                        name="chevron_right"
                    ></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: [
        "../../styles/info-card.component.scss",
        "./frequency-card.component.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsFrequencyComponent {
    @Input() disabled = false;
    @Input() set frequency(data: Frequency) {
        this._frequency = data;
        this.setSelectedDays();
        this.setFrequencyText();
    }
    get frequency(): Frequency {
        return this._frequency;
    }

    _isDaily = true;
    _frequencyText: string;
    _selectedDays: string[] = [];

    private _frequency: Frequency;

    constructor(private utilService: UtilService) {}

    trackByFn(index: number) {
        return index;
    }

    private setFrequencyText() {
        if (this.frequency) {
            const frequencyType = this.utilService.getWeekDaysFormatFromDays(
                this.frequency
            );

            this._isDaily = frequencyType === WEEK_DAYS_SELECTION_FORMAT.DAILY;
            this._frequencyText = this._isDaily ? "Daily" : "Weekly every";
        }
    }

    private setSelectedDays() {
        if (this.frequency) {
            let hasSunday = false;

            this._selectedDays = [];

            for (const day in this.frequency) {
                if (this.frequency[day]) {
                    if (
                        day.toLowerCase() ===
                        DAY_NAMES_MAP.SUNDAY.SHORT.toLowerCase()
                    ) {
                        hasSunday = true;
                    } else {
                        this._selectedDays.push(day);
                    }
                }
            }

            if (hasSunday) {
                this._selectedDays.push(
                    DAY_NAMES_MAP.SUNDAY.SHORT.toLowerCase()
                );
            }
        }
    }
}
