import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { Subscription } from "@models";

import { RouterService } from "@services/util/router.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { Segment } from "@types";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SUBSCRIPTION_LIST_TYPES } from "@pages/subscription/pages/list_v2/constants";

@Component({
    selector: "supr-subscription-details-footer",
    template: `
        <supr-sticky-wrapper>
            <div class="footer" [ngClass]="type">
                <div *ngIf="type === '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'">
                    <supr-text class="footerText wrap" type="body">
                        ${TEXTS.SUBSCRIPTION_ENDING_MSG}
                    </supr-text>
                    <div class="divider20"></div>
                </div>
                <div *ngIf="type === '${SUBSCRIPTION_LIST_TYPES.EXPIRED}'">
                    <supr-text class="footerText wrap" type="body">
                        ${TEXTS.SUBSCRIPTION_RESTART_MSG_1}
                    </supr-text>
                    <supr-text class="footerText wrap" type="body">
                        ${TEXTS.SUBSCRIPTION_RESTART_MSG_2}
                    </supr-text>
                    <div class="divider20"></div>
                </div>

                <supr-button
                    *ngIf="
                        type && type !== '${SUBSCRIPTION_LIST_TYPES.EXPIRED}'
                    "
                    class="actionBtn"
                    [class.recharge]="
                        type === '${SUBSCRIPTION_LIST_TYPES.RECHARGE}'
                    "
                    [saContextList]="saContextList"
                    [saObjectValue]="subscription?.id"
                    [saContext]="subscription?.sku_data?.id"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.RECHARGE}"
                    (handleClick)="goToRechargePage()"
                >
                    <div class="suprRow center">
                        <supr-icon name="recharge_sub"></supr-icon>
                        <div class="spacer8"></div>
                        <supr-text type="action">RECHARGE</supr-text>
                    </div>
                </supr-button>
                <supr-button
                    *ngIf="type === '${SUBSCRIPTION_LIST_TYPES.EXPIRED}'"
                    class="actionBtn"
                    (handleClick)="goToRechargePage()"
                    [saContextList]="saContextList"
                    [saObjectValue]="subscription?.id"
                    [saContext]="subscription?.sku_data?.id"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.RESTART}"
                >
                    <div class="suprRow center">
                        <supr-icon name="recharge_sub"></supr-icon>
                        <div class="spacer8"></div>
                        <supr-text type="action">RESTART</supr-text>
                    </div>
                </supr-button>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["./footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsStatusFooterComponent {
    @Input() type: string;
    @Input() subscription: Subscription;

    @Input() saContextList: Segment.ContextListItem[] = [];

    expireDaysMessage: string;

    constructor(
        private routerService: RouterService,
        private blockAccessService: BlockAccessService
    ) {}

    goToRechargePage() {
        /* Block recharge page route in case of access restrictions */
        if (this.blockAccessService.isSubscriptionAccessRestricted()) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.routerService.goToSubscriptionRechargePage(this.subscription.id);
    }
}
