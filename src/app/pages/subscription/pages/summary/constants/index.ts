export const TEXTS = {
    RESTART: "RESTART SUBSCRIPTION",
    SCHEDULE_DELIVERY: "SCHEDULE DELIVERY",
    SUMMARY_PAGE_HEADER: "Delivery schedule",
    NO_UPCOMING_ORDERS_HEADER:
        "This subscription has ended, there are no upcoming deliveries",
};

export const TABS = {
    SCHEDULE: {
        type: "schedule",
        label: "Upcoming",
    },

    TRANSACTIONS: {
        type: "transactions",
        label: "Past transactions",
    },
};
