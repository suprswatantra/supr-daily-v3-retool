export const SA_OBJECT_NAMES = {
    CLICK: {
        PAST_TAB: "past",
        RESTART: "restart",
        SCHEDULE: "schedule",
        UPCOMING_TAB: "upcoming",
        EDIT_DELIVERY: "edit-delivery",
    },
};
