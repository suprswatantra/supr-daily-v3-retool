import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { MODAL_NAMES } from "@constants";

import {
    Schedule,
    Subscription,
    ScheduleItem,
    SubscriptionTransaction,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-summary-list",
    template: `
        <ng-container *ngIf="history; else scheduleView">
            <div
                class="wrapper"
                *ngIf="!loading && transactions && transactions.length"
            >
                <ng-container
                    *ngFor="
                        let transaction of transactions;
                        trackBy: trackByFn;
                        last as isLast
                    "
                >
                    <supr-subscription-schedule-tile
                        [history]="history"
                        [transaction]="transaction"
                        [subscription]="subscription"
                    ></supr-subscription-schedule-tile>

                    <div class="divider16" *ngIf="!isLast"></div>
                </ng-container>
            </div>
        </ng-container>

        <ng-template #scheduleView>
            <div
                class="wrapper"
                *ngIf="!loading && schedules && schedules.length"
            >
                <ng-container
                    *ngFor="
                        let schedule of schedules;
                        trackBy: trackByFn;
                        last as isLast
                    "
                >
                    <supr-subscription-schedule-tile
                        [history]="history"
                        [schedule]="schedule"
                        [subscription]="subscription"
                        [saObjectValue]="subscription?.id"
                        [saContext]="subscription?.sku_data?.id"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.EDIT_DELIVERY}"
                        (handleEditClick)="handleEditClick(schedule)"
                    ></supr-subscription-schedule-tile>

                    <div class="divider16" *ngIf="!isLast"></div>
                </ng-container>
            </div>
        </ng-template>

        <ng-container *ngIf="loading">
            <supr-subscription-summary-loader>
            </supr-subscription-summary-loader>
        </ng-container>

        <supr-modal
            *ngIf="_showModal && _activeScheduleItem"
            [saObjectValue]="subscription?.id"
            modalName="${MODAL_NAMES.EDIT_SUBSCRIPTION_DELIVERY}"
            (handleClose)="closeEditModal()"
        >
            <supr-subscription-edit-card-container
                [date]="_activeDate"
                [subscription]="subscription"
                [sku]="subscription?.sku_data"
                [instantRefreshSchedule]="true"
                [schedule]="_activeScheduleItem"
            ></supr-subscription-edit-card-container>
        </supr-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryListComponent {
    @Input() loading = true;
    @Input() history = false;
    @Input() subscription: Subscription;
    @Input() transactions: SubscriptionTransaction[];
    @Input() set schedules(data: Schedule[]) {
        this.setVisibleSchedules(data);
    }
    get schedules(): Schedule[] {
        return this._schedules;
    }

    _showModal = false;
    _activeDate: string;
    _schedules: Schedule[];
    _activeScheduleItem: ScheduleItem;

    constructor(
        private utilService: UtilService,
        private scheduleService: ScheduleService
    ) {}

    openEditModal() {
        this._showModal = true;
    }

    closeEditModal() {
        this._showModal = false;
    }

    handleEditClick(schedule: Schedule) {
        this._activeDate = schedule && schedule.date;
        this._activeScheduleItem = this.utilService.getNestedValue(
            schedule,
            "subscriptions.0"
        );

        this.openEditModal();
    }

    private setVisibleSchedules(schedules: Schedule[]) {
        let filteredSchedules: Schedule[] = [];

        if (this.utilService.isLengthyArray(schedules)) {
            filteredSchedules = schedules.filter((schedule: Schedule) =>
                this.scheduleService.hasScheduledDeliveries(schedule)
            );
        }

        this._schedules = filteredSchedules;
    }
}
