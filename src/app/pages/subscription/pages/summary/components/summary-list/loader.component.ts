import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-subscription-summary-loader",
    template: `
        <ion-skeleton-text animated class="header"></ion-skeleton-text>
        <div class="divider16"></div>
        <ng-container *ngFor="let loader of loaders">
            <ion-skeleton-text animated class="first"></ion-skeleton-text>
            <div class="divider12"></div>
            <ion-skeleton-text animated class="second"></ion-skeleton-text>
            <div class="divider12"></div>
            <ion-skeleton-text animated class="third"></ion-skeleton-text>
            <div class="divider24"></div>
        </ng-container>
    `,
    styleUrls: ["./loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionSummaryLoader {
    loaders = [1, 2, 3, 4, 5, 6];
}
