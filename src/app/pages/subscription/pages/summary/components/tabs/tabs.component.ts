import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TABS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-subscription-summary-tabs",
    template: `
        <ng-container>
            <div class="suprRow spaceBetween">
                <supr-chip
                    saClick
                    class="suprColumn"
                    [class.active]="activeTab === '${TABS.SCHEDULE.type}'"
                    (handleClick)="
                        toggleActiveTab.emit('${TABS.SCHEDULE.type}')
                    "
                    [saContext]="saContext"
                    [saObjectValue]="saObjectValue"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.UPCOMING_TAB}"
                >
                    <supr-text type="body">
                        ${TABS.SCHEDULE.label}
                    </supr-text>
                </supr-chip>
                <supr-chip
                    saClick
                    class="suprColumn"
                    [class.active]="activeTab === '${TABS.TRANSACTIONS.type}'"
                    (handleClick)="
                        toggleActiveTab.emit('${TABS.TRANSACTIONS.type}')
                    "
                    [saContext]="saContext"
                    [saObjectValue]="saObjectValue"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.PAST_TAB}"
                >
                    <supr-text type="body">
                        ${TABS.TRANSACTIONS.label}
                    </supr-text>
                </supr-chip>
            </div>
        </ng-container>
    `,
    styleUrls: ["./tabs.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsComponent {
    @Input() activeTab: string = TABS.SCHEDULE.type;

    @Input() saContext: number;
    @Input() saObjectValue: number;

    @Output() toggleActiveTab: EventEmitter<string> = new EventEmitter();
}
