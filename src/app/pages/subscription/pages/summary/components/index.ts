import { TabsComponent } from "./tabs/tabs.component";
import { FooterComponent } from "./footer/footer.component";
import { SubscriptionSummaryLoader } from "./summary-list/loader.component";
import { SummaryListComponent } from "./summary-list/summary-list.component";
import { EmptyScheduleComponent } from "./empty-schedule/empty-schedule.component";

export const SummaryPageComponents = [
    TabsComponent,
    FooterComponent,
    SummaryListComponent,
    EmptyScheduleComponent,
    SubscriptionSummaryLoader,
];
