import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants";
import { TEXTS as CREATE_PAGE_TEXTS } from "@pages/subscription/pages/create/constants";

@Component({
    selector: "supr-subscription-summary-empty-schedule",
    template: `
        <ng-container>
            <div class="headerText">
                <supr-text type="caption">
                    ${TEXTS.NO_UPCOMING_ORDERS_HEADER}
                </supr-text>
            </div>
            <div class="image suprColumn">
                <supr-svg></supr-svg>
            </div>
            <div class="descriptionWrapper">
                <div class="suprRow top header">
                    <div>
                        <supr-text type="subtitle">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.HEADER_1}
                        </supr-text>
                        <supr-text type="subtitle">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.HEADER_2}
                        </supr-text>
                    </div>
                    <supr-svg></supr-svg>
                </div>
                <div class="divider20"></div>
                <div class="suprRow top descriptionItem">
                    <supr-icon name="settings"></supr-icon>
                    <div class="spacer12"></div>
                    <div>
                        <supr-text type="paragraph">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.DESC_HEADING_2}
                        </supr-text>
                        <div class="divider4"></div>
                        <supr-text type="caption">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.DESC_CONTENT_2}
                        </supr-text>
                    </div>
                </div>
                <div class="divider12"></div>
                <div class="suprRow top descriptionItem">
                    <supr-icon name="calendar"></supr-icon>
                    <div class="spacer12"></div>
                    <div>
                        <supr-text type="paragraph">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.DESC_HEADING_3}
                        </supr-text>
                        <div class="divider4"></div>
                        <supr-text type="caption">
                            ${CREATE_PAGE_TEXTS.DESCRIPTION.DESC_CONTENT_3}
                        </supr-text>
                    </div>
                </div>
            </div>
        </ng-container>
    `,
    styleUrls: ["./empty-schedule.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyScheduleComponent {}
