import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TEXTS } from "../../constants/";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SUBSCRIPTION_LIST_TYPES } from "@pages/subscription/pages/list_v2/constants";

@Component({
    selector: "supr-subscription-summary-footer",
    template: `
        <supr-sticky-wrapper>
            <div class="footer">
                <supr-button
                    *ngIf="
                        type === '${SUBSCRIPTION_LIST_TYPES.EXPIRED}';
                        else scheduleBtn
                    "
                    (handleClick)="handleRestartClick.emit()"
                    [saContext]="saContext"
                    [saObjectValue]="saObjectValue"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.RESTART}"
                >
                    <supr-text type="action">
                        ${TEXTS.RESTART}
                    </supr-text>
                </supr-button>
                <ng-template #scheduleBtn>
                    <supr-button
                        (handleClick)="handleScheduleClick.emit()"
                        [saContext]="saContext"
                        [saObjectValue]="saObjectValue"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.SCHEDULE}"
                    >
                        <supr-text type="action">
                            ${TEXTS.SCHEDULE_DELIVERY}
                        </supr-text>
                    </supr-button>
                </ng-template>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["./footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {
    @Input() type: string;
    @Input() saContext: number;
    @Input() saObjectValue: number;

    @Output() handleScheduleClick: EventEmitter<void> = new EventEmitter();
    @Output() handleRestartClick: EventEmitter<void> = new EventEmitter();
}
