import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";
import { SubscriptionCommonModule } from "@pages/subscription/common.module";

import { SummaryPageComponents } from "./components";
import { SubscriptionSummaryPageContainer } from "./containers/summary.container";
import { SubscriptionSummaryPageLayoutComponent } from "./layouts/summary.layout";

const routes: Routes = [
    {
        path: "",
        component: SubscriptionSummaryPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        SubscriptionCommonModule,
    ],
    declarations: [
        ...SummaryPageComponents,
        SubscriptionSummaryPageContainer,
        SubscriptionSummaryPageLayoutComponent,
    ],
})
export class SubscriptionSummaryPageModule {}
