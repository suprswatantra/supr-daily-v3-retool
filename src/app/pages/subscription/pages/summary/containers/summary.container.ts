import { Component, ChangeDetectionStrategy } from "@angular/core";

import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { Subscription, Schedule, SubscriptionTransaction } from "@models";

import { ScheduleCurrentState } from "@store/schedule/schedule.state";

import { FetchScheduleActionPayload } from "@types";

import { SubscriptionPageAdapter as Adapter } from "@pages/subscription/services/subscription.adapter";

@Component({
    selector: "supr-subscription-summary-container",
    template: `
        <supr-subscription-summary-layout
            [activeTab]="activeTab$ | async"
            [schedules]="schedules$ | async"
            [subscription]="subscription$ | async"
            [transactions]="transactions$ | async"
            [scheduleState]="scheduleCurrentState$ | async"
            (fetchSchedule)="fetchSchedule($event)"
        ></supr-subscription-summary-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionSummaryPageContainer {
    activeTab$: Observable<string>;
    schedules$: Observable<Schedule[]>;
    subscription$: Observable<Subscription>;
    transactions$: Observable<SubscriptionTransaction[]>;
    scheduleCurrentState$: Observable<ScheduleCurrentState>;

    constructor(
        private adapter: Adapter,
        private scheduleAdapter: ScheduleAdapter
    ) {
        this.transactions$ = this.adapter.transactions$;
        this.subscription$ = this.adapter.subscription$;
        this.schedules$ = this.adapter.getSubscriptionSchedule();
        this.scheduleCurrentState$ = this.scheduleAdapter.scheduleCurrentState$;
        this.activeTab$ = this.adapter.queryParams$.pipe(
            map((params: Params) => params["active"])
        );
    }

    fetchSchedule(data: FetchScheduleActionPayload) {
        this.scheduleAdapter.getSchedule(data);
    }
}
