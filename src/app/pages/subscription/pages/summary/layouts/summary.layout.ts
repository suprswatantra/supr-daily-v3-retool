import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    Schedule,
    Subscription,
    SubscriptionTransaction,
} from "@shared/models";

import { RouterService } from "@services/util/router.service";
import { SubscriptionService } from "@services/shared/subscription.service";

import { ScheduleCurrentState } from "@store/schedule/schedule.state";

import { TEXTS, TABS } from "../constants";

@Component({
    selector: "supr-subscription-summary-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.SUMMARY_PAGE_HEADER}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-subscription-summary-tabs
                    [activeTab]="activeTab"
                    [saObjectValue]="subscription?.id"
                    [saContext]="subscription?.sku_data?.id"
                    (toggleActiveTab)="toggleActiveTab($event)"
                >
                </supr-subscription-summary-tabs>

                <div class="divider18"></div>

                <supr-subscription-summary-list
                    *ngIf="activeTab === '${TABS.SCHEDULE.type}'"
                    [schedules]="schedules"
                    [loading]="_loadingSchedules"
                    [subscription]="subscription"
                ></supr-subscription-summary-list>

                <supr-subscription-summary-empty-schedule
                    *ngIf="
                        !_loadingSchedules &&
                        !schedules?.length &&
                        activeTab === '${TABS.SCHEDULE.type}'
                    "
                >
                </supr-subscription-summary-empty-schedule>

                <supr-subscription-summary-list
                    *ngIf="activeTab === '${TABS.TRANSACTIONS.type}'"
                    [history]="true"
                    [transactions]="transactions"
                    [subscription]="subscription"
                    [loading]="_loadingTransactions"
                ></supr-subscription-summary-list>

                <supr-subscription-delivery-schedule-container
                    *ngIf="_showScheduleModal"
                    [subscription]="subscription"
                    instantRefreshSchedule="true"
                    [showModal]="_showScheduleModal"
                    (handleModalClose)="closeScheduleModal()"
                ></supr-subscription-delivery-schedule-container>
            </div>
            <supr-subscription-summary-footer
                [type]="_subscriptionType"
                [saObjectValue]="subscription?.id"
                [saContext]="subscription?.sku_data?.id"
                (handleRestartClick)="goToRestartPage()"
                (handleScheduleClick)="openScheduleModal()"
            >
            </supr-subscription-summary-footer>
        </div>
    `,
    styleUrls: ["../styles/summary.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionSummaryPageLayoutComponent
    implements OnInit, OnChanges {
    @Input() schedules: Schedule[];
    @Input() subscription: Subscription;
    @Input() scheduleState: ScheduleCurrentState;
    @Input() activeTab: string = TABS.SCHEDULE.type;
    @Input() transactions: SubscriptionTransaction[];

    @Output() fetchSchedule: EventEmitter<{
        subscriptionId: number;
    }> = new EventEmitter();

    _loadingSchedules = true;
    _subscriptionType: string;
    _showScheduleModal = false;
    _loadingTransactions = false;

    constructor(
        private routerService: RouterService,
        private subscriptionService: SubscriptionService
    ) {}

    ngOnInit() {
        if (this.subscription) {
            this.setType();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleScheduleStateChange(changes["scheduleState"]);
    }

    toggleActiveTab(tab: string) {
        this.activeTab = tab;
    }

    openScheduleModal() {
        this._showScheduleModal = true;
    }

    closeScheduleModal() {
        this._showScheduleModal = false;
    }

    goToRestartPage() {
        this.routerService.goToSubscriptionRechargePage(this.subscription.id);
    }

    private setType() {
        this._subscriptionType = this.subscriptionService.getSubscriptionType(
            this.subscription
        );
    }

    private handleScheduleStateChange(change: SimpleChange) {
        if (!change) {
            return;
        }

        switch (change.currentValue) {
            case ScheduleCurrentState.FETCHING_SCHEDULE:
                this._loadingSchedules = true;
                break;
            default:
                this._loadingSchedules = false;
                break;
        }
    }
}
