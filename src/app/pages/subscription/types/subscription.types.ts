import { Subscription } from "@models";

export interface SubscriptionSplit {
    active: Subscription[];
    expired: Subscription[];
}
