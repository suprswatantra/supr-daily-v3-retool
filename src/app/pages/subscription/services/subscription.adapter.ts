import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Sku, Schedule } from "@models";

import {
    StoreState,
    SkuStoreSelectors,
    SubscriptionStoreSelectors,
    SubscriptionStoreActions,
    ScheduleStoreSelectors,
    RouterStoreSelectors,
    SuprPassStoreSelectors,
    SuprPassStoreActions,
    WalletStoreActions,
    WalletStoreSelectors,
} from "@supr/store";

import { SuprApi } from "@types";

@Injectable()
export class SubscriptionPageAdapter {
    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    subscriptions$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    suprPassInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassInfo)
    );

    autoDebitPaymentModes$ = this.store.pipe(
        select(WalletStoreSelectors.selectAutoDebitPaymentModes)
    );

    subscription$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionByUrlParams)
    );

    subscriptionState$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionState)
    );

    subscriptionError$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionError)
    );

    transactions$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionTransactions)
    );

    constructor(private store: Store<StoreState>) {}

    fetchSubscriptions() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }

    fetchSuprPassInfo() {
        this.store.dispatch(
            new SuprPassStoreActions.FetchSuprPassRequestAction({
                silent: false,
            })
        );
    }

    fetchAutoDebitPaymentModes() {
        this.store.dispatch(
            new WalletStoreActions.FetchAutoDebitPaymentModesInfoRequestAction({
                silent: true,
            })
        );
    }

    getSkuById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, { skuId })
        );
    }

    cancelSubscription(subscriptionId: number) {
        this.store.dispatch(
            new SubscriptionStoreActions.CancelSubscriptionRequestAction({
                subscriptionId,
            })
        );
    }

    getSubscriptionSchedule(): Observable<Schedule[]> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectSubscriptionScheduleByUrlParams)
        );
    }

    updateSubscription(
        subscriptionId: number,
        subscriptionData: SuprApi.SubscriptionBody,
        instantRefreshSchedule?: boolean
    ) {
        this.store.dispatch(
            new SubscriptionStoreActions.UpdateSubscriptionRequestAction({
                subscriptionId,
                body: subscriptionData,
                instantRefreshSchedule,
            })
        );
    }

    fetchSubscriptionTransactions(subscriptionId: number) {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubTransactionRequestAction(
                subscriptionId
            )
        );
    }
}
