import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

@Component({
    selector: "supr-delivery-proof-container",
    template: ` <supr-delivery-proof-layout
        [slot]="slot$ | async"
        [imageUrl]="imageUrl$ | async"
    ></supr-delivery-proof-layout>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProofContainer implements OnInit {
    slot$: Observable<string>;
    imageUrl$: Observable<string>;

    constructor(private scheduleAdapter: ScheduleAdapter) {}

    ngOnInit() {
        this.imageUrl$ = this.scheduleAdapter.deliveryImage$;
        this.slot$ = this.scheduleAdapter.queryParams$.pipe(
            map((params: Params) => params["slot"])
        );
    }
}
