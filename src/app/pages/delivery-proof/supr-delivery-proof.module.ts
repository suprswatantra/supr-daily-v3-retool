import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { DeliveryProofLayoutComponent } from "./layouts/delivery-proof.layout";
import { DeliveryProofContainer } from "./containers/delivery-proof.container";

const routes: Routes = [
    {
        path: "",
        component: DeliveryProofContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [DeliveryProofContainer, DeliveryProofLayoutComponent],
    providers: [],
})
export class DeliveryProofModule {}
