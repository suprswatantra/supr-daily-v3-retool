import { Input, Component, ChangeDetectionStrategy } from "@angular/core";
import { RouterService } from "@services/util/router.service";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-delivery-proof-layout",
    template: `
        <supr-page-header>
            <supr-text type="subheading">
                Delivery photo(s)
            </supr-text>
        </supr-page-header>

        <div class="suprScrollContent">
            <supr-image useDirectUrl="true" [src]="imageUrl"></supr-image>
        </div>

        <supr-page-footer>
            <div class="suprRow spaceBetween">
                <supr-button
                    class="back"
                    [saContext]="slot"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.CLOSE}"
                    (handleClick)="goBack()"
                >
                    <supr-text type="action14">CLOSE</supr-text>
                </supr-button>
                <supr-button
                    [saContext]="slot"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.GET_HELP}"
                    (handleClick)="goToSupportPage()"
                >
                    <supr-text type="action14">GET HELP</supr-text>
                </supr-button>
            </div>
        </supr-page-footer>
    `,
    styleUrls: ["../styles/delivery-proof.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryProofLayoutComponent {
    @Input() slot: string;
    @Input() imageUrl: string;

    constructor(private routerService: RouterService) {}

    goBack() {
        this.routerService.goBack();
    }

    goToSupportPage() {
        this.routerService.goToSupportPage();
    }
}
