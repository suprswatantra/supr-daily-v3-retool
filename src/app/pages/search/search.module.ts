import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

import { SharedModule } from "@shared/shared.module";

import { SearchPageContainer } from "./containers/search.container";
import { SearchPageLayoutComponent } from "./layouts/search.layout";

import { searchComponents } from "./components";
import { searchContainers } from "./containers";
import { searchServices } from "./services";

const routes: Routes = [
    {
        path: "",
        component: SearchPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        InfiniteScrollModule,
    ],
    declarations: [
        SearchPageLayoutComponent,
        ...searchComponents,
        ...searchContainers,
    ],
    providers: [...searchServices],
})
export class SearchPageModule {}
