export const DEFAULT_SEARCH_SCORE_PARAMS = {
    fullMatchWeight: 10,
    prefixMatchWeight: 8,
    partialMatchWeight: 5,
    skuScoreMergeType: "sum",
};

export const SEARCH_TERMS_WEIGHTS = {
    SKU: 10,
    SUB_CATEGORY: 7,
    CATEGORY: 5,
};

export const TEXTS = {
    ADD_EXISTING_SUBS: "Add from active subscriptions",
    CHOOSE_DELIVERY_DATE: "Choose delivery date",
    CONFIRM_DATE: "Confirm date",
    SCHEDULE: "Schedule",
    DELIVER_ONCE: "Deliver once",
    FILTER_BY_CATEGORY: "Filter by category",
    POPULAR_SEARCHES: "Popular searches",
    PAST_ORDERS: "Quickly add items from past",
};

export const SEARCH_START_MIN_CHAR_LENGTH = 1;

export const SEARCH_STOP_WORDS = ["the", "an"];

export const SEARCH_PRODUCT_PLACEHOLDER = "Search";

export { REQUEST_PRODUCT_TYPE, REQUEST_PRODUCT } from "./request.product";

export const ALL_CATEGORY_FILTER_CHIP = {
    id: -1,
    name: "All categories",
    image: {
        fullUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Logo-landing-page.png",
    },
};
