export const SA_OBJECT_NAMES = {
    ADD_FROM_SUBSCRIPTION: "add-from-subscription",
    MANAGE_SUBSCRIPTION: "manage-subscription",
    CHANGE_DATE: "change-date",
    ADD_PRODUCT: "add-product",
    POPULAR_SEARCH_CHIP: "popular-search",
    CATEGORY_FILTER: "category-filter",
    SUBCATEGORY_LINK: "subcategory-link",
};

export const MODAL_NAMES = {
    ADD_FROM_SUBSCRIPTION: "add-from-subscription-modal",
};

export const CUSTOM_EVENTS = {
    SEARCH_INITIATED: "search-initiated",
    SEARCH_COMPLETED: "search-completed",
};
