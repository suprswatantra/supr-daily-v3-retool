export const REQUEST_PRODUCT_TYPE = "Product";

export const REQUEST_PRODUCT = {
    enableRequestProduct: true,
    noResults: "No results found for ",
    title: "Looks like we don't have this product listed yet",
    subTitle:
        "Tell us what you are looking for and we will do our best to list them at the earliest",
    requestButtonText: "Request a product",
    header: "Request a product(s)",
    lable: "Give us some details of the product(s) you want on Supr Daily",
    textTitle: "Describe the product(s)",
    placeholder: "Type here",
    success:
        "Your request has been submitted successfully, We are working on making this product available on Supr Daily. Thank you.",
    submit: "Submit",
    error: "Please enter valid product details",
    suggestion: "e.g. Amul Taaza 1ltr",
};
