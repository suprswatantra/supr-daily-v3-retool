import { SearchPageContainer } from "./search.container";
import { SearchContentContainer } from "./content.container";
import { SearchResultsContainer } from "./search-results.container";
import { SubscribedProductTileContainer } from "./subscribed-product-tile.container";
import { SearchProductTileDepContainer } from "./product-tile-dep.container";
import { PastOrdersContainer } from "./past-orders.container";
import { PopularContainer } from "./popular.container";
import { SearchProductTileContainer } from "./product-tile.container";
import { CategoryChipContainer } from "./category-chip.container";

export const searchContainers = [
    SearchPageContainer,
    SearchContentContainer,
    SubscribedProductTileContainer,
    SearchProductTileDepContainer,
    SearchResultsContainer,
    PastOrdersContainer,
    PopularContainer,
    SearchProductTileContainer,
    CategoryChipContainer,
];
