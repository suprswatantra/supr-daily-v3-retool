import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { Sku, Subscription } from "@models";
import { ActiveSubscriptionBalance } from "../types";

@Component({
    selector: "supr-subscribed-product-tile-container",
    template: `
        <supr-subscribed-product-tile
            [sku]="sku"
            [subscription]="subscriptionItem"
            (handleTileClick)="handleTileClick.emit($event)"
        ></supr-subscribed-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscribedProductTileContainer implements OnInit {
    @Input() skuId: number;
    @Input() subscriptionItem: Subscription;

    @Output() handleTileClick: EventEmitter<
        ActiveSubscriptionBalance
    > = new EventEmitter();

    sku: Sku;

    ngOnInit() {
        this.sku = this.subscriptionItem.sku_data;
    }
}
