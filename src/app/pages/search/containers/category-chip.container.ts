import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { CategoryMeta } from "@models";

import { CategoryAdapter } from "@shared/adapters";

@Component({
    selector: "supr-search-category-chip-container",
    template: `
        <supr-search-category-chip
            [selected]="selected"
            [saContext]="saContext"
            [category]="category$ | async"
            (handleClick)="handleClick.emit($event)"
        ></supr-search-category-chip>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryChipContainer implements OnInit {
    @Input() saContext: string;
    @Input() selected: boolean;
    @Input() category: CategoryMeta;

    @Output() handleClick: EventEmitter<CategoryMeta> = new EventEmitter();

    category$: Observable<CategoryMeta>;

    constructor(private categoryAdapter: CategoryAdapter) {}

    ngOnInit() {
        if (this.category && !isNaN(this.category.id)) {
            this.category$ = this.categoryAdapter.getCategoryMetaById(
                this.category.id
            );
        }
    }
}
