import { OnInit, Component, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter } from "@shared/adapters";

@Component({
    selector: "supr-search-past-orders-container",
    template: `
        <supr-search-past-orders [skuList]="pastOrderSkuList$ | async">
        </supr-search-past-orders>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PastOrdersContainer implements OnInit {
    pastOrderSkuList$: Observable<Array<number>>;

    constructor(private cartAdapter: CartAdapter) {}

    ngOnInit() {
        this.pastOrderSkuList$ = this.cartAdapter.pastOrderSkuListOriginal$;
    }
}
