import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { SearchPageAdapter as Adapter } from "../services/search.adapter";

@Component({
    selector: "supr-search-container",
    template: `
        <supr-search-layout
            [isExperimentsFetched]="isExperimentsFetched$ | async"
        ></supr-search-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageContainer implements OnInit {
    isExperimentsFetched$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isExperimentsFetched$ = this.adapter.isExperimentsFetched$;
    }
}
