import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import {
    SEARCH_INSTRUMENTATION_EVENT_TYPE,
    SEARCH_INSTRUMENTATION_EVENT_NAME,
} from "@constants";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { CartAdapter } from "@shared/adapters/cart.adapter";

import {
    Sku,
    CartItem,
    SearchSku,
    SearchInstrumentation,
    SkuAttributes,
} from "@models";

import { UtilService } from "@services/util/util.service";

import { SearchPageAdapter } from "../services/search.adapter";

@Component({
    selector: "supr-search-product-tile-container",
    template: `
        <supr-search-product-tile
            [sku]="sku$ | async"
            [saContext]="searchId"
            [searchTerm]="searchTerm"
            [saPosition]="_saPosition"
            [headerText]="_headerText"
            [headerLink]="_headerLink"
            [cartItem]="cartItem$ | async"
            [skuAttributes]="skuAttributes$ | async"
            [subcategoryId]="_subcategoryId"
            [skuNotified]="skuNotified$ | async"
            [alternatesV2Experiment]="alternatesV2Experiment"
            (handleAddToCartInstrumentation)="handleAddToCartInstrumentation()"
            (handleSubscribeInstrumentation)="handleSubscribeInstrumentation()"
            (handleSubModalInstrumentation)="
                handleSubModalInstrumentation($event)
            "
        ></supr-search-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchProductTileContainer implements OnInit {
    @Input() searchId: string;
    @Input() searchTerm: string;
    @Input() searchFiltersEnabled: boolean;
    @Input() alternatesV2Experiment = true;
    @Input() set searchResult(data: SearchSku) {
        this._searchResult = data;
        this.setAdditionalData();
    }
    get searchResult(): SearchSku {
        return this._searchResult;
    }

    _skuId: number;
    _objectId: string;
    _headerLink: string;
    _headerText: string;
    _saPosition: number;
    _subcategoryId: number;

    sku$: Observable<Sku>;
    cartItem$: Observable<CartItem>;
    skuNotified$: Observable<string>;
    skuAttributes$: Observable<SkuAttributes>;

    private _searchResult: SearchSku;

    constructor(
        private skuAdapter: SkuAdapter,
        private cartAdapter: CartAdapter,
        private utilService: UtilService,
        private searchPageAdapter: SearchPageAdapter
    ) {}

    ngOnInit() {
        if (this._skuId) {
            this.sku$ = this.skuAdapter.getSkuDetails(this._skuId);
            this.skuNotified$ = this.skuAdapter.isNotified(this._skuId);
            this.cartItem$ = this.cartAdapter.getCartItemById(this._skuId);
            this.skuAttributes$ = this.skuAdapter.getSkuAttributes(this._skuId);
        }
    }

    handleAddToCartInstrumentation() {
        if (this._skuId) {
            const data = this.getSearchInstrumentationData();

            this.searchPageAdapter.sendAddToCartInstrumentation(
                data,
                this._skuId
            );
        }
    }

    handleSubscribeInstrumentation() {
        if (this._skuId) {
            const data = this.getSearchInstrumentationData();

            this.searchPageAdapter.saveSearchContextForSubscribe(
                data,
                this._skuId
            );
        }
    }

    handleSubModalInstrumentation(state: boolean) {
        if (this._skuId) {
            if (state) {
                const data = {
                    queryID: this.searchId,
                    search_objectIDs: [this._objectId],
                    search_positions: [this._saPosition],
                    eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CONVERSION,
                    search_eventName:
                        SEARCH_INSTRUMENTATION_EVENT_NAME.CONVERSION,
                };

                this.searchPageAdapter.saveSearchContextForSubscribe(
                    data,
                    this._skuId
                );
            } else {
                this.searchPageAdapter.clearSearchContext(this._skuId);
            }
        }
    }

    private setAdditionalData() {
        this._skuId = this.utilService.getNestedValue(
            this._searchResult,
            "sku_id"
        );
        this._objectId = this.utilService.getNestedValue(
            this._searchResult,
            "object_id"
        );
        this._saPosition = this.utilService.getNestedValue(
            this._searchResult,
            "search_position"
        );

        if (this.searchFiltersEnabled) {
            this._headerText = this.utilService.getNestedValue(
                this._searchResult,
                "sub_category_name"
            );
            this._subcategoryId = this.utilService.getNestedValue(
                this._searchResult,
                "sub_category_id"
            );
            this._headerLink = this.generateHeaderLink(this._searchResult);
        }
    }

    private generateHeaderLink(data: SearchSku): string {
        const categoryId = this.utilService.getNestedValue(data, "category_id");
        const subcategoryId = this.utilService.getNestedValue(
            data,
            "sub_category_id"
        );

        const categoryLink = isNaN(categoryId) ? "" : `category/${categoryId}`;
        const subcategoryLink = isNaN(subcategoryId)
            ? ""
            : `?filterId=${subcategoryId}`;

        return `${categoryLink}${subcategoryLink}`;
    }

    private getSearchInstrumentationData(): SearchInstrumentation {
        return {
            queryID: this.searchId,
            search_objectIDs: [this._objectId],
            search_positions: [this._saPosition],
            eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CLICK,
            search_eventName: SEARCH_INSTRUMENTATION_EVENT_NAME.CLICK,
        };
    }
}
