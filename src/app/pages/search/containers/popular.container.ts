import { Output, OnInit, Component, EventEmitter } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SearchAdapter } from "@shared/adapters";

import { PopularSkuSearch } from "@models";

import { SearchCurrentState } from "@store/search/search.state";

@Component({
    selector: "supr-search-popular-container",
    template: `
        <supr-search-popular-searches
            [loading]="loading$ | async"
            [searchQueries]="popularSearches$ | async"
            (handleClick)="handleClick.emit($event)"
            (fetchPopularSearches)="fetchPopularSearches()"
        >
        </supr-search-popular-searches>
    `,
})
export class PopularContainer implements OnInit {
    loading$: Observable<boolean>;
    popularSearches$: Observable<PopularSkuSearch[]>;

    @Output() handleClick: EventEmitter<string> = new EventEmitter();

    constructor(private searchAdapter: SearchAdapter) {}

    ngOnInit() {
        this.loading$ = this.searchAdapter.currentState$.pipe(
            map((state) => state === SearchCurrentState.LOADING_POPULAR)
        );
        this.popularSearches$ = this.searchAdapter.popularSearches$;
    }

    fetchPopularSearches() {
        this.searchAdapter.fetchPopularSearches();
    }
}
