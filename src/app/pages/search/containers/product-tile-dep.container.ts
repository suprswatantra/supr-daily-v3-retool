import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import {
    SEARCH_INSTRUMENTATION_EVENT_TYPE,
    SEARCH_INSTRUMENTATION_EVENT_NAME,
} from "@constants";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { CartAdapter } from "@shared/adapters/cart.adapter";

import { Sku, CartItem, SearchInstrumentation, SkuAttributes } from "@models";

import { SearchPageAdapter } from "../services/search.adapter";

@Component({
    selector: "supr-search-product-tile-dep-container",
    template: `
        <supr-product-tile
            [sku]="sku"
            [saContext]="searchId"
            [saPosition]="saPosition"
            [cartItem]="cartItem$ | async"
            [skuNotified]="skuNotified$ | async"
            [skuAttributes]="skuAttributes$ | async"
            [alternatesV2Experiment]="alternatesV2Experiment"
            (handlePostAddToCart)="handleAddToCartInstrumentation()"
            (handlePostSubscribe)="handleSubscribeInstrumentation()"
            (handleSubModalStateChange)="handleSubModalInstrumentation($event)"
        ></supr-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchProductTileDepContainer implements OnInit {
    @Input() sku: Sku;
    @Input() searchId: string;
    @Input() objectId: string;
    @Input() saPosition: number;
    @Input() alternatesV2Experiment = true;

    cartItem$: Observable<CartItem>;
    skuNotified$: Observable<string>;
    skuAttributes$: Observable<SkuAttributes>;

    constructor(
        private skuAdapter: SkuAdapter,
        private cartAdapter: CartAdapter,
        private searchPageAdapter: SearchPageAdapter
    ) {}

    ngOnInit() {
        if (this.sku) {
            this.skuNotified$ = this.skuAdapter.isNotified(this.sku.id);
            this.cartItem$ = this.cartAdapter.getCartItemById(this.sku.id);
            this.skuAttributes$ = this.skuAdapter.getSkuAttributes(this.sku.id);
        }
    }

    handleAddToCartInstrumentation() {
        if (this.sku && this.sku.id) {
            const data = this.getSearchInstrumentationData();

            this.searchPageAdapter.sendAddToCartInstrumentation(
                data,
                this.sku.id
            );
        }
    }

    handleSubscribeInstrumentation() {
        if (this.sku && this.sku.id) {
            const data = this.getSearchInstrumentationData();

            this.searchPageAdapter.saveSearchContextForSubscribe(
                data,
                this.sku.id
            );
        }
    }

    handleSubModalInstrumentation(state: boolean) {
        if (this.sku && this.sku.id) {
            if (state) {
                const data = {
                    queryID: this.searchId,
                    search_objectIDs: [this.objectId],
                    search_positions: [this.saPosition],
                    eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CONVERSION,
                    search_eventName:
                        SEARCH_INSTRUMENTATION_EVENT_NAME.CONVERSION,
                };

                this.searchPageAdapter.saveSearchContextForSubscribe(
                    data,
                    this.sku.id
                );
            } else {
                this.searchPageAdapter.clearSearchContext(this.sku.id);
            }
        }
    }

    private getSearchInstrumentationData(): SearchInstrumentation {
        return {
            queryID: this.searchId,
            search_objectIDs: [this.objectId],
            search_positions: [this.saPosition],
            eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CLICK,
            search_eventName: SEARCH_INSTRUMENTATION_EVENT_NAME.CLICK,
        };
    }
}
