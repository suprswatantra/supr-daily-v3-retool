import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable, of, Subscription } from "rxjs";
import { map, catchError, finalize } from "rxjs/operators";

import { CategoryAdapter } from "@shared/adapters";

import { SEARCH_RESULT_TYPES } from "@constants";

import {
    Sku,
    SearchSku,
    CategoryMeta,
    SkuSearchResponse,
    SearchCategoryMeta,
} from "@models";

import { UtilService } from "@services/util/util.service";

import { SkuSearchParams } from "@types";

import { SearchPageService } from "../services/search.service";

@Component({
    selector: "supr-search-results-container",
    template: `
        <supr-search-results
            *ngIf="backendSearch; else searchResultsDep"
            [loading]="_loading"
            [searchId]="_searchId"
            [pageSize]="_pageSize"
            [filtering]="_filtering"
            [searchTerm]="searchTerm"
            [categories]="_categories"
            [pageNumber]="_pageNumber"
            [resultType]="_resultType"
            [cartPresent]="cartPresent"
            [loadingMore]="_loadingMore"
            [numberOfHits]="_numberOfHits"
            [numberOfPages]="_numberOfPages"
            [moreResults]="_moreResults"
            [searchResults]="_searchResults"
            [categoryFilter]="_categoryFilter$ | async"
            [searchFiltersEnabled]="searchFiltersEnabled"
            (handleFetchMore)="_fetchMoreResults($event)"
            (handleCategoryChange)="_handleCategoryChange($event)"
            (handleShowRequestModal)="handleShowRequestModal.emit($event)"
        ></supr-search-results>

        <ng-template #searchResultsDep>
            <supr-search-results-dep
                [searchId]="searchId"
                [searchTerm]="searchTerm"
                [searchResults]="searchResults"
                (handleShowRequestModal)="handleShowRequestModal.emit($event)"
            ></supr-search-results-dep>
        </ng-template>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsContainer {
    @Input() searchId: string;
    @Input() cartPresent: boolean;
    @Input() searchResults: Sku[];
    @Input() backendSearch: boolean;
    @Input() searchFiltersEnabled: boolean;
    @Input() set filterId(id: number) {
        this._filterId = id;

        this.setCategoryFilter(id);
    }
    @Input() set searchTerm(query: string) {
        if (this._isSearchTermUpdated(query)) {
            this._setInitialSearchResults(query);
            this.setCategoryFilter(this._filterId);
        }

        this._searchTerm = query;
    }
    get searchTerm(): string {
        return this._searchTerm;
    }

    _pageNumber = 0;
    _loading = false;
    _pageSize: number;
    _searchId: string;
    _filtering = false;
    _loadingMore = false;
    _numberOfHits: number;
    _numberOfPages: number;
    _moreResults: SearchSku[];
    _searchResults: SearchSku[];
    _resultType: SEARCH_RESULT_TYPES;
    _categories: SearchCategoryMeta[];

    _categoryName$: Observable<string>;
    _categoryFilter$: Observable<CategoryMeta>;

    private _filterId: number;
    private _searchTerm: string;
    private searchSubscription: Subscription;

    @Output() handleShowRequestModal: EventEmitter<
        boolean
    > = new EventEmitter();

    constructor(
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private categoryAdapter: CategoryAdapter,
        private searchPageService: SearchPageService
    ) {}

    _fetchMoreResults(page: number) {
        this._loadingMore = true;

        const searchParams = this.getSearchParams(
            this._searchTerm,
            this._filterId,
            page
        );

        this.unsubscribeSearchSub();
        this.searchSubscription = this._fetchSearchResults(
            { ...searchParams },
            false,
            false,
            true
        ).subscribe((results: SearchSku[]) => {
            this._moreResults = results;
        });
    }

    _handleCategoryChange(categoryId: number) {
        const searchParams = this.getSearchParams(this._searchTerm, categoryId);

        this._filtering = true;
        this.setCategoryFilter(categoryId);
        /* [TODO_RITESH] disable this if you want to retain the category id from query param in category search flow
        even if user has toggled through filters */
        this._filterId = categoryId;
        this.unsubscribeSearchSub();
        this.searchSubscription = this._fetchSearchResults(
            searchParams,
            /* update all filters list when user clicks on All Categories */
            isNaN(categoryId),
            true,
            true
        ).subscribe((results: SearchSku[]) => {
            this._searchResults = results;
        });
    }

    private _setInitialSearchResults(query: string) {
        if (this.backendSearch) {
            const searchParams = this.getSearchParams(query, this._filterId);

            this._loading = true;

            if (this.searchPageService.isValidSearchTerm(query)) {
                this.unsubscribeSearchSub();
                this.searchSubscription = this._fetchSearchResults(
                    searchParams,
                    true,
                    true,
                    true
                ).subscribe((results: SearchSku[]) => {
                    this._searchResults = results;
                });
            }
        } else {
            this._loading = false;
        }
    }

    private _fetchSearchResults(
        searchData: SkuSearchParams,
        updateFilters = true,
        newSearch?: boolean,
        silent?: boolean
    ): Observable<SearchSku[]> {
        const query = this.utilService.getNestedValue(searchData, "query");

        this.searchPageService.generateSearchID(query);
        this.searchPageService.sendSearchInitiatedEvent(searchData);

        return this.searchPageService.searchSkus(searchData, silent).pipe(
            map((searchResponse: SkuSearchResponse) => {
                this.setAdditionalInfo(
                    searchResponse,
                    updateFilters,
                    newSearch
                );

                this.searchPageService.sendSearchCompletedEvent(
                    searchData,
                    this._numberOfHits,
                    this._searchId
                );

                return this.utilService.getNestedValue(searchResponse, "items");
            }),
            catchError((_) => {
                return [];
            }),
            finalize(() => {
                this._loading = this._loadingMore = this._filtering = false;
                this.cdr.detectChanges();
            })
        );
    }

    private _isSearchTermUpdated(searchTerm: string): boolean {
        if (
            searchTerm &&
            this.searchTerm &&
            this.searchTerm.trim() === searchTerm.trim()
        ) {
            return false;
        }

        return true;
    }

    private getSearchParams(
        query: string,
        categoryId: number,
        page = 0
    ): SkuSearchParams {
        const searchParams: SkuSearchParams = {
            page,
            query,
        };

        if (categoryId) {
            searchParams.category_id = categoryId;
        }

        return searchParams;
    }

    private setCategoryFilter(categoryId: number) {
        if (!isNaN(categoryId)) {
            this._categoryFilter$ = this.categoryAdapter.getCategoryMetaById(
                categoryId
            );
        } else {
            this._categoryFilter$ = of(null);
        }
    }

    private setAdditionalInfo(
        searchResponse: SkuSearchResponse,
        updateFilters: boolean,
        newSearch: boolean
    ) {
        this._searchId = this.utilService.getNestedValue(
            searchResponse,
            "meta_data.queryId"
        );
        this._pageNumber = this.utilService.getNestedValue(
            searchResponse,
            "meta_data.page"
        );

        if (newSearch) {
            this._pageSize = this.utilService.getNestedValue(
                searchResponse,
                "meta_data.hitsPerPage"
            );
            this._numberOfHits = this.utilService.getNestedValue(
                searchResponse,
                "meta_data.numberOfHits"
            );
            this._numberOfPages = this.utilService.getNestedValue(
                searchResponse,
                "meta_data.numberOfPages"
            );
            this._resultType = this.utilService.getNestedValue(
                searchResponse,
                "meta_data.matchType.type"
            );
        }

        if (updateFilters) {
            this._categories = this.utilService.getNestedValue(
                searchResponse,
                "meta_data.category"
            );
        }
    }

    private unsubscribeSearchSub() {
        if (this.searchSubscription && !this.searchSubscription.closed) {
            this.searchSubscription.unsubscribe();
            this.searchSubscription = null;
        }
    }
}
