import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { MiscAdapter, CartAdapter, UserAdapter } from "@shared/adapters";

import { SETTINGS } from "@constants";

import { Sku, Subscription, Vacation } from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { ScheduleDict } from "@types";

import { SearchPageAdapter as Adapter } from "../services/search.adapter";

@Component({
    selector: "supr-search-content-container",
    template: `
        <supr-search-content
            [searchTerm]="searchTerm"
            [skuList]="skuList$ | async"
            [vacation]="vacation$ | async"
            [backendSearch]="backendSearch"
            [cartPresent]="cartPresent$ | async"
            [loading]="skuLoadingState$ | async"
            [scheduleData]="scheduleData$ | async"
            [categoryFilter]="categoryFilter$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [searchFiltersEnabled]="searchFiltersEnabled"
            [subscriptionList]="subscriptionList$ | async"
            [openRequestProductModal]="openRequestProductModal$ | async"
            (handleUpdateTerm)="handleUpdateTerm.emit($event)"
        ></supr-search-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchContentContainer implements OnInit {
    @Input() searchTerm: string;

    @Output() handleUpdateTerm: EventEmitter<string> = new EventEmitter();

    backendSearch = false;
    skuList$: Observable<Sku[]>;
    searchFiltersEnabled = false;
    vacation$: Observable<Vacation>;
    cartPresent$: Observable<boolean>;
    categoryFilter$: Observable<number>;
    isAnonymousUser$: Observable<boolean>;
    skuLoadingState$: Observable<boolean>;
    scheduleData$: Observable<ScheduleDict>;
    openRequestProductModal$: Observable<string>;
    subscriptionList$: Observable<Subscription[]>;

    constructor(
        private adapter: Adapter,
        private cardAdapter: CartAdapter,
        private miscAdapter: MiscAdapter,
        private userAdapter: UserAdapter,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.skuList$ = this.adapter.skuList$;
        this.vacation$ = this.adapter.vacation$;
        this.scheduleData$ = this.adapter.schedule$;
        this.cartPresent$ = this.cardAdapter.miniCartPresent$;
        this.skuLoadingState$ = this.adapter.skuLoadingState$;
        this.subscriptionList$ = this.adapter.subscriptionList$;
        this.isAnonymousUser$ = this.userAdapter.isAnonymousUser$;
        this.openRequestProductModal$ = this.adapter.queryParams$.pipe(
            map((params: Params) => params["openRequestProductModal"])
        );
        this.backendSearch = this.settingsService.getSettingsValue(
            SETTINGS.BACKEND_SEARCH
        );
        this.searchFiltersEnabled =
            this.backendSearch &&
            this.settingsService.getSettingsValue(SETTINGS.SKU_SEARCH_FILTERS);
        this.categoryFilter$ = this.miscAdapter.urlParams$.pipe(
            map((params: Params) => {
                const categoryId = params["categoryId"];
                return !isNaN(categoryId) && Number(categoryId);
            })
        );
    }
}
