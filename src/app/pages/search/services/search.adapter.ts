import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import {
    StoreState,
    SkuStoreSelectors,
    CartStoreSelectors,
    SubscriptionStoreSelectors,
    VacationStoreSelectors,
    ScheduleStoreSelectors,
    RouterStoreSelectors,
    MiscStoreSelectors,
    SearchStoreActions,
} from "@supr/store";

import { Sku, SearchInstrumentation } from "@shared/models";

@Injectable()
export class SearchPageAdapter {
    constructor(private store: Store<StoreState>) {}

    skuList$ = this.store.pipe(select(SkuStoreSelectors.selectSkuList));

    skuLoadingState$ = this.store.pipe(
        select(SkuStoreSelectors.selectSkuLoading)
    );

    miniCart$ = this.store.pipe(select(CartStoreSelectors.selectMiniCart));

    subscriptionList$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    vacation$ = this.store.pipe(select(VacationStoreSelectors.selectVacation));

    schedule$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleDict)
    );

    isExperimentsFetched$ = this.store.pipe(
        select(MiscStoreSelectors.selectExperimentsFetched)
    );

    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    getSkuById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, { skuId })
        );
    }

    sendAddToCartInstrumentation(data: SearchInstrumentation, skuId: number) {
        this.store.dispatch(
            new SearchStoreActions.SendSearchInstrumentationAction({
                data,
                skuId,
            })
        );
    }

    saveSearchContextForSubscribe(data: SearchInstrumentation, skuId: number) {
        this.store.dispatch(
            new SearchStoreActions.SaveSearchInstrumentationAction({
                data,
                skuId,
            })
        );
    }

    clearSearchContext(skuId: number) {
        this.store.dispatch(
            new SearchStoreActions.ClearSearchInstrumentationAction(skuId)
        );
    }
}
