import { SearchPageService } from "./search.service";
import { SearchPageAdapter } from "./search.adapter";

export const searchServices = [SearchPageAdapter, SearchPageService];
