import { Injectable } from "@angular/core";

import { finalize } from "rxjs/operators";

import { Subscription, Observable } from "rxjs";

import { SETTINGS, SEARCH_RESULT_TYPES } from "@constants";

import { Sku, SkuSearchResponse, CategoryMeta } from "@models";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SuprApi, SkuSearchParams } from "@types";

import { DEFAULT_SEARCH_SCORE_PARAMS } from "@pages/search/constants";
import { CUSTOM_EVENTS } from "@pages/search/constants/analytics.constants";

import { SearchSetting } from "@pages/search/types";

@Injectable()
export class SearchPageService {
    private skuList = [];
    private searchId: string;
    private searchSettingsData: SearchSetting;
    private apiSubscription: Subscription;
    private preSearchTerms = [];
    private minSearchTermLength = 2;

    constructor(
        private apiService: ApiService,
        private settingsService: SettingsService,
        private utilService: UtilService,
        private analyticsService: AnalyticsService
    ) {
        this.getSearchSettingsData();
    }

    initialize(skuList: Sku[]) {
        this.skuList = skuList;
    }

    /* [TODO_RITESH] Deprecate once backend search is implemented */
    search_dep(query: string): Sku[] {
        if (!this.isSearchValid(query)) {
            this.searchId = null;
            return [];
        }

        this.generateSearchID(query);
        this.sendSearchInitiatedEvent({ query, page: 0 });

        let searchResults = this.getBaseSkuList();

        // 1. Calculate weight for each sku
        searchResults.forEach((sku) => this.calculateWeightForSku(query, sku));

        // 2. Filter sku list based on weight
        searchResults = searchResults.filter((sku) => sku.weight > 0);

        // 3. Sort sku list based on weight (Desc order)
        searchResults.sort((a, b) => b.weight - a.weight);

        this.sendSearchCompletedEvent(
            { query, page: 0 },
            searchResults.length,
            null
        );
        // 4. return the final copy of results
        return [...searchResults];
    }

    /**
     * Search SKUs
     *
     * @param {SkuSearchParams} searchData
     * @param {boolean} silent
     * @returns {Observable<SkuSearchResponse>}
     * @memberof SearchPageService
     */
    searchSkus(
        searchData: SkuSearchParams,
        silent: boolean = false
    ): Observable<SkuSearchResponse> {
        return this.apiService.searchForSkus(searchData, silent);
    }

    getSearchId(): string {
        return this.searchId;
    }

    isValidSearchTerm(searchTerm: string): boolean {
        if (isNaN(this.minSearchTermLength)) {
            this.minSearchTermLength = this.settingsService.getSettingsValue(
                SETTINGS.SEARCH_MIN_CHAR_LENGTH
            );
        }

        return (
            searchTerm && searchTerm.trim().length >= this.minSearchTermLength
        );
    }

    /**
     * Generate Search results count message
     *
     * @param {number} numberOfHits
     * @param {string} searchTerm
     * @returns {string}
     * @memberof SearchPageService
     */
    getCountMessage(numberOfHits: number, searchTerm: string): string {
        if (!searchTerm) {
            return `${numberOfHits} result${
                numberOfHits > 1 ? "s" : ""
            } found.`;
        }

        return `${numberOfHits} result${
            numberOfHits > 1 ? "s" : ""
        } found for '${searchTerm}'.`;
    }

    /**
     * Generate alternate Search results message
     *
     * @param {string} searchTerm
     * @param {number} numberOfHits
     * @param {CategoryMeta} categoryFilter
     * @param {SEARCH_RESULT_TYPES} matchType
     * @returns {string}
     * @memberof SearchPageService
     */

    getAlternateResultsMessage(
        searchTerm: string,
        categoryFilter: CategoryMeta,
        matchType: SEARCH_RESULT_TYPES
    ): string {
        if (
            matchType !== SEARCH_RESULT_TYPES.ALTERNATE ||
            !categoryFilter ||
            !categoryFilter.name
        ) {
            return "";
        }

        const categoryMsg =
            categoryFilter && categoryFilter.name
                ? `in '${categoryFilter.name}'. Showing results from all categories.`
                : ".";

        return `No results found for '${searchTerm}' ${categoryMsg}`;
    }

    /**
     * Generate search ID using query and epoch
     *
     * @param {string} query
     * @returns {void}
     * @memberof SearchPageService
     */
    generateSearchID(query: string): void {
        if (this.isValidSearchTerm(query)) {
            this.searchId = `${query}_${Date.now()}`;
        } else {
            this.searchId = null;
        }
    }

    /**
     * Send Search Initiated event
     *
     * @param {SkuSearchParams} searchData
     * @returns {void}
     * @memberof SearchPageService
     */
    sendSearchInitiatedEvent(searchData: SkuSearchParams): void {
        const page = this.utilService.getNestedValue(searchData, "page");
        const searchTerm = this.utilService.getNestedValue(searchData, "query");
        const categoryId = this.utilService.getNestedValue(
            searchData,
            "category_id"
        );

        this.analyticsService.trackCustom(CUSTOM_EVENTS.SEARCH_INITIATED, {
            page,
            searchTerm,
            categoryId,
            searchId: this.searchId,
        });
    }

    /**
     * Send Search Completed event
     *
     * @param {SkuSearchParams} searchData
     * @param {number} numberOfHits
     * @param {string} queryId
     * @returns {void}
     * @memberof SearchPageService
     */
    sendSearchCompletedEvent(
        searchData: SkuSearchParams,
        numberOfHits: number,
        queryId: string
    ): void {
        const page = this.utilService.getNestedValue(searchData, "page");
        const searchTerm = this.utilService.getNestedValue(searchData, "query");
        const categoryId = this.utilService.getNestedValue(
            searchData,
            "category_id"
        );

        this.analyticsService.trackCustom(CUSTOM_EVENTS.SEARCH_COMPLETED, {
            page,
            queryId,
            searchTerm,
            categoryId,
            searchId: this.searchId,
            resultsCount: numberOfHits,
        });
    }

    private getBaseSkuList(): Sku[] {
        return JSON.parse(JSON.stringify(this.skuList));
    }

    private calculateWeightForSku(query: string, sku: Sku) {
        const {
            fullMatchWeight,
            prefixMatchWeight,
            partialMatchWeight,
        } = this.searchSettingsData;

        // 1. Split query string into query terms (using spaces)
        const queryTerms = query.trim().split(" ");

        // 2. For each sku search term, loop through query terms
        sku.search_terms = sku.search_terms || [];
        sku.search_terms.forEach((searchTerm) => {
            // Start with weight zero
            searchTerm.weight = 0;

            // 3. For each query term get weight and sum up all weights.
            // This will be the search term weight
            queryTerms.forEach((queryTerm) => {
                const { term, score } = searchTerm;
                const _queryTerm = queryTerm.trim();
                if (this.isQueryFullMatch(_queryTerm, term)) {
                    searchTerm.weight += score * fullMatchWeight;
                } else if (this.isQueryPrefixMatch(_queryTerm, term)) {
                    searchTerm.weight += score * prefixMatchWeight;
                } else if (this.isQueryPartialMatch(_queryTerm, term)) {
                    searchTerm.weight += score * partialMatchWeight;
                }
            });
        });

        sku.weight = this.getSkuWeight(sku);
    }

    private getSkuWeight(sku: Sku) {
        let weight = 0;
        const mergeType = this.searchSettingsData.skuScoreMergeType;

        if (mergeType === "max") {
            const weights = sku.search_terms.map(
                (searchTerm) => searchTerm.weight
            );
            weight = Math.max(...weights);
        } else if (mergeType === "sum") {
            weight = sku.search_terms.reduce((acc, searchTerm) => {
                return searchTerm.weight + acc;
            }, 0);
        }

        return weight;
    }

    private isPreSearchTermExists(query: string): boolean {
        if (!this.utilService.isLengthyArray(this.preSearchTerms)) {
            return;
        }

        return this.preSearchTerms.indexOf(query.toLowerCase()) < 0
            ? false
            : true;
    }

    private isSearchValid(query: string): boolean {
        if (this.isPreSearchTermExists(query)) {
            return true;
        }

        if (!this.skuList.length || !this.isValidSearchTerm(query)) {
            return false;
        } else {
            return true;
        }
    }

    private getSearchSettingsData() {
        this.searchSettingsData = DEFAULT_SEARCH_SCORE_PARAMS;
        this.preSearchTerms = this.settingsService.getSettingsValue(
            "preSearchTerms",
            []
        );
    }

    private isQueryFullMatch(query: string, term: string): boolean {
        return query.toLowerCase() === term.toLowerCase();
    }

    private isQueryPrefixMatch(query: string, term: string): boolean {
        return term.toLowerCase().indexOf(query.toLowerCase()) === 0;
    }

    private isQueryPartialMatch(query: string, term: string): boolean {
        return term.toLowerCase().indexOf(query.toLowerCase()) > 0;
    }
    private unsubscribeApiCall() {
        if (this.apiSubscription && !this.apiSubscription.closed) {
            this.apiSubscription.unsubscribe();
        }
    }

    createProduct(body: SuprApi.CreateRequestBody): void {
        this.apiSubscription = this.apiService
            .createRequest(body)
            .pipe(
                finalize(() => {
                    this.unsubscribeApiCall();
                })
            )
            .subscribe();
    }
}
