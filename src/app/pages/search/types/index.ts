import { Sku, Subscription } from "@shared/models";

export interface SearchSetting {
    fullMatchWeight: number;
    prefixMatchWeight: number;
    partialMatchWeight: number;
    skuScoreMergeType: string;
}

export interface SkuPos {
    skuPos: number;
    skuId: number;
}

export interface ActiveSubscriptionBalance {
    sku: Sku;
    subscription: Subscription;
}

export interface RequestProductConstants {
    enableRequestProduct?: boolean;
    noResults?: string;
    title?: string;
    subTitle?: string;
    requestButtonText?: string;
    header?: string;
    lable?: string;
    placeholder?: string;
    success?: string;
    submit?: string;
    error?: string;
}
