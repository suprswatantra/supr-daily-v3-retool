import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { PopularSkuSearch } from "@models";
import { UtilService } from "@services/util/util.service";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-search-popular-searches",
    template: `
        <ng-container *ngIf="loading; else content">
            <div class="wrapper">
                <ion-skeleton-text animated></ion-skeleton-text>
                <ion-skeleton-text animated></ion-skeleton-text>
                <ion-skeleton-text animated></ion-skeleton-text>
            </div>
        </ng-container>

        <ng-template #content>
            <div class="suprRow wrap wrapper" *ngIf="searchQueries?.length">
                <supr-text class="header" type="body">
                    ${TEXTS.POPULAR_SEARCHES}
                </supr-text>
                <div class="divider16"></div>

                <supr-chip
                    [saObjectValue]="searchQuery?.query"
                    saObjectName="${SA_OBJECT_NAMES.POPULAR_SEARCH_CHIP}"
                    *ngFor="
                        let searchQuery of searchQueries;
                        trackBy: trackByFn
                    "
                    (handleClick)="handleClick.emit(searchQuery?.query)"
                >
                    <supr-text type="paragraph">
                        {{ searchQuery?.query | titlecase }}
                    </supr-text>
                </supr-chip>
            </div>
        </ng-template>
    `,
    styleUrls: ["./popular-searches.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopularSearchesComponent implements OnInit {
    @Input() loading: boolean;
    @Input() searchQueries: PopularSkuSearch[];

    @Output() handleClick: EventEmitter<string> = new EventEmitter();
    @Output() fetchPopularSearches: EventEmitter<void> = new EventEmitter();

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        if (!this.utilService.isLengthyArray(this.searchQueries)) {
            this.fetchPopularSearches.emit();
        }
    }

    trackByFn(index: number): number {
        return index;
    }
}
