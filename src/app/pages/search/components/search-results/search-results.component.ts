import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SEARCH_RESULT_TYPES } from "@constants";

import { SearchSku, CategoryMeta, SearchCategoryMeta } from "@models";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import { TEXTS } from "../../constants/";
import { SearchPageService } from "../../services/search.service";

const PAGE_SIZE = 30;

@Component({
    selector: "supr-search-results",
    template: `
        <ng-container
            *ngIf="
                !filtering && (loading || !searchResults?.length);
                else content_template
            "
        >
            <supr-search-no-results
                [searchTerm]="searchTerm"
                *ngIf="!loading && !searchResults?.length"
                (handleShowRequestModal)="handleShowRequestModal.emit($event)"
            ></supr-search-no-results>

            <div class="suprRow center loaderGif fullPage" *ngIf="loading">
                <ion-spinner name="lines" color="secondary"></ion-spinner>
            </div>
        </ng-container>

        <ng-template #content_template>
            <div class="results">
                <div class="paddedWrapper">
                    <ng-container *ngIf="_alternatesStr && !filtering">
                        <supr-text-highlight
                            class="wrap"
                            type="paragraph"
                            [text]="_alternatesStr"
                            [highlightedText]="searchTerm"
                        >
                        </supr-text-highlight>
                        <div class="divider12"></div>
                    </ng-container>

                    <div class="suprRow">
                        <supr-text
                            type="paragraph"
                            class="filterText"
                            *ngIf="searchFiltersEnabled"
                        >
                            ${TEXTS.FILTER_BY_CATEGORY}
                        </supr-text>

                        <supr-text-highlight
                            type="paragraph"
                            class="countText"
                            [text]="_countStr"
                            [highlightedText]="searchTerm"
                            [class.wrap]="!searchFiltersEnabled"
                        >
                        </supr-text-highlight>
                    </div>
                </div>

                <div
                    class="divider8"
                    *ngIf="searchFiltersEnabled; else divider16"
                ></div>

                <ng-template #divider16>
                    <div class="divider16"></div>
                </ng-template>

                <div class="categoryChipsWrapper" *ngIf="searchFiltersEnabled">
                    <supr-search-category-chips-wrapper
                        [saContext]="searchTerm"
                        [categories]="categories"
                        [resultType]="resultType"
                        [categoryFilter]="categoryFilter"
                        (handleClick)="updateSelectedCategory($event)"
                    >
                    </supr-search-category-chips-wrapper>
                </div>

                <ng-container *ngIf="!filtering; else loader_template">
                    <div
                        infiniteScroll
                        id="scrollWrapper"
                        class="resultsWrapper"
                        [scrollWindow]="false"
                        [infiniteScrollDistance]="3"
                        [infiniteScrollThrottle]="50"
                        [class.cartPresent]="cartPresent"
                        [class.alternateResults]="_alternatesStr"
                        [class.searchFiltersEnabled]="searchFiltersEnabled"
                        (scrolled)="onScrollDown()"
                    >
                        <div
                            class="paddedWrapper"
                            *ngFor="
                                let result of _searchResults;
                                trackBy: trackByFn
                            "
                        >
                            <supr-search-product-tile-container
                                class="suprVisibleDelay"
                                [searchId]="searchId"
                                [searchResult]="result"
                                [searchTerm]="searchTerm"
                                [alternatesV2Experiment]="
                                    _alternatesV2Experiment
                                "
                                [searchFiltersEnabled]="searchFiltersEnabled"
                            ></supr-search-product-tile-container>
                            <ng-container *ngIf="_alternatesV2Experiment">
                                <supr-oos-v5-alternates-container
                                    [skuId]="result?.sku_id"
                                ></supr-oos-v5-alternates-container>
                            </ng-container>

                            <div class="divider32"></div>
                        </div>

                        <div class="loadingMore" *ngIf="loadingMore">
                            <supr-loader></supr-loader>
                        </div>
                    </div>
                </ng-container>

                <ng-template #loader_template>
                    <div class="suprRow center loaderGif">
                        <ion-spinner
                            name="lines"
                            color="secondary"
                        ></ion-spinner>
                    </div>
                </ng-template>
            </div>
        </ng-template>
    `,
    styleUrls: [
        "../../styles/search.page.scss",
        "./search-results.component.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsComponent implements OnInit {
    @Input() loading = false;
    @Input() searchId: string;
    @Input() filtering = false;
    @Input() pageNumber: number;
    @Input() loadingMore = false;
    @Input() cartPresent: boolean;
    @Input() pageSize = PAGE_SIZE;
    @Input() numberOfPages: number;
    @Input() searchFiltersEnabled: boolean;
    @Input() resultType: SEARCH_RESULT_TYPES;
    @Input() categories: SearchCategoryMeta[];

    @Input() set searchTerm(term: string) {
        this._searchTerm = term;
        this.setMessages();
    }
    get searchTerm(): string {
        return this._searchTerm;
    }
    @Input() set categoryFilter(filter: CategoryMeta) {
        this._categoryFilter = filter;
        this.setMessages();
    }
    get categoryFilter(): CategoryMeta {
        return this._categoryFilter;
    }

    @Input() set numberOfHits(hits: number) {
        this._numberOfHits = hits;

        this.setMessages();
    }
    get numberOfHits(): number {
        return this._numberOfHits;
    }

    @Input()
    set searchResults(value: SearchSku[]) {
        this._searchResults = value;
    }
    get searchResults(): SearchSku[] {
        return this._searchResults;
    }

    @Input()
    set moreResults(value: SearchSku[]) {
        this.setMoreSearchResults(value);
    }

    @Output() handleFetchMore: EventEmitter<number> = new EventEmitter();
    @Output() handleCategoryChange: EventEmitter<number> = new EventEmitter();
    @Output() handleShowRequestModal: EventEmitter<
        boolean
    > = new EventEmitter();

    _countStr: string;
    _alternatesStr: string;
    _alternatesV2Experiment = true;

    private _searchTerm: string;
    private _numberOfHits: number;
    private _categoryFilter: CategoryMeta;
    private _searchResults: SearchSku[] = [];

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService,
        private searchPageService: SearchPageService
    ) {}

    ngOnInit() {
        this._alternatesV2Experiment = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );
    }

    trackByFn(index: number): number {
        return index;
    }

    onScrollDown() {
        const pageNumber = this.pageNumber !== null ? this.pageNumber : 0;

        if (pageNumber + 1 < this.numberOfPages) {
            this.handleFetchMore.emit(pageNumber + 1);
        }
    }

    updateSelectedCategory(category: CategoryMeta) {
        const newFilter = this.utilService.getNestedValue(category, "id");
        const appliedFilter = this.utilService.getNestedValue(
            this._categoryFilter,
            "id"
        );

        if (appliedFilter === newFilter) {
            return;
        }

        this.handleCategoryChange.emit(newFilter);
    }

    private setMessages() {
        if (this._numberOfHits) {
            this._countStr = this.searchPageService.getCountMessage(
                this._numberOfHits,
                !this.searchFiltersEnabled ? this._searchTerm : null
            );

            if (this.searchFiltersEnabled) {
                this._alternatesStr = this.searchPageService.getAlternateResultsMessage(
                    this._searchTerm,
                    this._categoryFilter,
                    this.resultType
                );
            }
        }
    }

    /* appending results for page to existing list */
    private setMoreSearchResults(results: SearchSku[]) {
        if (Array.isArray(this._searchResults)) {
            if (this.utilService.isLengthyArray(results)) {
                this._searchResults = [...this._searchResults, ...results];
            }
        }
    }
}
