import {
    Input,
    OnInit,
    NgZone,
    Output,
    Component,
    OnChanges,
    SimpleChange,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";

import { Sku, Subscription, Vacation } from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { SettingsService } from "@services/shared/settings.service";
import { SearchPageService } from "@pages/search/services/search.service";

import { ScheduleDict } from "@types";

import {
    RequestProductConstants,
    ActiveSubscriptionBalance,
} from "@pages/search/types";

import {
    SEARCH_START_MIN_CHAR_LENGTH,
    REQUEST_PRODUCT,
} from "@pages/search/constants";
import { SA_OBJECT_NAMES } from "@pages/search/constants/analytics.constants";

@Component({
    selector: "supr-search-content",
    template: `
        <ng-container *ngIf="!showInitialState; else initialState">
            <div class="suprScrollContent">
                <supr-search-results-container
                    [searchId]="_searchId"
                    [searchTerm]="searchTerm"
                    [cartPresent]="cartPresent"
                    [filterId]="categoryFilter"
                    [backendSearch]="backendSearch"
                    [searchResults]="searchResults"
                    [searchFiltersEnabled]="searchFiltersEnabled"
                    (handleShowRequestModal)="showRequestModal($event)"
                ></supr-search-results-container>
            </div>
        </ng-container>

        <ng-template #initialState>
            <div class="suprScrollContent" [class.cartPresent]="cartPresent">
                <supr-subscription-products
                    saImpression
                    [saImpressionEnabled]="
                        ${SA_IMPRESSIONS_IS_ENABLED.SUBSCRIPTION_PRODUCTS}
                    "
                    saObjectName="${SA_OBJECT_NAMES.ADD_FROM_SUBSCRIPTION}"
                    *ngIf="!isAnonymousUser && subscriptionItems?.length > 0"
                    [subscriptionItems]="subscriptionItems"
                    (handleTileClick)="onSubscriptionSkuClick($event)"
                ></supr-subscription-products>

                <ng-container *ngIf="searchFiltersEnabled">
                    <supr-search-popular-container
                        (handleClick)="onPopularSearchesClick($event)"
                    >
                    </supr-search-popular-container>

                    <supr-search-past-orders-container>
                    </supr-search-past-orders-container>
                </ng-container>

                <supr-search-categories></supr-search-categories>
            </div>
        </ng-template>

        <supr-subscription-balance-modal
            [activeSubscription]="activeSubscription"
            [activeSku]="activeSku"
            [vacation]="vacation"
            [scheduleData]="scheduleData"
            [showSubBalanceModal]="showSubBalanceModal"
            (handleShowModalChange)="onShowModalChange($event)"
        ></supr-subscription-balance-modal>
        <supr-search-request-product
            [searchTerm]="searchTerm"
            [isRequestModal]="_showRequestModal"
            [requestProductConfig]="requestProductConfig"
            (handleCloseModal)="closeModal($event)"
        ></supr-search-request-product>
        <supr-loader-overlay *ngIf="loading"> </supr-loader-overlay>
    `,
    styleUrls: ["../../styles/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit, OnChanges {
    @Input() skuList: Sku[];
    @Input() loading: boolean;
    @Input() vacation: Vacation;
    @Input() searchTerm: string;
    @Input() cartPresent: boolean;
    @Input() categoryFilter: number;
    @Input() backendSearch: boolean;
    @Input() isAnonymousUser: boolean;
    @Input() scheduleData: ScheduleDict;
    @Input() searchFiltersEnabled: boolean;
    @Input() subscriptionList: Subscription[];
    @Input() openRequestProductModal: string;

    @Output() handleUpdateTerm: EventEmitter<string> = new EventEmitter();

    _searchId: string;
    hasSearchRun = false;
    activeSku: Sku = null;
    showInitialState = true;
    searchResults: Sku[] = [];
    _showRequestModal = false;
    showSubBalanceModal = false;
    subscriptionItems: Subscription[] = [];
    activeSubscription: Subscription = null;
    requestProductConfig: RequestProductConstants = {};

    constructor(
        private ngZone: NgZone,
        private dateService: DateService,
        private utilService: UtilService,
        private settingService: SettingsService,
        private searchService: SearchPageService,
        private scheduleService: ScheduleService
    ) {}

    ngOnInit() {
        this.updateSkuList();
        this.setSubscriptionItems();
        this.fetchRequestProductConfig();
        this.checkShowRequestModal();
        this.fetchPopularSearches();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSearchTermChange(changes["searchTerm"]);
        this.handleSkuListChange(changes["skuList"]);
        this.handleSubscriptionListChange(changes["subscriptionList"]);
    }

    closeSubBalanceModal() {
        this.showSubBalanceModal = false;
    }

    onSubscriptionSkuClick(subscriptionBalance: ActiveSubscriptionBalance) {
        this.activeSku = subscriptionBalance.sku;
        this.activeSubscription = subscriptionBalance.subscription;

        this.showSubBalanceModal = true;
    }

    updateActionListener(date: string) {
        if (date && this.dateService.isValidDateString(date)) {
            const dateObject = this.dateService.dateFromText(date);
            const scheduleStartDateObject = this.dateService.startDateOfWeek(
                dateObject
            );
            const scheduleStartDate = this.dateService.textFromDate(
                scheduleStartDateObject
            );

            this.scheduleService.setCustomScheduleScrollDate(date);
            this.scheduleService.setCustomScheduleStartDate(scheduleStartDate);
        }
        this.scheduleService.setCustomScheduleScrollDate(date);
    }

    onShowModalChange(showModal: boolean) {
        this.showSubBalanceModal = showModal;
    }

    closeModal(clearSearchTerm = false) {
        if (clearSearchTerm) {
            this.handleUpdateTerm.emit("");
        }
        this._showRequestModal = false;
    }

    showRequestModal(state: boolean) {
        this._showRequestModal = state;
    }

    onPopularSearchesClick(query: string) {
        this.handleUpdateTerm.emit(query);
    }

    private fetchRequestProductConfig() {
        this.requestProductConfig = <RequestProductConstants>(
            this.settingService.getSettingsValue(
                "requestProduct",
                REQUEST_PRODUCT
            )
        );
    }

    private checkShowRequestModal() {
        if (
            this.openRequestProductModal &&
            this.openRequestProductModal === "true"
        ) {
            this.showRequestModal(true);
        }
    }

    private setSubscriptionItems() {
        if (this.utilService.isLengthyArray(this.subscriptionList)) {
            // if there is a search term,
            // set subscription items to the subscription items that are also in search results.
            this.subscriptionItems = this.hasSearchRun
                ? this.subscriptionList.filter((item) =>
                      this.searchResults.some(
                          (sku) => (item && item.sku_id) === sku.id
                      )
                  )
                : this.subscriptionList;
        } else {
            this.subscriptionItems = [];
            return;
        }

        // Check if subscriptionItems empty, if yes assign empty array
        if (this.utilService.isEmpty(this.subscriptionItems)) {
            this.subscriptionItems = [];
        }
    }

    private updateSkuList() {
        this.searchService.initialize(this.skuList);
    }

    private handleSubscriptionListChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setSubscriptionItems();
        }
    }

    private handleSearchTermChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setHasSearchRun();
            this.setViewState();
            this.searchProducts();
            this.setSubscriptionItems();
        }
    }

    private handleSkuListChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.updateSkuList();
            this.setViewState();
            this.searchProducts();
        }
    }

    private setViewState() {
        this.showInitialState = !this.hasSearchRun;
    }

    private setHasSearchRun() {
        this.hasSearchRun =
            this.searchTerm &&
            this.searchTerm.length >= SEARCH_START_MIN_CHAR_LENGTH;
    }

    private searchProducts() {
        if (!this.backendSearch) {
            this.ngZone.runOutsideAngular(() => {
                const results = this.searchService.search_dep(this.searchTerm);
                this.setSearchResults(results);
            });
        }
    }

    private setSearchResults(results: Sku[]) {
        this.ngZone.run(() => {
            this.searchResults = results;
            this._searchId = this.searchService.getSearchId();
        });
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchPopularSearches() {}
}
