import { Component, ChangeDetectionStrategy } from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

@Component({
    selector: "supr-search-categories",
    template: `
        <div class="wrapper">
            <supr-text class="header" type="body"> Categories </supr-text>
            <div class="divider16"></div>
            <supr-category-grid
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.CATEGORY_TILE}"
            ></supr-category-grid>
        </div>
    `,
    styleUrls: ["./categories.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesComponent {}
