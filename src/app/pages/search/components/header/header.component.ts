import {
    Component,
    Output,
    Input,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-search-header",
    template: `
        <supr-page-header>
            <supr-search-input
                [placeholder]="searchBoxPlaceholder"
                [searchTerm]="searchTerm"
                (handleChangeText)="handleChangeText.emit($event)"
            ></supr-search-input>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnChanges {
    @Input() searchTerm: string;
    @Input() isExperimentsFetched: boolean;

    @Output() handleChangeText: EventEmitter<string> = new EventEmitter();

    searchBoxPlaceholder = "";

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this.setSearchBoxPlaceholder();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["isExperimentsFetched"];

        if (this.canHandleChange(change)) {
            this.setSearchBoxPlaceholder();
        }
    }

    private setSearchBoxPlaceholder() {
        const searchBoxText = this.settingsService.getSettingsValue(
            SETTINGS.SEARCHBOX_PLACEHOLDER,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.SEARCHBOX_PLACEHOLDER]
        );

        this.searchBoxPlaceholder = searchBoxText.searchPage;
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
