import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Sku } from "@models";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

const INITIAL_RESULTS_SIZE = 20;

@Component({
    selector: "supr-search-results-dep",
    template: `
        <ng-container>
            <div class="results" *ngIf="searchResults?.length; else no_results">
                <div class="paddedWrapper">
                    <supr-text type="caption">{{ countStr }}</supr-text>
                </div>
                <div class="divider24"></div>
                <div
                    id="scrollWrapper"
                    class="resultsWrapper"
                    infiniteScroll
                    [scrollWindow]="false"
                    [infiniteScrollDistance]="3"
                    [infiniteScrollThrottle]="50"
                    (scrolled)="onScrollDown()"
                >
                    <div
                        class="paddedWrapper"
                        *ngFor="
                            let result of _visibleResults;
                            index as position;
                            trackBy: trackByFn
                        "
                    >
                        <supr-search-product-tile-dep-container
                            class="suprVisibleDelay"
                            [sku]="result"
                            [searchId]="searchId"
                            [saPosition]="position"
                            [alternatesV2Experiment]="_alternatesV2Experiment"
                        ></supr-search-product-tile-dep-container>
                        <ng-container *ngIf="_alternatesV2Experiment">
                            <supr-oos-v5-alternates-container
                                [skuId]="result?.id"
                            ></supr-oos-v5-alternates-container>
                        </ng-container>

                        <div class="divider32"></div>
                    </div>
                </div>
            </div>

            <ng-template #no_results>
                <supr-search-no-results
                    [searchTerm]="searchTerm"
                    (handleShowRequestModal)="
                        handleShowRequestModal.emit($event)
                    "
                ></supr-search-no-results>
            </ng-template>
        </ng-container>
    `,
    styleUrls: ["../../styles/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsDepComponent implements OnInit {
    @Input() searchId: string;
    @Input() searchTerm: string;
    @Input()
    set searchResults(value: Sku[]) {
        this._searchResults = value || [];
        this.setCountStr();
        this.setVisibleResults();
    }

    get searchResults(): Sku[] {
        return this._searchResults;
    }
    @Output() handleShowRequestModal: EventEmitter<
        boolean
    > = new EventEmitter();

    countStr: string;
    _searchResults: Sku[];
    _visibleResults: Sku[];
    _alternatesV2Experiment = true;

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this._alternatesV2Experiment = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );
    }

    trackByFn(index: number): number {
        return index;
    }

    setCountStr() {
        const count = this.searchResults.length;
        if (count) {
            const suffix = ` found for "${this.searchTerm}"`;
            this.countStr = `${count} result${count > 1 ? "s" : ""}${suffix}`;
        }
    }

    onScrollDown() {
        if (
            this.utilService.isLengthyArray(this.searchResults) &&
            this.utilService.isLengthyArray(this._visibleResults) &&
            this.searchResults.length === this._visibleResults.length
        ) {
            return;
        }

        const resultsToAppend = this.searchResults.slice(
            this._visibleResults.length,
            this._visibleResults.length + INITIAL_RESULTS_SIZE
        );

        this._visibleResults = [...this._visibleResults, ...resultsToAppend];
    }

    private setVisibleResults() {
        if (!this.utilService.isLengthyArray(this.searchResults)) {
            return;
        }

        const wrapper = document.getElementById("scrollWrapper");
        if (wrapper) {
            wrapper.scrollTop = 0;
        }

        this._visibleResults = this.searchResults.slice(
            0,
            INITIAL_RESULTS_SIZE
        );
    }
}
