import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { TEXTS } from "@pages/search/constants";
import { SA_OBJECT_NAMES } from "@pages/search/constants/analytics.constants";

import { Subscription } from "@models";

import { ActiveSubscriptionBalance } from "@pages/search/types";

import { SkuService } from "@services/shared/sku.service";

@Component({
    selector: "supr-subscription-products",
    template: `
        <div class="subscriptionBalance" *ngIf="showBlock">
            <div class="suprRow">
                <supr-text type="body">${TEXTS.ADD_EXISTING_SUBS}</supr-text>
            </div>

            <div class="divider12"></div>
            <div class="suprRow subscribedProducts">
                <ng-container *ngFor="let subscription of subscriptionItems">
                    <supr-subscribed-product-tile-container
                        *ngIf="subscription?.sku_data.active"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.ADD_FROM_SUBSCRIPTION}"
                        [saObjectValue]="subscription.sku_id"
                        [skuId]="subscription.sku_id"
                        [subscriptionItem]="subscription"
                        (handleTileClick)="handleTileClick.emit($event)"
                    ></supr-subscribed-product-tile-container>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionBalanceComponent implements OnInit, OnChanges {
    @Input() subscriptionItems: Subscription[] = [];

    @Output() handleTileClick: EventEmitter<
        ActiveSubscriptionBalance
    > = new EventEmitter();

    showBlock = true;

    constructor(private skuService: SkuService) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["subscriptionItems"])) {
            this.initialize();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private initialize() {
        const subscriptionItems = this.subscriptionItems || [];
        const suprPassSku = subscriptionItems.find((item) =>
            this.skuService.isSuprPassSku(item.sku_data)
        );

        if (suprPassSku && subscriptionItems.length === 1) {
            this.showBlock = false;
            return;
        }

        this.showBlock = true;
    }
}
