import { HeaderComponent } from "./header/header.component";
import { ContentComponent } from "./content/content.component";
import { CategoriesComponent } from "./categories/categories.component";
import { SearchResultsComponent } from "./search-results/search-results.component";
import { SearchResultsDepComponent } from "./search-results-dep/search-results-dep.component";
import { NoResultsComponent } from "./no-results/no-results.component";
import { SubscriptionBalanceComponent } from "./subscription-products/subscription-products.component";
import { SubscribedProductTileComponent } from "./subscribed-product-tile/subscribed-product-tile.component";
import { RequestProductComponent } from "./request-product/request-product.component";
import { PastOrdersComponent } from "./past-orders/past-orders.component";
import { PopularSearchesComponent } from "./popular-searches/popular-searches.component";
import { CategoryChipComponent } from "./category-chip/category-chip.component";
import { CategoryChipsWrapperComponent } from "./category-chips-wrapper/category-chips-wrapper.component";
import { SearchProductTileComponent } from "./product-tile/product-tile.component";

export const searchComponents = [
    HeaderComponent,
    ContentComponent,
    CategoriesComponent,
    SearchResultsComponent,
    SearchResultsDepComponent,
    NoResultsComponent,
    SubscriptionBalanceComponent,
    SubscribedProductTileComponent,
    RequestProductComponent,
    PastOrdersComponent,
    PopularSearchesComponent,
    CategoryChipComponent,
    CategoryChipsWrapperComponent,
    SearchProductTileComponent,
];
