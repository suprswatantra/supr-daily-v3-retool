import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-search-past-orders",
    template: `
        <div class="wrapper" *ngIf="skuList?.length">
            <div class="header">
                <supr-text type="body"> ${TEXTS.PAST_ORDERS} </supr-text>
            </div>
            <schedule-past-order-collection
                bgColor="#fff"
                [showHeader]="false"
                [pastOrderSkuList]="skuList"
            >
            </schedule-past-order-collection>
        </div>
    `,
    styleUrls: ["./past-orders.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PastOrdersComponent {
    @Input() skuList: Array<number>;
}
