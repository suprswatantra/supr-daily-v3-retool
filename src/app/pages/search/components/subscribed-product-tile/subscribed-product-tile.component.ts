import {
    Input,
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { Sku, Subscription } from "@shared/models";
import { ActiveSubscriptionBalance } from "@pages/search/types";

@Component({
    selector: "supr-subscribed-product-tile",
    template: `
        <div class="suprColumn tile center" (click)="handleClick()">
            <div class="suprColumn tileImage top center">
                <supr-image
                    [src]="sku?.image?.fullUrl"
                    [image]="sku?.image"
                    [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
                    [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
                ></supr-image>
                <div class="tilePlus suprRow center">
                    <supr-icon name="add_circle"></supr-icon>
                </div>
            </div>

            <div class="divider8"></div>

            <div class="suprColumn tileContent stretch center">
                <supr-text
                    class="tileContentName"
                    type="body"
                    [truncate]="true"
                >
                    {{ sku?.sku_name }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./subscribed-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscribedProductTileComponent {
    @Input() sku: Sku;
    @Input() subscription?: Subscription;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();
    @Output() handleTileClick: EventEmitter<
        ActiveSubscriptionBalance
    > = new EventEmitter();

    handleClick() {
        this.handleTileClick.emit({
            sku: this.sku,
            subscription: this.subscription,
        });
    }
}
