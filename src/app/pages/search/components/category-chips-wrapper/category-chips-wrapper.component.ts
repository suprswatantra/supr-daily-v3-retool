import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SEARCH_RESULT_TYPES } from "@constants";

import { CategoryMeta, SearchCategoryMeta } from "@models";

import { ALL_CATEGORY_FILTER_CHIP } from "../../constants";

@Component({
    selector: "supr-search-category-chips-wrapper",
    template: `<div class="suprRow wrapper">
        <supr-search-category-chip
            [saContext]="saContext"
            [category]="_allCategories"
            [selected]="
                !categoryFilter ||
                resultType === '${SEARCH_RESULT_TYPES.ALTERNATE}'
            "
            (handleClick)="handleClick.emit(null)"
        ></supr-search-category-chip>

        <ng-container *ngFor="let category of categories; trackBy: trackByFn">
            <supr-search-category-chip-container
                [category]="category"
                [saContext]="saContext"
                [selected]="
                    category?.id === categoryFilter?.id &&
                    resultType === '${SEARCH_RESULT_TYPES.EXACT}'
                "
                (handleClick)="handleClick.emit($event)"
            ></supr-search-category-chip-container>
        </ng-container>
    </div>`,
    styleUrls: ["./category-chips-wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryChipsWrapperComponent {
    @Input() saContext: string;
    @Input() categoryFilter: CategoryMeta;
    @Input() resultType: SEARCH_RESULT_TYPES;
    @Input() categories: SearchCategoryMeta[];

    @Output() handleClick: EventEmitter<CategoryMeta> = new EventEmitter();

    _allCategories = ALL_CATEGORY_FILTER_CHIP;

    trackByFn(_: number, category: SearchCategoryMeta): number {
        return category && category.id;
    }
}
