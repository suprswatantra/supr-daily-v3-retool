import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CategoryMeta } from "@models";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-search-category-chip",
    template: `<supr-chip
        *ngIf="category"
        [class.selected]="selected"
        [saContext]="saContext"
        [saObjectValue]="category?.id"
        saObjectName="${SA_OBJECT_NAMES.CATEGORY_FILTER}"
        (handleClick)="handleClick.emit(category)"
    >
        <div class="suprRow">
            <supr-image
                [src]="category?.image?.fullUrl"
                [image]="category?.image"
            ></supr-image>
            <div class="spacer4"></div>
            <supr-text type="paragraph">{{ category?.name }}</supr-text>
        </div>
    </supr-chip>`,
    styleUrls: ["./category-chip.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryChipComponent {
    @Input() saContext: any;
    @Input() selected = false;
    @Input() category: CategoryMeta;

    @Output() handleClick: EventEmitter<CategoryMeta> = new EventEmitter();
}
