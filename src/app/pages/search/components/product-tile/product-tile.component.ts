import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { Sku, CartItem, SkuAttributes } from "@models";

import { RouterService } from "@services/util/router.service";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-search-product-tile",
    template: `
        <div class="wrapper" *ngIf="sku">
            <div
                saClick
                *ngIf="headerText"
                class="suprRow header"
                [saContext]="searchTerm"
                [saObjectValue]="subcategoryId"
                saObjectName="${SA_OBJECT_NAMES.SUBCATEGORY_LINK}"
                (click)="goToPage()"
            >
                <supr-text type="caption">
                    {{ headerText | uppercase }}
                </supr-text>
                <supr-icon name="${ICON_NAMES.CHEVRON_RIGHT}"></supr-icon>
            </div>
            <supr-product-tile
                [sku]="sku"
                [cartItem]="cartItem"
                [saContext]="saContext"
                [saPosition]="saPosition"
                [skuNotified]="skuNotified"
                [skuAttributes]="skuAttributes"
                [alternatesV2Experiment]="alternatesV2Experiment"
                (handlePostAddToCart)="handleAddToCartInstrumentation.emit()"
                (handlePostSubscribe)="handleSubscribeInstrumentation.emit()"
                (handleSubModalStateChange)="
                    handleSubModalInstrumentation.emit($event)
                "
            ></supr-product-tile>
        </div>
    `,
    styleUrls: ["./product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchProductTileComponent {
    @Input() sku: Sku;
    @Input() saContext: string;
    @Input() searchTerm: string;
    @Input() cartItem: CartItem;
    @Input() saPosition: number;
    @Input() headerText: string;
    @Input() headerLink: string;
    @Input() skuNotified: string;
    @Input() subcategoryId: number;
    @Input() skuAttributes: SkuAttributes;
    @Input() alternatesV2Experiment = true;

    @Output() handleAddToCartInstrumentation: EventEmitter<
        void
    > = new EventEmitter();
    @Output() handleSubscribeInstrumentation: EventEmitter<
        void
    > = new EventEmitter();
    @Output() handleSubModalInstrumentation: EventEmitter<
        boolean
    > = new EventEmitter();

    constructor(private routerService: RouterService) {}

    goToPage() {
        if (this.headerLink) {
            this.routerService.goToUrl(this.headerLink);
        }
    }
}
