import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { ToastService } from "@services/layout/toast.service";

import { REQUEST_PRODUCT_TYPE, REQUEST_PRODUCT } from "@pages/search/constants";

import { MODAL_NAMES, TOAST_MESSAGES } from "@constants";

import { RequestProductConstants } from "@pages/search/types";

import { SearchPageService } from "@pages/search/services/search.service";

@Component({
    selector: "supr-search-request-product",
    styleUrls: ["../../styles/request.product.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <supr-modal
            *ngIf="isRequestModal"
            modalName="${MODAL_NAMES.REQUEST_PRODUCT}"
            modalType=""
            fullScreen="true"
            (handleClose)="closeModal()"
        >
            <supr-page-header (handleBackButtonClick)="closeModal()">
                <div class="header">{{ requestProductConfig.header }}</div>
            </supr-page-header>

            <div class="suprScrollContent">
                <supr-text type="subheading">
                    {{ requestProductConfig.lable }}
                </supr-text>

                <div class="divider16"></div>

                <supr-text class="textTitle" type="action14">
                    {{ requestProductConfig.textTitle }}
                </supr-text>

                <div class="divider12"></div>

                <textarea
                    [value]="searchTerm"
                    (keyup)="onChange($event)"
                    [placeholder]="requestProductConfig.placeholder"
                    rows="5"
                    type="text"
                ></textarea>

                <supr-profile-form-error
                    [errMsg]="isError"
                ></supr-profile-form-error>

                <div class="divider8"></div>
                <supr-text class="suggestionText" type="caption">
                    ${REQUEST_PRODUCT.suggestion}
                </supr-text>
            </div>

            <supr-page-footer>
                <supr-button (handleClick)="onSubmit()">
                    {{ requestProductConfig.submit }}
                </supr-button>
            </supr-page-footer>
        </supr-modal>
    `,
})
export class RequestProductComponent {
    @Input() isRequestModal: boolean;
    @Input() requestProductConfig: RequestProductConstants;
    @Input()
    set searchTerm(value: string) {
        this.productText = value || "";
    }

    get searchTerm() {
        return this.productText;
    }

    @Output() handleCloseModal: EventEmitter<boolean> = new EventEmitter();

    productText: string;
    showError = false;

    constructor(
        private searchPageService: SearchPageService,
        private toastService: ToastService
    ) {}

    get isError() {
        return this.showError && this.requestProductConfig.error;
    }

    onChange(event: any) {
        this.showError = false;
        const { value = "" } = event && event.target;
        this.productText = value.trim();
    }

    onSubmit() {
        if (this.productText) {
            this.searchPageService.createProduct({
                type: REQUEST_PRODUCT_TYPE,
                text: this.productText,
            });
            this.closeModal(true);
            this.toastService.present(TOAST_MESSAGES.REQUEST_PRODUCT);
        } else {
            this.showError = true;
        }
    }

    resetState() {
        this.showError = false;
    }

    closeModal(value: boolean = false) {
        this.resetState();
        this.handleCloseModal.emit(value);
    }
}
