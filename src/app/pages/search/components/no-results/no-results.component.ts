import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    OnInit,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { SettingsService } from "@services/shared/settings.service";

import { RequestProductConstants } from "@pages/search/types";
import { REQUEST_PRODUCT } from "@pages/search/constants";

@Component({
    selector: "supr-search-no-results",
    template: `
        <div
            class="noresults"
            saImpression
            [saObjectName]="saObjectName"
            [saObjectValue]="searchTerm"
        >
            <supr-text type="caption">
                {{ requestProductConfig.noResults }} "{{ searchTerm }}"
            </supr-text>

            <div class="divider24"></div>

            <div
                *ngIf="requestProductConfig.enableRequestProduct"
                class="requestProductBlock"
            >
                <supr-text type="subheading">{{
                    requestProductConfig.title
                }}</supr-text>

                <div class="divider12"></div>

                <supr-text class="subTitle" type="regular14">
                    {{ requestProductConfig.subTitle }}
                </supr-text>

                <div class="divider24"></div>

                <supr-button (handleClick)="onRequestButtonClick()">
                    {{ requestProductConfig.requestButtonText }}
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: [
        "../../styles/search.page.scss",
        "../../styles/request.product.scss",
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoResultsComponent implements OnInit {
    @Input() searchTerm: string;

    @Output() handleShowRequestModal: EventEmitter<
        boolean
    > = new EventEmitter();

    requestProductConfig: RequestProductConstants = {};
    saObjectName = ANALYTICS_OBJECT_NAMES.IMPRESSION.NO_PRODUCT_FOUND;

    constructor(private settingService: SettingsService) {}

    ngOnInit() {
        this.fetchRequestProductConfig();
    }

    onRequestButtonClick() {
        this.handleShowRequestModal.emit(true);
    }

    private fetchRequestProductConfig() {
        this.requestProductConfig = <RequestProductConstants>(
            this.settingService.getSettingsValue(
                "requestProduct",
                REQUEST_PRODUCT
            )
        );
    }
}
