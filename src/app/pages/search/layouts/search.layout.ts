import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-search-layout",
    template: `
        <div class="suprContainer">
            <supr-search-header
                [searchTerm]="searchTerm"
                [isExperimentsFetched]="isExperimentsFetched"
                (handleChangeText)="updateTerm($event)"
            ></supr-search-header>

            <supr-search-content-container
                [searchTerm]="searchTerm"
                (handleUpdateTerm)="updateTerm($event)"
            ></supr-search-content-container>
            <supr-cart></supr-cart>
        </div>
    `,
    styleUrls: ["../styles/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageLayoutComponent {
    @Input() isExperimentsFetched: boolean;

    searchTerm = "";

    updateTerm(searchTerm: string = "") {
        this.searchTerm = searchTerm;
    }
}
