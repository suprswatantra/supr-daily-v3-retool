import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";

import { FetchSimilarProductsActionData } from "@types";

import {
    RouterStoreSelectors,
    StoreState,
    SkuStoreActions,
    SkuStoreSelectors,
} from "@supr/store";

@Injectable()
export class OOSAlternatesPageAdapter {
    constructor(private store: Store<StoreState>) {}

    urlParams$ = this.store.pipe(select(RouterStoreSelectors.selectUrlParams));
    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    loading$ = this.store.pipe(
        select(SkuStoreSelectors.selectSimilarProductsFetchStatus)
    );

    similarProducts$ = this.store.pipe(
        select(SkuStoreSelectors.selectSimilarProducts)
    );

    fetchSimilarProducts(data: FetchSimilarProductsActionData) {
        if (data && data.skuId) {
            this.store.dispatch(
                new SkuStoreActions.FetchSimilarProductsRequestAction(data)
            );
        }
    }

    resetSimilarProductsList() {
        this.store.dispatch(
            new SkuStoreActions.ResetSimilarProductsListAction()
        );
    }
}
