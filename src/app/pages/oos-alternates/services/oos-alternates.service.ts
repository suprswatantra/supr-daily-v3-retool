import { Injectable } from "@angular/core";

import { CART_ITEM_TYPES } from "@constants";

import { CartItem, Sku, OutOfStockPreferredFlowType } from "@models";

import { Segment } from "@types";

import { ANALYTICS_TEXTS } from "../constants/analytics.constants";

@Injectable()
export class OOSAlternatesPageService {
    private parentSkuId: number;
    private oosAnalyticsId: string;

    setOOSAnalyticsId(parentSkuId: number) {
        this.parentSkuId = parentSkuId;
        this.oosAnalyticsId = `${parentSkuId}_${Date.now()}`;
    }

    getOOSAnalyticsId(): string {
        return this.oosAnalyticsId;
    }

    getPreferredFlow(
        sku: Sku,
        cartItem: CartItem
    ): OutOfStockPreferredFlowType | "" {
        if (sku && sku.out_of_stock_preferred_flow && cartItem) {
            return cartItem.type === CART_ITEM_TYPES.ADDON
                ? sku.out_of_stock_preferred_flow.addon
                : sku.out_of_stock_preferred_flow.subscription;
        }
        return "";
    }

    getOOSAnalytics(sku: Sku, cartItem: CartItem): Segment.ContextListItem[] {
        const contextList: Segment.ContextListItem[] = [];

        if (sku && cartItem) {
            contextList.push(
                {
                    name: ANALYTICS_TEXTS.UNIT_MRP,
                    value: sku.unit_mrp,
                },
                {
                    name: ANALYTICS_TEXTS.UNIT_PRICE,
                    value: sku.unit_price,
                },
                {
                    name: ANALYTICS_TEXTS.TYPE,
                    value: cartItem.type,
                }
            );

            if (cartItem.type === CART_ITEM_TYPES.ADDON) {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.SCHEDULE_DATE,
                        value: cartItem.delivery_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QUANTITY,
                        value: cartItem.quantity,
                    }
                );
            } else {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.START_DATE,
                        /* For subscription flow, we won't be having the delivery data mostly.
                        Hence use the default date provided by cart service */
                        value: cartItem.start_date || cartItem.delivery_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QTY_PER_DELIVERY,
                        value: cartItem.quantity,
                    }
                );
            }

            contextList.push(
                {
                    name: ANALYTICS_TEXTS.RECHARGE_QTY,
                    value: cartItem.recharge_quantity || 0,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_SKU_ID,
                    value: this.parentSkuId,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_PARENT_ID,
                    value: this.oosAnalyticsId,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_PREFERRED_FLOW,
                    value: this.getPreferredFlow(sku, cartItem),
                },
                {
                    name: ANALYTICS_TEXTS.OOS_AVAILABLE_DATE,
                    value: sku.next_available_date,
                }
            );
        }

        return contextList;
    }
}
