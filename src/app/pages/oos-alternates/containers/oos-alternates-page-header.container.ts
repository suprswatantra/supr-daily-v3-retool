import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SkuAdapter } from "@shared/adapters/sku.adapter";

import { Sku } from "@shared/models";

@Component({
    selector: "supr-oos-alternate-page-header-container",
    template: `
        <supr-oos-alternates-header
            [sku]="sku$ | async"
        ></supr-oos-alternates-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSAlternatePageHeaderContainer implements OnInit {
    @Input() skuId: number;

    sku$: Observable<Sku>;

    constructor(private skuAdapter: SkuAdapter) {}

    ngOnInit() {
        this.sku$ = this.skuAdapter.getSkuDetails(this.skuId);
    }
}
