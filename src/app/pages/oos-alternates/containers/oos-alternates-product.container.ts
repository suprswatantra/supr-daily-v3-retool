import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { CartAdapter } from "@shared/adapters/cart.adapter";
import { PauseAdapter } from "@shared/adapters/pause.adapter";

import { Sku, CartItem, Vacation } from "@shared/models";

import { OOSAlternatesPageAdapter } from "../services/oos-alternates.adapter";

@Component({
    selector: "supr-oos-alternate-product-container",
    template: `
        <supr-oos-alternates-product-tile
            [sku]="sku$ | async"
            [cartItem]="cartItem$ | async"
            [vacation]="vacation$ | async"
            [intendedType]="intent$ | async"
            [saPosition]="saPosition"
        ></supr-oos-alternates-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSAlternateProductContainer implements OnInit {
    @Input() skuId: number;
    @Input() saPosition: number;

    sku$: Observable<Sku>;

    intent$: Observable<string>;
    cartItem$: Observable<CartItem>;
    vacation$: Observable<Vacation>;

    constructor(
        private skuAdapter: SkuAdapter,
        private cartAdapter: CartAdapter,
        private pauseAdapter: PauseAdapter,
        private adapter: OOSAlternatesPageAdapter
    ) {}

    ngOnInit() {
        this.vacation$ = this.pauseAdapter.vacation$;
        this.sku$ = this.skuAdapter.getSkuDetails(this.skuId);
        this.cartItem$ = this.cartAdapter.getCartItemById(this.skuId);
        this.intent$ = this.adapter.queryParams$.pipe(
            map((params: Params) => {
                return params["intent"];
            })
        );
    }
}
