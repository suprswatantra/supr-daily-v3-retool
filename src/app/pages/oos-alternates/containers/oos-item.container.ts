import {
    Input,
    OnInit,
    OnChanges,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { Segment } from "@types";

import { CartService } from "@services/shared/cart.service";
import { CartAdapter } from "@shared/adapters/cart.adapter";

import { Sku, CartItem, Vacation } from "@shared/models";

import { SA_CONTEXT_VALUES } from "../constants/analytics.constants";

import { OOSAlternatesPageService } from "@pages/oos-alternates/services/oos-alternates.service";

@Component({
    selector: "supr-oos-item-container",
    template: `
        <supr-product-tile
            [sku]="sku"
            [cartItem]="cartItem$ | async"
            saContext="${SA_CONTEXT_VALUES.PARENT_PRODUCT}"
            [saContextList]="saContextList"
        ></supr-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSItemContainer implements OnInit, OnChanges {
    @Input() sku: Sku;
    /* Whether the user wanted to subscribe or get a one time delivery for the oos product */
    @Input() intendedType: string;
    @Input() vacation: Vacation;

    cartItem$: Observable<CartItem>;
    _oosIdCreated = false;
    saContextList: Segment.ContextListItem[] = [];

    constructor(
        private cartAdapter: CartAdapter,
        private cartService: CartService,
        private oosAlternatesService: OOSAlternatesPageService
    ) {}

    ngOnInit() {
        this.cartItem$ = this.cartAdapter.getCartItemById(this.sku.id);
    }

    ngOnChanges() {
        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        if (this.sku && this.intendedType) {
            const cartItem = this.cartService.getCartItemFromSku(
                this.sku,
                1,
                this.vacation
            );

            /* Set the type to addon or subscription_new based on intent */
            if (this.intendedType) {
                cartItem.type = this.intendedType;
            }

            this.saContextList = this.oosAlternatesService.getOOSAnalytics(
                this.sku,
                cartItem
            );
        }
    }
}
