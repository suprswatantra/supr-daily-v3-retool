import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { CartAdapter } from "@shared/adapters/cart.adapter";
import { OOSAlternatesPageAdapter } from "../services/oos-alternates.adapter";

@Component({
    selector: "supr-oos-alternates-container",
    template: `
        <supr-oos-alternates-layout
            [skuId]="skuId$ | async"
            [loading]="loading$ | async"
            [isCartEmpty]="isCartEmpty$ | async"
            [similarProducts]="similarProducts$ | async"
            (fetchSimilarProducts)="fetchSimilarProducts($event)"
            (resetSimilarProductsList)="resetSimilarProductsList()"
        ></supr-oos-alternates-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSAlternatesPageContainer {
    skuId$: Observable<number>;
    loading$: Observable<boolean>;
    isCartEmpty$: Observable<boolean>;
    similarProducts$: Observable<number[]>;

    constructor(
        private adapter: OOSAlternatesPageAdapter,
        private cartAdapter: CartAdapter
    ) {
        this.loading$ = this.adapter.loading$;
        this.isCartEmpty$ = this.cartAdapter.isCartEmpty$;
        this.similarProducts$ = this.adapter.similarProducts$;
        this.skuId$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["skuId"])
        );
    }

    fetchSimilarProducts(skuId: number) {
        this.adapter.fetchSimilarProducts({ skuId });
    }

    resetSimilarProductsList() {
        this.adapter.resetSimilarProductsList();
    }
}
