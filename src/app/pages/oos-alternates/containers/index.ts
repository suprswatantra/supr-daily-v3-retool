import { OOSAlternatePageHeaderContainer } from "./oos-alternates-page-header.container";
import { OOSAlternatesPageContainer } from "./oos-alternates.container";
import { OOSAlternateProductContainer } from "./oos-alternates-product.container";
import { OOSItemContainer } from "./oos-item.container";

export const oosalternatesContainers = [
    OOSAlternatesPageContainer,
    OOSAlternateProductContainer,
    OOSItemContainer,
    OOSAlternatePageHeaderContainer,
];
