import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-oos-alternates-ribbon",
    template: `
        <supr-ribbon>
            <div class="suprRow top">
                <supr-icon name="error"></supr-icon>
                <div class="spacer12"></div>
                <div class="suprColumn top left">
                    <supr-text class="title" type="subheading">
                        ${TEXTS.RIBBON_TITLE}
                    </supr-text>
                    <supr-text class="subtitle" type="caption">
                        ${TEXTS.RIBBON_SUBTITLE}
                    </supr-text>
                </div>
            </div>
        </supr-ribbon>
    `,
    styleUrls: ["./ribbon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RibbonComponent {}
