import { ProductCardComponent } from "./product-card.component";
import { WrapperComponent } from "./components/wrapper.component";

export const productCardComponents = [ProductCardComponent, WrapperComponent];
