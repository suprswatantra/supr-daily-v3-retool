import { Params } from "@angular/router";
import {
    Input,
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { PauseAdapter } from "@shared/adapters/pause.adapter";

import { Sku, Vacation } from "@shared/models";

import { OOSAlternatesPageAdapter } from "@pages/oos-alternates/services/oos-alternates.adapter";

@Component({
    selector: "supr-oos-alternates-product-card",
    template: `
        <supr-oos-alternates-product-card-wrapper
            [sku]="sku$ | async"
            [vacation]="vacation$ | async"
            [intendedType]="intent$ | async"
            (handleContainerHeaderBgColor)="handleContainerHeaderBgColor.emit()"
        >
        </supr-oos-alternates-product-card-wrapper>
    `,
    styleUrls: ["./product-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductCardComponent {
    @Input() set skuId(id: number) {
        this.sku$ = this.skuAdapter.getSkuDetails(id);
    }

    @Output() handleContainerHeaderBgColor: EventEmitter<
        void
    > = new EventEmitter();

    sku$: Observable<Sku>;
    intent$: Observable<string>;
    vacation$: Observable<Vacation>;

    constructor(
        private skuAdapter: SkuAdapter,
        private pauseAdapter: PauseAdapter,
        private adapter: OOSAlternatesPageAdapter
    ) {
        this.vacation$ = this.pauseAdapter.vacation$;
        this.intent$ = this.adapter.queryParams$.pipe(
            map((params: Params) => {
                return params["intent"];
            })
        );
    }
}
