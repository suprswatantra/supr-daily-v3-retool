import {
    Input,
    Component,
    OnChanges,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Sku, Vacation } from "@shared/models";

import { DateService } from "@services/date/date.service";
import { CartService } from "@services/shared/cart.service";
import { CalendarService } from "@services/date/calendar.service";
import { QuantityService } from "@services/util/quantity.service";

import { Segment } from "@types";

import { TEXTS } from "../../../constants";
import {
    SA_OBJECT_NAMES,
    SA_CONTEXT_VALUES,
} from "../../../constants/analytics.constants";
import { OOSAlternatesPageService } from "@pages/oos-alternates/services/oos-alternates.service";

@Component({
    selector: "supr-oos-alternates-product-card-wrapper",
    template: `
        <div
            class="wrapper"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.OUT_OF_STOCK_SKU}"
            [saObjectValue]="sku?.id"
            saContext="${SA_CONTEXT_VALUES.PARENT_PRODUCT}"
            [saContextList]="saContextList"
        >
            <supr-oos-alternates-product-container-header
                *ngIf="availableDate"
                [availableDate]="availableDate"
                [isOutOfStock]="sku?.out_of_stock"
            ></supr-oos-alternates-product-container-header>

            <div class="wrapperContainer">
                <div class="wrapperContainerSpace">
                    <ng-container *ngIf="availableDate; else oos">
                        <supr-oos-item-container
                            [sku]="sku"
                            [intendedType]="intendedType"
                            [vacation]="vacation"
                        ></supr-oos-item-container>

                        <div class="divider16"></div>
                        <div class="suprRow" class="horizontalSeparator"></div>
                        <div class="divider16"></div>

                        <supr-oos-info
                            [sku]="sku"
                            [isSingleLine]="true"
                            [saContextList]="saContextList"
                        ></supr-oos-info>
                    </ng-container>

                    <ng-template #oos>
                        <div
                            class="titleWrapper suprColumn left"
                            *ngIf="!availableDate"
                        >
                            <div class="suprRow top">
                                <div class="spacer8"></div>
                                <div class="suprColumn left">
                                    <supr-text type="body">
                                        ${TEXTS.PRODUCT_CARD_TITLE}
                                    </supr-text>
                                    <div class="suprRow">
                                        <supr-text
                                            type="caption"
                                            class="subtitle"
                                        >
                                            ${TEXTS.PRODUCT_CARD_UNAVAILABLE_SUBTITLE}
                                        </supr-text>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="productWrapper suprRow top item">
                            <div class="suprColumn itemImage">
                                <supr-image
                                    [src]="sku?.image?.fullUrl"
                                    [image]="sku?.image"
                                ></supr-image>
                            </div>

                            <div class="suprColumn itemContent left">
                                <supr-text class="itemName" type="body">
                                    {{ sku?.sku_name }}
                                </supr-text>
                                <div class="divider4"></div>
                                <supr-text class="itemQty" type="caption">
                                    {{ _displayQty }}
                                </supr-text>
                                <div class="divider8"></div>

                                <div
                                    class="suprRow spaceBetween itemPrice bottom"
                                >
                                    <div class="suprRow">
                                        <supr-text type="body">
                                            {{ sku?.unit_price | rupee: 2 }}
                                        </supr-text>

                                        <ng-container *ngIf="_showPriceStrike">
                                            <div class="spacer8"></div>
                                            <supr-text
                                                class="itemPriceStrike"
                                                type="caption"
                                            >
                                                {{ sku?.unit_mrp | rupee: 2 }}
                                            </supr-text>
                                        </ng-container>
                                    </div>

                                    <supr-button class="oos">
                                        <div class="suprRow center">
                                            <supr-text type="subtext11">
                                                Out of stock
                                            </supr-text>
                                        </div>
                                    </supr-button>
                                </div>
                            </div>
                        </div>

                        <div class="divider16"></div>
                        <div class="suprRow" class="horizontalSeparator"></div>
                        <div class="divider16"></div>

                        <supr-oos-info
                            [sku]="sku"
                            [isSingleLine]="true"
                            [saContextList]="saContextList"
                        ></supr-oos-info>
                    </ng-template>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../product-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() set sku(data: Sku) {
        this._sku = data;
        this.setMetaData();
    }
    get sku(): Sku {
        return this._sku;
    }

    @Input() vacation: Vacation;
    /* Whether the user wanted to subscribe or get a one time delivery for the oos product */
    @Input() intendedType: string;

    @Output() handleContainerHeaderBgColor: EventEmitter<
        void
    > = new EventEmitter();

    _displayQty: string;
    _oosIdCreated = false;
    _showPriceStrike = false;
    saContextList: Segment.ContextListItem[] = [];
    availableDate: string;

    private _sku: Sku;

    constructor(
        private cartService: CartService,
        private quantityService: QuantityService,
        private oosAlternatesService: OOSAlternatesPageService,
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    ngOnChanges() {
        this.setAnalyticsData();
    }

    private setMetaData() {
        if (this.sku) {
            this._displayQty = this.quantityService.toText(this.sku);
            this._showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
            this.setAvailableDate();
            this.handleContainerBgColor();
        }
    }

    private setAvailableDate() {
        if (this.sku.next_available_date) {
            this.availableDate = this.dateService.textFromDate(
                this.calendarService.getNextAvailableDate(null, this.sku)
            );
        }
    }

    private setAnalyticsData() {
        if (this.sku && this.intendedType) {
            /* Set the OOSAnalyticsID only once and the same will be used across the page */
            if (!this._oosIdCreated) {
                this.oosAlternatesService.setOOSAnalyticsId(this.sku.id);
                this._oosIdCreated = true;
            }

            const cartItem = this.cartService.getCartItemFromSku(
                this.sku,
                1,
                this.vacation
            );

            /* Set the type to addon or subscription_new based on intent */
            if (this.intendedType) {
                cartItem.type = this.intendedType;
            }

            this.saContextList = this.oosAlternatesService.getOOSAnalytics(
                this.sku,
                cartItem
            );
        }
    }

    private handleContainerBgColor() {
        if (!this.availableDate) {
            this.handleContainerHeaderBgColor.emit();
        }
    }
}
