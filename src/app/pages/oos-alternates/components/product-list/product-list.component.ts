import { Input, Component, ChangeDetectionStrategy } from "@angular/core";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
@Component({
    selector: "supr-oos-alternates-product-list",
    template: `
        <ng-container *ngIf="!loading && products && products.length">
            <div
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.OOS_SUGGESTIONS}"
                [saObjectValue]="products?.length"
            >
                <div
                    class="wrapper"
                    *ngFor="
                        let product of products;
                        index as position;
                        trackBy: trackByFn
                    "
                >
                    <supr-oos-alternate-product-container
                        [skuId]="product"
                        [saPosition]="position"
                    ></supr-oos-alternate-product-container>
                </div>
            </div>
        </ng-container>

        <ng-container *ngIf="loading">
            <supr-oos-alternates-product-list-skeleton>
            </supr-oos-alternates-product-list-skeleton>
        </ng-container>
    `,
    styleUrls: ["./product-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListComponent {
    @Input() loading: boolean;
    @Input() products: number[];

    trackByFn(skuId: number): number {
        return skuId;
    }
}
