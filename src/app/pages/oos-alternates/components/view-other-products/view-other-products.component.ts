import { Component, ChangeDetectionStrategy } from "@angular/core";

import { RouterService } from "@services/util/router.service";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-oos-alternates-view-other",
    template: `
        <supr-button (handleClick)="goBack()">
            <supr-text type="body"> ${TEXTS.CLOSE} </supr-text>
        </supr-button>
    `,
    styleUrls: ["./view-other-products.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewOtherProductsComponent {
    constructor(private routerService: RouterService) {}

    goBack() {
        this.routerService.goBack();
    }
}
