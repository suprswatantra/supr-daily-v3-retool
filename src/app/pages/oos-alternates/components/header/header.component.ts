import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { Sku } from "@shared/models";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-oos-alternates-header",
    template: `
        <supr-page-header>
            <supr-text type="body">
                {{ headerText }}
            </supr-text>
        </supr-page-header>
    `,
    styles: [
        `
            supr-page-header {
                supr-text {
                    --supr-text-color: var(--black-40);
                }
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    @Input() set sku(value: Sku) {
        if (value) {
            this.setHeaderText(value);
        }
    }

    headerText = "";

    private setHeaderText(sku: Sku) {
        if (sku.out_of_stock && sku.next_available_date) {
            this.headerText = TEXTS.HEADER_TEXT_WITH_DATE;
        } else {
            this.headerText = TEXTS.HEADER_TEXT;
        }
    }
}
