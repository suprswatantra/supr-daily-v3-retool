import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ScheduleService } from "@services/shared/schedule.service";
import { DateService } from "@services/date/date.service";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-oos-alternates-product-container-header",
    template: `
        <div
            *ngIf="_showHeader"
            class="suprRow header"
            [ngClass]="getHeaderClass()"
        >
            <div class="suprRow adOosHeader">
                <div
                    class="suprColumn stretch headerDate"
                    [ngClass]="getClass()"
                >
                    <supr-schedule-wrapper-date
                        [date]="availableDate"
                        *ngIf="availableDate"
                    ></supr-schedule-wrapper-date>
                </div>

                <div *ngIf="!similarProductsHeaderText">
                    <div class="suprRow left">
                        <div class="suprRow">
                            <supr-text type="heading" class="infoText">
                                ${TEXTS.AVAILABLE_FOR}
                            </supr-text>
                            <div class="spacer4"></div>
                            <supr-text
                                type="subtitle"
                                class="dayText headerTitle"
                            >
                                {{ _day }}
                            </supr-text>
                        </div>
                    </div>

                    <div class="headerSubtitle">
                        <supr-text type="caption">
                            ${TEXTS.EARLIEST_AVAILABLE_DATE}
                        </supr-text>
                    </div>
                </div>

                <div class="suprColumn left" *ngIf="similarProductsHeaderText">
                    <div class="suprRow">
                        <supr-text type="heading" class="infoText">
                            ${TEXTS.AVAILABLE_FOR}
                        </supr-text>
                        <div class="spacer4"></div>
                        <supr-text type="subtitle" class="dayText headerTitle">
                            ${TEXTS.TOMORROW}
                        </supr-text>
                    </div>

                    <div class="headerSubtitle">
                        <supr-text type="caption">
                            ${TEXTS.ALTERNATE_OPTIONS}
                        </supr-text>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/container-header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerHeaderComponent implements OnInit {
    @Input() availableDate: string;
    @Input() similarProductsHeaderText: string;
    @Input() isOutOfStock: boolean;
    @Input() showHeaderBgColor = false;

    _day: string;
    _showHeader = false;

    constructor(
        private scheduleService: ScheduleService,
        private dateService: DateService
    ) {}

    ngOnInit() {
        this.setDateInfo();
        this.showHeader();
    }

    getClass() {
        if (this.availableDate) {
            return {
                top: true,
                left: true,
            };
        } else {
            return {
                center: true,
            };
        }
    }

    getHeaderClass() {
        if (this.similarProductsHeaderText && !this.showHeaderBgColor) {
            return {
                similarProducts: true,
            };
        } else if (this.showHeaderBgColor) {
            return {
                similarProductsLightBg: true,
            };
        }
    }

    private setDateInfo() {
        if (this.availableDate) {
            this._day = this.scheduleService.getScheduleDay(this.availableDate);
        } else if (this.similarProductsHeaderText) {
            this.availableDate = this.dateService.textFromDate(
                this.dateService.getTomorrowDate()
            );
        }
    }

    private showHeader() {
        if (
            this.similarProductsHeaderText ||
            this.availableDate ||
            this.isOutOfStock
        ) {
            this._showHeader = true;
        }
    }
}
