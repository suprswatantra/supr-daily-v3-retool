import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-oos-alternates-product-list-skeleton",
    template: `
        <ng-container *ngFor="let item of items">
            <div class="divider16"></div>
            <div class="body suprRow">
                <div class="suprColumn left image">
                    <ion-skeleton-text
                        animated
                        class="square"
                    ></ion-skeleton-text>
                </div>
                <div class="suprColum space"></div>
                <div class="suprColumn stretch left info">
                    <ion-skeleton-text
                        animated
                        class="first"
                    ></ion-skeleton-text>
                    <ion-skeleton-text
                        animated
                        class="second"
                    ></ion-skeleton-text>
                    <ion-skeleton-text
                        animated
                        class="third"
                    ></ion-skeleton-text>
                </div>
            </div>
            <div class="divider16"></div>
        </ng-container>
    `,
    styleUrls: ["./product-list-skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListSkeletonComponent {
    items = [1, 2, 3, 4];
}
