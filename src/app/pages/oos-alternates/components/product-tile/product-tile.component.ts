import {
    Input,
    OnChanges,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Sku, CartItem, Vacation } from "@shared/models";

import { CartService } from "@services/shared/cart.service";

import { Segment } from "@types";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

import { OOSAlternatesPageService } from "@pages/oos-alternates/services/oos-alternates.service";

@Component({
    selector: "supr-oos-alternates-product-tile",
    template: `
        <supr-product-tile
            [sku]="sku"
            [cartItem]="cartItem"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
            saContext="${SA_OBJECT_NAMES.IMPRESSION.OUT_OF_STOCK_SKU}"
        ></supr-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSProductTileComponent implements OnChanges {
    @Input() sku: Sku;
    @Input() cartItem: CartItem;
    @Input() vacation: Vacation;
    @Input() intendedType: string;

    @Input() saPosition: number;

    saContextList: Segment.ContextListItem[];

    constructor(
        private cartService: CartService,
        private oosAlternatesService: OOSAlternatesPageService
    ) {}

    ngOnChanges() {
        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        if (this.sku && this.intendedType) {
            /* Set the OOSAnalyticsID only once and the same will be used across the page */

            const cartItem =
                this.cartItem ||
                this.cartService.getCartItemFromSku(this.sku, 1, this.vacation);

            /* Set the type to addon or subscription_new based on intent only if cart item
            item is not added in cart. If it is, then let the type be that from cartItem */
            if (!this.cartItem && this.intendedType) {
                cartItem.type = this.intendedType;
            }

            this.saContextList = this.oosAlternatesService.getOOSAnalytics(
                this.sku,
                cartItem
            );
        }
    }
}
