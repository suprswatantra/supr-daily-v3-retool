import { productCardComponents } from "./product-card";
import { RibbonComponent } from "./ribbon/ribbon.component";
import { HeaderComponent } from "./header/header.component";
import { ContainerHeaderComponent } from "./header/container-header.component";
import { ProductListComponent } from "./product-list/product-list.component";
import { OOSProductTileComponent } from "./product-tile/product-tile.component";
import { ViewOtherProductsComponent } from "./view-other-products/view-other-products.component";
import { ProductListSkeletonComponent } from "./product-list-skeleton/product-list-skeleton.component";
import { LoadMoreComponent } from "./load-more/load-more.component";

export const oosAlternatesComponents = [
    RibbonComponent,
    HeaderComponent,
    ContainerHeaderComponent,
    ProductListComponent,
    OOSProductTileComponent,
    ...productCardComponents,
    ViewOtherProductsComponent,
    ProductListSkeletonComponent,
    LoadMoreComponent,
];
