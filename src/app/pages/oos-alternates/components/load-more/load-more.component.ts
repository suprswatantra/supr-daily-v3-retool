import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-oos-alternates-load-more",
    template: `
        <div class="suprRow center loadMore" (click)="handleLoadMore.emit()">
            <supr-icon name="chevron_down"></supr-icon>
            <supr-text type="paragraph"> Load more </supr-text>
        </div>
    `,
    styleUrls: ["./load-more.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadMoreComponent {
    @Output() handleLoadMore: EventEmitter<void> = new EventEmitter();
}
