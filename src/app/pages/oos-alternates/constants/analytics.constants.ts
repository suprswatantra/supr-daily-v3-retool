export const SA_OBJECT_NAMES = {
    IMPRESSION: {
        OOS_SUGGESTIONS: "oos-suggestions",
        OUT_OF_STOCK_SKU: "out-of-stock-sku",
    },
};

export const SA_OBJECT_VALUES = {};

export const SA_CONTEXT_VALUES = {
    PARENT_PRODUCT: "oos-parent",
};

export const ANALYTICS_TEXTS = {
    TYPE: "type",
    QUANTITY: "quantity",
    UNIT_MRP: "unit-mrp",
    UNIT_PRICE: "unit-price",
    START_DATE: "start-date",
    RECHARGE_QTY: "recharge-qty",
    SCHEDULE_DATE: "schedule-date",
    QTY_PER_DELIVERY: "qty-per-delivery",
    OOS_SKU_ID: "oos-sku-id",
    OOS_PARENT_ID: "oos-parent-id",
    OOS_PREFERRED_FLOW: "preferred-flow",
    OOS_AVAILABLE_DATE: "next-available-date",
    NOTIFY_AVAILABLE: "notify_available",
    OOS_NOTIFIED: "notified",
};
