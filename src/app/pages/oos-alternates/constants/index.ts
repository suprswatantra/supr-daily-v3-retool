export const TEXTS = {
    HEADER_TEXT: "Item out of stock",
    HEADER_TEXT_WITH_DATE: "Delayed delivery",
    RIBBON_TITLE: "Item currently unavailable",
    RIBBON_SUBTITLE: "Please choose an alternate item or check back later",
    PRODUCT_CARD_TITLE: "Item currently unavailable",
    PRODUCT_CARD_UNAVAILABLE_SUBTITLE:
        "Due to high demand this product is currently unavailable",
    PRODUCT_CARD_TITLE_WITH_DATE: "Earliest available date for this item",
    PRODUCT_CARD_SUBTITLE_WITH_DATE:
        "Due to high demand this product is not available at an earlier date",
    PRODUCT_CARD_LABEL: "Out of stock",
    ITEMS_HEADER: "Replace with similar item",
    FOOTER_CHIP_TEXT: "Back to previous page",
    EARLIEST_AVAILABLE_DATE: "Earliest available date",
    AVAILABLE_FOR: "Available for",
    TOMORROW: "Tomorrow",
    ALTERNATE_OPTIONS: "Alternate options",
    CLOSE: "CLOSE",
    LOAD_MORE: "Load more",
};

export const MINIMUM_LENGTH_LOAD_MORE = 4;
