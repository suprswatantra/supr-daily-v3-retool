import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { oosalternatesContainers } from "./containers";
import { oosAlternatesComponents } from "./components";
import { OOSAlternatesPageLayoutComponent } from "./layouts/oos-alternates.layout";
import { OOSAlternatesPageContainer } from "./containers/oos-alternates.container";

import { OOSAlternatesPageAdapter } from "./services/oos-alternates.adapter";
import { OOSAlternatesPageService } from "./services/oos-alternates.service";

const routes: Routes = [
    {
        path: "",
        component: OOSAlternatesPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        OOSAlternatesPageLayoutComponent,
        ...oosAlternatesComponents,
        ...oosalternatesContainers,
    ],
    providers: [OOSAlternatesPageAdapter, OOSAlternatesPageService],
})
export class OOSAlternatesPageModule {}
