import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
    OnDestroy,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { SettingsService } from "@services/shared/settings.service";

import { MINIMUM_LENGTH_LOAD_MORE } from "../constants";

@Component({
    selector: "supr-oos-alternates-layout",
    template: `
        <div class="suprContainer">
            <supr-oos-alternate-page-header-container
                [skuId]="skuId"
            ></supr-oos-alternate-page-header-container>
            <div class="suprScrollContent" [class.cartPresent]="!isCartEmpty">
                <!-- <div class="divider16"></div>
                <supr-oos-alternates-ribbon></supr-oos-alternates-ribbon> -->
                <supr-oos-alternates-product-card
                    [skuId]="skuId"
                    (handleContainerHeaderBgColor)="containerHeaderBgColor()"
                ></supr-oos-alternates-product-card>

                <div
                    class="divider16"
                    *ngIf="_similarProducts && _similarProducts.length"
                ></div>

                <supr-oos-alternates-product-container-header
                    *ngIf="_similarProducts && _similarProducts.length"
                    [similarProductsHeaderText]="alternativeProductsHeader"
                    [showHeaderBgColor]="_showHeaderBgColor"
                ></supr-oos-alternates-product-container-header>
                <div class="listContainer">
                    <supr-oos-alternates-product-list
                        [loading]="loading"
                        [products]="_similarProducts"
                    ></supr-oos-alternates-product-list>
                    <div class="divider8"></div>
                    <supr-oos-alternates-load-more
                        *ngIf="_loadMore"
                        (handleLoadMore)="loadMore()"
                    ></supr-oos-alternates-load-more>
                    <supr-oos-alternates-view-other></supr-oos-alternates-view-other>
                </div>
            </div>
            <supr-cart></supr-cart>
        </div>
    `,
    styleUrls: ["../styles/oos-alternates.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSAlternatesPageLayoutComponent
    implements OnInit, OnChanges, OnDestroy {
    @Input() set skuId(id: number) {
        this._skuId = id;
        this.fetchSimilarProducts.emit(id);
    }
    get skuId(): number {
        return this._skuId;
    }

    @Input() loading: boolean;
    @Input() isCartEmpty: boolean;
    @Input() similarProducts: number[];

    @Output() fetchSimilarProducts: EventEmitter<number> = new EventEmitter();
    @Output() resetSimilarProductsList: EventEmitter<void> = new EventEmitter();

    private _skuId: number;
    alternativeProductsHeader = "";
    _showHeaderBgColor = false;
    _loadMore = false;
    _similarProducts: number[] = [];
    _showMoreSimilarProductsBtn = false;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this.alternativeProductsHeader = this.settingsService.getSettingsValue(
            "alternativeProductsHeader",
            SETTINGS_KEYS_DEFAULT_VALUE.alternativeProductsHeader
        );

        this.setShowLoadMoreBtn();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["similarProducts"];
        if (this.canHandleChange(change)) {
            this.handleSimilarProductsChange(change);
        }
    }

    ngOnDestroy() {
        this.resetSimilarProductsList.emit();
    }

    containerHeaderBgColor() {
        this._showHeaderBgColor = true;
    }

    loadMore() {
        this.setLoadMore(false);
        this._similarProducts = this.similarProducts;
    }

    private canHandleChange(change: SimpleChange) {
        return change && !change.firstChange && change.currentValue;
    }

    private handleSimilarProductsChange(change: SimpleChange) {
        const similarProducts = change.currentValue;
        if (
            similarProducts.length > MINIMUM_LENGTH_LOAD_MORE &&
            this._showMoreSimilarProductsBtn
        ) {
            this.setLoadMore(true);
            this._similarProducts = similarProducts.slice(
                0,
                MINIMUM_LENGTH_LOAD_MORE
            );
        } else {
            this.setLoadMore(false);
            this._similarProducts = similarProducts;
        }
    }

    private setShowLoadMoreBtn() {
        this._showMoreSimilarProductsBtn = this.settingsService.getSettingsValue(
            "showMoreSimilarProductsBtn",
            false
        );
    }

    private setLoadMore(status: boolean) {
        this._loadMore = status;
    }
}
