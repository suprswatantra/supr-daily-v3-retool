import { REWARD_PAGE_TEXT } from "@pages/milestone/constants";
import { MilestonePastRewards } from "@models";
import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { MileStoneCurrentState } from "@store/milestone/milestone.state";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-milestone-rewards-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    {{ rewards }}
                </supr-text>
            </supr-page-header>

            <div class="suprScrollContent">
                <ng-container *ngIf="!loading; else loader">
                    <supr-milestone-rewards-content
                        [milestonePastRewards]="milestonePastRewards"
                    ></supr-milestone-rewards-content>
                </ng-container>
                <ng-template #loader>
                    <div class="suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardsLayoutComponent implements OnInit, OnChanges {
    @Input() milestonePastRewards: MilestonePastRewards;
    @Input() milsestoneCurrentState: string;
    @Output() handleFetchMileStoneRewards: EventEmitter<
        void
    > = new EventEmitter();

    loading = true;
    rewards = REWARD_PAGE_TEXT.REWARDS;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["milsestoneCurrentState"];
        if (change && !change.firstChange) {
            if (
                change.currentValue ===
                MileStoneCurrentState.FETCHING_MILESTONE_DONE
            ) {
                this.loading = false;
            }
        }
    }

    private init() {
        if (this.utilService.isEmpty(this.milestonePastRewards)) {
            this.handleFetchMileStoneRewards.emit();
        } else {
            this.loading = false;
        }
    }
}
