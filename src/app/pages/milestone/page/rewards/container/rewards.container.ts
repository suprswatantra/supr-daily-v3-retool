import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { MilestonePageAdapter as Adapter } from "@pages/milestone/services/milestone.adapter";
import { MilestonePastRewards } from "@models";

@Component({
    selector: "supr-milestone-rewards-container",
    template: `
        <supr-milestone-rewards-layout
            [milestonePastRewards]="mileStoneRewards$ | async"
            [milsestoneCurrentState]="milsestoneCurrentState$ | async"
            (handleFetchMileStoneRewards)="fetchMileStoneRewards()"
        ></supr-milestone-rewards-layout>
    `,
})
export class RewardsPageContainer implements OnInit {
    mileStoneRewards$: Observable<MilestonePastRewards>;
    milsestoneCurrentState$: Observable<string>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.mileStoneRewards$ = this.adapter.milsestoneRewards$;
        this.milsestoneCurrentState$ = this.adapter.milsestoneCurrentState$;
    }

    fetchMileStoneRewards() {
        this.adapter.fetchMileStonePastRewards();
    }
}
