import { UtilService } from "@services/util/util.service";
import { Image, MilestonePastRewards } from "@models";
import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";
import { SettingsService } from "@services/shared/settings.service";
import { REWARD_PAGE_TEXT, STATES } from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-rewards-content",
    template: `
        <div class="content">
            <div class="divider16"></div>

            <div class="suprColumn contentUpper">
                <div class="contentUpperHeader">
                    <supr-text-fragment
                        [textFragment]="
                            milestonePastRewards?.header_card?.title
                        "
                    ></supr-text-fragment>
                </div>
                <div class="suprRow spaceBetween contentUpperBody">
                    <div class="suprColumn contentUpperBodyContent left">
                        <supr-text-fragment
                            [textFragment]="
                                milestonePastRewards?.header_card?.body?.title
                            "
                            class="bodyText"
                        ></supr-text-fragment>
                        <div class="divider4"></div>
                        <supr-text-fragment
                            [textFragment]="
                                milestonePastRewards?.header_card?.body
                                    ?.subTitle
                            "
                            type="montserratBold36"
                            class="bodyTextSubtitle"
                        ></supr-text-fragment>
                        <div class="divider4"></div>
                        <supr-text-fragment
                            [textFragment]="
                                milestonePastRewards?.header_card?.body
                                    ?.bottomText
                            "
                            class="bodyText"
                        ></supr-text-fragment>
                    </div>

                    <supr-image [image]="completedImage"></supr-image>
                </div>
            </div>
            <div class="suprColumn left contentRewardsHistory">
                <div class="divider32"></div>
                <supr-text type="subheading"> {{ pastRewardText }} </supr-text>
                <div class="divider16"></div>
                <div
                    *ngFor="let item of milestonePastRewards?.reward_lists"
                    class="contentActivity"
                >
                    <supr-activity-block
                        [activityItem]="item"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/rewards.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit {
    @Input() milestonePastRewards: MilestonePastRewards;

    completedImage: Image;
    pastRewardText = REWARD_PAGE_TEXT.PAST_REWARDS;

    constructor(
        private settingService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.setCompletedTypeImage();
    }

    private setCompletedTypeImage() {
        const milestoneRewardType = this.settingService.getSettingsValue(
            "milestones_rewards",
            null
        );

        if (!this.utilService.isEmpty(milestoneRewardType)) {
            this.completedImage = milestoneRewardType[STATES.COMPLETED];
        }
    }
}
