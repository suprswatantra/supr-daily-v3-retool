import { RewardsPageContainer } from "./rewards/container/rewards.container";
import { RewardsLayoutComponent } from "./rewards/layouts/rewards.layout";
import { ContentComponent } from "./rewards/components/content/content.component";

export const milestonePages = [
    RewardsPageContainer,
    RewardsLayoutComponent,
    ContentComponent,
];
