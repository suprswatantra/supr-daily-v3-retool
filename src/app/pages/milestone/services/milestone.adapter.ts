import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    MileStoneStoreActions,
    MileStoneStoreSelectors,
} from "@supr/store";

@Injectable()
export class MilestonePageAdapter {
    mileStoneDetail$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectMileStoneDetails)
    );

    milsestoneCurrentState$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectMilestoneState)
    );

    milsestoneRewards$ = this.store.pipe(
        select(MileStoneStoreSelectors.selectStartMilestoneRewards)
    );

    constructor(private store: Store<StoreState>) {}

    fetchMileStoneDetail() {
        this.store.dispatch(
            new MileStoneStoreActions.FetchMileStoneDetailRequestAction()
        );
    }

    fetchMileStonePastRewards() {
        this.store.dispatch(
            new MileStoneStoreActions.FetchMileStoneRewardsRequestAction()
        );
    }

    postUserMilestone(userMilestoneId: number) {
        this.store.dispatch(
            new MileStoneStoreActions.PostMileStoneStartRequestAction({
                user_milestone_id: userMilestoneId,
            })
        );
    }
}
