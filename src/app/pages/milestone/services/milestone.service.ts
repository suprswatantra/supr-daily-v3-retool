import { Injectable } from "@angular/core";

import { MileStoneCta } from "@shared/models";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
@Injectable()
export class MileStoneService {
    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    handleEvent(cta: MileStoneCta) {
        const link = this.utilService.getNestedValue(cta, "link", null);

        if (link) {
            this.routerService.goToUrl(link);
        }
    }
}
