import { PAGE_ROUTES } from "@constants";
import { filter } from "rxjs/operators";
import { NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    OnDestroy,
} from "@angular/core";
import { Router } from "@angular/router";

import { MileStoneRewards } from "@shared/models";
import { MileStoneCurrentState } from "@store/milestone/milestone.state";
import { MILESTONE_TEXTS } from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${MILESTONE_TEXTS.MILESTONE_REWARDS_HEADER_TEXT}
                </supr-text>
            </supr-page-header>

            <div class="suprScrollContent">
                    <supr-milestone-header
                        [users_rewards]="mileStoneDetail?.users_rewards"
                    ></supr-milestone-header>

                    <ng-container *ngIf="!loading; else loader">
                        <supr-milestone-content
                            [mileStoneDetail]="mileStoneDetail"
                            [state]="mileStoneDetail?.users_rewards?.state"
                            (handleStateChange)="handleStateChange.emit($event)"
                        ></supr-milestone-content>

                        <supr-milestone-rewards
                            [rewardsList]="
                                mileStoneDetail?.users_rewards?.rewards_earned
                            "
                            [groupId]="mileStoneDetail?.milestone_group_id"
                            [state]="mileStoneDetail?.users_rewards?.state"
                        ></supr-milestone-rewards>

                        <supr-milestone-faq></supr-milestone-faq>
                </ng-container>
                <ng-template #loader>
                    <div class="suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-template>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../styles/milestone.page.scss"],
})
export class MilestoneLayoutComponent implements OnChanges, OnDestroy {
    @Input() mileStoneDetail: MileStoneRewards;
    @Input() milsestoneCurrentState: string;
    @Output() handleFetchMileStoneDetails: EventEmitter<
        void
    > = new EventEmitter();
    @Output() handleStateChange: EventEmitter<number> = new EventEmitter();

    loading = true;
    private milestoneRouterSub: Subscription;

    constructor(private router: Router) {
        this.subscribeToRouterEvents();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["milsestoneCurrentState"];
        if (change && !change.firstChange) {
            if (
                change.currentValue ===
                MileStoneCurrentState.FETCHING_MILESTONE_DETAILS_DONE
            ) {
                this.loading = false;
            }
        }
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    private subscribeToRouterEvents() {
        this.milestoneRouterSub = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isMilestonePage(event.url)
                )
            )
            .subscribe(() => {
                this.init();
            });
    }

    private init() {
        this.handleFetchMileStoneDetails.emit();
    }

    private isMilestonePage(url: string) {
        if (url) {
            const path = url.slice(1, url.length);
            return path === PAGE_ROUTES.MILESTONE.PATH;
        }

        return false;
    }

    private unsubscribeRouterEvents() {
        if (this.milestoneRouterSub && !this.milestoneRouterSub.closed) {
            this.milestoneRouterSub.unsubscribe();
        }
    }
}
