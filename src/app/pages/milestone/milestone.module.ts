import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";
import { SharedModule } from "@shared/shared.module";

import { MilestonePageContainer } from "./containers/milestone.container";
import { MilestoneLayoutComponent } from "./layouts/milestone.layout";
import { milestoneComponents } from "./components";
import { mileStoneServices } from "./services";
import { MilestoneRoutingModule } from "./milestone.routing.module";
import { MilestoneCoreLayoutComponent } from "./layouts/core.layout";
import { milestonePages } from "./page/index";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        MilestoneRoutingModule,
    ],
    declarations: [
        MilestonePageContainer,
        MilestoneLayoutComponent,
        MilestoneCoreLayoutComponent,
        ...milestoneComponents,
        milestonePages,
    ],
    providers: [...mileStoneServices],
})
export class LandingPageModule {}
