import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";
import { MilestonePageContainer } from "./containers/milestone.container";
import { MilestoneCoreLayoutComponent } from "./layouts/core.layout";
import { RewardsPageContainer } from "./page/rewards/container/rewards.container";

const MilestoneRoutes: Routes = [
    {
        path: "",
        component: MilestoneCoreLayoutComponent,
        children: [
            {
                path: "",
                component: MilestonePageContainer,
                data: {
                    screenName: SCREEN_NAMES.MILESTONE,
                },
            },
            {
                path: PAGE_ROUTES.MILESTONE.CHILDREN.REWARDS.PATH,
                component: RewardsPageContainer,
                data: {
                    screenName: SCREEN_NAMES.MILESTONE_REWARDS,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(MilestoneRoutes)],
    exports: [RouterModule],
})
export class MilestoneRoutingModule {}
