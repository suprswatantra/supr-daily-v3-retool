export const MILESTONE_TEXTS = {
    START_NOW: "START NOW",
    START_NOW_SUB_TEXT: "Start now to earn amazing rewards !",
    EXPIRED_TEXTS: {
        TITLE:
            " Uh-oh! You could not complete all the tasks in the given time.",
        SUB_TITLE: "Go to current tasks",
    },
    LOCKED: {
        TITLE: "Complete previous tasks to unlock this ",
    },
    COMPLETE: {
        TITLE:
            "All tasks completed for this milestone. You won 25 days supr access and 120 supr credits",
        SUB_TITLE: "Go to current tasks",
    },
    FAQ: "Frequently asked questions",
    MILESTONE: "milestone",
    MILESTONE_URL: 115,
    SEE_ALL_REWARDS: "See all rewards",
    REWARDS_EARNED: "Rewards Earned",
    MILESTONE_REWARDS_HEADER_TEXT: "Milestone Rewards",
    SCRATCH_CARDS_SURPRISE: "+ 2 Scratch cards and 1 Surprise Coupon",
};

export const BG_COLORS = {
    EXPIRED: "#EA755B",
    COMPLETED: "#6AC259",
    IN_PROGRESS: "#FFED86",
    UN_LOCKED: "#50C1BA",
};

export const STATES = {
    UNLOCKED: "UNLOCKED",
    EXPIRED: "EXPIRED",
    LOCKED: "LOCKED",
    COMPLETED: "COMPLETED",
};

export const SLIDE_OPTIONS = {
    initialSlide: 0,
    speed: 200,
    slidesPerView: 1.35,
    slidesPerGroup: 1,
    spaceBetween: "26%",
    centeredSlides: true,
    effect: "fade",
};

export const WRAPPER_BG_COLORS = {
    EXPIRED: "#FFFAFA",
    COMPLETED: "#F8FFF6",
    LOCKED: "#FFFCF1",
};

export const awardItemConstants = {
    1: {
        imageWrapper: {
            height: "64px",
            width: "64px",
        },

        imgStyle: {
            height: "48px",
            width: "48px",
        },
        tittleType: "montserratBold36",
        subTittleType: "paragraph",
    },
    2: {
        imageWrapper: {
            height: "48px",
            width: "48px",
        },

        imgStyle: {
            height: "36px",
            width: "36px",
        },
        tittleType: "montserratBold36",
        subTittleType: "paragraph",
    },
};

export const REWARD_PAGE_TEXT = {
    PAST_REWARDS: "Past rewards",
    REWARDS: "Rewards",
};
