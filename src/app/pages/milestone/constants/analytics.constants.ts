export const MILESTONE_ANALYTICS = {
    MILESTONE_GROUP: "milestone-group",
    MILESTONE_USER: "milestone-user",
    MILESTONE_TASK: "milestone-task",
    MILESTONE: "milestone",
    MILESTONE_STATE: "milestone-state",
    GO_TO_CURRENT_TASK: "GO_TO_CURRENT_TASK",
    START_BUTTON: "button",
    BUTTON_VALUE: "start",
};

export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        SEE_ALL_REWARDS: "see-all-rewards",
        FAQ: "faq",
    },
    IMPRESSION: {
        MILESTONE_TASK: "milestone-task",
    },
    CONTEXT: {},
};
