import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-milestone-header-skeleton",
    template: `
        <div class="header">
            <div class="headerContainer">
                <ion-skeleton-text animated class="bg"></ion-skeleton-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderSkeletonComponent {}
