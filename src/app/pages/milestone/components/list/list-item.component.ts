

import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { MileStoneRewardsList, Image } from "@shared/models";

import { SettingsService } from "@services/shared/settings.service";

import { awardItemConstants } from "@pages/milestone/constants";
import { MILESTONE_ANALYTICS } from "@pages/milestone/constants/analytics.constants";

@Component({
    selector: "supr-milestone-rewards-list-item",
    template: `
        <div
            #listItemWrapper
            class="suprRow"
            saImpression
            [saObjectName]="reward.type"
            [saObjectValue]="reward?.title?.text"
            [saContext]="state"
            [saContextList]="getContext()"
        >
            <ng-container *ngIf="!isGridView; else gridView">
                <!-- Task Reward List Item show only two items -->
                <ng-container *ngIf="index < 2">
                    <div class="listItem suprColumn left">
                        <div class="spaceBetween suprRow">
                            <div class="listItemImage">
                                <supr-image
                                    [src]="milestonesTypeImage?.fullUrl"
                                    [image]="milestonesTypeImage"
                                ></supr-image>
                            </div>

                            <div class="spacer8"></div>

                            <supr-text-fragment
                                [textFragment]="reward?.title"
                                [type]="
                                    itemStyle?.tittleType || 'montserratBold36'
                                "
                            ></supr-text-fragment>
                        </div>
                        <div class="divider8"></div>
                        <div class="suprRow">
                            <supr-text-fragment
                                [textFragment]="reward?.sub_title"
                                [type]="itemStyle?.subTittleType || 'paragraph'"
                            ></supr-text-fragment>
                        </div>
                    </div>
                </ng-container>
            </ng-container>
            <ng-template #gridView>
                <div
                    class="listItem suprColumn"
                    *ngIf="!(index < 2 && length >= 4)"
                    [ngClass]="length === 3 ? 'center' : 'left'"
                >
                    <div
                        class="spaceBetween"
                        [ngClass]="
                            length < 3 || length >= 4 ? 'suprRow' : 'suprColumn'
                        "
                    >
                        <div class="listItemImage">
                            <supr-image
                                [src]="milestonesTypeImage?.fullUrl"
                                [image]="milestonesTypeImage"
                            ></supr-image>
                        </div>
                        <ng-container
                            *ngIf="length < 3 || length >= 4; else divider"
                        >
                            <div class="spacer8"></div>
                        </ng-container>
                        <ng-template #divider>
                            <div class="divider8"></div>
                        </ng-template>

                        <supr-text-fragment
                            [textFragment]="reward?.title"
                            [type]="itemStyle?.tittleType || 'montserratBold36'"
                        ></supr-text-fragment>
                    </div>
                    <div class="divider8"></div>
                    <div class="suprRow">
                        <supr-text-fragment
                            [textFragment]="reward?.sub_title"
                            [type]="itemStyle?.subTittleType || 'paragraph'"
                        ></supr-text-fragment>
                    </div>
                </div>
            </ng-template>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class ListItemComponent implements OnInit {
    @Input() reward: MileStoneRewardsList;
    @Input() length: number;
    @Input() index: number;
    @Input() groupId: number;
    @Input() milestoneId: number;
    @Input() isGridView: boolean;
    @Input() state: string;

    @ViewChild("listItemWrapper", { static: true }) listItemEl: ElementRef;

    itemStyle: any;
    milestonesTypeImage: Image;

    constructor(private settingService: SettingsService) {}

    ngOnInit() {
        this.itemStyle = awardItemConstants[this.length === 1 ? 1 : 2];

        if (this.itemStyle) {
            this.setStyle();
        }

        this.setMilestoneRewardTypeImage();
    }

    private setStyle() {
        if (!this.listItemEl) {
            return;
        }

        this.setBoxStyle();
        this.setBoxImageStyle();
    }

    private setBoxStyle() {
        this.listItemEl.nativeElement.style.setProperty(
            "--reward-image-box-width",
            this.itemStyle.imageWrapper.width
        );

        this.listItemEl.nativeElement.style.setProperty(
            "--reward-image-box-height",
            this.itemStyle.imageWrapper.height
        );
    }

    private setBoxImageStyle() {
        this.listItemEl.nativeElement.style.setProperty(
            "--reward-image-width",
            this.itemStyle.imgStyle.width
        );

        this.listItemEl.nativeElement.style.setProperty(
            "--reward-image-height",
            this.itemStyle.imgStyle.height
        );
    }

    private setMilestoneRewardTypeImage() {
        const milestoneRewardType = this.settingService.getSettingsValue(
            "milestones_rewards",
            null
        );

        if (!milestoneRewardType) {
            return;
        }

        this.milestonesTypeImage = milestoneRewardType[this.reward.type];
    }

    getContext() {
        const item = [
            {
                name: MILESTONE_ANALYTICS.MILESTONE_GROUP,
                value: this.groupId,
            },
            {
                name: MILESTONE_ANALYTICS.MILESTONE,
                value: this.milestoneId,
            },
        ];

        return item;
    }
}
