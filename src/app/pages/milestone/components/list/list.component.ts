import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { MileStoneRewardsList } from "@shared/models";

import { HomeLayoutService } from "@services/layout/home-layout.service";
@Component({
    selector: "supr-milestone-rewards-list",
    template: `
        <ng-container *ngIf="isGridView; else horizontal">
            <div
                class="list suprRow"
                *ngFor="let rowData of groupItems; let row = index"
            >
                <div
                    *ngFor="let item of rowData; let col = index"
                    class=""
                    [style.width.%]="rowStyle"
                >
                    <supr-milestone-rewards-list-item
                        [reward]="item"
                        [length]="numberLength"
                        [isGridView]="isGridView"
                        [groupId]="groupId"
                        [milestoneId]="milestoneId"
                        [state]="state"
                    ></supr-milestone-rewards-list-item>
                </div>
            </div>
        </ng-container>

        <ng-template #horizontal>
            <supr-horizontal>
                <div class="list suprRow">
                    <supr-milestone-rewards-list-item
                        *ngFor="let item of rewardsList; let i = index"
                        [reward]="item"
                        [length]="rewardsList?.length"
                        [index]="i"
                        [isGridView]="isGridView"
                        [groupId]="groupId"
                        [milestoneId]="milestoneId"
                        [state]="state"
                    ></supr-milestone-rewards-list-item>
                </div>
            </supr-horizontal>
        </ng-template>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class ListComponent implements OnInit, OnChanges {
    @Input() rewardsList: MileStoneRewardsList[];
    @Input() isGridView: boolean;
    @Input() groupId: number;
    @Input() milestoneId: number;
    @Input() state: string;

    groupItems: any[][];
    rowStyle = "";
    numberLength = 2;

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["rewardsList"];

        if (change && !change.firstChange && change.currentValue) {
            this.init();
        }
    }

    private init() {
        if (!this.rewardsList) {
            return;
        }

        this.groupItems = this.groupArray(this.rewardsList, this.numberLength);
        this.rowStyle = `${100 / this.numberLength}`;
    }

    private groupArray<T>(data: Array<T>, n: number): Array<T[]> {
        return this.homeLayoutService.groupArray(data, n);
    }
}
