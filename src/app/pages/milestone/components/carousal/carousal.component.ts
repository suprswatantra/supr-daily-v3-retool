import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    ViewChild,
    EventEmitter,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import { IonSlides } from "@ionic/angular";

import { UserMileStone } from "@shared/models";
import { SLIDE_OPTIONS } from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-carousal",
    template: `
        <ion-content>
            <ion-slides
                #slides
                [options]="slideOpts"
                (ionSlideDidChange)="slideChanged()"
            >
                <ion-slide
                    *ngFor="let milestoneReward of milestones; let i = index"
                >
                    <div class="suprColumn carousal">
                        <supr-milestone-carousal-item
                            [reward]="milestoneReward?.milestone_rewards"
                            [state]="milestoneReward?.state"
                            [activeIndex]="activeIndex"
                            [index]="i"
                            [id]="i"
                            [groupId]="groupId"
                            [milestoneId]="milestoneReward?.milestone_id"
                        ></supr-milestone-carousal-item>

                        <supr-milestone-tasks
                            [milestoneTasks]="milestoneReward?.milestone_tasks"
                            [state]="milestoneReward?.state"
                            [groupId]="groupId"
                            [milestoneId]="milestoneReward?.milestone_id"
                            (handleStateChange)="
                                handleStateChange.emit(
                                    milestoneReward?.user_milestone_id
                                )
                            "
                            (handleGoToCurrentTask)="goToCurrentTask()"
                        ></supr-milestone-tasks>
                    </div>
                </ion-slide>
            </ion-slides>
        </ion-content>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class CarousalComponent implements OnChanges {
    @Input() milestones: UserMileStone[];
    @Input() currentIndex: number;
    @Input() groupId: number;
    @Output() handleSlideChange: EventEmitter<number> = new EventEmitter();
    @Output() handleStateChange: EventEmitter<number> = new EventEmitter();

    @ViewChild("slides", { static: true }) slider: IonSlides;

    slideOpts = SLIDE_OPTIONS;
    activeIndex = 0;

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["milestones"];
        if (change && change.currentValue) {
            this.goToCurrentSlide();
        }
    }

    slideChanged() {
        this.slider.getActiveIndex().then((activeIndex) => {
            this.activeIndex = activeIndex;
            this.handleSlideChange.emit(activeIndex);
        });
    }

    goToCurrentTask() {
        const index = this.getCurrentActiveSlideIndex();
        this.slider.slideTo(index);
    }

    private getCurrentActiveSlideIndex(): number {
        let _index = 0;
        this.milestones.map((dt, index) => {
            if (dt.current_milestone) {
                _index = index;
            }
        });

        return _index;
    }

    private goToCurrentSlide() {
        const index = this.getCurrentActiveSlideIndex();
        this.slideOpts.initialSlide = index;
    }
}
