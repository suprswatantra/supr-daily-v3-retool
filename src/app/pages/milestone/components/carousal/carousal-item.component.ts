import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
    ChangeDetectorRef,
} from "@angular/core";

import {
    MileStoneRewardsUserCard,
    MileStoneState,
    Image,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import {
    BG_COLORS,
    MILESTONE_TEXTS,
    WRAPPER_BG_COLORS,
} from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-carousal-item",
    template: `
        <div
            class="carousalItem suprColumn left spaceBetween"
            [ngStyle]="getWrapperStyle()"
            [class.activeCard]="isActiveIndex"
            #carousalItem
        >
            <div class="suprRow spaceBetween carousalItemHeader">
                <supr-text-fragment
                    [textFragment]="reward?.title"
                ></supr-text-fragment>
                <div class="carousalItemHeaderStatus">
                    <supr-text-fragment
                        [textFragment]="reward?.sub_title"
                    ></supr-text-fragment>
                </div>
            </div>

            <div class="divider16"></div>
            <div class="suprRow  carousalItemList">
                <supr-milestone-rewards-list
                    [rewardsList]="reward?.rewards_list"
                    [groupId]="groupId"
                    [milestoneId]="milestoneId"
                    [state]="state"
                ></supr-milestone-rewards-list>
            </div>

            <ng-container
                *ngIf="
                    reward?.rewards_list?.length > 1 &&
                    reward?.others_rewards_info
                "
            >
                <div class="divider32"></div>
                <div class="suprRow carousalItemInfo">
                    <supr-image
                        [src]="milestonesTypeImage?.fullUrl"
                        [image]="milestonesTypeImage"
                        class="rewardTypeImage"
                    ></supr-image>
                    <div class="spacer8"></div>
                    <supr-text-fragment
                        [textFragment]="reward?.others_rewards_info?.text"
                    ></supr-text-fragment>
                </div>
            </ng-container>

            <div
                class="divider8"
                *ngIf="reward?.rewards_list?.length !== 3"
            ></div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class CarousalItemComponent implements OnInit {
    @Input() reward: MileStoneRewardsUserCard;
    @Input() state: string;
    @Input() activeIndex: number;
    @Input() index: number;
    @Input() groupId: number;
    @Input() milestoneId: number;

    @ViewChild("carousalItem", { static: true }) carousalItemEl: ElementRef;

    isActiveIndex = false;
    MILESTONE_TEXTS = MILESTONE_TEXTS;
    milestonesTypeImage: Image;

    constructor(
        private utilService: UtilService,
        private settingService: SettingsService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.setIndicatorBgColor();
        this.setMilestoneRewardTypeImage();

        this.cdr.markForCheck();
    }

    getWrapperStyle() {
        if (this.state === MileStoneState.EXPIRED) {
            return {
                background: WRAPPER_BG_COLORS.EXPIRED,
            };
        } else if (this.state === MileStoneState.COMPLETED) {
            return {
                background: WRAPPER_BG_COLORS.COMPLETED,
            };
        } else if (this.state === MileStoneState.LOCKED) {
            return {
                background: WRAPPER_BG_COLORS.LOCKED,
            };
        }
    }

    getInfoBgColor() {
        if (this.state === MileStoneState.EXPIRED) {
            return BG_COLORS.EXPIRED;
        } else if (this.state === MileStoneState.COMPLETED) {
            return BG_COLORS.COMPLETED;
        } else if (this.state === MileStoneState.UNLOCKED) {
            return BG_COLORS.UN_LOCKED;
        } else if (
            this.state === MileStoneState.IN_PROGRESS ||
            this.state === MileStoneState.LOCKED
        ) {
            return BG_COLORS.IN_PROGRESS;
        }
    }

    private setIndicatorBgColor() {
        let subTitleBgColor = this.utilService.getNestedValue(
            this.reward,
            "sub_title_bg_color",
            null
        );

        if (this.utilService.isEmpty(subTitleBgColor)) {
            subTitleBgColor = this.getInfoBgColor();
        }

        this.carousalItemEl.nativeElement.style.setProperty(
            "--milestone-carousal-item-indicator-bg-color",
            subTitleBgColor
        );
    }

    private setMilestoneRewardTypeImage() {
        const milestoneRewardType = this.settingService.getSettingsValue(
            "milestones_rewards",
            null
        );

        if (!milestoneRewardType) {
            return;
        }

        const otherRewardType = this.utilService.getNestedValue(
            this.reward,
            "others_rewards_info.type",
            null
        );

        if (!otherRewardType) {
            return;
        }

        this.milestonesTypeImage = milestoneRewardType[otherRewardType];
    }
}
