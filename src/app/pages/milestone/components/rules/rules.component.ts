import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-milestone-rules",
    template: `
        <div class="rules">
            <supr-card>
                <div class="suprRow spaceBetween" (click)="toggle()">
                    <supr-text type="body">Rules</supr-text>
                    <supr-icon name="chevron_down" *ngIf="!isOpen"></supr-icon>
                    <supr-icon name="chevron_up" *ngIf="isOpen"></supr-icon>
                </div>

                <div *ngIf="isOpen" class="rulesContainer">
                    <div class="divider16"></div>
                    <ng-container
                        *ngFor="
                            let rule of rules;
                            let last = last;
                            let i = index
                        "
                    >
                        <supr-milestone-rules-item
                            [rule]="rule"
                            [index]="i"
                        ></supr-milestone-rules-item>
                        <div class="divider16" *ngIf="!last"></div>
                    </ng-container>
                </div>
            </supr-card>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class RulesComponent {
    @Input() rules: string[];
    @Input() state: string;

    isOpen = false;

    toggle() {
        if (this.rules.length > 0) {
            this.isOpen = !this.isOpen;
        } else {
            this.isOpen = false;
        }
    }
}
