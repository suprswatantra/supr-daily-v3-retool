import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-milestone-rules-item",
    template: `
        <div class="suprRow  rulesContainerItem top">
            <div class="suprColumn center rulesContainerItemCounter">
                <supr-text type="paragraph">
                    {{ index + 1 }}
                </supr-text>
            </div>
            <div class="spacer16"></div>
            <supr-text type="action14" class="text"> {{ rule }} </supr-text>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class RulesItemComponent {
    @Input() rule: string;
    @Input() index: number;
}
