import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { UtilService } from "@services/util/util.service";
import { MileStoneRewards } from "@shared/models";

@Component({
    selector: "supr-milestone-content",
    template: `
        <div class="content">
            <div class="suprColumn left">
                <div
                    class="contentCarousal"
                    [class.removeDefaultStyle]="state === 'COMPLETED'"
                >
                    <ng-container *ngIf="state === 'COMPLETED'; else carousal">
                        <div class="suprColumn contentCarousalMilestoneCompete">
                            <supr-milestone-complete
                                [users_reward]="mileStoneDetail?.users_rewards"
                            ></supr-milestone-complete>
                        </div>
                    </ng-container>
                    <ng-template #carousal>
                        <supr-milestone-carousal
                            [milestones]="mileStoneDetail?.milestones"
                            (handleSlideChange)="slideChange($event)"
                            (handleStateChange)="handleStateChange.emit($event)"
                            [currentIndex]="currentIndex"
                            [groupId]="mileStoneDetail?.milestone_group_id"
                        ></supr-milestone-carousal>
                    </ng-template>
                </div>

                <div class="contentBody" *ngIf="state !== 'COMPLETED'">
                    <div class="divider16"></div>
                    <div class="divider16"></div>
                    <supr-milestone-rules
                        [rules]="rules"
                        [state]="_rulesState"
                    ></supr-milestone-rules>
                </div>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class ContentComponent implements OnInit {
    @Input() mileStoneDetail: MileStoneRewards;
    @Input() state: string;

    @Output() handleStateChange: EventEmitter<number> = new EventEmitter();

    currentIndex = 0;
    rules: string[];
    _rulesState: string;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setRules();
        this.setState();
    }

    slideChange(index: number) {
        this.currentIndex = index;
        this.setRules();
        this.setState();
    }

    private setRules() {
        if (this.utilService.isEmpty(this.mileStoneDetail)) {
            return;
        }

        if (this.mileStoneDetail.milestones.length > 0) {
            this.setRulesData();
        }
    }

    private setRulesData() {
        this.rules = this.mileStoneDetail.milestones[this.currentIndex].rules;
    }

    private setState() {
        if (this.mileStoneDetail.milestones.length > 0) {
            this._rulesState = this.mileStoneDetail.milestones[
                this.currentIndex
            ].state;
        }
    }
}
