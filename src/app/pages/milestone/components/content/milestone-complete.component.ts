import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";

import { SettingsService } from "@services/shared/settings.service";
import { Image, UserMileStoneRewards } from "@models";

@Component({
    selector: "supr-milestone-complete",
    template: `
        <div class="suprColumn milestoneComplete">
            <div class="milestoneCompleteHeader">
                <supr-text-fragment
                    [textFragment]="
                        users_reward?.milestone_complete_state_details?.title
                    "
                ></supr-text-fragment>
            </div>
            <div class="suprRow spaceBetween milestoneCompleteBody">
                <supr-text-fragment
                    [textFragment]="
                        users_reward?.milestone_complete_state_details
                            ?.sub_title
                    "
                    class="bodyText"
                ></supr-text-fragment>

                <supr-image [image]="completedImage"></supr-image>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class MilestoneCompleteComponent implements OnInit {
    @Input() users_reward: UserMileStoneRewards;

    completedImage: Image;

    constructor(private settingService: SettingsService) {}

    ngOnInit() {
        this.setCompletedTypeImage();
    }

    private setCompletedTypeImage() {
        const milestoneRewardType = this.settingService.getSettingsValue(
            "milestones_rewards",
            null
        );

        if (!milestoneRewardType) {
            return;
        }

        this.completedImage = milestoneRewardType["COMPLETED"];
    }
}
