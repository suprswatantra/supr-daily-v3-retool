import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { RouterService } from "@services/util/router.service";
import { MileStoneRewardsList } from "@shared/models";

import { MILESTONE_TEXTS } from "@pages/milestone/constants";
import {
    MILESTONE_ANALYTICS,
    ANALYTICS_OBJECT_NAMES,
} from "@pages/milestone/constants/analytics.constants";

@Component({
    selector: "supr-milestone-rewards",
    template: `
        <div class="rewards suprColumn left" *ngIf="rewardsList.length > 0">
            <supr-card>
                <supr-text type="body"
                    >${MILESTONE_TEXTS.REWARDS_EARNED}</supr-text
                >
                <div class="divider16"></div>
                <div class="rewardsList">
                    <supr-milestone-rewards-list
                        [rewardsList]="rewardsList"
                        [groupId]="groupId"
                        [milestoneId]="milestoneId"
                        [isGridView]="isGridView"
                        [state]="state"
                    ></supr-milestone-rewards-list>
                </div>

                <div class="divider16"></div>
                <div
                    class="suprRow rewardsCta"
                    (click)="goToRewards()"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .SEE_ALL_REWARDS}"
                    [saContext]="state"
                    [saContextList]="getContext()"
                >
                    <supr-text type="body">
                        ${MILESTONE_TEXTS.SEE_ALL_REWARDS}
                    </supr-text>
                    <div class="spacer4"></div>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </supr-card>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class RewardsComponent {
    @Input() rewardsList: MileStoneRewardsList[];
    @Input() isGridView = true;
    @Input() groupId: number;
    @Input() milestoneId: number;
    @Input() state: string;

    constructor(private routerService: RouterService) {}

    goToRewards() {
        this.routerService.goToMileStoneRewardPage();
    }

    getContext() {
        const item = [
            {
                name: MILESTONE_ANALYTICS.MILESTONE_GROUP,
                value: this.groupId,
            },
        ];

        return item;
    }
}
