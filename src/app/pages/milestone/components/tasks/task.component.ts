import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { UserMileStoneTasks, MileStoneState } from "@models";

@Component({
    selector: "supr-milestone-tasks",
    template: `
        <div class="wrapper">
            <supr-milestone-tasks-header
                [milestoneTasks]="milestoneTasks"
                [state]="state"
            ></supr-milestone-tasks-header>
            <div class="task" [class.overflowHidden]="state !== 'IN_PROGRESS'">
                <div class="taskCard">
                    <div
                        class="suprColumn left taskCardItem"
                        *ngFor="
                            let taskItem of milestoneTasks?.tasks;
                            let last = last;
                            let i = index
                        "
                    >
                        <supr-milestone-task-item
                            [taskItem]="taskItem"
                            [groupId]="groupId"
                            [milestoneId]="milestoneId"
                            [position]="i"
                        ></supr-milestone-task-item>

                        <div class="taskCardItemDivider" *ngIf="!last"></div>
                    </div>
                </div>

                <supr-milestone-mask
                    [state]="state"
                    [state_details]="milestoneTasks?.state_details"
                    (handleStateChange)="handleStateChange.emit($event)"
                    (handleGoToCurrentTask)="handleGoToCurrentTask.emit()"
                    [groupId]="groupId"
                    [milestoneId]="milestoneId"
                    *ngIf="state !== 'IN_PROGRESS'"
                ></supr-milestone-mask>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/task.component.scss"],
})
export class TasksComponent {
    @Input() milestoneTasks: UserMileStoneTasks;
    @Input() state: string;

    @Input() groupId: number;
    @Input() milestoneId: number;

    @Output() handleStateChange: EventEmitter<any> = new EventEmitter();
    @Output() handleGoToCurrentTask: EventEmitter<any> = new EventEmitter();

    isShowCount(state: string) {
        return (
            state === MileStoneState.LOCKED || state === MileStoneState.UNLOCKED
        );
    }
}
