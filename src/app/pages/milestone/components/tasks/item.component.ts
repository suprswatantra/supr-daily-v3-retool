import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { UserMileStoneTaskList, MileStoneState } from "@shared/models";
import { MileStoneService } from "@pages/milestone/services/milestone.service";

import {
    ANALYTICS_OBJECT_NAMES,
    MILESTONE_ANALYTICS,
} from "../../constants/analytics.constants";

@Component({
    selector: "supr-milestone-task-item",
    template: `
        <div
            class="taskCardItemInfo suprRow spaceBetween"
            (click)="handleEvent()"
            saImpression
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.MILESTONE_TASK}"
            [saObjectValue]="taskItem?.id"
            [saContext]="taskItem?.state"
            [saContextList]="getContext()"
            [saPosition]="position"
        >
            <div class="suprRow top">
                <supr-image
                    [src]="taskItem?.image?.fullUrl"
                    [image]="taskItem?.image"
                ></supr-image>
                <div class="spacer16"></div>
                <div class="suprRow taskCardItemInfoContent">
                    <div class="suprColumn left">
                        <supr-text-fragment
                            class="title"
                            [textFragment]="taskItem?.title"
                        ></supr-text-fragment>

                        <div class="divider4"></div>
                        <supr-text-fragment
                            [textFragment]="taskItem?.sub_title"
                        ></supr-text-fragment>

                        <div class="divider8"></div>
                        <div
                            class="suprRow"
                            *ngIf="
                                taskItem?.cta?.text && !checkIFStateComplete()
                            "
                        >
                            <supr-text-fragment
                                class="cta"
                                [textFragment]="taskItem?.cta?.text"
                            ></supr-text-fragment>
                            <supr-icon
                                name="chevron_right"
                                class="cta"
                            ></supr-icon>
                        </div>
                    </div>
                </div>
            </div>
            <ng-container>
                <ng-container
                    *ngIf="taskItem?.state === completed; else progress"
                >
                    <supr-icon name="approve_filled" class="done"></supr-icon>
                </ng-container>

                <ng-template #progress>
                    <ng-container
                        *ngIf="
                            taskItem?.progress?.percentage == '0';
                            else progressBar
                        "
                    >
                        <supr-icon name="approve_filled"></supr-icon>
                    </ng-container>
                    <ng-template #progressBar>
                        <div class="suprColumn taskCardItemInfoProgress">
                            <supr-progress-bar
                                [percentage]="taskItem?.progress?.percentage"
                            ></supr-progress-bar>
                            <div class="divider8"></div>
                            <div class="suprRow">
                                <supr-text type="subtext10">
                                    {{ taskItem?.progress.text }}
                                </supr-text>
                            </div>
                        </div>
                    </ng-template>
                </ng-template>
            </ng-container>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/task.component.scss"],
})
export class TaskItemComponent {
    @Input() taskItem: UserMileStoneTaskList;
    @Input() groupId: number;
    @Input() milestoneId: number;
    @Input() position: number;

    completed = MileStoneState.COMPLETED;

    constructor(private mileStoneService: MileStoneService) {}

    handleEvent() {
        if (!this.checkIFStateComplete()) {
            this.mileStoneService.handleEvent(this.taskItem.cta);
        }
    }

    getContext() {
        const item = [
            {
                name: MILESTONE_ANALYTICS.MILESTONE_GROUP,
                value: this.groupId,
            },
            {
                name: MILESTONE_ANALYTICS.MILESTONE,
                value: this.milestoneId,
            },
            {
                name: MILESTONE_ANALYTICS.MILESTONE_TASK,
                value: this.taskItem.id,
            },
        ];

        return item;
    }

    checkIFStateComplete() {
        return this.taskItem.state === MileStoneState.COMPLETED;
    }
}
