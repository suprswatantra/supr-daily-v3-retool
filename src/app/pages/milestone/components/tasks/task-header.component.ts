import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { UserMileStoneTasks, MileStoneState } from "@models";
import { WRAPPER_BG_COLORS } from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-tasks-header",
    template: `
        <div class="suprColumn left taskHeader" [ngStyle]="getWrapperStyle()">
            <div class="suprRow spaceBetween taskHeaderItem">
                <supr-text-fragment
                    [textFragment]="milestoneTasks?.task_header_text"
                    class="title"
                    type="body"
                ></supr-text-fragment>

                <div
                    class="suprRow taskHeaderItemCount"
                    *ngIf="!isShowCount(state)"
                >
                    <supr-text-fragment
                        [textFragment]="milestoneTasks?.completed"
                        class="title"
                        type="body"
                    ></supr-text-fragment>

                    <div class="taskHeaderItemCountDivider">/</div>

                    <supr-text-fragment
                        [textFragment]="milestoneTasks?.total"
                        class="title"
                        type="body"
                    ></supr-text-fragment>
                </div>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/task.component.scss"],
})
export class TasksHeaderComponent {
    @Input() milestoneTasks: UserMileStoneTasks;
    @Input() state: string;

    isShowCount(state: string) {
        return (
            state === MileStoneState.LOCKED || state === MileStoneState.UNLOCKED
        );
    }

    getWrapperStyle() {
        if (this.state === MileStoneState.EXPIRED) {
            return {
                background: WRAPPER_BG_COLORS.EXPIRED,
            };
        } else if (this.state === MileStoneState.COMPLETED) {
            return {
                background: WRAPPER_BG_COLORS.COMPLETED,
            };
        } else if (this.state === MileStoneState.LOCKED) {
            return {
                background: WRAPPER_BG_COLORS.LOCKED,
            };
        }
    }
}
