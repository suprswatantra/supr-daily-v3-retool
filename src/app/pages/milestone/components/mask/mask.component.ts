import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { AnalyticsService } from "@services/integration/analytics.service";

import { MILESTONE_ANALYTICS } from "@pages/milestone/constants/analytics.constants";

import { MileStoneState } from "@models";
import { UserMileStoneDetails } from "@shared/models";

import { Segment } from "@types";

import { STATES } from "@pages/milestone/constants";

@Component({
    selector: "supr-milestone-mask",
    template: `
        <div class="mask">
            <ng-container [ngSwitch]="state">
                <supr-milestone-mask-start
                    *ngSwitchCase="STATES.UNLOCKED"
                    [state_details]="state_details"
                    (handleStateChange)="handleClick($event)"
                ></supr-milestone-mask-start>
                <supr-milestone-mask-expired
                    *ngSwitchCase="STATES.EXPIRED"
                    [state_details]="state_details"
                    (handleGoToCurrentTask)="handleClick()"
                ></supr-milestone-mask-expired>
                <supr-milestone-mask-locked
                    *ngSwitchCase="STATES.LOCKED"
                    [state_details]="state_details"
                    (handleGoToCurrentTask)="handleClick()"
                ></supr-milestone-mask-locked>
                <supr-milestone-mask-complete
                    *ngSwitchCase="STATES.COMPLETED"
                    [state_details]="state_details"
                    (handleGoToCurrentTask)="handleClick()"
                ></supr-milestone-mask-complete>
            </ng-container>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/mask.component.scss"],
})
export class MaskComponent {
    @Input() state: string;
    @Input() state_details: UserMileStoneDetails;
    @Input() groupId: number;
    @Input() milestoneId: number;

    @Output() handleStateChange: EventEmitter<any> = new EventEmitter();
    @Output() handleGoToCurrentTask: EventEmitter<void> = new EventEmitter();

    STATES = STATES;

    constructor(private analyticsService: AnalyticsService) {}

    handleClick() {
        if (this.state === MileStoneState.UNLOCKED) {
            this.handleStateChange.emit(this.milestoneId);
        } else {
            this.handleGoToCurrentTask.emit();
        }

        this.handleSaClick();
    }

    handleSaClick() {
        const traits = this.setTraits();
        this.analyticsService.trackClick(traits);
    }

    private setTraits(): Segment.Traits {
        const traits: Segment.Traits = {};

        traits.objectName = MILESTONE_ANALYTICS.START_BUTTON;
        traits.objectValue =
            this.state === MileStoneState.UNLOCKED
                ? MILESTONE_ANALYTICS.BUTTON_VALUE
                : MILESTONE_ANALYTICS.GO_TO_CURRENT_TASK;
        traits.context = this.state;
        traits.context_1_name = MILESTONE_ANALYTICS.MILESTONE_GROUP;
        traits.context_1_value = String(this.groupId);
        traits.context_2_name = MILESTONE_ANALYTICS.MILESTONE;
        traits.context_2_value = String(this.milestoneId);

        return traits;
    }
}
