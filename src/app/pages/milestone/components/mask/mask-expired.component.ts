import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { UserMileStoneDetails } from "@shared/models";
import { MILESTONE_TEXTS } from "../../constants";

@Component({
    selector: "supr-milestone-mask-expired",
    template: `
        <div class="suprColumn center maskExpired">
            <div class="suprColumn maskExpiredText center">
                <!-- <supr-icon name="APPROVE_FILLED" class="big"></supr-icon> -->

                <div class="maskExpiredTextSvg">
                    <supr-svg></supr-svg>
                </div>

                <div class="divider16"></div>
                <supr-text type="regular14">
                    {{ state_details?.title }}
                </supr-text>
                <div class="divider8"></div>
                <div
                    class="suprRow maskExpiredTextCta"
                    (click)="handleGoToCurrentTask.emit()"
                >
                    <supr-text type="regular14">
                        {{ state_details?.sub_title }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/mask.component.scss"],
})
export class MaskExpiredComponent {
    @Input() state_details: UserMileStoneDetails;
    @Output() handleGoToCurrentTask: EventEmitter<void> = new EventEmitter();

    TEXTS = MILESTONE_TEXTS;
}
