import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { UserMileStoneDetails } from "@shared/models";
import { MILESTONE_TEXTS } from "../../constants";

@Component({
    selector: "supr-milestone-mask-locked",
    template: `
        <div class="suprColumn center maskLocked">
            <div class="suprColumn maskLockedText center">
                <div class="maskLockedTextRound suprColumn center">
                    <supr-svg></supr-svg>
                </div>
                <div class="divider16"></div>
                <supr-text type="regular14">
                    {{ state_details?.title }}
                </supr-text>
                <div class="divider8"></div>
                <div
                    class="suprRow maskLockedTextCta"
                    (click)="handleGoToCurrentTask.emit()"
                >
                    <supr-text type="regular14">
                        {{ state_details?.sub_title }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/mask.component.scss"],
})
export class MaskLockedComponent {
    @Input() state_details: UserMileStoneDetails;
    @Output() handleGoToCurrentTask: EventEmitter<void> = new EventEmitter();

    TEXTS = MILESTONE_TEXTS;
}
