import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

import { MILESTONE_TEXTS } from "../../constants";
import { UserMileStoneDetails } from "@shared/models";

@Component({
    selector: "supr-milestone-mask-start",
    template: `
        <div class="suprColumn center maskStart">
            <div class="maskStartContainer suprColumn center">
                <supr-button (click)="handleStateChange.emit()">
                    {{ TEXTS.START_NOW }}
                </supr-button>
                <div class="divider16"></div>
                <div class="maskStartContainerText">
                    <supr-text type="regular14">
                        {{ state_details?.title }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/mask.component.scss"],
})
export class MaskStartComponent {
    @Input() state_details: UserMileStoneDetails;
    @Output() handleStateChange: EventEmitter<any> = new EventEmitter();

    TEXTS = MILESTONE_TEXTS;
}
