import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
    SimpleChanges,
    OnChanges,
    ChangeDetectorRef,
} from "@angular/core";

import { UserMileStoneRewards, MileStoneState, Image } from "@shared/models";

@Component({
    selector: "supr-milestone-header",
    template: `
        <div class="header" #header [style.backgroundImage]="BG_CENTER_IMAGE">
            <div class="headerContent">
                <ng-container *ngIf="imgLoaded; else skeleton">
                    <div class="suprColumn headerContentLockText">
                        <supr-image
                            [image]="CENTER_IMAGE"
                            [lazyLoad]="false"
                        ></supr-image>
                        <div class="divider8"></div>

                        <supr-text-fragment
                            [textFragment]="users_rewards?.title"
                            [truncate]="true"
                            class="title"
                        ></supr-text-fragment>

                        <supr-text-fragment
                            [truncate]="true"
                            [textFragment]="users_rewards?.sub_title"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-template #skeleton>
                    <supr-milestone-header-skeleton></supr-milestone-header-skeleton>
                </ng-template>
            </div>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class HeaderComponent implements OnInit, OnChanges {
    @Input() users_rewards: UserMileStoneRewards;

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    LOCKED = MileStoneState.LOCKED;
    CENTER_IMAGE: Image;
    BG_CENTER_IMAGE: string;
    imgLoaded = false;

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnInit() {
        this.setBgImage();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["users_rewards"];

        if (change && change.currentValue) {
            this.setBgImage();
            this.setCenterImage();
        }
    }

    private setBgImage() {
        if (this.users_rewards && this.users_rewards.image) {
            this.BG_CENTER_IMAGE =
                "url(" +
                this.users_rewards.image.compressed_url["400_400"].fullUrl +
                ")";
        }
    }

    private setCenterImage() {
        if (!this.users_rewards.center_image) {
            return;
        }

        this.CENTER_IMAGE = this.users_rewards.center_image;

        const context = this;
        setTimeout(() => {
            context.imgLoaded = true;

            // require view to be updated
            context.cdr.markForCheck();
        }, 200);
    }
}
