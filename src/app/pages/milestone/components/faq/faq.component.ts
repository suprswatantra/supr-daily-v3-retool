import { Component, ChangeDetectionStrategy } from "@angular/core";

import { RouterService } from "@services/util/router.service";

import { MILESTONE_TEXTS } from "@pages/milestone/constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-milestone-faq",
    template: `
        <div
            class="faq suprRow"
            (click)="goToFaq()"
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.FAQ}"
        >
            <supr-text type="action14"> ${MILESTONE_TEXTS.FAQ} </supr-text>

            <div class="spacer4"></div>

            <supr-icon name="chevron_right"></supr-icon>
        </div>
    `,

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/milestone.page.scss"],
})
export class FaqComponent {
    constructor(
        private routerService: RouterService,
        private settingsService: SettingsService
    ) {}

    private milestoneRewardConfig() {
        const milestoneRewardType = this.settingsService.getSettingsValue(
            "milestones_rewards",
            null
        );

        return milestoneRewardType;
    }

    goToFaq() {
        const milestoneRewardType = this.milestoneRewardConfig();
        const MILESTONE_URL =
            milestoneRewardType.MILESTONE_URL || MILESTONE_TEXTS.MILESTONE_URL;
        this.routerService.goToFaqPage({}, MILESTONE_URL);
    }
}
