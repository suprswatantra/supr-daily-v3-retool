import { HeaderComponent } from "./header/header.component";
import { HeaderSkeletonComponent } from "./skeleton/header.skeleton";
import { CarousalComponent } from "./carousal/carousal.component";
import { CarousalItemComponent } from "./carousal/carousal-item.component";
import { ContentComponent } from "./content/content.component";

import { TasksComponent } from "./tasks/task.component";
import { TasksHeaderComponent } from "./tasks/task-header.component";
import { TaskItemComponent } from "./tasks/item.component";
import { RulesComponent } from "./rules/rules.component";
import { RulesItemComponent } from "./rules/rules-item.component";
import { RewardsComponent } from "./rewards/rewards.component";
import { ListComponent } from "./list/list.component";
import { ListItemComponent } from "./list/list-item.component";
import { FaqComponent } from "./faq/faq.component";

import { MaskComponent } from "./mask/mask.component";
import { MaskStartComponent } from "./mask/mask-start.component";
import { MaskExpiredComponent } from "./mask/mask-expired.component";
import { MaskLockedComponent } from "./mask/mask.locked.component";
import { MaskCompleteComponent } from "./mask/mask.complete.component";
import { MilestoneCompleteComponent } from "./content/milestone-complete.component";

export const milestoneComponents = [
    HeaderComponent,
    CarousalComponent,
    ContentComponent,
    CarousalItemComponent,
    TasksComponent,
    TaskItemComponent,
    TasksHeaderComponent,
    RulesComponent,
    RulesItemComponent,
    RewardsComponent,
    ListComponent,
    ListItemComponent,
    FaqComponent,
    MilestoneCompleteComponent,

    MaskComponent,
    MaskStartComponent,
    MaskExpiredComponent,
    MaskLockedComponent,
    MaskCompleteComponent,

    HeaderSkeletonComponent,
];
