import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { MilestonePageAdapter as Adapter } from "@pages/milestone/services/milestone.adapter";
import { MileStoneRewards } from "@shared/models";

@Component({
    selector: "supr-milestone-container",
    template: `
        <supr-milestone-layout
            [mileStoneDetail]="mileStoneDetail$ | async"
            [milsestoneCurrentState]="milsestoneCurrentState$ | async"
            (handleFetchMileStoneDetails)="fetchMileStoneDetail()"
            (handleStateChange)="updateState($event)"
        ></supr-milestone-layout>
    `,
})
export class MilestonePageContainer implements OnInit {
    mileStoneDetail$: Observable<MileStoneRewards>;
    milsestoneCurrentState$: Observable<string>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.mileStoneDetail$ = this.adapter.mileStoneDetail$;
        this.milsestoneCurrentState$ = this.adapter.milsestoneCurrentState$;
    }

    fetchMileStoneDetail() {
        this.adapter.fetchMileStoneDetail();
    }

    updateState(userMileStoneId: number) {
        this.adapter.postUserMilestone(userMileStoneId);
    }
}
