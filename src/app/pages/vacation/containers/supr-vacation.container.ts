import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Vacation } from "@models";
import { VacationCurrentState } from "@store/vacation/vacation.state";

import { PauseAdapter as Adapter } from "@shared/adapters/pause.adapter";

@Component({
    selector: "supr-vacation-container",
    template: `
        <supr-vacation-layout
            [vacation]="vacation$ | async"
            [vacationState]="vacationState$ | async"
            [vacationError]="vacationError$ | async"
            (handleCreateVacation)="createVacation($event)"
        ></supr-vacation-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprVacationContainer implements OnInit {
    vacation$: Observable<Vacation>;
    vacationState$: Observable<VacationCurrentState>;
    vacationError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.vacation$ = this.adapter.vacation$;
        this.vacationState$ = this.adapter.vacationState$;
        this.vacationError$ = this.adapter.vacationError$;
    }

    createVacation(vacation: Vacation) {
        this.adapter.createVacation(vacation);
    }
}
