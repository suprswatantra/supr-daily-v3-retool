export const TEXTS = {
    PAGE_HEADER_TEXT: "Vacation",
    INFO_HEADER: "Create vacation",
    INFO_SUBTEXT:
        "Select start and end date to suspend all deliveries during your vacation.",
    SELECT_VACATION_DATE: "Select vacation dates",
    START_DATE: "Start date",
    END_DATE: "End date",
    CONFIRM: "Confirm",
    CONFIRM_VACATION: "Confirm vacation",
    CONFIRM_VACATION_TITLE: "Confirm vacation to pause deliveries",
    VACATION_CREATE_SUCCESSFUL: "Deliveries successfully paused",
};

export const VACATION_INFO_POINTS = [
    {
        title: "All subscriptions will be extended",
        desc: "Your subscription will restart once you are back",
        svgClass: "refundClock",
    },
    {
        title: "Deliver once order refund",
        desc: "We will automatically cancel and refund one time orders",
        svgClass: "bill",
    },
];
