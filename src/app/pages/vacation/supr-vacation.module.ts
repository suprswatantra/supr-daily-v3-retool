import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SuprVacationLayout } from "./layouts/supr-vacation.layout";
import { SuprPassComponents } from "./components";
import { SuprVacationContainer } from "./containers/supr-vacation.container";

const routes: Routes = [
    {
        path: "",
        component: SuprVacationContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...SuprPassComponents,
        SuprVacationContainer,
        SuprVacationLayout,
    ],
})
export class SuprVacationModule {}
