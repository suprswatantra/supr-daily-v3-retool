import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES, MODAL_NAMES } from "@constants";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-vacation-confirm-modal",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.VACATION_START_DATE}"
            >
                <div class="confirmationModalWrapper">
                    <div class="divider24"></div>
                    <supr-text type="headingBold">{{
                        texts.CONFIRM_VACATION_TITLE
                    }}</supr-text>
                    <div class="divider16"></div>
                    <supr-text type="subheading" class="vacationDays">
                        Vacation date from {{ startDate | date: "d LLL" }} till
                        {{ endDate | date: "d LLL" }}
                    </supr-text>
                    <div class="divider24"></div>
                    <supr-button
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .PAUSE_CONFIRM}"
                        [loading]="loading"
                        (handleClick)="confirmVacation.emit()"
                    >
                        {{ texts.CONFIRM_VACATION }}
                    </supr-button>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmModalComponent {
    @Input() showModal: boolean;
    @Input() startDate: Date;
    @Input() endDate: Date;
    @Input() loading: boolean;
    @Output() confirmVacation: EventEmitter<void> = new EventEmitter();
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;

    constructor() {}
}
