import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES, MODAL_NAMES } from "@constants";

import { SuprDateService } from "@services/date/supr-date.service";
import { Calendar } from "@types";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-vacation-end-date-calendar-modal",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.VACATION_START_DATE}"
            >
                <div class="calendarModalWrapper">
                    <div class="divider24"></div>
                    <supr-calendar
                        [selectedDate]="_endDate"
                        [firstActiveDay]="_startDate"
                        [disablePastDays]="true"
                        [saObjectNameDateClick]="saObjectNameDateClick"
                        (handleSelectedDate)="selectNewDate($event)"
                    ></supr-calendar>
                    <div class="divider24"></div>
                    <supr-button (handleClick)="handleEndDate()">
                        {{ TEXT.CONFIRM }}
                    </supr-button>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VacationEndDateCalendarModalComponent
    implements OnInit, OnChanges {
    @Input() showModal: boolean;
    @Input() endDate: Date;
    @Input() startDate: Date;
    @Output() updateEndDate: EventEmitter<Date> = new EventEmitter();
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    _endDate: Calendar.DayData;
    _startDate: Calendar.DayData;
    saObjectNameDateClick = ANALYTICS_OBJECT_NAMES.CLICK.PAUSE_END_DATE;

    TEXT = TEXTS;

    constructor(private suprDateService: SuprDateService) {}

    ngOnInit() {
        this.setDates();
    }

    ngOnChanges(changes: SimpleChanges) {
        const endDateChange = changes["endDate"];
        const startDateChange = changes["startDate"];
        if (
            this.canHandleChange(startDateChange) ||
            this.canHandleChange(endDateChange)
        ) {
            this.setDates();
        }
    }

    selectNewDate(date: Calendar.DayData) {
        this._endDate = date;
    }

    handleEndDate() {
        this.updateEndDate.emit(this._endDate.jsDate);
    }

    private setDates() {
        this._endDate = this.suprDateService.suprDate(this.endDate);
        this._startDate = this.suprDateService.suprDate(this.startDate);
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
