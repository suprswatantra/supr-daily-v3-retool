import { VacationDateInfo } from "./dateinfo/date-info.component";
import { FooterComponent } from "./footer/footer.component";
import { InfoComponent } from "./info/info.component";
import { HeaderComponent } from "./header/header.component";
import { VacationEndDateCalendarModalComponent } from "./calendar/end-date-calendar.component";
import { VacationStartDateCalendarModalComponent } from "./calendar/start-date-calendar.component";
import { ConfirmModalComponent } from "./confirm/confirm-modal.component";

export const SuprPassComponents = [
    HeaderComponent,
    InfoComponent,
    FooterComponent,
    VacationStartDateCalendarModalComponent,
    VacationEndDateCalendarModalComponent,
    ConfirmModalComponent,
    VacationDateInfo,
];
