import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { DateService } from "@services/date/date.service";

@Component({
    selector: "supr-vaction-footer",
    template: `
        <supr-sticky-wrapper>
            <div class="footerWrapper">
                <div class="suprRow">
                    <supr-button
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .SET_VACATION}"
                        (handleClick)="handleSetVacation.emit()"
                    >
                        Set {{ dateText }} vacation
                    </supr-button>
                </div>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit, OnChanges {
    @Input() startDate: Date;
    @Input() endDate: Date;
    @Output() handleSetVacation: EventEmitter<void> = new EventEmitter();

    dateText = "1 day";

    constructor(private dateService: DateService) {}

    ngOnInit() {
        this.setDateText();
    }

    ngOnChanges(changes: SimpleChanges) {
        const startDateChange = changes["startDate"];
        const endDateChange = changes["endDate"];
        if (
            this.canHandleChange(startDateChange) ||
            this.canHandleChange(endDateChange)
        ) {
            this.setDateText();
        }
    }

    private setDateText() {
        if (this.startDate && this.endDate) {
            const daysDiff = this.dateService.daysBetweenTwoDates(
                this.startDate,
                this.endDate
            );
            const days = daysDiff + 1;
            const str = days > 1 ? "days" : "day";
            this.dateText = `${days} ${str}`;
        }
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
