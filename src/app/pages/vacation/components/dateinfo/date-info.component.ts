import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-vacation-date-info",
    template: `
        <div class="headerBg">
            <div class="suprRow">
                <supr-text type="subtitle">{{
                    texts.SELECT_VACATION_DATE
                }}</supr-text>
                <div class="absImg">
                    <supr-svg></supr-svg>
                </div>
            </div>
        </div>
        <div class="divider16"></div>
        <div class="dateBlock">
            <div class="suprRow top" (click)="toggleStartDateModal.emit()">
                <supr-icon name="calendar" class="calendarIcon"></supr-icon>
                <div>
                    <supr-text type="regular14" class="dateHeader">{{
                        texts.START_DATE
                    }}</supr-text>
                    <div class="suprRow">
                        <supr-text type="bold16">{{
                            startDate | date: "EEE, d LLL"
                        }}</supr-text>
                        <div class="spacer4"></div>
                        <supr-icon
                            name="chevron_right"
                            class="chevronRight"
                        ></supr-icon>
                    </div>
                </div>
            </div>
        </div>
        <div class="dateBlock">
            <div class="suprRow top" (click)="toggleEndDateModal.emit()">
                <supr-icon name="calendar" class="calendarIcon"></supr-icon>
                <div>
                    <supr-text type="regular14" class="dateHeader">{{
                        texts.END_DATE
                    }}</supr-text>
                    <div class="suprRow">
                        <supr-text type="bold16">{{
                            endDate | date: "EEE, d LLL"
                        }}</supr-text>
                        <div class="spacer4"></div>
                        <supr-icon
                            name="chevron_right"
                            class="chevronRight"
                        ></supr-icon>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.Default,
})
export class VacationDateInfo {
    @Input() startDate: Date;
    @Input() endDate: Date;
    @Output() toggleStartDateModal: EventEmitter<void> = new EventEmitter();
    @Output() toggleEndDateModal: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;

    constructor() {}
}
