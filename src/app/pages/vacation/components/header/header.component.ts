import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-vacation-header",
    template: `
        <supr-page-header>
            <supr-text type="body" class="pageHeader">
                {{ headerText }}
            </supr-text>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    headerText = TEXTS.PAGE_HEADER_TEXT;
}
