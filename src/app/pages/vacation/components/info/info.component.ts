import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS, VACATION_INFO_POINTS } from "../../constants";

@Component({
    selector: "supr-vacation-info",
    template: `
        <div class="infoBlock">
            <div class="divider20"></div>
            <div class="suprRow">
                <div class="textWrapper">
                    <supr-text type="subtitle" class="title">{{
                        header
                    }}</supr-text>
                    <div class="divider6"></div>
                    <supr-text type="paragraph" class="subTitle">{{
                        subText
                    }}</supr-text>
                </div>
            </div>
            <div class="divider16"></div>
            <div
                *ngFor="
                    let point of vacationInfoPoints;
                    trackBy: trackByFn;
                    last as _last
                "
            >
                <div class="suprRow top">
                    <supr-svg [class]="point.svgClass"></supr-svg>
                    <div class="textWrapper">
                        <supr-text type="paragraphBold" class="pointTitle">{{
                            point.title
                        }}</supr-text>
                        <supr-text type="paragraph" class="pointDesc">{{
                            point.desc
                        }}</supr-text>
                    </div>
                </div>
                <div class="divider12"></div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent {
    header = TEXTS.INFO_HEADER;
    subText = TEXTS.INFO_SUBTEXT;
    vacationInfoPoints = VACATION_INFO_POINTS;

    trackByFn(_: any, index: number): number {
        return index;
    }
}
