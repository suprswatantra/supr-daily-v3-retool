import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { DateService } from "@services/date/date.service";
import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { MoengageService } from "@services/integration/moengage.service";
import { ToastService } from "@services/layout/toast.service";

import { Vacation } from "@models";
import { VacationCurrentState } from "@store/vacation/vacation.state";

import { TEXTS } from "../constants";

@Component({
    selector: "supr-vacation-layout",
    template: `
        <div class="suprContainer">
            <supr-vacation-header></supr-vacation-header>
            <div class="suprScrollContent">
                <supr-vacation-info></supr-vacation-info>
                <div class="divider36"></div>
                <supr-vacation-date-info
                    [startDate]="startDate"
                    [endDate]="endDate"
                    (toggleStartDateModal)="toggleStartDateModal(true)"
                    (toggleEndDateModal)="toggleEndDateModal(true)"
                ></supr-vacation-date-info>
            </div>
            <supr-vaction-footer
                [startDate]="startDate"
                [endDate]="endDate"
                (handleSetVacation)="handleSetVacation()"
            ></supr-vaction-footer>
            <supr-vacation-start-date-calendar-modal
                [showModal]="showStartDateModal"
                [startDate]="startDate"
                (updateStartDate)="updateStartDate($event)"
                (handleHideModal)="toggleStartDateModal(false)"
            ></supr-vacation-start-date-calendar-modal>
            <supr-vacation-end-date-calendar-modal
                [showModal]="showEndDateModal"
                [startDate]="startDate"
                [endDate]="endDate"
                (updateEndDate)="updateEndDate($event)"
                (handleHideModal)="toggleEndDateModal(false)"
            ></supr-vacation-end-date-calendar-modal>
            <supr-vacation-confirm-modal
                [showModal]="showConfirmModal"
                [startDate]="startDate"
                [endDate]="endDate"
                [loading]="loading"
                (confirmVacation)="confirmVacation()"
                (handleHideModal)="toggleConfirmModal(false)"
            ></supr-vacation-confirm-modal>
        </div>
    `,
    styleUrls: ["../styles/supr-vacation.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprVacationLayout implements OnChanges {
    @Input() vacation: Vacation;
    @Input() vacationState: VacationCurrentState;
    @Input() vacationError: any;
    @Output() handleCreateVacation: EventEmitter<Vacation> = new EventEmitter();

    startDate: Date;
    endDate: Date;
    showStartDateModal = false;
    showEndDateModal = false;
    showConfirmModal = false;
    loading = false;

    constructor(
        private toastService: ToastService,
        private moengageService: MoengageService,
        private dateService: DateService,
        private modalService: ModalService,
        private routerService: RouterService
    ) {
        this.startDate = this.dateService.getTomorrowDate();
        this.endDate = this.dateService.getTomorrowDate();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleVacationValueChange(changes["vacation"]);
        this.handleVacationStateChange(changes["vacationState"]);
    }

    handleSetVacation() {
        this.toggleConfirmModal(true);
    }

    updateStartDate(date: Date) {
        this.startDate = date;
        this.endDate = date;
        this.toggleStartDateModal(false);
    }

    updateEndDate(date: Date) {
        this.endDate = date;
        this.toggleEndDateModal(false);
    }

    toggleStartDateModal(show: boolean) {
        this.showStartDateModal = show;
    }

    toggleEndDateModal(show: boolean) {
        this.showEndDateModal = show;
    }

    toggleConfirmModal(show: boolean) {
        this.showConfirmModal = show;
    }

    confirmVacation() {
        const vacation = {
            start_date: this.dateService.textFromDate(this.startDate),
            end_date: this.dateService.textFromDate(this.endDate),
        };
        this.loading = true;
        this.handleCreateVacation.emit(vacation);
    }

    private setDatesFromVacation() {
        if (
            this.vacation &&
            this.vacation.start_date &&
            this.vacation.end_date
        ) {
            this.startDate = this.dateService.dateFromText(
                this.vacation.start_date
            );
            this.endDate = this.dateService.dateFromText(
                this.vacation.end_date
            );
        }
    }

    private handleVacationValueChange(vacationChange: SimpleChange) {
        if (
            vacationChange &&
            vacationChange.currentValue !== vacationChange.previousValue
        ) {
            this.setDatesFromVacation();
        }
    }

    private handleVacationStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.previousValue === VacationCurrentState.CREATING_VACATION &&
            change.currentValue === VacationCurrentState.NO_ACTION
        ) {
            this.modalService.closeModal();
            if (!this.vacationError) {
                this.showSuccessMessage();
                this.sendAnalyticsEvents();
                this.routerService.goToHomePage();
            }
        }
    }

    private showSuccessMessage() {
        const startDate = this.getFormatDate(this.startDate);
        const endDate = this.getFormatDate(this.endDate);

        const message = this.getToastMessage(
            TEXTS.VACATION_CREATE_SUCCESSFUL,
            startDate,
            endDate
        );

        this.toastService.present(message);
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackVacationUpdate({
            start_date: this.dateService.textFromDate(this.startDate),
            end_date: this.dateService.textFromDate(this.endDate),
            status: "active",
        });
    }

    private getToastMessage(messagePrefix, startDate, endDate) {
        const isVacationOneDay = startDate === endDate;

        return `${messagePrefix} ${
            !isVacationOneDay ? "from" : "for"
        } ${startDate} ${!isVacationOneDay ? "to " + endDate : ""}`;
    }

    private getFormatDate(date: Date): string {
        return this.dateService.formatDate(date, "d LLL yyy");
    }
}
