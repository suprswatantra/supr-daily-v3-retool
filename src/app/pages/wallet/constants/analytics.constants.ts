export const SA_OBJECT_NAMES = {
    CLICK: {
        ADD_MONEY: "add-money",
        AMOUNT: "amount",
        ALL_TRANSACTIONS: "all-transactions",
        APPLY_PROMO: "apply-promo-text",
        CLEAR_PROMO: "clear-promo-code",
        APPLY_PROMO_BUTTON: "apply-promo-button",
        ADD_NEW_PAYMENT_MODE: "add-new",
        TRY_OTHER_MODES: "other-modes",
        RETRY: "retry",
        SHOW_OTHER_MODES: "show-other-modes",
    },
    IMPRESSION: {
        ALL_TRANSACTIONS: "all-transactions",
        APPLY_PROMO_CONTAINER: "apply-promo-container",
        PAYMENT_FAILED_MODAL: "transaction-failed",
        INVALID_COUPON: "invalid-recharge-coupon",
    },
    CONTEXT_LIST: {
        PAYMENT_METHOD: "payment-method",
    },
    CONTEXT: {
        REMOVE_COUPON: "wallet",
        FROM_CART: "from-cart",
        WALLET: "wallet",
    },
};
