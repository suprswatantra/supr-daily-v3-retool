export const TEXTS = {
    ADD_MONEY_TITLE: "Add money to Supr Wallet",
    ADD_MONEY_SUBTITLE: "We suggest an average balance of ₹1000",
    PROCESSING_PAYMENT: "Processing payment",
    PAYMENT_SUCCESS: "Payment is successful",
    CURRENT_BALANCE: "Wallet balance",
    SUPR_CREDITS_BALANCE: "Supr Credits",
    MOST_POPULAR: "Most popular",
    ALL_TRANSACTIONS: "View All transactions",
    ADD_MONEY: "ADD MONEY",
    ALL_TRANSACTIONS_HEADER: "All transactions",
    NO_SUPR_CREDIT_TRANSACTIONS_TITLE: "No Supr Credits yet",
    NO_WALLET_TRANSACTIONS_TITLE: "No transactions yet",
    NO_SUPR_CREDIT_TRANSACTIONS_SUBTITLE:
        "You haven’t earned any Supr Credits yet.",
    NO_WALLET_TRANSACTIONS_SUBTITLE:
        "There are currently no transactions made.",
    MAESTRO_NO_CVV_EXPIRY: "My Maestro Card doesn't have a CVV / Expiry",
    SAVE_CARD: "Save card for future",
    ADD_CARD: "Add credit or debit card",
    SAVE_VPA: "Save UPI id for future",
    ADD_VPA: "Add new UPI Id",
    VPA_PLACEHOLDER: "Enter new UPI id",
    INVALID_VPA_INPUT: "Please enter a valid UPI id",
    APPLY: "Apply",
    OTHER_MODES_RESTRICTED_TEXT:
        "NOTE: Applied coupon code is not applicable for other payment options. Please remove coupon to proceed.",
    OFFERS: "Offers",
    APPLY_COUPON: "Apply coupon",
    APPLY_COUPON_SUBTITLE: "Enter promo code below",
};

export const TRANSACTION_TYPE = {
    RECHARGE: "recharge",
    PAYMENT: "payment",
    SUPR_CREDITS: "supr credits",
    SUPR_CREDITS_PURCHASE: "Supr credit purchase",
    BOUGHT_SUPR_PASS: "Supr pass purchase",
};

export const TRANSACTION_TYPE_V2 = {
    WALLET: "Wallet",
    SUPR_CREDITS: "Supr Credits",
};

export const PAYMENT_TRANSACTION_TYPE = {
    SUBSCRIPTION_RECHARGE: "Subcription rechcarge",
    ADDON: "Addon",
};

export const PAYMENT_TRANSACTION_PAYMENT_TYPE = {
    PAID: "Paid",
    REFUND: "Refund",
};

export const RECHARGE_TRANSACTION_TYPE = {
    ONLINE: "online",
    PAYTM: "paytm",
    CASH: "cash",
    CARD: "card",
    CREDIT_CARD: "credit_card",
    DEBIT_CARD: "debit_card",
    NET_BANKING: "netbanking",
    WALLET: "wallet",
    EMI: "emi",
    UPI: "upi",
    CHEQUE: "cheque",
    COUPON: "coupon",
    TESTING: "testing",
    PROMO: "promo",
    REFERRAL: "referral",
    TRANSFER: "transfer",
    OTHER: "other",
};

export const RECHARGE_TRANSACTION_DISPLAY_NAME = {
    online: "Online",
    paytm: "PayTM",
    cash: "Cash",
    card: "Card",
    credit_card: "Credit card",
    debit_card: "Debit card",
    netbanking: "Net banking",
    wallet: "Wallet",
    emi: "Emi",
    upi: "Upi",
    cheque: "Cheque",
    coupon: "Coupon",
    testing: "Testing",
    promo: "Promo",
    referral: "Referral",
    transfer: "Transfer",
    other: "Other",
};

export const RECHARGE_TYPE_DISPLAY_NAME = {
    referral: "Referral Bonus",
};

export const TRANSACTION_FILTERS = [
    { key: "All", label: "All" },
    { key: TRANSACTION_TYPE.PAYMENT, label: `${TRANSACTION_TYPE.PAYMENT}s` },
    { key: TRANSACTION_TYPE.RECHARGE, label: `${TRANSACTION_TYPE.RECHARGE}s` },
];

export const TRANSACTION_V2_FILTERS = [
    {
        key: TRANSACTION_TYPE_V2.WALLET,
        label: "Wallet",
    },
    {
        key: TRANSACTION_TYPE_V2.SUPR_CREDITS,
        label: "Supr Credits",
    },
];

export const RECHARGE_COUPON_TEXTS = {
    NOT_APPLIED: "Apply Promo",
    APPLIED: "Applied!",
    PROMO_PLACEHOLDER: "Enter promo here",
};

export const RECHARGE_SUCCESSFUL_TEXTS = {
    RECHARGE_SUCCESSFUL: "Recharge successful",
    WALLET_BALANCE: "Wallet balance",
    SUPR_CREDITS_BALANCE: "Supr Credits",
};

export const SUPR_CREDITS_TRANSACTION_TYPE = {
    ISSUED: "ISSUED",
    USED: "USED",
    REFUNDED: "REFUNDED",
    REVERSED: "REVERSED",
    EXPIRED: "EXPIRED",
};

export const SC_GAIN_ARRAY = [
    SUPR_CREDITS_TRANSACTION_TYPE.ISSUED,
    SUPR_CREDITS_TRANSACTION_TYPE.REFUNDED,
];

export const TRANSACTION_TEXTS = {
    PAID_AFTER_DISCOUNT: "Paid after discount",
    SUPR_CREDITS: "Supr Credits",
    UPDATED_BALANCE: "Updated balance",
    REFUND: "Refund",
    PAYMENT_ID: "Payment id",
    BOUGHT_SC: "Bought Supr Credits",
};

export const RECHARGE_SUCCESSFUL_DISPLAY_TIME = 4000; // in ms

export const AMOUNT_ERROR_FOR_COUPON =
    "Please enter valid amount before applying promo";

export const EXPIRY_TEXT = "Supr Credits will expire on";

export const WALLET_OFFERS_TEXT = {
    SUB_TEXT_1: "Buy",
    SUB_TEXT_2: "for only",
    CTA_TEXT: "BUY NOW",
    NEW_OFFER_TEXT: "New Offer",
    HEADER: "Offers",
};

export const DURATION = {
    MONTH_TEXT: "Month",
    DAYS_IN_MONTH: 30,
    PLURAL_CHARACTER: "s",
};

export const BACK_BTN_SUB_PRIORITY = 2000;

export const CARD_FORM = {
    CVV: {
        text: "CVV*",
        formField: "card[cvv]",
    },
    NAME: {
        text: "Name on card*",
        formField: "card[name]",
    },
    EXPIRY: { text: "Expiry MM/YY*", formField: "expiry" },
    CARD_NUMBER: { text: "Card Number*", formField: "card" },
};

export const CARD_SERVICE_DATA = {
    SAVE: "save",
    CARD: "card",
    CVV: "card[cvv]",
    EXPIRY: "expiry",
    NAME: "card[name]",
    NETWORK: "network",
    VALIDATE_EXPIRY_CVV: "validateExpiryAndCvv",
};

export const VPA_SERVICE_DATA = {
    VPA: "vpa",
};

export const WALLET_BANKS_COUNT = 8;

export const APPLY_COUPON_ICON = "offers_hexagon";
