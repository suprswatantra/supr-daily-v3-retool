import { Component, ChangeDetectionStrategy } from "@angular/core";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-wallet-transactions-layout",
    template: `
        <div
            class="suprContainer"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.ALL_TRANSACTIONS}"
        >
            <supr-page-header>
                <supr-text type="subheading">All transactions</supr-text>
            </supr-page-header>

            <div class="suprScrollContent">
                <supr-wallet-transactions-header></supr-wallet-transactions-header>
                <supr-wallet-transactions-container></supr-wallet-transactions-container>
            </div>
        </div>
    `,
    styleUrls: ["../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsLayoutComponent {}
