import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-wallet-core-layout",
    template: `
        <ion-router-outlet
            [animated]="true"
            [swipeGesture]="false"
        ></ion-router-outlet>
    `,
    styleUrls: ["../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletPageCoreLayoutComponent {}
