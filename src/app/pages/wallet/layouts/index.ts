import { TransactionsLayoutComponent } from "./transactions.layout";
import { TransactionsV2LayoutComponent } from "./transactions-v2.layout";
import { WalletLayoutComponent } from "./wallet.layout";
import { WalletPageCoreLayoutComponent } from "./core.layout";
import { WalletOffersLayout } from "./offers.layout";

export const walletLayouts = [
    WalletPageCoreLayoutComponent,
    WalletLayoutComponent,
    TransactionsLayoutComponent,
    TransactionsV2LayoutComponent,
    WalletOffersLayout,
];
