import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    OnInit,
} from "@angular/core";

import { OffersList, RechargeCouponInfo } from "@models";

import { ToastService } from "@services/layout/toast.service";
import { RouterService } from "@services/util/router.service";

import { TEXTS, APPLY_COUPON_ICON } from "../constants/wallet.constants";

@Component({
    selector: "supr-wallet-offers-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="body">${TEXTS.OFFERS}</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-section-header
                    icon="${APPLY_COUPON_ICON}"
                    title="${TEXTS.APPLY_COUPON}"
                    subtitle="${TEXTS.APPLY_COUPON_SUBTITLE}"
                >
                </supr-section-header>
                <supr-apply-coupon-input
                    [hideHeading]="true"
                    [invalidCoupon]="invalidCoupon"
                    [couponErrorMsg]="couponMsg"
                    [isApplyingCouponCode]="isApplyingCouponCode"
                    (handleApply)="handleCouponApply($event)"
                    (handleCouponChange)="couponChange()"
                ></supr-apply-coupon-input>
                <supr-offers-list
                    [isFetching]="isFetchingWalletOffers"
                    [offersList]="walletOffersList"
                    (handleCouponTapApply)="handleCouponApply($event)"
                ></supr-offers-list>
            </div>
            <supr-loader-overlay *ngIf="isApplyingCouponCode">
            </supr-loader-overlay>
        </div>
    `,
    styleUrls: ["../styles/offers.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletOffersLayout implements OnInit, OnChanges {
    @Input() rechargeCouponInfo: RechargeCouponInfo;
    @Input() isFetchingWalletOffers: boolean;
    @Input() walletOffersList: OffersList;
    @Input() isApplyingCouponCode: boolean;
    @Input() amount: number;

    @Output() handleUpdateCoupon: EventEmitter<{
        couponCode: string;
        amount: number;
    }> = new EventEmitter();
    @Output()
    handleFetchWalletOffers: EventEmitter<number> = new EventEmitter();

    couponCodeValue = "";
    invalidCoupon = false;
    couponMsg = "";

    constructor(
        private routerService: RouterService,
        private toastService: ToastService
    ) {}

    ngOnInit() {
        setTimeout(() => {
            this.handleFetchWalletOffers.emit(this.amount);
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCouponCodeChange(changes["rechargeCouponInfo"]);
        this.handleAmountChange(changes["amount"]);
    }

    handleCouponApply(couponCode: string) {
        const amount = this.amount;
        this.handleUpdateCoupon.emit({ couponCode, amount });
    }

    couponChange() {
        this.invalidCoupon = false;
        this.couponMsg = "";
    }

    private handleAmountChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleFetchWalletOffers.emit(this.amount);
        }
    }

    private handleCoupon() {
        if (this.rechargeCouponInfo) {
            this.setCoupon();
        } else {
            this.unsetCoupon();
        }
    }

    private setCoupon() {
        const couponData = this.rechargeCouponInfo;

        this.couponCodeValue = couponData.couponCode;
        this.invalidCoupon = !couponData.isApplied;
        this.couponMsg = couponData.message;

        if (
            this.couponCodeValue &&
            !this.invalidCoupon &&
            !this.isApplyingCouponCode
        ) {
            this.toastService.present(
                `${this.couponCodeValue} applied successfully`
            );

            const queryParams = {
                amount: this.amount,
            };

            this.routerService.goToWalletPage({ queryParams });
        }
    }

    private unsetCoupon() {
        this.couponCodeValue = "";
        this.invalidCoupon = false;
        this.couponMsg = "";
    }

    private handleCouponCodeChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleCoupon();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
