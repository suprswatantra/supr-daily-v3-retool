import {
    Input,
    Output,
    OnInit,
    NgZone,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    SETTINGS,
    TOAST_MESSAGES,
    PAYMENT_METHOD_VERSIONS,
    SETTINGS_KEYS_DEFAULT_VALUE,
    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE,
} from "@constants";

import { User, WalletMeta, PaymentInfo, RechargeCouponInfo } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";

import { CartService } from "@services/shared/cart.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";
import { WalletService } from "@services/shared/wallet.service";
import { PaymentService } from "@services/integration/payment.service";
import { MoengageService } from "@services/integration/moengage.service";
import { WalletPageService } from "@pages/wallet/services/wallet.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { WalletCurrentState } from "@store/wallet/wallet.state";

import {
    SuprApi,
    SelectPaymentMethodV2,
    PaymentMethodComponent,
    PaymentMethodV2Component,
    RazorpayCustomPaymentSuccessRes,
} from "@types";

import { TEXTS } from "@pages/wallet/constants/wallet.constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-wallet-layout",
    template: `
        <div class="suprContainer">
            <ng-container *ngIf="!_showAllBanks; else allBanksView">
                <supr-page-header>
                    <supr-text type="subheading">Supr Wallet</supr-text>
                </supr-page-header>

                <div class="suprScrollContent" #suprScrollContent>
                    <supr-wallet-header
                        [autoCheckout]="autoCheckout"
                        [isSuprCreditsEnabled]="suprCreditsEnabled"
                    ></supr-wallet-header>
                    <supr-wallet-add-money
                        [options]="options"
                        [amountToFill]="_amountToFill"
                        [defaultAmount]="selectedAmount || defaultAmount"
                        [suprScrollContentEl]="suprScrollContent"
                        [walletMeta]="walletMeta"
                        [suprCreditsEnabled]="suprCreditsEnabled"
                        [isWalletVersionV2]="isWalletVersionV2"
                        [autoCheckout]="autoCheckout"
                        [usingCartPaymentCouponCode]="
                            usingCartPaymentCouponCode
                        "
                        (clearRechargeCoupon)="clearRechargeCoupon.emit()"
                        (handleInputChange)="updateAmountChange($event)"
                    ></supr-wallet-add-money>

                    <ng-container *ngIf="paymentMethods || paymentMethodsV2">
                        <ng-container
                            *ngFor="
                                let methodData of paymentMethods ||
                                    paymentMethodsV2;
                                last as _last
                            "
                        >
                            <supr-wallet-payment-mode-wrapper
                                [lastGroup]="_last"
                                [amount]="selectedAmount"
                                [methodData]="methodData"
                                [group]="methodData?.group"
                                [version]="paymentMethodsVersion"
                                [selectedMethod]="
                                    selectedPaymentMethodV2?.id ||
                                    selectedPaymentMethod
                                "
                                (updateSelectedMode)="
                                    updateSelectedPaymentMethod($event)
                                "
                                (updateSelectedModeV2)="
                                    updateSelectedPaymentMethodV2($event)
                                "
                                (handleNetbankingLayoutCtaClick)="
                                    _handleNetbankingLayoutCtaClick()
                                "
                            >
                            </supr-wallet-payment-mode-wrapper>
                        </ng-container>
                        <div
                            class="showOtherModes"
                            *ngIf="
                                hasFetchedFilteredMethods &&
                                (usingCartPaymentCouponCode ||
                                    walletMeta?.rechargeCouponInfo?.isApplied)
                            "
                        >
                            <supr-section-header
                                [chevronRight]="true"
                                subTitleType="paragraph"
                                icon="card"
                                [title]="paymentOfferOtherModesText?.title"
                                [subtitle]="
                                    paymentOfferOtherModesText?.subtitle
                                "
                                (click)="showOtherModes()"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .SHOW_OTHER_MODES}"
                                [saObjectValue]="
                                    usingCartPaymentCouponCode
                                        ? OBJECT_NAMES.CONTEXT.FROM_CART
                                        : OBJECT_NAMES.CONTEXT.WALLET
                                "
                            >
                            </supr-section-header>
                        </div>
                        <div class="divider24"></div>
                    </ng-container>
                </div>
            </ng-container>

            <ng-template #allBanksView>
                <supr-wallet-banks-search-container
                    [selectedOption]="selectedPaymentMethodV2?.id"
                    (closeBanksSearchView)="_hideAllBanksSeachView()"
                    (handleItemClick)="updateSelectedPaymentMethodV2($event)"
                >
                </supr-wallet-banks-search-container>
            </ng-template>

            <ng-container *ngIf="showCustomPaymentFailureErrorModal">
                <supr-wallet-payment-failure-modal
                    [showModal]="showCustomPaymentFailureErrorModal"
                    [errType]="customPaymentFailureErrorType"
                    [err]="customPaymentFailureError"
                    [amountToPay]="selectedAmount"
                    [selectedPaymentMethod]="selectedPaymentMethodV2"
                    (handleClose)="closeCustomPaymentFailureModal()"
                    (handlePaymentRetry)="handleCustomPaymentRetry()"
                >
                </supr-wallet-payment-failure-modal>
            </ng-container>

            <supr-page-footer>
                <supr-button
                    (handleClick)="openRazorpay()"
                    [disabled]="isBtnDisabled"
                    [loading]="_showButtonLoader"
                    [saObjectValue]="selectedAmount"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_MONEY}"
                    [saContext]="
                        selectedPaymentMethodV2?.method || selectedPaymentMethod
                    "
                >
                    <supr-text type="body">{{ TEXTS.ADD_MONEY }}</supr-text>
                </supr-button>
            </supr-page-footer>

            <supr-loader-overlay *ngIf="_showLoader">
                {{ TEXTS.PROCESSING_PAYMENT }}
            </supr-loader-overlay>
            <supr-loader-overlay *ngIf="loadingInitialData">
            </supr-loader-overlay>
        </div>
    `,
    styleUrls: ["../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletLayoutComponent implements OnInit, OnChanges {
    @Input() user: User;
    @Input() walletError: any;
    @Input() options: number[];
    @Input() defaultAmount: number;
    @Input() autoCheckout: boolean;
    @Input() walletMeta: WalletMeta;
    @Input() fromSuprPassPage: boolean;
    @Input() isWalletVersionV2: boolean;
    @Input() suprCreditsEnabled: boolean;
    @Input() loadingInitialData: boolean;
    @Input() paymentMethodsVersion: number;
    @Input() selectedPaymentMethod: string;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() walletState: WalletCurrentState;
    @Input() paymentMethods: PaymentMethodComponent[];
    @Input() paymentMethodsV2: PaymentMethodV2Component[];
    @Input() selectedPaymentMethodV2: SelectPaymentMethodV2;
    @Input() usingCartPaymentCouponCode: boolean;
    @Input() cartPaymentCouponCodeInfo: RechargeCouponInfo;
    @Input() hasFetchedFilteredMethods: boolean;
    @Input()
    set amountToFill(_amount: number) {
        this._amountToFill = _amount ? Math.ceil(_amount) : 0;
        this.updateAmountChange(this._amountToFill);
    }

    @Output()
    handleCreatePayment: EventEmitter<PaymentInfo> = new EventEmitter();
    @Output() clearRechargeCoupon: EventEmitter<void> = new EventEmitter();
    @Output()
    handleFetchInitialData: EventEmitter<boolean> = new EventEmitter();
    @Output()
    handleCustomPaymentCapture: EventEmitter<SuprApi.CustomPaymentCaptureReq> = new EventEmitter();

    _amountToFill = 0;
    _showLoader = false;
    isBtnDisabled = true;
    _showAllBanks = false;
    walletTncText: string;
    _showButtonLoader = false;
    showCustomPaymentFailureErrorModal: boolean;
    customPaymentFailureErrorType:
        | RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR
        | RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PRE_PROCESSING_ERROR;
    customPaymentFailureError: Error;

    selectedAmount: number;
    paymentOfferOtherModesText: { title: string; subtitle: string };

    private buySuprCredits: boolean;

    TEXTS = TEXTS;
    OBJECT_NAMES = SA_OBJECT_NAMES;

    constructor(
        private ngZone: NgZone,
        private cdr: ChangeDetectorRef,
        private cartService: CartService,
        private toastService: ToastService,
        private walletService: WalletService,
        private routerService: RouterService,
        private paymentService: PaymentService,
        private moengageService: MoengageService,
        private walletPageService: WalletPageService,
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {
        /* fetch razorpay checkoutjs file for web only */
        // this.paymentService.initialize();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.handleWalletStateChange(changes["walletState"]);
        if (this.canHandleChange(changes["usingCartPaymentCouponCode"])) {
            this.handleFetchInitialData.emit(this.usingCartPaymentCouponCode);
        }
    }

    showOtherModes() {
        /** If coming from cart then take user back to cart else clear coupon  */
        if (this.usingCartPaymentCouponCode) {
            return this.routerService.goBack();
        }
        this.clearRechargeCoupon.emit();
    }

    openRazorpay() {
        if (this.shouldUseCustomCheckout()) {
            this.processCustomCheckout();
            return;
        }

        /* open Razorpay */
        if (this.isCheckoutDisabled()) {
            return;
        }

        /* If coming from cart using payment coupon use coupon info from cart store else wallet store */
        const data: any = {
            amount: this.selectedAmount,
            user: this.user,
            method: this.getSelectedMethodForStandardCheckout(),
            couponInfo: this.usingCartPaymentCouponCode
                ? this.cartPaymentCouponCodeInfo
                : this.utilService.getNestedValue(
                      this.walletMeta,
                      "rechargeCouponInfo",
                      null
                  ),
        };

        try {
            this.showButtonLoader();
            this.paymentService.openRazorpay(
                data,
                this.onRazorpaySuccess,
                this.handleRazorpayModalDismiss
            );
        } catch (error) {
            this.hideButtonLoader();
        }
    }

    processCustomCheckout() {
        if (this.isCheckoutDisabled()) {
            return;
        }

        const selectedPaymentMethodInfo = this.walletPageService.getSelectedPaymentMethodInfo(
            this.selectedPaymentMethodV2
        );

        /* If coming from cart using payment coupon use coupon info from cart store else wallet store */
        this.walletPageService
            .getRazorpayCustomCheckoutData(
                this.user,
                this.selectedAmount,
                selectedPaymentMethodInfo,
                this.usingCartPaymentCouponCode
                    ? this.cartPaymentCouponCodeInfo
                    : this.utilService.getNestedValue(
                          this.walletMeta,
                          "rechargeCouponInfo",
                          null
                      ),
                this.buySuprCredits
            )
            .subscribe((data) => {
                if (!data) {
                    return;
                }

                try {
                    this.showButtonLoader();
                    this.paymentService.handleCustomCheckout(
                        data,
                        this.onCustomRazorpaySuccess,
                        this.handleCustomRazorpayFailure
                    );
                } catch (error) {
                    this.hideButtonLoader();
                }
            });
    }

    updateAmountChange(amount: number) {
        if (amount) {
            this.selectedAmount = amount;
        }
        this.setBtnDisabled(amount);
    }

    updateSelectedPaymentMethod(selectedPaymentMethod: string) {
        this.selectedPaymentMethod = selectedPaymentMethod;
    }

    updateSelectedPaymentMethodV2(data: SelectPaymentMethodV2) {
        this.selectedPaymentMethodV2 = data;
    }

    closeCustomPaymentFailureModal() {
        this.showCustomPaymentFailureErrorModal = false;
        this.customPaymentFailureErrorType = undefined;
        this.customPaymentFailureError = undefined;
    }

    handleCustomPaymentRetry() {
        this.processCustomCheckout();
    }

    _handleNetbankingLayoutCtaClick() {
        this._showAllBanks = true;
    }

    _hideAllBanksSeachView() {
        this._showAllBanks = false;
    }

    private setPaymentOfferOtherModesText() {
        this.paymentOfferOtherModesText = this.settingsService.getSettingsValue(
            SETTINGS.PAYMENT_OFFER_OTHER_MODES_TEXT,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.PAYMENT_OFFER_OTHER_MODES_TEXT]
        );
    }

    private shouldUseCustomCheckout() {
        if (this.paymentMethodsVersion !== PAYMENT_METHOD_VERSIONS.V2) {
            return false;
        }
        const selectedPaymentMethodInfo = this.walletPageService.getSelectedPaymentMethodInfo(
            this.selectedPaymentMethodV2
        );
        if (this.shouldUseStandarCheckoutOnV2(selectedPaymentMethodInfo)) {
            return false;
        }
        return true;
    }

    private getSelectedMethodForStandardCheckout() {
        if (this.paymentMethodsVersion !== PAYMENT_METHOD_VERSIONS.V2) {
            return this.selectedPaymentMethod || "";
        }
        const selectedPaymentMethodInfo = this.walletPageService.getSelectedPaymentMethodInfo(
            this.selectedPaymentMethodV2
        );
        if (this.shouldUseStandarCheckoutOnV2(selectedPaymentMethodInfo)) {
            return selectedPaymentMethodInfo.method || "";
        }
        return "";
    }

    private shouldUseStandarCheckoutOnV2(
        selectedPaymentMethodInfo: SelectPaymentMethodV2
    ) {
        const useStandardCheckout = this.utilService.getNestedValue(
            selectedPaymentMethodInfo,
            "paymentInfo.standardCheckout",
            false
        );
        return useStandardCheckout;
    }

    private isCheckoutDisabled(): boolean {
        return this.cartService.isCheckoutDisabled(this.userCheckoutInfo);
    }

    private initialize() {
        const previousUrl = this.routerService.getPreviousUrl();

        this.setWalletTnc();

        if (!this.walletPageService.isWalletTransactionsPage(previousUrl)) {
            this.handleFetchInitialData.emit(this.usingCartPaymentCouponCode);
        }

        this.setPaymentOfferOtherModesText();
    }

    private setWalletTnc() {
        const tnc = this.settingsService.getSettingsValue(
            SETTINGS.WALLET_TNC,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.WALLET_TNC]
        );

        this.walletTncText = tnc.text;
    }

    private handleWalletStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.previousValue === WalletCurrentState.CREATING_PAYMENT &&
            change.currentValue === WalletCurrentState.NO_ACTION
        ) {
            this.ngZone.run(() => {
                this.hideLoader();
                this.sendAnalyticsEvents();
                if (!this.walletError) {
                    this.handlePaymentSuccess();
                }
            });
        }
    }

    private handlePaymentSuccess() {
        if (!this.autoCheckout) {
            this.routerService.goToWalletRechargeThankyouPage();
            return;
        }

        if (this.fromSuprPassPage) {
            const queryParams = {
                fromSuprPassPage: true,
            };
            this.routerService.goToCartPage({ queryParams });
        } else {
            this.goBack();
        }

        this.showToast();
    }

    private onCustomRazorpaySuccess = (
        paymentInfo: RazorpayCustomPaymentSuccessRes
    ) => {
        this.handleRazorpayModalDismiss();
        this.ngZone.run(() => {
            this.showLoader();
            this.hideButtonLoader();
            this.selectedPaymentMethodV2 = null;
            const data: SuprApi.CustomPaymentCaptureReq = {
                payment_id: paymentInfo.payment_id,
                order_id: paymentInfo.order_id,
                partner_customer_id: paymentInfo.customer_id,
                payment_partner: paymentInfo.payment_gateway,
                amount: paymentInfo.amount,
                coupon_code:
                    this.isCouponApplied() &&
                    !this.buySuprCredits &&
                    this.walletMeta.rechargeCouponInfo.couponCode,
                buy_supr_credits: this.buySuprCredits,
            };

            this.handleCustomPaymentCapture.emit(data);
        });
    };

    private onRazorpaySuccess = (paymentId: string) => {
        this.ngZone.run(() => {
            this.hideButtonLoader();
            this.showLoader();
            const data: PaymentInfo = {
                paymentId: paymentId,
                amount: this.selectedAmount,
            };

            if (this.isCouponApplied() && !this.buySuprCredits) {
                data.couponCode = this.walletMeta.rechargeCouponInfo.couponCode;
            }

            if (this.buySuprCredits) {
                data.buySuprCredits = true;
            }

            this.handleCreatePayment.emit(data);
        });
    };

    private isCouponApplied() {
        return (
            this.walletMeta &&
            this.walletMeta.rechargeCouponInfo &&
            this.walletMeta.rechargeCouponInfo.isApplied
        );
    }

    private showLoader() {
        this._showLoader = true;
    }

    private hideLoader() {
        this._showLoader = false;
    }

    private showButtonLoader() {
        this._showButtonLoader = true;
    }

    private hideButtonLoader() {
        this._showButtonLoader = false;
    }

    private goBack() {
        if (this.autoCheckout) {
            this.cartService.setAutoCheckoutFlag();
            this.routerService.goBack();
        }
    }

    private showToast() {
        if (this.autoCheckout) {
            return;
        }

        this.toastService.present(TOAST_MESSAGES.WALLET.PAYMENT_SUCCESSFULL);
    }

    private setBtnDisabled(amount: number) {
        this.isBtnDisabled = amount ? false : true;
    }

    private sendAnalyticsEvents() {
        try {
            const walletBalance =
                this.selectedAmount + this.walletService.getWalletBalance();

            const couponCode =
                this.walletMeta && this.walletMeta.rechargeCouponInfo
                    ? this.walletMeta.rechargeCouponInfo.couponCode
                    : "";

            this.moengageService.trackWalletRecharge({
                recharge_amount: this.selectedAmount,
                status: this.walletError ? "failure" : "success",
                wallet_balance: walletBalance,
                coupon_code: couponCode,
            });
        } catch (_error) {}
    }

    private handleRazorpayModalDismiss = () => {
        this.hideButtonLoader();
        this.cdr.detectChanges();
    };

    private handleCustomRazorpayFailure = (
        errType?: RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE | null,
        err?: Error
    ) => {
        this.ngZone.run(() => {
            this.handleRazorpayModalDismiss();
            if (
                errType &&
                (errType ===
                    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR ||
                    errType ===
                        RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PRE_PROCESSING_ERROR)
            ) {
                this.showCustomPaymentFailureErrorModal = true;
                this.customPaymentFailureErrorType = errType;
                this.customPaymentFailureError = err;
                this.cdr.detectChanges();
            }
        });
    };

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
