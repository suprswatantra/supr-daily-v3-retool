import { Component, ChangeDetectionStrategy } from "@angular/core";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";
import { TEXTS } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-transactions-v2-layout",
    template: `
        <div
            class="suprContainer"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.ALL_TRANSACTIONS}"
        >
            <supr-page-header>
                <supr-text type="subheading">{{ ALL_TRANSACTIONS }}</supr-text>
            </supr-page-header>

            <div class="suprScrollContent">
                <supr-wallet-transactions-v2-header></supr-wallet-transactions-v2-header>
                <supr-wallet-transactions-v2-container></supr-wallet-transactions-v2-container>
            </div>
        </div>
    `,
    styleUrls: ["../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsV2LayoutComponent {
    ALL_TRANSACTIONS = TEXTS.ALL_TRANSACTIONS_HEADER;
}
