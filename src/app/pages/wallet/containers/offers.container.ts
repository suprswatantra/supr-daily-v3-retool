import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";

import { map, filter } from "rxjs/operators";

import { WalletAdapter } from "@shared/adapters/wallet.adapter";

import { OffersList, RechargeCouponInfo } from "@models";

@Component({
    selector: "supr-wallet-offers-container",
    template: `
        <supr-wallet-offers-layout
            [rechargeCouponInfo]="rechargeCouponInfo$ | async"
            [isFetchingWalletOffers]="isFetchingWalletOffers$ | async"
            [isApplyingCouponCode]="isApplyingCouponCode$ | async"
            [walletOffersList]="walletOffersList$ | async"
            [amount]="amount$ | async"
            (handleUpdateCoupon)="applyRechargeCoupon($event)"
            (handleFetchWalletOffers)="fetchWalletOffers($event)"
        ></supr-wallet-offers-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletOffersContainer implements OnInit {
    rechargeCouponInfo$: Observable<RechargeCouponInfo>;
    isFetchingWalletOffers$: Observable<boolean>;
    walletOffersList$: Observable<OffersList>;
    isApplyingCouponCode$: Observable<boolean>;
    amount$: Observable<number>;

    constructor(private walletAdapter: WalletAdapter) {}

    ngOnInit() {
        this.rechargeCouponInfo$ = this.walletAdapter.rechargeCouponInfo$;
        this.isFetchingWalletOffers$ = this.walletAdapter.isFetchingWalletOffers$;
        this.walletOffersList$ = this.walletAdapter.walletOffersList$;
        this.isApplyingCouponCode$ = this.walletAdapter.isApplyingCouponCode$;
        this.amount$ = this.walletAdapter.urlParams$.pipe(
            map((params: Params) => +params["amount"]),
            filter((amount) => !isNaN(amount) && amount > 0)
        );
    }

    applyRechargeCoupon(data: { couponCode: string; amount: number }) {
        this.walletAdapter.applyRechargeCoupon(data);
    }

    fetchWalletOffers(amount: number) {
        this.walletAdapter.fetchWalletOffers(amount);
    }
}
