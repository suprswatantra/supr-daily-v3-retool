import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Transaction, WalletExpiryDetails } from "@models";
import { WalletPageAdapter as Adapter } from "../services/wallet.adapter";

@Component({
    selector: "supr-wallet-transactions-v2-container",
    template: `
        <supr-wallet-transactions-v2
            [transactions]="transactionList$ | async"
            [walletExpiryDetails]="walletExpiryDetails$ | async"
        >
        </supr-wallet-transactions-v2>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsV2Container implements OnInit {
    transactionList$: Observable<Transaction[]>;
    walletExpiryDetails$: Observable<WalletExpiryDetails>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.transactionList$ = this.adapter.transactionList$;
        this.walletExpiryDetails$ = this.adapter.walletExpiryDetails$;

        this.fetchTransactionDetails();
    }

    private fetchTransactionDetails() {
        this.adapter.fetchTransactionDetails();
    }
}
