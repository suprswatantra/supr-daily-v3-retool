import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { PaymentMethodOption } from "@models";

import { SelectPaymentMethodV2 } from "@types";

import { WalletPageAdapter } from "../services/wallet.adapter";

@Component({
    selector: "supr-wallet-banks-search-container",
    template: `<supr-wallet-banks-search
        [selectedOption]="selectedOption"
        [options]="netBankingOptions$ | async"
        (handleItemClick)="handleItemClick.emit($event)"
        (closeBanksSearchView)="closeBanksSearchView.emit()"
    ></supr-wallet-banks-search>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BanksSearchContainer implements OnInit {
    @Input() selectedOption: number;

    @Output() closeBanksSearchView: EventEmitter<void> = new EventEmitter();
    @Output() handleItemClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    netBankingOptions$: Observable<PaymentMethodOption[]>;

    constructor(private walletPageAdapter: WalletPageAdapter) {}

    ngOnInit() {
        this.netBankingOptions$ = this.walletPageAdapter.netBankingOptions$;
    }
}
