import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Transaction } from "@models";
import { WalletPageAdapter as Adapter } from "../services/wallet.adapter";

@Component({
    selector: "supr-wallet-transactions-container",
    template: `
        <supr-wallet-transactions [transactions]="transactions$ | async">
        </supr-wallet-transactions>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsContainer implements OnInit {
    transactions$: Observable<Transaction[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.transactions$ = this.adapter.fetchTransactionsList();
    }
}
