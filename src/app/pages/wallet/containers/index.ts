import { BalanceContainer } from "./balance.container";
import { WalletPageContainer } from "./wallet.container";
import { BanksSearchContainer } from "./banks-search.container";
import { TransactionsContainer } from "./transactions.container";
import { TransactionsV2Container } from "./transactions-v2.container";
import { RechargeSuccessfulContainer } from "./recharge-successful.container";
import { WalletOffersContainer } from "./offers.container";

export const walletContainers = [
    BalanceContainer,
    WalletPageContainer,
    BanksSearchContainer,
    TransactionsContainer,
    TransactionsV2Container,
    RechargeSuccessfulContainer,
    WalletOffersContainer,
];
