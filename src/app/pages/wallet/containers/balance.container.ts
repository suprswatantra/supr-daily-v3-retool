import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { Observable } from "rxjs";

import { City } from "@shared/models/address.model";
import { WalletExpiryDetails } from "@models";
import { WalletCurrentState } from "@store/wallet/wallet.state";

import { WalletPageAdapter as Adapter } from "@pages/wallet/services/wallet.adapter";

@Component({
    selector: "supr-wallet-balance-container",
    template: `
        <supr-wallet-balance
            [balance]="balance$ | async"
            [suprCreditsEnabled]="suprCreditsEnabled$ | async"
            [suprCreditsBalance]="suprCreditsBalance$ | async"
            [suprCreditsExpiry]="suprCreditsExpiry$ | async"
            [city]="city$ | async"
            [walletState]="walletState$ | async"
            [type]="type"
        >
        </supr-wallet-balance>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BalanceContainer implements OnInit {
    @Input() type = "custom";

    balance$: Observable<number>;
    suprCreditsEnabled$: Observable<boolean>;
    suprCreditsBalance$: Observable<number>;
    walletState$: Observable<WalletCurrentState>;
    city$: Observable<City>;
    suprCreditsExpiry$: Observable<WalletExpiryDetails>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.balance$ = this.adapter.balance$;
        this.suprCreditsEnabled$ = this.adapter.isSuprCreditsEnabled$;
        this.suprCreditsBalance$ = this.adapter.suprCreditsBalance$;
        this.walletState$ = this.adapter.walletState$;
        this.city$ = this.adapter.city$;
        this.suprCreditsExpiry$ = this.adapter.suprCreditsExpiry$;
    }
}
