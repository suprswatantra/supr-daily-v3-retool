import { Component, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { Observable, of } from "rxjs";
import { map, filter } from "rxjs/operators";

import { MONEY_OPTIONS } from "@constants";
import {
    User,
    WalletMeta,
    PaymentInfo,
    CheckoutInfo,
    RechargeCouponInfo,
} from "@models";

import { UtilService } from "@services/util/util.service";
import { WalletCurrentState } from "@store/wallet/wallet.state";

import {
    PaymentMethodComponent,
    PaymentMethodV2Component,
    SuprApi,
    SelectPaymentMethodV2,
} from "@types";

import { WalletPageAdapter as Adapter } from "@pages/wallet/services/wallet.adapter";

@Component({
    selector: "supr-wallet-container",
    template: `
        <supr-wallet-layout
            [amountToFill]="amountToFill$ | async"
            [user]="user$ | async"
            [options]="moneyOptions"
            [defaultAmount]="moneyOptions[1]"
            [walletMeta]="walletMeta$ | async"
            [walletState]="walletState$ | async"
            [walletError]="walletError$ | async"
            [autoCheckout]="autoCheckout$ | async"
            [paymentMethods]="paymentMethods$ | async"
            [paymentMethodsV2]="paymentMethodsV2$ | async"
            [fromSuprPassPage]="fromSuprPassPage$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            [isWalletVersionV2]="isWalletVersionV2$ | async"
            [loadingInitialData]="loadingInitialData$ | async"
            [suprCreditsEnabled]="suprCreditsEnabled$ | async"
            [paymentMethodsVersion]="paymentMethodsVersion$ | async"
            [selectedPaymentMethod]="selectedPaymentMethod$ | async"
            [selectedPaymentMethodV2]="selectedPaymentMethodV2$ | async"
            [usingCartPaymentCouponCode]="usingCartPaymentCouponCode"
            [cartPaymentCouponCodeInfo]="cartPaymentCouponCodeInfo$ | async"
            [hasFetchedFilteredMethods]="hasFetchedFilteredMethods$ | async"
            (clearRechargeCoupon)="clearRechargeCoupon()"
            (handleCreatePayment)="createPayment($event)"
            (handleFetchInitialData)="fetchInitialData($event)"
            (handleCustomPaymentCapture)="captureCustomPayment($event)"
        >
        </supr-wallet-layout>
    `,
})
export class WalletPageContainer implements OnInit {
    user$: Observable<User>;
    moneyOptions = MONEY_OPTIONS;
    walletError$: Observable<any>;
    loadingInitialData$ = of(true);
    amountToFill$: Observable<number>;
    autoCheckout$: Observable<boolean>;
    walletMeta$: Observable<WalletMeta>;
    fromSuprPassPage$: Observable<boolean>;
    isWalletVersionV2$: Observable<boolean>;
    suprCreditsEnabled$: Observable<boolean>;
    paymentMethodsVersion$: Observable<number>;
    selectedPaymentMethod$: Observable<string>;
    userCheckoutInfo$: Observable<CheckoutInfo>;
    walletState$: Observable<WalletCurrentState>;
    paymentMethods$: Observable<PaymentMethodComponent[]>;
    paymentMethodsV2$: Observable<PaymentMethodV2Component[]>;
    selectedPaymentMethodV2$: Observable<SelectPaymentMethodV2>;
    cartPaymentCouponCodeInfo$: Observable<RechargeCouponInfo>;
    hasFetchedFilteredMethods$: Observable<boolean>;

    constructor(private adapter: Adapter, private utilService: UtilService) {}

    usingCartPaymentCouponCode = false;
    cartPaymentCouponCode;

    ngOnInit() {
        this.user$ = this.adapter.user$;
        this.walletState$ = this.adapter.walletState$;
        this.walletError$ = this.adapter.walletError$;
        this.walletMeta$ = this.adapter.walletMeta$;
        this.amountToFill$ = this.adapter.urlParams$.pipe(
            map((params: Params) => +params["amount"]),
            filter((amount) => !isNaN(amount) && amount > 0)
        );
        this.autoCheckout$ = this.adapter.urlParams$.pipe(
            map((params: Params) => !!params["auto"])
        );
        this.fromSuprPassPage$ = this.adapter.urlParams$.pipe(
            map((params: Params) => !!params["fromSuprPassPage"])
        );
        this.suprCreditsEnabled$ = this.adapter.isSuprCreditsEnabled$;
        this.isWalletVersionV2$ = this.adapter.isWalletVersionV2$;
        this.userCheckoutInfo$ = this.adapter.userCheckoutInfo$;
        this.loadingInitialData$ = this.adapter.walletState$.pipe(
            map((state: WalletCurrentState) => {
                return state === WalletCurrentState.LOADING_INITIAL_DATA;
            })
        );
        this.paymentMethods$ = this.adapter.paymentMethods$;
        this.paymentMethodsV2$ = this.adapter.paymentMethodsV2$;
        this.paymentMethodsVersion$ = this.adapter.paymentMethodsVersion$;
        this.selectedPaymentMethod$ = this.adapter.selectedPaymentMethod$;
        this.selectedPaymentMethodV2$ = this.adapter.selectedPaymentMethodV2$;
        this.cartPaymentCouponCodeInfo$ = this.adapter.cartPaymentCouponCodeInfo$;
        this.hasFetchedFilteredMethods$ = this.adapter.hasFetchedFilteredMethods$;
        this.setRechargeCouponFromUrl();
    }

    createPayment(data: PaymentInfo) {
        this.adapter.createPayment(data);
    }

    captureCustomPayment(data: SuprApi.CustomPaymentCaptureReq) {
        this.adapter.captureCustomPayment(data);
    }

    fetchInitialData(usingCartPaymentCouponCode = false) {
        this.adapter.fetchInitialData(usingCartPaymentCouponCode);
    }

    applyRechargeCoupon(data: { couponCode: string; amount: number }) {
        this.adapter.applyRechargeCoupon(data);
    }

    fetchRechargeCouponOffers() {
        this.adapter.fetchRechargeCouponOffers();
    }

    clearRechargeCoupon() {
        this.adapter.clearRechargeCoupon();
    }

    private setRechargeCouponFromUrl() {
        this.adapter.urlParams$.pipe().subscribe((params: Params) => {
            if (this.utilService.isEmpty(params)) {
                return;
            }

            const amount = +params["amount"];
            const rechargeCouponCode = params["rechargeCoupon"];
            const cartPaymentCoupon = params["cartPaymentCoupon"];
            if (!isNaN(amount) && amount > 0 && rechargeCouponCode) {
                this.applyRechargeCoupon({
                    amount,
                    couponCode: rechargeCouponCode,
                });
            }
            if (cartPaymentCoupon) {
                this.usingCartPaymentCouponCode = true;
                this.cartPaymentCouponCode = cartPaymentCoupon;
            }
        });
    }
}
