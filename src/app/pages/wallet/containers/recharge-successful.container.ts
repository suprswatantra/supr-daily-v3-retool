import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { WalletPageAdapter as Adapter } from "@pages/wallet/services/wallet.adapter";
import { WalletMeta } from "@models";

@Component({
    selector: "supr-recharge-successful-container",
    template: `
        <supr-recharge-successful
            [walletMeta]="walletMeta$ | async"
            (clearWalletMeta)="clearWalletMeta()"
        >
        </supr-recharge-successful>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RechargeSuccessfulContainer implements OnInit {
    walletMeta$: Observable<WalletMeta>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.walletMeta$ = this.adapter.walletMeta$;
    }

    clearWalletMeta() {
        this.adapter.clearWalletMeta();
    }
}
