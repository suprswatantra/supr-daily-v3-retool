import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { WalletRoutingModule } from "./wallet-routing.module";

import { walletPageServices } from "./services";

import { walletComponents } from "./components";
import { walletContainers } from "./containers";
import { walletLayouts } from "./layouts";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        WalletRoutingModule,
    ],
    declarations: [...walletComponents, ...walletContainers, ...walletLayouts],
    providers: [...walletPageServices],
})
export class WalletPageModule {}
