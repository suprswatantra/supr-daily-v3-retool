import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-wallet-payment-mode-tile",
    templateUrl: "./payment-mode-tile.component.html",
    styleUrls: ["./payment-mode-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModeTileComponent {
    @Input() icon: string;
    @Input() title: string;
    @Input() subtitle: string;
    @Input() imageSrc: string;
    @Input() subtitleIcon: string;
    @Input() useDirectImageUrl = false;
}
