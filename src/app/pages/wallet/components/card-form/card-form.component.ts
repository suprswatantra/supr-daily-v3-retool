import {
    Input,
    OnInit,
    Component,
    ViewChild,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { CARD_TYPES } from "@constants";

import { UtilService } from "@services/util/util.service";

import { WalletNewCardFormMeta } from "@types";

import { InputComponent } from "@shared/components/supr-input/supr-input.component";

import { WalletPageService } from "@pages/wallet/services/wallet.service";
import {
    TEXTS,
    CARD_FORM,
    CARD_SERVICE_DATA,
} from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-card-form",
    templateUrl: "card-form.component.html",
    styleUrls: ["./card-form.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardFormComponent implements OnInit {
    @Input() cardFormMeta: WalletNewCardFormMeta = {};

    @ViewChild("name", { static: true }) nameElem: InputComponent;
    @ViewChild("expiry", { static: true }) expiryElem: InputComponent;
    @ViewChild("cvvNumber", { static: true }) cvvElem: InputComponent;
    @ViewChild("cardNumber", { static: true }) cardNumberElem: InputComponent;

    _saveCard = true;
    _invalidCVV = false;
    _walletTexts = TEXTS;
    _cardForm = CARD_FORM;
    _noExpiryOrCvv = false;
    _iconNames = ICON_NAMES;
    _cardTypes = CARD_TYPES;
    _cardServiceData = CARD_SERVICE_DATA;
    _errorVisibility: { [field: string]: boolean } = {};

    private _formatterObservable: Observable<WalletNewCardFormMeta>;

    constructor(
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private walletPageService: WalletPageService
    ) {}

    ngOnInit() {
        this.setFormattingListener();
        this.walletPageService.resetNewCardFormData();
    }

    validateCVVField(cvv: string) {
        const cvvLength = this.cardFormMeta && this.cardFormMeta.cvvLength;

        this._invalidCVV = cvv && cvv.length !== cvvLength;
    }

    validateCardField(card: string) {
        this._errorVisibility[CARD_FORM.CARD_NUMBER.formField] =
            card && card.length > 0;
    }

    validateExpiryField(expiry: string) {
        this._errorVisibility[CARD_FORM.EXPIRY.formField] =
            expiry && expiry.length > 0;
    }

    validateFormField(field: string) {
        switch (field) {
            case CARD_FORM.CARD_NUMBER.formField: {
                const card = this.getInputValue(
                    CARD_FORM.CARD_NUMBER.formField
                );

                this.validateCardField(card);
                break;
            }
            case CARD_FORM.EXPIRY.formField: {
                const expiry = this.getInputValue(CARD_FORM.EXPIRY.formField);

                this.validateExpiryField(expiry);
                break;
            }
            case CARD_FORM.CVV.formField: {
                const cvv = this.getInputValue(CARD_FORM.CVV.formField);

                this.toggleCVVErrorState(cvv);
                break;
            }
            default:
                break;
        }
    }

    toggleSaveCard() {
        this._saveCard = !this._saveCard;

        this.walletPageService.setNewCardFormData(
            CARD_SERVICE_DATA.SAVE,
            this._saveCard ? 1 : 0
        );
    }

    toggleNoExpiryOrCvv() {
        this._noExpiryOrCvv = !this._noExpiryOrCvv;

        this.toggleCVVErrorState();
        this.walletPageService.setNewCardFormData(
            CARD_SERVICE_DATA.VALIDATE_EXPIRY_CVV,
            !this._noExpiryOrCvv
        );
    }

    handleInputChange(value: string, field: string) {
        /* update cvv value by validating it against max length */
        if (field === CARD_SERVICE_DATA.CVV) {
            this.validateCVVField(value);

            if (
                value === null ||
                value.length !== this.cardFormMeta.cvvLength
            ) {
                this.walletPageService.setNewCardFormData(field, null);
                return;
            }
        }

        this.walletPageService.setNewCardFormData(field, value);
    }

    private toggleCVVErrorState(cvv?: string) {
        const _cvv = cvv || this.getInputValue(CARD_FORM.CVV.formField);

        this._errorVisibility[CARD_FORM.CVV.formField] =
            !this._noExpiryOrCvv && _cvv !== null && _cvv.length > 0;
    }

    private getInputValue(field: string): string {
        let inputElement: any;

        switch (field) {
            case CARD_FORM.CVV.formField: {
                inputElement = this.cvvElem.getInputElement();
                break;
            }
            case CARD_FORM.EXPIRY.formField: {
                inputElement = this.expiryElem.getInputElement();
                break;
            }
            case CARD_FORM.CARD_NUMBER.formField: {
                inputElement = this.cardNumberElem.getInputElement();
                const value = this.utilService.getNestedValue(
                    inputElement,
                    "nativeElement.value"
                );

                return value && value.replace(" ", "");
            }
            case CARD_FORM.NAME.formField: {
                inputElement = this.nameElem.getInputElement();
                break;
            }
            default:
                return null;
        }

        return this.utilService.getNestedValue(
            inputElement,
            "nativeElement.value"
        );
    }

    private setFormattingListener() {
        const cvvInputElem = this.cvvElem.getInputElement();
        const expiryInputElem = this.expiryElem.getInputElement();
        const cardNumberInputElem = this.cardNumberElem.getInputElement();

        const parentForm = document.getElementById("add-card-form");
        const cvvNativeElem = cvvInputElem && cvvInputElem.nativeElement;
        const expiryNativeElem =
            expiryInputElem && expiryInputElem.nativeElement;
        const cardNumberNativeElem =
            cardNumberInputElem && cardNumberInputElem.nativeElement;

        if (
            parentForm &&
            cvvNativeElem &&
            expiryNativeElem &&
            cardNumberNativeElem
        ) {
            this._formatterObservable = this.walletPageService.formatAddNewCardForm(
                parentForm,
                cvvNativeElem,
                expiryNativeElem,
                cardNumberNativeElem
            );

            this._formatterObservable.subscribe(
                (cardFormMeta: WalletNewCardFormMeta) => {
                    this.cardFormMeta = cardFormMeta;

                    this.setServiceData();
                    this.cdr.detectChanges();
                }
            );
        }
    }

    private setServiceData() {
        const isCardValid = this.utilService.getNestedValue(
            this.cardFormMeta,
            "validation.card.valid"
        );
        const isExpiryValid = this.utilService.getNestedValue(
            this.cardFormMeta,
            "validation.expiry.valid"
        );
        const network = this.cardFormMeta && this.cardFormMeta.network;
        const expiry = isExpiryValid
            ? this.getInputValue(CARD_FORM.EXPIRY.formField)
            : null;
        const cardNumber = isCardValid
            ? this.getInputValue(CARD_FORM.CARD_NUMBER.formField)
            : null;

        this.walletPageService.setNewCardFormData(
            CARD_SERVICE_DATA.EXPIRY,
            expiry
        );
        this.walletPageService.setNewCardFormData(
            CARD_SERVICE_DATA.NETWORK,
            network
        );
        this.walletPageService.setNewCardFormData(
            CARD_SERVICE_DATA.CARD,
            cardNumber
        );
    }
}
