import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-wallet-cvv-input",
    template: ` <div class="wrapper">
        <supr-input
            type="password"
            [maxLength]="maxLength"
            placeholder="Enter CVV"
            (handleInputChange)="handleCvvChange.emit($event)"
        ></supr-input>
    </div>`,
    styleUrls: ["./cvv-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CVVInputComponent {
    @Input() maxLength = 4;

    @Output() handleCvvChange: EventEmitter<string> = new EventEmitter();
}
