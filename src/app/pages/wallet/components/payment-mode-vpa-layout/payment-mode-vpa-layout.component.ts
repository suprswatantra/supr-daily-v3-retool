import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import {
    CARD_TYPES,
    PAYMENT_METHODS_V2,
    PAYMENT_METHODS_CUSTOM_OPTIONS,
} from "@constants";

import { PaymentMethodOption } from "@models";

import { SelectPaymentMethodV2 } from "@types";

import { SA_OBJECT_NAMES } from "@pages/wallet/constants/analytics.constants";

import { TEXTS } from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-payment-mode-vpa-layout",
    templateUrl: "./payment-mode-vpa-layout.component.html",
    styleUrls: ["./payment-mode-vpa-layout.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModeVPALayoutComponent implements OnInit {
    @Input() options: PaymentMethodOption[];
    @Input() selectedOption: number | string;
    @Input() hideAddNewOption: boolean;

    @Input() saContext: number;
    @Input() saObjectName: string;

    @Output() handleItemClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    _saveVPA = true;
    _walletTexts = TEXTS;
    _noExpiryOrCvv = false;
    _cardTypes = CARD_TYPES;
    _iconNames = ICON_NAMES;
    _paymentMethods = PAYMENT_METHODS_V2;
    _customPaymentOptions = PAYMENT_METHODS_CUSTOM_OPTIONS;

    _saObjectNames = SA_OBJECT_NAMES;

    constructor(private walletPageService: WalletPageService) {}

    ngOnInit() {
        this._initFormDataInService();
    }

    _trackByFn(_: any, index: number): number {
        return index;
    }

    _handleItemClick(option: PaymentMethodOption) {
        if (option) {
            this.handleItemClick.emit({
                id: option.id,
                paymentInfo: option.payment_info,
            });
        }
    }

    _handleAddVPAClick() {
        this._initFormDataInService();

        this.handleItemClick.emit({
            paymentInfo: null,
            id: PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_VPA,
        });
    }

    _toggleSaveVPA() {
        this._saveVPA = !this._saveVPA;

        this.walletPageService.setNewVpaData("save", this._saveVPA ? 1 : 0);
    }

    private _initFormDataInService() {
        this.walletPageService.setNewVpaData("save", 1);
        this.walletPageService.setNewVpaData("vpa", null);
    }
}
