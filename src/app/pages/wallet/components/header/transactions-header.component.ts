import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-wallet-transactions-header",
    template: `
        <div class="header">
            <supr-wallet-balance-container
                type="title"
            ></supr-wallet-balance-container>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsHeaderComponent {}
