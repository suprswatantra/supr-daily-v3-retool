import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { RouterService } from "@services/util/router.service";
import { SA_OBJECT_NAMES } from "@pages/wallet/constants/analytics.constants";
import { TEXTS } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-header",
    template: `
        <div class="header">
            <supr-wallet-balance-container></supr-wallet-balance-container>

            <ng-container *ngIf="!autoCheckout">
                <div class="divider18"></div>
                <div
                    class="suprRow allTxn"
                    (click)="goToTransactionsPage()"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ALL_TRANSACTIONS}"
                >
                    <supr-text type="caption">
                        {{ ALL_TRANSACTIONS }}
                    </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletHeaderComponent {
    @Input() autoCheckout: boolean;
    @Input() isSuprCreditsEnabled: boolean;

    constructor(private routerService: RouterService) {}

    ALL_TRANSACTIONS = TEXTS.ALL_TRANSACTIONS;

    goToTransactionsPage() {
        if (!this.isSuprCreditsEnabled) {
            this.routerService.goToWalletTransactionsPage();
        } else {
            this.routerService.goToWalletTransactionsV2Page();
        }
    }
}
