import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-wallet-transactions-v2-header",
    template: `
        <div class="header">
            <supr-wallet-balance-container
                type="title"
            ></supr-wallet-balance-container>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsV2HeaderComponent {}
