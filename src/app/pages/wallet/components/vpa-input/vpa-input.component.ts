import {
    Component,
    OnDestroy,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription } from "rxjs";

import { SelectedPaymentInfoValidation } from "@types";

import {
    TEXTS,
    VPA_SERVICE_DATA,
} from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-vpa-input",
    template: ` <div class="wrapper">
        <div class="inputWrapper">
            <supr-input
                type="text"
                placeholder="${TEXTS.VPA_PLACEHOLDER}"
                (handleInputBlur)="_handleInputBlur()"
                (handleInputChange)="_handleInputChange($event)"
            ></supr-input>
            <div class="loaderWrapper" *ngIf="_validating">
                <supr-loader></supr-loader>
            </div>
        </div>
        <ng-container *ngIf="!_isValid && _showError">
            <div class="divider8"></div>
            <supr-text type="body"> ${TEXTS.INVALID_VPA_INPUT} </supr-text>
        </ng-container>
    </div>`,
    styleUrls: ["./vpa-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VPAInputComponent implements OnDestroy {
    _isValid = true;
    _showError = false;
    _validating = false;

    private _validationSubscription: Subscription;

    constructor(
        private cdr: ChangeDetectorRef,
        private walletPageService: WalletPageService
    ) {}

    ngOnDestroy() {
        this._validating = false;
        this.destroyValidationSubscription();
    }

    _handleInputBlur() {
        this._showError = true;
    }

    _handleInputChange(vpa: string) {
        this._validating = true;
        this.destroyValidationSubscription();
        this.walletPageService.setNewVpaData(VPA_SERVICE_DATA.VPA, null);

        this._validationSubscription = this.walletPageService
            .validateNewVpaId(vpa)

            .subscribe((validationInfo: SelectedPaymentInfoValidation) => {
                this._isValid = validationInfo.valid;
                this._showError = this._showError && vpa && vpa.length > 0;

                if (validationInfo.valid) {
                    this.walletPageService.setNewVpaData(
                        VPA_SERVICE_DATA.VPA,
                        vpa
                    );
                } else {
                    this.walletPageService.setNewVpaData(
                        VPA_SERVICE_DATA.VPA,
                        null
                    );
                }

                this._validating = false;
                this.cdr.detectChanges();
            });
    }

    private destroyValidationSubscription() {
        if (
            this._validationSubscription &&
            !this._validationSubscription.closed
        ) {
            this._validationSubscription.unsubscribe();
        }
    }
}
