import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import {
    CARD_TYPES,
    PAYMENT_METHODS_V2,
    PAYMENT_METHODS_CUSTOM_OPTIONS,
} from "@constants";

import { PaymentMethodOption } from "@models";

import { SelectPaymentMethodV2 } from "@types";

import { SA_OBJECT_NAMES } from "@pages/wallet/constants/analytics.constants";

import {
    TEXTS,
    CARD_SERVICE_DATA,
} from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-payment-mode-card-layout",
    templateUrl: "./payment-mode-card-layout.component.html",
    styleUrls: ["./payment-mode-card-layout.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModeCardLayoutComponent implements OnInit {
    @Input() showCVVInput = false;
    @Input() options: PaymentMethodOption[];
    @Input() selectedOption: number | string;

    @Input() saContext: number;
    @Input() saObjectName: string;

    @Output() handleItemClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    _walletTexts = TEXTS;
    _noExpiryOrCvv = false;
    _iconNames = ICON_NAMES;
    _cardTypes = CARD_TYPES;
    _paymentMethods = PAYMENT_METHODS_V2;
    _customOptions = PAYMENT_METHODS_CUSTOM_OPTIONS;

    _saObjectNames = SA_OBJECT_NAMES;

    constructor(private walletPageService: WalletPageService) {}

    ngOnInit() {
        this._initFormDataInService();
    }

    _trackByFn(_: any, index: number): number {
        return index;
    }

    _handleItemClick(option: PaymentMethodOption) {
        this.showCVVInput = true;
        this._initFormDataInService();

        if (option) {
            this.handleItemClick.emit({
                id: option.id,
                paymentInfo: option.payment_info,
            });
        }
    }

    _handleAddCardClick() {
        this.handleItemClick.emit({
            paymentInfo: null,
            id: PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_CARD,
        });
    }

    _getCvvMaxLength(option: PaymentMethodOption): number {
        return this.walletPageService.getCvvLengthBasedOnNetwork(
            (option && option.network) as CARD_TYPES
        );
    }

    _handleCvvChange(cvv: string, option: PaymentMethodOption) {
        this.walletPageService.handleFormCVVChange(cvv, option);
    }

    _toggleNoExpiryOrCvv() {
        this._noExpiryOrCvv = !this._noExpiryOrCvv;

        this.walletPageService.setSavedCardFormData(
            CARD_SERVICE_DATA.VALIDATE_EXPIRY_CVV,
            !this._noExpiryOrCvv
        );
    }

    private _initFormDataInService() {
        this.walletPageService.resetSavedCardFormData();
    }
}
