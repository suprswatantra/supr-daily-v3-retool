import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { PAYMENT_METHODS_V2 } from "@constants";

import {
    SelectPaymentMethodV2,
    PaymentMethodComponent,
    PaymentMethodV2Component,
    PaymentModeComponentTypes,
} from "@types";

import { UtilService } from "@services/util/util.service";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-payment-mode-wrapper",
    templateUrl: "./payment-mode-wrapper.component.html",
    styleUrls: ["./payment-mode-wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModeWrapperComponent implements OnInit {
    @Input() group: string;
    @Input() amount: number;
    @Input() version: number;
    @Input() expanded = true;
    @Input() lastGroup = false;
    @Input() set selectedMethod(method: string | number) {
        this._selectedMethod = method;
        this.setMethodData();
    }
    @Input() set methodData(
        data: PaymentMethodComponent | PaymentMethodV2Component
    ) {
        this.setMethodData(data);
    }

    @Output() updateSelectedMode: EventEmitter<
        string | number
    > = new EventEmitter();
    @Output() updateSelectedModeV2: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();
    @Output() handleNetbankingLayoutCtaClick: EventEmitter<
        void
    > = new EventEmitter();

    _settingsConfig: any;
    _showCardLayoutCVV = false;
    _selectedMethod: string | number;
    _componentTypes = PaymentModeComponentTypes;
    _methodData: PaymentMethodComponent | PaymentMethodV2Component;

    constructor(
        private utilService: UtilService,
        private walletService: WalletPageService
    ) {}

    ngOnInit() {
        this._settingsConfig = this.walletService.getPaymentWrapperComponentSettings(
            this.group
        );
    }

    setMethodData(data?: PaymentMethodComponent | PaymentMethodV2Component) {
        this._methodData = this.walletService.getWalletMethodData(
            data || this._methodData,
            this._selectedMethod,
            this.version
        );

        this._showCardLayoutCVV = this.walletService.isGroupOptionSelected(
            this._methodData as PaymentMethodV2Component,
            this._selectedMethod as number,
            this.version
        );
    }

    toggleGroup() {
        this.expanded = !this.expanded;
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    handleTileLayoutCtaClick(group: string) {
        switch (group) {
            case PAYMENT_METHODS_V2.NETBANKING:
                this.handleNetbankingLayoutCtaClick.emit();
        }
    }

    _updateSelectedModeV2(data: SelectPaymentMethodV2, method: string) {
        if (!this.utilService.isEmpty(data)) {
            /* preferred layout comes with method info as methods are defined at an option
            level rather than a group level */

            const _method = data.method || method;

            this.updateSelectedModeV2.emit({ ...data, method: _method });
        }
    }
}
