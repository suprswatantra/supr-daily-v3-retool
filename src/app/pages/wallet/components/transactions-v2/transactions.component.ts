import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { Transaction, WalletExpiryDetails } from "@models";

import {
    TRANSACTION_V2_FILTERS,
    TRANSACTION_TYPE_V2,
} from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-transactions-v2",
    template: `
        <div class="transactions">
            <div class="loader suprColumn center" *ngIf="loading; else txnList">
                <supr-loader></supr-loader>
            </div>

            <ng-template #txnList>
                <supr-wallet-transaction-credits-expiry
                    [walletExpiryDetails]="walletExpiryDetails"
                ></supr-wallet-transaction-credits-expiry>

                <supr-wallet-transaction-v2-filters
                    [activeTab]="activeTab"
                    (handleTabClick)="onTabClick($event)"
                ></supr-wallet-transaction-v2-filters>
                <ng-container
                    *ngIf="
                        filteredTxns &&
                            filteredTxns[activeTab] &&
                            filteredTxns[activeTab].length;
                        else noTxns
                    "
                >
                    <div class="divider16"></div>
                    <supr-wallet-transaction-v2-list
                        [transactions]="filteredTxns[activeTab]"
                        [activeTab]="activeTab"
                    ></supr-wallet-transaction-v2-list>
                </ng-container>
            </ng-template>

            <ng-template #noTxns>
                <supr-wallet-empty-transactions-v2
                    [view]="activeTab"
                ></supr-wallet-empty-transactions-v2>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsV2Component implements OnChanges {
    @Input() transactions: Transaction[];
    @Input() walletExpiryDetails: WalletExpiryDetails;

    loading = true;
    activeTab = TRANSACTION_V2_FILTERS[0].key;
    filteredTxns: { [key: string]: Transaction[] } = {};

    constructor(private walletService: WalletPageService) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["transactions"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue) {
            this.loading = false;
            this.setFilteredList(this.activeTab);
        }
    }

    onTabClick(selectedTab: string) {
        this.activeTab = selectedTab;
        this.setFilteredList(selectedTab);
    }

    private setFilteredList(tab: string) {
        if (this.filteredTxns[tab]) {
            return;
        }

        this.filteredTxns[tab] = this.walletService.getFilteredTransactionsV2(
            this.transactions || [],
            tab === TRANSACTION_TYPE_V2.SUPR_CREDITS
        );
    }
}
