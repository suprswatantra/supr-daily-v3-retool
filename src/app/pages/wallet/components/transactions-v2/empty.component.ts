import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import {
    TEXTS,
    TRANSACTION_TYPE_V2,
} from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-empty-transactions-v2",
    template: `
        <div class="empty suprColumn center">
            <supr-svg [ngClass]="view"></supr-svg>
            <div class="divider24"></div>

            <ng-container *ngIf="view === '${TRANSACTION_TYPE_V2.WALLET}'">
                <supr-text type="heading">
                    ${TEXTS.NO_WALLET_TRANSACTIONS_TITLE}
                </supr-text>
                <div class="divider8"></div>

                <supr-text type="caption" class="subText">
                    ${TEXTS.NO_WALLET_TRANSACTIONS_SUBTITLE}
                </supr-text>
            </ng-container>

            <ng-container
                *ngIf="view === '${TRANSACTION_TYPE_V2.SUPR_CREDITS}'"
            >
                <supr-text class="SuprCreditsTitle" type="heading">
                    ${TEXTS.NO_SUPR_CREDIT_TRANSACTIONS_TITLE}
                </supr-text>
                <div class="divider8"></div>

                <supr-text type="caption" class="subText">
                    ${TEXTS.NO_SUPR_CREDIT_TRANSACTIONS_SUBTITLE}
                </supr-text>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyTransactionsV2Component {
    @Input() view: string;
}
