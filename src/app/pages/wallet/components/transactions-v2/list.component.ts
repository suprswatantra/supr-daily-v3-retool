import { Component, Input, ChangeDetectionStrategy } from "@angular/core";
import { Transaction } from "@models";

import { TRANSACTION_TYPE_V2 } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-v2-list",
    template: `
        <ng-container
            *ngFor="let transaction of transactions; trackBy: trackByFn"
        >
            <supr-wallet-transaction-v2-wrapper
                [transaction]="transaction"
                *ngIf="activeTab !== suprCreditsTabKey"
            >
            </supr-wallet-transaction-v2-wrapper>
            <supr-wallet-transaction-supr-credits
                [transaction]="transaction"
                *ngIf="activeTab === suprCreditsTabKey"
            ></supr-wallet-transaction-supr-credits>
        </ng-container>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsV2ListComponent {
    @Input() transactions: Transaction[];
    @Input() activeTab: string;

    suprCreditsTabKey = TRANSACTION_TYPE_V2.SUPR_CREDITS;

    trackByFn(transaction: Transaction): number {
        return transaction.id;
    }
}
