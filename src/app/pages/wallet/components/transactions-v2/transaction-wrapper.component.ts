import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { Transaction } from "@models";

import { TRANSACTION_TYPE } from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-v2-wrapper",
    template: `
        <supr-wallet-transaction-v2-payment
            [transaction]="transaction"
            *ngIf="transaction.obj_type === transactionTypes.PAYMENT"
        >
        </supr-wallet-transaction-v2-payment>

        <supr-wallet-transaction-v2-recharge
            [transaction]="transaction"
            *ngIf="transaction.obj_type === transactionTypes.RECHARGE"
        >
        </supr-wallet-transaction-v2-recharge>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionV2WrapperComponent {
    @Input() transaction: Transaction;

    readonly transactionTypes = TRANSACTION_TYPE;
}
