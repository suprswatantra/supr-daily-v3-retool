import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    TRANSACTION_V2_FILTERS,
    TRANSACTION_TYPE_V2,
} from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-v2-filters",
    template: `
        <div class="suprRow filters spaceBetween">
            <div class="filtersItem" *ngFor="let filter of filters">
                <supr-button
                    [class.scTab]="filter.key === scTabKey"
                    [class.active]="filter.key === activeTab"
                    (handleClick)="handleTabClick.emit(filter.key)"
                >
                    <supr-text type="body">
                        {{ filter.label }}
                    </supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionV2FiltersComponent {
    @Input() activeTab: string;
    @Output() handleTabClick: EventEmitter<string> = new EventEmitter();

    filters = TRANSACTION_V2_FILTERS;
    scTabKey = TRANSACTION_TYPE_V2.SUPR_CREDITS;
}
