import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { WalletExpiryDetails } from "@models";
import { EXPIRY_TEXT } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-credits-expiry",
    template: `
        <div
            class="creditsExpiryWrapper"
            *ngIf="walletExpiryDetails?.supr_credits"
        >
            <div class="divider16"></div>
            <div class="suprRow creditsExpiry">
                <supr-svg class="expiring"></supr-svg>
                <div class="spacer8"></div>
                <supr-text type="caption">
                    {{ walletExpiryDetails.supr_credits.amount }} ${EXPIRY_TEXT}
                    {{
                        walletExpiryDetails.supr_credits.date
                            | date: "EEEE, MMM d, y"
                    }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionV2CreditsExpiryComponent {
    @Input() walletExpiryDetails: WalletExpiryDetails;
}
