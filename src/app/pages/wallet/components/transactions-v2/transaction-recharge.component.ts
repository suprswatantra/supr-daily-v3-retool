import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { RechargeTransaction } from "@models";

import {
    RECHARGE_TRANSACTION_TYPE,
    RECHARGE_TRANSACTION_DISPLAY_NAME,
    RECHARGE_TYPE_DISPLAY_NAME,
    TRANSACTION_TEXTS,
} from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-v2-recharge",
    template: `
        <div class="transaction">
            <div class="suprRow top">
                <div class="suprColumn left">
                    <supr-text type="body">
                        {{ rechargeString }}
                    </supr-text>
                </div>
                <div class="suprColumn right">
                    <supr-text
                        type="body"
                        class="recharge last"
                        [class.cashback]="isPromo"
                    >
                        {{ icon }} {{ transaction.paid_amount | rupee }}
                    </supr-text>
                </div>
            </div>
            <div class="divider4"></div>

            <div class="suprRow">
                <div class="suprColumn left" *ngIf="transaction.gateway_id">
                    <supr-text type="caption" class="subtext">
                        ${TRANSACTION_TEXTS.PAYMENT_ID}
                        {{ transaction.gateway_id }}
                    </supr-text>
                </div>
                <div class="suprColumn right" *ngIf="transaction.balance">
                    <supr-text type="caption" class="light">
                        ${TRANSACTION_TEXTS.UPDATED_BALANCE}
                        {{ transaction?.balance?.wallet | rupee }}
                    </supr-text>
                </div>
            </div>
            <div class="divider4"></div>

            <div class="suprRow">
                <supr-text type="caption" class="subtext">
                    {{ transaction?.created | date: "EEEE, MMM d, y" }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionV2RechargeComponent {
    @Input()
    set transaction(txn: RechargeTransaction) {
        this.init(txn);
    }

    rechargeTransactionDisplayName = RECHARGE_TRANSACTION_DISPLAY_NAME;

    get transaction(): RechargeTransaction {
        return this._transaction;
    }

    get isPromo(): boolean {
        return this._isPromo;
    }

    icon = "+";
    rechargeString = "";
    rechargeTypeString = "";

    private _isPromo = false;
    private _transaction: RechargeTransaction;

    private init(txn: RechargeTransaction) {
        this._transaction = txn;
        this._isPromo = txn.type === RECHARGE_TRANSACTION_TYPE.PROMO;
        this.icon = txn.paid_amount > 0 ? "+" : txn.paid_amount < 0 ? "-" : "";
        this.rechargeString = this._isPromo
            ? "Promo"
            : txn.paid_amount > 0
            ? "Wallet refill"
            : "";
        this.rechargeTypeString =
            RECHARGE_TYPE_DISPLAY_NAME[txn.type_details] || txn.type_details;
    }
}
