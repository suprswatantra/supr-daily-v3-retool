import { QuantityService } from "../../../../services/util/quantity.service";
import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { PaymentTransaction } from "@models";

import {
    PAYMENT_TRANSACTION_PAYMENT_TYPE,
    TRANSACTION_TYPE,
    TRANSACTION_TEXTS,
    DURATION,
} from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-v2-payment",
    template: `
        <div class="transaction">
            <div class="suprRow top">
                <div class="suprColumn left">
                    <supr-text
                        type="body"
                        *ngIf="
                            transaction.obj_type === transactionTypes.PAYMENT
                        "
                    >
                        {{ itemName }}
                    </supr-text>
                </div>

                <div class="suprColumn right">
                    <supr-text
                        type="body"
                        class="amountChange last"
                        [class.refund]="isRefund"
                    >
                        {{ icon }} {{ paidFromWallet | rupee }}
                    </supr-text>
                    <div class="tag" *ngIf="isRefund">
                        <supr-text type="paragraph" class="refund last">
                            ${TRANSACTION_TEXTS.REFUND}
                        </supr-text>
                    </div>
                </div>
            </div>

            <div class="divider8"></div>

            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="caption" class="light">
                        {{ qtyStr }}
                    </supr-text>
                </div>

                <div class="suprColumn right" *ngIf="transaction?.balance">
                    <supr-text type="caption" class="light">
                        ${TRANSACTION_TEXTS.UPDATED_BALANCE}
                        {{ transaction?.balance?.wallet | rupee }}
                    </supr-text>
                </div>
            </div>

            <div class="divider4"></div>

            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="caption" class="subtext">
                        {{ transaction?.created | date: "EEEE, MMM d, y" }}
                    </supr-text>
                </div>

                <div
                    class="suprColumn right"
                    *ngIf="transaction?.supr_credits_type"
                >
                    <supr-icon
                        name="chevron_down"
                        class="icon"
                        *ngIf="!showPaymentDetails"
                        (click)="togglePaymentDetails()"
                    ></supr-icon>
                    <supr-icon
                        name="chevron_up"
                        class="icon"
                        *ngIf="showPaymentDetails"
                        (click)="togglePaymentDetails()"
                    ></supr-icon>
                </div>
            </div>

            <ng-container *ngIf="showPaymentDetails">
                <div class="divider16"></div>
                <div class="breakup">
                    <div class="divider16"></div>
                    <div class="suprRow">
                        <div class="suprColumn left">
                            <supr-text type="caption" class="subtext">
                                ${TRANSACTION_TEXTS.PAID_AFTER_DISCOUNT}
                            </supr-text>
                        </div>

                        <div class="suprColumn right">
                            <supr-text type="caption" class="dark">
                                {{ transaction?.paid_amount | rupee }}
                            </supr-text>
                        </div>
                    </div>

                    <div class="divider8"></div>

                    <div class="suprRow">
                        <div class="suprColumn left">
                            <supr-text type="caption" class="subtext">
                                ${TRANSACTION_TEXTS.SUPR_CREDITS}
                                {{ transaction?.supr_credits_type }}
                            </supr-text>
                        </div>

                        <div class="suprColumn right">
                            <supr-text type="caption" class="dark">
                                {{
                                    transaction?.payment_details?.supr_credits
                                        | rupee
                                }}
                            </supr-text>
                        </div>
                    </div>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionV2PaymentComponent {
    @Input()
    set transaction(txn: PaymentTransaction) {
        this._transaction = txn;
        this.initData();
    }

    get transaction(): PaymentTransaction {
        return this._transaction;
    }

    get isRefund(): boolean {
        return this._isRefund;
    }

    icon = "+";
    qtyStr: string;
    showPaymentDetails: boolean;
    readonly transactionTypes = TRANSACTION_TYPE;
    paidFromWallet: number;
    itemName: string;

    private _isRefund = false;
    private _transaction: PaymentTransaction;

    constructor(private quantityService: QuantityService) {}

    togglePaymentDetails() {
        this.showPaymentDetails = !this.showPaymentDetails;
    }

    private initData() {
        const txn = this.transaction;
        this._isRefund =
            txn.payment_type === PAYMENT_TRANSACTION_PAYMENT_TYPE.REFUND;
        this.setPaidFromWallet();
        this.setQtyStr();

        if (this.paidFromWallet) {
            this.icon = this._isRefund ? "+" : "-";
        } else {
            this.icon = "";
        }

        if (this.transaction.obj_type === this.transactionTypes.PAYMENT) {
            if (
                this.transaction.type ===
                this.transactionTypes.SUPR_CREDITS_PURCHASE
            ) {
                this.itemName = TRANSACTION_TEXTS.BOUGHT_SC;
            } else {
                if (this.transaction.sku) {
                    this.itemName = this.transaction.sku.sku_name;
                }
            }
        }
    }

    private setQtyStr() {
        if (!this.transaction) {
            return;
        }

        const { sku, quantity } = this.transaction;

        if (this.transaction.type === this.transactionTypes.BOUGHT_SUPR_PASS) {
            const months = quantity / DURATION.DAYS_IN_MONTH;
            const numOfMonths = Math.floor(months);

            if (numOfMonths >= 1) {
                this.qtyStr =
                    numOfMonths +
                    " " +
                    DURATION.MONTH_TEXT +
                    (numOfMonths > 1 ? DURATION.PLURAL_CHARACTER : "");
            }
            return;
        }

        this.qtyStr = this.quantityService.toTextTxn(sku, quantity);
    }

    private setPaidFromWallet() {
        if (!this.transaction) {
            return;
        }

        if (this.transaction.payment_details) {
            this.paidFromWallet = this.transaction.payment_details.wallet;
            return;
        }

        this.paidFromWallet = this.transaction.paid_amount;
    }
}
