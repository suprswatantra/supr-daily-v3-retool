import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { PaymentTransaction, RechargeTransaction } from "@models";

import {
    SUPR_CREDITS_TRANSACTION_TYPE,
    TRANSACTION_TEXTS,
    TRANSACTION_TYPE,
    SC_GAIN_ARRAY,
} from "../../constants/wallet.constants";

import { QuantityService } from "@services/util/quantity.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-wallet-transaction-supr-credits",
    template: `
        <div class="transaction scTxn">
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="body" class="capitalize">
                        {{ transaction.supr_credits_type | lowercase }}
                    </supr-text>
                </div>
                <div class="suprColumn right">
                    <div class="suprRow">
                        <supr-text
                            type="body"
                            class="scAmount"
                            [class.isGain]="isGain"
                        >
                            {{ icon }} {{ suprCreditsToDisplay }}
                        </supr-text>
                    </div>
                </div>
            </div>
            <div class="divider4"></div>

            <ng-container
                *ngIf="
                    transaction.obj_type === transactionTypes.PAYMENT;
                    else rechargeType
                "
            >
                <div class="suprRow">
                    <div class="suprColumn left">
                        <supr-text type="caption" class="subtext">
                            {{ transaction?.sku?.sku_name }}
                        </supr-text>
                    </div>
                </div>
            </ng-container>
            <ng-template #rechargeType>
                <div class="suprRow">
                    <div class="suprColumn left">
                        {{ transaction?.benefit_uuid }}
                    </div>
                </div>
            </ng-template>

            <div class="divider16"></div>
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="caption" class="light">
                        {{ qtyStr }}
                    </supr-text>
                </div>
            </div>
            <div class="divider4"></div>
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-text type="caption" class="subtext">
                        {{ transaction?.created | date: "EEEE, MMM d, y" }}
                    </supr-text>
                </div>
                <div class="suprColumn right">
                    <supr-text
                        type="caption"
                        class="subtext"
                        *ngIf="transaction.balance"
                    >
                        ${TRANSACTION_TEXTS.UPDATED_BALANCE}
                        {{ transaction?.balance?.supr_credits }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions-v2.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionSuprCreditsComponent {
    @Input()
    set transaction(txn: Partial<PaymentTransaction & RechargeTransaction>) {
        this.init(txn);
    }

    suprCreditsTxnType = SUPR_CREDITS_TRANSACTION_TYPE;
    transactionTypes = TRANSACTION_TYPE;
    icon = "+";
    qtyStr: string;
    isGain: boolean;

    get transaction(): Partial<PaymentTransaction & RechargeTransaction> {
        return this._transaction;
    }

    suprCreditsToDisplay = 0;

    private _transaction: Partial<PaymentTransaction & RechargeTransaction>;

    constructor(
        private quantityService: QuantityService,
        private utilService: UtilService
    ) {}

    private init(txn: Partial<PaymentTransaction & RechargeTransaction>) {
        this._transaction = txn;

        if (txn.obj_type === TRANSACTION_TYPE.RECHARGE) {
            this.setDisplayInfoForRecharge(txn);
        }

        if (txn.obj_type === TRANSACTION_TYPE.PAYMENT) {
            this.setDisplayInfoForPayment(txn);
        }

        this.setQtyStr();
    }

    private setDisplayInfoForRecharge(
        txn: Partial<PaymentTransaction & RechargeTransaction>
    ) {
        this.suprCreditsToDisplay = Math.abs(txn.paid_amount || 0);

        const gainIndex = SC_GAIN_ARRAY.indexOf(
            txn.supr_credits_type.toUpperCase()
        );
        this.icon = gainIndex > -1 ? "+" : "-";
        this.isGain = gainIndex > -1;
    }

    private setDisplayInfoForPayment(
        txn: Partial<PaymentTransaction & RechargeTransaction>
    ) {
        this.suprCreditsToDisplay = this.utilService.getNestedValue(
            txn,
            "payment_details.supr_credits",
            0
        );

        if (!txn.supr_credits_type) {
            return;
        }

        const gainIndex = SC_GAIN_ARRAY.indexOf(
            txn.supr_credits_type.toUpperCase()
        );
        this.icon = gainIndex > -1 ? "+" : "-";
        this.isGain = gainIndex > -1;
    }

    private setQtyStr() {
        if (!this.transaction) {
            return;
        }

        const { sku, quantity } = this.transaction;
        this.qtyStr = this.quantityService.toTextTxn(sku, quantity);
    }
}
