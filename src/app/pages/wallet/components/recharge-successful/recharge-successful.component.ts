import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    EventEmitter,
    Output,
} from "@angular/core";

import { AUDIO_KEYS } from "@constants";
import { WalletMeta } from "@models";

import {
    RECHARGE_SUCCESSFUL_TEXTS,
    RECHARGE_SUCCESSFUL_DISPLAY_TIME,
} from "@pages/wallet/constants/wallet.constants";

import { AudioService } from "@services/util/audio.service";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-recharge-successful",
    template: `
        <div class="rechargeSuccessfulContainer suprContainer">
            <div class="rechargeSuccessful suprColumn center">
                <supr-svg class="approved"></supr-svg>
                <div class="divider8"></div>
                <supr-text type="subheading" class="successText">
                    ${RECHARGE_SUCCESSFUL_TEXTS.RECHARGE_SUCCESSFUL}
                </supr-text>

                <div class="divider24"></div>
                <div class="balanceContainer" *ngIf="showBalanceBlock">
                    <div
                        class="suprRow wallet"
                        *ngIf="
                            walletMeta?.rechargeSuccessInfo?.amountAddedToWallet
                        "
                    >
                        <div class="suprColumn left center">
                            <div class="suprRow">
                                <supr-svg class="walletIcon"></supr-svg>
                                <div class="spacer8"></div>
                                <supr-text type="action14">
                                    ${RECHARGE_SUCCESSFUL_TEXTS.WALLET_BALANCE}
                                </supr-text>
                            </div>
                        </div>

                        <div class="suprColumn right center">
                            <supr-text type="bold16">
                                +
                                {{
                                    walletMeta?.rechargeSuccessInfo
                                        ?.amountAddedToWallet | rupee
                                }}
                            </supr-text>
                        </div>
                    </div>

                    <div
                        class="suprRow suprCredits"
                        *ngIf="
                            walletMeta?.rechargeSuccessInfo
                                ?.amountAddedToSuprCredits
                        "
                    >
                        <div class="suprColumn left center">
                            <div class="suprRow">
                                <supr-svg class="scIcon"></supr-svg>
                                <div class="spacer8"></div>
                                <supr-text type="action14">
                                    ${RECHARGE_SUCCESSFUL_TEXTS.SUPR_CREDITS_BALANCE}
                                </supr-text>
                            </div>
                        </div>

                        <div class="suprColumn right center">
                            <supr-text type="bold16">
                                +
                                {{
                                    walletMeta?.rechargeSuccessInfo
                                        ?.amountAddedToSuprCredits
                                }}
                            </supr-text>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/recharge-successful.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RechargeSuccessfulComponent implements OnInit {
    @Input() walletMeta: WalletMeta;

    @Output() clearWalletMeta: EventEmitter<void> = new EventEmitter();

    showBalanceBlock: boolean;

    constructor(
        private audioService: AudioService,
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.audioService.play(AUDIO_KEYS.PAYMENT_DELIVERY_CONFIRMATION_AUDIO);
        this.initialize();
        this.clearWalletMetaAndRouteToHomePage();
    }

    private initialize() {
        const hasWalletBal = !!this.utilService.getNestedValue(
            this.walletMeta,
            "rechargeSuccessInfo.amountAddedToWallet"
        );
        const hasScBal = !!this.utilService.getNestedValue(
            this.walletMeta,
            "rechargeSuccessInfo.amountAddedToSuprCredits"
        );

        this.showBalanceBlock = hasWalletBal || hasScBal;
    }

    clearWalletMetaAndRouteToHomePage() {
        setTimeout(() => {
            this.clearWalletMeta.emit();
            this.routerService.goToHomePage();
        }, RECHARGE_SUCCESSFUL_DISPLAY_TIME);
    }
}
