import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { Transaction } from "@models";

import { TRANSACTION_TYPE } from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-wrapper",
    template: `
        <supr-wallet-transaction-payment
            [transaction]="transaction"
            *ngIf="transaction.obj_type === transactionTypes.PAYMENT"
        >
        </supr-wallet-transaction-payment>

        <supr-wallet-transaction-recharge
            [transaction]="transaction"
            *ngIf="transaction.obj_type === transactionTypes.RECHARGE"
        >
        </supr-wallet-transaction-recharge>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionWrapperComponent {
    @Input() transaction: Transaction;

    readonly transactionTypes = TRANSACTION_TYPE;
}
