import { Component, Input, ChangeDetectionStrategy } from "@angular/core";
import { Transaction } from "@models";

@Component({
    selector: "supr-wallet-transaction-list",
    template: `
        <ng-container
            *ngFor="let transaction of transactions; trackBy: trackByFn"
        >
            <supr-wallet-transaction-wrapper [transaction]="transaction">
            </supr-wallet-transaction-wrapper>
        </ng-container>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsListComponent {
    @Input() transactions: Transaction[];

    trackByFn(transaction: Transaction): number {
        return transaction.id;
    }
}
