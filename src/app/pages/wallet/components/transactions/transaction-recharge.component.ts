import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { RechargeTransaction } from "@models";

import {
    RECHARGE_TRANSACTION_TYPE,
    RECHARGE_TRANSACTION_DISPLAY_NAME,
    RECHARGE_TYPE_DISPLAY_NAME,
} from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-recharge",
    template: `
        <div class="transaction">
            <div class="suprRow">
                <supr-text type="body">
                    {{ rechargeTransactionDisplayName[transaction.type] }}
                </supr-text>
                <supr-text type="paragraph" class="refill last">
                    {{ rechargeString }}
                </supr-text>
            </div>

            <div class="suprRow">
                <supr-text type="caption" class="subtext">
                    {{ rechargeTypeString }}
                </supr-text>
                <supr-text
                    type="body"
                    class="recharge last"
                    [class.cashback]="isPromo"
                >
                    {{ icon }} {{ transaction.paid_amount | rupee }}
                </supr-text>
            </div>

            <div class="suprRow">
                <supr-text type="caption" class="subtext">
                    {{ transaction?.created | date: "EEEE, MMM d, y" }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionRechargeComponent {
    @Input()
    set transaction(txn: RechargeTransaction) {
        this.init(txn);
    }

    rechargeTransactionDisplayName = RECHARGE_TRANSACTION_DISPLAY_NAME;

    get transaction(): RechargeTransaction {
        return this._transaction;
    }

    get isPromo(): boolean {
        return this._isPromo;
    }

    icon = "+";
    rechargeString = "";
    rechargeTypeString = "";

    private _isPromo = false;
    private _transaction: RechargeTransaction;

    private init(txn: RechargeTransaction) {
        this._transaction = txn;
        this._isPromo = txn.type === RECHARGE_TRANSACTION_TYPE.PROMO;
        this.icon = txn.paid_amount > 0 ? "+" : txn.paid_amount < 0 ? "-" : "";
        this.rechargeString = this._isPromo
            ? "Promo"
            : txn.paid_amount > 0
            ? "Wallet refill"
            : "";
        this.rechargeTypeString =
            RECHARGE_TYPE_DISPLAY_NAME[txn.type_details] || txn.type_details;
    }
}
