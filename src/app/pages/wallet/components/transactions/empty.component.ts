import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-wallet-empty-transactions",
    template: `
        <div class="empty suprColumn center">
            <supr-svg></supr-svg>
            <div class="divider24"></div>

            <supr-text type="heading">No transactions yet</supr-text>
            <div class="divider8"></div>

            <supr-text type="caption" class="subText">
                There are currently no transactions made.
            </supr-text>
            <supr-text type="caption" class="subText">
                Make one to get maximum discounts.
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyTransactionsComponent {}
