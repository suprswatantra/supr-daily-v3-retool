import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TRANSACTION_FILTERS } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-filters",
    template: `
        <div class="suprRow filters spaceBetween">
            <div class="filtersItem" *ngFor="let filter of filters">
                <supr-button
                    [class.active]="filter.key === activeTab"
                    (handleClick)="handleTabClick.emit(filter.key)"
                >
                    <supr-text type="body">
                        {{ filter.label | titlecase }}
                    </supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionFiltersComponent {
    @Input() activeTab: string;
    @Output() handleTabClick: EventEmitter<string> = new EventEmitter();

    filters = TRANSACTION_FILTERS;
}
