import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { Transaction } from "@models";

import { TRANSACTION_FILTERS } from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-transactions",
    template: `
        <div class="transactions">
            <div class="loader suprColumn center" *ngIf="loading; else txnList">
                <supr-loader></supr-loader>
            </div>

            <ng-template #txnList>
                <ng-container *ngIf="transactions?.length; else noTxns">
                    <supr-wallet-transaction-filters
                        [activeTab]="activeTab"
                        (handleTabClick)="onTabClick($event)"
                    ></supr-wallet-transaction-filters>

                    <supr-wallet-transaction-list
                        [transactions]="filteredTxns[activeTab]"
                    ></supr-wallet-transaction-list>
                </ng-container>
            </ng-template>

            <ng-template #noTxns>
                <supr-wallet-empty-transactions></supr-wallet-empty-transactions>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsComponent implements OnChanges {
    @Input() transactions: Transaction[];

    loading = true;
    activeTab = TRANSACTION_FILTERS[0].label;
    filteredTxns: { [key: string]: Transaction[] } = {};

    constructor(private walletService: WalletPageService) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["transactions"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue) {
            this.loading = false;
            this.setFilteredList(this.activeTab);
        }
    }

    onTabClick(selectedTab: string) {
        this.activeTab = selectedTab;
        this.setFilteredList(selectedTab);
    }

    private setFilteredList(tab: string) {
        if (this.filteredTxns[tab]) {
            return;
        }

        this.filteredTxns[tab] = this.walletService.getFilteredTransactions(
            tab,
            this.transactions || []
        );
    }
}
