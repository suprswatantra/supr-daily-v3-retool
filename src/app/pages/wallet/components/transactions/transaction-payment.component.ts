import { QuantityService } from "./../../../../services/util/quantity.service";
import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { PaymentTransaction } from "@models";

import { PAYMENT_TRANSACTION_PAYMENT_TYPE } from "../../constants/wallet.constants";

@Component({
    selector: "supr-wallet-transaction-payment",
    template: `
        <div class="transaction">
            <div class="suprRow">
                <supr-text type="body">
                    {{ transaction?.sku?.sku_name }}
                </supr-text>
                <supr-text
                    type="paragraph"
                    class="refund last"
                    *ngIf="isRefund"
                >
                    Refund
                </supr-text>
            </div>

            <div class="suprRow">
                <supr-text type="caption" class="subtext">
                    {{ qtyStr }}
                </supr-text>
                <supr-text type="body" class="last" [class.refund]="isRefund">
                    {{ icon }} {{ transaction.paid_amount | rupee }}
                </supr-text>
            </div>

            <div class="suprRow">
                <supr-text type="caption" class="subtext">
                    {{ transaction?.created | date: "EEEE, MMM d, y" }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/transactions.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionPaymentComponent {
    @Input()
    set transaction(txn: PaymentTransaction) {
        this._transaction = txn;
        this.initData();
    }

    get transaction(): PaymentTransaction {
        return this._transaction;
    }

    get isRefund(): boolean {
        return this._isRefund;
    }

    icon = "+";
    qtyStr: string;

    private _isRefund = false;
    private _transaction: PaymentTransaction;

    constructor(private quantityService: QuantityService) {}

    private initData() {
        const txn = this.transaction;
        this._isRefund =
            txn.payment_type === PAYMENT_TRANSACTION_PAYMENT_TYPE.REFUND;
        this.icon = this._isRefund ? "+" : "-";
        this.setQtyStr();
    }

    private setQtyStr() {
        if (!this.transaction) {
            return;
        }

        const { sku, quantity } = this.transaction;
        this.qtyStr = this.quantityService.toTextTxn(sku, quantity);
    }
}
