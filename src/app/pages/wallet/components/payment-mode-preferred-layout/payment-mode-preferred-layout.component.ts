import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { PAYMENT_METHODS_V2, CARD_TYPES } from "@constants";

import { PaymentMethodOption } from "@models";

import { SelectPaymentMethodV2 } from "@types";

import {
    TEXTS,
    CARD_SERVICE_DATA,
} from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-payment-mode-preferred-layout",
    templateUrl: "./payment-mode-preferred-layout.component.html",
    styleUrls: ["./payment-mode-preferred-layout.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModePreferredLayoutComponent implements OnInit {
    @Input() showCVVInput = false;
    @Input() options: PaymentMethodOption[];
    @Input() selectedOption: number | string;

    @Input() saContext: number;
    @Input() saObjectName: string;

    @Output() handleItemClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    _cvvMaxLength = 3;
    _walletTexts = TEXTS;
    _noExpiryOrCvv = false;
    _cardTypes = CARD_TYPES;
    _iconNames = ICON_NAMES;
    _PAYMENT_METHODS_V2 = PAYMENT_METHODS_V2;

    constructor(private walletPageService: WalletPageService) {}

    ngOnInit() {
        this._initFormDataInService();
    }

    _trackByFn(_: any, index: number): number {
        return index;
    }

    _handleItemClick(option: PaymentMethodOption) {
        this.showCVVInput = true;
        this._initFormDataInService();

        if (option) {
            this._cvvMaxLength =
                option.method === PAYMENT_METHODS_V2.CARD
                    ? this._getCvvMaxLength(option)
                    : this._cvvMaxLength;

            this.handleItemClick.emit({
                id: option.id,
                method: option.method,
                paymentInfo: option.payment_info,
            });
        }
    }

    _toggleNoExpiryOrCvv() {
        this._noExpiryOrCvv = !this._noExpiryOrCvv;

        this.walletPageService.setSavedCardFormData(
            CARD_SERVICE_DATA.VALIDATE_EXPIRY_CVV,
            !this._noExpiryOrCvv
        );
    }

    _handleCvvChange(cvv: string, option: PaymentMethodOption) {
        this.walletPageService.handleFormCVVChange(cvv, option);
    }

    private _getCvvMaxLength(option: PaymentMethodOption): number {
        return this.walletPageService.getCvvLengthBasedOnNetwork(
            (option && option.network) as CARD_TYPES
        );
    }

    private _initFormDataInService() {
        this.walletPageService.resetSavedCardFormData();
    }
}
