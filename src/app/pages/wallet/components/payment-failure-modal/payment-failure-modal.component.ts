import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";

import {
    MODAL_NAMES,
    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE,
    SETTINGS,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { Segment } from "@types";

import { SettingsService } from "@services/shared/settings.service";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-wallet-payment-failure-modal",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleClose?.emit()"
                modalName="${MODAL_NAMES.PAYMENT_FAILURE}"
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                    .PAYMENT_FAILED_MODAL}"
                [saObjectValue]="err?.reason || err?.description || errType"
                [saContext]="amountToPay"
            >
                <div class="modalWrapper">
                    <div class="suprRow">
                        <supr-icon
                            [name]="paymentFailureModalData?.titleIcon"
                        ></supr-icon>
                        <div class="spacer8"></div>
                        <supr-text type="subtitle" class="title">{{
                            err?.description || paymentFailureModalData?.title
                        }}</supr-text>
                    </div>
                    <div class="divider12"></div>
                    <supr-text type="body" class="subtitle">{{
                        paymentFailureModalData?.subtitle
                    }}</supr-text>
                    <div class="divider8"></div>
                    <supr-text type="body" class="amountToPay"
                        >{{ paymentFailureModalData?.amountToPayText }}&nbsp;{{
                            amountToPay | rupee
                        }}</supr-text
                    >
                    <div class="divider20"></div>
                    <div class="suprRow spaceBetween">
                        <div class="btnWrapper suprRow">
                            <supr-button
                                class="otherModesBtn"
                                (handleClick)="handleClose.emit()"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .TRY_OTHER_MODES}"
                                >{{
                                    paymentFailureModalData?.primaryBtnText
                                }}</supr-button
                            >
                        </div>
                        <div
                            class="btnWrapper suprRow"
                            *ngIf="
                                errType ===
                                RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR
                            "
                        >
                            <div class="spacer8"></div>
                            <supr-button
                                class="retryBtn"
                                (handleClick)="handlePaymentRetry.emit()"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK.RETRY}"
                                >{{
                                    paymentFailureModalData?.secondaryBtnText
                                }}</supr-button
                            >
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["./payment-failure-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentFailureModalComponent implements OnInit {
    @Input() showModal: boolean;
    @Input() amountToPay: number;
    @Input() err: Error;
    @Input() errType:
        | RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR
        | RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PRE_PROCESSING_ERROR;
    @Input() selectedPaymentMethod: string;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output() handlePaymentRetry: EventEmitter<void> = new EventEmitter();

    paymentFailureModalData;
    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE = RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE;
    saContextList: Segment.ContextListItem[] = [];

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        this.paymentFailureModalData = this.settingsService.getSettingsValue(
            SETTINGS.PAYMENT_FAILURE_MODAL_DATA,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.PAYMENT_FAILURE_MODAL_DATA]
        );
        this.saContextList = [
            {
                name: SA_OBJECT_NAMES.CONTEXT_LIST.PAYMENT_METHOD,
                value: this.selectedPaymentMethod,
            },
        ];
    }
}
