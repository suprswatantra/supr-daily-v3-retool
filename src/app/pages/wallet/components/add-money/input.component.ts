import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-wallet-money-input",
    template: `
        <div class="suprRow addMoneyInput">
            <div class="addMoneyInputPrefix"></div>
            <div class="spacer16"></div>
            <div class="addMoneyInputBox">
                <supr-input
                    type="number"
                    [value]="amountToFill"
                    textType="heading"
                    (handleInputChange)="handleInputChange.emit($event)"
                    (handleInputClick)="handleInputClick.emit($event)"
                    placeholder="0"
                    debounceTime="0"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_MONEY}"
                ></supr-input>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent {
    @Input() amountToFill: number;

    @Output() handleInputChange: EventEmitter<number> = new EventEmitter();
    @Output() handleInputClick: EventEmitter<TouchEvent> = new EventEmitter();
}
