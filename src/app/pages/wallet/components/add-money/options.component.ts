import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { SA_OBJECT_NAMES } from "@pages/wallet/constants/analytics.constants";
import { TEXTS } from "@pages/wallet/constants/wallet.constants";

@Component({
    selector: "supr-wallet-money-options",
    template: `
        <div class="addMoneyOptions">
            <div class="suprRow spaceBetween">
                <ng-container
                    *ngFor="let option of options; trackBy: trackByFn"
                >
                    <div
                        class="addMoneyOptionsItem"
                        [class.active]="option === selectedAmount"
                        (click)="handleOptionClick.emit(option)"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK.AMOUNT}"
                        [saObjectValue]="option"
                    >
                        <supr-text type="body">{{ option | rupee }}</supr-text>
                    </div>
                </ng-container>
            </div>
            <div class="divider4"></div>
            <div class="suprColumn">
                <supr-text type="caption">{{ MOST_POPULAR }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionsComponent {
    @Input() options: number[];
    @Input() selectedAmount: number;
    @Output() handleOptionClick: EventEmitter<number> = new EventEmitter();

    MOST_POPULAR = TEXTS.MOST_POPULAR;

    trackByFn(_: any, index: number) {
        return index;
    }
}
