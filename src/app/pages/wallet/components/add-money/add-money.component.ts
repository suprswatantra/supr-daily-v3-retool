import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
    ViewChild,
    ElementRef,
    OnDestroy,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { WalletMeta } from "@models";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { RouterService } from "@services/util/router.service";

import {
    TEXTS,
    AMOUNT_ERROR_FOR_COUPON,
} from "@pages/wallet/constants/wallet.constants";
import { SA_OBJECT_NAMES } from "@pages/wallet/constants/analytics.constants";

@Component({
    selector: "supr-wallet-add-money",
    template: `
        <div class="addMoney" #addMoney>
            <supr-text type="body" class="title">
                ${TEXTS.ADD_MONEY_TITLE}
            </supr-text>

            <supr-text type="caption" class="note">
                ${TEXTS.ADD_MONEY_SUBTITLE}
            </supr-text>
            <div class="divider16"></div>

            <div class="addMoneyWithCoupon">
                <supr-wallet-money-input
                    [amountToFill]="selectedAmount"
                    (handleInputChange)="onInputChange($event)"
                    (handleInputClick)="onInputClick()"
                ></supr-wallet-money-input>

                <ng-container
                    *ngIf="
                        isWalletVersionV2 &&
                        showCouponBlock &&
                        !usingCartPaymentCouponCode
                    "
                >
                    <div
                        class="rechargeCouponContainer"
                        *ngIf="!isRechargeCouponApplied"
                    >
                        <supr-recharge-coupon
                            [walletMeta]="walletMeta"
                            [amount]="selectedAmount"
                            (goToOffersPage)="goToWalletOffers()"
                            (amountError)="amountErrorForCoupon()"
                        ></supr-recharge-coupon>
                    </div>
                </ng-container>
            </div>

            <ng-container *ngIf="amountErrorMsg">
                <div class="warningMsg">
                    <div class="divider4"></div>
                    <supr-text type="caption">
                        {{ amountErrorMsg }}
                    </supr-text>
                </div>
            </ng-container>

            <div class="divider16"></div>

            <supr-wallet-money-options
                [options]="options"
                [selectedAmount]="selectedAmount"
                (handleOptionClick)="onInputChange($event)"
            ></supr-wallet-money-options>

            <ng-container *ngIf="isRechargeCouponApplied && showCouponBlock">
                <div class="divider16"></div>
                <div class="couponAppliedContainer">
                    <supr-applied-coupon
                        [couponCode]="
                            walletMeta?.rechargeCouponInfo?.couponCode
                        "
                        [couponCodeMessage]="
                            walletMeta?.rechargeCouponInfo?.successMessage
                        "
                        [showTncButton]="showTncButton"
                        [saContext]="${SA_OBJECT_NAMES.CONTEXT.REMOVE_COUPON}"
                        (handleRemove)="clearRechargeCoupon.emit()"
                    ></supr-applied-coupon>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddMoneyComponent implements OnInit, OnChanges, OnDestroy {
    @Input() options: number[];
    @Input() amountToFill: number;
    @Input() defaultAmount: number;
    @Input() suprScrollContentEl: ElementRef;
    @Input() walletMeta: WalletMeta;
    @Input() suprCreditsEnabled: boolean;
    @Input() isWalletVersionV2: boolean;
    @Input() autoCheckout: boolean;
    @Input() usingCartPaymentCouponCode: boolean;

    @Output() handleInputChange: EventEmitter<number> = new EventEmitter();
    @Output() clearRechargeCoupon: EventEmitter<void> = new EventEmitter();

    @ViewChild("addMoney", { static: true }) addMoneyEl: ElementRef;

    selectedAmount: number;
    amountErrorMsg: string;
    showCouponBlock = true;
    isRechargeCouponApplied = false;
    showTncButton = false;

    constructor(
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private settingsService: SettingsService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.initialize();
        this.handleCouponBlock();
        const couponDisplayConfig = this.settingsService.getSettingsValue(
            SETTINGS.COUPON_TNC_DISPLAY_CONFIG,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.COUPON_TNC_DISPLAY_CONFIG]
        );
        if (couponDisplayConfig && couponDisplayConfig.wallet) {
            this.showTncButton = !couponDisplayConfig.wallet.hideTnc;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["walletMeta"])) {
            this.handleCouponBlock();
        }
    }

    ngOnDestroy() {
        this.clearRechargeCouponInfo();
    }

    onInputChange(newAmount: number, noCouponReset?: boolean) {
        this.amountErrorMsg = "";
        this.selectedAmount = +newAmount;
        this.cdr.detectChanges();

        if (!noCouponReset) {
            this.clearRechargeCouponInfo();
        }

        this.handleInputChange.emit(this.selectedAmount);
    }

    onInputClick() {
        setTimeout(() => {
            this.scrollAddMoneyIntoView();
        }, 300);
    }

    amountErrorForCoupon() {
        this.amountErrorMsg = AMOUNT_ERROR_FOR_COUPON;
    }

    goToWalletOffers() {
        this.routerService.goToWalletOffersPage(this.selectedAmount);
    }

    private initialize() {
        this.selectedAmount = this.amountToFill || this.defaultAmount || null;
        this.onInputChange(this.selectedAmount, true);
    }

    private handleCouponBlock() {
        if (this.autoCheckout && !this.isCouponCodeApplied()) {
            this.showCouponBlock = false;
        } else {
            this.showCouponBlock = true;
            if (this.isCouponCodeApplied()) {
                this.isRechargeCouponApplied = true;
            } else {
                this.isRechargeCouponApplied = false;
            }
        }
    }

    private scrollAddMoneyIntoView() {
        const suprScrollContent = this.suprScrollContentEl;

        const addMoneyContent = this.addMoneyEl.nativeElement as HTMLElement;

        if (suprScrollContent && addMoneyContent) {
            const computedStyle = getComputedStyle(document.documentElement);
            const property = computedStyle
                ? computedStyle.getPropertyValue("--supr-page-header-height")
                : null;
            const headerHeight = property ? property.split("px")[0] : 0;

            const offset = addMoneyContent.offsetTop - Number(headerHeight);

            if (suprScrollContent) {
                suprScrollContent["scrollTop"] = offset;
                suprScrollContent["scrollLeft"] = 0;
            }
        }
    }

    private clearRechargeCouponInfo() {
        if (this.walletMeta && this.walletMeta.rechargeCouponInfo) {
            this.clearRechargeCoupon.emit();
        }
    }

    private isCouponCodeApplied(): boolean {
        return this.utilService.getNestedValue(
            this.walletMeta,
            "rechargeCouponInfo.isApplied"
        );
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
