import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { WalletService } from "@services/shared/wallet.service";
import { WalletCurrentState } from "@store/wallet/wallet.state";

import { TEXTS } from "@pages/wallet/constants/wallet.constants";
import { City, WalletExpiryDetails } from "@models";

@Component({
    selector: "supr-wallet-balance",
    template: `
        <div class="balance">
            <ng-container *ngIf="loading; else amount">
                <supr-text class="walletAmount">
                    <span class="suprRupee">--</span>
                </supr-text>
            </ng-container>

            <ng-template #amount>
                <div class="suprRow">
                    <div
                        class="suprColumn wallet left"
                        [ngClass]="{
                            withBorder: suprCreditsEnabled
                        }"
                    >
                        <supr-text type="subtitle">
                            ${TEXTS.CURRENT_BALANCE}
                        </supr-text>
                        <div class="suprRow">
                            <supr-text type="subheading" class="walletAmount">
                                {{ balance | rupee }}
                            </supr-text>
                            <div class="spacer32"></div>
                        </div>
                    </div>
                    <ng-container *ngIf="suprCreditsEnabled">
                        <div class="suprColumn suprCredits left">
                            <supr-text type="subtitle">
                                ${TEXTS.SUPR_CREDITS_BALANCE}
                            </supr-text>
                            <div class="suprRow">
                                <supr-svg class="scIcon"></supr-svg>
                                <div class="spacer4"></div>
                                <supr-text
                                    type="subheading"
                                    class="walletAmount"
                                >
                                    {{ suprCreditsBalance }}
                                </supr-text>
                                <div class="spacer4"></div>
                                <supr-icon
                                    name="error"
                                    (click)="showSuprCreditsInfoModal()"
                                ></supr-icon>
                            </div>
                        </div>
                    </ng-container>
                </div>
            </ng-template>
            <supr-credits-info
                [showModal]="showCreditsInfoModal"
                [suprCreditsBalance]="suprCreditsBalance"
                [city]="city"
                [expiry]="suprCreditsExpiry"
                (handleClose)="closeSuprCreditsInfoModal()"
            ></supr-credits-info>
        </div>
    `,
    styleUrls: ["../../styles/wallet.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WalletBalanceComponent implements OnChanges {
    @Input() balance: number;
    @Input() suprCreditsBalance: number;
    @Input() suprCreditsEnabled: boolean;
    @Input() suprCreditsExpiry: WalletExpiryDetails;
    @Input() walletState: WalletCurrentState;
    @Input() type = "title";
    @Input() city: City;

    loading = false;
    showCreditsInfoModal = false;

    constructor(private walletService: WalletService) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["walletState"];
        if (!change) {
            return;
        }

        if (change.currentValue === WalletCurrentState.FETCHING_BALANCE) {
            this.loading = true;
        } else if (change.currentValue === WalletCurrentState.NO_ACTION) {
            this.walletService.setWalletBalance(this.balance);
            this.walletService.setSuprCreditsBalance(this.suprCreditsBalance);
            this.loading = false;
        }
    }

    closeSuprCreditsInfoModal() {
        this.showCreditsInfoModal = false;
    }

    showSuprCreditsInfoModal() {
        this.showCreditsInfoModal = true;
    }
}
