import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    OnInit,
} from "@angular/core";

import { WalletMeta } from "@shared/models";

import { RECHARGE_COUPON_TEXTS } from "../../constants/wallet.constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-recharge-coupon",
    template: `
        <div class="rechargeCoupon">
            <div class="suprRow rechargeCouponContent">
                <ng-container *ngIf="!isRechargeCouponApplied">
                    <div
                        class="suprRow applyCoupon"
                        (click)="showOffers()"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK.APPLY_PROMO}"
                    >
                        <supr-text type="paragraph">
                            ${RECHARGE_COUPON_TEXTS.NOT_APPLIED}
                        </supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/recharge-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RechargeCouponComponent implements OnInit, OnChanges {
    @Input() walletMeta: WalletMeta;
    @Input() amount: number;

    @Output() amountError: EventEmitter<void> = new EventEmitter();
    @Output() goToOffersPage: EventEmitter<void> = new EventEmitter();

    isRechargeCouponApplied = false;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const walletMetaChange = changes["walletMeta"];

        if (this.canHandleChange(walletMetaChange)) {
            this.initialize();
        }
    }

    showOffers() {
        if (this.amount > 0) {
            this.goToOffersPage.emit();
            return;
        }

        this.amountError.emit();
    }

    private initialize() {
        this.isRechargeCouponApplied = this.isCouponCodeApplied();
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private isCouponCodeApplied(): boolean {
        return this.utilService.getNestedValue(
            this.walletMeta,
            "rechargeCouponInfo.isApplied"
        );
    }
}
