import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { PaymentMethodOption } from "@models";

import { SelectPaymentMethodV2 } from "@types";

@Component({
    selector: "supr-wallet-payment-mode-tile-layout",
    templateUrl: "./payment-mode-tile-layout.component.html",
    styleUrls: ["./payment-mode-tile-layout.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentModeTileLayoutComponent {
    @Input() header: string;
    @Input() ctaText: string;
    @Input() ctaTextType: string;
    @Input() options: PaymentMethodOption[];
    @Input() selectedOption: number | string;

    @Input() saContext: number;
    @Input() saObjectName: string;

    @Output() handleCtaClick: EventEmitter<void> = new EventEmitter();
    @Output() handleTileClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    _iconNames = ICON_NAMES;

    _trackByFn(_: any, index: number): number {
        return index;
    }

    _handleTileClick(option: PaymentMethodOption) {
        if (option) {
            this.handleTileClick.emit({
                id: option.id,
                paymentInfo: option.payment_info,
            });
        }
    }
}
