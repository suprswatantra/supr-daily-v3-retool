import {
    Input,
    OnInit,
    Output,
    Component,
    OnDestroy,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Platform } from "@ionic/angular";

import { Subscription } from "rxjs";

import { ICON_NAMES } from "@components/supr-icon/supr-icon.constants";

import { PAYMENT_METHODS_V2 } from "@constants";

import { PaymentMethodOption } from "@models";

import { RouterService } from "@services/util/router.service";

import { SelectPaymentMethodV2 } from "@types";

import { BACK_BTN_SUB_PRIORITY } from "@pages/wallet/constants/wallet.constants";
import { WalletPageService } from "@pages/wallet/services/wallet.service";

@Component({
    selector: "supr-wallet-banks-search",
    templateUrl: "./banks-search.component.html",
    styleUrls: ["./banks-search.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BanksSearchComponent implements OnInit, OnDestroy {
    @Input() selectedOption: number;
    @Input() set options(options: PaymentMethodOption[]) {
        this._searchResults = options;
        this._unfilteredOptions = options;
    }
    get options(): PaymentMethodOption[] {
        return this._searchResults;
    }

    @Output() closeBanksSearchView: EventEmitter<void> = new EventEmitter();
    @Output() handleItemClick: EventEmitter<
        SelectPaymentMethodV2
    > = new EventEmitter();

    private _searchResults = [];
    private _unfilteredOptions = [];
    private _backBtnSubscription: Subscription;

    constructor(
        private platform: Platform,
        private routerService: RouterService,
        private walletPageService: WalletPageService
    ) {}

    ngOnInit() {
        this._registerBackButton();
    }

    ngOnDestroy() {
        this._deRegisterBackButton();
    }

    _goBack() {
        this.closeBanksSearchView.emit();
    }

    _filterBanksList(searchTerm: string) {
        this._searchResults = this.walletPageService.filterBanksBySearchTerm(
            this._unfilteredOptions,
            searchTerm
        );
    }

    _trackByFn(_: any, index: number): number {
        return index;
    }

    _getOptionIconLeft(optionId: number): string {
        return this.selectedOption === optionId
            ? ICON_NAMES.APPROVE_FILLED
            : ICON_NAMES.APPROVE;
    }

    _handleItemClick(option: PaymentMethodOption) {
        if (option) {
            this.handleItemClick.emit({
                id: option.id,
                paymentInfo: option.payment_info,
                method: PAYMENT_METHODS_V2.NETBANKING,
            });
        }
    }

    private _registerBackButton() {
        this._disableBackButton();
        this._backBtnSubscription = this.platform.backButton.subscribeWithPriority(
            BACK_BTN_SUB_PRIORITY,
            () => this._goBack()
        );
    }

    private _deRegisterBackButton() {
        if (this._backBtnSubscription && !this._backBtnSubscription.closed) {
            this._backBtnSubscription.unsubscribe();
        }

        this._enableBackButton();
    }

    private _enableBackButton() {
        this.routerService.setIgnoreBackButton(false);
    }

    private _disableBackButton() {
        this.routerService.setIgnoreBackButton(true);
    }
}
