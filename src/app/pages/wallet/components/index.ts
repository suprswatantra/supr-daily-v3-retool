import { WalletHeaderComponent } from "./header/wallet-header.component";
import { WalletBalanceComponent } from "./balance/balance.component";

import { AddMoneyComponent } from "./add-money/add-money.component";
import { InputComponent } from "./add-money/input.component";
import { OptionsComponent } from "./add-money/options.component";

import { TransactionsComponent } from "./transactions/transactions.component";
import { TransactionsHeaderComponent } from "./header/transactions-header.component";
import { TransactionFiltersComponent } from "./transactions/filters.component";
import { TransactionsListComponent } from "./transactions/list.component";
import { TransactionWrapperComponent } from "./transactions/transaction-wrapper.component";
import { TransactionPaymentComponent } from "./transactions/transaction-payment.component";
import { TransactionRechargeComponent } from "./transactions/transaction-recharge.component";
import { EmptyTransactionsComponent } from "./transactions/empty.component";

import { TransactionsV2Component } from "./transactions-v2/transactions.component";
import { TransactionsV2HeaderComponent } from "./header/transactions-v2-header.component";
import { TransactionV2FiltersComponent } from "./transactions-v2/filters.component";
import { TransactionsV2ListComponent } from "./transactions-v2/list.component";
import { TransactionV2WrapperComponent } from "./transactions-v2/transaction-wrapper.component";
import { TransactionV2PaymentComponent } from "./transactions-v2/transaction-payment.component";
import { TransactionV2RechargeComponent } from "./transactions-v2/transaction-recharge.component";
import { EmptyTransactionsV2Component } from "./transactions-v2/empty.component";
import { TransactionV2CreditsExpiryComponent } from "./transactions-v2/suprcredits-expiry.component";

import { RechargeCouponComponent } from "./recharge-coupon/recharge-coupon.component";

import { RechargeSuccessfulComponent } from "./recharge-successful/recharge-successful.component";
import { TransactionSuprCreditsComponent } from "./transactions-v2/transaction-suprcredits.component";

import { CVVInputComponent } from "./cvv-input/cvv-input.component";
import { CardFormComponent } from "./card-form/card-form.component";
import { VPAInputComponent } from "./vpa-input/vpa-input.component";
import { BanksSearchComponent } from "./banks-search/banks-search.component";
import { PaymentModeTileComponent } from "./payment-mode-tile/payment-mode-tile.component";
import { PaymentModeWrapperComponent } from "./payment-mode-wrapper/payment-mode-wrapper.component";
import { PaymentFailureModalComponent } from "./payment-failure-modal/payment-failure-modal.component";
import { PaymentModeVPALayoutComponent } from "./payment-mode-vpa-layout/payment-mode-vpa-layout.component";
import { PaymentModeTileLayoutComponent } from "./payment-mode-tile-layout/payment-mode-tile-layout.component";
import { PaymentModeCardLayoutComponent } from "./payment-mode-card-layout/payment-mode-card-layout.component";
import { PaymentModePreferredLayoutComponent } from "./payment-mode-preferred-layout/payment-mode-preferred-layout.component";

export const walletComponents = [
    WalletHeaderComponent,
    WalletBalanceComponent,

    AddMoneyComponent,
    InputComponent,
    OptionsComponent,

    TransactionsComponent,
    TransactionsHeaderComponent,
    TransactionFiltersComponent,
    TransactionsListComponent,
    TransactionPaymentComponent,
    TransactionRechargeComponent,
    TransactionWrapperComponent,
    EmptyTransactionsComponent,

    TransactionsV2Component,
    TransactionsV2HeaderComponent,
    TransactionV2FiltersComponent,
    TransactionsV2ListComponent,
    TransactionV2PaymentComponent,
    TransactionV2RechargeComponent,
    TransactionV2WrapperComponent,
    EmptyTransactionsV2Component,
    TransactionV2CreditsExpiryComponent,

    RechargeCouponComponent,

    RechargeSuccessfulComponent,
    TransactionSuprCreditsComponent,

    VPAInputComponent,
    CVVInputComponent,
    CardFormComponent,
    BanksSearchComponent,
    PaymentModeTileComponent,
    PaymentModeWrapperComponent,
    PaymentFailureModalComponent,
    PaymentModeVPALayoutComponent,
    PaymentModeTileLayoutComponent,
    PaymentModeCardLayoutComponent,
    PaymentModePreferredLayoutComponent,
];
