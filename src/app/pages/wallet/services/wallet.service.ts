import { Injectable } from "@angular/core";

import { Observable, of, from } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import {
    REGEX_MAP,
    CARD_TYPES,
    CVV_LENGTH,
    PAYMENT_METHODS_V2,
    PAYMENT_METHOD_VERSIONS,
    PAYMENT_METHODS_CUSTOM_OPTIONS,
    SELECTED_PAYMENT_METHODS_VALIDATION_TOASTS,
    SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES,
} from "@constants";

import {
    User,
    Transaction,
    PaymentMethodOption,
    RechargeCouponInfo,
} from "@models";

import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { SettingsService } from "@services/shared/settings.service";
import { PaymentService } from "@services/integration/payment.service";

import {
    WalletNewCardFormMeta,
    SelectPaymentMethodV2,
    PaymentMethodComponent,
    PaymentMethodV2Component,
    RazorpayPaymentMethodInfo,
    PaymentMethodComponentMeta,
    SelectedPaymentInfoValidation,
    RazorpayPaymentMethodFormInfo,
    RazorpayCreateCustomPaymentPayload,
} from "@types";

import {
    TRANSACTION_TYPE,
    CARD_SERVICE_DATA,
    WALLET_BANKS_COUNT,
} from "../constants/wallet.constants";

declare const Razorpay: any;
@Injectable()
export class WalletPageService {
    private newVpaFormData: RazorpayPaymentMethodInfo = {};
    private newCardFormData: RazorpayPaymentMethodFormInfo = {};
    private savedCardFormData: RazorpayPaymentMethodFormInfo = {};

    constructor(
        private utilService: UtilService,
        private toastService: ToastService,
        private paymentService: PaymentService,
        private settingsService: SettingsService
    ) {}

    getFilteredTransactions(
        transactionType: string,
        transactions: Transaction[] = []
    ): Transaction[] {
        let filteredTransactions = [...transactions];

        if (transactionType === TRANSACTION_TYPE.PAYMENT) {
            filteredTransactions = filteredTransactions.filter(
                (txn) => txn.obj_type === TRANSACTION_TYPE.PAYMENT
            );
        } else if (transactionType === TRANSACTION_TYPE.RECHARGE) {
            filteredTransactions = filteredTransactions.filter(
                (txn) => txn.obj_type === TRANSACTION_TYPE.RECHARGE
            );
        }

        return filteredTransactions;
    }

    // For SuprCredits
    getFilteredTransactionsV2(
        transactions: Transaction[] = [],
        suprCredits: boolean = false
    ): Transaction[] {
        if (!suprCredits) {
            return transactions.filter((txn) => this.isWalletTxn(txn));
        }

        return transactions.filter((txn) => txn.supr_credits_type);
    }

    isWalletTransactionsPage(url: string): boolean {
        const hasWallet = url && url.indexOf("wallet") !== -1;
        const hasTransactions = url && url.indexOf("transactions") !== -1;
        return hasWallet && hasTransactions;
    }

    isWalletTxn(txn: Transaction): boolean {
        return (
            txn.obj_type === TRANSACTION_TYPE.PAYMENT ||
            (txn.obj_type === TRANSACTION_TYPE.RECHARGE &&
                !txn.supr_credits_type)
        );
    }

    getPaymentWrapperComponentSettings(group: string): any {
        const settingsConfig = this.settingsService.getSettingsValue(
            "paymentMethods"
        );

        return this.utilService.getNestedValue(settingsConfig, `${group}`);
    }

    getWalletMethodData(
        methodData: PaymentMethodComponent | PaymentMethodV2Component,
        selectedMethod: string | number,
        version: number
    ): PaymentMethodComponent | PaymentMethodV2Component {
        if (version === PAYMENT_METHOD_VERSIONS.V2) {
            return this.getWalletMethodDataV2(
                methodData as PaymentMethodV2Component,
                selectedMethod as number
            );
        }

        /* append icon to list items for paymentV1 flow based on selected flow */
        const _methodData = { ...(methodData as PaymentMethodComponent) };
        const methods = this.utilService.getNestedValue(_methodData, "methods");

        if (methods) {
            const updatedMethods = {};

            for (const methodName in methods) {
                if (methods[methodName]) {
                    updatedMethods[methodName] = {
                        ...methods[methodName],
                        icon: this.getMethodIcon(
                            methodName,
                            selectedMethod as string
                        ),
                    };
                }
            }

            _methodData.methods = updatedMethods;
        }

        return _methodData;
    }

    /* Check if an option from a certain group is selected as the current option - only for V2 flow */
    isGroupOptionSelected(
        methodData: PaymentMethodV2Component,
        selectedMethod: number,
        version: number
    ): boolean {
        if (
            version !== 2 ||
            !methodData ||
            !this.utilService.isLengthyArray(methodData.options)
        ) {
            return false;
        }

        return !!methodData.options.find(
            (option: PaymentMethodOption) => option.id === selectedMethod
        );
    }

    formatAddNewCardForm(
        formElement: HTMLElement,
        cvvElement: HTMLElement,
        expiryElement: HTMLElement,
        cardElement: HTMLElement
    ): Observable<WalletNewCardFormMeta> {
        return from(
            this.paymentService.getRazorpayCustomCheckoutInstance()
        ).pipe(
            switchMap((_: any) => {
                const formatter = Razorpay.setFormatter(formElement);
                const validationMap: WalletNewCardFormMeta = {
                    network: "",
                    validation: {},
                };

                return new Observable((observer) => {
                    formatter
                        .add("card", cardElement)
                        .on("network", function () {
                            /* set length of cvv element based on amex card */
                            const cvvlength =
                                this.type &&
                                this.type.toLowerCase() === CARD_TYPES.AMEX
                                    ? CVV_LENGTH.AMEX
                                    : CVV_LENGTH.OTHER;

                            validationMap.network = this.type;
                            validationMap.cvvLength = cvvlength;
                            cvvElement["maxLength"] = cvvlength;
                            cvvElement["pattern"] =
                                "^[0-9]{" + cvvlength + "}$";
                        })
                        .on("change", function () {
                            const isValid = this.isValid();

                            if (!validationMap.validation.card) {
                                validationMap.validation.card = {};
                            }
                            validationMap.validation.card.valid = isValid;

                            /* automatically focus expiry field if card number is valid and filled */
                            if (this.el.value.length === this.caretPosition) {
                                if (isValid) {
                                    expiryElement.focus();
                                } else {
                                    validationMap.validation.card.message =
                                        "Invalid Card Number";
                                }
                            }

                            if (!this.el.value || !this.el.value.length) {
                                validationMap.validation.card.message =
                                    "*This field is a mandatory field";
                            }

                            observer.next(validationMap);
                        });

                    formatter
                        .add("expiry", expiryElement)
                        .on("change", function () {
                            const isValid = this.isValid();

                            if (!validationMap.validation.expiry) {
                                validationMap.validation.expiry = {};
                            }
                            validationMap.validation.expiry.valid = isValid;

                            /* automatically focus cvv field if expiry is valid and filled */
                            if (this.el.value.length === this.caretPosition) {
                                if (isValid) {
                                    cvvElement.focus();
                                } else {
                                    validationMap.validation.expiry.message =
                                        "Invalid Expiry Date";
                                }
                            }

                            if (!this.el.value || !this.el.value.length) {
                                validationMap.validation.expiry.message =
                                    "*This field is a mandatory field";
                            }

                            observer.next(validationMap);
                        });

                    /* remove formatter listener on unsubscribe */
                    return () => {
                        formatter.off();
                    };
                });
            })
        );
    }

    getRazorpayCustomCheckoutData(
        user: User,
        amount: number,
        selectedMethodInfo: SelectPaymentMethodV2,
        couponInfo: RechargeCouponInfo,
        buySuprCredits: boolean
    ): Observable<RazorpayCreateCustomPaymentPayload> {
        return this.validateSelectedPaymentInfo(selectedMethodInfo).pipe(
            map((validationInfo: SelectedPaymentInfoValidation) => {
                if (!validationInfo.valid) {
                    this.handlePaymentInfoValidationError(validationInfo.error);
                    return;
                }

                /* deleting unnecessary fields from payment info */
                delete selectedMethodInfo.paymentInfo["network"];
                delete selectedMethodInfo.paymentInfo["validateExpiryAndCvv"];

                return {
                    user,
                    amount,
                    couponInfo,
                    paymentGateway: "RAZORPAY",
                    buySuprCredits,
                    paymentInfo: {
                        ...selectedMethodInfo.paymentInfo,
                        method: selectedMethodInfo.method,
                    },
                };
            }),
            catchError((_) => {
                /* [TODO_RITESH] Log custom error to sentry if some validation throws an error.
                Possible suspects: Razorpay Verify VPA */
                return of(null);
            })
        );
    }

    getSelectedPaymentMethodInfo(
        selectedMethodInfo: SelectPaymentMethodV2
    ): SelectPaymentMethodV2 {
        if (selectedMethodInfo) {
            switch (selectedMethodInfo.method) {
                case PAYMENT_METHODS_V2.CARD: {
                    const paymentInfo = this.getCardFormData(
                        selectedMethodInfo
                    );

                    return { ...selectedMethodInfo, paymentInfo };
                }
                case PAYMENT_METHODS_V2.UPI: {
                    const paymentInfo = this.getVpaData(selectedMethodInfo);

                    return { ...selectedMethodInfo, paymentInfo };
                }
            }
        }

        return selectedMethodInfo;
    }

    setSavedCardFormData(key: string, value: any) {
        if (key) {
            this.savedCardFormData[key] = value;
        }
    }

    resetSavedCardFormData() {
        this.savedCardFormData = {
            validateExpiryAndCvv: true,
        };
    }

    setNewCardFormData(key: string, value: any) {
        if (key) {
            this.newCardFormData[key] = value;
        }
    }

    resetNewCardFormData() {
        this.newCardFormData = {
            save: 1,
            validateExpiryAndCvv: true,
        };
    }

    setNewVpaData(key: string, value: any) {
        if (key) {
            this.newVpaFormData[key] = value;
        }
    }

    getCvvLengthBasedOnNetwork(network: CARD_TYPES): number {
        return network && network.toLowerCase() === CARD_TYPES.AMEX
            ? CVV_LENGTH.AMEX
            : CVV_LENGTH.OTHER;
    }

    validateCvvLengthBasedOnNetwork(cvv: string, network: CARD_TYPES): boolean {
        return cvv && cvv.length === this.getCvvLengthBasedOnNetwork(network);
    }

    filterBanksBySearchTerm(
        options: PaymentMethodOption[],
        searchTerm: string
    ): PaymentMethodOption[] {
        if (!this.utilService.isLengthyArray(options) || !searchTerm) {
            return options;
        }

        const _searchTerm = searchTerm.toLowerCase();

        return options.filter((option: PaymentMethodOption) => {
            const bankName = this.utilService
                .getNestedValue(option, "desc.title", "")
                .toLowerCase();

            return bankName.indexOf(_searchTerm) > -1;
        });
    }

    validateNewVpaId(vpa: string): Observable<SelectedPaymentInfoValidation> {
        if (!vpa) {
            return of({
                valid: false,
                error:
                    SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.INVALID_VPA,
            });
        }

        const razorpayObservable = from(
            this.paymentService.getRazorpayCustomCheckoutInstance()
        );

        return razorpayObservable.pipe(
            switchMap((razorpay: any) => {
                return this.verifyVpaIdWithRazorpay(razorpay, vpa).pipe(
                    map((valid: boolean) => {
                        return {
                            valid,
                            error: !valid
                                ? SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.INVALID_VPA
                                : null,
                        };
                    })
                );
            })
        );
    }

    handleFormCVVChange(cvv: string, option: PaymentMethodOption) {
        const valid = this.validateCvvLengthBasedOnNetwork(
            cvv,
            (option && option.network) as CARD_TYPES
        );

        if (valid) {
            this.setSavedCardFormData(CARD_SERVICE_DATA.CVV, cvv);
            return;
        }

        this.setSavedCardFormData(CARD_SERVICE_DATA.CVV, null);
    }

    private validateSelectedPaymentMethodV2(
        selectedPaymentMethod: SelectPaymentMethodV2
    ): boolean {
        return !!(
            !this.utilService.isEmpty(selectedPaymentMethod) &&
            selectedPaymentMethod.id !== null &&
            selectedPaymentMethod.method
        );
    }

    private verifyVpaIdWithRazorpay(
        razorpay: any,
        vpa: string
    ): Observable<boolean> {
        return from(razorpay.verifyVpa(vpa)).pipe(
            map(() => {
                return true;
            }),
            catchError((_) => {
                /* [TODO_RITESH] Log custom error to sentry if razorpay throws an error. */
                return of(false);
            })
        );
    }

    private validateSavedVpa(
        vpa: string
    ): Observable<SelectedPaymentInfoValidation> {
        /* no need to run through validation if it's a saved vpa, just do a null check */
        const valid = !!vpa;

        return of({
            valid,
            error: !valid
                ? SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN
                : null,
        });
    }

    private getCardFormData(
        selectedMethodInfo: SelectPaymentMethodV2
    ): RazorpayPaymentMethodFormInfo {
        if (selectedMethodInfo.id === PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_CARD) {
            return this.getNewCardFormData();
        }

        return this.getSavedCardFormData(selectedMethodInfo.paymentInfo);
    }

    private getVpaData(
        selectedMethodInfo: SelectPaymentMethodV2
    ): RazorpayPaymentMethodFormInfo {
        if (selectedMethodInfo.id === PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_VPA) {
            return this.getNewVpaData();
        }

        return selectedMethodInfo.paymentInfo;
    }

    private getNewVpaData(): RazorpayPaymentMethodInfo {
        return this.newVpaFormData;
    }

    private validateUPIPaymentInfo(
        paymentInfo: RazorpayPaymentMethodFormInfo,
        savedOption: boolean
    ): Observable<SelectedPaymentInfoValidation> {
        if (this.utilService.isEmpty(paymentInfo)) {
            return of({
                valid: false,
                error: SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN,
            });
        }

        if (savedOption) {
            return this.validateSavedVpa(paymentInfo.token);
        }

        return this.validateNewVpaId(paymentInfo.vpa);
    }

    private getSavedCardFormData(
        paymentInfo: RazorpayPaymentMethodInfo
    ): RazorpayPaymentMethodFormInfo {
        const formData = {
            "card[cvv]": this.savedCardFormData["card[cvv]"],
            validateExpiryAndCvv: this.savedCardFormData.validateExpiryAndCvv,
        };

        if (this.utilService.isEmpty(paymentInfo)) {
            return formData;
        }

        return {
            ...paymentInfo,
            ...formData,
        };
    }

    private getNewCardFormData(): RazorpayPaymentMethodFormInfo {
        const cardFormData = {
            ...this.newCardFormData,
        };

        if (cardFormData.card) {
            /* trim the formatted card number and return it as a number */
            const cardNumber = cardFormData.card.replace(REGEX_MAP.SPACE, "");

            cardFormData["card[number]"] =
                cardNumber && !isNaN(Number(cardNumber)) && Number(cardNumber);

            delete cardFormData.card;
        }

        if (cardFormData.expiry) {
            /* convert expiry into numeric month and year */
            const expiryDetails =
                cardFormData.expiry && cardFormData.expiry.split("/");
            const expiryYear =
                expiryDetails[1] &&
                !isNaN(Number(expiryDetails[1])) &&
                Number(expiryDetails[1]);
            const expiryMonth =
                expiryDetails[0] &&
                !isNaN(Number(expiryDetails[0])) &&
                Number(expiryDetails[0]);

            cardFormData["card[expiry_year]"] = expiryYear;
            cardFormData["card[expiry_month]"] = expiryMonth;

            delete cardFormData.expiry;
        }

        return cardFormData;
    }

    /* Validate payment_info object of Razorpay payload for mandatory fields based on method */
    private validateSelectedPaymentInfo(
        selectedMethodInfo: SelectPaymentMethodV2
    ): Observable<SelectedPaymentInfoValidation> {
        if (!this.validateSelectedPaymentMethodV2(selectedMethodInfo)) {
            return of({
                valid: false,
                error:
                    SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.NO_SELECTION,
            });
        }

        switch (selectedMethodInfo.method) {
            case PAYMENT_METHODS_V2.WALLET:
                return this.validateWalletPaymentInfo();
            case PAYMENT_METHODS_V2.UPI:
                return this.validateUPIPaymentInfo(
                    selectedMethodInfo.paymentInfo,
                    selectedMethodInfo.id !==
                        PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_VPA
                );
            case PAYMENT_METHODS_V2.CARD:
                return this.validateCardPaymentInfo(
                    selectedMethodInfo.paymentInfo,
                    selectedMethodInfo.id !==
                        PAYMENT_METHODS_CUSTOM_OPTIONS.NEW_CARD
                );
            case PAYMENT_METHODS_V2.PREFERRED:
                return this.validatePreferredPaymentInfo(
                    selectedMethodInfo.paymentInfo
                );
            case PAYMENT_METHODS_V2.NETBANKING:
                return this.validateNetbankingPaymentInfo(
                    selectedMethodInfo.paymentInfo
                );
            default:
                return of({
                    valid: false,
                    error:
                        SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN,
                });
        }
    }

    private validateNetbankingPaymentInfo(
        paymentInfo: RazorpayPaymentMethodInfo
    ): Observable<SelectedPaymentInfoValidation> {
        return of({
            valid: !this.utilService.isEmpty(paymentInfo) && !!paymentInfo.bank,
            error: SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN,
        });
    }

    private validatePreferredPaymentInfo(
        paymentInfo: RazorpayPaymentMethodInfo | RazorpayPaymentMethodFormInfo
    ): Observable<SelectedPaymentInfoValidation> {
        switch (paymentInfo.method) {
            case PAYMENT_METHODS_V2.WALLET:
                return this.validateWalletPaymentInfo();
            case PAYMENT_METHODS_V2.UPI:
                return this.validateUPIPaymentInfo(paymentInfo, true);
            case PAYMENT_METHODS_V2.CARD:
                return this.validateCardPaymentInfo(paymentInfo, true);
            case PAYMENT_METHODS_V2.NETBANKING:
                return this.validateNetbankingPaymentInfo(paymentInfo);
            default:
                return of({
                    valid: false,
                    error:
                        SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN,
                });
        }
    }

    private validateWalletPaymentInfo(): Observable<
        SelectedPaymentInfoValidation
    > {
        return of({ valid: true });
    }

    private validateCardPaymentInfo(
        paymentInfo: RazorpayPaymentMethodFormInfo,
        savedOption: boolean
    ): Observable<SelectedPaymentInfoValidation> {
        if (this.utilService.isEmpty(paymentInfo)) {
            return of({
                valid: false,
                error: SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN,
            });
        }

        if (savedOption) {
            return this.validateSavedCard(paymentInfo);
        }

        return this.validateNewCard(paymentInfo);
    }

    private validateSavedCard(
        paymentInfo: RazorpayPaymentMethodFormInfo
    ): Observable<SelectedPaymentInfoValidation> {
        const cvv = Number(paymentInfo["card[cvv]"]);
        const isValid = paymentInfo.validateExpiryAndCvv
            ? paymentInfo["card[cvv]"] && !isNaN(cvv)
            : true;

        /* no need to run through validation if it's a saved vpa, just do a NaN check on CVV except
        for Maestro cards that have been marked as not having an expiry or cvv */
        return of({
            valid: isValid,
            error: !isValid
                ? SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.MISSING_FIELDS
                : null,
        });
    }

    private validateNewCard(
        paymentInfo: RazorpayPaymentMethodFormInfo
    ): Observable<SelectedPaymentInfoValidation> {
        let isValid = !!(
            paymentInfo["card[number]"] && paymentInfo["card[name]"]
        );
        const isCvvAndExpiryValid = !!(
            paymentInfo["card[cvv]"] &&
            paymentInfo["card[expiry_month]"] &&
            paymentInfo["card[expiry_year]"]
        );

        if (!isValid) {
            return of({
                valid: false,
                error:
                    SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.MISSING_FIELDS,
            });
        }

        if (paymentInfo.network === CARD_TYPES.MAESTRO) {
            /* validate cvv and expiry for maestro cards optionally based on checkbox value from form */
            isValid = paymentInfo.validateExpiryAndCvv
                ? isValid && isCvvAndExpiryValid
                : isValid;
        } else {
            isValid = isValid && isCvvAndExpiryValid;
        }

        return of({
            valid: isValid,
            error: !isValid
                ? SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.MISSING_FIELDS
                : null,
        });
    }

    private handlePaymentInfoValidationError(
        error: SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES
    ) {
        if (error && SELECTED_PAYMENT_METHODS_VALIDATION_TOASTS[error]) {
            this.toastService.present(
                SELECTED_PAYMENT_METHODS_VALIDATION_TOASTS[error]
            );
        }
    }

    /* Append meta for type of component to be rendered based on payment method group */
    private getWalletMethodDataV2(
        methodData: PaymentMethodV2Component,
        selectedMethod: number
    ): PaymentMethodV2Component {
        if (!methodData) {
            return;
        }

        const modifiedData = {
            ...methodData,
        };

        /* show only 8 banks upfront */
        if (
            methodData.group === PAYMENT_METHODS_V2.NETBANKING &&
            this.utilService.isLengthyArray(methodData.options)
        ) {
            const selectedBank = methodData.options.find(
                /* no need to select it explicitly if it's already in top 8 as it will be visible
                in the main layout already */
                (option, index) =>
                    option.id === selectedMethod && index > WALLET_BANKS_COUNT
            );

            /* [TODO_RITESH] remove the push selected bank logic once we move from view all page to
            drop down design, this hack enables the user to see the selected bank on the main view */
            if (selectedBank) {
                modifiedData.options = methodData.options.slice(
                    0,
                    WALLET_BANKS_COUNT - 1
                );
                modifiedData.options.push(selectedBank);
            } else {
                modifiedData.options = methodData.options.slice(
                    0,
                    WALLET_BANKS_COUNT
                );
            }
        }

        return {
            ...modifiedData,
            componentMeta: this.getPaymentMethodComponentMeta(
                modifiedData.group
            ),
        };
    }

    private getPaymentMethodComponentMeta(
        group: string
    ): PaymentMethodComponentMeta {
        const defaultComponentMeta = {
            type: "list",
        };
        const componentSettings = this.settingsService.getSettingsValue(
            "paymentMethodsComponentMapping"
        );

        return componentSettings[group] || defaultComponentMeta;
    }

    private getMethodIcon(method: string, selectedMethod: string): string {
        return selectedMethod === method ? "approve_filled" : "approve";
    }
}
