import { WalletPageService } from "./wallet.service";
import { WalletPageAdapter } from "./wallet.adapter";

export const walletPageServices = [WalletPageAdapter, WalletPageService];
