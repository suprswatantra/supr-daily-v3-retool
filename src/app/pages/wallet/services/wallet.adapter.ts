import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Transaction, PaymentInfo } from "@models";
import { SuprApi } from "@types";

import { ApiService } from "@services/data/api.service";
import {
    StoreState,
    WalletStoreActions,
    WalletStoreSelectors,
    RouterStoreSelectors,
    UserStoreSelectors,
    AddressStoreSelectors,
    CartStoreSelectors,
} from "app/store";

@Injectable()
export class WalletPageAdapter {
    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    userCheckoutInfo$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCheckoutInfo)
    );

    balance$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletBalance)
    );

    transactionDetails$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletTransactionDetails)
    );

    transactionList$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletTransactionList)
    );

    walletExpiryDetails$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletExpiryDetails)
    );

    suprCreditsBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectSuprCreditsBalance)
    );

    suprCreditsExpiry$ = this.store.pipe(
        select(WalletStoreSelectors.selectSuprCreditsExpiry)
    );

    isSuprCreditsEnabled$ = this.store.pipe(
        select(UserStoreSelectors.selectIsSuprCreditsEnabled)
    );

    isWalletVersionV2$ = this.store.pipe(
        select(UserStoreSelectors.selectIsWalletVersionV2)
    );

    wallet$ = this.store.pipe(select(WalletStoreSelectors.selectWallet));

    walletMeta$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletMeta)
    );

    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    walletState$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletCurrentState)
    );

    walletError$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletError)
    );

    city$ = this.store.pipe(select(AddressStoreSelectors.selectCity));

    paymentMethods$ = this.store.pipe(
        select(WalletStoreSelectors.selectPaymentMethodsList)
    );

    paymentMethodsV2$ = this.store.pipe(
        select(WalletStoreSelectors.selectPaymentMethodsV2List)
    );

    paymentMethodsVersion$ = this.store.pipe(
        select(WalletStoreSelectors.selectPaymentMethodVersion)
    );

    selectedPaymentMethod$ = this.store.pipe(
        select(WalletStoreSelectors.selectSelectedPaymentMethod)
    );

    selectedPaymentMethodV2$ = this.store.pipe(
        select(WalletStoreSelectors.selectSelectedPaymentMethodV2)
    );

    netBankingOptions$ = this.store.pipe(
        select(WalletStoreSelectors.selectNetBankingOptions)
    );

    cartPaymentCouponCodeInfo$ = this.store.pipe(
        select(CartStoreSelectors.selectCouponCodeInfoForWalletPayment)
    );

    hasFetchedFilteredMethods$ = this.store.pipe(
        select(WalletStoreSelectors.selectFetchedFilteredMethodsStatus)
    );

    constructor(
        private store: Store<StoreState>,
        private apiService: ApiService
    ) {}

    createPayment(data: PaymentInfo) {
        this.store.dispatch(
            new WalletStoreActions.CreatePaymentRequestAction(data)
        );
    }

    captureCustomPayment(data: SuprApi.CustomPaymentCaptureReq) {
        this.store.dispatch(
            new WalletStoreActions.CaptureCustomPaymentRequestAction(data)
        );
    }

    fetchTransactionsList(): Observable<Transaction[]> {
        return this.apiService.fetchWalletTransactions();
    }

    fetchTransactionDetails() {
        this.store.dispatch(
            new WalletStoreActions.FetchTransactionDetailsRequestAction()
        );
    }

    fetchWalletBalance() {
        this.store.dispatch(new WalletStoreActions.LoadBalanceRequestAction());
    }

    applyRechargeCoupon(data: { couponCode: string; amount: number }) {
        this.store.dispatch(
            new WalletStoreActions.ApplyRechargeCouponRequestAction(data)
        );
    }

    fetchRechargeCouponOffers() {
        this.store.dispatch(
            new WalletStoreActions.FetchRechargeCouponOffersRequestAction()
        );
    }

    clearRechargeCoupon() {
        this.store.dispatch(new WalletStoreActions.ClearRechargeCouponAction());
    }

    clearWalletMeta() {
        this.store.dispatch(new WalletStoreActions.ClearWalletMetaAction());
    }

    fetchInitialData(usingCartPaymentCouponCode = false) {
        this.store.dispatch(
            new WalletStoreActions.FetchInitialData({
                usingCartPaymentCouponCode,
            })
        );
    }
}
