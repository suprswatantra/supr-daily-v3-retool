import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

import { WalletPageContainer } from "./containers/wallet.container";
import { WalletPageCoreLayoutComponent } from "./layouts/core.layout";
import { BanksSearchContainer } from "./containers/banks-search.container";
import { TransactionsLayoutComponent } from "./layouts/transactions.layout";
import { TransactionsV2LayoutComponent } from "./layouts/transactions-v2.layout";
import { RechargeSuccessfulContainer } from "./containers/recharge-successful.container";
import { WalletOffersContainer } from "./containers/offers.container";

const walletRoutes: Routes = [
    {
        path: "",
        component: WalletPageCoreLayoutComponent,
        children: [
            {
                path: "",
                component: WalletPageContainer,
            },
            {
                path: PAGE_ROUTES.WALLET.CHILDREN.TRANSACTIONS.PATH,
                component: TransactionsLayoutComponent,
            },
            {
                path: PAGE_ROUTES.WALLET.CHILDREN.TRANSACTIONS_V2.PATH,
                component: TransactionsV2LayoutComponent,
            },
            {
                path: PAGE_ROUTES.WALLET.CHILDREN.THANK_YOU.PATH,
                component: RechargeSuccessfulContainer,
            },
            {
                path: PAGE_ROUTES.WALLET.CHILDREN.BANKS_SEARCH.PATH,
                component: BanksSearchContainer,
            },
            {
                path: PAGE_ROUTES.WALLET.CHILDREN.OFFERS.PATH,
                component: WalletOffersContainer,
                data: { screenName: SCREEN_NAMES.WALLET_OFFERS },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(walletRoutes)],
    exports: [RouterModule],
})
export class WalletRoutingModule {}
