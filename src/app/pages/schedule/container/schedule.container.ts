import { Params } from "@angular/router";
import { OnInit, OnDestroy, Component } from "@angular/core";

import { Observable, Subscription as RxJsSubscription } from "rxjs";
import { map } from "rxjs/operators";

import { CartAdapter } from "@shared/adapters/cart.adapter";
import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import { LOCAL_DB_DATA_KEYS } from "@constants";

import {
    Banner,
    Vacation,
    Feedback,
    Subscription,
    SystemVacation,
} from "@models";

import { DbService } from "@services/data/db.service";
import { DateService } from "@services/date/date.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import {
    CartMini,
    FetchOrdersActionPayload,
    UpdateCalendarActionPayload,
    FetchScheduleActionPayload,
    ScheduleCalendarDictionary,
} from "@types";

import { SchedulePageAdapter } from "../services/schedule.adapter";

@Component({
    selector: "supr-schedule-container",
    template: `
        <supr-schedule-orders-layout
            [vacation]="vacation$ | async"
            [nuxShown]="scheduleNux | async"
            [cartPresent]="cartPresent$ | async"
            [subscriptions]="subscriptions$ | async"
            [systemVacation]="systemVacation$ | async"
            [refreshSchedule]="refreshSchedule$ | async"
            [scheduleCalendar]="scheduleCalendar$ | async"
            [ordersCurrentState]="ordersCurrentState$ | async"
            [orderCalendarBanner]="orderCalendarBanner$ | async"
            [scheduleCurrentState]="scheduleCurrentState$ | async"
            [orderFeedbackRequest]="orderFeedbackRequest$ | async"
            [pastOrderSkuList]="pastOrderSkuListOriginal$ | async"
            [scheduleCalendarDict]="scheduleCalendarDict$ | async"
            [subscriptionCurrentState]="subscriptionCurrentState$ | async"
            (updateNuxKey)="updateNuxKey()"
            (fetchOrders)="fetchOrders($event)"
            (fetchSchedule)="fetchSchedule($event)"
            (fetchSubscriptions)="fetchSubscriptions()"
            (getScheduleCalendarDays)="getScheduleCalendarDays()"
            (updateScheduleCalendarDays)="updateScheduleCalendarDays($event)"
        >
        </supr-schedule-orders-layout>
    `,
})
export class SchedulePageContainer implements OnInit, OnDestroy {
    scheduleNux: Promise<boolean>;
    vacation$: Observable<Vacation>;
    cartPresent$: Observable<boolean>;
    refreshSchedule$: Observable<boolean>;
    scheduleCalendar$: Observable<string[]>;
    ordersCurrentState$: Observable<string>;
    orderCalendarBanner$: Observable<Banner>;
    scheduleCurrentState$: Observable<string>;
    subscriptions$: Observable<Subscription[]>;
    systemVacation$: Observable<SystemVacation>;
    pastOrderSkuListOriginal$: Observable<Array<number>>;
    orderFeedbackRequest$: Observable<Feedback.OrderFeedback[]>;
    scheduleCalendarDict$: Observable<ScheduleCalendarDictionary>;
    subscriptionCurrentState$: Observable<SubscriptionCurrentState>;

    private queryParamSub: RxJsSubscription;

    constructor(
        private dbService: DbService,
        private dateService: DateService,
        private cartAdapter: CartAdapter,
        private scheduleService: ScheduleService,
        private scheduleAdapter: ScheduleAdapter,
        private schedulePageAdapter: SchedulePageAdapter,
        private subscriptionAdapter: SubscriptionAdapter
    ) {
        this.vacation$ = this.schedulePageAdapter.vacation$;
        this.refreshSchedule$ = this.scheduleAdapter.refreshSchedule$;
        this.systemVacation$ = this.schedulePageAdapter.systemVacation$;
        this.subscriptions$ = this.subscriptionAdapter.subscriptionList$;
        this.scheduleCalendar$ = this.schedulePageAdapter.scheduleCalendar$;
        this.ordersCurrentState$ = this.schedulePageAdapter.ordersCurrentState$;
        this.scheduleCurrentState$ = this.scheduleAdapter.scheduleCurrentState$;
        this.scheduleCalendarDict$ = this.schedulePageAdapter.scheduleCalendarDict$;
        this.pastOrderSkuListOriginal$ = this.cartAdapter.pastOrderSkuListOriginal$;
        this.subscriptionCurrentState$ = this.subscriptionAdapter.subscriptionState$;
        this.cartPresent$ = this.cartAdapter.miniCart$.pipe(
            map((miniCart: CartMini) => miniCart.itemCount > 0)
        );
        this.scheduleNux = this.dbService.getData(
            LOCAL_DB_DATA_KEYS.SCHEDULE_NUX_SHOWN
        );
        this.orderCalendarBanner$ = this.cartAdapter.orderCalendarBanner$;
    }

    ngOnInit() {
        this.fetchOrderFeedbackRequest();
        this.scheduleAdapter.getOrderFeedback();
        this.setCaledarCustomDatesFromQueryParams();
    }

    ngOnDestroy() {
        this.unsubscribeQueryParams();
    }

    fetchSchedule(payload: FetchScheduleActionPayload) {
        this.scheduleAdapter.getSchedule(payload);
    }

    fetchOrders(payload: FetchOrdersActionPayload) {
        this.schedulePageAdapter.getOrders(payload);
    }

    getScheduleCalendarDays() {
        this.scheduleCalendar$ = this.schedulePageAdapter.getScheduleCalendar();
    }

    updateScheduleCalendarDays(options: UpdateCalendarActionPayload) {
        this.schedulePageAdapter.updateScheduleCalendarDays(
            options.startDate,
            options.history,
            options.reset
        );
    }

    updateNuxKey() {
        this.dbService.setData(LOCAL_DB_DATA_KEYS.SCHEDULE_NUX_SHOWN, true);
    }

    fetchSubscriptions() {
        this.subscriptionAdapter.fetchSubscriptions();
    }

    private fetchOrderFeedbackRequest() {
        this.orderFeedbackRequest$ = this.scheduleAdapter.orderFeedbackRequest$;
    }

    private setCaledarCustomDatesFromQueryParams() {
        this.unsubscribeQueryParams();
        this.queryParamSub = this.scheduleAdapter.queryParams$.subscribe(
            (params: Params) => {
                const scrollDate = params["scrollToDate"];

                /* check if custom scroll date is set via query params */
                if (
                    scrollDate &&
                    this.dateService.isValidDateString(scrollDate)
                ) {
                    this.scheduleService.setCustomScheduleScrollDate(
                        scrollDate
                    );
                }

                /* check if custom start date is set via query params */
                if (
                    params["startDate"] &&
                    this.dateService.isValidDateString(params["startDate"])
                ) {
                    this.scheduleService.setCustomScheduleStartDate(
                        params["startDate"]
                    );
                } else {
                    /* else fetch start date for the week */
                    this.setCalendarCustomStartDate();
                }
            }
        );
    }

    private setCalendarCustomStartDate() {
        const scrollDate = this.scheduleService.getCustomScheduleScrollDate();

        if (!scrollDate || !this.dateService.isValidDateString(scrollDate)) {
            return;
        }

        const scrollDateObject = this.dateService.dateFromText(scrollDate);
        const startDateObject = this.dateService.startDateOfWeek(
            scrollDateObject
        );
        const startDate = this.dateService.textFromDate(startDateObject);

        this.scheduleService.setCustomScheduleStartDate(startDate);
    }

    private unsubscribeQueryParams() {
        if (this.queryParamSub && !this.queryParamSub.closed) {
            this.queryParamSub.unsubscribe();
        }
    }
}
