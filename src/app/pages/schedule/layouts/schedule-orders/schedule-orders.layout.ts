import {
    Input,
    Output,
    OnInit,
    Component,
    OnChanges,
    OnDestroy,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { Complaintv2Store } from "@store/complaintv2";

import { MODAL_TYPES, FEEDBACK_ANALYTICS_OBJECT_NAMES } from "@constants";

import {
    Vacation,
    Feedback,
    SystemVacation,
    Subscription as SuprSubscription,
    Banner,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { CartService } from "@services/shared/cart.service";
import { CalendarService } from "@services/date/calendar.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { SchedulePageService } from "@pages/schedule/services/schedule.service";

import { OrderCurrentState } from "@store/order/order.state";
import { ScheduleCurrentState } from "@store/schedule/schedule.state";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import {
    Calendar,
    OrderFeedbackSetting,
    FetchOrdersActionPayload,
    FetchScheduleActionPayload,
    ScheduleCalendarDictionary,
    UpdateCalendarActionPayload,
} from "@types";

import {
    CALENDAR_OFFSET,
    CALENDAR_BASE_SIZE,
} from "@pages/schedule/constants/schedule.constants";
import { SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";

@Component({
    selector: "supr-schedule-orders-layout",
    templateUrl: "./schedule-orders.layout.html",
    styleUrls: ["../../styles/schedule.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleOrdersLayoutComponent
    implements OnChanges, OnDestroy, OnInit {
    @Input() nuxShown: boolean;
    @Input() vacation: Vacation;
    @Input() cartPresent: boolean;
    @Input() refreshSchedule: boolean;
    @Input() ordersCurrentState: string;
    @Input() systemVacation: SystemVacation;
    @Input() pastOrderSkuList: Array<number>;
    @Input() subscriptions: SuprSubscription[];
    @Input() scheduleCurrentState: ScheduleCurrentState;
    @Input() orderFeedbackRequest: Feedback.OrderFeedback[];
    @Input() scheduleCalendarDict: ScheduleCalendarDictionary;
    @Input() subscriptionCurrentState: SubscriptionCurrentState;
    @Input() orderCalendarBanner: Banner;

    @Input() set scheduleCalendar(calendar: string[]) {
        this._scheduleCalendar = calendar;
        this.splitCalendar(calendar);
    }
    get scheduleCalendar(): string[] {
        return this._scheduleCalendar;
    }

    @Output() updateNuxKey: EventEmitter<void> = new EventEmitter();
    @Output() fetchSubscriptions: EventEmitter<void> = new EventEmitter();
    @Output() getScheduleCalendarDays: EventEmitter<void> = new EventEmitter();
    @Output()
    fetchSchedule: EventEmitter<
        FetchScheduleActionPayload
    > = new EventEmitter();
    @Output()
    fetchOrders: EventEmitter<FetchOrdersActionPayload> = new EventEmitter();
    @Output()
    updateScheduleCalendarDays: EventEmitter<
        UpdateCalendarActionPayload
    > = new EventEmitter();

    today: Date;
    showShimmers = true;
    loadingMore = false;
    loadingPrevious = false;
    orderCalendar: string[];
    futureCalendar: string[];
    showCalendarModal = false;
    showOrderFeedback = false;
    _scheduleCalendar: string[];
    orderStates = OrderCurrentState;
    calendarStartDate: Calendar.DayData;
    scheduleStates = ScheduleCurrentState;
    calendarSelectedDate: Calendar.DayData;
    subscriptionStates = SubscriptionCurrentState;
    orderFeedbackObject: Feedback.FeedbackRequestObject = {};
    showOrderCalendarBanner = false;

    saObjectNames = {
        ...SA_OBJECT_NAMES,
        ...FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION,
    };
    calendarModalType = MODAL_TYPES.NO_WRAPPER;
    order_feedback: OrderFeedbackSetting.Nudge;

    private routerSub: Subscription;

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private dateService: DateService,
        private cartService: CartService,
        private utilService: UtilService,
        private calendarService: CalendarService,
        private suprDateService: SuprDateService,
        private scheduleService: ScheduleService,
        private feedbackService: FeedbackService,
        private schedulePageService: SchedulePageService,
        private routerService: RouterService,
        private complaintv2Store: Complaintv2Store
    ) {
        this.initRouterEvents();
    }

    ngOnInit() {
        this.getSubscriptionsData();
        this.feedbackService.fetchOrderFeedbackSettings();
        this.showOrderFeedback = this.scheduleService.getOrderFeedback();
        this.order_feedback = this.feedbackService.getOrderFeedbackSettings();
        this.showOrderCalendarBanner = this.canShowOrderCalendarBanner();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleScheduleRefresh(changes["refreshSchedule"]);
        this.handleStatusChange(
            changes["scheduleCurrentState"],
            changes["ordersCurrentState"],
            changes["subscriptionCurrentState"]
        );
        this.handleCalendarScrollState(changes["scheduleCalendar"]);
        this.handleOrderFeedback(changes["orderFeedbackRequest"]);
        this.handleOrderCalendarBannerChange(changes["orderCalendarBanner"]);
    }

    ngOnDestroy() {
        this.scheduleService.setOrderFeedback(false);

        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    handleOrderFeedback(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            this.utilService.isLengthyArray(change.currentValue) &&
            change.previousValue !== change.currentValue
        ) {
            this.initializeFeedbackObject();
        }
    }

    hideOrderCalendarBanner() {
        this.showOrderCalendarBanner = false;
    }

    private canShowOrderCalendarBanner(): boolean {
        if (!this.orderCalendarBanner) {
            return false;
        }

        const previousUrl = this.routerService.getPreviousUrl();

        if (!previousUrl) {
            return false;
        }

        if (this.scheduleService.isThankYouPageUrl(previousUrl)) {
            return true;
        }

        return false;
    }

    private handleOrderCalendarBannerChange(change: SimpleChange) {
        if (!this.canHandleChange(change)) {
            return;
        }

        this.showOrderCalendarBanner = this.canShowOrderCalendarBanner();
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private getSubscriptionsData() {
        if (!this.utilService.isLengthyArray(this.subscriptions)) {
            this.fetchSubscriptions.emit();
        }
    }

    private extractAction(request: any, response: any) {
        let isRequest = false;
        let isResponse = false;
        let feedbackAction = {};
        let feedback_request_id: any;

        if (!this.utilService.isEmpty(response)) {
            isResponse = true;
            feedbackAction = {
                feedback_value: response.feedback_value,
            };
            feedback_request_id = response.feedback_request_id;
        } else {
            isRequest = true;
            const { params = [], feedback_request_id: feedbackId } = request;
            feedbackAction = {
                params,
            };
            feedback_request_id = feedbackId;
        }

        return {
            isResponse,
            isRequest,
            feedback_request_id,
            feedbackAction,
        };
    }

    private initializeFeedbackObject() {
        if (
            this.orderFeedbackRequest &&
            this.utilService.isLengthyArray(this.orderFeedbackRequest) &&
            this.order_feedback.enabled
        ) {
            this.orderFeedbackObject = this.orderFeedbackRequest.reduce(
                (feedbackObj, feedback: Feedback.OrderFeedback) => {
                    const {
                        feedback_date,
                        feedback_entity_id,
                        collect_feedback_request,
                        feedback_response,
                    } = feedback;

                    const {
                        isResponse,
                        isRequest,
                        feedback_request_id,
                        feedbackAction,
                    } = this.extractAction(
                        collect_feedback_request,
                        feedback_response
                    );
                    if (feedbackObj[feedback_date]) {
                        const dateFeedback = feedbackObj[feedback_date];
                        dateFeedback[feedback_entity_id] = {
                            feedbackAction,
                            isRequest,
                            isResponse,
                            feedback_request_id,
                        };
                        return {
                            ...feedbackObj,
                        };
                    } else {
                        return {
                            ...feedbackObj,
                            [feedback_date]: {
                                [feedback_entity_id]: {
                                    feedbackAction,
                                    isRequest,
                                    isResponse,
                                    feedback_request_id,
                                },
                            },
                        };
                    }
                },
                {}
            );
        }
    }

    trackOrdersFn(index: number): string {
        if (
            Array.isArray(this.orderCalendar) &&
            this.orderCalendar.length > 0
        ) {
            return this.orderCalendar[index];
        }
    }

    trackSchedulesFn(index: number): string {
        if (
            Array.isArray(this.futureCalendar) &&
            this.futureCalendar.length > 0
        ) {
            return this.futureCalendar[index];
        }
    }

    onHeaderMonthClick() {
        this.showCalendarModal = true;
    }

    loadPrevious() {
        this.scheduleService.setOrderFeedback(false);
        this.showOrderFeedback = false;
        if (this._scheduleCalendar && this._scheduleCalendar.length > 0) {
            this.loadingMore = false;
            const currentDate = this.dateService.dateFromText(
                this._scheduleCalendar[0]
            );

            const renderOrders =
                this.dateService.daysFromToday(currentDate) <= CALENDAR_OFFSET
                    ? true
                    : false;

            /* Check if the previous 7 days will require order history data or only
            render of schedule calendar days */
            if (renderOrders) {
                this.loadOrders(currentDate);
                const previousDate = this.dateService.addDays(currentDate, -1);
                this.loadComplaints(previousDate);
            } else {
                this.updateScheduleCalendarDays.emit({
                    history: true,
                });
            }
        }
    }

    loadOrders(currentDate: Date) {
        const previousDate = this.dateService.addDays(currentDate, -1);
        const dateText = this.dateService.textFromDate(previousDate);

        this.fetchOrders.emit({ toDate: dateText });
    }

    loadComplaints(currentDate: Date, days?: number) {
        const previousDate = this.dateService.addDays(currentDate, -1);
        const dateText = this.dateService.textFromDate(previousDate);
        this.getComplaints(dateText, days, true);
    }

    loadMore() {
        this.loadingMore = true;
        this.updateScheduleCalendarDays.emit({});
    }

    onCalendarDateChange(date: string) {
        this.loadingMore = false;
        this.closeCalendarModal();

        if (!this.scheduleCalendarDict[date]) {
            this.updateScheduleCalendarDays.emit({
                reset: true,
                startDate: date,
                numberOfDays: CALENDAR_BASE_SIZE,
            });
            this.scheduleService.scrollScheduleWrapper(0);
        } else {
            this.scheduleService.scrollScheduleIntoView(date);
        }

        const dateObject = this.dateService.dateFromText(date);
        this.calendarSelectedDate = this.suprDateService.suprDate(dateObject);
    }

    closeCalendarModal() {
        this.showCalendarModal = false;
        this.cdr.detectChanges();
    }

    isSchedulePaused(date: string): boolean {
        if (!this.vacation) {
            return false;
        }

        return this.scheduleService.isDateInVacation(this.vacation, date);
    }

    isSchedulePausedBySystem(date: string): boolean {
        if (!this.systemVacation) {
            return false;
        }

        return this.scheduleService.isDateInVacation(this.systemVacation, date);
    }

    updateActionListener(date: string) {
        if (date && this.dateService.isValidDateString(date)) {
            const dateObject = this.dateService.dateFromText(date);
            const scheduleStartDateObject = this.dateService.startDateOfWeek(
                dateObject
            );
            const scheduleStartDate = this.dateService.textFromDate(
                scheduleStartDateObject
            );

            this.scheduleService.setCustomScheduleScrollDate(date);
            this.scheduleService.setCustomScheduleStartDate(scheduleStartDate);
        }
    }

    private fetchInitialSchedule(startDate?: string) {
        let _startDate = startDate,
            numberOfDays = CALENDAR_OFFSET;
        const thankYouFlag = this.scheduleService.getThankYouFlag();
        const latestOrderDate = this.calendarService.getLatestOrderDate();
        const latestOrderDateDaysFromToday = this.dateService.daysFromToday(
            latestOrderDate
        );
        /* latestOrderDateDaysFromToday would be 1 if latestOrderDate is tomorrow,
        happens if today's cutoff time has crossed */
        const offsetAdjustmentDays = latestOrderDateDaysFromToday === 0 ? 1 : 2;
        this.scheduleService.setThankYouFlag(false);

        if (startDate) {
            /* Order Calendar flow from home page */
            ({
                startDate: _startDate,
                numberOfDays,
            } = this.getOrdersForHomePageFlow(
                startDate,
                numberOfDays,
                offsetAdjustmentDays
            ));
        } else if (!thankYouFlag) {
            /* Don't render today's order if calendar isn't starting from a specific date
            This logic works only because we only allow navigation to future dates via
            these flows. No support for historical dates */
            this.getInitialOrders();

            /* Reduce number of days for Schedule Data by as many days for which order data
            is being rendered */

            /* orderDateDaysFromToday would be 1 when we have crossed hub cutoff time, in this
            case we render 2 days of order data, hence reducing schedule view by 2 days */
            numberOfDays = CALENDAR_OFFSET - offsetAdjustmentDays;
        }

        this.fetchSchedule.emit({
            thankYouFlag,
            numberOfDays,
            startDate: _startDate,
        });
    }

    private getComplaints(
        date: string,
        days?: number,
        append: boolean = false
    ) {
        this.complaintv2Store.getComplaintsByOrders(date, days, append);
    }

    private getOrdersForHomePageFlow(
        startDate: string,
        numberOfDays: number,
        offsetAdjustmentDays: number
    ): { startDate: string; numberOfDays: number } {
        let scheduleStartDate = startDate,
            _numberOfDays = numberOfDays;

        const today = this.dateService.getTodayDate();
        /* OrderAction toDate should be today if time is before cutoff time else tomorrow */
        const orderToDate = this.dateService.addDays(
            today,
            offsetAdjustmentDays - 1
        );
        const daysFromToday = this.dateService.daysFromToday(
            this.dateService.dateFromText(startDate)
        );

        if (daysFromToday <= 1) {
            this.loadingPrevious = true;
            /* If the startDate is historical, set the schedule start date to Today + 1.
                If cutoff time has crossed, then start date would be Today + 2 */
            scheduleStartDate = this.dateService.textFromDate(
                this.dateService.addDays(today, offsetAdjustmentDays)
            );

            /* In order to always load 7 days at once, reduce the schedule days by number of
                days for which you are showing order data */
            _numberOfDays =
                CALENDAR_BASE_SIZE -
                Math.abs(daysFromToday) -
                (offsetAdjustmentDays - 1);

            const toDate = this.dateService.textFromDate(orderToDate);
            const days = Math.abs(daysFromToday) + offsetAdjustmentDays;

            this.fetchOrders.emit({
                firstLoad: true,
                toDate,
                days,
            });

            this.getComplaints(toDate, days);
        }

        return {
            numberOfDays: _numberOfDays,
            startDate: scheduleStartDate,
        };
    }

    private setDateInfo() {
        this.today = this.dateService.getTodayDate();
        this.calendarStartDate = this.suprDateService.suprDate(this.today);
        this.calendarSelectedDate = this.calendarStartDate;
    }

    private initRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isSchedulePageUrl(event.url)
                )
            )
            .subscribe(() => {
                this.init();
            });
    }

    private init() {
        const customStartDate = this.scheduleService.getCustomScheduleStartDate();

        this.cartService.clearDeliveryDate();
        this.scheduleService.setCustomScheduleStartDate(null);
        this.setDateInfo();
        this.fetchInitialSchedule(customStartDate);

        if (!this._scheduleCalendar || this._scheduleCalendar.length === 0) {
            this.getScheduleCalendarDays.emit();
        }

        if (!this.nuxShown) {
            this.updateNuxKey.emit();
        }
    }

    private isSchedulePageUrl(url: string): boolean {
        return url && url.indexOf("schedule") > -1;
    }

    private setLoadingState() {
        this.showShimmers =
            this.ordersCurrentState === this.orderStates.FETCHING_ORDERS ||
            this.scheduleCurrentState ===
                this.scheduleStates.FETCHING_SCHEDULE ||
            this.subscriptionCurrentState ===
                this.subscriptionStates.FETCHING_SUBSCRIPTIONS;
    }

    private getInitialOrders() {
        const orderDate = this.calendarService.getLatestOrderDate();
        const daysFromToday = this.dateService.daysFromToday(orderDate);

        /* daysFromToday would be 1 if we have already crossed cutoff time
        as orderDate would be that of tomorrow, so we need to load 2 days */
        const days = daysFromToday === 0 ? 1 : 2;
        const toDate = this.dateService.textFromDate(orderDate);
        this.fetchOrders.emit({
            days,
            firstLoad: true,
            toDate,
        });

        this.getComplaints(toDate, days);
    }

    private handleScheduleRefresh(refreshScheduleChange: SimpleChange) {
        if (
            refreshScheduleChange &&
            !refreshScheduleChange.firstChange &&
            refreshScheduleChange.currentValue &&
            refreshScheduleChange.currentValue !==
                refreshScheduleChange.previousValue
        ) {
            setTimeout(() => {
                const customStartDate = this.scheduleService.getCustomScheduleStartDate();
                this.scheduleService.setCustomScheduleStartDate(null);

                if (!customStartDate) {
                    this.getInitialOrders();
                }

                this.fetchInitialSchedule(customStartDate);
            }, 0);
        }
    }

    private handleStatusChange(
        scheduleStatusChange: SimpleChange,
        orderStatusChange: SimpleChange,
        subscriptionStatusChange: SimpleChange
    ) {
        if (
            (orderStatusChange && !orderStatusChange.firstChange) ||
            (scheduleStatusChange && !scheduleStatusChange.firstChange) ||
            (subscriptionStatusChange && !subscriptionStatusChange.firstChange)
        ) {
            this.setLoadingState();
        }
    }

    private handleCalendarScrollState(calendarChange: SimpleChange) {
        /* loading more days flow */
        if (calendarChange && !calendarChange.firstChange && this.loadingMore) {
            this.loadingMore = false;

            setTimeout(() => {
                this.scheduleService.scrollScheduleIntoView(
                    this._scheduleCalendar[
                        this._scheduleCalendar.length - CALENDAR_OFFSET
                    ]
                );
            }, 0);
        } else {
            /* Custom scroll to date flow; scroll to the clicked date after loading
            of orders and schedule calendar is complete */
            this.handleCustomDateScrollState(calendarChange);
        }
    }

    private getScrollOffset(): number {
        let offset = (this.showOrderFeedback && 96) || 0;
        offset = offset + ((!this.nuxShown && 84) || 0);
        return offset || 0;
    }

    private handleCustomDateScrollState(calendarChange: SimpleChange) {
        if (
            calendarChange &&
            !calendarChange.firstChange &&
            this.ordersCurrentState === this.orderStates.FETCHING_ORDERS_DONE &&
            this.scheduleCurrentState ===
                this.scheduleStates.UPDATING_CALENDAR_DONE
        ) {
            this.loadingPrevious = false;
            const scrollToDate = this.scheduleService.getCustomScheduleScrollDate();
            this.scheduleService.setCustomScheduleScrollDate(null);
            if (scrollToDate) {
                setTimeout(() => {
                    this.scheduleService.scrollScheduleIntoView(
                        scrollToDate,
                        this.getScrollOffset()
                    );
                }, 300);
            }
        }
    }

    private splitCalendar(calendar: string[]) {
        const {
            orderCalendar,
            futureCalendar,
        } = this.schedulePageService.splitCalendar(calendar);

        this.orderCalendar = orderCalendar;
        this.futureCalendar = futureCalendar;
    }
}
