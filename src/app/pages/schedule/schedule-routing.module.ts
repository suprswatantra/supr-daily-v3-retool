import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { SchedulePageContainer } from "./container/schedule.container";

const scheduleRoutes: Routes = [
    {
        path: "",
        component: SchedulePageContainer,
    },
];

@NgModule({
    imports: [RouterModule.forChild(scheduleRoutes)],
    exports: [RouterModule],
})
export class ScheduleRoutingModule {}
