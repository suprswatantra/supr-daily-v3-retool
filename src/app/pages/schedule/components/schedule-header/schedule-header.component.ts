import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

import { Vacation } from "@models";
import { StoreService } from "@services/data/store.service";

import { TEXTS } from "@pages/schedule/constants/schedule.constants";
import { SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";

@Component({
    selector: "supr-schedule-orders-header",
    templateUrl: "./schedule-header.component.html",
    styleUrls: ["../../styles/schedule.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleHeaderComponent {
    saObjectName = SA_OBJECT_NAMES.CLICK.PAUSE_ORDERS;

    @Input() year: number;
    @Input() month: string;
    @Input() vacation: Vacation;
    @Output() handleMonthClick: EventEmitter<any> = new EventEmitter();

    pauseOrdersText = TEXTS.PAUSE_ORDERS_TEXT;
    pauseActiveText = TEXTS.PAUSE_ACTIVE_TEXT;

    constructor(private storeService: StoreService) {}

    openPauseModal() {
        this.storeService.togglePauseModal();
    }
}
