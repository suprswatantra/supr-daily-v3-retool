import { ScheduleHeaderComponent } from "./schedule-header/schedule-header.component";
import { ScheduleCalendarComponent } from "./schedule-calendar/schedule-calendar.component";

export const SchedulePageComponents = [
    ScheduleHeaderComponent,
    ScheduleCalendarComponent,
];
