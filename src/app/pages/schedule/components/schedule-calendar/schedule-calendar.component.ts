import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SuprDateService } from "@services/date/supr-date.service";

import { ModalComponent } from "@shared/components/supr-modal/supr-modal.component";

import { Calendar } from "@types";

@Component({
    selector: "supr-schedule-calendar",
    templateUrl: "./schedule-calendar.component.html",
    styleUrls: ["../../styles/schedule.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleCalendarComponent {
    @Input() year: number;
    @Input() month: string;
    @Input() suprModalRef: ModalComponent;
    @Input() selectedDate: Calendar.DayData;

    @Output() handleDateChange: EventEmitter<string> = new EventEmitter();

    constructor(private suprDateService: SuprDateService) {}

    onDateChange(day: Calendar.DayData) {
        this.handleDateChange.emit(day.dateText);
    }

    onMonthChange(day: Calendar.DayData) {
        this.year = day.year;
        this.month = this.suprDateService.getMonthFromDate(day);
    }
}
