import { Injectable } from "@angular/core";

import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";

@Injectable()
export class SchedulePageService {
    constructor(
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    splitCalendar(
        calendar: string[]
    ): { orderCalendar: string[]; futureCalendar: string[] } {
        if (!calendar || !calendar.length) {
            return {
                orderCalendar: [],
                futureCalendar: [],
            };
        }

        const today = this.calendarService.getLatestOrderDate();
        const todayText = this.dateService.textFromDate(today);
        const todayIndex = calendar.indexOf(todayText);

        if (todayIndex >= 0) {
            return {
                orderCalendar: calendar.slice(0, todayIndex + 1),
                futureCalendar: calendar.slice(todayIndex + 1),
            };
        }

        /* Use Case: When the calendar is either before of after today */
        const daysFromToday = this.dateService.daysFromToday(
            this.dateService.dateFromText(calendar[0])
        );

        if (daysFromToday > 0) {
            return {
                orderCalendar: [],
                futureCalendar: calendar,
            };
        } else {
            return {
                orderCalendar: calendar,
                futureCalendar: [],
            };
        }
    }
}
