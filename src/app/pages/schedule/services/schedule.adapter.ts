import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import {
    StoreState,
    OrderStoreActions,
    OrderStoreSelectors,
    ScheduleStoreActions,
    RouterStoreSelectors,
    ScheduleStoreSelectors,
    VacationStoreSelectors,
} from "@supr/store";

import { Schedule } from "@models";
import { FetchOrdersActionPayload } from "@types";

import { CALENDAR_OFFSET } from "../constants/schedule.constants";

@Injectable()
export class SchedulePageAdapter {
    schedule$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleList)
    );
    scheduleCalendar$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleCalendar)
    );
    scheduleCalendarDict$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleCalendarDictionary)
    );

    ordersCurrentState$ = this.store.pipe(
        select(OrderStoreSelectors.selectOrdersCurrentState)
    );

    vacation$ = this.store.pipe(select(VacationStoreSelectors.selectVacation));

    systemVacation$ = this.store.pipe(
        select(VacationStoreSelectors.selectSystemVacation)
    );

    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    constructor(private store: Store<StoreState>) {}

    getScheduleByDate(date: string): Observable<Schedule> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectScheduleByDate, { date })
        );
    }

    getScheduleCalendar(): Observable<string[]> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectScheduleCalendar)
        );
    }

    updateScheduleCalendarDays(
        startDate?: string,
        history?: boolean,
        reset?: boolean
    ) {
        this.store.dispatch(
            new ScheduleStoreActions.UpdateScheduleCalendarAction({
                reset,
                history,
                startDate,
                numberOfDays: CALENDAR_OFFSET,
            })
        );
    }

    getOrders(payload: FetchOrdersActionPayload) {
        this.store.dispatch(
            new OrderStoreActions.GetOrdersDataAction({
                toDate: payload.toDate,
                firstLoad: payload.firstLoad,
                days: payload.days || CALENDAR_OFFSET,
            })
        );
    }
}
