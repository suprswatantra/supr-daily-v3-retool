export const CALENDAR_OFFSET = 7;
export const CALENDAR_BASE_SIZE = 6;

export const TEXTS = {
    PAUSE_ORDERS_TEXT: "Set vacation",
    PAUSE_ACTIVE_TEXT: "Vacation active",
};

export const SCHEDULE_DAYS = {
    TODAY: "Today",
    TOMORROW: "Tomorrow",
};
