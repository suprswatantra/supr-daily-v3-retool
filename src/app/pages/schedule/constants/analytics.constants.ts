export const SA_OBJECT_NAMES = {
    CLICK: {
        CHANGE_BUTTON: "change",
        CHANGE_DATE: "change-date",
        MODAL_DONE: "done-button",
        PRODUCT_TILE: "product-tile",
        PAUSE_ORDERS: "pause-orders",
        DELIVER_ONCE: "deliver-once",
        RECHARGE_PLAN: "recharge-plan",
        CANCEL_DELIVERY: "cancel-delivery",
        VIEW_PREVIOUS: "tap-to-view-previous",
        ADJUSTMENT_DONE: "subscription-adjusted",
        MANAGE_SUBSCRIPTION: "manage-subscription",
        REFUND_TOOLTIP: "cancellation-info-tooltip",
        CONFIRM_CANCEL_DELIVERY: "confirm-cancel-delivery",
    },
    IMPRESSION: {
        CONFIRM_PAUSE: "confirm-pause",
        CONFIRM_RESUME: "confirm-resume",
    },
};

export const MODAL_NAMES = {
    EDIT_ADDON_DELIVERY: "edit-addon-delivery",
    EDIT_SUBSCRIPTION_DELIVERY: "edit-subscription-delivery",
};
