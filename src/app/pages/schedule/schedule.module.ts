import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { MobxAngularModule } from "mobx-angular";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";
import { ScheduleRoutingModule } from "./schedule-routing.module";

import { SchedulePageService } from "./services/schedule.service";
import { SchedulePageAdapter } from "./services/schedule.adapter";

import { SchedulePageContainer } from "./container/schedule.container";

import { ScheduleOrdersLayoutComponent } from "./layouts/schedule-orders/schedule-orders.layout";

import { SchedulePageComponents } from "./components";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        ScheduleRoutingModule,
        MobxAngularModule,
    ],
    declarations: [
        ScheduleOrdersLayoutComponent,
        ...SchedulePageComponents,
        SchedulePageContainer,
    ],
    providers: [SchedulePageService, SchedulePageAdapter],
})
export class SchedulePageModule {}
