import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import {
    StoreState,
    CartStoreActions,
    WalletStoreActions,
    SubscriptionStoreActions,
} from "@supr/store";

@Injectable()
export class ThankyouPageAdapter {
    constructor(private store: Store<StoreState>) {}

    clearCart() {
        this.store.dispatch(new CartStoreActions.ClearCartAction());
    }

    fetchBalance() {
        this.store.dispatch(new WalletStoreActions.LoadBalanceRequestAction());
    }

    fetchSubscriptions() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }
}
