import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

import { CartItem, CartMeta, CartOrder } from "@models";

import { UserCurrentState } from "@store/user/user.state";

import { UserAdapter } from "@shared/adapters/user.adapter";

@Component({
    selector: "supr-thankyou-container",
    template: `
        <supr-thankyou-layout
            [cartId]="cartId$ | async"
            [cartMeta]="cartMeta$ | async"
            [cartItems]="cartItems$ | async"
            [userState]="userState$ | async"
            [whatsAppOptIn]="whatsAppOptIn$ | async"
            [whatsAppUpdateError]="userError$ | async"
            [isFirstPurchase]="isFirstPurchase$ | async"
            [cartOrder]="cartOrder$ | async"
            (clearCart)="clearCart()"
            (fetchBalance)="fetchBalance()"
            (fetchSubscriptions)="fetchSubscriptions()"
            (handleUpdateWhatsApp)="updateWhatsAppStatus($event)"
            [isAlternateDeliveryEligible]="isAlternateDeliveryEligible$ | async"
        ></supr-thankyou-layout>
    `,
})
export class ThankyouPageContainer implements OnInit {
    userError$: Observable<any>;
    cartId$: Observable<string>;
    cartMeta$: Observable<CartMeta>;
    cartOrder$: Observable<CartOrder>;
    cartItems$: Observable<CartItem[]>;
    whatsAppOptIn$: Observable<boolean>;
    isFirstPurchase$: Observable<boolean>;
    userState$: Observable<UserCurrentState>;
    isAlternateDeliveryEligible$: Observable<boolean>;

    constructor(private adapter: Adapter, private userAdapter: UserAdapter) {}

    ngOnInit() {
        this.cartId$ = this.adapter.cartId$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.cartItems$ = this.adapter.cartItems$;
        this.userState$ = this.userAdapter.userState$;
        this.userError$ = this.userAdapter.userError$;
        this.isFirstPurchase$ = this.adapter.isFirstPurchase$;
        this.whatsAppOptIn$ = this.userAdapter.whatsAppOptIn$;
        this.isAlternateDeliveryEligible$ = this.adapter.isAlternateDeliveryEligible$;
        this.cartOrder$ = this.adapter.cartOrder$;
    }

    clearCart() {
        this.adapter.clearCart();
    }

    fetchBalance() {
        this.adapter.fetchBalance();
    }

    fetchSubscriptions() {
        this.adapter.fetchSubscriptions();
    }

    updateWhatsAppStatus(isWhatsAppOptIn: boolean) {
        this.userAdapter.updateUser({ isWhatsAppOptIn });
    }
}
