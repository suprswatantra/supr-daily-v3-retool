import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    AUDIO_KEYS,
    MODAL_NAMES,
    TOAST_MESSAGES,
    CART_ITEM_TYPES,
    RATING_NUDGE_TRIGGERS,
    ECOMMERCE_ANALYTICS_EVENTS,
} from "@constants";

import { CartItem, CartMeta, CartOrder, PostCheckoutContent } from "@models";

import { AudioService } from "@services/util/audio.service";
import { UtilService } from "@services/util/util.service";
import { NudgeService } from "@services/layout/nudge.service";
import { RouterService } from "@services/util/router.service";
import { OrderService } from "@services/shared/order.service";
import { ToastService } from "@services/layout/toast.service";
import { SettingsService } from "@services/shared/settings.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { MoengageService } from "@services/integration/moengage.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { UserCurrentState } from "@store/user/user.state";

import { Ecommerce, OrderSuccessADModalConfig } from "@types";

import { ANIMATION_INTERVAL } from "@pages/cart/constants";

@Component({
    selector: "supr-thankyou-layout",
    templateUrl: "./thankyou.layout.html",
    styleUrls: ["../styles/thankyou.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThankyouPageLayoutComponent implements OnInit, OnChanges {
    @Input() cartId: string;
    @Input() cartMeta: CartMeta;
    @Input() cartItems: CartItem[];
    @Input() cartOrder: CartOrder;
    @Input() whatsAppOptIn: boolean;
    @Input() whatsAppUpdateError: any;
    @Input() isFirstPurchase: boolean;
    @Input() userState: UserCurrentState;
    @Input() isAlternateDeliveryEligible: boolean;

    @Output() clearCart: EventEmitter<void> = new EventEmitter();
    @Output() handleUpdateWhatsApp: EventEmitter<boolean> = new EventEmitter();

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    _showADModal = false;
    _modalNames = MODAL_NAMES;
    _updatingWhatsapp = false;
    _showWhatsappModal = false;
    _firstDeliveryDate: string;
    _adModalConfig: OrderSuccessADModalConfig;
    postCheckoutContent: PostCheckoutContent;
    checkoutAnimationDone: boolean;

    constructor(
        private audioService: AudioService,
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private toastService: ToastService,
        private orderService: OrderService,
        private nudgeService: NudgeService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private scheduleService: ScheduleService,
        private moengageService: MoengageService,
        private appsFlyerService: AppsFlyerService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.audioService.play(AUDIO_KEYS.PAYMENT_DELIVERY_CONFIRMATION_AUDIO);
        this.fireOrderCompletedEvent();
        this.sendAnalyticsEvents();
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["userState"];

        if (this.canHandleChange(change)) {
            this.handleProfileChange(change);
        }

        if (this.canHandleChange(changes["cartOrder"])) {
            this.initialize();
        }
    }

    /**
     * Route to order calendar page after clearing out cart store.
     * Send rating nudge if required
     *
     * @memberof ThankyouPageLayoutComponent
     */
    routeToSchedulePage() {
        this.scheduleService.setThankYouFlag(true);
        this.routerService.goToSchedulePage({
            replaceUrl: true,
        });
        this.clearCart.emit();
        this.sendRatingNudge();
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private handleProfileChange(change: SimpleChange) {
        if (
            change.previousValue === UserCurrentState.UPDATING_USER &&
            change.currentValue === UserCurrentState.NO_ACTION
        ) {
            if (!this.whatsAppUpdateError) {
                this.handleUpdateWhatsappOptInSuccess();
            } else {
                this.handleUpdateWhatsappOptInError();
            }
            this._updatingWhatsapp = false;
            this.routeToSchedulePage();
        }
    }

    private handleUpdateWhatsappOptInSuccess() {
        this.showWhatsappSuccessToast();
    }

    private handleUpdateWhatsappOptInError() {
        this.showWhatsappErrorToast();
    }

    private showWhatsappSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.WHATSAPP_OPT_IN_ORDER.SUCCESS);
    }

    private showWhatsappErrorToast() {
        this.toastService.present(TOAST_MESSAGES.WHATSAPP_OPT_IN_ORDER.ERROR);
    }

    private initialize() {
        if (!this.cartOrder) {
            return;
        }

        const postCheckoutContent: PostCheckoutContent[] = this.utilService.getNestedValue(
            this.cartOrder,
            "postCheckoutContent",
            null
        );

        if (!this.utilService.isLengthyArray(postCheckoutContent)) {
            setTimeout(() => {
                this.handlePostOrder();
            }, ANIMATION_INTERVAL);
            return;
        }

        postCheckoutContent.forEach((el: PostCheckoutContent, index) => {
            this.startAnimation(el, index);
        });

        setTimeout(() => {
            this.handlePostOrder();
        }, ANIMATION_INTERVAL * (postCheckoutContent.length + 1));
        return;
    }

    private handlePostOrder() {
        this.orderService
            .canShowADModal(this.isAlternateDeliveryEligible, this.cartItems)
            .then((show: boolean) => {
                if (show) {
                    this.enableADModal();
                } else {
                    this.checkForWhatsappModal();
                }
            })
            .catch(() => {
                this.checkForWhatsappModal();
            });
    }

    /**
     * Start the fadein out and slide in out animation
     * Using ngIf rerendering the dom element to trigger multiple animations
     *
     * @private
     * @param {PostCheckoutContent} el
     * @param {number} index
     * @memberof ThankyouPageLayoutComponent
     */
    private startAnimation(el: PostCheckoutContent, index: number) {
        setTimeout(() => {
            this.checkoutAnimationDone = true;
            this.postCheckoutContent = null;
            this.cdr.detectChanges();
            this.postCheckoutContent = el;
            this.checkAndSetAnimateNumberValues(el);
            this.cdr.detectChanges();
        }, (index + 1) * ANIMATION_INTERVAL);
    }

    private checkAndSetAnimateNumberValues(el: PostCheckoutContent) {
        const animateNumValues: number[] = this.utilService.getNestedValue(
            el,
            "middleFoldText.animationNumValues",
            []
        );

        if (!this.utilService.isLengthyArray(animateNumValues)) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--start-num",
            animateNumValues[0]
        );

        this.wrapperEl.nativeElement.style.setProperty(
            "--end-num",
            animateNumValues[1]
        );
    }

    private checkForWhatsappModal() {
        this.orderService
            .canShowWhatsappModal(this.whatsAppOptIn)
            .then((show: boolean) => {
                if (show) {
                    this.enableWhatsappModal();
                    this.orderService.updateWhatsappModalImpression();
                    this.cdr.detectChanges();
                } else {
                    this.routeToSchedulePage();
                }
            })
            .catch(() => {
                this.routeToSchedulePage();
            });
    }

    private enableWhatsappModal() {
        this._showWhatsappModal = true;
    }

    private enableADModal() {
        this._showADModal = true;
        this._adModalConfig = this.settingsService.getSettingsValue(
            "orderSuccessADModal"
        );

        const firstADDelivery = this.orderService.getFirstADDeliveryFromCart(
            this.cartItems
        );

        if (firstADDelivery) {
            this._firstDeliveryDate = firstADDelivery.delivery_date;
        }

        this.orderService.updateADModalImpression();
        this.cdr.detectChanges();
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.ORDER_COMPLETED
        );
    }

    private fireOrderCompletedEvent() {
        const data = this.getOrderCompletedInfo();
        this.analyticsService.trackCustom(
            ECOMMERCE_ANALYTICS_EVENTS.ORDER_COMPLETED,
            data
        );
    }

    private getOrderCompletedInfo(): Ecommerce.OrderPlaced {
        return {
            order_id: this.cartId,
            cart_type: this.getCartType(),
            total: this.getTotalAmount(),
            discount: this.getDiscountAmount(),
            discount_type: this.getDiscountType(),
            benefit_source: this.getBenefitSource(),
            product_ids: this.cartItems.map((item) => item.sku_id),
            product_count: this.cartItems.length,
            coupon: this.getCouponCode(),
            isSubscriptionRechargePresent: this.isSubscriptionRechargePresent(),
        };
    }

    private getCartType(): Ecommerce.CartType {
        const types = {};
        this.cartItems.forEach((item) => {
            types[item.type] = true;
        });

        const addonPresent = !!types[CART_ITEM_TYPES.ADDON];
        const subscriptionPresent = !!types[CART_ITEM_TYPES.SUBSCRIPTION_NEW];
        if (addonPresent && subscriptionPresent) {
            return Ecommerce.CartType.BOTH;
        } else if (addonPresent) {
            return Ecommerce.CartType.DELIVER_ONCE;
        } else if (subscriptionPresent) {
            return Ecommerce.CartType.SUBSCRIPTION;
        }
    }

    private isSubscriptionRechargePresent(): boolean {
        return this.cartItems.some(
            (item) => item.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        );
    }

    private getTotalAmount(): number {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.pre_discount",
            0
        );
    }

    private getDiscountAmount(): number {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.discount_amount",
            0
        );
    }

    private getDiscountType(): string {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.discount_type",
            "-"
        );
    }

    private getBenefitSource(): string {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.benefit_source",
            "-"
        );
    }

    private getCouponCode(): string {
        const itemWithCouponCode = this.cartItems.find(
            (item) => !!item.coupon_code
        );
        return itemWithCouponCode ? itemWithCouponCode.coupon_code : "-";
    }

    private sendAnalyticsEvents() {
        if (this.isFirstPurchase) {
            this.appsFlyerService.trackFirstOrder(this.cartItems);
        }
        this.appsFlyerService.trackPurchase(this.cartItems);
        this.moengageService.trackCompletePurchaseSummary(
            this.cartItems,
            this.cartMeta,
            this.isFirstPurchase
        );

        this.cartItems.forEach((cartItem: CartItem) => {
            this.moengageService.trackCompletePurchaseDetails(cartItem);
        });
    }
}
