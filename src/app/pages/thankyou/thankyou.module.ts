import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { ThankyouPageLayoutComponent } from "./layouts/thankyou.layout";
import { ThankyouPageContainer } from "./containers/thankyou.container";
import { ThankyouPageAdapter } from "./services/thankyou.adapter";

const routes: Routes = [
    {
        path: "",
        component: ThankyouPageContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [ThankyouPageLayoutComponent, ThankyouPageContainer],
    providers: [ThankyouPageAdapter],
})
export class ThankyouPageModule {}
