import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

import { MapPageContainer } from "./containers/map.container";
import { ReviewPageContainer } from "./containers/review.container";
import { SearchPageContainer } from "./containers/search.container";
import { FormPageContainer } from "./containers/form.container";
import { TypePageContainer } from "./containers/type.container";

import { AddressPageLayoutComponent } from "./layouts/core.layout";

const addressRoutes: Routes = [
    {
        path: "",
        component: AddressPageLayoutComponent,
        children: [
            {
                path: "",
                redirectTo: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.SELECTION.PATH,
                pathMatch: "full",
            },
            {
                path: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.SELECTION.PATH,
                component: TypePageContainer,
            },
            {
                path: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.FORM.PATH,
                component: FormPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_FORM },
            },
            {
                path: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.MAP.PATH,
                component: MapPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_MAP },
            },
            {
                path: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.SEARCH.PATH,
                component: SearchPageContainer,
            },
            {
                path: PAGE_ROUTES.ADDRESS_DEP.CHILDREN.REVIEW.PATH,
                component: ReviewPageContainer,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(addressRoutes)],
    exports: [RouterModule],
})
export class AddressRoutingModule {}
