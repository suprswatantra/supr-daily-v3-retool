import {
    Component,
    Output,
    EventEmitter,
    Input,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    SimpleChanges,
    OnChanges,
    SimpleChange,
} from "@angular/core";

import { Subscription } from "rxjs";
import { finalize } from "rxjs/operators";

import {
    KEY_NAME,
    MAP,
    REQUEST_TYPE,
    SA_OBJECT_NAMES,
    SEARCH_PLACEHOLDER,
    SEARCH_APARTMENT_INFO_MESSAGE,
    UNKNOWN_SOCIETY_ID,
    SOCIETY_NAME_TEXTS,
} from "@pages/address_old/constants";
import { MODAL_NAMES, CITY_LIST } from "@constants";

import { SuprApi } from "@types";
import { FlatSearchOption } from "@pages/address_old/type";

import { Address, City } from "@models";

import { ApiService } from "@services/data/api.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";
import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";

@Component({
    selector: "supr-address-search",
    template: `
        <supr-address-search-header
            [searchPlaceholder]="searchPlaceholder"
            (handleChangeText)="handleChangeText($event)"
        ></supr-address-search-header>
        <div class="societyNameInfo" *ngIf="isSelectionTypeApartment">
            <div class="societyNameInfoWrapper suprRow">
                <supr-icon name="error"></supr-icon>
                <div class="suprColumn left center">
                    <supr-text type="caption">
                        ${SOCIETY_NAME_TEXTS.INFO_TEXT}
                    </supr-text>
                    <supr-text type="caption">
                        ${SOCIETY_NAME_TEXTS.NAME_EXAMPLE_TEXT}
                    </supr-text>
                </div>
            </div>
            <div class="divider16"></div>
            <div class="societyNameInfoDivider"></div>
        </div>
        <div
            class="suprScrollContent"
            [class.hasInfo]="isSelectionTypeApartment"
        >
            <ng-container *ngIf="!isSelectionTypeApartment">
                <supr-address-search-current-location
                    *ngIf="showCurrentLocation"
                    (click)="moveCurrentLocation()"
                ></supr-address-search-current-location>
            </ng-container>

            <ng-container
                *ngIf="!isSelectionTypeApartment; else searchApartment"
            >
                <supr-address-search-results
                    *ngFor="let location of options; let i = index"
                    [primaryLocationText]="
                        location?.structured_formatting?.main_text
                    "
                    [secondaryLocationText]="
                        location?.structured_formatting?.secondary_text
                    "
                    [index]="i"
                    [saObjectName]="SA_OBJECT_NAME.MAP_SEARCH_OPTION"
                    [saContext]="saContext"
                    (handleSearchResultClick)="mapSearchResultClick($event)"
                ></supr-address-search-results>
                <div class="divider24"></div>
            </ng-container>

            <ng-template #searchApartment>
                <ng-container *ngIf="isFetchingPremiseList; else apartmentList">
                    <supr-loader class="searchLoader"></supr-loader>
                </ng-container>
                <ng-template #apartmentList>
                    <supr-address-search-results
                        *ngFor="let premise of premiseOptions; let i = index"
                        [primaryLocationText]="premise?.name"
                        [secondaryLocationText]="
                            premise?.streetAddress || defaultLocationText
                        "
                        [index]="i"
                        [saObjectName]="SA_OBJECT_NAME.APARTMENT_SEARCH_OPTION"
                        [saContext]="saContext"
                        (handleSearchResultClick)="
                            handleSearchPremiseClick($event)
                        "
                    ></supr-address-search-results>
                    <ng-container
                        *ngIf="
                            isInputBoxDirty &&
                            !isFetchingPremiseList &&
                            premiseOptions.length === 0
                        "
                    >
                        <supr-address-search-results
                            [primaryLocationText]="
                                utilService.toTitleCase(searchTerm)
                            "
                            [secondaryLocationText]="defaultLocationText"
                            [index]="1"
                            [saObjectName]="
                                SA_OBJECT_NAME.APARTMENT_SEARCH_OPTION
                            "
                            [saContext]="saContext"
                            (handleSearchResultClick)="
                                handleUserTypedResultClick($event)
                            "
                        ></supr-address-search-results>
                    </ng-container>
                </ng-template>
                <div class="divider24"></div>

                <ng-container *ngIf="_showNewSocietyNameModal">
                    <supr-modal
                        (handleClose)="closeNewSocietyNameModal()"
                        modalName="${MODAL_NAMES.UPDATE_SOCIETY_NAME}"
                    >
                        <supr-update-address-field
                            title="${SOCIETY_NAME_TEXTS.MODAL_TITLE}"
                            label="${SOCIETY_NAME_TEXTS.MODAL_INPUT_LABEL}"
                            placeholder="${SOCIETY_NAME_TEXTS.MODAL_INPUT_PLACEHOLDER}"
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .UPDATE_SOCIETY_NAME}"
                            [saContext]="saContext"
                            (handleUpdateClick)="
                                handleNewSocietyNameUpdate($event)
                            "
                            [initialValue]="utilService.toTitleCase(searchTerm)"
                        ></supr-update-address-field>
                    </supr-modal>
                </ng-container>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/map.page.scss"],
})
export class SearchComponent implements OnInit, OnDestroy, OnChanges {
    @Input() from: string;
    @Input() premise: SuprApi.PremiseSearchRes[];
    @Input() address: Address;
    @Input() isFetchingPremiseList: boolean;

    @Output() handleSearchPremise: EventEmitter<
        SuprApi.PremiseSearchReq
    > = new EventEmitter();

    @Output()
    handleSearchPremiseAddress: EventEmitter<number> = new EventEmitter();

    showCurrentLocation = true;
    isSelectionTypeApartment = false;
    showErrorText: boolean;
    searchPlaceholder: string;
    selectionType: string;
    searchTerm: string;
    options = [];
    premiseOptions: SuprApi.PremiseSearchRes[] = [];
    preimiseAddressesOptions: SuprApi.PremiseAddressRes = null;
    infoBoxMessage = SEARCH_APARTMENT_INFO_MESSAGE;
    isInputBoxDirty = false;
    defaultLocationText: string;
    _showNewSocietyNameModal = false;

    SA_OBJECT_NAME = SA_OBJECT_NAMES.CLICK;
    saContext: string;

    private keyPressSubscription: Subscription;
    private getSearchResultsSubscription: Subscription;

    constructor(
        private routerService: RouterService,
        private apiService: ApiService,
        private changeDetectionRef: ChangeDetectorRef,
        private addressPageService: AddressPageService,
        private addressService: AddressService
    ) {}

    ngOnInit() {
        this.setInitData();
        this.setAnalyticsData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const premiseChange = changes["premise"];
        this.setPremiseData(premiseChange);
    }

    ngOnDestroy() {
        this.unsubscribeKeyPress();
        this.unsubscribeSearchApi();
    }

    setPremiseData(change: SimpleChange) {
        if (change && !change.firstChange) {
            this.premiseOptions = change.currentValue;
        }
    }

    handleChangeText(searchTerm: string) {
        if (searchTerm.length > 0) {
            if (this.isSelectionTypeApartment) {
                this.searchPremise(searchTerm);
            } else {
                this.showCurrentLocation = false;
                this.getSearchResults(searchTerm);
            }
            this.isInputBoxDirty = true;
        } else {
            this.showCurrentLocation = true;
            this.isInputBoxDirty = false;
        }
    }

    mapSearchResultClick(index: number) {
        this.addressService.setTempAddressData(
            this.options[index].place_id,
            "placeId"
        );
        this.goToMapPage();
    }

    handleSearchPremiseClick(index: number) {
        const id = this.premiseOptions[index].id;
        if (id !== -1) {
            if (this.addressPageService.address.societyId !== id) {
                this.handleSearchPremiseAddress.emit(id);
                this.setEmptyFormFields();
            }
            this.addressPageService.address.type = REQUEST_TYPE.knownSociety;
            this.addressPageService.address = {
                ...this.addressPageService.address,
                [KEY_NAME.SOCIETY_NAME]: this.premiseOptions[index].name,
                [KEY_NAME.SOCIETY_ID]: id,
                premiseId: id,
            };
        }
        this.setAddressAndRoute();
    }

    setAddressAndRoute() {
        this.addressPageService.address = {
            ...this.addressPageService.address,
            from: "search",
        };

        setTimeout(() => {
            this.routerService.goToAddressFormPage({
                queryParams: {
                    selectionType: this.selectionType,
                    from: "search",
                },
            });
        }, 250);
    }

    handleNewSocietyNameUpdate(society: FlatSearchOption) {
        this.setEmptyFormFields();

        this.addressPageService.setAddressForm(
            KEY_NAME.SOCIETY_ID,
            UNKNOWN_SOCIETY_ID
        );

        this.addressPageService.setAddressForm(
            KEY_NAME.SOCIETY_NAME,
            society.name
        );

        this.addressPageService.setAddressForm(
            "type",
            REQUEST_TYPE.unknownSociety
        );
        this.addressPageService.address.type = REQUEST_TYPE.unknownSociety;
        this.setAddressAndRoute();
    }

    moveCurrentLocation() {
        this.addressService.setTempAddressData(MAP.currentLocation, "placeId");

        this.goToMapPage();
    }

    handleUserTypedResultClick() {
        this._showNewSocietyNameModal = true;
    }

    closeNewSocietyNameModal() {
        this._showNewSocietyNameModal = false;
    }

    private setEmptyFormFields() {
        this.addressPageService.address = {
            ...this.addressPageService.address,
            [KEY_NAME.DOOR_NUMBER]: "",
        };

        this.addressPageService.address = {
            ...this.addressPageService.address,
            [KEY_NAME.BUILDING_NAME]: "",
        };

        this.addressPageService.address = {
            ...this.addressPageService.address,
            [KEY_NAME.STREET_ADDRESS]: "",
        };
    }

    private setInitData() {
        if (this.addressPageService.address) {
            this.selectionType = this.addressPageService.address.type;
            if (this.from !== "map") {
                this.searchPlaceholder = SEARCH_PLACEHOLDER.apartment;
                this.isSelectionTypeApartment = true;
                this.setDefaultLocationText(this.address.city.id);
            } else {
                this.searchPlaceholder = SEARCH_PLACEHOLDER.map;
                this.isSelectionTypeApartment = false;
            }
        }
    }

    private setAnalyticsData() {
        const addressType = this.addressPageService.getAddressType();
        const addressContext = this.addressService.getAddressContext();
        this.saContext = `${addressType}-${addressContext}`;
    }

    private goToMapPage() {
        setTimeout(() => {
            this.routerService.goToAddressMapPage({
                queryParams: {
                    selectionType: this.selectionType,
                    from: "search",
                },
            });
        }, 250);
    }

    private getSearchResults(searchTerm: string) {
        this.getSearchResultsSubscription = this.apiService
            .getLocationSuggestions(searchTerm)
            .pipe(
                finalize(() => {
                    this.unsubscribeSearchApi();
                })
            )
            .subscribe(
                (response: any) => {
                    if (!response.success) {
                        this.showErrorText = true;
                        this.options = [];
                    } else {
                        this.showErrorText = false;
                        this.options = response.data.suggestions;
                        this.changeDetectionRef.detectChanges();
                    }
                },
                () => {
                    this.showErrorText = true;
                    this.options = [];
                }
            );
    }

    private unsubscribeSearchApi() {
        if (this.getSearchResultsSubscription) {
            this.getSearchResultsSubscription.unsubscribe();
            this.getSearchResultsSubscription = null;
        }
    }

    private unsubscribeKeyPress() {
        if (this.keyPressSubscription) {
            this.keyPressSubscription.unsubscribe();
            this.keyPressSubscription = null;
        }
    }

    private searchPremise(searchTerm: string) {
        const obj = {
            cityId: this.addressPageService.address.city.id,
            searchTerm,
        };

        this.searchTerm = searchTerm;
        this.handleSearchPremise.emit(obj);
    }

    private setDefaultLocationText(cityId) {
        const { value, state } = CITY_LIST.find(
            (city: City) => city.id === cityId
        );

        this.defaultLocationText = value + (state ? `, ${state}` : "");
    }
}
