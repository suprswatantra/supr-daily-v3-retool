import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-address-search-current-location",
    template: `
        <div class="current">
            <div class="suprRow currentRow">
                <supr-icon name="gps" class="icon"></supr-icon>
                <div class="spacer16"></div>
                <div class="suprColumn left">
                    <supr-text class="currentRowLocation" type="regular14">
                        Use current location
                    </supr-text>
                    <supr-text class="currentRowGps" type="caption">
                        Using GPS
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurrentLocationComponent {}
