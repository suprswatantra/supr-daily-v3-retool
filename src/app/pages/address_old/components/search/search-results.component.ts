import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

@Component({
    selector: "supr-address-search-results",
    template: `
        <div
            class="results"
            (click)="handleSearchResultClick.emit(index)"
            saClick
            [saContext]="saContext"
            [saObjectName]="saObjectName"
            [saObjectValue]="primaryLocationText"
        >
            <div class="divider12"></div>
            <div class="suprRow">
                <supr-text class="primary" type="regular14">
                    {{ primaryLocationText }}
                </supr-text>
            </div>
            <div class="suprRow" *ngIf="secondaryLocationText">
                <supr-icon name="location"></supr-icon>
                <div class="spacer8"></div>
                <supr-text class="secondary" type="caption">
                    {{ secondaryLocationText }}
                </supr-text>
            </div>
            <div class="divider12"></div>
            <div class="resultsDivider"></div>
        </div>
    `,
    styleUrls: ["../../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsComponent {
    @Input() primaryLocationText: string;
    @Input() secondaryLocationText: string;
    @Input() index: number;
    @Input() saObjectName: string;
    @Input() saContext: string;

    @Output()
    handleSearchResultClick: EventEmitter<number> = new EventEmitter();
}
