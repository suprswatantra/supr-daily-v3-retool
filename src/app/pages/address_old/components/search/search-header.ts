import {
    Component,
    Output,
    EventEmitter,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";
import { MAX_TEXT_INPUT_LIMIT } from "@constants";

@Component({
    selector: "supr-address-search-header",
    template: `
        <supr-page-header>
            <supr-search-input
                [placeholder]="searchPlaceholder"
                [maxLength]="maxLength"
                (handleChangeText)="handleChangeText.emit($event)"
            ></supr-search-input>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchHeaderComponent {
    @Input() searchPlaceholder: string;
    @Input() maxLength = MAX_TEXT_INPUT_LIMIT;
    @Output() handleChangeText: EventEmitter<string> = new EventEmitter();
}
