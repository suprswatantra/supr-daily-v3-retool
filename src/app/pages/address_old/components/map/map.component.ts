import {
    Input,
    OnInit,
    ViewChild,
    Component,
    OnDestroy,
    ChangeDetectorRef,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Platform } from "@ionic/angular";
import { Observable, Subscription, of } from "rxjs";
import { finalize, catchError, filter } from "rxjs/operators";

import { AgmMap } from "@agm/core";

import { MapsSettings, LOCATION_AUTHORIZATION_STATUS } from "@constants";

import { ApiService } from "@services/data/api.service";
import { MapService } from "@services/shared/map.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";
import { LocationService } from "@services/util/location.service";
import { PlatformService } from "@services/util/platform.service";

import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";

import { SuprApi, SuprMaps } from "@types";
import {
    MAP,
    MAP_TOAST_MESSAGES,
    SA_OBJECT_NAMES,
} from "@pages/address_old/constants";

@Component({
    selector: "supr-address-map",
    templateUrl: "./map.component.html",
    styleUrls: ["../../styles/map.page.scss"],
})
export class MapComponent implements OnInit, OnDestroy {
    @Input() from: string;
    @Input() selectionType: string;

    @ViewChild(AgmMap, { static: true }) myMap: AgmMap;

    private init = false;
    private routerSub: Subscription;
    private locationStatusCalled = false;
    private locationUpdateSubscription: Subscription;
    private locationStatusCheckSubscription: Subscription;
    private defaultLocationSubscription: Subscription;

    lat: number;
    lng: number;
    title: string;
    newCenterLat: number;
    newCenterLng: number;
    disabledButton: boolean;
    setAddressDone = false;
    formatted_address: string;
    isLocationServiceable = false;
    checkingServiceability = false;
    toastMeta = MAP_TOAST_MESSAGES.serviceable;
    zoom = MapsSettings.zoomLevel;
    locationEnableDialog = false;
    locationPermissionDialog = false;
    resumeEventSubscription: Subscription;

    SA_OBJECT_NAME = SA_OBJECT_NAMES.CLICK;
    saContext: string;

    constructor(
        private router: Router,
        private platform: Platform,
        private apiService: ApiService,
        private utilService: UtilService,
        private routerService: RouterService,
        private addressService: AddressService,
        private locationService: LocationService,
        private platformService: PlatformService,
        private addressPageService: AddressPageService,
        private mapService: MapService,
        private changeDetectionRef: ChangeDetectorRef
    ) {
        this.subscribeToRouterEvents();
        this.subscribeToResumeEvent();
    }

    ngOnInit() {
        this.setInit();
        this.init = true;
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    openSearchAddress() {
        this.routerService.goToAddressSearchPage({
            queryParams: {
                from: "map",
                selectionType: this.selectionType,
            },
        });
    }

    goBack() {
        this.routerService.goBack();
    }

    handleProceedClick() {
        this.addressPageService.setLatLong(this.lat, this.lng);

        this.routerService.goToAddressReviewPage({
            queryParams: {
                selectionType: this.selectionType,
            },
        });
    }

    onSearchLocationSelected(placeId: string) {
        if (placeId === MAP.currentLocation) {
            this.moveToCurrentLocation();
            return;
        }
        this.doReverseCode(placeId, true);
    }

    doReverseCode(placeId = "", updateLatLng?: boolean) {
        this.checkingServiceability = true;
        const data = placeId
            ? {
                  place_id: placeId,
              }
            : {
                  latlng: `${this.lat},${this.lng}`,
              };

        this.unsubscribeLocationUpdate();
        this.locationUpdateSubscription = this.reverseGeoCode(data)
            .pipe(
                finalize(() => {
                    this.unsubscribeLocationUpdate();
                })
            )
            .subscribe((geocodeResponse: SuprApi.GeoCodeRes) => {
                this.updateMapLocationDetails(geocodeResponse, updateLatLng);

                /* Check for location status */
                this.locationStatusCheck(null, geocodeResponse);
            });
    }

    /* [TODO] Add type for map */
    mapReady(map: any) {
        map.addListener("dragend", () => {
            this.setLocation(this.newCenterLat, this.newCenterLng);
            this.doReverseCode();
            this.checkLocationStatus();
        });
    }

    /* [TODO] Add type for e */
    centerChange(e: any) {
        this.newCenterLat = e.lat;
        this.newCenterLng = e.lng;
    }

    async moveToCurrentLocation() {
        /* Checking for Location Service Enabled/Disabled on iOS */
        const locationStatus = await this.locationService.getLocationStatus();
        if (
            this.platformService.isIOS() &&
            (locationStatus === LOCATION_AUTHORIZATION_STATUS.ios.DISABLED ||
                locationStatus ===
                    LOCATION_AUTHORIZATION_STATUS.ios.UNAUTHORIZED)
        ) {
            this.locationEnableDialog =
                locationStatus === LOCATION_AUTHORIZATION_STATUS.ios.DISABLED;
            this.locationPermissionDialog =
                locationStatus ===
                LOCATION_AUTHORIZATION_STATUS.ios.UNAUTHORIZED;
            return;
        }

        this.fetchCurrentLocation()
            .then((latLng: SuprMaps.Location) => {
                this.setLocation(latLng.lat, latLng.lng);
                this.doReverseCode();
            })
            .catch(() => {
                this.setServiceabilityInfo("noPermission");
                this.moveToDefaultLocation();
            });
    }

    private async fetchCurrentLocation(): Promise<SuprMaps.Location> {
        return this.locationService
            .getLocationAccess()
            .then(async (accessGranted: boolean) => {
                if (accessGranted) {
                    const location = await this.locationService.getCurrentLocation();
                    return location;
                } else {
                    return Promise.reject();
                }
            });
    }

    private setInit() {
        this.myMap.zoom = MAP.zoom;
        this.resizeMap();
        this.setButtonState();
        this.setInitialMarker();
        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        const addressType = this.addressPageService.getAddressType();
        const addressContext = this.addressService.getAddressContext();
        this.saContext = `${addressType}-${addressContext}`;
    }

    private resizeMap() {
        setTimeout(() => {
            this.myMap.triggerResize();
        }, MAP.resizeTimeOut);
    }

    private setButtonState() {
        if (this.from === "search") {
            this.disabledButton = false;
        } else {
            this.disabledButton = true;
        }
    }

    /* Set marker position based on search result or existing address */
    private setInitialMarker() {
        const placeId = this.addressService.getTempAddressData("placeId");
        const savedLocation = this.addressService.getTempAddressData(
            "locationData"
        );

        /* Place id received from search results and isn't equal to current location */
        if (placeId) {
            this.onSearchLocationSelected(placeId);
            this.addressService.setTempAddressData(null, "placeId");
        } else if (
            savedLocation &&
            savedLocation.latitude &&
            savedLocation.longitude
        ) {
            this.setLocation(savedLocation.latitude, savedLocation.longitude);
            this.doReverseCode();
        } else {
            /* Land user to current location */
            this.moveToCurrentLocation();
        }
    }

    private updateMapLocationDetails(
        geocodeResponse: SuprApi.GeoCodeRes,
        updateLatLng?: boolean
    ) {
        if (geocodeResponse) {
            const results = this.selectGeocodeResults(geocodeResponse);
            if (results.length > 0) {
                this.formatted_address = results[0].formatted_address;
                this.addressPageService.address.location = {
                    ...this.addressPageService.address.location,
                    ["reverseGeocode"]: results,
                };
                /* Need not update lat long in view if change triggered by map drag */
                if (updateLatLng) {
                    this.setLocation(
                        results[0].geometry.location.lat,
                        results[0].geometry.location.lng
                    );
                }
            }
        }
    }

    private setLocation(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;

        this.addressService.setTempAddressData(
            {
                latitude: this.lat,
                longitude: this.lng,
            },
            "locationData"
        );

        this.changeDetectionRef.detectChanges();
    }

    private selectGeocodeResults(
        geocodeResponse: SuprApi.GeoCodeRes
    ): SuprMaps.GeoCodeResult[] {
        return this.utilService.getNestedValue(
            geocodeResponse,
            "data.results",
            []
        );
    }

    private reverseGeoCode(
        geoCodeInput: SuprMaps.GeocodeInput
    ): Observable<SuprApi.GeoCodeRes> {
        return this.apiService.reverseGeocode(geoCodeInput);
    }

    private unsubscribeLocationUpdate() {
        if (this.locationUpdateSubscription) {
            this.locationUpdateSubscription.unsubscribe();
            this.locationUpdateSubscription = null;
        }
    }

    private checkLocationStatus() {
        /* Stop the previous request */
        this.unsubscribeLocationStatusCheck();

        const location = {
            lat: this.lat,
            lng: this.lng,
        };

        this.locationStatusCheckSubscription = this.apiService
            .getLocationStatus(location)
            .pipe(
                catchError(() => {
                    /** do nothing */

                    return of();
                }),

                finalize(() => {
                    this.unsubscribeLocationStatusCheck();
                })
            )
            .subscribe((response: SuprApi.LocationStatusRes) =>
                this.handleLocationStatusResponse(response)
            );
    }

    private handleLocationStatusResponse(response: SuprApi.LocationStatusRes) {
        if (!response || response.statusCode !== 0) {
            this.setServiceabilityInfo("unserviceable");
            return;
        }
        const { city, unServiceableMessage } = response.data;

        if (unServiceableMessage) {
            this.isLocationServiceable = false;
            this.setServiceabilityInfo("unserviceable");
        } else {
            this.checkCityChange(city);
            this.setServiceabilityInfo();
        }

        this.changeDetectionRef.detectChanges();
    }

    private setServiceabilityInfo(status?: string) {
        switch (status) {
            case "unserviceable":
                this.isLocationServiceable = false;
                this.toastMeta = MAP_TOAST_MESSAGES.unserviceable;
                break;

            case "noPermission":
                this.isLocationServiceable = false;
                this.toastMeta = MAP_TOAST_MESSAGES.no_permission;
                break;

            default:
                this.isLocationServiceable = true;
                this.toastMeta = MAP_TOAST_MESSAGES.serviceable;
                break;
        }
    }

    private unsubscribeLocationStatusCheck() {
        if (this.locationStatusCheckSubscription) {
            this.locationStatusCheckSubscription.unsubscribe();
            this.locationStatusCheckSubscription = null;
        }
    }

    private locationStatusCheck(
        latLng?: SuprMaps.Location,
        geocodeResponse?: SuprApi.GeoCodeRes
    ) {
        let location = latLng;
        if (!location) {
            const results = this.selectGeocodeResults(geocodeResponse);
            location = this.utilService.getNestedValue(
                results[0],
                "geometry.location"
            );
        }

        // Set the address done flag in 2nd check
        if (!this.setAddressDone && this.locationStatusCalled) {
            this.setAddressDone = true;
        }

        // Stop the previous request
        this.unsubscribeLocationStatusCheck();

        this.locationStatusCheckSubscription = this.apiService
            .getLocationStatus(location)
            .pipe(
                catchError(() => {
                    /** do nothing */
                    return of();
                }),

                finalize(() => {
                    this.checkingServiceability = false;
                    this.changeDetectionRef.detectChanges();
                    if (!this.locationStatusCalled) {
                        this.locationStatusCalled = true;
                    }
                    this.unsubscribeLocationStatusCheck();
                })
            )
            .subscribe((response: SuprApi.LocationStatusRes) =>
                this.handleLocationStatusResponse(response)
            );
    }

    private checkCityChange(city: { id?: number; name?: string }) {
        const address = this.addressPageService.getAddress();
        if (city.id !== address.city.id) {
            this.addressPageService.setAddressForm("city", city);
        }
    }

    private isMapPageUrl(url: string): boolean {
        return url && url.indexOf("map") > -1;
    }

    private moveToDefaultLocation() {
        const address = this.addressPageService.getAddress();

        /* Stop previous request */
        this.unsubscribeDefaultLocation();
        this.defaultLocationSubscription = this.mapService
            .getDefaultLocation(address)
            .pipe(
                finalize(() => {
                    this.unsubscribeDefaultLocation();
                })
            )
            .subscribe((location: SuprMaps.Location) => {
                this.setLocation(location.lat, location.lng);
                this.doReverseCode();
            });
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isMapPageUrl(event.url))
            )
            .subscribe(() => {
                if (this.init) {
                    this.setInit();
                }
            });
    }

    private subscribeToResumeEvent() {
        this.resumeEventSubscription = this.platform.resume.subscribe(
            async () => {
                try {
                    const locationStatus = await this.locationService.getLocationStatus();
                    /* Check if user enabled location, then get user's location */
                    if (
                        locationStatus !==
                        LOCATION_AUTHORIZATION_STATUS.ios.DISABLED
                    ) {
                        this.moveToCurrentLocation();
                    }
                } catch (e) {}
            }
        );
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    private unsubscribeDefaultLocation() {
        if (this.defaultLocationSubscription) {
            this.defaultLocationSubscription.unsubscribe();
            this.defaultLocationSubscription = null;
        }
    }
}
