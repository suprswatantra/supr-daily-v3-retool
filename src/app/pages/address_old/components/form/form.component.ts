import {
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    SimpleChange,
    SimpleChanges,
    OnChanges,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { Address } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import { AddressCurrentState } from "@store/address/address.state";
import { SuprApi } from "@types";

import {
    ADDRESS_REQUIRED_KEYS,
    BUTTON_TEXT,
    FORM_INPUTS,
    REQUEST_TYPE,
    INPUT_TYPE,
    KEY_NAME,
} from "@pages/address_old/constants";
import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";
import { InputChangeType } from "@pages/address_old/type";
import {
    SA_OBJECT_NAMES,
    SA_OBJECT_VALUES,
} from "@pages/address_old/constants/analytics.constants";

@Component({
    selector: "supr-address-form",
    template: `
        <div class="suprScrollContent">
            <div class="form">
                <div class="divider16"></div>

                <supr-text type="subtitle">Enter your full address </supr-text>
                <div class="divider16"></div>

                <div *ngFor="let input of form_input">
                    <supr-address-form-input
                        [label]="input.name"
                        [type]="input.type"
                        [key]="input.key"
                        [value]="input.value"
                        [searchList]="input.searchList"
                        [placeholder]="input.placeholder"
                        [disabled]="input.disabled"
                        [isFieldAccesible]="areFieldsAcccesible"
                        [modalName]="input.modalName"
                        [saContext]="saContext"
                        (handleInputChange)="handleInputChange($event)"
                    ></supr-address-form-input>
                    <supr-profile-form-error
                        [errMsg]="input?.error"
                    ></supr-profile-form-error>

                    <div class="divider16"></div>
                </div>
                <div class="divider24"></div>
            </div>
        </div>
        <supr-page-footer>
            <supr-button
                [loading]="isSavingAddress"
                (handleClick)="onNextClick()"
                saObjectName="${SA_OBJECT_NAMES.CLICK.NEXT_BUTTON}"
                [saObjectValue]="saObjectValueNextButton"
                [saContext]="saContext"
            >
                {{ nextButtonText }}
            </supr-button>
        </supr-page-footer>
    `,
    styleUrls: ["../../styles/form.page.scss"],
})
export class FormComponent implements OnInit, OnChanges, OnDestroy {
    @Input() address: Address;
    @Input() premiseAddresses: SuprApi.PremiseAddressRes;
    @Input() addressState: AddressCurrentState;

    @Output() handleSearchPremiseAddress: EventEmitter<
        number
    > = new EventEmitter();
    @Output()
    handleUpdateAddress: EventEmitter<void> = new EventEmitter();

    preimiseAddressesOption: SuprApi.PremiseAddressRes;

    disabled = true;
    init = false;
    inputObject = {};
    form_input: [];
    requiredKeys = [];
    areFieldsAcccesible = false;
    isSavingAddress = false;
    nextButtonText = BUTTON_TEXT.NEXT;
    saObjectValueNextButton = SA_OBJECT_VALUES.ACTIVE;
    addressType: string;
    saContext: string;
    saContextFormInput: string;

    private routerSub: Subscription;
    private referrerUrl: string;

    constructor(
        private addressPageService: AddressPageService,
        private cdr: ChangeDetectorRef,
        private routerService: RouterService,
        private router: Router,
        private utilService: UtilService,
        private addressService: AddressService
    ) {
        this.setReferrerUrl();
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isFormPageUrl(event.url))
            )
            .subscribe(() => {
                if (this.init) {
                    this.setInitData();
                }
            });
    }

    ngOnInit() {
        this.setInitData();
        this.init = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        const premiseAddressChange = changes["premiseAddresses"];
        this.setPremiseAddress(premiseAddressChange);
        this.handleAddressStateChange(changes["addressState"]);
    }

    ngOnDestroy() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    handleInputChange(inputType: InputChangeType) {
        if (inputType.type === INPUT_TYPE.buttonText) {
            this.addressPageService.setAddressForm("from", "search");
            this.routerService.goToAddressSearchPage({
                queryParams: {
                    selectionType: this.addressType,
                    from: "society",
                },
            });
        } else if (inputType.type === INPUT_TYPE.searchAbleText) {
            this.setSearchableTextData(inputType);
        } else {
            this.inputObject[inputType.label] = inputType.value;
            this.addressPageService.setAddressForm(
                inputType.key,
                inputType.value
            );

            this.addressPageService.setFormInputValue(
                inputType.value,
                inputType.key,
                this.form_input
            );
        }

        if (inputType.key === KEY_NAME.STREET_ADDRESS) {
            this.addressPageService.address.streetAddress = inputType.value;
        }
    }

    setFormInputValue(key: string, value: any) {
        this.form_input = this.addressPageService.setFormInputValue(
            value,
            key,
            this.form_input
        );
    }

    setBuildingOptionsList() {
        this.form_input = this.addressPageService.setSearchOptionLists(
            this.premiseAddresses.buildings,
            this.form_input,
            KEY_NAME.BUILDING_NAME
        );
    }

    setDoorsOptionsList(index: number) {
        this.form_input = this.addressPageService.setSearchOptionLists(
            this.getDoors(index),
            this.form_input,
            KEY_NAME.DOOR_NUMBER
        );
    }

    getDoors(index: number) {
        if (!this.premiseAddresses) {
            return [];
        }

        return index === -1
            ? []
            : this.premiseAddresses.buildings
                  .map(dt => {
                      if (dt.id === index) {
                          return dt.doors;
                      }
                  })
                  .filter(dt => dt !== undefined)[0];
    }

    onNextClick() {
        if (!this.isFormValid()) {
            return;
        }

        if (this.isKnownSociety()) {
            this.handleUpdateAddress.emit();
        } else {
            setTimeout(() => {
                this.routerService.goToAddressMapPage({
                    queryParams: {
                        selectionType: this.addressPageService.getAddressType(),
                    },
                });
            }, 250);
        }
    }

    private isFormValid(): boolean {
        let isValid = true;
        const { address } = this.addressPageService;
        this.setRequiredKeys(address.type);

        this.requiredKeys.map(key => {
            if (!address[key]) {
                isValid = false;
                this.addressPageService.setFormInputError(key, this.form_input);
            }
        });

        this.cdr.detectChanges();

        return isValid;
    }

    private handleAddressStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        this.handleButtonLoading(change);
        this.updateButtonText(change);
    }

    private handleButtonLoading(change: SimpleChange) {
        if (
            change.currentValue === AddressCurrentState.CREATING_ADDRESS ||
            change.currentValue === AddressCurrentState.REFRESHING_ON_HUB_CHANGE
        ) {
            this.isSavingAddress = true;
        } else {
            this.isSavingAddress = false;
        }
    }

    private updateButtonText(change) {
        if (change.currentValue === AddressCurrentState.NO_ACTION) {
            let buttonText;

            if (this.isKnownSociety()) {
                if (this.isReferredFromCartPage()) {
                    buttonText = BUTTON_TEXT.CONFIRM_N_GO_TO_CART;
                } else {
                    buttonText = BUTTON_TEXT.SAVE;
                }
            } else {
                buttonText = BUTTON_TEXT.NEXT;
            }

            this.nextButtonText = buttonText;
        }
    }

    private setPremiseAddress(change: SimpleChange) {
        if (change) {
            if (
                !change.firstChange &&
                change.previousValue !== change.currentValue
            ) {
                if (change.currentValue) {
                    this.preimiseAddressesOption = change.currentValue;

                    this.setFormInputValue(
                        KEY_NAME.SOCIETY_NAME,
                        this.preimiseAddressesOption.name
                    );

                    this.setFormInputValue(
                        KEY_NAME.STREET_ADDRESS,
                        this.preimiseAddressesOption.streetAddress
                    );

                    this.addressPageService.address.societyName = this.preimiseAddressesOption.name;
                    this.addressPageService.address.streetAddress = this.preimiseAddressesOption.streetAddress;

                    this.addressPageService.address.societyId = this.preimiseAddressesOption.id;
                    this.addressPageService.address.premiseId = this.preimiseAddressesOption.id;
                    this.setFormDataOnLoad();
                    this.setBuildingOptionsList();
                    this.checkToClearBuildingdDoor();
                    this.makeFieldsAccessible();
                }
            }
        }
    }

    private setInitData() {
        const addressData = this.addressPageService.address;
        if (addressData.type) {
            if (addressData.from === "search") {
                this.addressPageService.setAddressForm("from", "");
            }
            if (this.address) {
                this.addressPageService.address.city = this.address.city;
            }
        } else {
            if (this.address) {
                this.addressPageService.setDataToAddressService(this.address);
                this.checkForSocietyId();
                this.makeFieldsAccessible();
            }
        }
        if (addressData.type === REQUEST_TYPE.individualHouse) {
            this.makeFieldsAccessible();
        }
        if (addressData.type === REQUEST_TYPE.unknownSociety) {
            this.setUnknownSocietyId();
        }
        this.getPremiseData(this.addressPageService.getAddress());
        this.getFormInputFields(addressData.type);
        this.setFormDataOnLoad();
        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        const addressContext = this.addressService.getAddressContext();
        this.saContext = `${this.addressType}-${addressContext}`;
    }

    private setFormDataOnLoad() {
        const address = this.addressPageService.address;
        if (address.type) {
            for (const key in address) {
                if (address.hasOwnProperty(key)) {
                    this.setFormInputValue(key, address[key]);
                }
            }
            this.addressType = address.type;
        }
    }

    private getFormInputFields(type: string) {
        this.form_input = FORM_INPUTS[type];
    }

    private setRequiredKeys(type: string) {
        this.requiredKeys = ADDRESS_REQUIRED_KEYS[type];
    }

    private checkForSocietyId() {
        if (this.address.societyId > 0) {
            // [TODO]: Society, DoorId and BuildingId
            this.addressPageService.setAddressForm(
                KEY_NAME.SOCIETY_ID,
                this.address.societyId
            );
            // [TODO Call premised address Search]
            // will not call api if we will get building id and door id same time
        }
    }

    private changeResidenceType(type: string) {
        this.addressPageService.address.type = type;
        this.form_input = FORM_INPUTS[this.addressPageService.address.type];
        this.setFormDataOnLoad();
    }

    private setSearchableTextData(inputType: InputChangeType) {
        if (inputType.value !== "") {
            if (inputType.label === KEY_NAME.BUILDING_NAME) {
                if (
                    this.addressPageService.address.buildingName !==
                    inputType.value
                ) {
                    this.addressPageService.address.doorNumber = "";
                    this.addressPageService.address.doorId = 0;
                    this.setFormInputValue(KEY_NAME.DOOR_NUMBER, "");
                }

                this.addressPageService.address.buildingId = +inputType.key;
                this.addressPageService.address.buildingName = inputType.value;
                this.setFormInputValue(KEY_NAME.BUILDING_NAME, inputType.value);

                if (inputType.key !== -1) {
                    this.setDoorsOptionsList(+inputType.key);
                } else {
                    // Changing Type of known to unknown
                    this.changeResidenceType(REQUEST_TYPE.unknownSociety);
                }
            } else if (inputType.label === KEY_NAME.DOOR_NUMBER) {
                this.addressPageService.address.doorId = +inputType.key;
                this.addressPageService.address.doorNumber = inputType.value;
                this.setFormInputValue(KEY_NAME.DOOR_NUMBER, inputType.value);
                if (inputType.key === -1) {
                    // Changing Type of known to unknown
                    this.changeResidenceType(REQUEST_TYPE.unknownSociety);
                }
            }
        }
    }

    private getPremiseData(address: Address) {
        if (
            this.addressPageService.isKnownSociety() &&
            address &&
            address.premiseId > 0
        ) {
            this.handleSearchPremiseAddress.emit(address.premiseId);
            this.makeFieldsAccessible();
        }
    }

    private checkToClearBuildingdDoor() {
        const address = this.addressPageService.getAddress();

        if (this.address.societyName !== address.societyName) {
            this.setFormInputValue(KEY_NAME.DOOR_NUMBER, "");
            this.setFormInputValue(KEY_NAME.BUILDING_NAME, "");
            this.addressPageService.setSearchOptionLists(
                [],
                this.form_input,
                KEY_NAME.DOOR_NUMBER
            );
        } else if (address.type === REQUEST_TYPE.knownSociety) {
            if (address.buildingName) {
                this.setBuildingId(address.buildingName);
            }
            if (address.doorNumber) {
                this.setDoorId(address.doorNumber);
            }
        }
    }

    private setBuildingId(buildingName: string) {
        const { isLengthyArray } = this.utilService;
        if (isLengthyArray(this.premiseAddresses.buildings)) {
            const selectedBuilding = this.premiseAddresses.buildings.filter(
                (item: any) => item.name === buildingName
            );
            if (isLengthyArray(selectedBuilding)) {
                this.addressPageService.address.buildingId =
                    selectedBuilding[0].id;
                this.setDoorsOptionsList(selectedBuilding[0].id);
                this.setFormInputValue(KEY_NAME.BUILDING_NAME, buildingName);
            } else {
                this.addressPageService.address.buildingId = 0;
                this.addressPageService.address.buildingName = "";
                this.setFormInputValue(KEY_NAME.BUILDING_NAME, "");
            }
        }
    }

    private setDoorId(doorNumber: string) {
        const form_input_door: any = this.form_input.filter(
            (item: any) => item.key === KEY_NAME.DOOR_NUMBER
        )[0];

        const { isLengthyArray } = this.utilService;
        if (isLengthyArray(form_input_door.searchList)) {
            const selectedDoor = form_input_door.searchList.filter(
                (item: any) => item.name === doorNumber
            );

            if (isLengthyArray(selectedDoor)) {
                this.addressPageService.address.doorId = selectedDoor[0].id;
                this.setFormInputValue(
                    KEY_NAME.DOOR_NUMBER,
                    this.addressPageService.address.doorNumber
                );
            } else {
                this.addressPageService.address.doorId = 0;
                this.addressPageService.address.doorNumber = "";
                this.setFormInputValue(KEY_NAME.DOOR_NUMBER, "");
            }
        }
    }

    private setUnknownSocietyId() {
        const addressData = this.addressPageService.address;
        if (addressData.societyName) {
            this.addressPageService.address.societyId = -1;
            this.makeFieldsAccessible();
        }
    }

    private isFormPageUrl(url: string): boolean {
        return url && url.indexOf("form") > -1;
    }

    private makeFieldsAccessible() {
        this.areFieldsAcccesible = true;
    }

    private isKnownSociety(): boolean {
        return this.addressPageService.isKnownSociety();
    }

    private isReferredFromCartPage(): boolean {
        const urlData = this.utilService.getUrlData(this.referrerUrl);

        return (
            urlData &&
            urlData.queryParams &&
            urlData.queryParams.redirect === "cart"
        );
    }

    private setReferrerUrl() {
        this.referrerUrl = this.routerService.getPreviousUrl();
    }
}
