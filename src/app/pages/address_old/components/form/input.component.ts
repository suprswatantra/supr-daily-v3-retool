import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { MODAL_TYPES, MODAL_NAMES } from "@constants";
import {
    INPUT_TYPE,
    KEY_NAME,
    CANT_FIND_BLOCK_TEXTS,
    CANT_FIND_FLAT_NO_TEXTS,
} from "@pages/address_old/constants";
import { SA_OBJECT_NAMES } from "@pages/address_old/constants/analytics.constants";

import { InputChangeType } from "@pages/address_old/type";

import { ModalService } from "@services/layout/modal.service";

@Component({
    selector: "supr-address-form-input",
    template: `
        <div class="suprColumn left input" [class.relative]="!isFieldAccesible">
            <div
                class="absoluteOverlay"
                *ngIf="!isFieldAccesible && type !== buttonText"
            ></div>

            <div
                class="inputField"
                [class.disabled]="!isFieldAccesible"
                *ngIf="type === 'text'"
            >
                <supr-input-label
                    textType="body"
                    [placeholder]="placeholder"
                    [disabled]="disabled"
                    [label]="label"
                    [value]="value"
                    (handleInputChange)="onInputChange($event)"
                    [saObjectName]="label"
                    [saContext]="saContext"
                ></supr-input-label>
            </div>

            <div
                class="inputField"
                [class.disabled]="!isFieldAccesible"
                *ngIf="type === 'buttonText'"
                (click)="onInputChange(type)"
                saClick
                [saObjectName]="label"
                [saContext]="saContext"
            >
                <div class="inputFieldLabel">{{ label }}</div>
                <div class="divider4"></div>
                <div class="suprRow inputFieldButton">
                    <supr-text type="subheading">
                        {{ value ? value : "" }}
                    </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>

            <div
                class="inputField"
                [class.disabled]="!isFieldAccesible"
                *ngIf="type === 'searchAbleText'"
            >
                <ng-container *ngIf="!_showAddressSearchModal; else modalView">
                    <supr-input-label
                        textType="body"
                        [placeholder]="placeholder"
                        [disabled]="disabled"
                        [label]="label"
                        [value]="value"
                        (click)="showAddressSearchModal($event)"
                        [saObjectName]="label"
                        [saContext]="saContext"
                    ></supr-input-label>
                </ng-container>
                <ng-template #modalView>
                    <supr-modal
                        #suprModal
                        [animate]="true"
                        [fullScreen]="true"
                        [backdropDismiss]="true"
                        [modalType]="_modalType"
                        [modalName]="modalName"
                        [saContext]="saContext"
                        (handleClose)="closeAddressSearchModal()"
                    >
                        <supr-address-input-search
                            [placeholder]="placeholder"
                            [optionsList]="searchList"
                            [value]="value"
                            [optionObjectName]="key"
                            [addUserInputObjectName]="addUserInputObjectName"
                            [cantFindAddressObjectName]="
                                cantFindAddressObjectName
                            "
                            [saContext]="saContext"
                            [cantFindLabel]="cantFindTexts.LIST_ITEM_TEXT"
                            textType="body"
                            (handleCloseModal)="closeAddressSearchModal()"
                            (handleInputChange)="
                                handleSearchInputChange($event)
                            "
                            (handleNoMatchClick)="openUpdateAddressModal()"
                        ></supr-address-input-search>
                    </supr-modal>
                </ng-template>
            </div>

            <ng-container *ngIf="_showUpdateAddressModal">
                <supr-modal
                    (handleClose)="closeUpdateAddressModal()"
                    [modalName]="updateAddressModalName"
                >
                    <supr-update-address-field
                        [title]="cantFindTexts.MODAL_TITLE"
                        [label]="cantFindTexts.MODAL_INPUT_LABEL"
                        [placeholder]="cantFindTexts.MODAL_INPUT_PLACEHOLDER"
                        [saObjectName]="updateClickObjectName"
                        [saContext]="saContext"
                        (handleUpdateClick)="
                            handleAddressComponentUpdate($event)
                        "
                    ></supr-update-address-field>
                </supr-modal>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/form.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent implements OnInit {
    @Input() label: string;
    @Input() type: string;
    @Input() key: string;
    @Input() value: string;
    @Input() placeholder: string;
    @Input() disabled: boolean;
    @Input() searchList: [];
    @Input() isFieldAccesible: boolean;
    @Input() modalName: string;
    @Input() saContext: string;

    @Output() handleInputChange: EventEmitter<
        InputChangeType
    > = new EventEmitter();

    buttonText = INPUT_TYPE.buttonText;
    _showAddressSearchModal = false;
    _showUpdateAddressModal = false;
    _modalType = MODAL_TYPES.NO_WRAPPER;
    addUserInputObjectName: string;
    cantFindAddressObjectName: string;
    updateClickObjectName: string;
    updateAddressModalName: string;
    cantFindTexts: any;

    constructor(private ms: ModalService) {}

    ngOnInit() {
        this.setAnalyticsData();
        this.setStrings();
    }

    onInputChange(text: any) {
        if (typeof text === "string") {
            this.handleInputChange.emit({
                value: text,
                label: this.label,
                key: this.key,
                type: this.type,
            });
        } else {
            this.handleInputChange.emit({
                type: this.type,
                label: this.key,
                key: text.id,
                value: text.name,
            });
        }
    }

    handleSearchInputChange(text: any) {
        this.onInputChange(text);
        this.closeAddressSearchModal();
    }

    showAddressSearchModal(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation();
        this._showAddressSearchModal = true;
    }

    closeAddressSearchModal() {
        this._showAddressSearchModal = false;
        this.ms.closeModal();
    }

    openUpdateAddressModal() {
        this.closeAddressSearchModal();
        this._showUpdateAddressModal = true;
    }

    closeUpdateAddressModal() {
        this._showUpdateAddressModal = false;
        this.ms.closeModal();
    }

    handleAddressComponentUpdate(text: any) {
        this.onInputChange(text);
        this.closeUpdateAddressModal();
    }

    private setAnalyticsData() {
        const {
            ADD_BLOCK,
            ADD_FLAT_NO,
            CANT_FIND_BLOCK,
            CANT_FIND_FLAT_NO,
            UPDATE_BLOCK,
            UPDATE_FLAT_NO,
        } = SA_OBJECT_NAMES.CLICK;

        if (this.key === KEY_NAME.BUILDING_NAME) {
            this.addUserInputObjectName = ADD_BLOCK;
            this.cantFindAddressObjectName = CANT_FIND_BLOCK;
            this.updateClickObjectName = UPDATE_BLOCK;
            this.updateAddressModalName = MODAL_NAMES.UPDATE_BLOCK;
        } else if (this.key === KEY_NAME.DOOR_NUMBER) {
            this.addUserInputObjectName = ADD_FLAT_NO;
            this.cantFindAddressObjectName = CANT_FIND_FLAT_NO;
            this.updateClickObjectName = UPDATE_FLAT_NO;
            this.updateAddressModalName = MODAL_NAMES.UPDATE_FLAT_NO;
        }
    }

    private setStrings() {
        if (this.key === KEY_NAME.BUILDING_NAME) {
            this.cantFindTexts = CANT_FIND_BLOCK_TEXTS;
        } else if (this.key === KEY_NAME.DOOR_NUMBER) {
            this.cantFindTexts = CANT_FIND_FLAT_NO_TEXTS;
        }
    }
}
