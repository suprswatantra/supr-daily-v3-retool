import { InputSearchComponent } from "./modals/search-input.component";
import { UpdateAddressComponent } from "./modals/update-address.component";

import { TypeComponent } from "./type/type.component";
import { TileComponent } from "./type/tile.component";
import { TypeHeaderComponent } from "./type/header.component";

import { FormComponent } from "./form/form.component";
import { InputComponent } from "./form/input.component";
import { AddressHeaderComponent } from "./header/header.component";
import { InfoComponent } from "./info/info.component";
import { MapComponent } from "./map/map.component";
import { SearchComponent } from "./search/search.component";
import { CurrentLocationComponent } from "./search/current-location.component";
import { SearchHeaderComponent } from "./search/search-header";
import { SearchResultsComponent } from "./search/search-results.component";
import { ReviewComponent } from "./review/review.component";

export const addressComponents = [
    TypeComponent,
    TileComponent,
    TypeHeaderComponent,
    InputComponent,
    FormComponent,
    InfoComponent,
    AddressHeaderComponent,
    MapComponent,
    SearchComponent,
    CurrentLocationComponent,
    SearchHeaderComponent,
    SearchResultsComponent,
    ReviewComponent,
    InputSearchComponent,
    UpdateAddressComponent,
];
