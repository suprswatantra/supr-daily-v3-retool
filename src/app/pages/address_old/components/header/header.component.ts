import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-address-header",
    template: `
        <supr-page-header>
            <supr-text type="subheading">{{ headerText }}</supr-text>
        </supr-page-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressHeaderComponent {
    @Input() headerText: string;
}
