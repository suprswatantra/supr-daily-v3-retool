import { Component, OnInit } from "@angular/core";
import { BELL_MESSAGE } from "@pages/address_old/constants";

@Component({
    selector: "supr-address-info",
    templateUrl: "./info.component.html",
    styleUrls: ["../../styles/info.scss"],
})
export class InfoComponent implements OnInit {
    message: string;

    ngOnInit() {
        this.message = BELL_MESSAGE;
    }
}
