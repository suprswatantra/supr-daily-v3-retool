import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
    OnInit,
} from "@angular/core";

import { BUTTON_TEXT } from "../../constants";

import { FlatSearchOption } from "@pages/address_old/type";

@Component({
    selector: "supr-update-address-field",
    template: `
        <div class="updateAddressContainer">
            <div class="wrapper">
                <div class="suprRow">
                    <supr-text type="subtitle">{{ title }}</supr-text>
                </div>
                <div class="divider24"></div>

                <div class="suprRow">
                    <supr-input-label
                        textType="body"
                        [placeholder]="placeholder"
                        [label]="label"
                        [value]="addressComponentValue"
                        (handleInputChange)="onInputChange($event)"
                    ></supr-input-label>
                </div>
            </div>

            <div class="divider24"></div>
            <supr-button
                [disabled]="!addressComponentValue"
                [saObjectName]="saObjectName"
                [saObjectValue]="addressComponentValue"
                [saContext]="saContext"
                (handleClick)="updateAddressField()"
            >
                <supr-text type="body">${BUTTON_TEXT.UPDATE}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateAddressComponent implements OnInit {
    @Input() title: string;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() saObjectName: string;
    @Input() saContext: string;
    @Input() initialValue: string;

    @Output() handleUpdateClick: EventEmitter<
        FlatSearchOption
    > = new EventEmitter();

    addressComponentValue = "";

    ngOnInit() {
        this.setInputValue();
    }

    updateAddressField() {
        this.handleUpdateClick.emit({
            id: -1,
            name: this.addressComponentValue,
        });
    }

    onInputChange(text: string) {
        this.addressComponentValue = text;
    }

    private setInputValue() {
        if (this.initialValue) {
            this.addressComponentValue = this.initialValue;
        }
    }
}
