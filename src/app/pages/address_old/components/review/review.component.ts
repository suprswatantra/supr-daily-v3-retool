import {
    Component,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    SimpleChange,
} from "@angular/core";

import { Address } from "@models";

import { UtilService } from "@services/util/util.service";
import { AddressService } from "@services/shared/address.service";
import { ErrorService } from "@services/integration/error.service";
import { MoengageService } from "@services/integration/moengage.service";

import { AddressCurrentState } from "@store/address/address.state";

import { SuprApi } from "@types";

import {
    ADDRESS_DISPLAY_TEXT,
    SA_OBJECT_NAMES,
} from "@pages/address_old/constants/";
import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";

@Component({
    selector: "supr-address-review",
    templateUrl: "./review.component.html",
    styleUrls: ["../../styles/review.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewComponent implements OnInit, OnChanges {
    @Input() address: Address;
    @Input() addressState: AddressCurrentState;
    @Input() errorMsg: any;
    @Output()
    handleUpdateAddress: EventEmitter<SuprApi.AddressBody> = new EventEmitter();

    @ViewChild("input", { static: true }) inputEl: ElementRef;

    updatingAddress = false;
    isApartment = true;
    addressDisplayText = ADDRESS_DISPLAY_TEXT;
    saObjectNameConfirmLocation = SA_OBJECT_NAMES.CLICK.CONFIRM_LOCATION;
    saContext: string;

    constructor(
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private errorService: ErrorService,
        private addressService: AddressService,
        private moengageService: MoengageService,
        private addressPageService: AddressPageService
    ) {}

    ngOnInit() {
        this.hydrateAddressData();
        this.setIsApartment();
        this.setInstruction();
        this.setAnalyticsData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleAddressStateChange(changes["addressState"]);
    }

    updateAddress() {
        this.updatingAddress = true;
        this.cdr.detectChanges();

        this.addressPageService.address.instructions = this.inputEl.nativeElement.value;

        const data = this.addressPageService.getAddressRequestData();
        this.handleUpdateAddress.emit(data);
    }

    setIsApartment() {
        if (this.addressPageService.isSocietySelected()) {
            this.isApartment = true;
        } else {
            this.isApartment = false;
        }
    }

    private handleAddressStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (this.hasAddressChanged(change)) {
            if (this.errorMsg) {
                this.handleUpdateAddressFailure();
            } else {
                this.handleUpdateAddressSuccess();
            }
        }
    }

    private hasAddressChanged(change: SimpleChange) {
        return (
            (change.previousValue === AddressCurrentState.CREATING_ADDRESS ||
                change.previousValue ===
                    AddressCurrentState.REFRESHING_ON_HUB_CHANGE) &&
            change.currentValue === AddressCurrentState.NO_ACTION
        );
    }

    private handleUpdateAddressSuccess() {
        this.updatingAddress = false;
        this.sendAnalyticsEvents();
        this.addressPageService.resetAddress();
        this.addressPageService.showSuccessToast();
        this.resetTempAddressData();
        this.addressPageService.redirectBack();
    }

    private handleUpdateAddressFailure() {
        this.updatingAddress = false;
        this.errorService.handleCustomError(this.errorMsg);
    }

    private hydrateAddressData() {
        const addressDataInService = this.addressPageService.address;
        if (
            !addressDataInService.streetAddress &&
            this.address &&
            this.address.streetAddress
        ) {
            this.addressPageService.address = { ...this.address };
        }
    }

    private setInstruction() {
        const instructions = this.utilService.getNestedValue(
            this.addressPageService.address,
            "instructions",
            ""
        );

        this.inputEl.nativeElement.value = instructions;
    }

    private setAnalyticsData() {
        const addressType = this.addressPageService.getAddressType();
        const addressContext = this.addressService.getAddressContext();
        this.saContext = `${addressType}-${addressContext}`;
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackAddressChange({
            address_type: this.address.type,
        });
    }
    
    private resetTempAddressData() {
        const location = this.utilService.getNestedValue(
            this.address,
            "location.latLong"
        );

        this.addressService.resetTempAddressData();
        this.addressService.setTempAddressData(this.address, "address");
        this.addressService.setTempAddressData(location, "locationData");
    }
}
