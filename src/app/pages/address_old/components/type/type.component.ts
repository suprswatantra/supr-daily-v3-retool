import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

import { Address } from "@models";

import { Type } from "@pages/address_old/type";
import {
    APARTMENT_TYPE,
    INDEPENDENT_TYPE,
    REQUEST_TYPE,
    ADDRESS_DISPLAY_TEXT,
} from "@pages/address_old/constants";
import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";

@Component({
    selector: "supr-address-type",
    template: `
        <div class="type">
            <div class="typeBody">
                <supr-text type="subtitle">
                    ${ADDRESS_DISPLAY_TEXT.RESIDENCE_TYPE}
                </supr-text>

                <div class="divider16"></div>

                <div class="typeBodyTile suprRow">
                    <supr-address-type-tile
                        [cardType]="APARTMENT"
                        (click)="selectCard(APARTMENT)"
                    ></supr-address-type-tile>

                    <div class="spacer8"></div>

                    <supr-address-type-tile
                        [cardType]="INDEPENDENT"
                        (click)="selectCard(INDEPENDENT)"
                    ></supr-address-type-tile>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/type.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypeComponent implements OnInit, OnDestroy {
    @Input() address: Address;
    @Input() fromCart: boolean;
    @Output() handleTypeChange: EventEmitter<string> = new EventEmitter();
    @Output() handleDisableButton: EventEmitter<boolean> = new EventEmitter();

    APARTMENT = { ...APARTMENT_TYPE };
    INDEPENDENT = { ...INDEPENDENT_TYPE };

    isButtonDisabled = true;
    selectedType: string;

    constructor(private addressPageService: AddressPageService) {}

    ngOnInit() {
        this.setInitData();

        const selectionType = this.addressPageService.address.type;
        if (selectionType && selectionType !== "skip") {
            this.selectedType = selectionType;
            this.isButtonDisabled = false;
        }

        this.toggleDisableButton();
    }

    ngOnDestroy() {
        this.addressPageService.resetAddress();
        this.resetCards();
    }

    selectCard(card: Type) {
        if (!card.selected) {
            card.selected = true;
            this.handleTypeChange.emit(card.type);
        }

        if (card.type === this.APARTMENT.type) {
            this.INDEPENDENT.selected = false;
        } else {
            this.APARTMENT.selected = false;
        }

        if (this.APARTMENT.selected || this.INDEPENDENT.selected) {
            this.isButtonDisabled = false;
        } else {
            this.isButtonDisabled = true;
        }

        if (card.type === REQUEST_TYPE.individualHouse) {
            this.selectedType = card.type;
        } else {
            this.handleSocietyType(card);
        }

        this.toggleDisableButton();
    }

    private handleSocietyType(card: Type) {
        this.selectedType =
            this.address.type &&
            this.address.type === REQUEST_TYPE.unknownSociety &&
            this.address.streetAddress
                ? REQUEST_TYPE.unknownSociety
                : card.type;
    }

    private setInitData() {
        // Set the address exit redirect flag
        this.addressPageService.setCartRedirectFlag(this.fromCart);
    }

    private toggleDisableButton() {
        this.handleDisableButton.emit(this.isButtonDisabled);
    }

    private resetCards() {
        this.APARTMENT = { ...APARTMENT_TYPE };
        this.INDEPENDENT = { ...INDEPENDENT_TYPE };
    }
}
