import { Component, ChangeDetectionStrategy } from "@angular/core";

import { ADDRESS_DISPLAY_TEXT } from "@pages/address_old/constants";

@Component({
    selector: "supr-address-type-header",
    template: `
        <div class="typeHeader">
            <supr-page-header>
                <supr-text type="subheading">
                    ${ADDRESS_DISPLAY_TEXT.RESIDENCE_TYPE}
                </supr-text>
            </supr-page-header>

            <supr-text type="title">
                ${ADDRESS_DISPLAY_TEXT.DELIVERY_TEXT}
            </supr-text>

            <div class="divider16"></div>
        </div>
    `,
    styleUrls: ["../../styles/type.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypeHeaderComponent {}
