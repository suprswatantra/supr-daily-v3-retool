import { Component, Input, OnInit } from "@angular/core";

import { SA_OBJECT_NAMES } from "@pages/address_old/constants/";
import { Type } from "@pages/address_old/type";
import { AddressService } from "@services/shared/address.service";

@Component({
    selector: "supr-address-type-tile",
    template: `
        <div
            class="typeBodyTileCard"
            [class.approve]="cardType.selected"
            saClick
            saObjectName="${SA_OBJECT_NAMES.CLICK.RESIDENCY_TYPE}"
            [saObjectValue]="cardType.type"
            [saContext]="saContext"
        >
            <div class="typeBodyTileCardSvg">
                <supr-icon
                    name="approve_filled"
                    class="typeBodyTileCardSvgApprove"
                    *ngIf="cardType.selected"
                ></supr-icon>

                <supr-svg [ngClass]="cardType.svgName"></supr-svg>
            </div>
            <div
                class="typeBodyTileCardText"
                [class.approve]="cardType.selected"
            >
                <supr-text type="body" class="type">
                    {{ cardType.text }}
                </supr-text>
                <supr-text type="caption" class="disc">
                    {{ cardType.disc }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/type.page.scss"],
})
export class TileComponent implements OnInit {
    @Input() cardType: Type;

    saContext: string;

    constructor(private addressService: AddressService) {}

    ngOnInit() {
        this.saContext = this.addressService.getAddressContext();
    }
}
