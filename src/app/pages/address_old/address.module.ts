import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";

import { IonicModule } from "@ionic/angular";

import { environment } from "@environments/environment";

import { SharedModule } from "@shared/shared.module";

import { AddressRoutingModule } from "./address-routing.module";

import { AddressPageLayoutComponent } from "./layouts/core.layout";
import { TypePageLayoutComponent } from "./layouts/type.layout";
import { FormPageLayoutComponent } from "./layouts/form.layout";
import { SearchPageLayoutComponent } from "./layouts/search.layout";
import { MapPageLayoutComponent } from "./layouts/map.layout";
import { ReviewPageLayoutComponent } from "./layouts/review.layout";

import { addressComponents } from "./components";
import { addressContainers } from "./containers";
import { addressServices } from "./services";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AddressRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: environment.mapKey.android,
        }),
        SharedModule,
    ],
    declarations: [
        AddressPageLayoutComponent,
        TypePageLayoutComponent,
        FormPageLayoutComponent,
        SearchPageLayoutComponent,
        MapPageLayoutComponent,
        ReviewPageLayoutComponent,
        ...addressComponents,
        ...addressContainers,
    ],
    providers: [...addressServices, GoogleMapsAPIWrapper],
})
export class AddressOldPageModule {}
