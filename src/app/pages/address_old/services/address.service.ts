import { Injectable } from "@angular/core";

import { LOCAL_DB_DATA_KEYS, TOAST_MESSAGES } from "@constants";
import { Address } from "@models";

import { DbService } from "@services/data/db.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";

import { REQUEST_TYPE } from "../constants";

@Injectable()
export class AddressService {
    isInitialValueCounter = false;
    DOOR_NUMBER = "doorNumber";
    CITY = "city";

    empty_address = {
        id: 0,
        type: "",
        floorNumber: "",
        societyName: "",
        buildingName: "",
        doorNumber: "",
        streetAddress: "",
        societyId: "",
        buildingId: "",
        premiseId: "",
        doorId: "",
        location: {
            latLong: {
                latitude: 0,
                longitude: 0,
            },
            reverseGeocode: [],
        },
        instructions: "",
        city: {
            id: 0,
            name: "",
        },
        hub: {
            deliveryFee: 0,
            id: 0,
            name: "",
            cutOffTime: "",
        },
        from: "",
    };

    address: Address = JSON.parse(JSON.stringify(this.empty_address));

    constructor(
        private dbService: DbService,
        private routerService: RouterService,
        private toastService: ToastService
    ) {}

    // setResidenceType(type: string, resetAddress: boolean = false) {
    setResidenceType(type: string) {
        if (this.address.type !== type) {
            // if (resetAddress) {
            this.address = JSON.parse(JSON.stringify(this.empty_address));
            // }

            this.address.type = type;
        }
    }

    setAddressForm(key: string, value: any) {
        this.address = { ...this.address, [key]: value };
    }

    getAddressRequestData() {
        const type = this.address.type;
        const address = this.address;

        if (type === REQUEST_TYPE.individualHouse) {
            return this.getIndividualHouse(address);
        } else if (type === REQUEST_TYPE.knownSociety) {
            return this.getKnownSocietyReq(address);
        } else if (type === REQUEST_TYPE.unknownSociety) {
            return this.getUnknownSociety(address);
        }
    }

    isSocietySelected() {
        if (
            this.address.type === REQUEST_TYPE.knownSociety ||
            this.address.type === REQUEST_TYPE.unknownSociety
        ) {
            return true;
        }

        return false;
    }

    setCartRedirectFlag(flag = false) {
        this.dbService.setLocalData(LOCAL_DB_DATA_KEYS.REDIRECT_TO_CART, flag);
    }

    getCartRedirectFlag(): boolean {
        return this.dbService.getLocalData(LOCAL_DB_DATA_KEYS.REDIRECT_TO_CART);
    }

    setFormInputValue(data: any, keyName: string, formInput: any) {
        if (!formInput) {
            return null;
        }

        for (let i = 0; i < formInput.length; i++) {
            if (formInput[i].key === keyName) {
                if (keyName === this.DOOR_NUMBER) {
                    formInput[i].value = data;
                } else if (keyName === this.CITY) {
                    formInput[i].value = data.name;
                } else {
                    formInput[i].value = data;
                }
                formInput[i].error = "";
                break;
            }
        }

        formInput = [...formInput];
        return formInput;
    }

    setFormInputError(keyName: string, formInput: any) {
        if (!formInput) {
            return null;
        }

        for (let i = 0; i < formInput.length; i++) {
            if (formInput[i].key === keyName) {
                formInput[i].error = `${formInput[i].name} can't be blank`;
                break;
            }
        }

        formInput = [...formInput];
        return formInput;
    }

    setSearchOptionLists(data: any, formInput: any, keyName: string) {
        let tempData = [];

        tempData = data.map(dt => {
            return { id: dt.id, name: dt.name };
        });

        for (let i = 0; i < formInput.length; i++) {
            if (formInput[i].key === keyName) {
                formInput[i].searchList = tempData;
                break;
            }
        }

        return formInput;
    }

    setLatLong(lat: number, lng: number) {
        const location = {
            latLong: {
                latitude: lat,
                longitude: lng,
            },
            reverseGeocode: this.address.location.reverseGeocode,
        };

        this.address = {
            ...this.address,
            ["location"]: location,
        };
    }

    isKnownSociety(): boolean {
        const { type } = this.address;
        if (type === REQUEST_TYPE.knownSociety) {
            return true;
        }

        return false;
    }

    setDataToAddressService(address: Address) {
        for (const key in address) {
            if (address.hasOwnProperty(key)) {
                this.address[key] = address[key];
            }
        }
    }

    resetAddress(type: string = "", city?: { id: number; name: string }) {
        const emptyAddressJson = JSON.parse(JSON.stringify(this.empty_address));

        if (type) {
            this.address = {
                ...emptyAddressJson,
                ["type"]: type,
                ["city"]: city,
            };
        } else {
            this.address = emptyAddressJson;
        }
    }

    getAddressType(): string {
        return this.address.type;
    }

    getAddress(): Address {
        return this.address;
    }

    redirectBack() {
        const toCart = this.getCartRedirectFlag();
        if (toCart) {
            this.routerService.goToCartPage({ replaceUrl: true });
        } else {
            this.routerService.goToProfilePage({ replaceUrl: true });
        }
    }

    showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.ADDRESS.SUCCESS);
    }

    private getIndividualHouse(address: Address) {
        return {
            type: address.type,
            floorNumber: address.floorNumber,
            doorNumber: address.doorNumber,
            streetAddress: address.streetAddress,
            cityId: address.city.id ? address.city.id : 0,
            location: {
                latLong: {
                    latitude: address.location.latLong.latitude,
                    longitude: address.location.latLong.longitude,
                },
                reverseGeocode: address.location.reverseGeocode,
            },
            instructions: address.instructions,
        };
    }

    private getKnownSocietyReq(address: Address) {
        return {
            type: address.type,
            societyId: address.societyId,
            buildingId: address.buildingId,
            doorId: address.doorId,
        };
    }

    private getUnknownSociety(address: Address) {
        return {
            type: "unknownSociety",
            societyName: address.societyName,
            buildingName: address.buildingName,
            doorNumber: address.doorNumber,
            streetAddress: address.streetAddress,
            cityId: address.city.id,
            location: {
                latLong: {
                    latitude: address.location.latLong.latitude,
                    longitude: address.location.latLong.longitude,
                },
                reverseGeocode: address.location.reverseGeocode,
            },
            instructions: address.instructions,
        };
    }
}
