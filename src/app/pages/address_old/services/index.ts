import { AddressPageAdapter } from "./address.adapter";
import { AddressService } from "./address.service";

export const addressServices = [AddressPageAdapter, AddressService];
