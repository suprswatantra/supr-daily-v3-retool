export interface Type {
    type?: string;
    svgName?: string;
    text?: string;
    disc?: string;
    selected?: boolean;
}

export interface InputChangeType {
    label?: string;
    type?: string;
    key?: any;
    value?: string;
}

export interface FlatSearchOption {
    id: number;
    name: string;
}
