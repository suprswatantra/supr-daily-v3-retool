import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { Address } from "@models";
import { AddressService } from "@services/shared/address.service";
import { SuprApi } from "@types";

import { AddressService as AddressPageService } from "@pages/address_old/services/address.service";
import { AddressPageAdapter as Adapter } from "@pages/address_old/services/address.adapter";

import { AddressCurrentState } from "@store/address/address.state";

@Component({
    selector: "supr-address-form-container",
    template: `
        <supr-address-form-layout
            [address]="address$ | async"
            [premiseAddresses]="premiseAddresses$ | async"
            [addressState]="addressState$ | async"
            [errorMsg]="errorMsg$ | async"
            (handleUpdateAddress)="updateAddress()"
            (handleSearchPremiseAddress)="searchPremiseAddress($event)"
        ></supr-address-form-layout>
    `,
    styleUrls: ["../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPageContainer implements OnInit {
    address$: Observable<Address>;
    premiseAddresses$: Observable<SuprApi.PremiseAddressRes>;
    errorMsg$: Observable<any>;
    addressState$: Observable<AddressCurrentState>;

    constructor(
        private adapter: Adapter,
        private addressService: AddressService,
        private addressPageService: AddressPageService
    ) {}

    ngOnInit() {
        this.address$ = this.adapter.address$;
        this.premiseAddresses$ = this.adapter.premiseAddress$;
        this.errorMsg$ = this.adapter.errorMsg$;
        this.addressState$ = this.adapter.addressState$;
    }

    updateAddress() {
        const data = this.addressPageService.getAddressRequestData();
        const addressContext = this.addressService.getAddressContext();
        const context = `${data.type}-${addressContext}`;
        this.adapter.updateAddress(data, { context });
    }

    searchPremiseAddress(id: number) {
        this.adapter.searchPremiseAddress(id);
    }
}
