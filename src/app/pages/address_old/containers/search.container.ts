import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { map } from "rxjs/operators";
import { Observable } from "rxjs";

import { Address } from "@models";
import { SuprApi } from "@types";

import { AddressPageAdapter as Adapter } from "@pages/address_old/services/address.adapter";

@Component({
    selector: "supr-address-search-container",
    template: `
        <supr-address-search-layout
            [from]="from$ | async"
            [premise]="premise$ | async"
            [address]="address$ | async"
            [isFetchingPremiseList]="isFetchingPremiseList$ | async"
            (handleSearchPremise)="searchPremise($event)"
            (handleSearchPremiseAddress)="searchPremiseAddress($event)"
        ></supr-address-search-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageContainer implements OnInit {
    from$: Observable<string>;
    premise$: Observable<SuprApi.PremiseSearchRes[]>;
    address$: Observable<Address>;
    isFetchingPremiseList$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.from$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["from"])
        );

        this.premise$ = this.adapter.premise$;
        this.address$ = this.adapter.address$;
        this.isFetchingPremiseList$ = this.adapter.isFetchingPremiseList$;
    }

    searchPremise(searchObj: SuprApi.PremiseSearchReq) {
        this.adapter.searchPremise(searchObj);
    }

    searchPremiseAddress(id: number) {
        this.adapter.searchPremiseAddress(id);
    }
}
