import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { AddressPageAdapter as Adapter } from "@pages/address_old/services/address.adapter";

@Component({
    selector: "supr-address-map-container",
    template: `
        <supr-address-map-layout
            [selectionType]="selectionType$ | async"
            [from]="from$ | async"
        ></supr-address-map-layout>
    `,
    styleUrls: ["../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapPageContainer implements OnInit {
    selectionType$: Observable<string>;
    from$: Observable<string>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.selectionType$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["selectionType"])
        );

        this.from$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["from"])
        );
    }
}
