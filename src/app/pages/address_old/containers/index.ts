import { MapPageContainer } from "./map.container";
import { ReviewPageContainer } from "./review.container";
import { SearchPageContainer } from "./search.container";
import { FormPageContainer } from "./form.container";
import { TypePageContainer } from "./type.container";

export const addressContainers = [
    MapPageContainer,
    ReviewPageContainer,
    SearchPageContainer,
    FormPageContainer,
    TypePageContainer,
];
