import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Address } from "@models";
import { AddressPageAdapter as Adapter } from "@pages/address_old/services/address.adapter";

@Component({
    selector: "supr-address-type-container",
    template: `
        <supr-address-type-layout
            [address]="address$ | async"
            [fromCart]="fromCart$ | async"
        ></supr-address-type-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypePageContainer implements OnInit {
    address$: Observable<Address>;
    fromCart$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.address$ = this.adapter.address$;
        this.fromCart$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["redirect"] === "cart")
        );
    }
}
