import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { Address } from "@models";
import { AddressService } from "@services/shared/address.service";
import { AddressCurrentState } from "@store/address/address.state";
import { SuprApi } from "@types";

import { AddressPageAdapter as Adapter } from "@pages/address_old/services/address.adapter";

@Component({
    selector: "supr-address-review-container",
    template: `
        <supr-address-review-layout
            [address]="address$ | async"
            [addressState]="addressState$ | async"
            [errorMsg]="errorMsg$ | async"
            (handleUpdateAddress)="updateAddress($event)"
        ></supr-address-review-layout>
    `,
    styleUrls: ["../styles/review.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewPageContainer implements OnInit {
    address$: Observable<Address>;
    addressState$: Observable<AddressCurrentState>;
    errorMsg$: Observable<any>;

    constructor(
        private adapter: Adapter,
        private addressService: AddressService
    ) {}

    ngOnInit() {
        this.address$ = this.adapter.address$;
        this.addressState$ = this.adapter.addressState$;
        this.errorMsg$ = this.adapter.errorMsg$;
    }

    updateAddress(data: SuprApi.AddressBody) {
        const addressContext = this.addressService.getAddressContext();
        const context = `${data.type}-${addressContext}`;
        this.adapter.updateAddress(data, { context });
    }
}
