import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ADDRESS_DISPLAY_TEXT } from "@pages/address_old/constants";

@Component({
    selector: "supr-address-map-layout",
    template: `
        <div class="suprContainer">
            <supr-address-header
                headerText="${ADDRESS_DISPLAY_TEXT.LOCATE_ADDRESS}"
            ></supr-address-header>

            <supr-address-map
                [selectionType]="selectionType"
                [from]="from"
            ></supr-address-map>
        </div>
    `,
    styleUrls: ["./../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapPageLayoutComponent {
    @Input() selectionType: string;
    @Input() from: string;
}
