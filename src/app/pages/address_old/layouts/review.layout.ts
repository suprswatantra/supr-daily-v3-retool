import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { Address } from "@models";
import { AddressCurrentState } from "@store/address/address.state";
import { SuprApi } from "@types";

@Component({
    selector: "supr-address-review-layout",
    template: `
        <div class="suprContainer">
            <supr-address-header headerText="Add address"></supr-address-header>

            <div class="suprScrollContent">
                <supr-address-review
                    [address]="address"
                    [addressState]="addressState"
                    [errorMsg]="errorMsg"
                    (handleUpdateAddress)="handleUpdateAddress.emit($event)"
                ></supr-address-review>
            </div>
        </div>
    `,
    styleUrls: ["./../styles/form.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewPageLayoutComponent {
    @Input() address: Address;
    @Input() addressState: AddressCurrentState;
    @Input() errorMsg: any;
    @Output()
    handleUpdateAddress: EventEmitter<SuprApi.AddressBody> = new EventEmitter();
}
