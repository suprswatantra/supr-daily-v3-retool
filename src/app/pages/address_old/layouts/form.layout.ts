import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { Address } from "@models";

import { AddressService } from "@services/shared/address.service";
import { ErrorService } from "@services/integration/error.service";
import { MoengageService } from "@services/integration/moengage.service";
import { UtilService } from "@services/util/util.service";

import { AddressCurrentState } from "@store/address/address.state";

import { SuprApi } from "@types";

import { AddressService as AddressPageService } from "../services/address.service";

@Component({
    selector: "supr-address-form-layout",
    template: `
        <div class="suprContainer">
            <supr-address-header headerText="Add address"></supr-address-header>

            <supr-address-form
                [address]="address"
                [premiseAddresses]="premiseAddresses"
                [addressState]="addressState"
                (handleUpdateAddress)="handleUpdateAddress.emit()"
                (handleSearchPremiseAddress)="
                    handleSearchPremiseAddress.emit($event)
                "
            ></supr-address-form>

            <ng-container *ngIf="isFetchingSocietyDetails">
                <supr-backdrop></supr-backdrop>
            </ng-container>
        </div>
    `,
    styleUrls: ["./../styles/form.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPageLayoutComponent implements OnChanges {
    @Input() address: Address;
    @Input() premiseAddresses: SuprApi.PremiseAddressRes;
    @Input() errorMsg: any;
    @Input() addressState: AddressCurrentState;

    @Output()
    handleUpdateAddress: EventEmitter<void> = new EventEmitter();
    @Output()
    handleSearchPremiseAddress: EventEmitter<number> = new EventEmitter();

    isFetchingSocietyDetails = false;

    constructor(
        private addressService: AddressService,
        private errorService: ErrorService,
        private moengageService: MoengageService,
        private utilService: UtilService,
        private addressPageService: AddressPageService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleAddressStateChange(changes["addressState"]);
    }

    private handleAddressStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }
        this.handleSocietyDetailsFetch(change);

        if (this.hasAddressChanged(change)) {
            if (this.errorMsg) {
                this.handleSaveAddressFailure();
            } else {
                this.handleSaveAddressSuccess();
            }
        }
    }

    private hasAddressChanged(change: SimpleChange) {
        return (
            (change.previousValue === AddressCurrentState.CREATING_ADDRESS ||
                change.previousValue ===
                    AddressCurrentState.REFRESHING_ON_HUB_CHANGE) &&
            change.currentValue === AddressCurrentState.NO_ACTION
        );
    }

    private handleSocietyDetailsFetch(change: SimpleChange) {
        if (
            change.currentValue ===
            AddressCurrentState.SEARCHING_SOCIETY_DETAILS
        ) {
            this.isFetchingSocietyDetails = true;
        } else {
            this.isFetchingSocietyDetails = false;
        }
    }

    private handleSaveAddressSuccess() {
        this.sendAnalyticsEvents();
        this.addressPageService.resetAddress();
        this.addressPageService.showSuccessToast();
        this.resetTempAddressData();
        this.addressPageService.redirectBack();
    }

    private handleSaveAddressFailure() {
        this.errorService.handleCustomError(this.errorMsg);
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackAddressChange({
            address_type: this.address.type,
        });
    }

    private resetTempAddressData() {
        const location = this.utilService.getNestedValue(
            this.address,
            "location.latLong"
        );

        this.addressService.resetTempAddressData();
        this.addressService.setTempAddressData(this.address, "address");
        this.addressService.setTempAddressData(location, "locationData");
    }
}
