import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

import { Address } from "@models";
import { SuprApi } from "@types";

@Component({
    selector: "supr-address-search-layout",
    template: `
        <div class="suprContainer">
            <supr-address-search
                [from]="from"
                [premise]="premise"
                [address]="address"
                [isFetchingPremiseList]="isFetchingPremiseList"
                (handleSearchPremise)="handleSearchPremise.emit($event)"
                (handleSearchPremiseAddress)="
                    handleSearchPremiseAddress.emit($event)
                "
            ></supr-address-search>
        </div>
    `,
    styleUrls: ["./../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageLayoutComponent {
    @Input() from: string;
    @Input() premise: SuprApi.PremiseSearchRes[];
    @Input() address: Address;
    @Input() isFetchingPremiseList: boolean;

    @Output() handleSearchPremise: EventEmitter<
        SuprApi.PremiseSearchReq
    > = new EventEmitter();

    @Output()
    handleSearchPremiseAddress: EventEmitter<number> = new EventEmitter();
}
