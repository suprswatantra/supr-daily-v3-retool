import { AddressService } from "./../services/address.service";
import { Component, ChangeDetectionStrategy, OnDestroy } from "@angular/core";

@Component({
    selector: "supr-address-layout",
    template: `
        <ion-router-outlet
            animated="true"
            [swipeGesture]="false"
        ></ion-router-outlet>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressPageLayoutComponent implements OnDestroy {
    constructor(private addressService: AddressService) {}
    ngOnDestroy() {
        this.addressService.resetAddress();
    }
}
