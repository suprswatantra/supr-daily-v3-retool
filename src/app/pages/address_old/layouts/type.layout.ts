import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { Address } from "@models";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import {
    SA_OBJECT_NAMES,
    SA_OBJECT_VALUES,
} from "../constants/analytics.constants";
import { AddressService as AddressPageService } from "../services/address.service";
import { REQUEST_TYPE } from "../constants";

@Component({
    selector: "supr-address-type-layout",
    template: `
        <div class="suprContainer">
            <supr-address-type-header></supr-address-type-header>

            <div class="suprScrollContent">
                <supr-address-type
                    [address]="address"
                    [fromCart]="fromCart"
                    (handleTypeChange)="onTypeChange($event)"
                    (handleDisableButton)="disableButton($event)"
                ></supr-address-type>
            </div>

            <supr-page-footer>
                <supr-button
                    [disabled]="isButtonDisabled"
                    (handleClick)="goToNext()"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.NEXT_BUTTON}"
                    [saObjectValue]="saObjectValueNextButton"
                    [saContext]="saContext"
                >
                    Next
                </supr-button>
            </supr-page-footer>
        </div>
    `,
    styleUrls: ["./../styles/type.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypePageLayoutComponent implements OnInit {
    @Input() address: Address;
    @Input() fromCart: boolean;

    isButtonDisabled = true;
    saObjectValueNextButton = SA_OBJECT_VALUES.INACTIVE;
    saContext: string;
    selectedType: string;

    constructor(
        private routerService: RouterService,
        private addressService: AddressService,
        private addressPageService: AddressPageService
    ) {}

    ngOnInit() {
        this.saContext = this.addressService.getAddressContext();
    }

    disableButton(isDisable: boolean) {
        this.isButtonDisabled = isDisable;
        this.setSaObjectValue();
    }

    goToNext() {
        if (!this.isButtonDisabled) {
            this.handleServiceDataReset();
            this.routerService.goToAddressFormPage({
                queryParams: {
                    selectionType: this.addressPageService.getAddressType(),
                },
            });
        }
    }

    onTypeChange(type: string) {
        this.selectedType = type;
    }

    private setSaObjectValue() {
        if (this.isButtonDisabled) {
            this.saObjectValueNextButton = SA_OBJECT_VALUES.INACTIVE;
        } else {
            this.saObjectValueNextButton = SA_OBJECT_VALUES.ACTIVE;
        }
    }

    private handleServiceDataReset() {
        const currentAddressType = this.addressPageService.getAddressType();

        /* Reset saved map location on change of type, unknown and known are considered as one type */
        if (currentAddressType !== this.selectedType) {
            if (
                currentAddressType === REQUEST_TYPE.individualHouse ||
                this.selectedType === REQUEST_TYPE.individualHouse
            ) {
                this.addressService.resetTempAddressData();
                this.addressPageService.setResidenceType(
                    this.selectedType
                    // true
                );
            } else if (!currentAddressType) {
                /* Reset address only but not location data when address type changes between known and unknown society */
                this.addressService.setTempAddressData(null, "locationData");
                /* Set the address type for the first time */
                this.addressPageService.setResidenceType(this.selectedType);
            }
        }
    }
}
