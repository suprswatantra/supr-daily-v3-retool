import { MODAL_NAMES } from "@constants";

export * from "./analytics.constants";

export const APARTMENT_TYPE = {
    type: "knownSociety",
    svgName: "apartment",
    text: "Apartment or Flat",
    disc: "A housing complex or society with multiple apartments.",
    selected: false,
};

export const INDEPENDENT_TYPE = {
    type: "individualHouse",
    svgName: "independent",
    text: "Independent House",
    disc: "A stand-alone house that you live in.",
    selected: false,
};

export const REQUEST_TYPE = {
    individualHouse: "individualHouse",
    knownSociety: "knownSociety",
    unknownSociety: "unknownSociety",
};

export const KEY_NAME = {
    DOOR_NUMBER: "doorNumber",
    BUILDING_NAME: "buildingName",
    STREET_ADDRESS: "streetAddress",
    SOCIETY_NAME: "societyName",
    SOCIETY_ID: "societyId",
};

export const INPUT_TYPE = {
    buttonText: "buttonText",
    searchAbleText: "searchAbleText",
    text: "text",
};

export const INDIVIDUAL_FORM_INPUT_FIELDS = {
    floorNumber: "Floor no.",
    doorNumber: "Plot / house no.",
    streetAddress: "Street address",
    city: "City",
};

export const SOCIETY_FORM_INPUT_FIELDS = {
    societyName: "Apartment / Society name",
    buildingName: "Tower / Block no.",
    doorNumber: "Flat no.",
    streetAddress: "Street address",
    city: "City",
};

export const KNOWN_SOCIETY_FORM_INPUT = [
    {
        name: "Apartment / Society name",
        type: INPUT_TYPE.buttonText,
        disabled: false,
        placeholder: "Search for apartments",
        key: "societyName",
        value: "",
    },
    {
        name: "Tower / Block no.",
        type: INPUT_TYPE.searchAbleText,
        disabled: false,
        placeholder: "eg. Tower L",
        key: "buildingName",
        value: "",
        searchList: [],
        modalName: MODAL_NAMES.TOWER_SEARCH,
    },
    {
        name: "Flat no.",
        type: INPUT_TYPE.searchAbleText,
        disabled: false,
        placeholder: "eg. 1604",
        key: "doorNumber",
        value: "",
        searchList: [],
        modalName: MODAL_NAMES.FLAT_SEARCH,
    },
    {
        name: "Street address",
        type: INPUT_TYPE.text,
        disabled: true,
        placeholder: "eg. 26th main,1st sector, HSR Layout",
        key: "streetAddress",
        value: "",
    },
    {
        name: "City",
        type: INPUT_TYPE.text,
        disabled: true,
        placeholder: "eg. Bangalore, Karnataka",
        value: "",
        key: "city",
    },
];
export const UN_KNOWN_SOCIETY_FORM_INPUT = [
    {
        name: "Apartment / Society Name",
        type: INPUT_TYPE.buttonText,
        disabled: false,
        placeholder: "Search for apartments",
        key: "societyName",
        value: "",
    },
    {
        name: "Tower / Block no.",
        type: INPUT_TYPE.text,
        disabled: false,
        placeholder: "eg. Tower L",
        key: "buildingName",
        value: "",
        searchList: [],
    },
    {
        name: "Flat no.",
        type: INPUT_TYPE.text,
        disabled: false,
        placeholder: "eg. 1604",
        key: "doorNumber",
        value: "",
        searchList: [],
    },
    {
        name: "Street address",
        type: INPUT_TYPE.text,
        disabled: false,
        placeholder: "eg. 26th main,1st sector, HSR Layout",
        key: "streetAddress",
        value: "",
    },
    {
        name: "City",
        type: INPUT_TYPE.text,
        disabled: true,
        placeholder: "eg. Bangalore, Karnataka",
        value: "",
        key: "city",
    },
];

export const FORM_INPUTS = {
    individualHouse: [
        {
            name: "Floor no.",
            type: INPUT_TYPE.text,
            disabled: false,
            placeholder: "eg. 3rd floor",
            key: "floorNumber",
            value: "",
        },
        {
            name: "Plot / house no.",
            type: INPUT_TYPE.text,
            disabled: false,
            placeholder: "eg. Plot no. 191 / House no. 24425",
            key: "doorNumber",
            value: "",
        },
        {
            name: "Street address",
            type: INPUT_TYPE.text,
            disabled: false,
            placeholder: "eg. 26th main,1st sector, HSR Layout",
            key: "streetAddress",
            value: "",
        },
        {
            name: "City",
            type: INPUT_TYPE.text,
            disabled: true,
            placeholder: "Bangalore, Karnataka",
            value: "",
            key: "city",
        },
    ],

    knownSociety: KNOWN_SOCIETY_FORM_INPUT,
    unknownSociety: UN_KNOWN_SOCIETY_FORM_INPUT,
};

export const SEARCH_PLACEHOLDER = {
    apartment: "Search for your apartment / society",
    map: "Search",
};

export const BELL_MESSAGE = "We don’t call or ring the doorbell.";

export const MAP = {
    zoom: 18,
    resizeTimeOut: 500,
    currentLocation: "currentLocation",
    default: {
        lat: 20.5937,
        lng: 78.9629,
    },
};

export const ADDRESS_REQUIRED_KEYS = {
    individualHouse: ["doorNumber", "streetAddress", "city"],
    knownSociety: ["doorNumber", "societyName", "city"],
    unknownSociety: ["doorNumber", "societyName", "city", "streetAddress"],
};

export const MAP_TOAST_MESSAGES = {
    serviceable: { icon: "location", text: "Order will be delivered here" },
    unserviceable: {
        icon: "error",
        text: "Sorry, this location is not supported",
    },
    no_permission: {
        icon: "error",
        text: "Please enable location access",
    },
};

export const SEARCH_APARTMENT_INFO_MESSAGE =
    "Enter complete apartment name before selecting";

export const ADDRESS_DISPLAY_TEXT = {
    APARTMENT: {
        line1: "Door on the left after coming up the staircase",
        line2: "Brown door with nameplate",
        line3: "White door right in front of the lift",
    },
    INDIVIDUAL_HOUSE: {
        line1: "Black gate, yellow building",
        line2: "Brown corner house opposite Ratnadeep Supermarket",
        line3: "White building after dental clinic",
    },
    USER_INPUT_ADD_PREFIX: "Add ",
    USER_INPUT_ADD_SUFFIX: " to this address",
    FIND_DOORSTEP: "Help our Supr agent find your doorstep easily",
    FRONT_DOOR_DESC: "Describe your front door or gate",
    ADD_SOCIETY: "Add your society",
    NO_KNOWN_SOCIETY:
        "Seems like we don’t have your apartment / society listed on our platform yet.",
    RESIDENCE_TYPE: "Select residence type",
    DELIVERY_TEXT: "Where should we deliver your order?",
    LOCATE_ADDRESS: "Move pin to exact location",
};

export const CANT_FIND_BLOCK_TEXTS = {
    LIST_ITEM_TEXT: "CAN'T MY FIND MY TOWER / BLOCK",
    MODAL_TITLE: "Can't find your tower / block?",
    MODAL_INPUT_LABEL: "Enter tower / block number",
    MODAL_INPUT_PLACEHOLDER: "eg. Tower L",
};

export const CANT_FIND_FLAT_NO_TEXTS = {
    LIST_ITEM_TEXT: "CAN'T MY FIND MY FLAT NO.",
    MODAL_TITLE: "Can't find your flat number?",
    MODAL_INPUT_LABEL: "Enter flat number",
    MODAL_INPUT_PLACEHOLDER: "eg. 1604",
};

export const SOCIETY_NAME_TEXTS = {
    INFO_TEXT: "Please enter full apartment / society name",
    NAME_EXAMPLE_TEXT: "eg. Princeton Apartment",
    MODAL_TITLE: "Please confirm your apartment / society name",
    MODAL_INPUT_LABEL: "Apartment / Society Name",
    MODAL_INPUT_PLACEHOLDER: "eg. Ashiana Tower",
};

export const BUTTON_TEXT = {
    SAVE: "Save",
    NEXT: "Next",
    UPDATE: "Confirm",
    CONFIRM_N_GO_TO_CART: "Confirm & proceed to cart",
};

export const UNKNOWN_SOCIETY_ID = -1;
