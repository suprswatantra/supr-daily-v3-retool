export const SA_OBJECT_NAMES = {
    CLICK: {
        APARTMENT_SEARCH_OPTION: "apartment-name",
        MAP_SEARCH_BOX: "map-search-box",
        MAP_CURRENT_LOCATION: "current-location",
        MAP_SEARCH_OPTION: "search-results",
        NEXT_BUTTON: "next-button",
        RESIDENCY_TYPE: "residency-type",
        CONFIRM_LOCATION: "confirm-location",
        ADD_SOCIETY: "add-society",
        UPDATE_ADDRESS: "update-address",
        CANT_FIND_BLOCK: "cant-find-block",
        CANT_FIND_FLAT_NO: "cant-find-flat-no",
        ADD_BLOCK: "add-block",
        ADD_FLAT_NO: "add-flat-no",
        UPDATE_BLOCK: "update-block",
        UPDATE_FLAT_NO: "update-flat-no",
        UPDATE_SOCIETY_NAME: "update-society-name",
    },
};

export const SA_OBJECT_VALUES = {
    ACTIVE: "active",
    INACTIVE: "inactive",
};

export const SA_OBJECT_CONTEXT = {
    CREATE_ADDRESS: "create",
    UPDATE_ADDRESS: "update",
};
