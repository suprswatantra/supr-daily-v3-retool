export const TEXTS = {
    SUBSCRIPTION: "Subscriptions",
    ONE_TIME_PURCHASE: "One-time purchase",
    STARTING: "Starting on ",
    TO_BE_DELIVERED: "To be delivered",
    SUBSCRIPTION_RECHARGE: "Subscription recharge",
    TOMORROW: "tomorrow",
    SUBSCRIBE_DESC: "Get deliveries on autopilot",
    SUBSCRIBE: "SUBSCRIBE",
    EDIT_SETTINGS: "EDIT SETTINGS",
    EDIT_DELIVERIES: "EDIT DELIVERIES",
    RESELECT_PLAN: "RESELECT PLAN",
    RESCHEDULE: "RESCHEDULE",
    REMOVE: "REMOVE",
    REMOVE_SUBSCRIPTION_TITLE: "Remove subscription from cart?",
    CONFIRM_REMOVE_SUBSCRIPTION_TEXT:
        "Are you sure you want to remove the product? All the customisations for the subscription will be deleted.",
    REMOVE_FROM_CART: "Remove from cart",
    RESCHEDULE_DELIVERY: "Change delivery date",
    CHANGE_DATE: "Change date",
    PLACING_ORDER: "Placing order",
    ALMOST_THERE: "Almost there...",
    ADD_ADDRESS_TO_CONTINUE: "Add your address to continue",
    CONTINUE: "Continue",
    SOME_ISSUES: "Some issues in the cart",
    RESOLVE_ISSUES: "Please resolve the issues to continue",
    NO_SUBSCRIPTIONS_TITLE: "Nothing added for subscription",
    NO_SUBSCRIPTIONS_SUBTITLE: "Convert any orders above into a subscription.",
    TOTAL_AMOUNT: "Cart amount",
    DELIVERY_FEE: "Service fee",
    FINAL_AMOUNT: "Amount to pay",
    WALLET_BALANCE: "Wallet balance available",
    AMOUNT_TO_ADD: "Amount to add",
    DISCOUNT: "Discount",
    CREDITS_USED: "Supr Credits used",
    CREDITS_GAINED: "You will earn cashback on this order",
    DEL_FEE_REFUND_MESSAGE:
        "Congrats you’re getting service fee refunded as Supr Credits",
    FREE: "Free",
    ADD_ADDRESS: "Add address",
    ADD_ADDRESS_FOR_APPLY_COUPON: "Add address to apply the coupon",
    ADD_ADDRESS_TO_APPLY_COUPON_MESSAGE:
        "Please add your complete address to apply the coupon.",
    CART_ITEM_OOS_MESSAGE: "Product is out of stock, remove to proceed.",
    CART_NO_OF_DELIVERY_SUB:
        "Total no. of deliveries for subscriptions may be adjusted in future as per market rates.",
    SUB_MESSAGE_WITH_NO_COUNT:
        "Get your groceries on auto-pilot with subscriptions",
    SCHEDULE_FOR_FUTURE_DATE: "Schedule for a future date",
    SUPR_PASS: "Supr Access",
    SUBSCRIPTIONS_HEADER: {
        icon: "refresh",
        title: "Subscriptions",
        subtitle: "Get your groceries on auto-pilot with subscriptions",
    },
    PAST_ORDER_HEADER: {
        icon: "star_outline",
        title: "Quickly add items from past",
        subtitle: "Frequently bought by you",
    },
    PAYMENT_HEADER: {
        icon: "bill",
        title: "Payment",
        subtitle: "Save on service fee with Supr Access",
    },
    SUPR_PASS_TILE: {
        START: "Start date ",
        END: "Will be valid till ",
    },
    SCHEDULE_HEADER_SUBTITLE: "already scheduled on this day",
    SCHEDULE_HEADER_NO_SCHEDULES: "Nothing scheduled on this day yet",
    SUBSCRIBE_SUPR_PASS: "Subscribe",
    APPLY_COUPON: "Apply coupon",
    APPLY_COUPON_SUBTITLE: "Enter promo code below",
    OFFERS: "Offers",
    APPLY: "Apply",
};

export const ANIMATION_INTERVAL = 4000; // in ms

export enum ITEM_HEADER_ACTION_TYPES {
    CHANGE_DATE = 1,
    EDIT_SETTINGS = 2,
    REMOVE_FROM_CART = 3,
    EDIT_DELIVERIES = 4,
}

export const SUPR_CREDITS_DEFAULT_TEXTS = {
    HEADING: {
        SUB_TEXT_USE: "Use",
        SUB_TEXT_CREDITS: "Supr Credits!",
    },
    INFO: {
        SUB_TEXT_INFO: "Supr Credits are only applicable on non-milk products",
    },
    LEARN: "Learn more",
    NOT_APPLICABLE_DISCLAIMER: "Add non-milk products to use",
    NOT_APPLICABLE_HEADING: "Supr Credits not applicable",
};

export const SUPR_CREDITS_CLASS_NAMES = {
    USABLE: "usable",
    NON_USABLE: "nonUsable",
};

export const DURATION = {
    MONTH_TEXT: "Month",
    DAYS_IN_MONTH: 30,
    PLURAL_CHARACTER: "s",
};

export const SUPR_PASS_TEXTS = {
    SUPR_PASS_APPLIED: "Supr Access Membership Applied!",
    SUPR_PASS: "Supr Access",
    APPLY_SUPR_PASS: "Want zero service fees? Get ",
};

export const ADD_SUPR_PASS_PROMPT_TEXTS = {
    TITLE: {
        FOLD_1: "Yay! You could save",
        FOLD_2: "on this order",
    },
    SUB_TITLE:
        "We have added Supr Access to your cart. Become an access member and get unlimited free deliveries + exclusive discounts with Supr Access starting ₹99",
    PRIMARY_BUTTON_TEXT: "Okay got it",
    SECONDARY_BUTTON_TEXT: "Remove",
};

export const SAVINGS_TEXTS = {
    TOTAL_SAVINGS: "Total saving : ",
    SUPR_PASS_DISCOUNT: "Supr Access exclusive item discount",
    SERVICE_FEE_SAVINGS: "Saving on service fee",
    SUPR_CREDITS_USED: "Supr Credits used",
    ITEM_DISCOUNT: "Item discount",
    COUPON_DISCOUNT: "Coupon discount",
};

export const DEFAULT_SUPR_PASS_LOGO_URL = "assets/images/app/supr-pass.svg";

export const APPLY_COUPON_ICON = "offers_hexagon";
