import { CartItem } from "@models";

export interface CartItemsGroup {
    subscriptionList: CartItem[];
    oneTimeList: CartItem[];
}

export interface CartItemsGroupByDate {
    subscriptionList: Array<ListItemGroupedByDate>;
    oneTimeList: Array<ListItemGroupedByDate>;
}

export interface ListItemGroupedByDate {
    date: string;
    items: CartItem[];
}

export interface GroupedBySlot {
    date: string;
    ALTERNATE_DELIVERY: CartItem[];
    REGULAR_DELIVERY: CartItem[];
}
