import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";

@Component({
    selector: "supr-cart-coupon",
    template: `
        <supr-coupon
            [couponCode]="couponCode"
            [cartCurrentState]="cartCurrentState"
            (applyCoupon)="onApplyCoupon($event)"
            (removeCoupon)="updateCoupon.emit()"
        ></supr-coupon>
        <supr-modal
            *ngIf="showModal"
            modalName="${MODAL_NAMES.COUPON_ADD_ADDRESS}"
            (handleClose)="closeModal()"
        >
            <supr-cart-coupon-add-address
                (handleAddAddress)="closeModal()"
            ></supr-cart-coupon-add-address>
        </supr-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponComponent {
    @Input() couponCode: CouponCode;
    @Input() cartCurrentState: CartCurrentState;
    @Input() hasAddress: boolean;

    @Output() updateCoupon: EventEmitter<string> = new EventEmitter();

    showModal: boolean;

    constructor(private cdr: ChangeDetectorRef) {}

    onApplyCoupon(couponCode: string) {
        if (this.hasAddress) {
            this.updateCoupon.emit(couponCode);
        } else {
            this.openModal();
        }
    }

    closeModal() {
        this.showModal = false;
        this.cdr.detectChanges();
    }

    private openModal() {
        this.showModal = true;
        this.cdr.detectChanges();
    }
}
