import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
    OnInit,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import {
    ADD_SUPR_PASS_PROMPT_TEXTS,
    DEFAULT_SUPR_PASS_LOGO_URL,
} from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

import { SuprPassPrompt } from "@shared/models";

import { ModalService } from "@services/layout/modal.service";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-cart-auto-add-supr-pass-prompt",
    template: `
        <ng-container *ngIf="showModal && suprPassPrompt?.title">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.AUTO_ADD_SUPR_PASS_PROMPT}"
            >
                <div class="wrapper">
                    <ng-container *ngIf="suprPassPrompt?.title">
                        <div class="suprRow">
                            <supr-text type="subtitle">
                                {{ suprPassPrompt?.title }}
                            </supr-text>
                        </div>
                        <div class="divider28"></div>
                    </ng-container>

                    <ng-container
                        *ngIf="
                            suprPassPrompt?.savingsText &&
                            suprPassPrompt.savingsText.length
                        "
                    >
                        <div class="suprRow top">
                            <div class="savingsTexts">
                                <ng-container
                                    *ngFor="
                                        let text of suprPassPrompt.savingsText
                                    "
                                >
                                    <div class="suprRow top">
                                        <supr-icon name="tick"></supr-icon>
                                        <supr-text type="subheading">
                                            {{ text }}
                                        </supr-text>
                                    </div>
                                    <div class="divider8"></div>
                                </ng-container>
                            </div>
                            <div class="logo">
                                <div class="imgWrapper">
                                    <img
                                        [src]="
                                            suprPassPrompt?.logoUrl ||
                                            defaultImgUrl
                                        "
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="divider4"></div>
                    </ng-container>

                    <ng-container *ngIf="suprPassPrompt?.subTitle">
                        <supr-text type="body" class="subtitle">
                            {{ suprPassPrompt?.subTitle }}
                        </supr-text>
                        <div class="divider24"></div>
                    </ng-container>

                    <ng-container *ngIf="suprPassPrompt?.warningText">
                        <supr-text type="body" class="warning">
                            {{ suprPassPrompt?.warningText }}
                        </supr-text>
                        <div class="divider24"></div>
                    </ng-container>

                    <div class="actionButtons">
                        <div class="buttonWrapper">
                            <supr-button
                                class="remove"
                                (handleClick)="secondaryBtnClick()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .AUTO_ADD_SUPR_PASS_PROMPT_REMOVE}"
                            >
                                <supr-text type="body">
                                    {{ secondaryButtonText }}
                                </supr-text>
                            </supr-button>
                        </div>
                        <div class="divider16"></div>
                        <div class="buttonWrapper">
                            <supr-button
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .AUTO_ADD_SUPR_PASS_PROMPT_OK}"
                                (handleClick)="primaryBtnClick()"
                            >
                                <supr-text type="body">
                                    {{ primaryButtontext }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/add-supr-pass-prompt.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddSuprPassPromptComponent implements OnInit, OnChanges {
    @Input() showModal: boolean;
    @Input() suprPassPrompt: SuprPassPrompt;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();
    @Output() handleRemoveSuprAccess: EventEmitter<void> = new EventEmitter();

    primaryButtontext: string;
    secondaryButtonText: string;
    defaultImgUrl = DEFAULT_SUPR_PASS_LOGO_URL;

    constructor(
        private modalService: ModalService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["showModal"] || changes["suprPassPrompt"];
        this.handleChange(change);
    }

    hideModal() {
        this.modalService.closeModal();
    }

    primaryBtnClick() {
        this.hideModal();
    }

    secondaryBtnClick() {
        this.handleRemoveSuprAccess.emit();
        this.hideModal();
    }

    private handleChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        if (!this.showModal) {
            return;
        }

        const promptText = this.settingsService.getSettingsValue(
            "autoAddSuprPassPrompt",
            ADD_SUPR_PASS_PROMPT_TEXTS
        );

        this.primaryButtontext = promptText.PRIMARY_BUTTON_TEXT;
        this.secondaryButtonText = promptText.SECONDARY_BUTTON_TEXT;
    }
}
