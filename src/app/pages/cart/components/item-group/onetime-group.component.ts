import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { CartItemDeliveryType, CartSlotInfo } from "@models";

import { ListItemGroupedByDate, GroupedBySlot } from "@pages/cart/types";
import { TEXTS } from "@pages/cart/constants";

@Component({
    selector: "supr-cart-item-onetime-group",
    template: `
        <div class="itemGroupWrapper" *ngIf="_cartItems?.length">
            <ng-container
                *ngFor="
                    let listItem of _cartItemsGroupedBySlot;
                    let last = last
                "
            >
                <supr-cart-day-header [date]="listItem.date">
                </supr-cart-day-header>

                <div class="divider16"></div>

                <ng-container
                    *ngIf="
                        listItem.REGULAR_DELIVERY &&
                        listItem.REGULAR_DELIVERY.length
                    "
                >
                    <supr-delivery-slot-chip
                        [title]="slotInfo?.REGULAR_DELIVERY?.title"
                        slot="${CartItemDeliveryType.REGULAR_DELIVERY}"
                    >
                    </supr-delivery-slot-chip>
                    <div class="divider12"></div>
                    <ng-container
                        *ngFor="
                            let item of listItem.REGULAR_DELIVERY;
                            let innerLast = last
                        "
                    >
                        <div class="itemWrapper">
                            <supr-cart-item-wrapper-container
                                [cartItem]="item"
                            ></supr-cart-item-wrapper-container>
                            <div class="divider16" *ngIf="innerLast"></div>
                            <div class="divider12" *ngIf="!innerLast"></div>
                        </div>
                    </ng-container>
                </ng-container>

                <ng-container
                    *ngIf="
                        listItem.ALTERNATE_DELIVERY &&
                        listItem.ALTERNATE_DELIVERY.length
                    "
                >
                    <supr-delivery-slot-chip
                        [title]="slotInfo?.ALTERNATE_DELIVERY?.title"
                        slot="${CartItemDeliveryType.ALTERNATE_DELIVERY}"
                    >
                    </supr-delivery-slot-chip>
                    <div class="divider12"></div>
                    <ng-container
                        *ngFor="
                            let item of listItem.ALTERNATE_DELIVERY;
                            let innerLast = last
                        "
                    >
                        <div class="itemWrapper">
                            <supr-cart-item-wrapper-container
                                [cartItem]="item"
                            ></supr-cart-item-wrapper-container>
                            <div class="divider16" *ngIf="innerLast"></div>
                            <div class="divider12" *ngIf="!innerLast"></div>
                        </div>
                    </ng-container>
                </ng-container>
                <div class="divider8" *ngIf="!last"></div>
            </ng-container>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/item-group.scss"],
})
export class OneTimeGroupComponent {
    @Input()
    set cartItems(data: ListItemGroupedByDate[]) {
        this._cartItems = data;
        this._cartItemsGroupedBySlot = this.groupItemsByDeliveryType(data);
    }

    @Input() slotInfo: CartSlotInfo;

    TEXTS = TEXTS;
    _cartItemsGroupedBySlot: GroupedBySlot[] = [];
    _cartItems: ListItemGroupedByDate[] = [];

    /*
        TODO [MANISH]: Move this to either selector or reducer.
        No point in computing it again and again.
        Also it gets triggered in between digest cycle which is bad
    */
    private groupItemsByDeliveryType(
        cartItems: ListItemGroupedByDate[]
    ): GroupedBySlot[] {
        return cartItems.map((item) => {
            return {
                date: item.date,
                ALTERNATE_DELIVERY: item.items.filter(
                    (i) =>
                        i.delivery_type &&
                        i.delivery_type ===
                            CartItemDeliveryType.ALTERNATE_DELIVERY
                ),
                REGULAR_DELIVERY: item.items.filter(
                    (i) =>
                        !i.delivery_type ||
                        i.delivery_type ===
                            CartItemDeliveryType.REGULAR_DELIVERY
                ),
            };
        });
    }
}
