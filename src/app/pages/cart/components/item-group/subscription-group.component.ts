import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { CART_ITEM_TYPES } from "@constants";

import { ListItemGroupedByDate } from "@pages/cart/types";
import { TEXTS } from "@pages/cart/constants";

@Component({
    selector: "supr-cart-item-subscription-group",
    template: `
        <div class="itemGroupWrapper">
            <supr-section-header
                icon="${TEXTS.SUBSCRIPTIONS_HEADER.icon}"
                title="${TEXTS.SUBSCRIPTIONS_HEADER.title}"
                subtitle="${TEXTS.SUBSCRIPTIONS_HEADER.subtitle}"
            >
            </supr-section-header>

            <div class="itemWrapper">
                <ng-container *ngIf="cartItems?.length">
                    <div class="divider16"></div>
                    <ng-container
                        *ngFor="let listItem of cartItems; let last = last"
                    >
                        <ng-container
                            *ngFor="
                                let item of listItem.items;
                                let innerLast = last
                            "
                        >
                            <supr-cart-item-wrapper-container
                                [cartItem]="item"
                            ></supr-cart-item-wrapper-container>
                            <div class="divider12" *ngIf="!innerLast"></div>
                        </ng-container>
                        <div class="divider16" *ngIf="!last"></div>
                    </ng-container>
                    <div class="divider16"></div>
                </ng-container>
            </div>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["../../styles/item-group.scss"],
})
export class SubscriptionGroupComponent {
    @Input() cartItems: ListItemGroupedByDate[];

    TEXTS = TEXTS;
    CART_ITEM_TYPES = CART_ITEM_TYPES;
}
