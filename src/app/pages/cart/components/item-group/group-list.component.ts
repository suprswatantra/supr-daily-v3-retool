import {
    Component,
    OnInit,
    Input,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CartItem, SlotInfo } from "@models";

import { CartItemsGroupByDate } from "@pages/cart/types";
import { CartPageService } from "@pages/cart/services/cart.service";

@Component({
    selector: "supr-cart-item-group-list",
    template: `
        <ng-container *ngIf="cartItemsGroup?.oneTimeList?.length">
            <supr-cart-item-onetime-group
                [cartItems]="cartItemsGroup?.oneTimeList"
                [slotInfo]="slotInfo"
            ></supr-cart-item-onetime-group>
            <div class="divider8"></div>
        </ng-container>

        <supr-cart-item-subscription-group
            *ngIf="cartItemsGroup?.subscriptionList?.length"
            [cartItems]="cartItemsGroup?.subscriptionList"
        ></supr-cart-item-subscription-group>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemGroupListComponent implements OnInit, OnChanges {
    @Input() cartItems: CartItem[] = [];
    @Input() slotInfo: SlotInfo;

    cartItemsGroup: CartItemsGroupByDate;

    constructor(private cartService: CartPageService) {}

    ngOnInit() {
        this.splitItemsIntoGroups();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["cartItems"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.splitItemsIntoGroups();
        }
    }

    private splitItemsIntoGroups() {
        this.cartItemsGroup = this.cartService.splitCartItemsIntoGroupsByModesAndDates(
            this.cartItems
        );
    }
}
