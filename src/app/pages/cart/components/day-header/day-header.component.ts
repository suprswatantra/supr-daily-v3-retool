import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { Schedule, ScheduleItem } from "@models";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-cart-day-header",
    template: `
        <supr-schedule-day-header [date]="date" [subtitle]="subtitle$ | async">
        </supr-schedule-day-header>
    `,
    styleUrls: ["./day-header-component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DayHeaderComponent implements OnInit {
    @Input() date: string;

    subtitle$: Observable<string>;

    constructor(private scheduleAdapter: ScheduleAdapter) {}

    ngOnInit() {
        this.subtitle$ = this.scheduleAdapter.getScheduleByDate(this.date).pipe(
            map((schedule: Schedule) => {
                if (!schedule) {
                    return TEXTS.SCHEDULE_HEADER_NO_SCHEDULES;
                }

                const addons =
                    schedule.addons &&
                    schedule.addons.filter(
                        (addon: ScheduleItem) => addon.quantity > 0
                    );
                const subscriptions =
                    schedule.subscriptions &&
                    schedule.subscriptions.filter(
                        (subscription: ScheduleItem) =>
                            subscription.quantity > 0
                    );

                const scheduledItems =
                    ((addons && addons.length) || 0) +
                    ((subscriptions && subscriptions.length) || 0);

                if (!scheduledItems) {
                    return TEXTS.SCHEDULE_HEADER_NO_SCHEDULES;
                }

                return `${scheduledItems} item${
                    scheduledItems > 1 ? "s" : ""
                } ${TEXTS.SCHEDULE_HEADER_SUBTITLE}`;
            })
        );
    }
}
