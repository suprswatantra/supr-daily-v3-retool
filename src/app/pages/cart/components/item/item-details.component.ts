import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { CART_ITEM_TYPES, CART_ITEM_ERROR_CODES } from "@constants";
import { Sku, CartItem } from "@models";
import { Segment } from "@types";

import { CartPageService } from "@pages/cart/services/cart.service";
import { SkuService } from "@services/shared/sku.service";
import { SettingsService } from "@services/shared/settings.service";

import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-cart-item-details",
    template: `
        <div class="itemDetails" *ngIf="sku">
            <supr-text class="itemName" type="body">
                {{ sku?.sku_name }}
            </supr-text>
            <div class="divider4"></div>

            <div class="suprRow">
                <supr-cart-item-qty
                    [sku]="sku"
                    [quantity]="cartItem?.quantity"
                    [rechargeQuantity]="cartItem?.recharge_quantity"
                ></supr-cart-item-qty>
                <ng-container *ngIf="cartItem?.frequency && !isSuprPassSku">
                    <div class="spacer12"></div>
                    <supr-week-days-small
                        [selectedDays]="cartItem?.frequency"
                    ></supr-week-days-small>
                </ng-container>
            </div>

            <div class="divider8"></div>

            <div class="suprRow spaceBetween">
                <supr-cart-item-price
                    [sku]="sku"
                    [cartItem]="cartItem"
                    [switchToAccessPrice]="switchToAccessPrice"
                ></supr-cart-item-price>
                <supr-oos-overlay
                    [outOfStock]="
                        sku?.out_of_stock && !sku?.next_available_date
                    "
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION.OUT_OF_STOCK}"
                    [saObjectValue]="sku?.id"
                    [saContextList]="saContextList"
                    [class.itemDetailsIncreaseCounterZIndex]="
                        isMaxQuantityError
                    "
                >
                    <ng-container *ngIf="isAddon; else rechargeQuantity">
                        <supr-cart-item-counter
                            [quantity]="cartItem?.quantity"
                            [plusDisabled]="cartItem?.is_reward_sku"
                            [minusDisabled]="cartItem?.is_reward_sku"
                            [saObjectValueCounter]="saObjectValueCounter"
                            (handleQtyChange)="handleQtyChange.emit($event)"
                        ></supr-cart-item-counter>
                    </ng-container>

                    <ng-template #rechargeQuantity>
                        <supr-cart-item-qty
                            *ngIf="!isSuprPassSku"
                            [sku]="sku"
                            [quantity]="cartItem?.recharge_quantity"
                            textType="body"
                        ></supr-cart-item-qty>
                    </ng-template>
                </supr-oos-overlay>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemDetailsComponent {
    @Input() sku: Sku;
    @Input() switchToAccessPrice: boolean;
    @Input()
    set cartItem(item: CartItem) {
        this._cartItem = item;
        this.isSuprPassSku = this.skuService.isSuprPassSku(this.sku);
        this.isAddon = item.type === CART_ITEM_TYPES.ADDON;
        this.quantity = this.isAddon ? item.quantity : item.recharge_quantity;
        this.setOOSAnalyticsData();

        if (item) {
            this.checkMaxQuantityError(item);
        }
    }
    @Input() saObjectValueCounter: number;

    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();

    get cartItem(): CartItem {
        return this._cartItem;
    }

    isAddon = true;
    quantity: number;
    isMaxQuantityError = false;
    saContextList: Segment.ContextListItem[] = [];
    isSuprPassSku: boolean;

    private _cartItem: CartItem;

    constructor(
        private cartPageService: CartPageService,
        private skuService: SkuService,
        private settingsService: SettingsService
    ) {}

    private setOOSAnalyticsData() {
        if (this.sku && this._cartItem) {
            this.saContextList = this.cartPageService.getOOSAnalytics(
                this.sku,
                this._cartItem
            );
        }
    }

    private checkMaxQuantityError(item: CartItem) {
        const { validation } = item;

        const {
            showPlusMinusErrorCode,
        } = this.settingsService.getSettingsValue("cartErrorCodes");

        const _showPlusMinusErrorCode =
            showPlusMinusErrorCode &&
            showPlusMinusErrorCode.indexOf(CART_ITEM_ERROR_CODES.BULK_ORDER) >=
                0;
        if (validation && !validation.is_valid && _showPlusMinusErrorCode) {
            this.isMaxQuantityError = true;
        } else {
            this.isMaxQuantityError = false;
        }
    }
}
