import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "supr-cart-item-counter",
    template: `
        <supr-number-counter
            [class.remove]="_quantity < 1"
            [class.disabled]="plusDisabled && minusDisabled"
            [quantity]="_quantity"
            [thresholdQty]="-1"
            [plusDisabled]="plusDisabled"
            [minusDisabled]="minusDisabled"
            [saObjectValue]="saObjectValueCounter"
            (handleClick)="updateQty($event)"
        >
        </supr-number-counter>
    `,
    styleUrls: ["../../styles/item.component.scss"],
})
export class ItemCounterComponent {
    @Input()
    set quantity(qty: number) {
        this._quantity = qty;
    }
    @Input() saObjectValueCounter: number;
    @Input() plusDisabled = false;
    @Input() minusDisabled = false;

    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();

    _quantity: number;

    updateQty(direction: number) {
        if (direction > 0) {
            this._quantity++;
        } else if (direction < 0 && this._quantity > 0) {
            this._quantity--;
        }

        this.handleQtyChange.emit(this._quantity);
    }
}
