import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { SkuFooter } from "@shared/models";

import { TEXTS } from "@pages/cart/constants";

@Component({
    selector: "supr-cart-item-footer",
    template: `
        <ng-container *ngIf="skuFooter?.text || showSubscribe">
            <div class="divider24"></div>
            <div class="suprRow itemSubscribe">
                <ng-container *ngIf="!skuFooter?.text && showSubscribe">
                    <supr-icon name="repeat"></supr-icon>
                    <div class="spacer4"></div>
                </ng-container>

                <ng-container *ngIf="skuFooter?.text; else defaultText">
                    <div
                        class="suprRow footerText"
                        (click)="handleFooterClick.emit(skuFooter)"
                    >
                        <supr-text type="caption" [truncate]="true">
                            {{ skuFooter?.text }}
                        </supr-text>

                        <ng-container *ngIf="skuFooter?.url">
                            <div class="spacer4"></div>
                            <supr-icon name="locked" class="locked"></supr-icon>
                            <supr-icon
                                name="chevron_right"
                                class="chev"
                            ></supr-icon>
                        </ng-container>
                    </div>
                </ng-container>

                <ng-template #defaultText>
                    <ng-container *ngIf="showSubscribe">
                        <supr-text type="caption">
                            {{ TEXTS.SUBSCRIBE_DESC }}
                        </supr-text>
                    </ng-container>
                </ng-template>

                <ng-container *ngIf="showSubscribe">
                    <div
                        class="suprRow right"
                        (click)="handleSubscribeClick.emit()"
                        saClick
                        [saObjectName]="subscribeSaObjectName"
                        [saObjectValue]="subscribeSaObjectValue"
                    >
                        <supr-text type="paragraph" class="subscribe">
                            {{ TEXTS.SUBSCRIBE }}
                        </supr-text>
                        <supr-icon
                            name="chevron_right"
                            class="chevron"
                        ></supr-icon>
                    </div>
                </ng-container>
            </div>
        </ng-container>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartItemFooterComponent {
    @Input() skuFooter: SkuFooter;
    @Input() showSubscribe: boolean;
    @Input() subscribeSaObjectName: string;
    @Input() subscribeSaObjectValue: string;

    @Output() handleFooterClick: EventEmitter<SkuFooter> = new EventEmitter();
    @Output() handleSubscribeClick: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
}
