import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { OOS_TEXT } from "@constants";

import { CartItem, Vacation, Sku } from "@models";

import { ModalService } from "@services/layout/modal.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { SkuService } from "@services/shared/sku.service";
import { BlockAccessService } from "@services/shared/block-access.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";

import { Calendar } from "@types";

import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-item-reschedule-modal",
    template: `
        <div class="itemReschedule">
            <div class="suprRow">
                <supr-icon
                    name="chevron_left"
                    (click)="closeModal()"
                ></supr-icon>
                <supr-text type="subtitle">
                    {{ TEXTS.RESCHEDULE_DELIVERY }}
                </supr-text>
            </div>
            <div class="divider24"></div>
            <supr-calendar
                [selectedDate]="_deliveryDate"
                [disablePastDays]="true"
                [vacation]="vacation"
                [customDisabledDates]="oosDates"
                [customDisabledDateText]="oosDateCalendarText"
                dateClickSaName="${SA_OBJECT_NAMES.CLICK
                    .CART_CALENDAR_DATE_CLICK}"
                (handleSelectedDate)="selectNewDate($event)"
            ></supr-calendar>
            <div class="divider24"></div>

            <!-- Rendered only when supr access cutoff time experiment is active -->
            <ng-container *ngIf="suprPassInfoText">
                <div class="suprRow suprPassInfo top">
                    <supr-icon name="error"></supr-icon>
                    <div class="spacer4"></div>
                    <supr-text type="caption">
                        {{ suprPassInfoText }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <div class="subscribeText">
                        <div
                            class="suprRow"
                            (click)="gotoSuprPassPage()"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .SUBSCRIBE_SUPR_PASS_CLICK}"
                        >
                            <supr-text type="paragraph">
                                ${TEXTS.SUBSCRIBE_SUPR_PASS}
                            </supr-text>
                            <supr-icon name="chevron_right"></supr-icon>
                        </div>
                    </div>
                </div>
                <div class="divider12"></div>
            </ng-container>

            <supr-button
                saObjectName="${SA_OBJECT_NAMES.CLICK.CART_DATE_CHANGE_CONFIRM}"
                (handleClick)="updateDeliveryDate()"
            >
                <supr-text type="body">
                    {{ TEXTS.CHANGE_DATE }}
                    ({{ this._deliveryDate.dateText | date: "d LLL, yyy" }})
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemRescheduleModalComponent implements OnInit {
    @Input() vacation: Vacation;
    @Input()
    set cartItem(value: CartItem) {
        this.setDeliveryDate(value);
    }
    @Input()
    set sku(value: Sku) {
        this.setOOSDates(value);
    }

    @Output() handleSelectDate: EventEmitter<string> = new EventEmitter();

    _deliveryDate: Calendar.DayData;
    TEXTS = TEXTS;
    oosDates = [];
    oosDateCalendarText = "";
    suprPassInfoText: string;

    constructor(
        private modalService: ModalService,
        private suprDateService: SuprDateService,
        private skuService: SkuService,
        private blockAccessService: BlockAccessService,
        private dateService: DateService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.setSuprPassInfoText();
    }

    selectNewDate(date: Calendar.DayData) {
        this._deliveryDate = date;
    }

    updateDeliveryDate() {
        this.handleSelectDate.emit(this._deliveryDate.dateText);
    }

    closeModal() {
        this.modalService.closeModal();
    }

    gotoSuprPassPage() {
        this.routerService.goToSuprPassPage();
    }

    /**
     * Only sets text in case supr pass cutoff time experiment is active for the current user
     *
     * @private
     * @returns
     * @memberof ItemRescheduleModalComponent
     */
    private setSuprPassInfoText() {
        if (!this.dateService.hasTodaysCutOffTimePassed()) {
            this.suprPassInfoText = "";
            return;
        }

        this.suprPassInfoText = this.blockAccessService.getCalendarInfoText();
    }

    private setDeliveryDate(cartItem: CartItem) {
        if (cartItem) {
            this._deliveryDate = this.suprDateService.suprDateFromText(
                cartItem.delivery_date
            );
        }
    }

    private setOOSDates(sku: Sku) {
        const oosDates = this.skuService.getOOSDates(sku);
        if (oosDates) {
            this.oosDates = oosDates;
            this.oosDateCalendarText = OOS_TEXT;
        }
    }
}
