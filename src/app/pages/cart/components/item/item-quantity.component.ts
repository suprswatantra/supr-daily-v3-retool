import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { Sku } from "@models";
import { DURATION } from "@pages/cart/constants";

import { QuantityService } from "@services/util/quantity.service";
import { SkuService } from "@services/shared/sku.service";

@Component({
    selector: "supr-cart-item-qty",
    template: `
        <supr-text class="itemQty" [type]="textType">
            {{ displayQty }}
        </supr-text>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemQuantityComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() quantity: number;
    @Input() rechargeQuantity: number;
    @Input() textType = "caption";

    displayQty: string;

    constructor(
        private qtyService: QuantityService,
        private skuService: SkuService
    ) {}

    ngOnInit() {
        this.setDisplayQty();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["quantity"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.setDisplayQty();
        }
    }

    private setDisplayQty() {
        if (!this.sku) {
            return;
        }

        const isSuprPassSku = this.skuService.isSuprPassSku(this.sku);

        if (isSuprPassSku && this.rechargeQuantity) {
            const months = this.rechargeQuantity / DURATION.DAYS_IN_MONTH;
            const numOfMonths = Math.floor(months);
            this.displayQty =
                numOfMonths +
                " " +
                DURATION.MONTH_TEXT +
                (numOfMonths > 1 ? DURATION.PLURAL_CHARACTER : "");
            return;
        }

        this.displayQty = this.qtyService.toText(this.sku, this.quantity);
    }
}
