import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-cart-item-coupon",
    template: `
        <div class="suprRow itemCoupon" *ngIf="couponCode?.length">
            <supr-icon name="approve_filled"></supr-icon>
            <div class="spacer4"></div>
            <supr-text type="paragraph">
                "{{ couponCode | uppercase }}" Applied
            </supr-text>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemCouponComponent {
    @Input() couponCode: string;
}
