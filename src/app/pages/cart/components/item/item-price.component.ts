import {
    Component,
    Input,
    OnInit,
    OnChanges,
    ChangeDetectionStrategy,
    SimpleChanges,
} from "@angular/core";

import { CART_ITEM_TYPES } from "@constants";
import { CartItem, Sku, SkuRewardTagType } from "@models";

import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";

@Component({
    selector: "supr-cart-item-price",
    template: `
        <div class="suprColumn left">
            <ng-container
                *ngIf="cartItem?.is_reward_sku; else withoutRewardSku"
            >
                <supr-reward-sku-tag
                    [skuRewardType]="skuRewardType.BASKET_OFFER_REWARD"
                ></supr-reward-sku-tag>
                <div class="divider8"></div>
                <div class="suprRow itemPrice">
                    <supr-text type="body">
                        {{ discountedPrice | rupee: 2 }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <supr-text class="itemPriceStrike" type="caption">
                        {{ itemUnitPrice | rupee: 2 }}
                    </supr-text>
                </div>
            </ng-container>

            <ng-template #withoutRewardSku>
                <div class="suprRow itemPrice">
                    <supr-text type="body">
                        {{ itemUnitPrice | rupee: 2 }}
                    </supr-text>
                    <div class="spacer8"></div>
                    <ng-container *ngIf="showPriceStrike">
                        <div class="spacer8"></div>
                        <supr-text class="itemPriceStrike" type="caption">
                            {{ itemUnitMrp | rupee: 2 }}
                        </supr-text>
                    </ng-container>
                </div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemPriceComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() cartItem: CartItem;
    @Input() switchToAccessPrice: boolean;

    displayQty: string;
    showPriceStrike = false;
    itemUnitPrice: number;
    itemUnitMrp: number;
    discountedPrice: number;

    skuRewardType = SkuRewardTagType;

    constructor(
        private utilService: UtilService,
        private skuService: SkuService
    ) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;

        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["cartItem"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        const isSuprPassSku = this.skuService.isSuprPassSku(this.sku);
        const { quantity, recharge_quantity, type } = this.cartItem;
        const qty =
            type === CART_ITEM_TYPES.ADDON ? quantity : recharge_quantity;

        if (!isSuprPassSku) {
            this.setSkuPrice(qty);
            return;
        }

        this.setSuprPassSkuPrice();
    }

    private setDiscountedPrice() {
        if (this.cartItem.is_reward_sku) {
            this.discountedPrice = this.cartItem.payment_data.post_discount;
        }
    }

    private setSkuPrice(qty: number) {
        let unitPrice: number;

        if (this.switchToAccessPrice) {
            unitPrice = this.utilService.getNestedValue(
                this.sku,
                "access_price",
                this.sku.unit_price
            );
        } else {
            unitPrice = this.sku.unit_price;
        }

        this.itemUnitPrice = unitPrice * qty;
        this.itemUnitMrp = this.sku.unit_mrp * qty;
        this.setDiscountedPrice();
    }

    private setSuprPassSkuPrice() {
        const plans = this.utilService.getNestedValue(
            this.sku,
            "virtual_sku_info.data.plans",
            null
        );

        if (!plans) {
            return;
        }

        const planObj = plans.find((plan) => plan.id === this.cartItem.plan_id);

        if (planObj && planObj.unitMrp) {
            this.itemUnitMrp = planObj.unitMrp;
        }

        if (planObj && planObj.unitPrice) {
            this.itemUnitPrice = planObj.unitPrice;
        }

        if (this.itemUnitMrp && this.itemUnitPrice) {
            this.showPriceStrike = this.itemUnitPrice < this.itemUnitMrp;
        }
    }
}
