import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { CartItem, Sku } from "@models";
import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-item-remove-modal",
    template: `
        <div class="itemRemove">
            <supr-text type="subtitle">
                {{ TEXTS.REMOVE_SUBSCRIPTION_TITLE }}
            </supr-text>
            <div class="divider24"></div>

            <div class="suprRow top relative">
                <div class="suprColumn itemImage">
                    <supr-image
                        [src]="sku?.image?.fullUrl"
                        [image]="sku?.image"
                    ></supr-image>
                </div>

                <div class="suprColumn itemContent left">
                    <supr-cart-item-details
                        [sku]="sku"
                        [cartItem]="cartItem"
                        [switchToAccessPrice]="switchToAccessPrice"
                    ></supr-cart-item-details>
                </div>
            </div>
            <div class="divider16"></div>

            <supr-text type="body" class="body">
                {{ TEXTS.CONFIRM_REMOVE_SUBSCRIPTION_TEXT }}
            </supr-text>
            <div class="divider24"></div>

            <supr-button
                saObjectName="${SA_OBJECT_NAMES.CLICK.REMOVE_SUB_FROM_CART_BTN}"
                (handleClick)="handleRemove.emit()"
            >
                <supr-text type="body">
                    {{ TEXTS.REMOVE_FROM_CART }}
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemRemoveModalComponent {
    @Input() cartItem: CartItem;
    @Input() sku: Sku;
    @Input() switchToAccessPrice: boolean;

    @Output()
    handleEditSubscription: EventEmitter<void> = new EventEmitter();
    @Output() handleRemove: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
}
