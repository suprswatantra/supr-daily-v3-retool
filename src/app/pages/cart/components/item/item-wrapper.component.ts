import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CART_ITEM_TYPES, MODAL_NAMES, OUT_OF_STOCK_ENABLED } from "@constants";

import {
    CartItem,
    Sku,
    Vacation,
    SuprPassInfo,
    CartMeta,
    SkuFooter,
} from "@models";

import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { MoengageService } from "@services/integration/moengage.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { CartService } from "@services/shared/cart.service";

import { TEXTS, ITEM_HEADER_ACTION_TYPES } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-item-wrapper",
    template: `
        <div
            class="itemWrapper"
            [class.itemOverlay]="_showOverlay"
            [class.rewardBorder]="cartItem?.is_reward_sku"
        >
            <supr-cart-item-header
                [sku]="sku"
                [cartItem]="cartItem"
                [suprPassInfo]="suprPassInfo"
                (handleActionClick)="handleHeaderActionClick($event)"
                (handleItemIssue)="updateShowOverlay($event)"
            ></supr-cart-item-header>
            <div class="divider12"></div>

            <supr-cart-item-body
                [cartItem]="cartItem"
                [sku]="sku"
                [switchToAccessPrice]="cartMeta?.switch_access_price"
                (handleQtyChange)="updateQty($event)"
                (handleRemove)="removeItem()"
            ></supr-cart-item-body>

            <div class="itemSuprPassFooter" *ngIf="_isSuprPassSku">
                <supr-text type="caption" class="dark">
                    ${TEXTS.SUPR_PASS_TILE.END}
                    {{ cartItem?.end_date | date: "d LLL, yyy" }}
                </supr-text>
            </div>

            <ng-container *ngIf="!cartItem?.is_reward_sku">
                <supr-cart-item-footer
                    subscribeSaObjectName="${SA_OBJECT_NAMES.CLICK
                        .SUBSCRIBE_FROM_CART}"
                    [subscribeSaObjectValue]="sku?.id"
                    [skuFooter]="cartItem?.sku_footer"
                    [showSubscribe]="_isAddon && showSubscribe"
                    (handleSubscribeClick)="goToCreateSubscriptionPage()"
                    (handleFooterClick)="footerClicked($event)"
                ></supr-cart-item-footer>
            </ng-container>

            <ng-container *ngIf="_showRescheduleModal">
                <supr-modal
                    modalName="${MODAL_NAMES.CHANGE_DATE}"
                    (handleClose)="closeRescheduleModal()"
                >
                    <supr-cart-item-reschedule-modal
                        [vacation]="vacation"
                        [cartItem]="cartItem"
                        [sku]="sku"
                        (handleSelectDate)="handleNewDeliveryDate($event)"
                    ></supr-cart-item-reschedule-modal>
                </supr-modal>
            </ng-container>

            <ng-container *ngIf="_showConfirmRemoveModal">
                <supr-modal
                    modalName="${MODAL_NAMES.REMOVE_SUB_FROM_CART}"
                    (handleClose)="closeConfirmRemoveModal()"
                >
                    <supr-cart-item-remove-modal
                        [cartItem]="cartItem"
                        [sku]="sku"
                        [switchToAccessPrice]="cartMeta?.switch_access_price"
                        (handleEditSubscription)="goToCreateSubscriptionPage()"
                        (handleRemove)="removeItem(true)"
                    ></supr-cart-item-remove-modal>
                </supr-modal>
            </ng-container>
        </div>
        <supr-cart-auto-add-supr-pass-prompt
            [showModal]="_showAutoAddSuprPassModal"
            [suprPassPrompt]="cartMeta?.auto_add_supr_pass_prompt"
            (handleHideModal)="closeAutoAddSuprPassModal()"
            (handleRemoveSuprAccess)="removeItem(true)"
        ></supr-cart-auto-add-supr-pass-prompt>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemWrapperComponent implements OnInit {
    @Input() cartItem: CartItem;
    @Input() cartMeta: CartMeta;
    @Input() vacation: Vacation;
    @Input() suprPassInfo: SuprPassInfo;
    @Input() set sku(data: Sku) {
        this._sku = data;
        this.setSuprPassFlag();
    }
    get sku(): Sku {
        return this._sku;
    }

    @Output() updateItemInCart: EventEmitter<CartItem> = new EventEmitter();

    _isAddon = true;
    _showOverlay = false;
    _isSuprPassSku = false;
    _showRescheduleModal = false;
    _showConfirmRemoveModal = false;
    _showAutoAddSuprPassModal = false;

    private _sku: Sku;
    private _updateCart: (item: CartItem) => void;

    constructor(
        private skuService: SkuService,
        private utilService: UtilService,
        private modalService: ModalService,
        private routerService: RouterService,
        private moengageService: MoengageService,
        private appsFlyerService: AppsFlyerService,
        private cartService: CartService
    ) {}

    ngOnInit() {
        this._setType();
        this._setDebouncedUpdateCart();
        this._setAutoAddSuprPassModal();
    }

    openRescheduleModal() {
        this._showRescheduleModal = true;
    }

    closeRescheduleModal() {
        this._showRescheduleModal = false;
    }

    openConfirmRemoveModal() {
        this._showConfirmRemoveModal = true;
    }

    closeConfirmRemoveModal() {
        this._showConfirmRemoveModal = false;
    }

    openAutoAddSuprPassModal() {
        this._showAutoAddSuprPassModal = true;
    }

    closeAutoAddSuprPassModal() {
        this._showAutoAddSuprPassModal = false;
    }

    updateCart(item: CartItem) {
        this._updateCart(item);
    }

    updateShowOverlay(showOverlay = false) {
        this._showOverlay = showOverlay;
    }

    handleHeaderActionClick(actionType: ITEM_HEADER_ACTION_TYPES) {
        switch (actionType) {
            case ITEM_HEADER_ACTION_TYPES.CHANGE_DATE:
                this.openRescheduleModal();
                break;
            case ITEM_HEADER_ACTION_TYPES.EDIT_DELIVERIES:
                this.goToSubscriptionRechargePage();
                break;
            case ITEM_HEADER_ACTION_TYPES.EDIT_SETTINGS:
                this.goToCreateSubscriptionPage();
                break;
            case ITEM_HEADER_ACTION_TYPES.REMOVE_FROM_CART:
                this.removeItem(true);
                break;
        }
    }

    goToSubscriptionRechargePage() {
        const subscriptionId = this.utilService.getNestedValue(
            this.cartItem,
            "subscription_id",
            null
        );
        if (subscriptionId) {
            this.routerService.goToSubscriptionRechargePage(subscriptionId);
        }
    }

    goToCreateSubscriptionPage() {
        this.routerService.goToSubscriptionCreatePage({
            queryParams: {
                skuId: this.sku.id,
                from: "cart",
            },
        });
    }

    handleNewDeliveryDate(deliveryDate: string) {
        this.modalService.closeModal();

        const newCartItem = { ...this.cartItem, delivery_date: deliveryDate };
        this.updateItemInCart.emit(newCartItem);
    }

    updateQty(quantity: number) {
        const newCartItem = { ...this.cartItem, quantity };

        if (quantity === 0) {
            this.sendAnalyticsEvents();
        }

        this.updateCart(newCartItem);
    }

    get showSubscribe() {
        const skuOutOfStock = this.utilService.getNestedValue(
            this.sku,
            "out_of_stock"
        );
        const dateAvailable = this.utilService.getNestedValue(
            this.sku,
            "next_available_date"
        );
        return (
            OUT_OF_STOCK_ENABLED &&
            (!skuOutOfStock || (skuOutOfStock && dateAvailable))
        );
    }

    removeItem(force = false) {
        const outOfStock = this.utilService.getNestedValue(
            this.sku,
            "out_of_stock",
            false
        );
        const dateAvailable = this.utilService.getNestedValue(
            this.sku,
            "next_available_date"
        );

        if (force || this._isAddon || (outOfStock && !dateAvailable)) {
            this.modalService.closeModal();
            this.updateQty(0);
            if (this.skuService.isSuprPassSku(this.sku)) {
                this.cartService.setSuprPassRemovedInfo();
            }
        } else {
            this.openConfirmRemoveModal();
        }
    }

    footerClicked(skuFooter: SkuFooter) {
        const url = this.utilService.getNestedValue(skuFooter, "url", null);

        if (!url) {
            return;
        }

        this.routerService.goToUrl(url);
    }

    private setSuprPassFlag() {
        if (this.sku) {
            this._isSuprPassSku = this.skuService.isSuprPassSku(this.sku);
        }
    }

    private _setType() {
        this._isAddon = this.cartItem.type === CART_ITEM_TYPES.ADDON;
    }

    private _setDebouncedUpdateCart() {
        this._updateCart = this.utilService.debounce(300, (item: CartItem) =>
            this.updateItemInCart.emit(item)
        );
    }

    private _setAutoAddSuprPassModal() {
        const isSuprAccess = this.utilService.getNestedValue(
            this.cartItem,
            "is_supr_access",
            false
        );

        const isAutoAdded = this.utilService.getNestedValue(
            this.cartItem,
            "is_auto_added",
            false
        );

        this._showAutoAddSuprPassModal = isAutoAdded && isSuprAccess;
    }

    private sendAnalyticsEvents() {
        if (!this.sku) {
            return;
        }
        this.moengageService.trackRemoveFromCart({
            sku_id: this.sku.id,
            sku_price: this.sku.unit_price,
            product_identifier: this.sku.product_identifier,
        });

        this.appsFlyerService.trackRemoveFromCart(this.sku);
    }
}
