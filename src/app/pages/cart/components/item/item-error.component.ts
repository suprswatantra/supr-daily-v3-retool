import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-cart-item-error",
    template: `
        <div
            saImpression
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
            [saContext]="saContext"
            class="suprRow top itemError"
        >
            <supr-icon name="error"></supr-icon>
            <div class="spacer4"></div>
            <supr-text type="caption">{{ errMsg }}</supr-text>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemErrorComponent {
    @Input() errMsg: string;
    @Input() saObjectName: string;
    @Input() saObjectValue = "";
    @Input() saContext = "";
}
