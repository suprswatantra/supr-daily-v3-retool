import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import {
    CART_ITEM_TYPES,
    OUT_OF_STOCK_ENABLED,
    CLODUINARY_IMAGE_SIZE,
} from "@constants";
import { CartItem, Sku } from "@models";
import { Segment } from "@types";

import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";
import { CartPageService } from "@pages/cart/services/cart.service";

@Component({
    selector: "supr-cart-item-body",
    template: `
        <div class="itemBody">
            <div class="suprRow top">
                <div class="suprColumn itemImage">
                    <supr-image
                        [src]="sku?.image?.fullUrl"
                        [image]="sku?.image"
                        [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
                        [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
                    ></supr-image>
                </div>

                <div class="suprColumn itemContent left">
                    <supr-cart-item-coupon
                        [couponCode]="cartItem?.coupon_code"
                    ></supr-cart-item-coupon>
                    <supr-cart-item-details
                        [sku]="sku"
                        [cartItem]="cartItem"
                        [switchToAccessPrice]="switchToAccessPrice"
                        [saObjectValueCounter]="sku?.id"
                        (handleQtyChange)="handleQtyChange.emit($event)"
                    ></supr-cart-item-details>
                </div>
            </div>
            <supr-icon
                [class.outOfStockDelete]="
                    ${OUT_OF_STOCK_ENABLED} &&
                    sku?.out_of_stock &&
                    !sku?.next_available_date
                "
                saClick
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                [saContextList]="saContextList"
                name="delete"
                (click)="handleRemove.emit($event)"
            ></supr-icon>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemBodyComponent implements OnInit {
    @Input() sku: Sku;
    @Input() cartItem: CartItem;
    @Input() switchToAccessPrice: boolean;

    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();
    @Output() handleRemove: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
    saObjectName: string;
    saObjectValue: number;
    saContextList: Segment.ContextListItem[] = [];

    constructor(private cartPageService: CartPageService) {}

    ngOnInit() {
        this.setAnalyticsData();
    }

    private setAnalyticsData() {
        if (this.cartItem && this.sku) {
            this.saObjectValue = this.sku.id;

            if (this.sku.out_of_stock && !this.sku.next_available_date) {
                this.setOOSAnalyticsData();
            } else {
                this.saObjectName =
                    this.cartItem.type === CART_ITEM_TYPES.ADDON
                        ? SA_OBJECT_NAMES.CLICK.REMOVE_ADDON_FROM_CART_ICON
                        : SA_OBJECT_NAMES.CLICK.REMOVE_SUB_FROM_CART_ICON;
            }
        }
    }

    private setOOSAnalyticsData() {
        this.saObjectName = SA_OBJECT_NAMES.IMPRESSION.OUT_OF_STOCK;

        this.saContextList = this.cartPageService.getOOSAnalytics(
            this.sku,
            this.cartItem
        );
    }
}
