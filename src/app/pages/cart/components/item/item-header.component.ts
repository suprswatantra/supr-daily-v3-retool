import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    CART_ITEM_TYPES,
    OUT_OF_STOCK_ENABLED,
    CART_ITEM_ERROR_CODES,
} from "@constants";

import {
    CartItem,
    Sku,
    SkuRewardTagType,
    SuprPassInfo,
    CartItemValidation,
} from "@models";

import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { SettingsService } from "@services/shared/settings.service";
import { RouterService } from "@services/util/router.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";
import { TEXTS, ITEM_HEADER_ACTION_TYPES } from "@pages/cart/constants";

@Component({
    selector: "supr-cart-item-header",
    template: `
        <div
            class="suprRow itemHeader"
            [class.rewardSku]="
                _cartItem?.is_reward_sku && _cartItem?.header_text
            "
        >
            <ng-container
                *ngIf="
                    ${OUT_OF_STOCK_ENABLED} &&
                        sku?.out_of_stock &&
                        !sku?.next_available_date;
                    then outOfStock;
                    else valid
                "
            >
            </ng-container>
            <ng-template #outOfStock>
                <supr-cart-item-error
                    errMsg="${TEXTS.CART_ITEM_OOS_MESSAGE}"
                    [saObjectName]="saObjectNameError"
                ></supr-cart-item-error>
            </ng-template>
            <ng-template #valid>
                <ng-container
                    *ngIf="_cartItem?.validation?.is_valid; else error"
                >
                    <div class="suprRow textWrapper">
                        <ng-container
                            *ngIf="!isAddon && !isSubscriptionRecharge"
                        >
                            <supr-icon
                                name="calendar"
                                class="calendar"
                            ></supr-icon>
                            <div class="spacer12"></div>
                        </ng-container>

                        <ng-container *ngIf="_cartItem?.is_reward_sku">
                            <supr-reward-sku-tag
                                [noTag]="true"
                                [showInfoIcon]="true"
                                [skuRewardType]="
                                    skuRewardType.BASKET_OFFER_REWARD
                                "
                                [useCommonStyles]="false"
                            ></supr-reward-sku-tag>
                            <div class="spacer4"></div>
                        </ng-container>

                        <supr-text
                            class="dark"
                            [class.rewardsSkuHeadertext]="
                                _cartItem.is_reward_sku
                            "
                            [type]="
                                _cartItem.is_reward_sku
                                    ? 'paragraph'
                                    : 'caption'
                            "
                        >
                            {{ _cartItem.header_text }}
                        </supr-text>

                        <ng-container
                            *ngIf="!isAddon && !isSubscriptionRecharge"
                        >
                            <div class="spacer4"></div>
                            <supr-text class="dark" type="caption">
                                {{ _cartItem?.start_date | date: "d LLL" }}
                            </supr-text>
                        </ng-container>
                    </div>
                </ng-container>
            </ng-template>

            <ng-template #error>
                <supr-cart-item-error
                    [errMsg]="errMsg"
                    [saObjectName]="saObjectNameError"
                    [saObjectValue]="sku?.id"
                    [saContext]="saContextError"
                ></supr-cart-item-error>
            </ng-template>

            <ng-container *ngIf="!isSuprPassSku; else suprPassAction">
                <supr-cart-item-action
                    *ngIf="!_cartItem.is_reward_sku && showActionItem()"
                    [text]="actionText"
                    [saObjectName]="saObjectNameAction"
                    [saObjectValue]="saObjectValueAction"
                    (click)="onActionClick()"
                >
                </supr-cart-item-action>
            </ng-container>

            <ng-template #suprPassAction>
                <supr-cart-item-action
                    *ngIf="showActionItem() && suprPassInfo"
                    [text]="TEXTS.RESELECT_PLAN"
                    saObjectName="${SA_OBJECT_NAMES.CLICK
                        .RESELECT_PLANS_SUPR_PASS}"
                    [saObjectValue]="saObjectValueAction"
                    (click)="onSuprPassActionClick()"
                >
                </supr-cart-item-action>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemHeaderComponent {
    @Input() sku: Sku;
    @Input() suprPassInfo: SuprPassInfo;
    @Input()
    set cartItem(item: CartItem) {
        this._cartItem = item;
        this.isSuprPassSku = this.skuService.isSuprPassSku(this.sku);
        this.setData();
    }

    @Output()
    handleActionClick: EventEmitter<
        ITEM_HEADER_ACTION_TYPES
    > = new EventEmitter();
    @Output() handleItemIssue: EventEmitter<boolean> = new EventEmitter();

    TEXTS = TEXTS;

    _cartItem: CartItem;
    itemUnavailable = false;
    isAddon = true;
    isSubscriptionRecharge = false;
    actionText: string;
    errMsg: string;
    isSuprPassSku: boolean;
    skuRewardType = SkuRewardTagType;

    saObjectNameError: string;
    saContextError: string;
    saObjectNameAction: string;
    saObjectValueAction: number;

    _settingCartErrorCodes: any;

    constructor(
        private utilService: UtilService,
        private skuService: SkuService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private blockAccessService: BlockAccessService
    ) {}

    checkItemIssue(errorCode = 0) {
        this.setItemUnavailable(errorCode);
        this.setActionText();

        // Update parent
        const issuePresent = errorCode !== 0;
        this.handleItemIssue.emit(issuePresent);
    }

    /**
     * Sets the action type, blocks change date in case
     * of access restrictions
     *
     * @returns
     * @memberof ItemHeaderComponent
     */
    onActionClick() {
        let actionType: ITEM_HEADER_ACTION_TYPES;
        if (this.itemUnavailable) {
            actionType = ITEM_HEADER_ACTION_TYPES.REMOVE_FROM_CART;
        } else if (this.isAddon) {
            /* Block date modal in case of access restrictions */
            if (this.blockAccessService.isFutureDeliveryAccessRestricted()) {
                this.blockAccessService.showBlockAcessNudge();
                return;
            }

            actionType = ITEM_HEADER_ACTION_TYPES.CHANGE_DATE;
        } else if (this.isSubscriptionRecharge) {
            actionType = ITEM_HEADER_ACTION_TYPES.EDIT_DELIVERIES;
        } else {
            actionType = ITEM_HEADER_ACTION_TYPES.EDIT_SETTINGS;
        }

        this.handleActionClick.emit(actionType);
    }

    onSuprPassActionClick() {
        if (!this.suprPassInfo) {
            return;
        }

        const isSuprPassActive = this.utilService.getNestedValue(
            this.suprPassInfo,
            "isActive",
            false
        );
        const priorPurchaseId = this.utilService.getNestedValue(
            this.suprPassInfo,
            "expiredSuprPassData.purchaseId"
        );

        if (isSuprPassActive || priorPurchaseId) {
            let suprPassSubscriptionId: number;
            if (isSuprPassActive) {
                suprPassSubscriptionId = this.utilService.getNestedValue(
                    this.suprPassInfo,
                    "activePlanDetails.purchaseId"
                );
            } else {
                suprPassSubscriptionId = priorPurchaseId;
            }

            if (suprPassSubscriptionId) {
                this.goToSubscriptionRechargePage(suprPassSubscriptionId);
            }
            return;
        }

        this.goToSuprPassPage();
    }

    showActionItem() {
        return (
            this.sku &&
            (!this.sku.out_of_stock ||
                (this.sku.out_of_stock && this.sku.next_available_date))
        );
    }

    private goToSubscriptionRechargePage(subId: number) {
        this.delayOpen(() =>
            this.routerService.goToSubscriptionRechargePage(subId)
        );
    }

    private goToSuprPassPage() {
        this.delayOpen(() => this.routerService.goToSuprPassPage());
    }

    private delayOpen(fn: () => void) {
        setTimeout(fn, 100);
    }

    private setData() {
        this.setType();
        this.setActionText();
        this.setError();
        this.setCartErrorCodes();
        this.setAnalyticsData();
    }

    private setType() {
        this.isAddon = this._cartItem.type === CART_ITEM_TYPES.ADDON;
        this.isSubscriptionRecharge =
            this._cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE;
    }

    private setActionText() {
        if (this.itemUnavailable) {
            this.actionText = TEXTS.REMOVE_FROM_CART;
        } else if (this.isAddon) {
            this.actionText = TEXTS.CHANGE_DATE;
        } else if (this.isSubscriptionRecharge) {
            this.actionText = TEXTS.EDIT_DELIVERIES;
        } else {
            this.actionText = TEXTS.EDIT_SETTINGS;
        }
    }

    private setItemUnavailable(errorCode: number) {
        this.itemUnavailable =
            errorCode === CART_ITEM_ERROR_CODES.ITEM_UNAVAILABLE;
    }

    private setError() {
        const { validation } = this._cartItem;
        let errorCode = 0;
        if (validation && !validation.is_valid) {
            this.errMsg = validation.message;
            errorCode = validation.status_code;
        } else {
            this.errMsg = null;
        }

        const issuePresent = errorCode !== 0;
        setTimeout(() => this.handleItemIssue.emit(issuePresent), 0);
    }

    private setAnalyticsData() {
        if (this.actionText === TEXTS.EDIT_SETTINGS && this._cartItem) {
            this.saObjectValueAction = this._cartItem.sku_id;
            this.saObjectNameAction = SA_OBJECT_NAMES.CLICK.EDIT_SETTINGS_CART;
        }

        if (this.actionText === TEXTS.CHANGE_DATE) {
            this.saObjectNameAction = SA_OBJECT_NAMES.CLICK.CHANGE_DATE_BUTTON;
        }

        if (this._cartItem && this._cartItem.validation) {
            const validation: CartItemValidation = this._cartItem.validation;

            this.setSaObjectName(validation.status_code);

            if (this.checkToSendMessageFromErrorCode(validation)) {
                this.setSaContextErrorMessage(validation.message);
            }
        }
    }

    private checkToSendMessageFromErrorCode(
        validation: CartItemValidation
    ): boolean {
        const takeMessageFromBackendErrorCode = this._settingCartErrorCodes
            ? this._settingCartErrorCodes.takeMessageFromBackendErrorCode
            : [];

        if (
            takeMessageFromBackendErrorCode &&
            takeMessageFromBackendErrorCode.indexOf(validation.status_code) >= 0
        ) {
            return true;
        }

        return false;
    }

    private setSaObjectName(status_code: number) {
        switch (status_code) {
            case CART_ITEM_ERROR_CODES.CUT_OFF_TIME_OVER:
                this.setSaObjectNameErrorMessage(
                    SA_OBJECT_NAMES.IMPRESSION.CUT_OFF_TIME_OVER_ERROR
                );
                break;
            case CART_ITEM_ERROR_CODES.INVALID_DATE:
                this.setSaObjectNameErrorMessage(
                    SA_OBJECT_NAMES.IMPRESSION.INVALID_DATE_ERROR
                );
                break;
            case CART_ITEM_ERROR_CODES.ITEM_UNAVAILABLE:
                this.setSaObjectNameErrorMessage(
                    SA_OBJECT_NAMES.IMPRESSION.ITEM_UNAVAILABLE_ERROR
                );
                break;
            case CART_ITEM_ERROR_CODES.BULK_ORDER:
                this.setSaObjectNameErrorMessage(
                    SA_OBJECT_NAMES.IMPRESSION.BULK_ORDER_ERROR
                );
                break;
            case CART_ITEM_ERROR_CODES.SYSTEM_VACATION:
                this.setSaObjectNameErrorMessage(
                    SA_OBJECT_NAMES.IMPRESSION.SYSTEM_VACATION_ERROR
                );
                break;
            default: {
                this.setObjectNameSettings(status_code);
            }
        }
    }

    private setSaObjectNameErrorMessage(message: string) {
        this.saObjectNameError = message;
    }

    private setSaContextErrorMessage(message: string) {
        this.saContextError = message;
    }

    private setObjectNameSettings(status_code: number) {
        if (this._settingCartErrorCodes) {
            const errorCodeObjectName = this.utilService.getNestedValue(
                this._settingCartErrorCodes,
                "errorCodeObjectName",
                null
            );

            if (errorCodeObjectName) {
                this.setSaObjectNameErrorMessage(
                    errorCodeObjectName[status_code]
                );
            }
        }
    }

    private setCartErrorCodes() {
        this._settingCartErrorCodes = this.settingsService.getSettingsValue(
            "cartErrorCodes"
        );
    }
}
