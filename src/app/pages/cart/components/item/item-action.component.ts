import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-cart-item-action",
    template: `
        <div
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
            class="suprRow itemAction"
        >
            <supr-text type="paragraph">{{ text | uppercase }}</supr-text>
            <supr-icon name="chevron_right"></supr-icon>
        </div>
    `,
    styleUrls: ["../../styles/item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemActionComponent {
    @Input() text: string;
    @Input() saObjectName: string;
    @Input() saObjectValue?: number;
}
