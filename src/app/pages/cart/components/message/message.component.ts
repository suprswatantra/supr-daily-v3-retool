import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { CartMessageInfo } from "@models";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-cart-message-content",
    template: `
        <div *ngIf="cartMessageContent" (click)="handleClick()">
            <div class="divider20"></div>
            <div class="cartMessage">
                <supr-text type="action14" class="title">{{
                    cartMessageContent?.title
                }}</supr-text>
                <div class="divider4"></div>

                <ng-container *ngIf="!cartMessageContent?.cta; else cta">
                    <supr-text type="paragraph" class="desc">
                        {{ cartMessageContent?.description }}
                    </supr-text>
                </ng-container>
                <ng-template #cta>
                    <supr-text-fragment
                        [textFragment]="cartMessageContent?.cta?.text"
                    ></supr-text-fragment>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/message.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageContentComponent {
    @Input() cartMessageContent: CartMessageInfo;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    handleClick() {
        if (this.utilService.isEmpty(this.cartMessageContent.cta)) {
            return;
        }

        const link = this.utilService.getNestedValue(
            this.cartMessageContent.cta,
            "link",
            ""
        );

        if (link) {
            this.routerService.goToUrl(link);
        }
    }
}
