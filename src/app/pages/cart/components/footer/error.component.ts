import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-footer-error",
    template: `
        <supr-sticky-wrapper>
            <div
                class="footer"
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                    .CART_VALIDATION_ERROR}"
            >
                <supr-text type="subtitle">{{ TEXTS.SOME_ISSUES }}</supr-text>
                <supr-text type="body" class="body">
                    {{ TEXTS.RESOLVE_ISSUES }}
                </supr-text>
                <div class="divider8"></div>
                <supr-button [disabled]="true">
                    <supr-text type="body">
                        {{ TEXTS.CONTINUE }}
                    </supr-text>
                </supr-button>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["../../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterErrorComponent {
    TEXTS = TEXTS;
}
