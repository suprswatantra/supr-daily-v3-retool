import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { CartMeta } from "@models";

import { CheckoutInfo } from "@shared/models/user.model";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-cart-page-footer",
    template: `
        <ng-container *ngIf="!hasAddress; else errorState">
            <supr-cart-footer-address></supr-cart-footer-address>
        </ng-container>

        <ng-template #errorState>
            <ng-container *ngIf="hasError; else pay">
                <supr-cart-footer-error></supr-cart-footer-error>
            </ng-container>
        </ng-template>

        <ng-template #pay>
            <ng-container *ngIf="showNewPaymentFooter; else oldFooter">
                <supr-cart-footer
                    [amountToAdd]="amountToAdd"
                    [amountToPay]="amountToPay"
                    [suprCreditsUsed]="suprCreditsUsed"
                    [cartMeta]="cartMeta"
                    [isEligibleForSuprPass]="isEligibleForSuprPass"
                    [fromSuprPassPage]="fromSuprPassPage"
                    [userCheckoutInfo]="userCheckoutInfo"
                    (handlePay)="handlePay.emit()"
                ></supr-cart-footer>
            </ng-container>
            <ng-template #oldFooter>
                <supr-cart-footer-dep
                    [amountToAdd]="amountToAdd"
                    [amountToPay]="amountToPay"
                    [suprCreditsUsed]="suprCreditsUsed"
                    [cartMeta]="cartMeta"
                    [isEligibleForSuprPass]="isEligibleForSuprPass"
                    [fromSuprPassPage]="fromSuprPassPage"
                    [userCheckoutInfo]="userCheckoutInfo"
                    (handlePay)="handlePay.emit()"
                >
                </supr-cart-footer-dep>
            </ng-template>
        </ng-template>
    `,
    styleUrls: ["../../styles/summary.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit, OnChanges {
    @Input() cartMeta: CartMeta;
    @Input() hasAddress: boolean;
    @Input() cartState: CartCurrentState;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() isEligibleForSuprPass: boolean;
    @Input() fromSuprPassPage: boolean;

    @Output() handlePay: EventEmitter<void> = new EventEmitter();

    hasError = false;
    amountToPay: number;
    amountToAdd: number;
    suprCreditsUsed: number;
    showNewPaymentFooter = false;

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCartMetaChange(changes["cartMeta"]);
        this.handleCartStateChange(changes["cartState"]);
    }

    private handleCartMetaChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setTotals();
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setError();
        }
    }

    private initData() {
        this.setNewFooterStatus();
        this.setError();
        this.setTotals();
    }

    private setNewFooterStatus() {
        this.showNewPaymentFooter = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_NEW_PAYMENT_FOOTER,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.SHOW_NEW_PAYMENT_FOOTER]
        );
    }

    private setError() {
        this.hasError =
            this.cartState === CartCurrentState.VALIDATION_FAILED ||
            this.cartState === CartCurrentState.CHECKOUT_FAILED;
    }

    private setTotals() {
        if (this.utilService.isEmpty(this.cartMeta)) {
            return;
        }

        this.setAmountToPay();
        this.setAmountToAdd();
        this.setSuprCreditsUsed();
    }

    private setAmountToPay() {
        const scUsed = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.supr_credits_used",
            0
        );

        this.amountToPay = this.getAmountToPay(scUsed);
    }

    private getAmountToPay(scUsed): number {
        const paymentData = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data",
            null
        );

        if (!paymentData) {
            return 0;
        }

        const {
            real_amount = 0,
            post_discount,
            supr_credits_used,
            delivery_fee = 0,
        } = this.cartMeta.payment_data;

        if (real_amount) {
            return real_amount;
        }

        if (scUsed) {
            return post_discount + delivery_fee - supr_credits_used;
        }

        return post_discount + delivery_fee;
    }

    private setAmountToAdd() {
        this.amountToAdd = Math.ceil(this.cartMeta.wallet_info.extra_required);
    }

    private setSuprCreditsUsed() {
        this.suprCreditsUsed = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.supr_credits_used",
            0
        );
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
