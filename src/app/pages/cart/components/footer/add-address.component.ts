import { Component, ChangeDetectionStrategy } from "@angular/core";

import { ADDRESS_FROM_PARAM } from "@constants";

import { RouterService } from "@services/util/router.service";

import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-footer-address",
    template: `
        <supr-sticky-wrapper>
            <div class="footer">
                <supr-text type="subtitle">{{ TEXTS.ALMOST_THERE }}</supr-text>
                <supr-text type="body" class="body">
                    {{ TEXTS.ADD_ADDRESS_TO_CONTINUE }}
                </supr-text>
                <div class="divider8"></div>
                <supr-button
                    saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_ADDRESS}"
                    (handleClick)="goToAddressPage()"
                >
                    <supr-text type="body">{{ TEXTS.CONTINUE }}</supr-text>
                </supr-button>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["../../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterAddressComponent {
    TEXTS = TEXTS;

    constructor(private routerService: RouterService) {}

    goToAddressPage() {
        this.routerService.goToAddressMapPage({
            queryParams: {
                from: ADDRESS_FROM_PARAM.CART,
            },
        });
    }
}
