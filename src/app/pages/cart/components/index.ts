import { HeaderComponent } from "./header/header.component";

import { FooterComponent } from "./footer/footer.component";
import { FooterAddressComponent } from "./footer/add-address.component";
import { FooterErrorComponent } from "./footer/error.component";

import { ItemWrapperComponent } from "./item/item-wrapper.component";
import { ItemCouponComponent } from "./item/item-coupon.component";
import { CartItemFooterComponent } from "./item/item-footer.component";
import { ItemPriceComponent } from "./item/item-price.component";
import { ItemQuantityComponent } from "./item/item-quantity.component";
import { ItemActionComponent } from "./item/item-action.component";
import { ItemRemoveModalComponent } from "./item/item-remove-modal.component";
import { ItemRescheduleModalComponent } from "./item/item-reschedule-modal.component";
import { ItemCounterComponent } from "./item/item-counter.component";
import { ItemErrorComponent } from "./item/item-error.component";
import { ItemDetailsComponent } from "./item/item-details.component";
import { ItemHeaderComponent } from "./item/item-header.component";
import { ItemBodyComponent } from "./item/item-body.component";

import { ItemGroupListComponent } from "./item-group/group-list.component";
import { SubscriptionGroupComponent } from "./item-group/subscription-group.component";
import { OneTimeGroupComponent } from "./item-group/onetime-group.component";

import { CouponComponent } from "./coupon/coupon.component";
import { CouponAddressComponent } from "./coupon/add-address-modal.componnent";

import { SummaryComponent } from "./summary/summary.component";

import { LoaderSkeletonComponent } from "./loader/skeleton.component";
import { LoaderOverlayComponent } from "./loader/overlay.component";

import { MessageContentComponent } from "./message/message.component";

import { ReviewDatesComponent } from "./review-dates/review-dates.component";
import { AlternateDeliveryBannerComponent } from "./ad-details/alternate-delivery.banner";
import { AlternateDeliveryModalComponent } from "./ad-details/alternate-delivery.modal.component";

import { DayHeaderComponent } from "./day-header/day-header.component";

import { AddSuprPassPromptComponent } from "./add-supr-pass-prompt/add-supr-pass-prompt.component";
import { CartCollectionComponent } from "./collection/collection.component";

import { CartPromptComponent } from "./prompt/prompt.component";

export const cartPageComponents = [
    HeaderComponent,

    FooterComponent,
    FooterAddressComponent,
    FooterErrorComponent,

    ItemWrapperComponent,
    ItemCouponComponent,
    CartItemFooterComponent,
    ItemPriceComponent,
    ItemQuantityComponent,
    ItemActionComponent,
    ItemRemoveModalComponent,
    ItemRescheduleModalComponent,
    ItemCounterComponent,
    ItemErrorComponent,
    ItemDetailsComponent,
    ItemHeaderComponent,
    ItemBodyComponent,

    ItemGroupListComponent,
    SubscriptionGroupComponent,
    OneTimeGroupComponent,

    CouponComponent,
    CouponAddressComponent,

    SummaryComponent,

    LoaderSkeletonComponent,
    LoaderOverlayComponent,

    MessageContentComponent,

    ReviewDatesComponent,
    AlternateDeliveryBannerComponent,
    AlternateDeliveryModalComponent,

    DayHeaderComponent,

    AddSuprPassPromptComponent,
    CartCollectionComponent,

    CartPromptComponent,
];
