import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { SettingsService } from "@services/shared/settings.service";
import { CartBannerConfig } from "@types";

@Component({
    selector: "supr-cart-review-dates",
    template: `
        <supr-cart-banner [config]="bannerConfig"> </supr-cart-banner>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewDatesComponent implements OnInit {
    bannerConfig: CartBannerConfig;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        const revieDatesComponentConfig = this.settingsService.getSettingsValue(
            "cartReviewDateComponentConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.cartReviewDateComponentConfig
        );

        if (revieDatesComponentConfig) {
            this.bannerConfig = {
                icon: revieDatesComponentConfig.icon,
                title: revieDatesComponentConfig.title,
                subtitle: revieDatesComponentConfig.subtitle,
            };
        }
    }
}
