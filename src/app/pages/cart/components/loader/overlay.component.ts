import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-cart-loader-overlay",
    template: `
        <supr-backdrop></supr-backdrop>
    `,
    styleUrls: ["../../styles/loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderOverlayComponent {}
