import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-cart-loader-skeleton",
    template: `
        <div class="suprScrollContent suprColumn center">
            <supr-loader></supr-loader>
        </div>
    `,
    styleUrls: ["../../styles/loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderSkeletonComponent {}
