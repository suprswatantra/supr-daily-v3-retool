import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import {
    AlternateDeliveryModalInfo,
    AlternateDeliveryBannerInfo,
} from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { CartBannerConfig } from "@types";

@Component({
    selector: "supr-cart-alternate-delivery-banner",
    template: `
        <supr-cart-banner
            [config]="bannerConfig"
            (handleActionClick)="handleShowModal()"
        ></supr-cart-banner>
        <supr-cart-alternate-delivery-modal
            [showModal]="showModal"
            [adModalInfo]="modalInfo"
            (handleClose)="handleModalClose()"
        ></supr-cart-alternate-delivery-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlternateDeliveryBannerComponent implements OnInit {
    showModal: boolean;
    bannerConfig: CartBannerConfig;
    modalInfo: AlternateDeliveryModalInfo;

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        const adInfo: AlternateDeliveryBannerInfo = this.settingsService.getSettingsValue(
            "cartAlternateDeliveryBannerDetails",
            SETTINGS_KEYS_DEFAULT_VALUE.cartAlternateDeliveryBannerDetails
        );

        if (adInfo) {
            this.modalInfo = adInfo.modalInfo;
            this.bannerConfig = {
                type: adInfo.type,
                icon: adInfo.icon,
                title: adInfo.title,
                ctaIcon: adInfo.ctaIcon,
                cta: adInfo.showMoreText,
                subtitle: adInfo.subtitle,
            };
        }
    }

    handleShowModal() {
        this.showModal = true;
    }

    handleModalClose() {
        this.showModal = false;
    }
}
