import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE, MODAL_NAMES } from "@constants";

import { AlternateDeliveryModalInfo } from "@shared/models";

import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-cart-alternate-delivery-modal",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleClose?.emit()"
                modalName="${MODAL_NAMES.DELIVERY_FEE_INFO_MODAL}"
            >
                <div class="modalWrapper" (click)="handleImageClick()">
                    <supr-image
                        [src]="adModalInfo?.image"
                        [imgWidth]="
                            ${CLODUINARY_IMAGE_SIZE.CAMPAIGN_POPUP.WIDTH}
                        "
                        [imgHeight]="
                            ${CLODUINARY_IMAGE_SIZE.CAMPAIGN_POPUP.HEIGHT}
                        "
                        [lazyLoad]="false"
                        [withWrapper]="false"
                    ></supr-image>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/ad-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlternateDeliveryModalComponent {
    @Input() showModal: boolean;
    @Input() adModalInfo: AlternateDeliveryModalInfo;
    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    constructor(private routerService: RouterService) {}

    handleImageClick() {
        const { url, outsideRedirection } = this.adModalInfo;
        if (url) {
            if (outsideRedirection) {
                window.open(url, "_system", "location=yes");
            } else {
                this.routerService.goToUrl(url);
            }
        } else {
            this.handleClose.emit();
        }
    }
}
