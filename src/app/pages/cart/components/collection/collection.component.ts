import {
    Component,
    Input,
    Output,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { V3Layout } from "@models";

import { HomeLayoutCurrentState } from "@store/home-layout/home-layout.state";

@Component({
    selector: "supr-cart-collection",
    template: `
        <ng-container *ngIf="!loading">
            <ng-container
                *ngFor="
                    let layout of collectionList;
                    trackBy: trackByFn;
                    index as position;
                    last as isLast
                "
            >
                <div class="collection">
                    <supr-v3-widget-container
                        [layout]="layout"
                        saImpression
                        [saImpressionEnabled]="
                            layout?.data?.analytics?.saImpression
                        "
                        [saObjectName]="
                            layout?.data?.analytics?.saImpression?.objectName
                        "
                        [saObjectValue]="
                            layout?.data?.analytics?.saImpression?.objectValue
                        "
                        [saPosition]="position + 1"
                    ></supr-v3-widget-container>

                    <div class="divider24"></div>
                </div>
            </ng-container>
        </ng-container>
    `,
    styleUrls: ["../../styles/collection.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartCollectionComponent implements OnInit, OnChanges {
    @Input() collectionList: V3Layout[];
    @Input() collectionState: HomeLayoutCurrentState;

    @Output() handleFetchHomeLayouts: EventEmitter<void> = new EventEmitter();

    loading = true;

    ngOnInit() {
        this.fetchHomeLayoutList();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleHomeLayoutStateChange(changes["homeLayoutState"]);
    }

    trackByFn(homeLayout: V3Layout): string {
        return homeLayout.widgetType;
    }

    private fetchHomeLayoutList() {
        if (this.collectionState === HomeLayoutCurrentState.NO_ACTION) {
            this.handleFetchHomeLayouts.emit();
        } else {
            this.loading = false;
        }
    }

    private handleHomeLayoutStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        const { previousValue, currentValue } = change;
        if (
            previousValue !== HomeLayoutCurrentState.NO_ACTION &&
            currentValue === HomeLayoutCurrentState.NO_ACTION
        ) {
            this.fetchHomeLayoutList();
        } else if (this.fetchHomeLayoutListDone()) {
            this.loading = false;
        }
    }

    private fetchHomeLayoutListDone(): boolean {
        return (
            this.collectionState ===
            HomeLayoutCurrentState.FETCHING_HOME_LAYOUT_DONE
        );
    }
}
