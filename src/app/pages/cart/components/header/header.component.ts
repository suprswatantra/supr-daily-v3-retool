import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-cart-header",
    template: `
        <supr-page-header>
            <supr-text type="body">Cart {{ itemCountStr }}</supr-text>
        </supr-page-header>
    `,
    styles: [
        `
            supr-page-header {
                supr-text {
                    --supr-text-color: var(--black-40);
                }
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    @Input()
    set itemCount(value: number) {
        this.setItemCountStr(value);
    }

    itemCountStr = "";

    private setItemCountStr(count: number) {
        if (count > 0) {
            this.itemCountStr = `(${count} item${count > 1 ? "s" : ""})`;
        } else {
            this.itemCountStr = "";
        }
    }
}
