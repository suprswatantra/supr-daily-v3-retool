import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { CartMeta, DeliveryFeeMessageInfo, Banner } from "@models";
import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { TEXTS, SUPR_PASS_TEXTS, SAVINGS_TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

import { CartCurrentState } from "@store/cart/cart.state";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-cart-summary",
    templateUrl: "./summary.component.html",
    styleUrls: ["../../styles/summary.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryComponent implements OnInit, OnChanges {
    @Input() cartMeta: CartMeta;
    @Input() cartState: CartCurrentState;
    @Input() isEligibleForSuprPass: boolean;

    TEXTS = TEXTS;
    savingsTexts = SAVINGS_TEXTS;

    _totalAmount: number;
    _discountAmount: number;
    _suprCreditsUsed: number;
    _finalAmount: number;
    _balance: number;
    _amountToAdd: number;
    _suprCreditsGainedAmount: number;
    _deliveryFee: number;
    _preDeliveryFee: number;
    _isSuprPassApplied: boolean;
    delFeeInfo: DeliveryFeeMessageInfo;
    showDeliveryFeeInfoModal: boolean;
    applySuprPassText: string;
    appliedSuprPassText: string;
    suprPassText: string;
    _serviceFeeBannerInfo: Banner;
    savingsPercentText: string;
    savingsCollapsed = false;

    saObjectNames = ANALYTICS_OBJECT_NAMES;
    suprAccessCartCtaObjectName = SA_OBJECT_NAMES.CLICK.SUPR_ACCESS_CART_CTA;

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["cartMeta"])) {
            this.initData();
        }
    }

    toggleSavings() {
        this.savingsCollapsed = !this.savingsCollapsed;
    }

    showDeliveryFeeInfo() {
        this.showDeliveryFeeInfoModal = true;
    }

    closeDeliveryFeeInfo() {
        this.showDeliveryFeeInfoModal = false;
    }

    goToSuprPassPage() {
        this.delayOpen(() => this.routerService.goToSuprPassPage());
    }

    private delayOpen(fn: () => void) {
        setTimeout(fn, 100);
    }

    private initData() {
        if (!this.cartMeta) {
            return;
        }

        this.setTotalAmount();
        this.setDiscountAmount();
        this.setSuprCreditsGainedAmount();
        this.setSuprCreditsUsed();
        this.setAmountToAdd();
        this.setDeliveryFee();
        this.setDelFeeInfo();
        this.setSuprPassInfo();
        this.setShowServiceFeeBanner();
        this.setSavingsPercent();
    }

    private setSavingsPercent() {
        const cartSavingsPercent = this.utilService.getNestedValue(
            this.cartMeta,
            "savings_info.savings_percent",
            0
        );

        if (!cartSavingsPercent) {
            this.savingsPercentText = "";
            return;
        }

        this.savingsPercentText = `(${cartSavingsPercent}%)`;
    }

    private setShowServiceFeeBanner() {
        const serviceFeeBanner = this.utilService.getNestedValue(
            this.cartMeta,
            "service_fee_banner"
        );

        if (!serviceFeeBanner || this.hasCartError()) {
            this._serviceFeeBannerInfo = null;
            return;
        }

        this._serviceFeeBannerInfo = serviceFeeBanner;
    }

    private hasCartError() {
        return (
            this.cartState === CartCurrentState.VALIDATION_FAILED ||
            this.cartState === CartCurrentState.CHECKOUT_FAILED
        );
    }

    private setTotalAmount() {
        const { payment_data } = this.cartMeta;
        this._totalAmount = payment_data.pre_discount;

        const scUsed = this.utilService.getNestedValue(
            payment_data,
            "supr_credits_used",
            0
        );

        this._finalAmount = this.getAmountToPay(scUsed);
    }

    private getAmountToPay(scUsed): number {
        const {
            real_amount = 0,
            post_discount,
            supr_credits_used,
            delivery_fee = 0,
        } = this.cartMeta.payment_data;

        if (real_amount) {
            return real_amount;
        }

        if (scUsed) {
            return post_discount + delivery_fee - supr_credits_used;
        }

        return post_discount + delivery_fee;
    }

    private setDiscountAmount() {
        this._discountAmount = 0;

        const { total_discount_amount } = this.cartMeta;

        this._discountAmount =
            (total_discount_amount ? total_discount_amount : 0) * -1;
    }

    private setDeliveryFee() {
        this._deliveryFee = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.delivery_fee",
            0
        );

        this._preDeliveryFee = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.pre_delivery_fee",
            0
        );
    }

    private setSuprCreditsGainedAmount() {
        this._suprCreditsGainedAmount = this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.supr_credits_awarded",
            0
        );
    }

    private setSuprPassInfo() {
        this._isSuprPassApplied = this.utilService.getNestedValue(
            this.cartMeta,
            "supr_pass_applied",
            false
        );

        const suprPassTexts = this.settingsService.getSettingsValue(
            "suprPassTexts",
            SUPR_PASS_TEXTS
        );

        this.applySuprPassText = suprPassTexts.APPLY_SUPR_PASS;
        this.appliedSuprPassText = suprPassTexts.SUPR_PASS_APPLIED;
        this.suprPassText = suprPassTexts.SUPR_PASS;
    }

    private setSuprCreditsUsed() {
        this._suprCreditsUsed = 0;

        const { payment_data } = this.cartMeta;

        if (payment_data) {
            if (payment_data.supr_credits_used) {
                this._suprCreditsUsed = payment_data.supr_credits_used * -1;
            }
        }
    }

    private setAmountToAdd() {
        const { wallet_info } = this.cartMeta;
        this._balance = wallet_info.balance;
        this._amountToAdd = Math.ceil(wallet_info.extra_required);
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private setDelFeeInfo() {
        this.delFeeInfo = this.utilService.getNestedValue(
            this.cartMeta,
            "delivery_fee_message_info"
        );
    }
}
