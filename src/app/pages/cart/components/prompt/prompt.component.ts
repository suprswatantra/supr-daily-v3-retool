import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { CartMeta } from "@shared/models";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";
import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-cart-prompt",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                modalName="${MODAL_NAMES.CART_PROMPT}"
                (handleClose)="handleHideModal.emit()"
            >
                <div class="container">
                    <div *ngIf="cartMeta?.cart_prompt?.title">
                        <div class="suprRow">
                            <supr-text type="subtitle" class="title">
                                {{ cartMeta?.cart_prompt?.title }}
                            </supr-text>
                        </div>
                        <div class="divider16"></div>
                    </div>

                    <div *ngIf="cartMeta?.cart_prompt?.imgUrl">
                        <supr-image
                            [src]="cartMeta?.cart_prompt?.imgUrl"
                            [imgWidth]="500"
                        ></supr-image>
                        <div class="divider16"></div>
                    </div>

                    <div *ngIf="cartMeta?.cart_prompt?.subTitle">
                        <supr-text type="body" class="subtitle">
                            {{ cartMeta?.cart_prompt?.subTitle }}
                        </supr-text>
                        <div class="divider16"></div>
                    </div>

                    <div class="divider24"></div>

                    <div class="suprRow">
                        <div class="buttonWrapper">
                            <supr-button
                                class="remove"
                                (handleClick)="closeModal()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .CART_PROMPT_SKIP}"
                            >
                                <supr-text type="body">
                                    {{ cartMeta?.cart_prompt?.skipBtnText }}
                                </supr-text>
                            </supr-button>
                        </div>

                        <div class="buttonWrapper right">
                            <supr-button
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .CART_PROMPT_ACTION}"
                                (handleClick)="primaryBtnClick()"
                            >
                                <supr-text type="body">
                                    {{ cartMeta?.cart_prompt?.actionBtnText }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/cart-prompt.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartPromptComponent {
    @Input() cartMeta: CartMeta;
    @Input() showModal: boolean;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    constructor(
        private modalService: ModalService,
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    primaryBtnClick() {
        this.closeModal();

        const actionUrl = this.utilService.getNestedValue(
            this.cartMeta,
            "cart_prompt.actionUrl",
            null
        );

        if (!actionUrl) {
            return;
        }

        this.routerService.goToUrl(actionUrl);
    }

    closeModal() {
        this.modalService.closeModal();
    }
}
