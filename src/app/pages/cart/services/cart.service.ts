import { Injectable } from "@angular/core";

import { CART_ITEM_TYPES } from "@constants";
import { CartItem, Sku } from "@models";
import { SuprApi, Segment } from "@types";

import { DateService } from "@services/date/date.service";

import {
    CartItemsGroup,
    CartItemsGroupByDate,
    ListItemGroupedByDate,
} from "@pages/cart/types";
import { ANALYTICS_TEXTS } from "../constants/analytics.constants";

@Injectable()
export class CartPageService {
    constructor(private dateService: DateService) {}

    splitCartItemsIntoGroupsByModes(items: CartItem[]): CartItemsGroup {
        if (!items.length) {
            return;
        }

        const subscriptionList = [];
        const oneTimeList = [];

        items.forEach((item) =>
            this.isSubscriptionItem(item)
                ? subscriptionList.push(item)
                : oneTimeList.push(item)
        );

        return {
            subscriptionList: this.sortItemsByDeliveryDate(subscriptionList),
            oneTimeList: this.sortItemsByDeliveryDate(oneTimeList),
        };
    }

    splitCartItemsIntoGroupsByModesAndDates(
        items: CartItem[]
    ): CartItemsGroupByDate {
        if (!items.length) {
            return;
        }

        let subscriptionList = [];
        let oneTimeList = [];

        items.forEach((item) =>
            this.isSubscriptionItem(item)
                ? subscriptionList.push(item)
                : oneTimeList.push(item)
        );

        subscriptionList = this.sortItemsByDeliveryDate(subscriptionList);
        oneTimeList = this.sortItemsByDeliveryDate(oneTimeList);

        return {
            subscriptionList: this.groupSubscriptionsByStartDate(
                subscriptionList
            ),
            oneTimeList: this.groupAddonsByDeliveryDate(oneTimeList),
        };
    }

    getValidateCartBody(items: CartItem[]): SuprApi.CartBody {
        let coupon_code: string;

        const itemWithCoupon = items.find((item) => item && !!item.coupon_code);

        if (itemWithCoupon && itemWithCoupon.coupon_code) {
            coupon_code = itemWithCoupon.coupon_code;
        }

        const body = { items } as SuprApi.CartBody;

        if (coupon_code) {
            body.coupon_code = coupon_code;
        }

        return body;
    }

    getOOSAnalytics(sku: Sku, cartItem: CartItem): Segment.ContextListItem[] {
        const contextList: Segment.ContextListItem[] = [];

        if (sku && cartItem) {
            contextList.push(
                {
                    name: ANALYTICS_TEXTS.UNIT_MRP,
                    value: sku.unit_mrp,
                },
                {
                    name: ANALYTICS_TEXTS.UNIT_PRICE,
                    value: sku.unit_price,
                },
                {
                    name: ANALYTICS_TEXTS.TYPE,
                    value: cartItem.type,
                }
            );

            if (cartItem.type === CART_ITEM_TYPES.ADDON) {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.SCHEDULE_DATE,
                        value: cartItem.delivery_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QUANTITY,
                        value: cartItem.quantity,
                    }
                );
            } else {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.START_DATE,
                        value: cartItem.start_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QTY_PER_DELIVERY,
                        value: cartItem.quantity,
                    },
                    {
                        name: ANALYTICS_TEXTS.RECHARGE_QTY,
                        value: cartItem.recharge_quantity,
                    }
                );
            }
        }

        return contextList;
    }

    /***********************************
     *       Private Methods           *
     **********************************/

    private sortItemsByDeliveryDate(items: CartItem[]): CartItem[] {
        return items.sort((a, b) => {
            const aDeliveryDate = this.getCartItemDeliveryDate(a);
            const bDeliveryDate = this.getCartItemDeliveryDate(b);

            const aDate = this.dateService.dateFromText(aDeliveryDate);
            const bDate = this.dateService.dateFromText(bDeliveryDate);

            if (aDate && bDate) {
                return aDate.getTime() - bDate.getTime();
            } else {
                return 0;
            }
        });
    }

    private getCartItemDeliveryDate(item: CartItem): string {
        switch (item.type) {
            case CART_ITEM_TYPES.ADDON:
                return item.delivery_date;
            case CART_ITEM_TYPES.SUBSCRIPTION_NEW:
            case CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE:
                return item.start_date;
            default:
                return "";
        }
    }

    private isSubscriptionItem(item: CartItem): boolean {
        return (
            item.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW ||
            item.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        );
    }

    private groupSubscriptionsByStartDate(
        items: CartItem[]
    ): Array<ListItemGroupedByDate> {
        return items.reduce((accum, item) => {
            const length = accum.length;
            if (length && accum[length - 1].date === item.start_date) {
                accum[length - 1].items = [...accum[length - 1].items, item];
            } else {
                accum.push({
                    date: item.start_date,
                    items: [item],
                });
            }
            return accum;
        }, []);
    }

    private groupAddonsByDeliveryDate(
        items: CartItem[]
    ): Array<ListItemGroupedByDate> {
        return items.reduce((accum, item) => {
            const length = accum.length;
            if (length && accum[length - 1].date === item.delivery_date) {
                accum[length - 1].items = [...accum[length - 1].items, item];
            } else {
                accum.push({
                    date: item.delivery_date,
                    items: [item],
                });
            }
            return accum;
        }, []);
    }
}
