import {
    Input,
    Output,
    OnInit,
    Component,
    OnChanges,
    OnDestroy,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import {
    SETTINGS,
    CART_ITEM_TYPES,
    SETTINGS_KEYS_DEFAULT_VALUE,
    CART_LAYPUT_ELEMENT_IDS,
} from "@constants";

import { CartItem, CartMeta, V3Layout } from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { CartService } from "@services/shared/cart.service";
import { RouterService } from "@services/util/router.service";
import { CalendarService } from "@services/date/calendar.service";
import { SettingsService } from "@services/shared/settings.service";
import { SegmentService } from "@services/integration/segment.service";
import { ToastService } from "@services/layout/toast.service";

import { CartCurrentState } from "@store/cart/cart.state";

import { SuprApi } from "@types";

import { TEXTS } from "@pages/cart/constants";
import { CartPageService } from "@pages/cart/services/cart.service";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";
import { HomeLayoutCurrentState } from "@store/home-layout/home-layout.state";

@Component({
    selector: "supr-cart-layout",
    template: `
        <div class="suprContainer">
            <ng-container *ngIf="_cartUpdating; else cartContent">
                <supr-cart-loader-skeleton></supr-cart-loader-skeleton>
            </ng-container>

            <ng-template #cartContent>
                <supr-cart-header-container></supr-cart-header-container>
                <div
                    class="suprScrollContent"
                    [class.error]="hasError"
                    [class.newFooter]="showNewPaymentFooter"
                >
                    <ng-container *ngIf="displayAlternateDeliveryBanner()">
                        <supr-cart-alternate-delivery-banner></supr-cart-alternate-delivery-banner>
                        <div class="divider8"></div>
                    </ng-container>

                    <ng-container *ngIf="tpEnabled">
                        <supr-t-plus-one-banner> </supr-t-plus-one-banner>
                        <div class="divider8"></div>
                    </ng-container>

                    <supr-cart-reward-sku-banner-container
                        *ngIf="cartMeta?.reward_sku_banner"
                        [skuRewardCartInfo]="cartMeta?.reward_sku_banner"
                    ></supr-cart-reward-sku-banner-container>

                    <div class="divider8"></div>
                    <!-- Supr Pass saving banner, data from BE -->
                    <ng-container
                        *ngIf="
                            cartMeta?.access_savings_banner &&
                            showSuprPassSavingsBanner
                        "
                    >
                        <supr-cart-banner
                            class="suprPassSavingsBanner"
                            [config]="cartMeta?.access_savings_banner"
                            [isIconRightAligned]="true"
                            (handleActionClick)="goToSuprPassPage()"
                        ></supr-cart-banner>
                        <div class="divider8"></div>
                    </ng-container>

                    <ng-container *ngIf="showReviewDatesBanner()">
                        <supr-cart-review-dates></supr-cart-review-dates>
                        <div class="divider8"></div>
                    </ng-container>

                    <supr-cart-item-group-list
                        [cartItems]="cartItems"
                        [slotInfo]="cartMeta?.slot_info"
                        id="${CART_LAYPUT_ELEMENT_IDS.ITEMS_LIST}"
                    ></supr-cart-item-group-list>

                    <ng-container *ngIf="cartMeta?.common_info_banner">
                        <div class="subsMessageWrapper">
                            <supr-banner
                                [banner]="cartMeta?.common_info_banner"
                            ></supr-banner>
                        </div>
                    </ng-container>

                    <ng-template #divider8>
                        <div class="divider8"></div>
                    </ng-template>

                    <supr-past-order-collection
                        *ngIf="pastOrderSkuList.length > 0"
                        type="cart"
                        [pastOrderSkuList]="pastOrderSkuList"
                        (handleScrollPosition)="updateScrollPosition($event)"
                    ></supr-past-order-collection>

                    <supr-cart-collection
                        [collectionList]="collectionList"
                        [collectionState]="collectionState"
                        (handleFetchHomeLayouts)="handleFetchHomeLayouts.emit()"
                    ></supr-cart-collection>

                    <supr-discount-block-container></supr-discount-block-container>

                    <div class="summaryWrapper">
                        <supr-cart-message-container></supr-cart-message-container>

                        <div class="divider24"></div>

                        <supr-cart-summary-container
                            id="${CART_LAYPUT_ELEMENT_IDS.SUMMARY}"
                        ></supr-cart-summary-container>
                    </div>
                </div>
                <supr-cart-footer-container
                    (handlePay)="placeOrder()"
                ></supr-cart-footer-container>
                <ng-container *ngIf="_validatingCart">
                    <supr-cart-loader-overlay></supr-cart-loader-overlay>
                </ng-container>
                <supr-loader-overlay *ngIf="_placingOrder">
                    {{ TEXTS.PLACING_ORDER }}
                </supr-loader-overlay>
            </ng-template>

            <supr-cart-prompt
                [showModal]="showCartPrompt"
                [cartMeta]="cartMeta"
                (handleHideModal)="hideCartPrompt()"
            ></supr-cart-prompt>
        </div>
    `,
    styleUrls: ["../styles/cart.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartPageLayoutComponent implements OnInit, OnChanges, OnDestroy {
    @Input() cartError: any;
    @Input() cartId: string;
    @Input() cartMeta: CartMeta;
    @Input() cartItems: CartItem[];
    @Input() cartModified: boolean;
    @Input() cartReValidate: boolean;
    @Input() pastOrderSkuList: Array<number>;
    @Input() cartCurrentState: CartCurrentState;
    @Input() isAlternateDeliveryEligible: boolean;
    @Input() collectionList: V3Layout[];
    @Input() collectionState: HomeLayoutCurrentState;

    @Output() handleCheckoutCart: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchHomeLayouts: EventEmitter<void> = new EventEmitter();
    @Output()
    handleValidateCart: EventEmitter<SuprApi.CartBody> = new EventEmitter();

    domEl: string;
    hasError = false;
    tpEnabled = false;
    _cartUpdating = false;
    _placingOrder = false;
    _validatingCart = false;
    showCartReviewDates = false;
    showAlternateDeliveryBanner = false;
    showNewPaymentFooter = false;
    showCartPrompt = false;
    showSuprPassSavingsBanner: boolean;

    TEXTS = TEXTS;

    private init = false;
    private isCheckingOut = false;
    private routerSub: Subscription;

    constructor(
        private router: Router,
        private segment: SegmentService,
        private cartService: CartService,
        private utilService: UtilService,
        private dateService: DateService,
        private routerService: RouterService,
        private cartPageService: CartPageService,
        private calendarService: CalendarService,
        private settingsService: SettingsService,
        private cdr: ChangeDetectorRef,
        private toastService: ToastService
    ) {
        this.initRouterEvents();
    }

    ngOnInit() {
        this.init = true;

        if (this.cartModified || this.cartReValidate) {
            this.updateCart();
        }

        this.setTpStatus();
        this.setCartShowReviewDatesStatus();
        this.setShowAlternateDeliveryBanner();
        this.setNewFooterStatus();
        this.handleAutoCheckout();
        this.triggerTPCartImpression();
        this.setShowSuprPassSavingsBanner();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCartItemsChange(changes["cartItems"]);
        this.handleCartStateChange(changes["cartCurrentState"]);
        this.handleCartReValidateChange(changes["cartReValidate"]);
        this.handleCartModifiedChange(changes["cartModified"]);
        this.handleCartMetaChange(changes["cartMeta"]);
    }

    ngOnDestroy() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    placeOrder() {
        this.isCheckingOut = true;
        setTimeout(() => {
            this.handleValidateCart.emit();
        }, 0);
    }

    updateScrollPosition(domId: string) {
        this.domEl = domId;
    }

    areAllItemsScheduledForTomorrow(): boolean {
        let areAllItemsScheduledForTomorrow = true;
        if (this.utilService.isLengthyArray(this.cartItems)) {
            this.cartItems.forEach((item: CartItem) => {
                const deliveryDateText =
                    item.type === CART_ITEM_TYPES.ADDON
                        ? item.delivery_date
                        : item.start_date;
                if (deliveryDateText) {
                    const tomorrowDateText = this.dateService.textFromDate(
                        this.dateService.getTomorrowDate()
                    );
                    areAllItemsScheduledForTomorrow =
                        areAllItemsScheduledForTomorrow &&
                        tomorrowDateText === deliveryDateText;
                }
            });
        }

        return areAllItemsScheduledForTomorrow;
    }

    showReviewDatesBanner() {
        return (
            !this.displayAlternateDeliveryBanner() &&
            this.showCartReviewDates &&
            !this.areAllItemsScheduledForTomorrow()
        );
    }

    displayAlternateDeliveryBanner() {
        return (
            this.showAlternateDeliveryBanner && this.isAlternateDeliveryEligible
        );
    }

    goToSuprPassPage() {
        this.routerService.goToSuprPassPage();
    }

    hideCartPrompt() {
        this.showCartPrompt = false;
        this.cartService.setCartPromptShownProps(this.cartId);
    }

    private setShowSuprPassSavingsBanner() {
        this.showSuprPassSavingsBanner = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_SUPR_PASS_SAVINGS_BANNER_ON_CART,
            SETTINGS_KEYS_DEFAULT_VALUE[
                SETTINGS.SHOW_SUPR_PASS_SAVINGS_BANNER_ON_CART
            ]
        );
    }

    private async setShowCartPrompt() {
        this.showCartPrompt = await this.cartService.canShowCartPrompt(
            this.cartMeta,
            this.cartId
        );

        this.cdr.detectChanges();
    }

    private setNewFooterStatus() {
        this.showNewPaymentFooter = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_NEW_PAYMENT_FOOTER,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.SHOW_NEW_PAYMENT_FOOTER]
        );
    }

    private setShowAlternateDeliveryBanner() {
        const adBannerConfig = this.settingsService.getSettingsValue(
            "cartAlternateDeliveryBannerDetails",
            SETTINGS_KEYS_DEFAULT_VALUE.cartAlternateDeliveryBannerDetails
        );
        this.showAlternateDeliveryBanner = adBannerConfig.enabled;
    }

    private setCartShowReviewDatesStatus() {
        const revieDatesComponentConfig = this.settingsService.getSettingsValue(
            "cartReviewDateComponentConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.cartReviewDateComponentConfig
        );
        this.showCartReviewDates = revieDatesComponentConfig.enabled;
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private initRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isCartPageUrl(event.url))
            )
            .subscribe(() => {
                if (this.init) {
                    this.setTpStatus();
                    this.handleAutoCheckout();
                }
            });
    }

    private handleCartItemsChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.checkCartEmpty();
            this.triggerTPCartImpression();
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (!this.canHandleChange(change)) {
            return;
        }

        this.setError();

        if (change.currentValue === CartCurrentState.CHECKOUT_FAILED) {
            this.resetUpdatingFlags();
        } else if (this.isValidatingCart(change)) {
            this._validatingCart = true;
        } else if (this.isValidationDone(change)) {
            if (this.isCheckingOut) {
                if (this.canProceedToPlaceOrder(change)) {
                    this.checkoutCart();
                } else {
                    this.isCheckingOut = false;
                    this.resetUpdatingFlags();
                }
            } else {
                this.resetUpdatingFlags();
            }

            this.handleScrollOfPastOrder();
        } else if (this.isPlacingOrder(change)) {
            this._placingOrder = true;
        } else if (this.isCheckoutDone(change)) {
            if (!this.cartError) {
                this.handlePlaceOrderSuccess();
            } else {
                this.resetUpdatingFlags();
            }
        }
    }

    private setError() {
        this.hasError =
            this.cartCurrentState === CartCurrentState.VALIDATION_FAILED ||
            this.cartCurrentState === CartCurrentState.CHECKOUT_FAILED;
    }

    private handleScrollOfPastOrder() {
        if (this.domEl) {
            setTimeout(() => {
                const elem = document.getElementById(this.domEl);
                if (!elem) {
                    return;
                }
                elem.scrollIntoView({
                    behavior: "smooth",
                    block: "center",
                });
                this.domEl = null;
            }, 0);
        }
    }

    private handleCartReValidateChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.cartReValidate) {
            this.updateCart();
        }
    }

    private handleCartModifiedChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.cartModified) {
            this.updateCart();
        }
    }

    private handleCartMetaChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setShowCartPrompt();
            this.showUrgentToast();
        }
    }

    private showUrgentToast() {
        const toastMsg = this.utilService.getNestedValue(
            this.cartMeta,
            "toast_message_info",
            null
        );

        if (!toastMsg) {
            return;
        }

        this.toastService.present(toastMsg);
    }

    private checkoutCart() {
        const canCheckout = this.utilService.getNestedValue(
            this.cartMeta,
            "wallet_info.is_sufficient",
            false
        );

        if (canCheckout) {
            setTimeout(() => this.handleCheckoutCart.emit(), 0);
        } else {
            this.resetUpdatingFlags();
        }
    }

    private handleAutoCheckout() {
        const isAutoCheckoutSet = this.cartService.getAutoCheckoutFlag();
        if (isAutoCheckoutSet) {
            this.cartService.clearAutoCheckoutFlag();
            this.placeOrder();
        }
    }

    private handlePlaceOrderSuccess() {
        this.triggerOrderSuccessEvent();
        this.routerService.goToThankyouPage({ replaceUrl: true });
    }

    private canProceedToPlaceOrder(change: SimpleChange) {
        if (this.cartError || this.isValidationFailed(change)) {
            return false;
        } else {
            return true;
        }
    }

    private isValidatingCart(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.UPDATING_CART;
    }

    private isValidationDone(change: SimpleChange): boolean {
        return (
            change.previousValue === CartCurrentState.UPDATING_CART &&
            (change.currentValue === CartCurrentState.NO_ACTION ||
                change.currentValue === CartCurrentState.VALIDATION_FAILED ||
                change.currentValue === CartCurrentState.CHECKOUT_FAILED)
        );
    }

    private isValidationFailed(change: SimpleChange): boolean {
        return (
            change.currentValue === CartCurrentState.VALIDATION_FAILED ||
            change.currentValue === CartCurrentState.CHECKOUT_FAILED
        );
    }

    private isPlacingOrder(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.PLACING_ORDER;
    }

    private isCheckoutDone(change: SimpleChange): boolean {
        return (
            change.previousValue === CartCurrentState.PLACING_ORDER &&
            change.currentValue === CartCurrentState.NO_ACTION
        );
    }

    private checkCartEmpty() {
        if (
            this.isCartEmpty() &&
            this.cartCurrentState === CartCurrentState.NO_ACTION
        ) {
            this.routerService.goBack();
        }
    }

    private updateCart() {
        if (this.isCartEmpty()) {
            this.routerService.goBack();
        } else {
            this._cartUpdating = true;

            const cartBody = this.cartPageService.getValidateCartBody(
                this.cartItems
            );

            setTimeout(() => this.handleValidateCart.emit(cartBody), 0);
        }
    }

    private isCartEmpty(): boolean {
        return !this.cartItems || !this.cartItems.length;
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private resetUpdatingFlags() {
        this._validatingCart = false;
        this._cartUpdating = false;
        this._placingOrder = false;
        this.isCheckingOut = false;
    }

    private isCartPageUrl(url: string): boolean {
        return url && url.indexOf("cart") > -1;
    }

    private triggerTPCartImpression() {
        const objectValue = this.cartTpImpression();
        if (this.tpEnabled) {
            this.segment.trackImpression({
                objectName: SA_OBJECT_NAMES.IMPRESSION.TPONE_BANNER_CART,
                objectValue,
            });
        }
    }
    private cartDeliveryDates() {
        try {
            return (
                this.cartItems.reduce((acc, item) => {
                    const dateText = item.delivery_date
                        ? item.delivery_date
                        : item.start_date;
                    if (dateText) {
                        const date = this.dateService.dateFromText(dateText);
                        const daysBetweenTodayAndDate = this.dateService.daysFromToday(
                            date
                        );
                        acc.push(daysBetweenTodayAndDate);
                    }
                    return acc;
                }, []) || [0]
            );
        } catch (err) {
            return [];
        }
    }

    private cartTpImpression() {
        return `tpEnabled: ${this.tpEnabled}, totalItems: ${
            this.cartItems.length
        }, items: [${this.cartDeliveryDates()}]`;
    }

    private triggerOrderSuccessEvent() {
        const objectValue = this.cartDeliveryDates() || [];
        this.segment.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.EARLIEST_DELIVERY_DATE,
            objectValue: `${Math.min(...objectValue)}`,
        });
    }
}
