import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { OffersList } from "@models";
import { CouponCode } from "@types";

import { ToastService } from "@services/layout/toast.service";
import { RouterService } from "@services/util/router.service";

import { CartCurrentState } from "@store/cart/cart.state";

import { TEXTS, APPLY_COUPON_ICON } from "../constants";

@Component({
    selector: "supr-cart-offers-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="body">${TEXTS.OFFERS}</supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-section-header
                    icon="${APPLY_COUPON_ICON}"
                    title="${TEXTS.APPLY_COUPON}"
                    subtitle="${TEXTS.APPLY_COUPON_SUBTITLE}"
                >
                </supr-section-header>
                <supr-apply-coupon-input
                    [hideHeading]="true"
                    [invalidCoupon]="invalidCoupon"
                    [couponErrorMsg]="couponMsg"
                    [isApplyingCouponCode]="isApplyingCouponCode"
                    (handleApply)="handleCouponApply($event)"
                    (handleCouponChange)="couponChange()"
                ></supr-apply-coupon-input>
                <supr-offers-list
                    [isFetching]="isFetchingOffers"
                    [offersList]="cartOffersList"
                    (handleCouponTapApply)="handleCouponTapToApply($event)"
                ></supr-offers-list>
            </div>
            <supr-loader-overlay *ngIf="applyingCoupon"> </supr-loader-overlay>
        </div>
    `,
    styleUrls: ["../styles/offers.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartOffersLayout implements OnChanges {
    @Input() couponCode: CouponCode;
    @Input() cartCurrentState: CartCurrentState;
    @Input() hasAddress: boolean;
    @Input() isFetchingOffers: boolean;
    @Input() cartOffersList: OffersList;
    @Input() isApplyingCouponCode: boolean;

    @Output() handleUpdateCoupon: EventEmitter<string> = new EventEmitter();

    couponCodeValue = "";
    invalidCoupon = false;
    applyingCoupon = false;
    couponMsg = "";

    constructor(
        private routerService: RouterService,
        private toastService: ToastService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleCouponCodeChange(changes["couponCode"]);
        this.handleCartStateChange(changes["cartCurrentState"]);
    }

    handleCouponApply(couponCode: string) {
        this.applyingCoupon = true;
        this.handleUpdateCoupon.emit(couponCode);
    }

    couponChange() {
        this.invalidCoupon = false;
        this.couponMsg = "";
        this.applyingCoupon = false;
    }

    handleCouponTapToApply(couponCode: string) {
        this.applyingCoupon = true;
        this.handleUpdateCoupon.emit(couponCode);
    }

    private handleCoupon() {
        if (this.couponCode) {
            this.setCoupon();
        } else {
            this.unsetCoupon();
        }
    }

    private setCoupon() {
        const couponData = this.couponCode;

        this.couponCodeValue = couponData.couponCode;
        this.invalidCoupon = !couponData.isApplied;
        this.couponMsg = couponData.message;

        if (
            this.couponCodeValue &&
            !this.invalidCoupon &&
            !couponData.couponApplying
        ) {
            this.toastService.present(
                `${this.couponCodeValue} applied successfully`
            );
            this.routerService.goBack();
        }
    }

    private unsetCoupon() {
        this.couponCodeValue = "";
        this.invalidCoupon = false;
        this.couponMsg = "";
    }

    private resetApplyingCouponFlag() {
        this.applyingCoupon = false;
    }

    private setApplyingCouponFlag() {
        this.applyingCoupon = true;
    }

    private handleCouponCodeChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleCoupon();
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.cartCurrentState) {
            if (this.isValidatingCart(change)) {
                this.setApplyingCouponFlag();
            } else {
                this.resetApplyingCouponFlag();
            }
        }
    }

    private isValidatingCart(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.UPDATING_CART;
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
