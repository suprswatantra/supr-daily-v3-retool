import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

import { SharedModule } from "@shared/shared.module";

import { CartPageContainer } from "./containers/cart.container";
import { CartOffersContainer } from "./containers/offers.container";

import { cartPageComponents } from "./components";
import { cartPageContainers } from "./containers";
import { CartPageLayoutComponent } from "./layouts/cart.layout";
import { CartOffersLayout } from "./layouts/offers.layout";
import { cartPageServices } from "./services";

const routes: Routes = [
    {
        path: "",
        component: CartPageContainer,
    },
    {
        path: PAGE_ROUTES.CART.CHILDREN.OFFERS.PATH,
        component: CartOffersContainer,
        data: { screenName: SCREEN_NAMES.CART_OFFERS },
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...cartPageComponents,
        ...cartPageContainers,
        CartPageLayoutComponent,
        CartOffersLayout,
    ],
    providers: [...cartPageServices],
})
export class CartPageModule {}
