import { HeaderContainer } from "./header.container";
import { ItemContainer } from "./item.container";
import { CartPageContainer } from "./cart.container";
import { CouponContainer } from "./coupon.container";
import { SummaryContainer } from "./summary.container";
import { FooterContainer } from "./footer.container";
import { MessageContainer } from "./message.container";
import { CartOffersContainer } from "./offers.container";

export const cartPageContainers = [
    CartPageContainer,
    HeaderContainer,
    ItemContainer,
    CouponContainer,
    SummaryContainer,
    FooterContainer,
    MessageContainer,
    CartOffersContainer,
];
