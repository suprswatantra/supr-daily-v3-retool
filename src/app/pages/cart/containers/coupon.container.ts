import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";

@Component({
    selector: "supr-cart-coupon-container",
    template: `
        <supr-cart-coupon
            [couponCode]="couponCode$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [hasAddress]="hasAddress$ | async"
            (updateCoupon)="updateCoupon($event)"
        ></supr-cart-coupon>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponContainer implements OnInit {
    couponCode$: Observable<CouponCode>;
    cartCurrentState$: Observable<CartCurrentState>;
    hasAddress$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.couponCode$ = this.adapter.cartCouponCode$;
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
        this.hasAddress$ = this.adapter.hasAddress$;
    }

    updateCoupon(couponCode?: string) {
        this.adapter.updateCouponInCart(couponCode);
    }
}
