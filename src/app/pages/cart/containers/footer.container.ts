import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { CartMeta } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";
import { CartCurrentState } from "@store/cart/cart.state";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";

@Component({
    selector: "supr-cart-footer-container",
    template: `
        <supr-cart-page-footer
            [hasAddress]="hasAddress$ | async"
            [cartState]="cartState$ | async"
            [cartMeta]="cartMeta$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            [isEligibleForSuprPass]="isEligibleForSuprPass$ | async"
            [fromSuprPassPage]="fromSuprPassPage$ | async"
            (handlePay)="handlePay.emit()"
        ></supr-cart-page-footer>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterContainer implements OnInit {
    @Output() handlePay: EventEmitter<void> = new EventEmitter();

    hasAddress$: Observable<boolean>;
    cartState$: Observable<CartCurrentState>;
    cartMeta$: Observable<CartMeta>;
    userCheckoutInfo$: Observable<CheckoutInfo>;
    isEligibleForSuprPass$: Observable<boolean>;
    fromSuprPassPage$: Observable<boolean>;

    constructor(private adapter: Adapter, private userAdapter: UserAdapter) {}

    ngOnInit() {
        this.hasAddress$ = this.adapter.hasAddress$;
        this.cartState$ = this.adapter.cartCurrentState$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.isEligibleForSuprPass$ = this.adapter.isSuprPassEligible$;
        this.userCheckoutInfo$ = this.userAdapter.userCheckoutInfo$;
        this.fromSuprPassPage$ = this.adapter.urlParams$.pipe(
            map((params: Params) => !!params["fromSuprPassPage"])
        );
    }
}
