import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CollectionAdapter } from "@shared/adapters/collection.adapter";

import { CartItem, CartMeta, V3Layout, HomeLayoutPositions } from "@models";
import { CartCurrentState } from "@store/cart/cart.state";
import { SuprApi } from "@types";
import { HomeLayoutCurrentState } from "@store/home-layout/home-layout.state";

@Component({
    selector: "supr-cart-container",
    template: `
        <supr-cart-layout
            [cartItems]="cartItems$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [cartMeta]="cartMeta$ | async"
            [cartError]="cartError$ | async"
            [cartModified]="cartModified$ | async"
            [cartReValidate]="cartReValidate$ | async"
            [pastOrderSkuList]="pastOrderSkuList$ | async"
            [isAlternateDeliveryEligible]="isAlternateDeliveryEligible$ | async"
            [collectionList]="collectionList$ | async"
            [collectionState]="collectionState$ | async"
            [cartId]="cartId$ | async"
            (handleValidateCart)="validateCart($event)"
            (handleCheckoutCart)="checkoutCart()"
            (handleFetchHomeLayouts)="fetchHomeLayouts()"
        ></supr-cart-layout>
    `,
})
export class CartPageContainer implements OnInit {
    cartItems$: Observable<CartItem[]>;
    cartCurrentState$: Observable<CartCurrentState>;
    cartError$: Observable<any>;
    cartMeta$: Observable<CartMeta>;
    cartModified$: Observable<boolean>;
    cartReValidate$: Observable<boolean>;
    pastOrderSkuList$: Observable<Array<number>>;
    isAlternateDeliveryEligible$: Observable<boolean>;
    collectionList$: Observable<V3Layout[]>;
    collectionState$: Observable<HomeLayoutCurrentState>;
    cartId$: Observable<string>;

    constructor(
        private adapter: Adapter,
        private collectionCart: CollectionAdapter
    ) {}

    ngOnInit() {
        this.cartItems$ = this.adapter.cartItems$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
        this.cartModified$ = this.adapter.cartModified$;
        this.cartError$ = this.adapter.cartError$;
        this.cartReValidate$ = this.adapter.cartReValidate$;
        this.pastOrderSkuList$ = this.adapter.pastOrderSkuList$;
        this.isAlternateDeliveryEligible$ = this.adapter.isAlternateDeliveryEligible$;
        this.collectionList$ = this.collectionCart.fetchCollectionListForPosition(
            HomeLayoutPositions.CART
        );
        this.collectionState$ = this.collectionCart.collectionLayoutCurrentState$;
        this.cartId$ = this.adapter.cartId$;
    }

    checkoutCart() {
        this.adapter.checkoutCart();
    }

    validateCart(cartBody?: SuprApi.CartBody) {
        this.adapter.validateCart(cartBody);
    }

    fetchHomeLayouts() {
        this.collectionCart.fetchCollectionLayoutList();
    }
}
