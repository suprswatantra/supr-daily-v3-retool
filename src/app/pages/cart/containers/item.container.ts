import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { Observable } from "rxjs";

import { PauseAdapter } from "@shared/adapters/pause.adapter";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

import { Sku, CartItem, Vacation, SuprPassInfo, CartMeta } from "@models";

@Component({
    selector: "supr-cart-item-wrapper-container",
    template: `
        <supr-cart-item-wrapper
            [sku]="sku$ | async"
            [cartItem]="cartItem"
            [vacation]="vacation$ | async"
            [suprPassInfo]="suprPassInfo$ | async"
            [cartMeta]="cartMeta$ | async"
            (updateItemInCart)="updateItemInCart($event)"
        ></supr-cart-item-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemContainer implements OnInit {
    @Input() cartItem: CartItem;

    sku$: Observable<Sku>;
    vacation$: Observable<Vacation>;
    suprPassInfo$: Observable<SuprPassInfo>;
    cartMeta$: Observable<CartMeta>;
    addRewardSku$: Observable<Boolean>;

    addRewardSku = false;

    constructor(private adapter: Adapter, private pauseAdapter: PauseAdapter) {}

    ngOnInit() {
        this.vacation$ = this.pauseAdapter.vacation$;
        this.suprPassInfo$ = this.adapter.suprPassInfo$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.addRewardSku$ = this.adapter.addRewardSku$;
        this.sku$ = this.adapter.getSkuById(this.cartItem.sku_id);
        this.setAddRewardSkuData();
    }

    updateItemInCart(cartItem: CartItem) {
        if (this.cartItem.is_reward_sku && this.addRewardSku) {
            this.setAddRewardSkuState();
        }

        this.adapter.updateItemInCart(cartItem, true, this.addRewardSku);
    }

    private setAddRewardSkuData() {
        this.addRewardSku$.pipe().subscribe((state: boolean) => {
            this.addRewardSku = state;
        });
    }

    private setAddRewardSkuState(state = false) {
        this.adapter.updateAddRewardSku(state);
    }
}
