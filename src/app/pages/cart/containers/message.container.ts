import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

import { CartMessageInfo } from "@models";

@Component({
    selector: "supr-cart-message-container",
    template: `
        <supr-cart-message-content
            [cartMessageContent]="cartMessageContent$ | async"
        ></supr-cart-message-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageContainer implements OnInit {
    cartMessageContent$: Observable<CartMessageInfo>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.cartMessageContent$ = this.adapter.cartMessageContent$;
    }
}
