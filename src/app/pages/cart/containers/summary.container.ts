import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { CartMeta } from "@models";

import { CartCurrentState } from "@store/cart/cart.state";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

@Component({
    selector: "supr-cart-summary-container",
    template: `
        <supr-cart-summary
            [cartMeta]="cartMeta$ | async"
            [isEligibleForSuprPass]="isEligibleForSuprPass$ | async"
            [cartState]="cartState$ | async"
        ></supr-cart-summary>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryContainer implements OnInit {
    cartMeta$: Observable<CartMeta>;
    isEligibleForSuprPass$: Observable<boolean>;
    cartState$: Observable<CartCurrentState>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.cartMeta$ = this.adapter.cartMeta$;
        this.isEligibleForSuprPass$ = this.adapter.isSuprPassEligible$;
        this.cartState$ = this.adapter.cartCurrentState$;
    }
}
