import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CartMini } from "@types";

@Component({
    selector: "supr-cart-header-container",
    template: `
        <supr-cart-header [itemCount]="itemCount$ | async"></supr-cart-header>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderContainer implements OnInit {
    itemCount$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.itemCount$ = this.adapter.miniCart$.pipe(
            map((cart: CartMini) => cart.itemCount)
        );
    }
}
