import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { OffersList } from "@models";
import { CartAdapter } from "@shared/adapters/cart.adapter";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";

@Component({
    selector: "supr-cart-offers-container",
    template: `
        <supr-cart-offers-layout
            [couponCode]="couponCode$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [hasAddress]="hasAddress$ | async"
            [isFetchingOffers]="isFetchingOffers$ | async"
            [isApplyingCouponCode]="isApplyingCouponCode$ | async"
            [cartOffersList]="cartOffersList$ | async"
            (handleUpdateCoupon)="updateCoupon($event)"
        ></supr-cart-offers-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartOffersContainer implements OnInit {
    couponCode$: Observable<CouponCode>;
    cartCurrentState$: Observable<CartCurrentState>;
    hasAddress$: Observable<boolean>;
    isFetchingOffers$: Observable<boolean>;
    cartOffersList$: Observable<OffersList>;
    isApplyingCouponCode$: Observable<boolean>;

    constructor(private cartAdapter: CartAdapter) {}

    ngOnInit() {
        this.couponCode$ = this.cartAdapter.cartCouponCode$;
        this.cartCurrentState$ = this.cartAdapter.cartCurrentState$;
        this.hasAddress$ = this.cartAdapter.hasAddress$;
        this.isFetchingOffers$ = this.cartAdapter.isFetchingOffers$;
        this.cartOffersList$ = this.cartAdapter.cartOffersList$;
        this.isApplyingCouponCode$ = this.cartAdapter.isApplyingCouponCode$;
    }

    updateCoupon(couponCode?: string) {
        this.cartAdapter.updateCouponInCart(couponCode);
    }

    fetchCartOffers() {
        this.cartAdapter.fetchCartOffers();
    }
}
