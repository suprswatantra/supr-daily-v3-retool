import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    AddressStoreSelectors,
    AddressStoreActions,
    UserStoreSelectors,
    UserStoreActions,
} from "@supr/store";
import { SuprApi } from "@types";

@Injectable()
export class CitySelectionPageAdapter {
    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    userState$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCurrentState)
    );

    userError$ = this.store.pipe(select(UserStoreSelectors.selectUserError));

    address$ = this.store.pipe(select(AddressStoreSelectors.selectAddress));

    addressState$ = this.store.pipe(
        select(AddressStoreSelectors.selectAddressCurrentState)
    );

    addressError$ = this.store.pipe(
        select(AddressStoreSelectors.selectAddressError)
    );

    isAnonymousUser$ = this.store.pipe(
        select(UserStoreSelectors.isAnonymousUser)
    );

    constructor(private store: Store<StoreState>) {}

    createAddress(data: SuprApi.AddressBody) {
        this.store.dispatch(
            new AddressStoreActions.CreateAddressRequestAction({ data })
        );
    }

    fetchAnonymousToken(data: SuprApi.AnonymousTokenReqParams) {
        this.store.dispatch(
            new UserStoreActions.FetchAnonymousUserTokenRequest(data)
        );
    }
}
