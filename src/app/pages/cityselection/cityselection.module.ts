import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { citySelectionComponents } from "./components";
import { CitySelectionContainer } from "./containers/cityselection.container";
import { CitySelectionLayoutComponent } from "./layouts/cityselection.layout";
import { CitySelectionPageAdapter } from "./services/cityselection.adapter";

const routes: Routes = [
    {
        path: "",
        component: CitySelectionContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        CitySelectionLayoutComponent,
        CitySelectionContainer,
        ...citySelectionComponents,
    ],
    providers: [CitySelectionPageAdapter],
})
export class CitySelectionPageModule {}
