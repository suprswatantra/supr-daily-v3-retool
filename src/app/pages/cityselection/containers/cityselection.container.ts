import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Address, User } from "@models";
import { AddressCurrentState } from "@store/address/address.state";
import { UserCurrentState } from "@store/user/user.state";
import { SuprApi } from "@types";

import { CitySelectionPageAdapter as Adapter } from "@pages/cityselection/services/cityselection.adapter";

@Component({
    selector: "supr-city-selection-container",
    template: `
        <supr-city-selection-layout
            [user]="user$ | async"
            [userState]="userState$ | async"
            [userError]="userError$ | async"
            [address]="address$ | async"
            [addressState]="addressState$ | async"
            [addressError]="addressError$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            (handleUpdateAddress)="updateAddress($event)"
            (handleFetchAnonymousToken)="fetchAnonymousToken($event)"
        ></supr-city-selection-layout>
    `,
})
export class CitySelectionContainer implements OnInit {
    user$: Observable<User>;
    userState$: Observable<UserCurrentState>;
    userError$: Observable<any>;
    address$: Observable<Address>;
    addressState$: Observable<AddressCurrentState>;
    addressError$: Observable<any>;
    isAnonymousUser$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.user$ = this.adapter.user$;
        this.userState$ = this.adapter.userState$;
        this.userError$ = this.adapter.userError$;
        this.address$ = this.adapter.address$;
        this.addressState$ = this.adapter.addressState$;
        this.addressError$ = this.adapter.addressError$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
    }

    updateAddress(data: SuprApi.AddressBody) {
        this.adapter.createAddress(data);
    }

    fetchAnonymousToken(data: SuprApi.AnonymousTokenReqParams) {
        this.adapter.fetchAnonymousToken(data);
    }
}
