import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnChanges,
    SimpleChanges,
    ChangeDetectorRef,
    SimpleChange,
} from "@angular/core";

import { CITY_LIST, ANALYTICS_OBJECT_NAMES } from "@constants";
import { City, Address, User } from "@models";
import { AddressCurrentState } from "@store/address/address.state";
import { UserCurrentState } from "@store/user/user.state";
import { SuprApi } from "@types";
import { InitializeService } from "@services/util/initialize.service";
import { RouterService } from "@services/util/router.service";
@Component({
    selector: "supr-city-selection-layout",
    template: `
        <div class="suprContainer">
            <div class="suprScrollContent">
                <div class="header">
                    <supr-svg></supr-svg>
                </div>

                <div class="divider16"></div>
                <supr-city-selection-title></supr-city-selection-title>
                <div class="divider24"></div>

                <supr-city-selection-grid
                    [cityList]="cityList"
                    [selectedCity]="selectedCity"
                    (handleSelectCity)="updateCity($event)"
                ></supr-city-selection-grid>
                <div class="divider24"></div>

                <supr-city-selection-note></supr-city-selection-note>
            </div>
            <supr-page-footer>
                <supr-button
                    [disabled]="!selectedCity"
                    [loading]="updating"
                    (handleClick)="handleContinue()"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.CONTINUE}"
                >
                    <supr-text type="body">Continue</supr-text>
                </supr-button>
            </supr-page-footer>
        </div>
    `,
    styleUrls: ["../styles/cityselection.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CitySelectionLayoutComponent implements OnChanges {
    @Input() user: User;
    @Input() userState: UserCurrentState;
    @Input() userError: any;
    @Input() address: Address;
    @Input() addressState: AddressCurrentState;
    @Input() addressError: any;
    @Input() isAnonymousUser: boolean;
    @Output()
    handleUpdateAddress: EventEmitter<SuprApi.AddressBody> = new EventEmitter();
    @Output()
    handleFetchAnonymousToken: EventEmitter<
        SuprApi.AnonymousTokenReqParams
    > = new EventEmitter();

    cityList = CITY_LIST as City[];
    selectedCity: City;
    updating = false;

    constructor(
        private initializeService: InitializeService,
        private routerService: RouterService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleAddressStateChanges(changes["addressState"]);
        this.handleUserStateChange(changes["userState"]);
    }

    updateCity(city: City) {
        this.selectedCity = city;
        this.cdr.detectChanges();
    }

    handleContinue() {
        if (!this.selectedCity) {
            return;
        }
        if (this.isAnonymousUser) {
            this.getAnonymousToken();
            return;
        }
        this.updateAddress();
    }

    private getAnonymousToken() {
        this.handleFetchAnonymousToken.emit({
            city_id: this.selectedCity.id,
        });
    }

    private updateAddress() {
        if (!this.selectedCity) {
            return;
        }

        this.handleUpdateAddress.emit({
            type: "skip",
            cityId: this.selectedCity.id,
        });
    }

    private handleAddressStateChanges(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue === AddressCurrentState.CREATING_ADDRESS) {
            this.updating = true;
        } else if (change.currentValue === AddressCurrentState.NO_ACTION) {
            if (!this.addressError) {
                this.initializeService.handleUserFlow(this.user, this.address);
            }
        }
    }

    private handleUserStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue === UserCurrentState.FETCHING_ANONYMOUS_TOKEN) {
            this.updating = true;
        } else if (change.currentValue === AddressCurrentState.NO_ACTION) {
            this.updating = false;
            if (!this.userError) {
                this.routerService.goToHomePage();
            }
        }
    }
}
