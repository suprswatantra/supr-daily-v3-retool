export const CITY_GRID_COLUMNS = 3;

export const TEXTS = {
    TITLE: "Select your city",
    TITLE_CAPTION:
        "Milk, bread, eggs, and more delivered to your doorstep daily!",
    DID_NOT_FIND_CITY: "Didn’t find your city here?",
    TRYING_BEST: "We are trying our best to reach every city",
    LET_US_KNOW: "LET US KNOW",
};
