import { Component, ChangeDetectionStrategy } from "@angular/core";
import { TEXTS } from "@pages/cityselection/constants";

@Component({
    selector: "supr-city-selection-title",
    template: `
        <supr-text type="heading">{{ TEXTS.TITLE }}</supr-text>
        <supr-text class="subtitle" type="caption">
            {{ TEXTS.TITLE_CAPTION }}
        </supr-text>
    `,
    styleUrls: ["../../styles/cityselection.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TitleComponent {
    TEXTS = TEXTS;
}
