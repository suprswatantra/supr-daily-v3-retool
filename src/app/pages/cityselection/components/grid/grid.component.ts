import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { City } from "@models";
import { UtilService } from "@services/util/util.service";

import { CITY_GRID_COLUMNS } from "@pages/cityselection/constants";

@Component({
    selector: "supr-city-selection-grid",
    template: `
        <div class="grid">
            <ng-container
                *ngFor="let rowData of cityGridArr; trackBy: trackByFn"
            >
                <div class="suprRow">
                    <supr-city-selection-city
                        [city]="city"
                        [selectedCity]="selectedCity"
                        (handleSelectCity)="handleSelectCity.emit($event)"
                        *ngFor="let city of rowData; trackBy: trackByFn"
                    ></supr-city-selection-city>
                </div>
                <div class="divider16"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/cityselection.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridComponent {
    @Input()
    set cityList(list: City[]) {
        if (list) {
            this.prepareGridData(list);
        }
    }
    @Input() selectedCity: City;
    @Output() handleSelectCity: EventEmitter<City> = new EventEmitter();

    cityGridArr: City[][] = [];

    constructor(private utilService: UtilService) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    private prepareGridData(cityList: City[]) {
        this.cityGridArr = this.utilService.chunkArray(
            cityList,
            CITY_GRID_COLUMNS
        );
    }
}
