import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";
import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { City } from "@models";

@Component({
    selector: "supr-city-selection-city",
    template: `
        <div
            class="city suprColumn center"
            #cityWrapper
            (click)="selectCity()"
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.SELECT_CITY}"
            [saObjectValue]="city?.value"
        >
            <div class="cityIcon suprColumn" [class.selected]="isSelected">
                <supr-svg [ngClass]="city?.name"></supr-svg>
                <supr-icon name="approve_filled" *ngIf="isSelected"></supr-icon>
            </div>
            <div class="divider8"></div>
            <supr-text type="paragraph">{{ city?.value }}</supr-text>
        </div>
    `,
    styleUrls: ["../../styles/cityselection.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CityComponent implements OnInit, OnChanges {
    @Input() city: City;
    @Input() selectedCity: City;
    @Output() handleSelectCity: EventEmitter<City> = new EventEmitter();

    @ViewChild("cityWrapper", { static: true }) cityEl: ElementRef;

    isSelected = false;

    ngOnInit() {
        this.cityEl.nativeElement.style.setProperty(
            "--city-svg-url",
            `var(--supr-svg-city-${this.city.name}-url)`
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["selectedCity"];
        if (change && change.previousValue !== change.currentValue) {
            if (this.selectedCity) {
                this.isSelected = this.city.id === this.selectedCity.id;
            }
        }
    }

    selectCity() {
        if (!this.isSelected) {
            this.handleSelectCity.emit(this.city);
        }
    }
}
