import { Component, ChangeDetectionStrategy } from "@angular/core";
import { TEXTS } from "@pages/cityselection/constants";

@Component({
    selector: "supr-city-selection-note",
    template: `
        <div class="note">
            <supr-text type="caption">{{ TEXTS.DID_NOT_FIND_CITY }}</supr-text>
            <div class="suprRow spaceBetween">
                <supr-text type="caption">{{ TEXTS.TRYING_BEST }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/cityselection.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoteComponent {
    TEXTS = TEXTS;
}
