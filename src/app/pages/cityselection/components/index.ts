import { TitleComponent } from "./title/title.component";
import { GridComponent } from "./grid/grid.component";
import { CityComponent } from "./grid/city.component";
import { NoteComponent } from "./note/note.component";

export const citySelectionComponents = [
    TitleComponent,
    GridComponent,
    CityComponent,
    NoteComponent,
];
