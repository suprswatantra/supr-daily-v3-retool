import {
    Component,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
} from "@angular/core";
import { ModalComponent } from "@shared/components/supr-modal/supr-modal.component";
import { ModalService } from "app/services/layout/modal.service";

@Component({
    selector: "modal-content",
    template: `
        <supr-text type="title">Modal content</supr-text>
        <div class="divider24"></div>
        <div>
            <supr-text class="subtitle" type="subtitle">
                Sub Title text
            </supr-text>
            <supr-text class="heading" type="heading">
                Heading text
            </supr-text>
        </div>
        <div class="divider24"></div>
        <supr-button (handleClick)="suprModalRef.closeModal()">
            Close modal(ref)
        </supr-button>
        <supr-button (handleClick)="_closeModal()">
            Close modal(ser)
        </supr-button>
    `,
    styleUrls: ["content.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalContentComponent {
    @Input() suprModalRef: ModalComponent;
    @Output() closeModal: EventEmitter<TouchEvent> = new EventEmitter();

    constructor(private ms: ModalService) {}

    _closeModal() {
        this.ms.closeModal();
    }
}
