import { Component } from "@angular/core";

@Component({
    selector: "supr-icon-list",
    template: `
        <div class="section icons">
            <ion-title padding class="cardTitle">Icons</ion-title>
            <ion-grid>
                <ion-row class="ion-justify-content-center">
                    <ion-col class="ion-padding">
                        <div class="supr-icon-minus"></div>
                        <ion-label>Minus</ion-label>
                    </ion-col>
                    <ion-col class="ion-padding">
                        <div class="supr-icon-plus"></div>
                        <ion-label>Plus</ion-label>
                    </ion-col>
                    <ion-col class="ion-padding">
                        <supr-icon name="approve" [disabled]="true"></supr-icon>
                        <ion-label>Approve (disabled)</ion-label>
                    </ion-col>
                    <ion-col class="ion-padding">
                        <supr-icon
                            class="secondary"
                            name="arrow_down"
                        ></supr-icon>
                        <ion-label>Arrow Down</ion-label>
                    </ion-col>
                    <ion-col class="ion-padding">
                        <supr-icon name="arrow_up" class="primary"></supr-icon>
                        <ion-label>Arrow Up</ion-label>
                    </ion-col>
                </ion-row>
            </ion-grid>
        </div>
    `,
    styleUrls: ["../common.page.scss"],
})
export class IconListComponent {}
