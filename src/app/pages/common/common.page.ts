import { Component, OnInit, ViewChild } from "@angular/core";

import { Calendar } from "@types";

import { DateService } from "@services/date/date.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { RouterService } from "@services/util/router.service";

import classNames from "classnames";

import { ModalComponent } from "@shared/components/supr-modal/supr-modal.component";

@Component({
    selector: "supr-page-common",
    templateUrl: "./common.page.html",
    styleUrls: ["./common.page.scss"],
})
export class CommonPage implements OnInit {
    searchInputText: string;
    otpEntered: number;
    quantity = 0;
    quantity1 = 0;
    quantity2 = 0;
    quantity3 = 0;
    quantity4 = 5;
    initDate: Calendar.DayData;
    selectedDate: string;
    btnDisabled = false;
    title = "No Results Found";
    subtitle = "Britania Mix";
    radio1 = false;
    radio2 = true;
    isCartVisible = false;

    showModal = false;
    modalAnimate = true;

    productTile = {
        unit_price: 259.0,
        id: 1024,
        suprsku: false,
        unit_mrp: 259.0,
        item: {
            id: 1051,
            name: "Bournvita Women - Jar",
            step_quantity: 1.0,
            sub_unit_conversion: 1.0,
            unit_quantity: 1.0,
            sub_unit_name: "jar",
            unit_name: "jar",
            minimum_quantity: 1.0,
        },
        // tslint:disable: max-line-length
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Bournvita Women (400 g)",
        search_terms: [
            {
                term: "bournvita",
                score: "10",
            },
            {
                term: "women",
                score: "10",
            },
            {
                term: "400",
                score: "10",
            },
        ],
    };

    category = {
        id: 9,
        name: "Milk & Dairy Products",
        rank: 445,
        image: {
            url:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#64b053",
        },
    };

    selectedDays = {
        sun: true,
        fri: true,
    };

    selectedDays1 = {
        sun: true,
        fri: true,
    };

    showBackdrop = false;
    showBottomSheet = false;

    modalCloseMsg = "";

    @ViewChild(ModalComponent, { static: false }) modalComp: ModalComponent;

    constructor(
        private dateService: DateService,
        private suprDateService: SuprDateService,
        private routerService: RouterService
    ) {
        this.initDate = this.suprDateService.todaySuprDate();
        this.selectedDate = this.initDate.dateText;
    }

    handleWeekDaysSelect(day: string) {
        this.selectedDays1[day] = !this.selectedDays1[day];
    }

    openModal1(animate = true) {
        this.modalAnimate = animate;
        this.showModal = true;
    }

    _closeModal() {
        this.showModal = false;
    }

    modalContentAction() {
        // Do task
        this.modalComp.closeModal();
    }

    onOtpUpdate(otp: string) {
        this.otpEntered = +otp;
    }

    ngOnInit() {}

    goToPage(pageName: string) {
        switch (pageName) {
            case "home":
                this.routerService.goToHomePage();
                break;
            case "search":
                this.routerService.goToSearchPage();
                break;
            case "wallet":
                this.routerService.goToWalletPage();
                break;
            case "login":
                this.routerService.goToLoginPage();
                break;
            case "password":
                this.routerService.goToPasswordPage();
                break;
            case "category":
                this.routerService.goToCategoryPage(1);
                break;
            case "cart":
                this.routerService.goToCartPage();
                break;
            case "schedule":
                this.routerService.goToSchedulePage();
                break;
            case "address":
                this.routerService.goToAddressPage();
                break;
            case "subscription":
                this.routerService.goToSubscriptionListPage();
                break;
            case "cityselection":
                this.routerService.goToCitySelectionPage();
                break;
            case "walletCart":
                this.routerService.goToWalletPage({
                    queryParams: { amount: 200 },
                });
                break;
        }
    }

    onSearchInputText(searchInputText: string) {
        this.searchInputText = searchInputText;
    }

    onQuantityUpdate(direction: number) {
        if (direction > 0) {
            this.quantity++;
        } else if (direction < 0 && this.quantity > 0) {
            this.quantity--;
        }
    }

    onQuantityUpdate1(direction: number) {
        if (direction > 0) {
            this.quantity1++;
        } else if (direction < 0 && this.quantity1 > 0) {
            this.quantity1--;
        }
    }

    onQuantityUpdate2(direction: number) {
        if (direction > 0) {
            this.quantity2++;
        } else if (direction < 0 && this.quantity2 > 0) {
            this.quantity2--;
        }
    }

    onQuantityUpdate3(direction: number) {
        if (direction > 0) {
            this.quantity3++;
        } else if (direction < 0 && this.quantity3 > 0) {
            this.quantity3--;
        }
    }

    onQuantityUpdate4(direction: number) {
        if (direction > 0) {
            this.quantity4++;
        } else if (direction < 0 && this.quantity4 > 0) {
            this.quantity4--;
        }
    }

    openCal1() {
        const initDate = this.dateService.dateFromText("2019-07-25");
        this.initDate = this.suprDateService.suprDate(initDate);
    }

    openCal2() {
        const initDate = this.dateService.dateFromText("2019-08-10");
        this.initDate = this.suprDateService.suprDate(initDate);
    }

    updateSelectedDate(selectedDate: Calendar.DayData) {
        this.selectedDate = selectedDate.dateText;
    }

    toggleBackdrop() {
        this.showBackdrop = !this.showBackdrop;
    }

    getClassNames(): string {
        return classNames({
            backdrop: this.showBackdrop,
        });
    }

    toggleBottomSheet(status: boolean) {
        this.showBottomSheet = this.showBackdrop = status;
    }

    onBottomSheetClose() {
        this.showBottomSheet = this.showBackdrop = false;
    }

    openCart() {
        this.isCartVisible = true;
    }

    handleCart(isVisible: boolean) {
        this.isCartVisible = isVisible;
    }

    handleWeekDays(day: string) {
        if (this.selectedDays.hasOwnProperty(day)) {
            delete this.selectedDays[day];
        } else {
            this.selectedDays[day] = true;
        }
    }

    addItem() {
        // console.log("Add Item Clicked");
    }

    inputChangeLabel() {
        // console.log(text);
    }
}
