export interface AddressSearchFormattedResult {
    title?: string;
    subtitle?: string;
    id?: string | number;
}

export interface FlatSearchOption {
    id: number;
    name: string;
}

export interface FormInputData {
    key: string;
    value: string;
}
