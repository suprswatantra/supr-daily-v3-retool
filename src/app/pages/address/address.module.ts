import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";

import { IonicModule } from "@ionic/angular";

import { environment } from "@environments/environment";

import { SharedModule } from "@shared/shared.module";

import { AddressRoutingModule } from "./address-routing.module";

import { MapPageLayoutComponent } from "./layouts/map.layout";
import { FormPageLayoutComponent } from "./layouts/form.layout";
import { AddressPageLayoutComponent } from "./layouts/core.layout";
import { SocietySearchPageLayoutComponent } from "./layouts/society-search.layout";
import { LocationSearchPageLayoutComponent } from "./layouts/location-search.layout";
import { SocietyDetailsSearchPageLayoutComponent } from "./layouts/society-details-search-layout";

import { addressComponents } from "./components";
import { addressContainers } from "./containers";
import { addressServices } from "./services";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AddressRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: environment.mapKey.android,
        }),
        SharedModule,
    ],
    declarations: [
        AddressPageLayoutComponent,
        MapPageLayoutComponent,
        FormPageLayoutComponent,
        SocietySearchPageLayoutComponent,
        LocationSearchPageLayoutComponent,
        SocietyDetailsSearchPageLayoutComponent,
        ...addressComponents,
        ...addressContainers,
    ],
    providers: [...addressServices, GoogleMapsAPIWrapper],
})
export class AddressPageModule {}
