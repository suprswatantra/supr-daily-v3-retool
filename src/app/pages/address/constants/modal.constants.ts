export const CANT_FIND_MODAL_TEXTS = {
    TOWER: {
        LIST_ITEM_TEXT: "CAN'T MY FIND MY TOWER / BLOCK",
        MODAL_TITLE: "Can't find your tower / block?",
        MODAL_INPUT_LABEL: "Enter tower / block number",
        MODAL_INPUT_PLACEHOLDER: "eg. Tower L",
    },
    FLAT: {
        LIST_ITEM_TEXT: "CAN'T MY FIND MY FLAT NO.",
        MODAL_TITLE: "Can't find your flat number?",
        MODAL_INPUT_LABEL: "Enter flat number",
        MODAL_INPUT_PLACEHOLDER: "eg. 1604",
    },
    FLOOR: {
        LIST_ITEM_TEXT: "CAN'T MY FIND MY FLOOR NO.",
        MODAL_TITLE: "Can't find your floor number?",
        MODAL_INPUT_LABEL: "Enter floor number",
        MODAL_INPUT_PLACEHOLDER: "eg. 2nd",
    },
    SOCIETY: {
        INFO_TEXT: "Please enter full apartment / society name",
        NAME_EXAMPLE_TEXT: "eg. Princeton Apartment",
        MODAL_TITLE: "Please confirm your apartment / society name",
        MODAL_INPUT_LABEL: "Apartment / Society Name",
        MODAL_INPUT_PLACEHOLDER: "eg. Ashiana Tower",
    },
};
