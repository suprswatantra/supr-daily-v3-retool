export const SA_OBJECT_NAMES = {
    CLICK: {
        LOCATION_SEARCH_BOX: "location-search-box",
        RESIDENCE_TYPE: "residency-type",
        PLOT_NO: "plot-no",
        STREET_ADDRESS: "street-address",
        INSTRUCTIONS: "instructions",
        SAVE_BUTTON: "save-button",
        APARTMENT_NAME: "apartment-name",
        TOWER_NO: "tower-no",
        FLAT_NO: "flat-no",
        FLOOR_NO: "floor-no",
        MAP_HOME_ICON: "map-home-icon",
        EDIT_BUTTON: "edit-button",
        CONMFIRM_BUTTON: "confirm-button",
        SKIP_BUTTON: "skip-button",
        SAVE_ADDRESS: "save-address",
        SEARCH_RESULTS: "search_results",
        CURRENT_LOCATION_MAP: "current-location-map",
        SET_LOCATION_ON_MAP: "set-location-on-map",
    },
    IMPRESSION: {
        ADDRESS_NOT_SERVICEABLE: "address-not-serviceable",
    },
};

export const SA_OBJECT_VALUES = {
    ACTIVE: "active",
    INACTIVE: "inactive",
};

export const SA_OBJECT_CONTEXT = {
    CREATE_ADDRESS: "create",
    UPDATE_ADDRESS: "update",
    POPULAR: "popular",
    NEARBY: "nearby",
    SEARCH: "search",
};
