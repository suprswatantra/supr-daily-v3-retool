import { ADDRESS_TYPE } from "@constants";
import { SEARCH_INPUT_MAX_LENGTH } from "./search.constants";

export * from "./map.constants";
export * from "./modal.constants";
export * from "./search.constants";
export * from "./analytics.constants";

export const RESIDENCE_TYPE_OPTIONS = [
    "Apartment or Gated Society",
    "Individual House",
];

export const FORM_ERRORS = {
    [ADDRESS_TYPE.UNKNOWN_SOCIETY]: {
        societyName: {
            error: "Please select apartment",
        },
        buildingName: {
            error: "Please enter tower no.",
        },
        doorNumber: {
            error: "Please enter flat no.",
        },
        streetAddress: {
            error: "Please enter address",
        },
        instructions: {
            error: "Please enter landmark / instructions",
        },
    },
    [ADDRESS_TYPE.KNOWN_SOCIETY]: {
        societyName: {
            error: "Please select apartment",
        },
        buildingName: {
            error: "Please enter tower no.",
        },
        doorNumber: {
            error: "Please enter flat no.",
        },
    },
    [ADDRESS_TYPE.INDIVIDUAL_HOUSE]: {
        doorNumber: {
            error: "Please enter house no. / plot no",
        },
        floorNumber: {
            error: "Please enter floor number",
        },
        streetAddress: {
            error: "Please enter address",
        },
        instructions: {
            error: "Please enter landmark / instructions",
        },
    },
    APARTMENT_SOCIETY_EMPTY: "Please select apartment",
    TOWER_EMPTY: "Please enter tower no.",
    FLAT_EMPTY: "Please enter flat no.",
    ADDRESS_EMPTY: "Please enter address",
    HOUSE_EMPTY: "Please enter house no. / plot no",
    FLOOR_EMPTY: "Please enter floor number",
    FLOOR_FLAT_TOWER_20_CHAR: `Please enter less than ${SEARCH_INPUT_MAX_LENGTH} characters`,
    INSTRUCTIONS_EMPTY: "Please enter instructions to help us find your door",
};

export const APARTMENT_FORM_INPUT = {
    societyName: {
        name: "Apartment / Society*",
        type: "text",
        disabled: false,
        placeholder: "e.g. Princeton Apartment",
        key: "societyName",
        value: "",
        isLabelIcon: true,
        error: FORM_ERRORS.APARTMENT_SOCIETY_EMPTY,
    },
    buildingName: {
        name: "Tower / Block*",
        type: "text",
        disabled: false,
        placeholder: "e.g. Tower 1",
        key: "buildingName",
        value: "",
        searchList: [],
        isLabelIcon: true,
        error: FORM_ERRORS.TOWER_EMPTY,
        maxLength: SEARCH_INPUT_MAX_LENGTH,
    },
    doorNumber: {
        name: "Flat / House no.*",
        type: "text",
        disabled: false,
        placeholder: "e.g. C 8798",
        key: "doorNumber",
        value: "",
        searchList: [],
        isLabelIcon: true,
        error: FORM_ERRORS.FLAT_EMPTY,
        maxLength: SEARCH_INPUT_MAX_LENGTH,
    },
    streetAddress: {
        name: "Address*",
        type: "text",
        disabled: false,
        placeholder: "Street, Area, Colony, Sector",
        key: "streetAddress",
        value: "",
        searchList: [],
        isLabelIcon: false,
        error: FORM_ERRORS.ADDRESS_EMPTY,
    },
    instructions: {
        name: "Landmark / Instructions*",
        type: "text",
        disabled: false,
        placeholder: "e.g. White door in front of the lift",
        key: "instructions",
        value: "",
        searchList: [],
        isLabelIcon: false,
        error: FORM_ERRORS.INSTRUCTIONS_EMPTY,
    },
};

export const INDIVIDUAL_FORM_INPUT = {
    doorNumber: {
        name: "House no. / Plot no.*",
        type: "text",
        disabled: false,
        placeholder: "e.g. C 8798",
        key: "doorNumber",
        value: "",
        isLabelIcon: false,
        error: FORM_ERRORS.HOUSE_EMPTY,
        maxLength: SEARCH_INPUT_MAX_LENGTH,
    },
    floorNumber: {
        name: "Floor*",
        type: "text",
        disabled: false,
        placeholder: "e.g. 2",
        key: "floorNumber",
        value: "",
        searchList: [],
        isLabelIcon: true,
        error: FORM_ERRORS.FLOOR_EMPTY,
        maxLength: SEARCH_INPUT_MAX_LENGTH,
    },
    streetAddress: {
        name: "Address*",
        type: "text",
        disabled: false,
        placeholder: "Street, Area, Colony, Sector",
        key: "streetAddress",
        value: "",
        searchList: [],
        isLabelIcon: false,
        error: FORM_ERRORS.ADDRESS_EMPTY,
    },
    instructions: {
        name: "Landmark / Instructions*",
        type: "text",
        disabled: false,
        placeholder: "e.g. White door in front of the lift",
        key: "instructions",
        value: "",
        searchList: [],
        isLabelIcon: false,
        error: FORM_ERRORS.INSTRUCTIONS_EMPTY,
    },
};

export const MANDATORY_FORM_FIELDS = {
    individualHouse: {
        doorNumber: true,
        streetAddress: true,
        floorNumber: true,
        instructions: true,
    },
    knownSociety: {
        buildingName: true,
        doorNumber: true,
        societyName: true,
    },
    unknownSociety: {
        societyName: true,
        streetAddress: true,
        buildingName: true,
        doorNumber: true,
        instructions: true,
    },
};

export const MANDATORY_FORM_FIELDS_CHARACTERS_LIMIT = {
    doorNumber: true,
    floorNumber: true,
    buildingName: true,
};

export const APARTMENT_NAME_TEXTS = {
    INFO_TEXT: "Please enter full apartment / society name",
    NAME_EXAMPLE_TEXT: "eg. Princeton Apartment",
    MODAL_TITLE: "Please confirm your apartment / society name",
    MODAL_INPUT_LABEL: "Apartment / Society Name",
    MODAL_INPUT_PLACEHOLDER: "eg. Ashiana Tower",
};

export const BUTTON_TEXT = {
    SAVE: "Save",
    NEXT: "Next",
    CONFIRM: "Confirm",
    SAVE_ADDRESS: "Save address",
    EDIT: "Edit",
};

export const ADDRESS_DISPLAY_TEXT = {
    APARTMENT: {
        line1: "Door on the left after coming up the staircase",
        line2: "Brown door with nameplate",
        line3: "White door right in front of the lift",
    },
    INDIVIDUAL_HOUSE: {
        line1: "Black gate, yellow building",
        line2: "Brown corner house opposite Ratnadeep Supermarket",
        line3: "White building after dental clinic",
    },
    USER_INPUT_ADD_PREFIX: "Add ",
    USER_INPUT_ADD_SUFFIX: " to this address",
    FIND_DOORSTEP: "Help our Supr agent find your doorstep easily",
    FRONT_DOOR_DESC: "Describe your front door or gate",
    ADD_SOCIETY: "Add your society",
    NO_KNOWN_SOCIETY:
        "Seems like we don’t have your apartment / society listed on our platform yet.",
    RESIDENCE_TYPE_TEXT: "Residency type",
    DELIVERY_TEXT: "Where should we deliver your order?",
    LOCATE_ADDRESS: "Move pin to exact location",
    INFO_TEXT:
        "Note: We make deliveries before 7am. This description helps us leave the package at the right door",
    CANCEL_EXISTING_ORDER: "Cancel existing orders to proceed",
};

export const UNKNOWN_SOCIETY_ID = -1;
