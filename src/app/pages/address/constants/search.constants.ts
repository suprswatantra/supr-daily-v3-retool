export const LOCATION_SEARCH_NO_RESULTS = {
    CURRENT_LOCATION: {
        title: "Use current location",
        subtitle: "Using GPS",
        icon: "gps",
    },
    SET_LOCATION_ON_MAP: { icon: "location", title: "Set location on map" },
};

export const MIN_SEARCH_TERM_LENGTH = {
    LOCATION: 3,
    SOCIETY: 1,
};

export const SOCIETY_SEARCH_RESULTS_TITLE = {
    POPULAR: "POPULAR RESULTS",
    NEARBY: "NEAR BY APARTMENTS / SOCIETIES",
};

export const CUSTOM_INFO_TITLE = {
    FLAT: "ADD MY FLAT / HOUSE NO",
    TOWER: "ADD MY TOWER / BLOCK",
    FLOOR: "ADD MY FLOOR",
};

export const CUSTOM_SOCIETY_INFO_SUBTITLE =
    "We deliver everywhere in your locality";

export const ADDRESS_SEARCH_PLACEHOLDER = {
    TOWER: "Search for your Tower / Block",
    FLAT: "Search for your Flat / Door No.",
    APARTMENT: "Search Apartment / Society",
    FLOOR: "Type floor here",
};

export const FLOOR_NO = [
    {
        id: "Basement",
        title: "Basement",
    },
    {
        id: "Ground",
        title: "Ground",
    },
    {
        id: "1",
        title: "1",
    },
    {
        id: "2",
        title: "2",
    },
    {
        id: "3",
        title: "3",
    },
    {
        id: "4",
        title: "4",
    },
    {
        id: "5",
        title: "5",
    },
];

export const SEARCH_TRUNCATE_LENGTH = 10;

export const APARTMENT_SEARCH_INFO_TEXTS = {
    TO_SEE_MORE_RESULTS: "Search your apartment name to see more results",
    WE_DELIVER_EVERY: "We deliver everywhere in your city",
    SEARCH_TO_SEE_NEARBY: "Search to find more apartments / societies near you",
};

export const LOCATION_SEARCH_NOT_INFO_TEXT =
    "No results found. Try searching for your area or locality name";

export const SEARCH_INPUT_MAX_LENGTH = 40;
