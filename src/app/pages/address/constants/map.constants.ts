export const LOCATION_STATUS = {
    SERVICEABLE: {
        markerText: "Deliver here",
        searchTitle: "Move pin to set delivery location",
    },
    UNSERVICEABLE: {
        markerText: "Not covered",
        searchTitle: "Sorry we don’t cover this location",
    },
};

export const CURRENT_LOCATION = "currentLocation";

export const MAP_TEXTS = {
    SKIP: "Skip",
    CONFIRM_DELIVERY_LOCATION: "Confirm delivery location",
};

export const MAP = {
    zoom: 18,
    resizeTimeOut: 500,
    currentLocation: "currentLocation",
    default: {
        lat: 20.5937,
        lng: 78.9629,
    },
};
