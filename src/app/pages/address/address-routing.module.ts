import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

import { MapPageContainer } from "./containers/map.container";
import { SocietySearchPageContainer } from "./containers/society-search.container";
import { LocationSearchPageContainer } from "./containers/location-search.container";
import { SocietyDetailsSearchPageContainer } from "./containers/society-details-search-container";
import { FormPageContainer } from "./containers/form.container";

import { AddressPageLayoutComponent } from "./layouts/core.layout";

const addressRoutes: Routes = [
    {
        path: "",
        component: AddressPageLayoutComponent,
        children: [
            {
                path: "",
                redirectTo: PAGE_ROUTES.ADDRESS.CHILDREN.MAP.PATH,
                pathMatch: "full",
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.MAP.PATH,
                component: MapPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_MAP },
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.MAP_EDIT.PATH,
                component: MapPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_MAP_EDIT },
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.FORM.PATH,
                component: FormPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_FORM },
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.ADDRESS_SEARCH.PATH,
                component: LocationSearchPageContainer,
                data: { screenName: SCREEN_NAMES.ADDRESS_SEARCH },
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.SOCIETY_SEARCH.PATH,
                component: SocietySearchPageContainer,
                data: { screenName: SCREEN_NAMES.SOCIETY_SEARCH },
            },
            {
                path: PAGE_ROUTES.ADDRESS.CHILDREN.SOCIETY_DETAILS_SEARCH.PATH,
                component: SocietyDetailsSearchPageContainer,
                data: { screenName: SCREEN_NAMES.SOCIETY_DETAILS_SEARCH },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(addressRoutes)],
    exports: [RouterModule],
})
export class AddressRoutingModule {}
