import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Address } from "@models";
import {
    StoreState,
    RouterStoreSelectors,
    AddressStoreSelectors,
    AddressStoreActions,
} from "@supr/store";
import { SuprApi } from "@types";

@Injectable()
export class AddressPageAdapter {
    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    address$ = this.store.pipe(select(AddressStoreSelectors.selectAddress));

    premise$ = this.store.pipe(select(AddressStoreSelectors.selectPremise));

    popularPremise$ = this.store.pipe(
        select(AddressStoreSelectors.selectPopularPremiseAddress)
    );

    premiseAddress$ = this.store.pipe(
        select(AddressStoreSelectors.selectPremiseAddress)
    );

    buildings$ = this.store.pipe(select(AddressStoreSelectors.selectBuildings));

    addressState$ = this.store.pipe(
        select(AddressStoreSelectors.selectAddressCurrentState)
    );

    hasAddress$ = this.store.pipe(
        select(AddressStoreSelectors.selectHasAddress)
    );

    isFetchingPremiseList$ = this.store.pipe(
        select(AddressStoreSelectors.selectIsFetchingSocieties)
    );

    errorMsg$ = this.store.pipe(
        select(AddressStoreSelectors.selectAddressError)
    );

    constructor(private store: Store<StoreState>) {}

    getAddressData(): Observable<Address> {
        return this.address$;
    }

    updateAddress(data: any, meta?: SuprApi.Meta) {
        this.store.dispatch(
            new AddressStoreActions.CreateAddressRequestAction({ data, meta })
        );
    }

    searchPremise(data: SuprApi.PremiseSearchReq) {
        this.store.dispatch(
            new AddressStoreActions.SearchPremiseRequestAction({ data })
        );
    }

    searchPopularPremise(data: SuprApi.PremiseSearchReq) {
        this.store.dispatch(
            new AddressStoreActions.SearchPopularPremiseRequestAction({ data })
        );
    }

    searchPremiseAddress(id: number) {
        this.store.dispatch(
            new AddressStoreActions.SearchPremiseAddressRequestAction({
                data: id,
            })
        );
    }
}
