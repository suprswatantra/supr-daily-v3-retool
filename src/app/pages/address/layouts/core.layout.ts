import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-address-layout",
    template: `
        <ion-router-outlet
            animated="true"
            [swipeGesture]="false"
        ></ion-router-outlet>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressPageLayoutComponent {
    constructor() {}
}
