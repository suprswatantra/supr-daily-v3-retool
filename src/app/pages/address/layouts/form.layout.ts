import {
    Input,
    OnInit,
    Output,
    Component,
    OnDestroy,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import {
    PAGE_ROUTES,
    MODAL_NAMES,
    TOAST_MESSAGES,
    ADDRESS_TYPE,
} from "@constants";
import { MAP_VARIANTS } from "@shared/components/supr-map/constants/map.constants";

import { Address, Building } from "@shared/models";

import { AddressService } from "@services/shared/address.service";
import { RouterService } from "@services/util/router.service";
import { ErrorService } from "@services/integration/error.service";
import { ToastService } from "@services/layout/toast.service";
import { UtilService } from "@services/util/util.service";

import { AddressCurrentState } from "@store/address/address.state";

import { SuprMaps, GlobalError } from "@types";

import {
    SA_OBJECT_NAMES,
    BUTTON_TEXT,
    MANDATORY_FORM_FIELDS,
    ADDRESS_DISPLAY_TEXT,
} from "@pages/address/constants";

@Component({
    selector: "supr-address-form-layout",
    template: `
        <div class="suprContainer">
            <supr-address-form-header
                [addressText]="addressText"
                (handleClick)="goToMapEditPage()"
            ></supr-address-form-header>
            <div class="suprScrollContent">
                <supr-map
                    [location]="location"
                    variant="${MAP_VARIANTS.READ_ONLY}"
                    (handleStaticMarkerClick)="goToMapEditPage()"
                ></supr-map>

                <supr-address-form-wrapper
                    [formData]="formData"
                    [addressType]="addressType"
                    [mandatoryFormFields]="mandatoryFormFields"
                    [isOverlayApartment]="isOverlayApartment"
                ></supr-address-form-wrapper>
            </div>

            <supr-page-footer>
                <supr-button
                    (handleClick)="confirmAddress()"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.CONMFIRM_BUTTON}"
                >
                    <supr-text type="body">
                        {{ BUTTON_TEXT.CONFIRM }}
                    </supr-text>
                </supr-button>
            </supr-page-footer>

            <ng-container *ngIf="showReviewModal">
                <supr-modal
                    modalName="${MODAL_NAMES.ADDRESS_REVIEW}"
                    (handleClose)="closeReviewModal()"
                >
                    <div class="addressSummary">
                        <supr-address-summary
                            [address]="tempAddressData"
                        ></supr-address-summary>

                        <div class="divider24"></div>

                        <div class="suprRow">
                            <supr-button
                                class="edit"
                                (handleClick)="closeReviewModal()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .EDIT_BUTTON}"
                            >
                                <supr-text type="body">
                                    {{ BUTTON_TEXT.EDIT }}
                                </supr-text>
                            </supr-button>

                            <div class="spacer16"></div>

                            <supr-button
                                [loading]="isLoading"
                                (handleClick)="saveAddress()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .SAVE_ADDRESS}"
                            >
                                <supr-text type="body">
                                    {{ BUTTON_TEXT.SAVE_ADDRESS }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </supr-modal>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/form.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPageLayoutComponent implements OnInit, OnChanges, OnDestroy {
    @Input() errorMsg: any;
    @Input() buildings: Building[];
    @Input() addressState: AddressCurrentState;

    @Output() fetchPremiseDetails: EventEmitter<number> = new EventEmitter();
    @Output() handleUpdateAddress: EventEmitter<Address> = new EventEmitter();

    /* [TODO_RITESH] Add type */
    formData: any;
    addressType: string;
    addressText: string;
    mandatoryFormFields: {};
    location: SuprMaps.Location;
    isLocationServiceable = true;
    checkingServiceability = false;
    showReviewModal = false;
    isLoading = false;
    isOverlayApartment = false;
    tempAddressData: Address;

    BUTTON_TEXT = BUTTON_TEXT;

    private routerSub: Subscription;

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private addressService: AddressService,
        private routerService: RouterService,
        private errorService: ErrorService,
        private toastService: ToastService,
        private utilService: UtilService
    ) {
        this.subscribeToRouterEvents();
    }

    ngOnInit() {
        this.checkPremiseDetails();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleAddressStateChange(changes["addressState"]);
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    goToMapEditPage() {
        this.routerService.goToAddressMapEditPage();
    }

    closeReviewModal() {
        this.showReviewModal = false;
    }

    openReviewModal(address: Address) {
        this.showReviewModal = true;
        this.createAddressData(address);
    }

    saveAddress() {
        this.setLoader(true);
        const tempAddress = this.addressService.getModifiedAddress(
            this.buildings
        );

        this.handleUpdateAddress.emit(tempAddress);
    }

    showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.ADDRESS.SUCCESS);
    }

    confirmAddress() {
        if (this.isFormValid()) {
            const address = this.addressService.getModifiedAddress(
                this.buildings
            );
            if (!this.utilService.isEmpty(address)) {
                this.openReviewModal(address);
            }
        }

        this.cdr.detectChanges();
    }

    private initialize() {
        this.location = this.addressService.getAddressData("location");
        this.addressText = this.addressService.getAddressData(
            "formattedAddress.addressText"
        );
        this.addressType = this.addressService.getAddressData(
            "formAddressType"
        );

        const serviceDataKey = this.addressService.getFormDataTypeKey(
            this.addressType
        );

        this.formData = this.addressService.getAddressData(serviceDataKey);
    }

    private checkPremiseDetails() {
        const address: Address = this.addressService.getAddressData(
            "savedAddress"
        );

        if (address && address.premiseId) {
            this.fetchPremiseDetails.emit(address.premiseId);
        }
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) => this.isFormPageUrl(event.url))
            )
            .subscribe(() => {
                this.initialize();
                this.cdr.detectChanges();
            });
    }

    private isFormPageUrl(url: string): boolean {
        return url && url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.FORM.PATH) > -1;
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }

    private createAddressData(address: Address) {
        this.tempAddressData = address;
    }

    private handleAddressStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        this.setOverlayApartmentStatus(true);

        if (this.hasAddressChanged(change)) {
            this.closeReviewModal();
            this.setLoader(false);
            this.setOverlayApartmentStatus(false);

            if (this.errorMsg) {
                this.handleSaveAddressFailure();
            } else {
                this.handleSaveAddressSuccess();
            }
        } else if (
            this.hasPremiseListChanged(change) ||
            this.hasPopularPremiseListChanged(change)
        ) {
            this.setOverlayApartmentStatus(false);
        }
    }

    private hasAddressChanged(change: SimpleChange) {
        return (
            (change.previousValue === AddressCurrentState.CREATING_ADDRESS ||
                change.previousValue ===
                    AddressCurrentState.REFRESHING_ON_HUB_CHANGE) &&
            change.currentValue === AddressCurrentState.NO_ACTION
        );
    }

    private hasPremiseListChanged(change: SimpleChange) {
        return (
            (change.previousValue ===
                AddressCurrentState.SEARCHING_SOCIETY_DETAILS ||
                change.previousValue ===
                    AddressCurrentState.REFRESHING_ON_HUB_CHANGE) &&
            change.currentValue === AddressCurrentState.NO_ACTION
        );
    }

    private hasPopularPremiseListChanged(change: SimpleChange) {
        return (
            (change.previousValue ===
                AddressCurrentState.SEARCHING_POPULAR_SOCIETIES ||
                change.previousValue ===
                    AddressCurrentState.REFRESHING_ON_HUB_CHANGE) &&
            change.currentValue === AddressCurrentState.NO_ACTION
        );
    }

    private handleSaveAddressSuccess() {
        this.showSuccessToast();
        this.addressService.redirect();
    }

    private handleSaveAddressFailure() {
        const { statusCode, message } = this.errorMsg;
        const title = ADDRESS_DISPLAY_TEXT.CANCEL_EXISTING_ORDER;
        const errorObj: GlobalError = {
            statusCode,
            title,
            subTitle: message,
        };

        this.errorService.handleCustomError(errorObj);
    }

    private setLoader(status: boolean) {
        this.isLoading = status;
    }

    private setOverlayApartmentStatus(status: boolean) {
        this.isOverlayApartment = status;
    }

    private isFormValid(): boolean {
        let addressType = this.addressService.getAddressData("formAddressType");

        if (!addressType) {
            return false;
        }

        if (addressType === "skip") {
            addressType = ADDRESS_TYPE.KNOWN_SOCIETY;
        }

        const _mandatoryFormFields = {
            ...MANDATORY_FORM_FIELDS[addressType],
        };

        if (addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            const individualFormData = this.addressService.getAddressData(
                "individualHouseFormData"
            );

            this.mandatoryFormFields = this.addressService.validateForm(
                _mandatoryFormFields,
                individualFormData,
                addressType
            );
        } else {
            const apartmentFormData = this.addressService.getAddressData(
                "apartmentFormData"
            );

            this.mandatoryFormFields = this.addressService.validateForm(
                _mandatoryFormFields,
                apartmentFormData,
                addressType
            );
        }

        if (!this.utilService.isEmpty(this.mandatoryFormFields)) {
            return false;
        }

        return true;
    }
}
