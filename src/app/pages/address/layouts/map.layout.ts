import {
    Input,
    OnInit,
    OnDestroy,
    Component,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

import { Observable, Subscription, of } from "rxjs";
import { finalize, catchError, filter } from "rxjs/operators";

import {
    PAGE_ROUTES,
    LOCAL_DB_DATA_KEYS,
    ADDRESS_FROM_PARAM,
    LOCATION_AUTHORIZATION_STATUS,
} from "@constants";

import { Address } from "@shared/models";

import { DbService } from "@services/data/db.service";
import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";
import { PlatformService } from "@services/util/platform.service";
import { LocationService } from "@services/util/location.service";

import { SuprMaps, SuprApi } from "@types";

import {
    MAP_TEXTS,
    LOCATION_STATUS,
    SA_OBJECT_NAMES,
    CURRENT_LOCATION,
} from "../constants";

@Component({
    selector: "supr-address-map-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header
                *ngIf="!canSkip"
                [withBackground]="false"
            ></supr-page-header>
            <div
                *ngIf="canSkip"
                class="skip"
                (click)="onSkipButtonClick()"
                saObjectName="${SA_OBJECT_NAMES.CLICK.SKIP_BUTTON}"
            >
                <div class="suprRow center">
                    <supr-text type="action14">${MAP_TEXTS.SKIP}</supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
            <div class="suprScrollContent">
                <supr-map
                    [location]="location"
                    [markerText]="markerText"
                    [cityId]="address?.city?.id"
                    [unserviceable]="!isLocationServiceable"
                    [moveToCurrentLocation]="moveToCurrentLocation"
                    [checkingServiceability]="checkingServiceability"
                    (handleMapLocationChange)="onMapLocationChange($event)"
                ></supr-map>
                <supr-page-footer>
                    <supr-map-address-search
                        [title]="searchTitle"
                        [address]="formattedAddress"
                        [error]="!isLocationServiceable"
                        [updating]="checkingServiceability"
                        (handleSearchClick)="goToSearchPage()"
                    ></supr-map-address-search>
                    <supr-button
                        [disabled]="
                            !isLocationServiceable ||
                            checkingServiceability ||
                            !formattedAddress?.city?.id
                        "
                        [loading]="checkingServiceability"
                        (handleClick)="goToFormPage()"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.CONMFIRM_BUTTON}"
                    >
                        <supr-text type="body">
                            ${MAP_TEXTS.CONFIRM_DELIVERY_LOCATION}
                        </supr-text>
                    </supr-button>
                </supr-page-footer>
            </div>
        </div>
    `,
    styleUrls: ["../styles/map.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapPageLayoutComponent implements OnInit, OnDestroy {
    @Input() set from(value: string) {
        if (value) {
            this.setCanSkip(value);
            this.setResetFlag(value);
            this.setRedirectFlag(value);
        }
    }

    @Input() set address(data: Address) {
        this._address = data;

        if (this.resetAddressData || this.resetFormData) {
            this.resetServiceData();
        }
    }

    get address(): Address {
        return this._address;
    }

    @Input() set hasAddress(status: boolean) {
        this.setAddressContext(status);
    }

    canSkip = false;
    markerError = true;
    markerText: string;
    searchTitle: string;
    location: SuprMaps.Location;
    isLocationServiceable = true;
    moveToCurrentLocation = false;
    reverseGeocodedAddress: string;
    checkingServiceability = true;
    showLocationEnableModal = false;
    showLocationPermissionModal = false;
    formattedAddress: SuprMaps.MapSearchAddressInfo;

    private placeId: string;
    private _address: Address;
    private initialized = false;
    private resetFormData = false;
    private resetAddressData = false;
    private mapRouterSub: Subscription;
    private editRouterSub: Subscription;
    private locationUpdateSubscription: Subscription;
    private locationStatusCheckSubscription: Subscription;

    constructor(
        private router: Router,
        private dbService: DbService,
        private cdr: ChangeDetectorRef,
        private apiService: ApiService,
        private utilService: UtilService,
        private routerService: RouterService,
        private addressService: AddressService,
        private platformService: PlatformService,
        private locationService: LocationService
    ) {
        this.subscribeToRouterEvents();
    }

    ngOnInit() {
        this.initialized = true;
        this.init();
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    onMapLocationChange(locationInfo: SuprMaps.MapComponentLocationInfo) {
        /* Comparing new and existing location */
        if (
            this.locationService.compareLocations(
                locationInfo.location,
                this.location,
                5
            )
        ) {
            return;
        }
        if (
            !locationInfo.authorization &&
            this.locationService.isValidLocation(locationInfo.location)
        ) {
            this.moveToCurrentLocation = false;
            const geoCodeInput = {
                latlng: `${locationInfo.location.lat},${locationInfo.location.lng}`,
            };

            return this.fetchAddressTextFromLocation(
                geoCodeInput,
                locationInfo.location
            );
        }

        this.handleAuthorizationPrompts(locationInfo.authorization);
    }

    goToFormPage() {
        const formattedCity = this.utilService.getNestedValue(
            this.formattedAddress,
            "city"
        );

        if (
            this.locationService.isValidLocation(this.location) &&
            this.utilService.isEmpty(formattedCity)
        ) {
            return;
        }

        this.routerService.goToAddressFormPage();
    }

    goToSearchPage() {
        this.routerService.goToAddressSearchPage();
    }

    onSkipButtonClick() {
        this.resetServiceData();
        this.updateSkipImpression();
        this.routerService.goToHomePage({ replaceUrl: true });
    }

    private init() {
        this.setMapMarker();
    }

    private setCanSkip(from: string) {
        this.canSkip = from === "citySelection";
    }

    private setResetFlag(from: string) {
        this.resetFormData = from === ADDRESS_FROM_PARAM.NUDGE;

        this.resetAddressData =
            from === ADDRESS_FROM_PARAM.CITY_SELECTION ||
            from === ADDRESS_FROM_PARAM.CART;
    }

    private setRedirectFlag(from: string) {
        this.addressService.setRedirectFlag(from);
    }

    private resetServiceData() {
        /* If user comes from retro nudge flow, clean up form data and set address data from store */
        if (this.resetFormData) {
            this.addressService.setInitialAddressData(this.address);
            this.addressService.resetAddressData("apartmentFormData");
            this.addressService.resetAddressData("individualHouseFormData");
            /* If user comes from City Selection flow or cart flow, set address and form data from store */
        } else if (this.resetAddressData) {
            this.addressService.setInitialAddressData(this.address);
            this.addressService.setInitialFormData(this.address);
        }

        this.resetFormData = false;
        this.resetAddressData = false;
    }

    private updateSkipImpression() {
        this.dbService.setData(
            LOCAL_DB_DATA_KEYS.ADDRESS_SKIP_IMPRESSION_V1,
            true
        );
    }

    private setAddressContext(status: boolean) {
        if (status) {
            this.addressService.updateAddressContext();
        }
    }

    private setMapMarker() {
        const placeId = this.addressService.getAddressData("placeId");
        const location = this.addressService.getAddressData("location");
        const savedAddressLocation = this.addressService.getAddressData(
            "savedAddress.location.latLong"
        );
        const savedLocation = savedAddressLocation && {
            lat: savedAddressLocation.latitude,
            lng: savedAddressLocation.longitude,
        };

        if (placeId) {
            this.addressService.setAddressData("", "placeId");
            this.onSearchResultSelected(placeId);
        } else if (location) {
            this.onMapLocationChange({ location });
        } else if (this.locationService.isValidLocation(savedLocation)) {
            this.onMapLocationChange({
                location: savedLocation,
            });
        } else {
            this.moveToCurrentLocation = true;
        }
    }

    private onSearchResultSelected(placeId: string) {
        if (placeId === CURRENT_LOCATION) {
            this.moveToCurrentLocation = true;
            return;
        }

        if (placeId === this.placeId) {
            return;
        }

        this.placeId = placeId;

        this.fetchAddressTextFromLocation({ place_id: placeId });
    }

    private subscribeToRouterEvents() {
        const currentUrl = this.routerService.getCurrentUrl();

        if (this.isMapPageUrl(currentUrl)) {
            this.mapRouterSub = this.router.events
                .pipe(
                    filter(event => event instanceof NavigationEnd),
                    filter((event: NavigationEnd) =>
                        this.isMapPageUrl(event.url)
                    )
                )
                .subscribe(() => {
                    if (this.initialized) {
                        this.init();
                    }
                });
        }

        if (this.isEditPageUrl(currentUrl)) {
            this.editRouterSub = this.router.events
                .pipe(
                    filter(event => event instanceof NavigationEnd),
                    filter((event: NavigationEnd) =>
                        this.isEditPageUrl(event.url)
                    )
                )
                .subscribe(() => {
                    if (this.initialized) {
                        this.init();
                    }
                });
        }
    }

    private isMapPageUrl(url: string): boolean {
        return (
            url &&
            url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.MAP.PATH) > -1 &&
            url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.MAP_EDIT.PATH) < 0
        );
    }

    private isEditPageUrl(url: string): boolean {
        return (
            url && url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.MAP_EDIT.PATH) > -1
        );
    }

    private fetchAddressTextFromLocation(
        geoCodeInput: SuprMaps.GeocodeInput,
        location?: SuprMaps.Location
    ) {
        if (location) {
            this.updateLocation(location);
        }

        this.checkingServiceability = true;
        this.unsubscribeLocationUpdateSub();
        this.locationUpdateSubscription = this.reverseGeoCode(geoCodeInput)
            .pipe(
                finalize(() => {
                    this.unsubscribeLocationUpdateSub();
                })
            )
            .subscribe((geocodeResponse: SuprApi.GeoCodeRes) => {
                let updatedLocation = location;

                /* If user selected location manually, don't update it with reverse
                geocoded location, update only when user comes from search location */
                if (!location) {
                    updatedLocation = this.addressService.getLocationFromGeoCodeRes(
                        geocodeResponse
                    );
                    this.updateLocation(updatedLocation);
                }

                this.updateAddressTextAndLocation(geocodeResponse);

                this.addressService.setAddressData(
                    geocodeResponse.data.results,
                    "reverseGeocode"
                );

                /* Check for location status */
                this.locationStatusCheck(updatedLocation);
            });
    }

    private locationStatusCheck(location: SuprMaps.Location) {
        this.unsubscribeLocationStatusCheck();
        this.locationStatusCheckSubscription = this.apiService
            .getLocationStatus(location)
            .pipe(
                catchError(() => {
                    this.setServiceabilityInfo("unserviceable");
                    return of();
                }),

                finalize(() => {
                    setTimeout(() => {
                        this.checkingServiceability = false;
                        this.cdr.detectChanges();
                    }, 0);

                    this.unsubscribeLocationStatusCheck();
                })
            )
            .subscribe((response: SuprApi.LocationStatusRes) => {
                this.handleLocationStatusResponse(response);
                this.appendCityInfoInFormattedAddress(response);
            });
    }

    private appendCityInfoInFormattedAddress(
        locationCheckRes: SuprApi.LocationStatusRes
    ) {
        const city = this.addressService.getCityInfoFromLocationStatusRes(
            locationCheckRes
        );
        const state = this.addressService.getStateInformationFromCity(city);

        this.formattedAddress = {
            ...this.formattedAddress,
            city,
            state,
        };

        this.addressService.setAddressData(
            this.formattedAddress,
            "formattedAddress"
        );
    }

    private handleLocationStatusResponse(response: SuprApi.LocationStatusRes) {
        if (!response || response.statusCode !== 0) {
            this.setServiceabilityInfo("unserviceable");
            return;
        }
        const { unServiceableMessage } = response.data;

        if (unServiceableMessage) {
            this.setServiceabilityInfo("unserviceable");
        } else {
            this.setServiceabilityInfo();
        }

        this.cdr.detectChanges();
    }

    private setServiceabilityInfo(status?: string) {
        switch (status) {
            case "unserviceable":
                this.isLocationServiceable = false;
                this.markerText = LOCATION_STATUS.UNSERVICEABLE.markerText;
                this.searchTitle = LOCATION_STATUS.UNSERVICEABLE.searchTitle;
                this.markerError = true;
                break;

            default:
                setTimeout(() => {
                    this.isLocationServiceable = true;
                    this.cdr.detectChanges();
                }, 0);
                this.markerText = LOCATION_STATUS.SERVICEABLE.markerText;
                this.searchTitle = LOCATION_STATUS.SERVICEABLE.searchTitle;
                this.markerError = false;
                break;
        }
    }

    private reverseGeoCode(
        geoCodeInput: SuprMaps.GeocodeInput
    ): Observable<SuprApi.GeoCodeRes> {
        return this.apiService.reverseGeocode(geoCodeInput);
    }

    private handleAuthorizationPrompts(authorization: string) {
        /* [TODO_RITESH] Show corresponding modals based on status */
        const platform = this.platformService.getPlatform();

        if (!LOCATION_AUTHORIZATION_STATUS[platform]) {
            return;
        }

        switch (authorization) {
            case LOCATION_AUTHORIZATION_STATUS[platform].DISABLED:
                this.showLocationEnableModal = true;
                this.showLocationPermissionModal = false;
                break;
            case LOCATION_AUTHORIZATION_STATUS[platform].UNAUTHORIZED:
                this.showLocationEnableModal = false;
                this.showLocationPermissionModal = true;
                break;
            default:
                break;
        }
    }

    private updateAddressTextAndLocation(geocodeResponse: SuprApi.GeoCodeRes) {
        if (geocodeResponse) {
            this.formattedAddress = this.addressService.getFormattedAddressForMapFooter(
                geocodeResponse
            );
        }
    }

    private updateLocation(location: SuprMaps.Location) {
        this.location = location;
        this.addressService.setAddressData(location, "location");
        this.cdr.detectChanges();
    }

    private unsubscribeRouterEvents() {
        if (this.mapRouterSub && !this.mapRouterSub.closed) {
            this.mapRouterSub.unsubscribe();
        }

        if (this.editRouterSub && !this.editRouterSub.closed) {
            this.editRouterSub.unsubscribe();
        }
    }

    private unsubscribeLocationUpdateSub() {
        if (this.locationUpdateSubscription) {
            this.locationUpdateSubscription.unsubscribe();
            this.locationUpdateSubscription = null;
        }
    }

    private unsubscribeLocationStatusCheck() {
        if (this.locationStatusCheckSubscription) {
            this.locationStatusCheckSubscription.unsubscribe();
            this.locationStatusCheckSubscription = null;
        }
    }
}
