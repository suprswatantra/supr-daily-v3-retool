import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { MODAL_NAMES, ADDRESS_TYPE } from "@constants";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import { Building, Address, Door } from "@shared/models";

import { SuprApi } from "@types";

import { AddressSearchFormattedResult, FlatSearchOption } from "../type";

import {
    APARTMENT_FORM_INPUT,
    CANT_FIND_MODAL_TEXTS,
    ADDRESS_SEARCH_PLACEHOLDER,
    CUSTOM_SOCIETY_INFO_SUBTITLE,
    CUSTOM_INFO_TITLE,
    INDIVIDUAL_FORM_INPUT,
    SEARCH_INPUT_MAX_LENGTH,
} from "../constants";

@Component({
    selector: "supr-society-details-search-layout",
    template: `
        <div class="suprContainer">
            <supr-apartment-search
                [searchTerm]="searchTerm"
                [searchPlaceholder]="searchPlaceholder"
                (handleSearchTextChange)="onSearchTextChange($event)"
            ></supr-apartment-search>
            <div class="suprScrollContent">
                <supr-address-search-result-wrapper
                    [searchResults]="options"
                    (handleSearchResultClick)="onSearchResultClick($event)"
                ></supr-address-search-result-wrapper>

                <div class="divider16"></div>

                <supr-address-add-custom-info
                    leftIcon="add"
                    [title]="addCustomTitle"
                    [searchTerm]="searchTerm"
                    subTitle="${CUSTOM_SOCIETY_INFO_SUBTITLE}"
                    (click)="onAddCustomInfoClick()"
                >
                </supr-address-add-custom-info>
                <ng-container *ngIf="showCustomInfoModal">
                    <supr-modal
                        [modalName]="customModalName"
                        (handleClose)="closeCustomInfoModal()"
                    >
                        <supr-update-address-field
                            [initialValue]="searchTerm"
                            [title]="customModalData?.MODAL_TITLE"
                            [label]="customModalData?.MODAL_INPUT_LABEL"
                            [placeholder]="
                                customModalData?.MODAL_INPUT_PLACEHOLDER
                            "
                            [saContext]="saContext"
                            [maxLength]="${SEARCH_INPUT_MAX_LENGTH}"
                            (handleUpdateClick)="
                                onConfirmCustomInfoClick($event)
                            "
                        ></supr-update-address-field>
                    </supr-modal>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../components/search/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocietyDetailsSearchPageLayoutComponent {
    @Input() set type(type: string) {
        this._type = type;
        this.setConstants(type);
    }
    get type(): string {
        return this._type;
    }

    @Input() set floorOptions(options: AddressSearchFormattedResult[]) {
        this.setFloorOptions(options);
    }
    @Input() set premiseDetails(details: SuprApi.PremiseAddressRes) {
        this.setOptions(details);
    }

    @Output() handleSearchTextChange: EventEmitter<
        SuprApi.PremiseSearchReq
    > = new EventEmitter();

    flats: Door[];
    searchTerm = "";
    towers: Building[];
    /* [TODO_RITESH]: Add type */
    customModalData: any;
    addCustomTitle: string;
    customModalName: string;
    selectedTower: Building;
    searchPlaceholder: string;
    showCustomInfoModal = false;
    floors: AddressSearchFormattedResult[] = [];
    options: AddressSearchFormattedResult[] = [];

    private _type: string;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService,
        private addressService: AddressService
    ) {}

    onSearchTextChange(searchTerm: string) {
        this.searchTerm = searchTerm.trimLeft();

        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            this.filterTowersList(this.searchTerm);
        } else if (this.type === APARTMENT_FORM_INPUT.doorNumber.key) {
            this.filterFlatList(this.searchTerm);
        } else if (this.type === INDIVIDUAL_FORM_INPUT.floorNumber.key) {
            this.filterFloorList(this.searchTerm);
        }
    }

    onSearchResultClick(detailId: string | number) {
        if (this.type === INDIVIDUAL_FORM_INPUT.floorNumber.key) {
            this.updateIndividualForm(detailId);
            return;
        }

        this.updateApartmentForm(detailId);
    }

    onAddCustomInfoClick() {
        if (this.searchTerm && this.searchTerm.length) {
            return this.onConfirmCustomInfoClick({
                id: -1,
                name: this.searchTerm,
            });
        }

        this.openCustomInfoModal();
    }

    onConfirmCustomInfoClick(data: FlatSearchOption) {
        if (this.type === INDIVIDUAL_FORM_INPUT.floorNumber.key) {
            this.updateIndividualForm(data.name);
            return;
        }

        this.onCustomTowerOrFlatClick(data);
    }

    openCustomInfoModal() {
        this.showCustomInfoModal = true;
    }

    closeCustomInfoModal() {
        this.showCustomInfoModal = false;
    }

    private setOptions(premiseDetails: SuprApi.PremiseAddressRes) {
        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            this.setTowerOptions(premiseDetails);
        } else if (this.type === APARTMENT_FORM_INPUT.doorNumber.key) {
            this.setFlatOptions(premiseDetails);
        }
    }

    private setTowerOptions(premiseDetails: SuprApi.PremiseAddressRes) {
        this.towers = this.addressService.getBuildingsFromPremiseDetails(
            premiseDetails
        );
        this.options = this.addressService.formatBuildingsSearchResult(
            this.towers
        );
    }

    private setFlatOptions(premiseDetails: SuprApi.PremiseAddressRes) {
        const towerId = this.addressService.getAddressData("formTowerId");
        const address: Address = this.addressService.getAddressData(
            "savedAddress"
        );
        const addressTowerName = address && address.buildingName;

        if (towerId || addressTowerName) {
            this.flats = this.addressService.getFlatsFromPremiseDetails(
                premiseDetails,
                { id: towerId, name: addressTowerName }
            );

            this.options = this.addressService.formatFlatsSearchResult(
                this.flats
            );
        }
    }

    private setFloorOptions(options: AddressSearchFormattedResult[]) {
        this.floors = options;
        this.options = options;
    }

    private setConstants(type: string) {
        if (type === APARTMENT_FORM_INPUT.buildingName.key) {
            this.addCustomTitle = CUSTOM_INFO_TITLE.TOWER;
            this.customModalName = MODAL_NAMES.UPDATE_BLOCK;
            this.customModalData = CANT_FIND_MODAL_TEXTS.TOWER;
            this.searchPlaceholder = ADDRESS_SEARCH_PLACEHOLDER.TOWER;
        } else if (type === APARTMENT_FORM_INPUT.doorNumber.key) {
            this.addCustomTitle = CUSTOM_INFO_TITLE.FLAT;
            this.customModalName = MODAL_NAMES.UPDATE_FLAT_NO;
            this.customModalData = CANT_FIND_MODAL_TEXTS.FLAT;
            this.searchPlaceholder = ADDRESS_SEARCH_PLACEHOLDER.FLAT;
        } else if (type === INDIVIDUAL_FORM_INPUT.floorNumber.key) {
            this.addCustomTitle = CUSTOM_INFO_TITLE.FLOOR;
            this.customModalName = MODAL_NAMES.UPDATE_FLOOR_NO;
            this.customModalData = CANT_FIND_MODAL_TEXTS.FLOOR;
            this.searchPlaceholder = ADDRESS_SEARCH_PLACEHOLDER.FLOOR;
        }
    }

    private updateIndividualForm(detailId: string | number) {
        const individualFormData = this.getUpdatedIndividualData(detailId);

        this.addressService.setAddressData(
            individualFormData,
            "individualHouseFormData"
        );
        this.routerService.goBack();
    }

    private updateApartmentForm(detailId: string | number) {
        const apartmentData = this.getUpdatedSocietyData(detailId);

        this.addressService.setAddressData(
            ADDRESS_TYPE.KNOWN_SOCIETY,
            "formAddressType"
        );
        this.addressService.setAddressData(apartmentData, "apartmentFormData");
        this.addressService.setAddressData(
            ADDRESS_TYPE.KNOWN_SOCIETY,
            "apartmentType"
        );

        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            this.addressService.setAddressData(detailId, "formTowerId");
        }

        this.routerService.goBack();
    }

    private onCustomTowerOrFlatClick(data: FlatSearchOption) {
        const apartmentData = this.getUpdatedCustomSocietyData(data);

        this.addressService.setAddressData(
            ADDRESS_TYPE.UNKNOWN_SOCIETY,
            "formAddressType"
        );
        this.addressService.setAddressData(apartmentData, "apartmentFormData");
        this.addressService.setAddressData(
            ADDRESS_TYPE.UNKNOWN_SOCIETY,
            "apartmentType"
        );
        this.routerService.goBack();
    }

    private getUpdatedSocietyData(detailId: string | number): Address {
        const apartmentFormData = this.addressService.getAddressData(
            "apartmentFormData"
        );

        const detailName = this.getFlatOrTowerNameFromId(detailId);

        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            return {
                ...apartmentFormData,
                /*  [TODO_RITESH] Read keys like buildingId and doorId from some global constants */
                buildingId: detailId,
                [APARTMENT_FORM_INPUT.doorNumber.key]: "",
                [APARTMENT_FORM_INPUT.buildingName.key]: detailName,
            };
        }

        return {
            ...apartmentFormData,
            /*  [TODO_RITESH] Read keys like buildingId and doorId from some global constants */
            doorId: detailId,
            [APARTMENT_FORM_INPUT.doorNumber.key]: detailName,
        };
    }

    private getUpdatedIndividualData(detailId: string | number): Address {
        const individualFormData = this.addressService.getAddressData(
            "individualHouseFormData"
        );

        return {
            ...individualFormData,
            [INDIVIDUAL_FORM_INPUT.floorNumber.key]: detailId,
        };
    }

    private getUpdatedCustomSocietyData(data: FlatSearchOption) {
        const apartmentFormData = this.addressService.getAddressData(
            "apartmentFormData"
        );

        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            return {
                ...apartmentFormData,
                [APARTMENT_FORM_INPUT.doorNumber.key]: "",
                [APARTMENT_FORM_INPUT.buildingName.key]: data.name,
            };
        }

        return {
            ...apartmentFormData,
            [APARTMENT_FORM_INPUT.doorNumber.key]: data.name,
        };
    }

    private getFlatOrTowerNameFromId(detailId: string | number): string {
        if (this.type === APARTMENT_FORM_INPUT.buildingName.key) {
            const tower = this.towers.find(
                (towerData: Building) => towerData.id === detailId
            );
            return tower.name || "";
        }

        if (this.utilService.isLengthyArray(this.flats)) {
            const flat = this.flats.find(
                (flatData: Door) => flatData.id === detailId
            );
            return flat.name || "";
        }
        return "";
    }

    private filterTowersList(searchTerm: string) {
        const filteredTowers = this.towers.filter((tower: Building) => {
            const name = this.utilService
                .getNestedValue(tower, "name", "")
                .toLowerCase();
            return name.includes(searchTerm.toLowerCase());
        });

        this.options = this.addressService.formatBuildingsSearchResult(
            filteredTowers
        );
    }

    private filterFlatList(searchTerm: string) {
        const filteredFlats = this.flats.filter((flat: Door) => {
            const name = this.utilService
                .getNestedValue(flat, "name", "")
                .toLowerCase();
            return name.includes(searchTerm.toLowerCase());
        });

        this.options = this.addressService.formatFlatsSearchResult(
            filteredFlats
        );
    }

    private filterFloorList(searchTerm: string) {
        const filteredFloors = this.floors.filter(
            (floor: AddressSearchFormattedResult) => {
                const title = this.utilService
                    .getNestedValue(floor, "title", "")
                    .toLowerCase();
                return title.includes(searchTerm.toLowerCase());
            }
        );

        this.options = filteredFloors;
    }
}
