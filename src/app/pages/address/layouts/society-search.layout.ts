import { Router, NavigationEnd } from "@angular/router";
import {
    Input,
    Output,
    Component,
    OnDestroy,
    EventEmitter,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { PAGE_ROUTES, ADDRESS_TYPE } from "@constants";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import { SuprApi } from "@types";

import {
    UNKNOWN_SOCIETY_ID,
    MIN_SEARCH_TERM_LENGTH,
    ADDRESS_SEARCH_PLACEHOLDER,
    CUSTOM_SOCIETY_INFO_SUBTITLE,
    SOCIETY_SEARCH_RESULTS_TITLE,
    SA_OBJECT_CONTEXT,
    APARTMENT_SEARCH_INFO_TEXTS,
} from "../constants";

@Component({
    selector: "supr-society-search-layout",
    template: `
        <div class="suprContainer">
            <supr-apartment-search
                showSearchInfo="true"
                [searchTerm]="searchTerm"
                searchPlaceholder="${ADDRESS_SEARCH_PLACEHOLDER.APARTMENT}"
                (handleSearchTextChange)="onSearchTextChange($event)"
            ></supr-apartment-search>
            <div class="suprScrollContent">
                <ng-container *ngIf="isFetchingPremiseList; else resultWrapper">
                    <supr-loader class="society"></supr-loader>
                </ng-container>

                <ng-template #resultWrapper>
                    <supr-address-search-result-wrapper
                        [title]="searchTitle"
                        [searchResults]="searchResults"
                        iconName="apartment"
                        (handleSearchResultClick)="onSearchResultClick($event)"
                        [isSocietySearch]="true"
                        [saContext]="getContext()"
                    ></supr-address-search-result-wrapper>

                    <div
                        *ngIf="popularPremiseResults?.length && !searchTerm"
                        class="divider12"
                    ></div>

                    <supr-apartment-search-info
                        *ngIf="!searchTerm && searchResults.length"
                        searchInfotextLine1="${APARTMENT_SEARCH_INFO_TEXTS.SEARCH_TO_SEE_NEARBY}"
                        searchInfotextLine2="${APARTMENT_SEARCH_INFO_TEXTS.WE_DELIVER_EVERY}"
                    ></supr-apartment-search-info>

                    <div
                        *ngIf="popularPremiseResults?.length && !searchTerm"
                        class="divider36"
                    ></div>

                    <supr-address-search-result-wrapper
                        *ngIf="!searchTerm"
                        [title]="popularSearchTitle"
                        iconName="apartment"
                        [searchResults]="popularPremiseResults"
                        (handleSearchResultClick)="
                            onSearchResultClick($event, 'popular')
                        "
                        [isSocietySearch]="searchResults.length ? false : true"
                        saContext="${SA_OBJECT_CONTEXT.POPULAR}"
                    ></supr-address-search-result-wrapper>

                    <div *ngIf="!searchTerm" class="divider12"></div>

                    <supr-apartment-search-info
                        *ngIf="!searchTerm && searchResults.length"
                        searchInfotextLine1="${APARTMENT_SEARCH_INFO_TEXTS.TO_SEE_MORE_RESULTS}"
                        searchInfotextLine2="${APARTMENT_SEARCH_INFO_TEXTS.WE_DELIVER_EVERY}"
                    ></supr-apartment-search-info>

                    <div *ngIf="searchResults?.length" class="divider16"></div>

                    <supr-address-add-custom-info
                        *ngIf="searchTerm"
                        leftIcon="add"
                        [searchTerm]="searchTerm"
                        subTitle="${CUSTOM_SOCIETY_INFO_SUBTITLE}"
                        (click)="onAddNewSocietyClick()"
                    >
                    </supr-address-add-custom-info>

                    <div class="divider8"></div>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../components/search/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocietySearchPageLayoutComponent implements OnDestroy {
    @Input() showSearchError: boolean;
    @Input() isFetchingPremiseList: boolean;

    @Input() set searchResults(results: SuprApi.PremiseSearchRes[]) {
        this.originalSearchResults = results;
        this._searchResults = this.addressService.formatSocietySearchResults(
            results
        );
    }

    get searchResults() {
        return this._searchResults;
    }

    @Input() set popularPremiseResults(results: SuprApi.PremiseSearchRes[]) {
        this.popularSearchResults = results;
        this._popularSearchResults = this.addressService.formatSocietySearchResults(
            results
        );
    }

    get popularPremiseResults() {
        return this._popularSearchResults;
    }

    @Output() handleSocietyChange: EventEmitter<number> = new EventEmitter();
    @Output() handleSearchTextChange: EventEmitter<
        SuprApi.PremiseSearchReq
    > = new EventEmitter();
    @Output() handlePopularSearchPremise: EventEmitter<
        SuprApi.PremiseSearchReq
    > = new EventEmitter();

    searchTerm = "";
    searchTitle = SOCIETY_SEARCH_RESULTS_TITLE.NEARBY;
    popularSearchTitle = SOCIETY_SEARCH_RESULTS_TITLE.POPULAR;

    private routerSub: Subscription;
    /* [TODO_RITESH]: Add type */
    private _searchResults: Array<any>;
    private _popularSearchResults: Array<any>;
    private originalSearchResults: SuprApi.PremiseSearchRes[];
    private popularSearchResults: SuprApi.PremiseSearchRes[];

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private routerService: RouterService,
        private addressService: AddressService
    ) {
        this.subscribeToRouterEvents();
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    onSearchTextChange(searchTerm: string) {
        this.searchTerm = searchTerm.trimLeft();
        this.updateSearchTermInService();

        if (this.searchTerm.length > MIN_SEARCH_TERM_LENGTH.SOCIETY) {
            this.searchTitle = "";
            return this.emitSearch(searchTerm, true);
        }

        this.searchTitle = SOCIETY_SEARCH_RESULTS_TITLE.NEARBY;

        /* [TODO_RITESH] Save nearby searches in service unless location changes */
        this.emitSearch("", true);
        this.emitPopularSearch();
    }

    onSearchResultClick(societyId: number, from = "") {
        const societyData = this.getSocietyDataById(societyId, from);

        this.addressService.setAddressData(societyId, "societyId");
        this.addressService.resetAddressData("apartmentFormData");
        this.addressService.setAddressData(societyData, "apartmentFormData");
        this.addressService.setAddressData(
            ADDRESS_TYPE.KNOWN_SOCIETY,
            "formAddressType"
        );
        this.addressService.setAddressData(
            ADDRESS_TYPE.KNOWN_SOCIETY,
            "apartmentType"
        );
        this.handleSocietyChange.emit(societyId);

        setTimeout(() => this.routerService.goBack(), 10);
    }

    onAddNewSocietyClick() {
        const societyData = {
            premiseId: UNKNOWN_SOCIETY_ID,
            societyName: this.utilService.toTitleCase(this.searchTerm),
        };

        this.addressService.resetAddressData("apartmentFormData");
        this.addressService.setAddressData(societyData, "apartmentFormData");
        this.addressService.setAddressData(
            ADDRESS_TYPE.UNKNOWN_SOCIETY,
            "formAddressType"
        );
        this.addressService.setAddressData(
            ADDRESS_TYPE.UNKNOWN_SOCIETY,
            "apartmentType"
        );
        this.routerService.goBack();
    }

    getContext() {
        if (this.searchTerm) {
            return SA_OBJECT_CONTEXT.SEARCH;
        }

        return SA_OBJECT_CONTEXT.NEARBY;
    }

    private initialize() {
        this.searchTerm = this.addressService.getAddressData(
            "societySearchTerm"
        );

        this.onSearchTextChange(this.searchTerm);

        this.cdr.detectChanges();
    }

    private emitSearch(searchTerm = "", includeLocation = false) {
        const formattedAddress = this.addressService.getAddressData(
            "formattedAddress"
        );

        const cityId = this.utilService.getNestedValue(
            formattedAddress,
            "city.id"
        );

        const searchData: any = { cityId, searchTerm };

        if (includeLocation) {
            const location = this.addressService.getAddressData("location");
            searchData.latitude = location.lat;
            searchData.longitude = location.lng;
        }

        this.handleSearchTextChange.emit(searchData);
    }

    private emitPopularSearch() {
        const formattedAddress = this.addressService.getAddressData(
            "formattedAddress"
        );

        const cityId = this.utilService.getNestedValue(
            formattedAddress,
            "city.id"
        );

        const searchData: any = { cityId };

        if (cityId) {
            this.handlePopularSearchPremise.emit(searchData);
        }
    }

    private updateSearchTermInService() {
        this.addressService.setAddressData(
            this.searchTerm,
            "societySearchTerm"
        );
    }

    /* [TODO_RITESH] Add type */
    private getSocietyDataById(societyId: number, from: string): any {
        let society: SuprApi.PremiseSearchRes;

        if (from === "popular") {
            society = this.popularSearchResults.find(
                (result: SuprApi.PremiseSearchRes) => result.id === societyId
            );
        } else {
            society = this.originalSearchResults.find(
                (result: SuprApi.PremiseSearchRes) => result.id === societyId
            );
        }

        return {
            premiseId: society.id,
            societyName: society.name,
            streetAddress: society.streetAddress,
        };
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isSocietySearchPageUrl(event.url)
                )
            )
            .subscribe(() => {
                this.initialize();
            });
    }

    private isSocietySearchPageUrl(url: string): boolean {
        return (
            url &&
            url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.SOCIETY_SEARCH.PATH) > -1
        );
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }
}
