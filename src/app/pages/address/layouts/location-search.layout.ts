import { Router, NavigationEnd } from "@angular/router";
import {
    Input,
    Output,
    Component,
    OnDestroy,
    EventEmitter,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";

import { PAGE_ROUTES } from "@constants";

import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import {
    CURRENT_LOCATION,
    MIN_SEARCH_TERM_LENGTH,
    LOCATION_SEARCH_NO_RESULTS,
    SA_OBJECT_NAMES,
} from "../constants";

@Component({
    selector: "supr-address-search-layout",
    template: `
        <div class="suprContainer">
            <supr-map-search
                [searchTerm]="searchTerm"
                (handleSearchTextChange)="onSearchTextChange($event)"
            ></supr-map-search>
            <div class="suprScrollContent">
                <ng-container *ngIf="isFetchingResults; else resultWrapper">
                    <supr-loader></supr-loader>
                </ng-container>

                <ng-template #resultWrapper>
                    <supr-location-search-not-found-info
                        *ngIf="
                            searchTerm &&
                            searchTerm.length >
                                ${MIN_SEARCH_TERM_LENGTH.LOCATION} &&
                            (!searchResults || !searchResults.length)
                        "
                    ></supr-location-search-not-found-info>

                    <supr-address-search-custom-result
                        *ngIf="
                            !searchTerm ||
                            !searchResults ||
                            !searchResults.length
                        "
                        icon="${LOCATION_SEARCH_NO_RESULTS.CURRENT_LOCATION
                            .icon}"
                        title="${LOCATION_SEARCH_NO_RESULTS.CURRENT_LOCATION
                            .title}"
                        subtitle="${LOCATION_SEARCH_NO_RESULTS.CURRENT_LOCATION
                            .subtitle}"
                        (click)="onSearchResultClick('${CURRENT_LOCATION}')"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .CURRENT_LOCATION_MAP}"
                    ></supr-address-search-custom-result>

                    <supr-address-search-custom-result
                        *ngIf="
                            searchTerm &&
                            searchTerm.length >
                                ${MIN_SEARCH_TERM_LENGTH.LOCATION} &&
                            (!searchResults || !searchResults.length)
                        "
                        icon="${LOCATION_SEARCH_NO_RESULTS.SET_LOCATION_ON_MAP
                            .icon}"
                        title="${LOCATION_SEARCH_NO_RESULTS.SET_LOCATION_ON_MAP
                            .title}"
                        (click)="goBack()"
                        saClick
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .SET_LOCATION_ON_MAP}"
                    ></supr-address-search-custom-result>

                    <supr-address-search-result-wrapper
                        [searchResults]="searchResults"
                        iconName="location"
                        (handleSearchResultClick)="onSearchResultClick($event)"
                    ></supr-address-search-result-wrapper>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../components/search/search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationSearchPageLayoutComponent implements OnDestroy {
    @Input() showSearchError: boolean;
    @Input() isFetchingResults: boolean;

    /* [TODO_RITESH]: Add type */
    @Input() set searchResults(results: Array<any>) {
        this._searchResults = this.addressService.formatLocationSearchResults(
            results
        );
    }

    get searchResults() {
        return this._searchResults;
    }

    @Output() handleSearchTextChange: EventEmitter<string> = new EventEmitter();

    searchTerm = "";

    /* [TODO_RITESH]: Add type */
    private _searchResults: Array<any>;
    private routerSub: Subscription;

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private routerService: RouterService,
        private addressService: AddressService
    ) {
        this.subscribeToRouterEvents();
    }

    ngOnDestroy() {
        this.unsubscribeRouterEvents();
    }

    onSearchTextChange(searchTerm: string) {
        this.searchTerm = searchTerm.trim();
        this.updateSearchTermInService();

        if (
            this.searchTerm &&
            this.searchTerm.length >= MIN_SEARCH_TERM_LENGTH.LOCATION
        ) {
            this.handleSearchTextChange.emit(this.searchTerm);
            return;
        }

        this._searchResults = [];
    }

    onSearchResultClick(placeId: string) {
        this.addressService.setAddressData(placeId, "placeId");
        this.goBack();
    }

    goBack() {
        this.routerService.goBack();
    }

    private initialize() {
        const searchTerm = this.addressService.getAddressData("searchTerm");

        this.onSearchTextChange(searchTerm);
        this.cdr.detectChanges();
    }

    private updateSearchTermInService() {
        this.addressService.setAddressData(this.searchTerm, "searchTerm");
    }

    private subscribeToRouterEvents() {
        this.routerSub = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                filter((event: NavigationEnd) =>
                    this.isSearchPageUrl(event.url)
                )
            )
            .subscribe(() => {
                /* [TODO_RITESH] Fix bug with search results */
                this.initialize();
            });
    }

    private isSearchPageUrl(url: string): boolean {
        return (
            url &&
            url.indexOf(PAGE_ROUTES.ADDRESS.CHILDREN.ADDRESS_SEARCH.PATH) > -1
        );
    }

    private unsubscribeRouterEvents() {
        if (this.routerSub && !this.routerSub.closed) {
            this.routerSub.unsubscribe();
        }
    }
}
