import { MapPageContainer } from "./map.container";
import { SocietySearchPageContainer } from "./society-search.container";
import { LocationSearchPageContainer } from "./location-search.container";
import { SocietyDetailsSearchPageContainer } from "./society-details-search-container";
import { FormPageContainer } from "./form.container";

export const addressContainers = [
    MapPageContainer,
    SocietySearchPageContainer,
    LocationSearchPageContainer,
    SocietyDetailsSearchPageContainer,
    FormPageContainer,
];
