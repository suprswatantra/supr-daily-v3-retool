import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Address, Building } from "@shared/models";

import { AddressService } from "@services/shared/address.service";
import { AddressCurrentState } from "@store/address/address.state";

import { AddressPageAdapter as Adapter } from "@pages/address/services/address.adapter";

@Component({
    selector: "supr-address-form-container",
    template: `
        <supr-address-form-layout
            [errorMsg]="errorMsg$ | async"
            [buildings]="buildings$ | async"
            [addressState]="addressState$ | async"
            (handleUpdateAddress)="updateAddress($event)"
            [addressState]="addressState$ | async"
            [errorMsg]="errorMsg$ | async"
            (fetchPremiseDetails)="fetchPremiseDetails($event)"
        ></supr-address-form-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPageContainer implements OnInit {
    errorMsg$: Observable<any>;
    buildings$: Observable<Building[]>;
    addressState$: Observable<AddressCurrentState>;

    constructor(
        private adapter: Adapter,
        private addressService: AddressService
    ) {}

    ngOnInit() {
        this.errorMsg$ = this.adapter.errorMsg$;
        this.addressState$ = this.adapter.addressState$;
        this.buildings$ = this.adapter.buildings$;
    }

    updateAddress(address: Address) {
        const data = this.addressService.getAddressRequestData(address);

        const addressContext = this.addressService.getAddressContext();
        const context = `${data.type}-${addressContext}`;
        this.adapter.updateAddress(data, { context });
    }

    fetchPremiseDetails(premiseId: number) {
        this.adapter.searchPremiseAddress(premiseId);
    }
}
