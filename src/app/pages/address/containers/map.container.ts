import { Params } from "@angular/router";
import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { Address } from "@shared/models";

import { AddressPageAdapter } from "../services/address.adapter";

@Component({
    selector: "supr-address-map-container",
    template: `
        <supr-address-map-layout
            [from]="from$ | async"
            [address]="address$ | async"
            [hasAddress]="hasAddress$ | async"
        ></supr-address-map-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapPageContainer implements OnInit {
    from$: Observable<string>;
    address$: Observable<Address>;
    hasAddress$: Observable<boolean>;

    constructor(private addressPageAdapter: AddressPageAdapter) {}

    ngOnInit() {
        this.address$ = this.addressPageAdapter.address$;
        this.hasAddress$ = this.addressPageAdapter.hasAddress$;
        this.from$ = this.addressPageAdapter.urlParams$.pipe(
            map((params: Params) => params["from"])
        );
    }
}
