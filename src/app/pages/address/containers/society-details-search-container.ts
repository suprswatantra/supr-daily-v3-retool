import { Params } from "@angular/router";
import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SuprApi } from "@types";

import { FLOOR_NO } from "../constants";
import { AddressPageAdapter } from "../services/address.adapter";
import { AddressSearchFormattedResult } from "../type";

@Component({
    selector: "supr-society-search-container",
    template: `
        <supr-society-details-search-layout
            [type]="type$ | async"
            [floorOptions]="floorOptions"
            [premiseDetails]="premiseDetails$ | async"
        ></supr-society-details-search-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocietyDetailsSearchPageContainer implements OnInit {
    type$: Observable<string>;
    floorOptions: AddressSearchFormattedResult[];
    premiseDetails$: Observable<SuprApi.PremiseAddressRes>;

    constructor(private addressAdapter: AddressPageAdapter) {}

    ngOnInit() {
        this.floorOptions = FLOOR_NO;
        this.premiseDetails$ = this.addressAdapter.premiseAddress$;
        this.type$ = this.addressAdapter.urlParams$.pipe(
            map((params: Params) => params["type"])
        );
    }
}
