import { Component, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-address-search-container",
    template: `
        <supr-address-search-layout
            [showSearchError]="showSearchError"
            [isFetchingResults]="isFetchingResults"
            [searchResults]="searchResults$ | async"
            (handleSearchTextChange)="onSearchTextChange($event)"
        ></supr-address-search-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationSearchPageContainer {
    showSearchError = false;
    isFetchingResults = false;
    /* [TODO_RITESH] Add type for search response */
    searchResults$: Observable<any[]>;

    constructor(
        private apiService: ApiService,
        private utilService: UtilService
    ) {}

    onSearchTextChange(searchTerm: string) {
        this.setFetchingResultStatus(true);

        this.searchResults$ = this.apiService
            .getLocationSuggestions(searchTerm)
            .pipe(
                map((response: any) => {
                    this.setFetchingResultStatus(false);

                    let searchResults = [];

                    if (!response.success) {
                        this.showSearchError = true;
                        searchResults = [];
                    } else {
                        this.showSearchError = false;
                        searchResults = this.utilService.getNestedValue(
                            response,
                            "data.suggestions",
                            []
                        );
                    }

                    return searchResults;
                })
            );
    }

    private setFetchingResultStatus(status: boolean) {
        this.isFetchingResults = status;
    }
}
