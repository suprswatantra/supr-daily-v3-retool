import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { SuprApi } from "@types";

import { AddressPageAdapter } from "../services/address.adapter";

@Component({
    selector: "supr-society-search-container",
    template: `
        <supr-society-search-layout
            [showSearchError]="showSearchError"
            [isFetchingPremiseList]="isFetchingPremiseList$ | async"
            [searchResults]="searchResults$ | async"
            [popularPremiseResults]="popularPremiseResults$ | async"
            (handleSocietyChange)="onSocietyChange($event)"
            (handleSearchTextChange)="onSearchTextChange($event)"
            (handlePopularSearchPremise)="searchPopularPremise($event)"
        ></supr-society-search-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocietySearchPageContainer implements OnInit {
    showSearchError = false;
    isFetchingPremiseList$: Observable<boolean>;
    searchResults$: Observable<SuprApi.PremiseSearchRes[]>;
    popularPremiseResults$: Observable<SuprApi.PremiseSearchRes[]>;

    constructor(private addressAdapter: AddressPageAdapter) {}

    ngOnInit() {
        this.searchResults$ = this.addressAdapter.premise$;
        this.popularPremiseResults$ = this.addressAdapter.popularPremise$;
        this.isFetchingPremiseList$ = this.addressAdapter.isFetchingPremiseList$;
    }

    onSearchTextChange(searchData: SuprApi.PremiseSearchReq) {
        this.addressAdapter.searchPremise(searchData);
    }

    searchPopularPremise(searchData: SuprApi.PremiseSearchReq) {
        this.addressAdapter.searchPopularPremise(searchData);
    }

    onSocietyChange(societyId: number) {
        this.addressAdapter.searchPremiseAddress(societyId);
    }
}
