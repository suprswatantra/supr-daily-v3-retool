import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ADDRESS_TYPE } from "@constants";

import { RadioButton } from "@types";

import {
    SA_OBJECT_NAMES,
    ADDRESS_DISPLAY_TEXT,
    RESIDENCE_TYPE_OPTIONS,
} from "@pages/address/constants";

@Component({
    selector: "address-residence-type",
    template: `
        <div class="suprColumn left wrapper">
            <supr-text type="body">
                {{ RESIDENCE_TYPE_TEXT }}
            </supr-text>
            <div class="divider8"></div>
            <supr-radio-button-group
                [options]="_residenceOptions"
                colSize="2"
                [selectedIndex]="_selectedIndex"
                (handleSelect)="onSelectOption($event)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.RESIDENCE_TYPE}"
            ></supr-radio-button-group>
        </div>
    `,
    styleUrls: ["./residence-type.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResidenceTypeComponent implements OnInit {
    @Input() set addressType(type: string) {
        this._selectedIndex = type === ADDRESS_TYPE.INDIVIDUAL_HOUSE ? 1 : 0;
    }

    @Output()
    handleOptionChange: EventEmitter<number> = new EventEmitter();

    _selectedIndex: number;
    _residenceOptions: RadioButton[];
    RESIDENCE_TYPE_OPTIONS = RESIDENCE_TYPE_OPTIONS;
    RESIDENCE_TYPE_TEXT = ADDRESS_DISPLAY_TEXT.RESIDENCE_TYPE_TEXT;

    ngOnInit() {
        this.setRechargeOptions();
    }

    onSelectOption(selectedOption: number) {
        this._selectedIndex = selectedOption;
        this.handleOptionChange.emit(selectedOption);
    }

    private setRechargeOptions() {
        this._residenceOptions = RESIDENCE_TYPE_OPTIONS.map(
            (option: string, index) => ({ index, text: `${option}` })
        );
    }
}
