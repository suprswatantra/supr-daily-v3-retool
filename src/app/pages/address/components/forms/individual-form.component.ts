import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import {
    INDIVIDUAL_FORM_INPUT,
    SA_OBJECT_NAMES,
    ADDRESS_DISPLAY_TEXT,
} from "@pages/address/constants";

@Component({
    selector: "supr-address-individual-form",
    template: `
        <div class="form">
            <div class="suprRow base">
                <supr-input-label
                    textType="body"
                    class="formInput"
                    [placeholder]="DOOR_NUMBER.placeholder"
                    [disabled]="DOOR_NUMBER.disabled"
                    [label]="DOOR_NUMBER.name"
                    [value]="(formData && formData[DOOR_NUMBER.key]) || ''"
                    [error]="
                        (mandatoryFormFields &&
                        mandatoryFormFields[DOOR_NUMBER.key]
                            ? DOOR_NUMBER.error
                            : '') || ''
                    "
                    (handleInputChange)="onChange($event, DOOR_NUMBER.key)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.PLOT_NO}"
                ></supr-input-label>
                <div class="spacer16"></div>
                <supr-input-label
                    textType="body"
                    class="formInput"
                    [label]="FLOOR.name"
                    [disabled]="FLOOR.disabled"
                    [placeholder]="FLOOR.placeholder"
                    [isLabelIcon]="FLOOR.isLabelIcon"
                    [value]="(formData && formData[FLOOR.key]) || ''"
                    [error]="
                        (mandatoryFormFields && mandatoryFormFields[FLOOR.key]
                            ? FLOOR.error
                            : '') || ''
                    "
                    (click)="handleInputFieldClick.emit(FLOOR.key)"
                    (handleInputChange)="onChange($event, FLOOR.key)"
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.CLICK.FLOOR_NO}"
                ></supr-input-label>
            </div>

            <div class="divider20"></div>

            <supr-input-label
                textType="body"
                [placeholder]="ADDRESS.placeholder"
                [disabled]="ADDRESS.disabled"
                [label]="ADDRESS.name"
                [value]="(formData && formData[ADDRESS.key]) || ''"
                [error]="
                    (mandatoryFormFields && mandatoryFormFields[ADDRESS.key]
                        ? ADDRESS.error
                        : '') || ''
                "
                (handleInputChange)="onChange($event, ADDRESS.key)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.STREET_ADDRESS}"
            ></supr-input-label>
            <div class="divider20"></div>
            <supr-input-label
                textType="body"
                [placeholder]="INSTRUCTION.placeholder"
                [disabled]="INSTRUCTION.disabled"
                [label]="INSTRUCTION.name"
                [value]="(formData && formData[INSTRUCTION.key]) || ''"
                [error]="
                    (mandatoryFormFields && mandatoryFormFields[INSTRUCTION.key]
                        ? INSTRUCTION.error
                        : '') || ''
                "
                (handleInputChange)="onChange($event, INSTRUCTION.key)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.INSTRUCTIONS}"
            ></supr-input-label>
            <div class="divider8"></div>
            <supr-text type="body">
                {{ INFO_TEXT }}
            </supr-text>
        </div>
    `,
    styleUrls: ["./forms.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndividualFormComponent {
    /* [TODO_RITESH] Add Type */
    @Input() formData: any;
    @Input() mandatoryFormFields: {};

    @Output() handleInputFieldClick: EventEmitter<string> = new EventEmitter();
    /* [TODO_RITESH] Add Type */
    @Output() handleInputChange: EventEmitter<{
        [key: string]: string;
    }> = new EventEmitter();

    FLOOR = INDIVIDUAL_FORM_INPUT.floorNumber;
    ADDRESS = INDIVIDUAL_FORM_INPUT.streetAddress;
    DOOR_NUMBER = INDIVIDUAL_FORM_INPUT.doorNumber;
    INSTRUCTION = INDIVIDUAL_FORM_INPUT.instructions;
    INFO_TEXT = ADDRESS_DISPLAY_TEXT.INFO_TEXT;

    onChange(value: any, key: string) {
        this.handleInputChange.emit({ [key]: value.trimLeft() });
    }
}
