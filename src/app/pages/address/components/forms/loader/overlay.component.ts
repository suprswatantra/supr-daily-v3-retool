import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-address-apartment-loader-overlay",
    template: `
        <supr-backdrop></supr-backdrop>
    `,
    styleUrls: ["./loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderOverlayComponent {}
