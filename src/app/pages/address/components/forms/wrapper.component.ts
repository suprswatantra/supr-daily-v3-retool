import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { ADDRESS_TYPE } from "@constants";

import { RouterService } from "@services/util/router.service";
import { AddressService } from "@services/shared/address.service";

import {
    BUTTON_TEXT,
    MANDATORY_FORM_FIELDS,
    APARTMENT_FORM_INPUT,
    INDIVIDUAL_FORM_INPUT,
} from "@pages/address/constants";

@Component({
    selector: "supr-address-form-wrapper",
    template: `
        <div class="wrapper">
            <address-residence-type
                [addressType]="addressType"
                (handleOptionChange)="optionChange($event)"
            ></address-residence-type>
            <div class="divider8"></div>
            <supr-address-apartment-form
                *ngIf="addressType !== '${ADDRESS_TYPE.INDIVIDUAL_HOUSE}'"
                [addressType]="addressType"
                [formData]="apartmentFormData"
                [disabledFields]="disabledFields"
                [mandatoryFormFields]="mandatoryFormFields"
                [isOverlayApartment]="isOverlayApartment"
                (handleInputChange)="onInputChange($event)"
                (handleInputFieldClick)="onInputFieldClick($event)"
            ></supr-address-apartment-form>

            <supr-address-individual-form
                *ngIf="addressType === '${ADDRESS_TYPE.INDIVIDUAL_HOUSE}'"
                [formData]="individualFormData"
                [mandatoryFormFields]="mandatoryFormFields"
                (handleInputChange)="onInputChange($event)"
                (handleInputFieldClick)="onInputFieldClick($event)"
            ></supr-address-individual-form>
        </div>
    `,
    styleUrls: ["./forms.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormWrapperComponent {
    @Input() set formData(data: any) {
        this._formData = data;
        this.setInitialFormData(data);
        this.setInitialFormState(data);
        this.setInitialFormAddressType();
    }
    get formData(): any {
        return this._formData;
    }

    @Input() set addressType(type: string) {
        this._addressType = type;
        this.setInitialFormData();
        this.setInitialFormState();
    }
    get addressType(): string {
        return this._addressType;
    }

    @Input() isOverlayApartment: boolean;
    @Input() mandatoryFormFields: {};

    @Output() handleTypeChange: EventEmitter<string> = new EventEmitter();

    disabledFields = {};
    apartmentFormData: any;
    individualFormData: any;
    BUTTON_TEXT = BUTTON_TEXT;
    MANDATORY_FORM_FIELDS = MANDATORY_FORM_FIELDS;

    /* [TODO_RITESH] Add type */
    private _formData: any;
    private _addressType: string;

    constructor(
        private routerService: RouterService,
        private addressService: AddressService
    ) {}

    optionChange(option: number) {
        const serviceDataKey = option
            ? "individualHouseFormData"
            : "apartmentFormData";

        const apartmentType = this.addressService.getAddressData(
            "apartmentType"
        );

        this.addressType = option
            ? ADDRESS_TYPE.INDIVIDUAL_HOUSE
            : apartmentType
            ? apartmentType
            : ADDRESS_TYPE.KNOWN_SOCIETY;

        this.addressService.setAddressData(this.addressType, "formAddressType");
        this.formData = this.addressService.getAddressData(serviceDataKey);
        this.mandatoryFormFields = {};
    }

    onInputChange(inputData: any) {
        const serviceDataKey = this.addressService.getFormDataTypeKey(
            this.addressType
        );
        const savedFormData = this.addressService.getAddressData(
            serviceDataKey
        );

        const newData = { ...savedFormData, ...inputData };
        this.addressService.setAddressData(newData, serviceDataKey);

        if (this.addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            this.individualFormData = newData;
        } else if (
            this.addressType === ADDRESS_TYPE.KNOWN_SOCIETY ||
            this.addressType === ADDRESS_TYPE.UNKNOWN_SOCIETY
        ) {
            this.apartmentFormData = newData;
        }
    }

    onInputFieldClick(type: string) {
        if (type === APARTMENT_FORM_INPUT.societyName.key) {
            this.routerService.goToSocietySearchPage();
        } else if (
            (type === APARTMENT_FORM_INPUT.doorNumber.key ||
                type === APARTMENT_FORM_INPUT.buildingName.key ||
                type === INDIVIDUAL_FORM_INPUT.floorNumber.key) &&
            !this.disabledFields[type]
        ) {
            this.routerService.goToSocietyDetailSearchPage({
                queryParams: { type },
            });
        }
    }

    private setInitialFormData(formData?: any) {
        let _formData = formData;

        if (!_formData) {
            _formData = this.getFormData();
        }

        if (!_formData) {
            return;
        }

        if (this.addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            this.individualFormData = _formData;
        } else if (
            this.addressType === ADDRESS_TYPE.KNOWN_SOCIETY ||
            this.addressType === ADDRESS_TYPE.UNKNOWN_SOCIETY
        ) {
            this.apartmentFormData = _formData;
        }
    }

    private setInitialFormState(formData?: any) {
        if (this.addressType !== ADDRESS_TYPE.KNOWN_SOCIETY) {
            APARTMENT_FORM_INPUT.buildingName.disabled = false;
            APARTMENT_FORM_INPUT.doorNumber.disabled = false;
            this.disabledFields = {};
            return;
        }

        let _formData = formData;

        if (!_formData) {
            _formData = this.getFormData();
        }

        if (this.addressType === ADDRESS_TYPE.KNOWN_SOCIETY) {
            APARTMENT_FORM_INPUT.buildingName.disabled = true;
            APARTMENT_FORM_INPUT.doorNumber.disabled = true;

            this.disabledFields = {
                [APARTMENT_FORM_INPUT.buildingName.key]: true,
                [APARTMENT_FORM_INPUT.doorNumber.key]: true,
            };
        }

        if (!_formData) {
            return;
        }

        if (!_formData[APARTMENT_FORM_INPUT.societyName.key]) {
            this.disabledFields = {
                [APARTMENT_FORM_INPUT.buildingName.key]: true,
                [APARTMENT_FORM_INPUT.doorNumber.key]: true,
            };

            return;
        } else {
            APARTMENT_FORM_INPUT.buildingName.disabled = false;
            this.disabledFields = {
                [APARTMENT_FORM_INPUT.buildingName.key]: false,
            };
        }

        if (!_formData[APARTMENT_FORM_INPUT.buildingName.key]) {
            this.disabledFields = {
                [APARTMENT_FORM_INPUT.buildingName.key]: false,
                [APARTMENT_FORM_INPUT.doorNumber.key]: true,
            };

            return;
        } else {
            APARTMENT_FORM_INPUT.doorNumber.disabled = false;
            this.disabledFields = {
                [APARTMENT_FORM_INPUT.doorNumber.key]: false,
            };
        }
    }

    /* [TODO_RITESH] Add type */
    private getFormData(): any {
        const serviceDataKey = this.addressService.getFormDataTypeKey(
            this.addressType
        );

        if (!serviceDataKey) {
            return null;
        }

        return this.addressService.getAddressData(serviceDataKey);
    }

    private setInitialFormAddressType() {
        const formAddressType = this.addressService.getAddressData(
            "formAddressType"
        );
        if (formAddressType) {
            return;
        }

        this.addressService.setAddressData(
            ADDRESS_TYPE.KNOWN_SOCIETY,
            "formAddressType"
        );
    }
}
