import { Component, Output, EventEmitter, Input } from "@angular/core";
import { SA_OBJECT_NAMES } from "@pages/address/constants";

@Component({
    selector: "supr-address-form-header",
    template: `
        <supr-page-header>
            <div class="suprRow spaceBetween header">
                <supr-text type="caption">
                    {{ addressText }}
                </supr-text>

                <supr-button
                    (handleClick)="handleClick.emit()"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.EDIT_BUTTON}"
                >
                    Edit
                </supr-button>
            </div>
        </supr-page-header>
    `,
    styleUrls: ["./header.component.scss"],
})
export class FormHeaderComponent {
    @Input() addressText: string;
    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
