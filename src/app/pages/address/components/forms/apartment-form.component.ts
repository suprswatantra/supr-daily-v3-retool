import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ADDRESS_TYPE, MAX_TEXT_INPUT_LIMIT } from "@constants";

import {
    APARTMENT_FORM_INPUT,
    SA_OBJECT_NAMES,
    ADDRESS_DISPLAY_TEXT,
} from "@pages/address/constants";

@Component({
    selector: "supr-address-apartment-form",
    template: `
        <div class="form">
            <supr-input-label
                textType="body"
                [placeholder]="APARTMENT.placeholder"
                [disabled]="APARTMENT.disabled"
                [label]="APARTMENT.name"
                [value]="formData ? formData[APARTMENT.key] : '' || ''"
                [error]="
                    (mandatoryFormFields && mandatoryFormFields[APARTMENT.key]
                        ? APARTMENT.error
                        : '') || ''
                "
                (handleInputChange)="onChange($event, APARTMENT.key)"
                [isLabelIcon]="APARTMENT.isLabelIcon"
                (click)="handleInputFieldClick.emit(APARTMENT.key)"
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.APARTMENT_NAME}"
            ></supr-input-label>
            <div class="divider20"></div>

            <div class="suprRow base">
                <supr-input-label
                    textType="body"
                    class="formInput"
                    [placeholder]="TOWER.placeholder"
                    [disabled]="TOWER.disabled || disabledFields[TOWER.key]"
                    [label]="TOWER.name"
                    [value]="(formData && formData[TOWER.key]) || ''"
                    [error]="
                        (mandatoryFormFields && mandatoryFormFields[TOWER.key]
                            ? TOWER.error
                            : '') || ''
                    "
                    [isLabelIcon]="
                        TOWER.isLabelIcon &&
                        addressType === '${ADDRESS_TYPE.KNOWN_SOCIETY}'
                    "
                    (click)="onInputFieldClick(TOWER.key)"
                    (handleInputChange)="onChange($event, TOWER.key)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.TOWER_NO}"
                ></supr-input-label>
                <div class="spacer16"></div>
                <supr-input-label
                    textType="body"
                    class="formInput"
                    [placeholder]="FLAT.placeholder"
                    [disabled]="FLAT.disabled || disabledFields[FLAT.key]"
                    [label]="FLAT.name"
                    (handleInputChange)="onChange($event, FLAT.key)"
                    (click)="onInputFieldClick(FLAT.key)"
                    [isLabelIcon]="
                        FLAT.isLabelIcon &&
                        addressType === '${ADDRESS_TYPE.KNOWN_SOCIETY}'
                    "
                    [value]="(formData && formData[FLAT.key]) || ''"
                    [error]="
                        (mandatoryFormFields && mandatoryFormFields[FLAT.key]
                            ? FLAT.error
                            : '') || ''
                    "
                    saObjectName="${SA_OBJECT_NAMES.CLICK.FLAT_NO}"
                ></supr-input-label>
            </div>
            <div *ngIf="addressType === '${ADDRESS_TYPE.UNKNOWN_SOCIETY}'">
                <div class="divider20"></div>
                <supr-input-label
                    textType="body"
                    [label]="ADDRESS.name"
                    [value]="(formData && formData[ADDRESS.key]) || ''"
                    [error]="
                        (mandatoryFormFields && mandatoryFormFields[ADDRESS.key]
                            ? ADDRESS.error
                            : '') || ''
                    "
                    [disabled]="ADDRESS.disabled"
                    [placeholder]="ADDRESS.placeholder"
                    (handleInputChange)="onChange($event, ADDRESS.key)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.STREET_ADDRESS}"
                ></supr-input-label>
                <div class="divider20"></div>
                <supr-input-label
                    textType="body"
                    [label]="INSTRUCTION.name"
                    [value]="(formData && formData[INSTRUCTION.key]) || ''"
                    [error]="
                        (mandatoryFormFields &&
                        mandatoryFormFields[INSTRUCTION.key]
                            ? INSTRUCTION.error
                            : '') || ''
                    "
                    [disabled]="INSTRUCTION.disabled"
                    [placeholder]="INSTRUCTION.placeholder"
                    (handleInputChange)="onChange($event, INSTRUCTION.key)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.INSTRUCTIONS}"
                ></supr-input-label>
                <div class="divider8"></div>
                <supr-text type="body">
                    {{ INFO_TEXT }}
                </supr-text>
            </div>

            <supr-address-apartment-loader-overlay *ngIf="isOverlayApartment">
            </supr-address-apartment-loader-overlay>
        </div>
    `,
    styleUrls: ["./forms.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApartmentFormComponent {
    /* [TODO_RITESH] Add Type */
    @Input() formData: any;
    @Input() addressType: string;
    @Input() disabledFields = {};
    @Input() mandatoryFormFields: {};
    @Input() isOverlayApartment: boolean;

    @Output() handleInputFieldClick: EventEmitter<string> = new EventEmitter();
    @Output() handleInputChange: EventEmitter<{
        [key: string]: string;
    }> = new EventEmitter();

    FLAT = APARTMENT_FORM_INPUT.doorNumber;
    TOWER = APARTMENT_FORM_INPUT.buildingName;
    ADDRESS = APARTMENT_FORM_INPUT.streetAddress;
    APARTMENT = APARTMENT_FORM_INPUT.societyName;
    INSTRUCTION = APARTMENT_FORM_INPUT.instructions;
    INFO_TEXT = ADDRESS_DISPLAY_TEXT.INFO_TEXT;

    MAX_TEXT_INPUT_LIMIT = MAX_TEXT_INPUT_LIMIT;

    onChange(value: string, key: string) {
        this.handleInputChange.emit({ [key]: value });
    }

    onInputFieldClick(field: string) {
        if (
            this.addressType === ADDRESS_TYPE.KNOWN_SOCIETY &&
            !this.disabledFields[field]
        ) {
            this.handleInputFieldClick.emit(field);
        }
    }
}
