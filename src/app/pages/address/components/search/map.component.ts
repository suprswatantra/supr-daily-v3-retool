import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
} from "@angular/core";

@Component({
    selector: "supr-map-search",
    template: `
        <supr-address-search-header
            [searchTerm]="searchTerm"
            searchPlaceholder="Enter area, street, apartment name..."
            (handleSearchTextChange)="handleSearchTextChange.emit($event)"
        ></supr-address-search-header>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapSearchComponent {
    @Input() searchTerm: string;
    @Output() handleSearchTextChange: EventEmitter<string> = new EventEmitter();
}
