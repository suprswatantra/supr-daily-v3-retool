import { Component, ChangeDetectionStrategy } from "@angular/core";
import { APARTMENT_NAME_TEXTS } from "@pages/address/constants";

@Component({
    selector: "supr-apartment-name-info",
    template: `
        <div class="apartmentNameInfo">
            <div class="apartmentNameInfoWrapper suprRow">
                <supr-icon name="error"></supr-icon>
                <div class="suprColumn left center">
                    <supr-text type="caption"
                        >${APARTMENT_NAME_TEXTS.INFO_TEXT}</supr-text
                    >
                    <supr-text type="caption"
                        >${APARTMENT_NAME_TEXTS.NAME_EXAMPLE_TEXT}</supr-text
                    >
                </div>
            </div>
            <div class="divider16"></div>
            <div class="apartmentNameInfoDivider"></div>
        </div>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApartmentNameInfoComponent {}
