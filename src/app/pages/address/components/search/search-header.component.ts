import {
    Component,
    Output,
    EventEmitter,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-address-search-header",
    template: `
        <supr-page-header>
            <supr-search-input
                [searchTerm]="searchTerm"
                [placeholder]="searchPlaceholder"
                (handleChangeText)="handleSearchTextChange.emit($event)"
            ></supr-search-input>
        </supr-page-header>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchHeaderComponent {
    @Input() searchTerm: string;
    @Input() searchPlaceholder: string;
    @Output() handleSearchTextChange: EventEmitter<string> = new EventEmitter();
}
