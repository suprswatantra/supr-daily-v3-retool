import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { AddressSearchFormattedResult } from "@pages/address/type";

@Component({
    selector: "supr-address-search-result-wrapper",
    template: `
        <div
            class="suprColumn left resultWrapper"
            [class.paddingTop]="isSocietySearch"
            [class.paddingTopNoTitle]="!title && isSocietySearch"
        >
            <div
                class="titleWrapper"
                *ngIf="title && searchResults && searchResults.length"
            >
                <supr-text type="body">
                    {{ title }}
                </supr-text>

                <div class="divider8"></div>
                <div class="resultWrapperDivider"></div>
            </div>

            <div
                class="resultWrapperContent"
                *ngFor="
                    let result of searchResults;
                    trackBy: trackByFn;
                    last as _last
                "
            >
                <supr-address-search-result
                    [title]="result.title"
                    [subTitle]="result.subtitle"
                    [isLast]="_last"
                    [iconName]="iconName"
                    (click)="onSearchResultClick(result)"
                    [saContext]="saContext"
                ></supr-address-search-result>
            </div>
        </div>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultWrapperComponent {
    @Input() title: string;
    @Input() saContext: string;
    @Input() isSocietySearch = false;
    /* [TODO_RITESH]: Add type */
    @Input() searchResults: Array<any>;
    @Input() iconName: string;

    @Output() handleSearchResultClick: EventEmitter<
        string | number
    > = new EventEmitter();

    trackByFn(_: any, index: number): number {
        return index;
    }

    onSearchResultClick(result: AddressSearchFormattedResult) {
        if (result.id) {
            setTimeout(() => {
                this.handleSearchResultClick.emit(result.id);
            }, 0);
        }
    }
}
