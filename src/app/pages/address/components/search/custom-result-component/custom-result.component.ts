import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-address-search-custom-result",
    template: `
        <div class="current">
            <div class="divider12"></div>
            <div class="suprRow">
                <supr-icon *ngIf="icon" [name]="icon" class="icon"></supr-icon>
                <div class="spacer16"></div>
                <div class="suprColumn left">
                    <supr-text *ngIf="title" type="regular14">
                        {{ title }}
                    </supr-text>
                    <div class="divider4"></div>

                    <ng-container *ngIf="subtitle">
                        <supr-text class="secondary" type="caption">
                            {{ subtitle }}
                        </supr-text>
                    </ng-container>
                </div>
            </div>
            <div class="divider16"></div>
            <div class="currentDivider"></div>
        </div>
    `,
    styleUrls: ["./custom-result-component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomResultComponent {
    @Input() icon: string;
    @Input() title: string;
    @Input() subtitle: string;
}
