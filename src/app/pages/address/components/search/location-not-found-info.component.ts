import { Component, ChangeDetectionStrategy } from "@angular/core";
import { LOCATION_SEARCH_NOT_INFO_TEXT } from "@pages/address/constants";

@Component({
    selector: "supr-location-search-not-found-info",
    template: `
        <div>
            <div class="suprRow locationSearchNotFoundInfo">
                <supr-icon name="search"></supr-icon>
                <div class="spacer24"></div>

                <supr-text type="regular14">
                    ${LOCATION_SEARCH_NOT_INFO_TEXT}
                </supr-text>
            </div>
            <div class="divider16"></div>
            <div class="locationSearchNotFoundInfoDivider"></div>
        </div>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationSearchNotFoundInfoComponent {}
