import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { SA_OBJECT_NAMES } from "@pages/address/constants";

@Component({
    selector: "supr-address-search-result",
    template: `
        <div
            saClick
            class="results"
            [class.icon]="iconName"
            [saContext]="saContext"
            [saObjectValue]="title"
            saObjectName="${SA_OBJECT_NAMES.CLICK.SEARCH_RESULTS}"
        >
            <div class="divider16"></div>
            <div class="suprRow">
                <ng-container *ngIf="iconName">
                    <supr-icon [name]="iconName"></supr-icon>
                    <div class="spacer16"></div>
                </ng-container>
                <div class="suprColumn left">
                    <supr-text class="primary" type="regular14">
                        {{ title }}
                    </supr-text>
                    <ng-container *ngIf="subTitle">
                        <div class="divider4"></div>
                        <supr-text class="secondary" type="caption">
                            {{ subTitle }}
                        </supr-text>
                    </ng-container>
                </div>
            </div>
            <div class="divider16"></div>
            <div class="resultsDivider"></div>
        </div>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultComponent {
    @Input() title: string;
    @Input() subTitle: string;

    // Removed on requirement for to see difference
    @Input() isLast: boolean;
    @Input() saContext: string;
    @Input() iconName: string;
}
