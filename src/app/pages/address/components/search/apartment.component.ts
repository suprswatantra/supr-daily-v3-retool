import {
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

@Component({
    selector: "supr-apartment-search",
    template: `
        <supr-address-search-header
            [searchTerm]="searchTerm"
            [searchPlaceholder]="searchPlaceholder"
            (handleSearchTextChange)="handleSearchTextChange.emit($event)"
        ></supr-address-search-header>
        <supr-apartment-name-info
            *ngIf="showSearchInfo"
        ></supr-apartment-name-info>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApartmentSearchComponent {
    @Input() searchTerm: string;
    @Input() showSearchInfo = false;
    @Input() searchPlaceholder: string;

    @Output() handleSearchTextChange: EventEmitter<string> = new EventEmitter();
}
