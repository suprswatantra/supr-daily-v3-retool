import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-apartment-search-info",
    template: `
        <div class="apartmentSearchInfo">
            <div class="suprRow">
                <div class="spacer8"></div>
                <supr-icon name="search"></supr-icon>
                <div class="spacer16"></div>

                <div class="suprColumn left">
                    <supr-text type="caption">
                        {{ searchInfotextLine1 }}
                    </supr-text>
                    <supr-text type="caption">
                        {{ searchInfotextLine2 }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./search.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApartmentSearchInfoComponent {
    @Input() searchInfotextLine1: string;
    @Input() searchInfotextLine2: string;
}
