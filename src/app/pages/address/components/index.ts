import { ResidenceTypeComponent } from "./residence-type/residency-type.component";
import { MapAddressSearchComponent } from "./map-address-search/map-address-search.component";
import { AddCustomInfoComponent } from "./custom-info/add-custom-info.component";

import { FormWrapperComponent } from "./forms/wrapper.component";
import { FormHeaderComponent } from "./forms/header/header.component";
import { ApartmentFormComponent } from "./forms/apartment-form.component";
import { IndividualFormComponent } from "./forms/individual-form.component";
import { LoaderOverlayComponent } from "./forms/loader/overlay.component";

import { MapSearchComponent } from "./search/map.component";
import { ApartmentSearchComponent } from "./search/apartment.component";
import { SearchHeaderComponent } from "./search/search-header.component";
import { SearchResultComponent } from "./search/search-result.component";
import { SearchResultWrapperComponent } from "./search/search-results-wrapper.component";
import { ApartmentNameInfoComponent } from "./search/apartment-name-info-wrapper.component";
import { CustomResultComponent } from "./search/custom-result-component/custom-result.component";
import { ApartmentSearchInfoComponent } from "./search/apartment-search-info.component";
import { LocationSearchNotFoundInfoComponent } from "./search/location-not-found-info.component";

import { UpdateAddressComponent } from "./modals/update-address.component";
import { InputSearchComponent } from "./modals/search-input.component";

export const addressComponents = [
    MapSearchComponent,
    ResidenceTypeComponent,
    AddCustomInfoComponent,

    FormWrapperComponent,
    FormHeaderComponent,
    ApartmentFormComponent,
    IndividualFormComponent,

    SearchHeaderComponent,
    SearchResultComponent,
    CustomResultComponent,
    ApartmentSearchComponent,
    MapAddressSearchComponent,
    ApartmentNameInfoComponent,
    SearchResultWrapperComponent,
    ApartmentSearchInfoComponent,
    LocationSearchNotFoundInfoComponent,

    UpdateAddressComponent,
    InputSearchComponent,

    LoaderOverlayComponent,
];
