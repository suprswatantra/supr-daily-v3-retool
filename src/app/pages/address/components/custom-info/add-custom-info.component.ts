import { Component, Input } from "@angular/core";

@Component({
    selector: "supr-address-add-custom-info",
    template: `
        <div class="suprRow center custom">
            <supr-icon
                [name]="leftIcon"
                class="add"
                *ngIf="leftIcon"
            ></supr-icon>

            <div class="spacer16"></div>

            <div class="suprColumn left customContent">
                <supr-text
                    *ngIf="searchTerm; else titleText"
                    type="action14"
                    class="title"
                >
                    ADD : {{ searchTerm }}
                </supr-text>
                <ng-template #titleText>
                    <supr-text type="action14" class="title">
                        {{ title }}
                    </supr-text>
                </ng-template>
                <div class="divider4"></div>
                <supr-text type="caption">
                    {{ subTitle }}
                </supr-text>
            </div>

            <supr-icon name="chevron_right"></supr-icon>
        </div>
    `,
    styleUrls: ["./add-custom-info.component.scss"],
})
export class AddCustomInfoComponent {
    @Input() title: string;
    @Input() subTitle: string;
    @Input() leftIcon: string;
    @Input() searchTerm: string;
}
