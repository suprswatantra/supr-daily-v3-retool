import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";
import { SuprMaps } from "@types";
import { SA_OBJECT_NAMES } from "@pages/address/constants";

@Component({
    selector: "supr-map-address-search",
    template: `
        <div class="wrapper">
            <div class="suprRow title">
                <supr-icon
                    *ngIf="error"
                    name="error_triangle"
                    saImpression
                    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                        .ADDRESS_NOT_SERVICEABLE}"
                    [saObjectValue]="address?.addressText"
                    [saContext]="address?.city?.name"
                ></supr-icon>
                <div *ngIf="error" class="spacer8"></div>
                <supr-text type="body" [class.error]="error">
                    {{ title }}
                </supr-text>
            </div>
            <div
                class="suprRow search"
                (click)="handleSearchClick.emit()"
                saImpression
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.LOCATION_SEARCH_BOX}"
            >
                <supr-text type="paragraph" [class.error]="error">
                    {{ address?.addressText }}
                </supr-text>
                <supr-icon name="search"></supr-icon>
            </div>
            <div class="suprRow province">
                <supr-text *ngIf="address?.city?.name" type="caption">
                    City: {{ address?.city?.name }}
                </supr-text>
                <supr-text
                    *ngIf="address?.city?.name && address?.state"
                    type="caption"
                >
                    ,&nbsp;
                </supr-text>
                <supr-text *ngIf="address?.state" type="caption">
                    {{ address?.state }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./map-addres-search.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapAddressSearchComponent {
    @Input() title: string;
    @Input() error: boolean;
    @Input() updating: boolean;
    @Input() address: SuprMaps.MapSearchAddressInfo;

    @Output() handleSearchClick: EventEmitter<void> = new EventEmitter();
}
