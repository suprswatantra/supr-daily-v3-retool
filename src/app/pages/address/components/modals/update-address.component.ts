import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    Input,
    OnInit,
} from "@angular/core";

import { MAX_TEXT_INPUT_LIMIT } from "@constants";
import { UtilService } from "@services/util/util.service";

import { BUTTON_TEXT } from "../../constants";
import { FlatSearchOption } from "@pages/address/type";

@Component({
    selector: "supr-update-address-field",
    template: `
        <div class="updateAddressContainer">
            <div class="wrapper">
                <div class="suprRow">
                    <supr-text type="subtitle">{{ title }}</supr-text>
                </div>
                <div class="divider24"></div>

                <div class="suprRow">
                    <supr-input-label
                        textType="body"
                        [placeholder]="placeholder"
                        [label]="label"
                        [value]="addressComponentValue"
                        [maxLength]="maxLength"
                        (handleInputChange)="onInputChange($event)"
                    ></supr-input-label>
                </div>
            </div>

            <div class="divider24"></div>
            <supr-button
                [disabled]="!addressComponentValue"
                [saObjectName]="saObjectName"
                [saObjectValue]="addressComponentValue"
                [saContext]="saContext"
                (handleClick)="updateAddressField()"
            >
                <supr-text type="body">${BUTTON_TEXT.CONFIRM}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateAddressComponent implements OnInit {
    @Input() title: string;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() saObjectName: string;
    @Input() saContext: string;
    @Input() maxLength = MAX_TEXT_INPUT_LIMIT;
    @Input() set initialValue(value: string) {
        this._initialValue = this.utilService.toTitleCase(value);
    }
    get initialValue(): string {
        return this._initialValue;
    }

    @Output() handleUpdateClick: EventEmitter<
        FlatSearchOption
    > = new EventEmitter();

    addressComponentValue = "";

    private _initialValue: string;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setInputValue();
    }

    updateAddressField() {
        this.handleUpdateClick.emit({
            id: -1,
            name: this.addressComponentValue,
        });
    }

    onInputChange(text: string) {
        this.addressComponentValue = text.trimLeft();
    }

    private setInputValue() {
        if (this.initialValue) {
            this.addressComponentValue = this.initialValue;
        }
    }
}
