import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { INPUT } from "@constants";

import { ADDRESS_DISPLAY_TEXT } from "../../constants";
import { FlatSearchOption } from "@pages/address/type";

@Component({
    selector: "supr-address-input-search",
    template: `
        <div class="searchWrapper">
            <supr-page-header (handleBackButtonClick)="handleCloseModal.emit()">
                <supr-search-input
                    [placeholder]="placeholder"
                    (handleChangeText)="onInputChange($event)"
                ></supr-search-input>
            </supr-page-header>
            <div class="suprScrollContent listWrapper">
                <ng-container
                    *ngFor="let option of filteredOptions; trackBy: trackByFn"
                >
                    <div class="listOption">
                        <supr-text
                            type="body"
                            (click)="onSelect(option, $event)"
                            saClick
                            [saObjectName]="optionObjectName"
                            [saObjectValue]="option.name"
                            [saContext]="saContext"
                        >
                            {{ option.name }}
                        </supr-text>
                    </div>
                </ng-container>

                <div
                    class="listOption listOptionNoResults"
                    *ngIf="noResultsOption && !filteredOptions.length"
                >
                    <supr-text
                        type="body"
                        (click)="onSelect(noResultsOption, $event)"
                        saClick
                        saImpression
                        [saObjectName]="addUserInputObjectName"
                        [saObjectValue]="noResultsOption.name"
                        [saContext]="saContext"
                    >
                        ${ADDRESS_DISPLAY_TEXT.USER_INPUT_ADD_PREFIX}'{{
                            noResultsOption?.name
                        }}'${ADDRESS_DISPLAY_TEXT.USER_INPUT_ADD_SUFFIX}
                    </supr-text>
                </div>

                <div
                    class="listOption listOptionAddressNotFound suprRow spaceBetween"
                    *ngIf="filteredOptions.length > 0"
                    (click)="handleNoMatchClick.emit()"
                    saClick
                    saImpression
                    [saObjectName]="cantFindAddressObjectName"
                    [saContext]="saContext"
                >
                    <supr-text type="body">
                        {{ cantFindLabel }}
                    </supr-text>

                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputSearchComponent implements OnInit, OnChanges {
    @Input() type: string = INPUT.TYPES.TEXT;
    @Input() textType: string;
    @Input() placeholder = "";
    @Input() disabled = false;
    @Input() debounceTime = INPUT.DEBOUNCE_TIME;
    @Input() optionsList: FlatSearchOption[] = [];
    @Input() value = "";
    @Input() optionObjectName: string;
    @Input() addUserInputObjectName: string;
    @Input() cantFindAddressObjectName: string;
    @Input() saContext: string;
    @Input() cantFindLabel: string;

    @Output() handleInputChange: EventEmitter<any> = new EventEmitter();
    @Output() handleCloseModal: EventEmitter<void> = new EventEmitter();
    @Output() handleNoMatchClick: EventEmitter<void> = new EventEmitter();

    filteredOptions: FlatSearchOption[] = [];
    noResultsOption: FlatSearchOption;

    private rawData: FlatSearchOption[] = [];

    ngOnInit() {
        this.setFilteredOptionsList();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleOptionListChange(changes["optionsList"]);
    }

    onInputChange(searchTerm: string) {
        this.filterOptions(searchTerm);
    }

    onSelect(selected: any, event?: MouseEvent) {
        if (event) {
            event.stopPropagation();
        }
        this.handleInputChange.emit(selected);
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private filterOptions(searchTerm: string) {
        const _searchTerm = searchTerm.toLowerCase();

        const filterList = this.rawData.filter(
            (item: FlatSearchOption) => item.name.indexOf(_searchTerm) > -1
        );

        // if user typed string is not present then add it to options
        if (!filterList.length) {
            this.setUserInputAsOption(searchTerm);
        }

        this.filteredOptions = [...filterList];
    }

    private setFilteredOptionsList() {
        // Init the filtered list with input list
        this.filteredOptions = this.optionsList;

        // Transform the data once
        this.rawData = this.optionsList.map((item: any) => ({
            ...item,
            name: item.name.toLowerCase(),
        }));
    }

    private handleOptionListChange(change: SimpleChange) {
        if (!change || change.firstChange || !change.currentValue) {
            return;
        }

        if (change.currentValue !== change.previousValue) {
            this.setFilteredOptionsList();
        }
    }

    private setUserInputAsOption(searchTerm: string) {
        this.noResultsOption = {
            id: -1,
            name: searchTerm,
        };
    }
}
