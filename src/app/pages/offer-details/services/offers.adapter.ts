import { Injectable } from "@angular/core";

import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import { OfferDetails } from "@shared/models";
import {
    OffersStoreSelectors,
    OffersStoreActions,
    RouterStoreSelectors,
    StoreState,
} from "@supr/store";

@Injectable()
export class OffersAdapter {
    urlParams$ = this.store.pipe(select(RouterStoreSelectors.selectUrlParams));

    isFetchingOfferDetails$ = this.store.pipe(
        select(OffersStoreSelectors.selectIsFetchingOfferDetails)
    );

    offerDetails$ = this.store.pipe(
        select(OffersStoreSelectors.selectOfferDetailsByNameInUrl)
    );

    constructor(private store: Store<StoreState>) {}

    fetchOfferDetails(offerName: string) {
        this.store.dispatch(
            new OffersStoreActions.FetchOfferDetailsRequestAction({
                offerName,
            })
        );
    }

    getOfferDetailsByName(offerName: string): Observable<OfferDetails> {
        return this.store.pipe(
            select(OffersStoreSelectors.selectOfferDetailsByName, { offerName })
        );
    }
}
