import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ICONS, TEXTS } from "../constants";

@Component({
    selector: "offer-tnc",
    template: ` <ng-container *ngIf="tnc">
        <div class="accordionHeaderWrapper">
            <supr-section-header
                [chevronDown]="true"
                [chevronUp]="expanded"
                subTitleType="paragraph"
                title="${TEXTS.TNC_TITLE}"
                subtitle="${TEXTS.TNC_SUBTITLE}"
                icon="${ICONS.TNC_ICON}"
                (click)="handleAccordionClick()"
            >
            </supr-section-header>
        </div>
        <div class="accordionContentWrapper" *ngIf="expanded">
            <ol class="suprColumn left">
                <li *ngFor="let item of tnc; trackBy: trackByFn">
                    <supr-text type="body">{{ item }}</supr-text>
                </li>
            </ol>
        </div>
    </ng-container>`,
    styleUrls: ["../styles/offer-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferDetailsTncComponent {
    @Input() tnc: string[];

    expanded = false;

    handleAccordionClick() {
        this.expanded = !this.expanded;
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
