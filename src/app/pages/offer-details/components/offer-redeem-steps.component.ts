import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ICONS, TEXTS } from "../constants";

@Component({
    selector: "offer-redeem-steps",
    template: ` <ng-container *ngIf="redeemSteps">
        <div class="accordionHeaderWrapper">
            <supr-section-header
                [chevronDown]="true"
                [chevronUp]="expanded"
                subTitleType="paragraph"
                title="${TEXTS.REDEEM_STEPS_TITLE}"
                subtitle="${TEXTS.REDEEM_STEPS_SUBTITLE}"
                icon="${ICONS.REDEEM_STEPS_ICON}"
                (click)="handleAccordionClick()"
            >
            </supr-section-header>
        </div>
        <div class="accordionContentWrapper" *ngIf="expanded">
            <ol class="suprColumn left">
                <li *ngFor="let step of redeemSteps; trackBy: trackByFn">
                    <supr-text type="body">{{ step }}</supr-text>
                </li>
            </ol>
        </div>
    </ng-container>`,
    styleUrls: ["../styles/offer-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferDetailsRedeemStepsComponent {
    @Input() redeemSteps: string[];

    expanded = true;

    handleAccordionClick() {
        this.expanded = !this.expanded;
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
