import { SuprOfferDetailsFaqComponent } from "./offer-faq.component";
import { SuprOfferDetailsRedeemStepsComponent } from "./offer-redeem-steps.component";
import { SuprOfferDetailsTncComponent } from "./offer-tnc.component";

export const offerPageComponents = [
    SuprOfferDetailsTncComponent,
    SuprOfferDetailsRedeemStepsComponent,
    SuprOfferDetailsFaqComponent,
];
