import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ICONS, TEXTS } from "../constants";

@Component({
    selector: "offer-faq",
    template: ` <ng-container *ngIf="faq">
        <div class="accordionHeaderWrapper">
            <supr-section-header
                [chevronDown]="true"
                [chevronUp]="expanded"
                subTitleType="paragraph"
                title="${TEXTS.FAQ_TITLE}"
                subtitle="${TEXTS.FAQ_SUBTITLE}"
                icon="${ICONS.FAQ_ICON}"
                (click)="handleAccordionClick()"
            >
            </supr-section-header>
        </div>
        <div class="accordionContentWrapper" *ngIf="expanded">
            <ol class="suprColumn left">
                <li *ngFor="let item of faq; let i = index; trackBy: trackByFn">
                    <supr-text class="question"
                        >{{ i + 1 }}. {{ item.question }}</supr-text
                    >
                    <supr-text type="body">{{ item.answer }}</supr-text>
                </li>
            </ol>
        </div>
    </ng-container>`,
    styleUrls: ["../styles/offer-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferDetailsFaqComponent {
    @Input() faq: Array<{ question: String; answer: string }>;

    expanded = false;

    handleAccordionClick() {
        this.expanded = !this.expanded;
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
