export const TEXTS = {
    OFFER_DETAILS_PAGE_TITLE: "Offer details",
    REDEEM_STEPS_TITLE: "Steps to redeem",
    REDEEM_STEPS_SUBTITLE: "Follow the steps below",
    TNC_TITLE: "Terms and Conditions",
    TNC_SUBTITLE:
        "By availing the offer you will be accepting the terms stated below",
    FAQ_TITLE: "Frequently Asked Questions",
    FAQ_SUBTITLE: "Please refer to these answers ",
};

export const ICONS = {
    REDEEM_STEPS_ICON: "bill",
    TNC_ICON: "receipt",
    FAQ_ICON: "question",
};

export const DEFAULT_BANNER_IMG =
    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/120583145_348354483033896_681618425083697763_n.png";
