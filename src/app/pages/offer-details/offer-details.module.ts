import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SuprOfferDetailsContainer } from "./containers/offer-details.container";
import { SuprOfferDetailsLayoutComponent } from "./layouts/offer-details.layout";
import { offerPageComponents } from "./components/index";

import { OffersAdapter } from "./services/offers.adapter";

const routes: Routes = [
    {
        path: "",
        component: SuprOfferDetailsContainer,
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        SuprOfferDetailsContainer,
        SuprOfferDetailsLayoutComponent,
        ...offerPageComponents,
    ],
    providers: [OffersAdapter],
})
export class OfferDetailsModule {}
