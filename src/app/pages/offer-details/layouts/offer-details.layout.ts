import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { OfferDetails } from "@shared/models";

import { TEXTS, DEFAULT_BANNER_IMG } from "../constants";

@Component({
    selector: "supr-offer-details-layout",
    template: `<div class="suprContainer">
        <supr-page-header>
            <supr-text type="subheading">
                ${TEXTS.OFFER_DETAILS_PAGE_TITLE}
            </supr-text>
        </supr-page-header>
        <div class="suprScrollContent">
            <ng-container *ngIf="isFetchingOfferDetails; else content">
                <div class="loaderContainer suprColumn center">
                    <supr-loader></supr-loader>
                </div>
            </ng-container>
            <ng-template #content>
                <ng-container *ngIf="offerDetails">
                    <div class="description" *ngIf="offerDetails?.displayInfo">
                        <supr-image
                            [src]="
                                offerDetails?.displayInfo?.bannerImg ||
                                defaultImgSrc
                            "
                            [useDirectUrl]="true"
                        ></supr-image>
                        <div class="divider16"></div>
                        <supr-text type="subtitle" class="offerTitle">{{
                            offerDetails?.displayInfo?.title
                        }}</supr-text>
                        <div class="divider8"></div>
                        <supr-text type="body" class="offerSubtitle">{{
                            offerDetails?.displayInfo?.description
                        }}</supr-text>
                        <div class="divider16"></div>
                    </div>
                    <ng-container *ngIf="offerDetails?.redeemSteps">
                        <offer-redeem-steps
                            [redeemSteps]="offerDetails?.redeemSteps"
                        ></offer-redeem-steps>
                    </ng-container>
                    <ng-container *ngIf="offerDetails?.faq">
                        <offer-faq [faq]="offerDetails?.faq"></offer-faq>
                    </ng-container>
                    <ng-container *ngIf="offerDetails?.tnc">
                        <offer-tnc [tnc]="offerDetails?.tnc"></offer-tnc>
                    </ng-container>
                </ng-container>
            </ng-template>
        </div>
    </div>`,
    styleUrls: ["../styles/offer-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferDetailsLayoutComponent {
    @Input() set offerName(name: string) {
        if (name) {
            this._offerName = name;
            this.fetchOfferDetails.emit(name);
        }
    }
    get offerName(): string {
        return this._offerName;
    }

    @Input() isFetchingOfferDetails: boolean;
    @Input() offerDetails: OfferDetails;

    @Output() fetchOfferDetails: EventEmitter<string> = new EventEmitter();

    defaultImgSrc = DEFAULT_BANNER_IMG;

    private _offerName: string;
}
