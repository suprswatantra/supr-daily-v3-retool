import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { OfferDetails } from "@shared/models";

import { OffersAdapter } from "../services/offers.adapter";

@Component({
    selector: "supr-offer-details-container",
    template: `<supr-offer-details-layout
        [offerName]="offerName$ | async"
        [offerDetails]="offerDetails$ | async"
        [isFetchingOfferDetails]="isFetchingOfferDetails$ | async"
        (fetchOfferDetails)="fetchOfferDetails($event)"
    ></supr-offer-details-layout>`,
    styleUrls: ["../styles/offer-details.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferDetailsContainer implements OnInit {
    offerDetails$: Observable<OfferDetails>;
    isFetchingOfferDetails$: Observable<boolean>;
    offerName$: Observable<string>;

    constructor(private adapter: OffersAdapter) {}

    ngOnInit() {
        this.isFetchingOfferDetails$ = this.adapter.isFetchingOfferDetails$;
        this.offerDetails$ = this.adapter.offerDetails$;
        this.offerName$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["offerName"])
        );
    }

    fetchOfferDetails(offerName: string) {
        this.adapter.fetchOfferDetails(offerName);
    }
}
