import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    SuprPassStoreSelectors,
    CartStoreSelectors,
    RouterStoreSelectors,
    SuprPassStoreActions,
    CartStoreActions,
    SubscriptionStoreActions,
    SubscriptionStoreSelectors,
    WalletStoreSelectors,
    WalletStoreActions,
} from "@supr/store";
import { CartItem } from "@shared/models";

@Injectable()
export class SuprPassAdapter {
    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    cartError$ = this.store.pipe(select(CartStoreSelectors.selectCartError));

    cartItems$ = this.store.pipe(select(CartStoreSelectors.selectCartItems));

    cartMeta$ = this.store.pipe(select(CartStoreSelectors.selectCartMeta));

    cartCurrentState$ = this.store.pipe(
        select(CartStoreSelectors.selectCartCurrentState)
    );

    suprPassInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassInfo)
    );

    suprPassFaqs$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassFaqs)
    );

    suprPassActivity$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassActivity)
    );

    suprPassSavings$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassSavingsDetails)
    );

    isFetchingSuprPassSavingsInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsFetchingSuprPassSavingsInfo)
    );

    isFetchingSuprPassInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsFetchingSuprPassInfo)
    );

    isCancelingSuprPassAutoDebit$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsCancelingSuprPassAutoDebit)
    );

    subscriptions$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    autoDebitPaymentModes$ = this.store.pipe(
        select(WalletStoreSelectors.selectAutoDebitPaymentModes)
    );

    constructor(private store: Store<StoreState>) {}

    fetchSuprPassInfo() {
        this.store.dispatch(
            new SuprPassStoreActions.FetchSuprPassRequestAction({
                silent: false,
            })
        );
    }

    fetchSuprPassSavingsInfo() {
        this.store.dispatch(
            new SuprPassStoreActions.FetchSuprPassSavingsRequestAction({
                silent: false,
            })
        );
    }

    cancelSuprPassAutoDebit() {
        this.store.dispatch(
            new SuprPassStoreActions.CancelSuprPassAutoDebitRequestAction()
        );
    }

    fetchSubscriptions() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }

    fetchAutoDebitPaymentModes() {
        this.store.dispatch(
            new WalletStoreActions.FetchAutoDebitPaymentModesInfoRequestAction({
                silent: true,
            })
        );
    }

    updateItemInCart(item: CartItem, validateCart?: boolean) {
        this.store.dispatch(
            new CartStoreActions.UpdateItemInCartAction({ item, validateCart })
        );
    }
}
