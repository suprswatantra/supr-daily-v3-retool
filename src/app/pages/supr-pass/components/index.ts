import { AutoDebitCancelModal } from "./cancel-autodebit/cancel-autodebit-modal.component";
import { ActiveAutoDebitInfoComponent } from "./active-autodebit-info/active-autodebit-info.component";
import { ActivePlanComponent } from "./active-plan/active-plan.component";
import { FooterComponent } from "./footer/footer.component";
import { FaqComponent } from "./faqs/faq.component";
import { TopBarComponent } from "./top-bar/top-bar.component";
import { HeaderComponent } from "./header/header.component";
import { ActivityComponent } from "./activity/activity.component";
import { SuprPassSavingsListComponent } from "./savings/supr-pass-savings-list.component";
import { SuprPassSavingsHeaderComponent } from "./savings/supr-pass-savings-header.component";
import { SuprPassSavingsFiltersComponent } from "./savings/supr-pass-savings-filter.component";

export const SuprPassComponents = [
    TopBarComponent,
    HeaderComponent,
    FaqComponent,
    FooterComponent,
    ActivePlanComponent,
    ActivityComponent,
    ActiveAutoDebitInfoComponent,
    AutoDebitCancelModal,
    SuprPassSavingsHeaderComponent,
    SuprPassSavingsListComponent,
    SuprPassSavingsFiltersComponent,
];
