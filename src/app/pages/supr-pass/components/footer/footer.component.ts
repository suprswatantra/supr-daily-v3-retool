import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";

import { SuprPassPlan, Subscription } from "@shared/models";

import { ANALYTICS_OBJECT_NAME } from "../../constants";

import { TEXTS } from "../../constants";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-pass-footer",
    template: `
        <supr-sticky-wrapper>
            <div class="footerWrapper">
                <ng-container *ngIf="showSkipBtn">
                    <div class="suprRow">
                        <supr-button
                            class="skipBtn"
                            (handleClick)="handleSuprPassSkip.emit()"
                            saObjectName="${ANALYTICS_OBJECT_NAME.CLICK.SKIP}"
                        >
                            {{ skipBtnText }}
                        </supr-button>
                    </div>
                    <div class="divider16"></div>
                </ng-container>

                <ng-container
                    *ngIf="!isActive && !priorPurchaseId && selectedPlan"
                >
                    <div class="suprRow">
                        <supr-button
                            *ngIf="isPlanBlockVisible; else planNotVisible"
                            (handleClick)="planButtonClick()"
                            saObjectName="${ANALYTICS_OBJECT_NAME.CLICK.BUY}"
                            [saObjectValue]="selectedPlan?.id"
                        >
                            Buy {{ selectedPlan?.durationMonths }}
                            {{
                                selectedPlan?.durationMonths > 1
                                    ? "Months"
                                    : "Month"
                            }}
                            for
                            {{ selectedPlan?.unitPrice | rupee }}
                        </supr-button>
                        <ng-template #planNotVisible>
                            <supr-button
                                (handleClick)="planButtonClick()"
                                saObjectName="${ANALYTICS_OBJECT_NAME.CLICK
                                    .SEE_PLANS}"
                            >
                                {{ seePlansText }}
                            </supr-button>
                        </ng-template>
                    </div>
                </ng-container>

                <ng-container
                    *ngIf="(isActive || priorPurchaseId) && subscriptions"
                >
                    <div class="suprRow">
                        <supr-button
                            (handleClick)="handleRechargeBtnClick.emit()"
                            saObjectName="${ANALYTICS_OBJECT_NAME.CLICK
                                .RECHARGE}"
                        >
                            <div class="suprRow center">
                                <supr-icon name="recharge_sub"></supr-icon>
                                <div class="spacer8"></div>
                                <supr-text type="action">
                                    {{ rechargeBtnText }}
                                </supr-text>
                            </div>
                        </supr-button>
                    </div>
                </ng-container>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
    @Input() priorPurchaseId: number;
    @Input() isActive: boolean;
    @Input() selectedPlan: SuprPassPlan;
    @Input() showSkipBtn: boolean;
    @Input() isPlanBlockVisible: boolean;
    @Input() subscriptions: Subscription[];

    @Output() handleSuprPassBuy: EventEmitter<void> = new EventEmitter();
    @Output() handleSuprPassSkip: EventEmitter<void> = new EventEmitter();
    @Output() handleRechargeBtnClick: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchSubscription: EventEmitter<void> = new EventEmitter();
    @Output() handleSeePlans: EventEmitter<void> = new EventEmitter();

    skipBtnText = TEXTS.SKIP_BTN_TEXT;
    seePlansText = TEXTS.SEE_PLANS;
    rechargeBtnText = TEXTS.RECHARGE_BTN_TEXT;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    planButtonClick() {
        if (!this.isPlanBlockVisible) {
            this.handleSeePlans.emit();
            return;
        }

        this.handleSuprPassBuy.emit();
    }

    private initialize() {
        /* Fetch Subscriptions if the subscription list is empty.
        Required because Supr Access recharge page requires an active subscription. */

        if (!(this.isActive || this.priorPurchaseId)) {
            return;
        }

        const hasSubscriptions = this.utilService.isLengthyArray(
            this.subscriptions
        );

        if (hasSubscriptions) {
            return;
        }

        this.handleFetchSubscription.emit();
    }
}
