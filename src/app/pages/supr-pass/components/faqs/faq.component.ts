import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { FaqItem } from "@shared/models";

import { TEXTS, FAQ_ITEMS_TO_SHOW } from "../../constants";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-pass-faq",
    template: `
        <div class="faqsBlock" *ngIf="suprPassFaq && suprPassFaq.length > 0">
            <ng-container *ngIf="!hasHeader">
                <supr-text type="bold16" class="sectionTitle">
                    {{ sectionTitle }}
                </supr-text>
                <div class="divider16"></div>
            </ng-container>

            <div class="faqContentWrapper" [class.fullPage]="showFullList">
                <div
                    *ngFor="
                        let faq of suprPassFaq;
                        let i = index;
                        trackBy: trackByFn;
                        last as _last
                    "
                >
                    <ng-container *ngIf="showFullList; else showFewFaqs">
                        <supr-faq-item [faq]="faq"></supr-faq-item>
                        <div class="divider16" *ngIf="!_last"></div>
                    </ng-container>
                    <ng-template #showFewFaqs>
                        <ng-container *ngIf="i < ${FAQ_ITEMS_TO_SHOW}">
                            <supr-faq-item [faq]="faq"></supr-faq-item>
                            <div class="divider16"></div>
                        </ng-container>
                    </ng-template>
                </div>
                <ng-container
                    *ngIf="
                        !showFullList &&
                        suprPassFaq.length > ${FAQ_ITEMS_TO_SHOW}
                    "
                >
                    <div class="suprRow viewAllBlock" (click)="goToFaqPage()">
                        <supr-text type="paragraph">
                            {{ faqsViewAllText }}
                        </supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqComponent {
    @Input() suprPassFaq: FaqItem[] = [];
    @Input() showFullList: boolean;
    @Input() hasHeader: boolean;

    sectionTitle = TEXTS.FAQ_SECTION_TITLE;
    faqsViewAllText = TEXTS.VIEW_ALL_FAQS;

    constructor(private routerService: RouterService) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    goToFaqPage() {
        this.routerService.goToSuprPassFaqPage();
    }
}
