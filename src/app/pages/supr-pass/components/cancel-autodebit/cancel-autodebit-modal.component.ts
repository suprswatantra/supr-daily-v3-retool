import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
    OnInit,
} from "@angular/core";

import { MODAL_NAMES, SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { ModalService } from "@services/layout/modal.service";
import { SettingsService } from "@services/shared/settings.service";

import { ANALYTICS_OBJECT_NAME, TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-cancel-auto-debit-modal",
    template: `
        <ng-container *ngIf="showModal && (subTitleText || titleText)">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.AUTO_ADD_SUPR_PASS_PROMPT}"
            >
                <div class="wrapper">
                    <ng-container *ngIf="titleText">
                        <div class="suprRow">
                            <supr-text type="subtitle">
                                {{ titleText }}
                            </supr-text>
                        </div>
                        <div class="divider12"></div>
                    </ng-container>

                    <supr-text type="body" class="subtitle">
                        {{ subTitleText }}
                    </supr-text>
                    <div class="divider24"></div>

                    <div class="suprRow">
                        <div class="buttonWrapper">
                            <supr-button
                                class="remove"
                                (handleClick)="secondaryBtnClick()"
                                saObjectName="${ANALYTICS_OBJECT_NAME.CLICK
                                    .DONT_CANCEL_MANDATE}"
                            >
                                <supr-text type="body">
                                    {{ secondaryButtonText }}
                                </supr-text>
                            </supr-button>
                        </div>

                        <div class="buttonWrapper right">
                            <supr-button
                                saObjectName="${ANALYTICS_OBJECT_NAME.CLICK
                                    .CANCEL_MANDATE}"
                                (handleClick)="primaryBtnClick()"
                            >
                                <supr-text type="body">
                                    {{ primaryButtontext }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../../styles/cancel-autodebit-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutoDebitCancelModal implements OnInit, OnChanges {
    @Input() showModal: boolean;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();
    @Output() handleCancelSuprAccessAutoDebit: EventEmitter<
        void
    > = new EventEmitter();

    titleText: string;
    subTitleText: string;
    primaryButtontext = TEXTS.CANCEL_MANDATE;
    secondaryButtonText = TEXTS.DONT_CANCEL;

    constructor(
        private modalService: ModalService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["showModal"];
        this.handleChange(change);
    }

    hideModal() {
        this.modalService.closeModal();
        this.handleHideModal.emit();
    }

    secondaryBtnClick() {
        this.hideModal();
    }

    primaryBtnClick() {
        this.handleCancelSuprAccessAutoDebit.emit();
        this.hideModal();
    }

    private handleChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        const modalTexts = this.settingsService.getSettingsValue(
            SETTINGS.CANCEL_AUTO_DEBIT_MODAL_DATA,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.CANCEL_AUTO_DEBIT_MODAL_DATA]
        );
        this.titleText = modalTexts.title;
        this.subTitleText = modalTexts.subtitle;
    }
}
