import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SAVINGS_FILTERS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-pass-savings-filters",
    template: `
        <div class="suprRow savingsFilters spaceBetween">
            <div class="savingsFiltersItem" *ngFor="let filter of filters">
                <supr-button
                    [class.active]="filter.key === activeTab"
                    (handleClick)="handleTabClick.emit(filter.key)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.SAVINGS_FILTER}"
                    [saObjectValue]="filter.key"
                >
                    <supr-text type="body">
                        {{ filter.label }}
                    </supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSavingsFiltersComponent {
    @Input() activeTab: string;
    @Output() handleTabClick: EventEmitter<string> = new EventEmitter();

    filters = SAVINGS_FILTERS;
}
