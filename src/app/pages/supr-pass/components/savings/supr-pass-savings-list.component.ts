import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { ActivityBlock } from "@models";
import { DateService } from "@services/date/date.service";

import { SAVINGS_FILTERS, SavingsFilters } from "./../../constants/index";

@Component({
    selector: "supr-pass-savings-list",
    template: `
        <ng-container
            *ngIf="suprPassSavingsList && suprPassSavingsList?.length"
        >
            <div class="divider16"></div>
            <supr-pass-savings-filters
                [activeTab]="activeTab"
                (handleTabClick)="onTabClick($event)"
            ></supr-pass-savings-filters>
            <div class="divider20"></div>

            <ng-container *ngIf="filteredSavings && filteredSavings?.length">
                <div
                    *ngFor="let activity of filteredSavings; trackBy: trackByFn"
                >
                    <supr-activity-block
                        [activityItem]="activity"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </ng-container>
        </ng-container>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSavingsListComponent implements OnChanges {
    @Input() suprPassSavingsList: ActivityBlock[] = [];
    @Input() selectedTabName: SavingsFilters;

    activeTab = SAVINGS_FILTERS[0].key;
    filteredSavings: ActivityBlock[] = [];

    constructor(private dateService: DateService) {}

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["suprPassSavingsList"];

        if (!change || change.firstChange) {
            return;
        }

        if (
            this.selectedTabName &&
            (this.selectedTabName === SAVINGS_FILTERS[0].key ||
                this.selectedTabName === SAVINGS_FILTERS[1].key)
        ) {
            this.activeTab = this.selectedTabName;
        }

        if (change.currentValue) {
            this.setFilteredList();
        }
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    onTabClick(selectedTab: SavingsFilters) {
        if (selectedTab === this.activeTab) {
            return;
        }
        this.activeTab = selectedTab;
        this.setFilteredList();
    }

    // Filters out 30 days txns
    private setFilteredList() {
        const savingsList = [...this.suprPassSavingsList];

        if (this.activeTab === SavingsFilters.LIFETIME) {
            this.filteredSavings = savingsList;
            return;
        }

        const todayDate = this.dateService.getTodayDate();

        this.filteredSavings = savingsList.filter((item) => {
            if (!item || !item.date) {
                return false;
            }
            const savingsDate = this.dateService.dateFromText(item.date);

            return (
                this.dateService.daysBetweenTwoDates(savingsDate, todayDate) <=
                30
            );
        });
    }
}
