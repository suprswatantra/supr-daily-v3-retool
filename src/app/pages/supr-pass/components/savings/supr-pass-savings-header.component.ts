import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-savings-header",
    template: `
        <div class="savingsHeader">
            <div class="suprRow">
                <div
                    class="savingsBlock"
                    *ngIf="monthlySavings || monthlySavings === 0"
                >
                    <supr-text type="body">
                        {{ TEXTS.MONTHLY_SAVINGS }}
                    </supr-text>
                    <supr-text type="heading">
                        {{ monthlySavings | rupee }}
                    </supr-text>
                </div>
                <div
                    class="savingsBlock"
                    *ngIf="totalSavings || totalSavings === 0"
                >
                    <supr-text type="body">
                        {{ TEXTS.LIFETIME_SAVINGS }}
                    </supr-text>
                    <supr-text type="heading">
                        {{ totalSavings | rupee }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSavingsHeaderComponent {
    @Input() totalSavings: number;
    @Input() monthlySavings: number;

    TEXTS = TEXTS;
}
