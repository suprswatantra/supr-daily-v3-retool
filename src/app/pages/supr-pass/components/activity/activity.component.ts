import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { ActivityBlock } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { TEXTS, ACTIVITY_PAGE_URL } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-pass-activity",
    template: `
        <div
            class="activityBlock"
            *ngIf="suprPassActivity && suprPassActivity.length > 0"
            [class.activityBlockPadding]="showFullList"
        >
            <ng-container *ngIf="showFullList; else singleActivity">
                <div
                    *ngFor="
                        let activity of suprPassActivity;
                        trackBy: trackByFn
                    "
                >
                    <supr-activity-block
                        [activityItem]="activity"
                    ></supr-activity-block>
                    <div class="divider16"></div>
                </div>
            </ng-container>
            <ng-template #singleActivity>
                <supr-activity-block
                    [showAllActivityCta]="showAllActivityCta"
                    [activityItem]="singleActivityBlock"
                    [saClickViewAllObjectName]="saClickViewAllObjectName"
                    [activityFullPageUrl]="activityPageUrl"
                    [viewAllText]="viewAllText"
                ></supr-activity-block>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityComponent implements OnInit {
    @Input() suprPassActivity: ActivityBlock[] = [];
    @Input() showFullList: boolean;

    viewAllText = TEXTS.VIEW_ALL_ACTIVITY;
    saClickViewAllObjectName = SA_OBJECT_NAMES.CLICK.VIEW_ALL_ACTIVITY;
    singleActivityBlock: ActivityBlock;
    showAllActivityCta: boolean;
    activityPageUrl = ACTIVITY_PAGE_URL;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initialize();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private initialize() {
        if (this.utilService.isLengthyArray(this.suprPassActivity)) {
            this.singleActivityBlock = this.suprPassActivity[0];

            if (this.suprPassActivity.length === 1) {
                this.showAllActivityCta = false;
                return;
            }

            this.showAllActivityCta = true;
        }
    }
}
