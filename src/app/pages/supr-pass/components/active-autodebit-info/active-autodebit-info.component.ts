import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    OnInit,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { ActiveAutoDebitInfo, BannerContent } from "@shared/models";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-active-autodebit-info",
    template: `
        <div
            class="activeAutoDebitInfo"
            *ngIf="activeAutoDebitData && activeAutoDebitBannerContent"
        >
            <supr-banner-content
                [last]="true"
                [first]="true"
                [content]="activeAutoDebitBannerContent"
            ></supr-banner-content>
            <div class="divider12"></div>
            <div class="suprRow spaceBetween rowReverse">
                <div
                    class="suprRow changeAutoDebit"
                    (click)="handleCancelClick()"
                >
                    <supr-text type="paragraph">
                        {{ cancelAutoDebitText }}
                    </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </div>
            <ng-container *ngIf="showCancelModal">
                <supr-pass-cancel-auto-debit-modal
                    [showModal]="showCancelModal"
                    (handleHideModal)="hideCancelModal()"
                    (handleCancelSuprAccessAutoDebit)="cancelAutoDebit()"
                >
                </supr-pass-cancel-auto-debit-modal>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActiveAutoDebitInfoComponent implements OnInit, OnChanges {
    @Input() activeAutoDebitData: ActiveAutoDebitInfo;

    @Output() handleCancelAutoDebit: EventEmitter<void> = new EventEmitter();

    activeAutoDebitBannerContent: BannerContent;
    cancelAutoDebitText = TEXTS.CANCEL;
    showCancelModal = false;

    ngOnInit() {
        if (this.activeAutoDebitData) {
            this.initialize();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["activeAutoDebitData"];

        if (change && !change.firstChange && change.currentValue) {
            this.initialize();
        }
    }

    handleCancelClick() {
        this.showCancelModal = true;
    }

    hideCancelModal() {
        this.showCancelModal = false;
    }

    cancelAutoDebit() {
        this.handleCancelAutoDebit.emit();
        this.hideCancelModal();
    }

    private initialize() {
        this.activeAutoDebitBannerContent = this.activeAutoDebitData.content;
    }
}
