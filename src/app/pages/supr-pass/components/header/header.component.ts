import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    OnInit,
    Input,
    Output,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { STYLE_LIST } from "@pages/supr-pass/constants";

import { SuprPassInfoHeader, SuprPassActivePlanDetails } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-header",
    template: `
        <div class="suprPassHeaderContainer" #headerWrapper>
            <div
                class="suprPassHeader"
                [class.lowBottomPadding]="isActive || priorPurchaseId"
            >
                <div class="divider20"></div>
                <div class="imgWrapper">
                    <img [src]="headerData?.logo || defaultImgUrl" />
                </div>
                <ng-container *ngIf="headerData?.title?.text">
                    <div class="divider12"></div>
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.title"
                            class="boldText"
                        ></supr-text-fragment>
                    </div>
                    <ng-container *ngIf="isUserOnAutoDebit">
                        <div class="divider8"></div>
                        <div
                            class="suprRow center viewDetails"
                            (click)="handleViewMembershipDetailsClick.emit()"
                        >
                            <supr-text type="paragraph">{{
                                texts.VIEW_DETAILS
                            }}</supr-text>
                            <div class="spacer2"></div>
                            <supr-icon name="chevron_right"></supr-icon>
                        </div>
                    </ng-container>
                </ng-container>
                <ng-container *ngIf="headerData?.subTitle?.text">
                    <div class="divider8"></div>
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.subTitle"
                            class="semiBoldText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container *ngIf="headerData?.infoText?.text">
                    <div class="divider4"></div>
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="headerData?.infoText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container
                    *ngIf="(isActive || priorPurchaseId) && !isUserOnAutoDebit"
                >
                    <ng-container *ngIf="isActive">
                        <supr-pass-active-plan-info
                            [plan]="activePlanDetails"
                        ></supr-pass-active-plan-info>
                    </ng-container>
                    <ng-container *ngIf="headerData?.warningText?.text">
                        <div class="divider16"></div>
                        <div class="suprRow center">
                            <supr-icon
                                name="error"
                                class="warningInfo"
                            ></supr-icon>
                            <div class="spacer4"></div>
                            <supr-text-fragment
                                [textFragment]="headerData?.warningText"
                            ></supr-text-fragment>
                        </div>
                    </ng-container>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    @Input() headerData: SuprPassInfoHeader;
    @Input() isActive: boolean;
    @Input() activePlanDetails: SuprPassActivePlanDetails;
    @Input() priorPurchaseId: number;
    @Input() isUserOnAutoDebit: boolean;

    @Output() handleViewMembershipDetailsClick: EventEmitter<
        void
    > = new EventEmitter();

    @ViewChild("headerWrapper", { static: true }) wrapperEl: ElementRef;

    defaultImgUrl = "assets/images/app/supr-pass.svg";
    texts = TEXTS;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.headerData) {
            return;
        }

        this.utilService.setStyles(this.headerData, STYLE_LIST, this.wrapperEl);
    }
}
