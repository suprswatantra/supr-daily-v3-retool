import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-top-bar",
    template: `
        <supr-page-header>
            <supr-text type="body" class="pageHeader">
                {{ headerText }}
            </supr-text>
        </supr-page-header>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent implements OnInit, OnChanges {
    @Input() isActive: boolean;
    @Input() priorPurchaseId: number;
    @Input() isAutoDebitActive: boolean;

    headerText: string = TEXTS.HEADER_TEXT;

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (
            this.canHandleChange(changes["isActive"]) ||
            this.canHandleChange(changes["priorPurchaseId"]) ||
            this.canHandleChange(changes["isAutoDebitActive"])
        ) {
            this.initialize();
        }
    }

    private initialize() {
        if (
            (this.isActive || this.priorPurchaseId) &&
            !this.isAutoDebitActive
        ) {
            this.headerText = TEXTS.RECHARGE_HEADER_TEXT;
            return;
        }

        this.headerText = TEXTS.HEADER_TEXT;
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
