import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SuprPassActivePlanDetails } from "@shared/models";

import { TEXTS } from "../../constants";

@Component({
    selector: "supr-pass-active-plan-info",
    template: `
        <div class="activePlanBlock" *ngIf="plan">
            <div class="divider8"></div>
            <div class="suprRow center">
                <div class="suprColumn right">
                    <supr-text type="paragraph" class="activePlanInfoLabel">
                        {{ daysLeftText }}
                    </supr-text>
                    <supr-text type="action14" class="activePlanInfoValue">
                        {{ plan?.daysLeft }}
                        {{ plan?.daysLeft > 1 ? "Days" : "Day" }}
                    </supr-text>
                </div>
                <div class="separator"></div>
                <div class="suprColumn left">
                    <supr-text type="paragraph" class="activePlanInfoLabel">
                        {{ validTillText }}
                    </supr-text>
                    <supr-text type="action14" class="activePlanInfoValue">
                        {{ plan?.endDate | date: "d LLL, yyy" }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivePlanComponent {
    @Input() plan: SuprPassActivePlanDetails;

    daysLeftText = TEXTS.DAYS_LEFT;
    validTillText = TEXTS.VALID_TILL;
}
