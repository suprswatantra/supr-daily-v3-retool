import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";

import { SuprPassSavingsInfo } from "@models";

import { TEXTS, SavingsFilters } from "../constants";

@Component({
    selector: "supr-pass-savings-layout",
    template: `
        <div class="suprContainer savingsScreen">
            <supr-page-header>
                <supr-text type="subheading" class="savingsScreenTitle">
                    ${TEXTS.SAVINGS_TITLE}
                    {{ selectedTabName }}
                </supr-text>
            </supr-page-header>
            <ng-container *ngIf="isFetchingSuprPassSavingsInfo; else data">
                <div class="loaderContainer suprColumn center">
                    <supr-loader></supr-loader>
                </div>
            </ng-container>
            <ng-template #data>
                <div class="suprScrollContent">
                    <supr-pass-savings-header
                        [totalSavings]="suprPassSavings?.totalSavings"
                        [monthlySavings]="suprPassSavings?.monthlySavings"
                    ></supr-pass-savings-header>
                    <supr-pass-savings-list
                        [suprPassSavingsList]="suprPassSavings?.savingsList"
                        [selectedTabName]="selectedTabName"
                    ></supr-pass-savings-list>
                </div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSavingsLayoutComponent implements OnInit {
    @Input() suprPassSavings: SuprPassSavingsInfo;
    @Input() selectedTabName: SavingsFilters;
    @Input() isFetchingSuprPassSavingsInfo: boolean;

    @Output() handleFetchSuprPassSavingsInfo: EventEmitter<
        void
    > = new EventEmitter();

    ngOnInit() {
        this.handleFetchSuprPassSavingsInfo.emit();
    }
}
