import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { TEXTS } from "../constants";

import { FaqItem } from "@models";

@Component({
    selector: "supr-pass-faq-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.FAQ_SECTION_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-pass-faq
                    [suprPassFaq]="faqs"
                    [showFullList]="true"
                    [hasHeader]="true"
                ></supr-pass-faq>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassFaqLayoutComponent {
    @Input() faqs: FaqItem[];
}
