import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    Output,
    SimpleChange,
    SimpleChanges,
    EventEmitter,
} from "@angular/core";

import { CART_ITEM_TYPES, SETTINGS } from "@constants";

import {
    SuprPassPlan,
    SuprPassInfo,
    CartItem,
    AutoCheckoutQueryParams,
    CartMeta,
    Subscription,
    ImageBannerContent,
    BenefitsBannerCtaAction,
    AutoDebitPaymentMode,
    AutoDebitPlan,
} from "@shared/models";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";
import { TEXTS, ACTION_TYPES } from "../constants";

import { CartService } from "@services/shared/cart.service";
import { CalendarService } from "@services/date/calendar.service";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { NudgeService } from "@services/layout/nudge.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import {
    ANALYTICS_OBJECT_NAME,
    ANALYTICS_OBJECT_VALUE,
} from "../constants/index";
import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-pass-layout",
    template: `
        <div class="suprContainer">
            <!-- Top Bar -->
            <supr-pass-top-bar
                [isActive]="isSuprPassActive"
                [priorPurchaseId]="expiredSuprPassPurchaseId"
                [isAutoDebitActive]="suprPassInfo?.autoDebitData?.isActive"
            ></supr-pass-top-bar>

            <div
                class="suprScrollContent suprPassInfoWrapper"
                [class.hasSkipBlock]="showSkipBtn"
                (scroll)="onScroll()"
            >
                <ng-container *ngIf="isFetchingSuprPassInfo; else content">
                    <div class="loaderContainer suprColumn center">
                        <supr-loader></supr-loader>
                    </div>
                </ng-container>
                <ng-template #content>
                    <ng-container *ngIf="suprPassInfo; else error">
                        <!-- Header -->
                        <ng-container *ngIf="suprPassInfo?.header">
                            <supr-pass-header
                                [headerData]="suprPassInfo?.header"
                                [isActive]="isSuprPassActive"
                                [activePlanDetails]="
                                    suprPassInfo?.activePlanDetails
                                "
                                [priorPurchaseId]="expiredSuprPassPurchaseId"
                                [isUserOnAutoDebit]="
                                    suprPassInfo?.autoDebitData?.isActive
                                "
                                (handleViewMembershipDetailsClick)="
                                    scrollToMembershipDetails()
                                "
                            ></supr-pass-header>
                        </ng-container>

                        <!-- Alert Banner -->
                        <ng-container *ngIf="suprPassInfo?.alertBanner">
                            <div class="divider28"></div>
                            <supr-banner
                                [banner]="suprPassInfo?.alertBanner"
                            ></supr-banner>
                        </ng-container>

                        <!-- Image Banner -->
                        <ng-container *ngIf="suprPassInfo?.imgBanner">
                            <div class="divider28"></div>
                            <supr-img-banner
                                [imgBanner]="suprPassInfo?.imgBanner"
                                (handleImgBannerClick)="
                                    handleImgBannerClick($event)
                                "
                            ></supr-img-banner>
                        </ng-container>

                        <!-- Auto debit alert box -->
                        <ng-container
                            *ngIf="
                                isSuprPassActive &&
                                isAutoDebitEnabled &&
                                suprPassInfo?.autoDebitAlert
                            "
                        >
                            <div class="divider28"></div>
                            <supr-img-banner
                                [imgBanner]="suprPassInfo?.autoDebitAlert"
                                (handleImgBannerClick)="
                                    handleImgBannerClick($event)
                                "
                                saClick
                                saImpression
                                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                    .SUPR_PASS_AUTO_DEBIT_BANNER}"
                            ></supr-img-banner>
                        </ng-container>

                        <!-- Active Member Benefits -->
                        <ng-container
                            *ngIf="
                                suprPassInfo?.benefits &&
                                (isSuprPassActive || expiredSuprPassPurchaseId)
                            "
                        >
                            <div class="divider28"></div>
                            <supr-benefits-banner
                                [benefitsBanner]="suprPassInfo?.benefits"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick($event)
                                "
                            ></supr-benefits-banner>
                        </ng-container>

                        <!-- Activity -->
                        <ng-container *ngIf="suprPassInfo?.activity">
                            <div id="suprPassMemberActivityWrapper">
                                <div class="divider28"></div>
                                <supr-text type="bold16" class="sectionTitle">
                                    {{ activitySectionTitle }}
                                </supr-text>
                                <div class="divider16"></div>
                                <ng-container
                                    *ngIf="
                                        suprPassInfo?.autoDebitData?.isActive &&
                                        suprPassInfo?.autoDebitData
                                            ?.activeDataBlock
                                    "
                                >
                                    <supr-pass-active-autodebit-info
                                        [activeAutoDebitData]="
                                            suprPassInfo?.autoDebitData
                                                ?.activeDataBlock
                                        "
                                        (handleCancelAutoDebit)="
                                            handleCancelAutoDebit()
                                        "
                                    ></supr-pass-active-autodebit-info>
                                    <div class="divider16"></div>
                                </ng-container>
                                <supr-pass-activity
                                    [suprPassActivity]="suprPassInfo?.activity"
                                ></supr-pass-activity>
                            </div>
                        </ng-container>

                        <!-- Banner -->
                        <ng-container *ngIf="suprPassInfo?.banner">
                            <div class="divider28"></div>
                            <supr-banner
                                [banner]="suprPassInfo?.banner"
                            ></supr-banner>
                        </ng-container>

                        <!-- Inctive Member Benefits -->
                        <ng-container
                            *ngIf="
                                suprPassInfo?.benefits &&
                                !(isSuprPassActive || expiredSuprPassPurchaseId)
                            "
                        >
                            <div class="divider28"></div>
                            <supr-benefits-banner
                                [benefitsBanner]="suprPassInfo?.benefits"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick($event)
                                "
                            ></supr-benefits-banner>
                        </ng-container>

                        <!-- Plans -->
                        <ng-container
                            *ngIf="
                                !isSuprPassActive && !expiredSuprPassPurchaseId
                            "
                        >
                            <div id="suprPassPlansWrapper">
                                <supr-pass-plans
                                    [suprPassPlans]="suprPassInfo?.plans"
                                    [planSelectionText]="
                                        suprPassInfo?.planSelectionText
                                    "
                                    [selectedPlan]="selectedPlan"
                                    (handlePlanSelect)="
                                        handlePlanSelect($event)
                                    "
                                ></supr-pass-plans>
                            </div>
                        </ng-container>

                        <!-- FAQs -->
                        <ng-container *ngIf="suprPassInfo?.faqs">
                            <div class="divider28"></div>
                            <supr-pass-faq
                                [suprPassFaq]="suprPassInfo?.faqs"
                            ></supr-pass-faq>
                        </ng-container>
                    </ng-container>

                    <!-- Error -->
                    <ng-template #error></ng-template>
                </ng-template>
            </div>

            <ng-container *ngIf="showAutoDebitPaymentScreen">
                <supr-auto-debit-payment-container
                    [texts]="suprPassInfo?.autoDebitData?.texts"
                    [startPaymentProcess]="showAutoDebitPaymentScreen"
                    [selectedSuprPassPlan]="selectedPlan"
                    [selectedAutoDebitPlan]="selectedAutoDebitPlan"
                    [isActiveSuprPassMember]="isSuprPassActive"
                    (cancelPaymentProcess)="toggleAutoDebitPaymentScreen(false)"
                    (handlePaymentSuccess)="handleAutoDebitPaymentSuccess()"
                    (handlePaymentFailure)="handleAutoDebitPaymentFailure()"
                    (proceedToCart)="goToCartPage()"
                ></supr-auto-debit-payment-container>
            </ng-container>

            <!-- Footer -->
            <ng-container *ngIf="!suprPassInfo?.autoDebitData?.isActive">
                <supr-pass-footer
                    [isActive]="isSuprPassActive"
                    [priorPurchaseId]="expiredSuprPassPurchaseId"
                    [isPlanBlockVisible]="isPlanBlockVisible"
                    [selectedPlan]="selectedPlan"
                    [showSkipBtn]="showSkipBtn"
                    [subscriptions]="subscriptions"
                    (handleSuprPassBuy)="handleSuprPassBuy()"
                    (handleSuprPassSkip)="handleSkipBtnClick()"
                    (handleRechargeBtnClick)="handleRechargeBtnClick()"
                    (handleFetchSubscription)="handleFetchSubscription.emit()"
                    (handleSeePlans)="scrollToPlans()"
                ></supr-pass-footer>
            </ng-container>

            <!-- Loaders -->
            <supr-loader-overlay *ngIf="isCancelingSuprPassAutoDebit">
            </supr-loader-overlay>

            <!-- Nudge -->
            <supr-auto-debit-payment-failure-nudge></supr-auto-debit-payment-failure-nudge>
        </div>
    `,
    styleUrls: ["../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassLayout implements OnInit, OnChanges {
    @Input() suprPassInfo: SuprPassInfo;
    @Input() autoCheckoutInfo: AutoCheckoutQueryParams;
    @Input() isFetchingSuprPassInfo: boolean;
    @Input() isCancelingSuprPassAutoDebit: boolean;
    @Input() cartError: any;
    @Input() cartCurrentState: CartCurrentState;
    @Input() cartMeta: CartMeta;
    @Input() cartItems: CartItem[];
    @Input() subscriptions: Subscription[];
    @Input() isPlanBlockVisible: boolean;
    @Input() autoDebitPaymentModes: AutoDebitPaymentMode[];

    @Output() handleFetchSuprPassInfo: EventEmitter<void> = new EventEmitter();
    @Output() handleAddSuprPass: EventEmitter<CartItem> = new EventEmitter();
    @Output() handleFetchSubscription: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchAutoDebitPaymentModes: EventEmitter<
        void
    > = new EventEmitter();
    @Output() handleCancelSuprPassAutoDebit: EventEmitter<
        void
    > = new EventEmitter();

    selectedPlan: SuprPassPlan;
    hasError = false;
    isSuprPassActive = false;
    expiredSuprPassPurchaseId: number;
    _groupWrapper: any;
    showSkipBtn = false;
    amountToAdd: number;
    defaultImgUrl = "assets/images/app/supr-pass.svg";
    activitySectionTitle = TEXTS.ACTIVITY_TITLE;
    selectedAutoDebitPlan: AutoDebitPlan = null;
    isAutoDebitEnabled = true;
    showAutoDebitPaymentScreen = false;

    constructor(
        private calendarService: CalendarService,
        private cartService: CartService,
        private routerService: RouterService,
        private utilService: UtilService,
        private settingsService: SettingsService,
        private nudgeService: NudgeService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.setCheckoutInfo();

        this.isAutoDebitEnabled = this.settingsService.getSettingsValue(
            SETTINGS.SHOW_SUPR_PASS_AUTO_DEBIT,
            true
        );

        if (!this.autoDebitPaymentModes) {
            this.handleFetchAutoDebitPaymentModes.emit();
        }

        if (!this.utilService.isEmpty(this.suprPassInfo)) {
            this.setLocalInfo();
            this.setDefaultActivePlan();
            this.setAnalyticsData();
        } else {
            this.handleFetchSuprPassInfo.emit();
        }

        this.nudgeService.sendAutoDebitPaymentFailureNudge();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["suprPassInfo"];

        if (this.canHandleChange(change)) {
            this.setLocalInfo();
            this.setDefaultActivePlan();
            this.setAnalyticsData();
        }

        if (this.canHandleChange(changes["autoCheckoutInfo"])) {
            this.setCheckoutInfo();
            this.setAnalyticsData();
        }
    }

    handleSuprPassBuy() {
        const hasActiveAutoDebit: boolean = this.utilService.getNestedValue(
            this.suprPassInfo,
            "autoDebitData.isActive",
            false
        );

        if (
            !hasActiveAutoDebit &&
            this.isAutoDebitEnabled &&
            this.selectedAutoDebitPlan
        ) {
            this.toggleAutoDebitPaymentScreen(true);
            return;
        }

        this.goToCartPage();
    }

    goToCartPage() {
        const suprPassCartItem = {
            type: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
            recharge_quantity: this.selectedPlan.durationDays,
            quantity: 1.0,
            sku_id: this.suprPassInfo.skuId,
            frequency: {
                mon: true,
                tue: true,
                wed: true,
                thu: true,
                fri: true,
                sat: true,
                sun: true,
            },
            plan_id: this.selectedPlan.id,
            start_date: this.calendarService.getSuprPassStartDate(),
        };

        this.handleAddSuprPass.emit(suprPassCartItem);
        setTimeout(() => {
            this.routerService.goToCartPage();
        }, 100);
    }

    handlePlanSelect(plan: SuprPassPlan) {
        this.selectedPlan = plan;
        this.setAutoDebitPlan();
    }

    handleSkipBtnClick() {
        this.cartService.setAutoCheckoutFlag();
        const queryParams = {
            fromSuprPassPage: true,
        };
        this.routerService.goToCartPage({ queryParams });
    }

    handleRechargeBtnClick() {
        let suprPassSubscriptionId: number;

        if (this.isSuprPassActive) {
            suprPassSubscriptionId = this.utilService.getNestedValue(
                this.suprPassInfo,
                "activePlanDetails.purchaseId"
            );
        } else {
            suprPassSubscriptionId = this.expiredSuprPassPurchaseId;
        }

        if (!suprPassSubscriptionId) {
            return;
        }

        this.goToSubscriptionRechargePage(suprPassSubscriptionId);
    }

    handleBenefitCtaClick(click: BenefitsBannerCtaAction) {
        const action = this.utilService.getNestedValue(click, "action", null);
        const appUrl = this.utilService.getNestedValue(click, "appUrl", null);
        const outsideUrl = this.utilService.getNestedValue(
            click,
            "outsideUrl",
            null
        );

        if (!action) {
            this.noActionCtaClick();
            return;
        }

        switch (action) {
            case ACTION_TYPES.ROUTE:
                if (appUrl) {
                    this.routerService.goToUrl(click.appUrl);
                    this.trackBenefitsCtaClick(appUrl);
                    return;
                }

                if (outsideUrl) {
                    this.routerService.openExternalUrl(click.outsideUrl);
                    this.trackBenefitsCtaClick(outsideUrl);
                    return;
                }
                return;

            default:
                this.noActionCtaClick();
                this.trackBenefitsCtaClick();
                return;
        }
    }

    trackBenefitsCtaClick(context: string = "") {
        this.analyticsService.trackClick({
            objectName: SA_OBJECT_NAMES.CLICK.BENEFITS_CTA,
            context: context,
        });
    }

    noActionCtaClick() {
        if (this.expiredSuprPassPurchaseId || this.isSuprPassActive) {
            this.handleRechargeBtnClick();
            return;
        }

        this.scrollToPlans();
    }

    scrollToMembershipDetails() {
        const membershipBlock = document.getElementById(
            "suprPassMemberActivityWrapper"
        );

        if (membershipBlock) {
            membershipBlock.scrollIntoView();
        }
    }

    scrollToPlans() {
        setTimeout(() => {
            if (!this._groupWrapper) {
                this._groupWrapper = document.getElementById(
                    "suprPassPlansWrapper"
                );
            }

            if (this._groupWrapper) {
                this._groupWrapper.scrollIntoView();
                this.isPlanBlockVisible = true;
            }
        }, 100);
    }

    onScroll() {
        if (this.isSuprPassActive || this.expiredSuprPassPurchaseId) {
            return;
        }

        if (!this._groupWrapper) {
            this._groupWrapper = document.getElementById(
                "suprPassPlansWrapper"
            );
        }

        if (!this._groupWrapper) {
            return;
        }

        this.isPlanBlockVisible = this.utilService.isElementInViewport(
            this._groupWrapper
        );
    }

    setAutoDebitPlan() {
        const autoDebitPlans: AutoDebitPlan[] = this.utilService.getNestedValue(
            this.suprPassInfo,
            "autoDebitData.autoDebitPlans",
            []
        );
        const { id: planId } = this.selectedPlan || { id: null };
        if (autoDebitPlans && autoDebitPlans.length && planId) {
            this.selectedAutoDebitPlan = autoDebitPlans.find(
                (item) => item.suprPassPlanId === planId
            );
        } else {
            this.selectedAutoDebitPlan = null;
        }
    }

    toggleAutoDebitPaymentScreen(show: boolean) {
        this.showAutoDebitPaymentScreen = show;
    }

    handleAutoDebitPaymentSuccess() {
        this.handleFetchSuprPassInfo.emit();
        this.toggleAutoDebitPaymentScreen(false);
        // If user came from cart page then take him back there
        if (this.showSkipBtn) {
            const queryParams = {
                fromSuprPassPage: true,
            };
            this.routerService.goToCartPage({ queryParams });
        }
    }

    handleAutoDebitPaymentFailure() {
        this.toggleAutoDebitPaymentScreen(false);
        if (this.showSkipBtn) {
            const queryParams = {
                fromSuprPassPage: true,
            };
            this.routerService.goToCartPage({ queryParams });
        }
    }

    handleCancelAutoDebit() {
        this.handleCancelSuprPassAutoDebit.emit();
    }

    handleImgBannerClick(bannerImage: ImageBannerContent) {
        if (bannerImage && bannerImage.action === "route") {
            if (bannerImage.appUrl) {
                this.routerService.goToUrl(bannerImage.appUrl);
            }
            if (bannerImage.outsideUrl) {
                this.routerService.openExternalUrl(bannerImage.outsideUrl);
            }
        }
    }

    private setLocalInfo() {
        this.isSuprPassActive = this.utilService.getNestedValue(
            this.suprPassInfo,
            "isActive"
        );
        this.expiredSuprPassPurchaseId = this.utilService.getNestedValue(
            this.suprPassInfo,
            "expiredSuprPassData.purchaseId"
        );
    }

    private goToSubscriptionRechargePage(subId: number) {
        this.delayOpen(() =>
            this.routerService.goToSubscriptionRechargePage(subId)
        );
    }

    private delayOpen(fn: () => void) {
        setTimeout(fn, 100);
    }

    private setAnalyticsData() {
        const hasAutoCheckoutInfo = !this.utilService.isEmpty(
            this.autoCheckoutInfo
        );

        setTimeout(() => {
            this.analyticsService.trackImpression({
                objectName:
                    ANALYTICS_OBJECT_NAME.IMPRESSION.SUPR_PASS_SCREEN_VIEW,
                objectValue:
                    this.suprPassInfo && this.suprPassInfo.isActive
                        ? ANALYTICS_OBJECT_VALUE.MEMBER
                        : ANALYTICS_OBJECT_VALUE.NON_MEMBER,
                context: this.utilService.getNestedValue(
                    this.suprPassInfo,
                    "autoDebitData.isActive",
                    false
                ),
                context_1_name: ANALYTICS_OBJECT_NAME.CONTEXT.CART_PROMPT,
                context_1_value: String(hasAutoCheckoutInfo),
                context_2_name:
                    ANALYTICS_OBJECT_NAME.CONTEXT.EXPIRED_SUPR_PASS_MEMBER,
                context_2_value: this.expiredSuprPassPurchaseId
                    ? "true"
                    : "false",
            });
        }, 1000);
    }

    private setCheckoutInfo() {
        const showSkip = this.utilService.getNestedValue(
            this.autoCheckoutInfo,
            "auto",
            false
        );

        this.showSkipBtn = !!showSkip;
    }

    private setDefaultActivePlan() {
        if (!this.utilService.isEmpty(this.suprPassInfo)) {
            const { plans = [] } = this.suprPassInfo;

            const suprPassObj = this.cartItems.find((obj) => {
                return obj && obj.hasOwnProperty("plan_id");
            });

            if (suprPassObj && typeof suprPassObj.plan_id === "number") {
                this.selectedPlan = plans.find(
                    (plan: SuprPassPlan) => plan.id === suprPassObj.plan_id
                );
                this.setAutoDebitPlan();
                return;
            }

            this.selectedPlan = plans.find(
                (plan: SuprPassPlan) => plan.isRecommended
            );
            this.setAutoDebitPlan();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
