import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { SuprPassInfo, ActivityBlock } from "@models";

import { UtilService } from "@services/util/util.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import {
    ANALYTICS_OBJECT_NAME,
    ANALYTICS_OBJECT_VALUE,
    TEXTS,
} from "../constants";

@Component({
    selector: "supr-pass-activity-layout",
    template: `
        <div class="suprContainer">
            <supr-page-header>
                <supr-text type="subheading">
                    ${TEXTS.ACTIVITY_TITLE}
                </supr-text>
            </supr-page-header>
            <div class="suprScrollContent">
                <supr-pass-activity
                    [suprPassActivity]="activity"
                    [showFullList]="true"
                ></supr-pass-activity>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-pass.page.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassActivityLayoutComponent implements OnInit {
    @Input() suprPassInfo: SuprPassInfo;
    @Input() activity: ActivityBlock[];

    constructor(
        private utilService: UtilService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.sendAnalyticsData();
    }

    private sendAnalyticsData() {
        if (this.suprPassInfo) {
            this.analyticsService.trackImpression({
                objectName:
                    ANALYTICS_OBJECT_NAME.IMPRESSION.MEMBER_DETAILS_VIEW,
                objectValue:
                    this.suprPassInfo && this.suprPassInfo.isActive
                        ? ANALYTICS_OBJECT_VALUE.MEMBER
                        : ANALYTICS_OBJECT_VALUE.NON_MEMBER,
                context: this.utilService.getNestedValue(
                    this.suprPassInfo,
                    "autoDebitData.isActive",
                    false
                ),
            });
        }
    }
}
