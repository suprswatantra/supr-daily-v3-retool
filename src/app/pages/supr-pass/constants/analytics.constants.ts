export const SA_OBJECT_NAMES = {
    IMPRESSION: {
        EARLIEST_DELIVERY_DATE: "earliest-delivery-date",
        SUPR_PASS_AUTO_DEBIT_BANNER: "auto-debit-banner",
    },
    CLICK: {
        VIEW_ALL_FAQ: "supr-access-view-all-faq",
        VIEW_ALL_ACTIVITY: "supr-access-view-all-activity",
        BENEFITS_CTA: "supr-access-benefits-cta",
        SAVINGS_FILTER: "savings-filter",
    },
};
