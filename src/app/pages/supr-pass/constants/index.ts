export const TEXTS = {
    HEADER_TEXT: "Supr Access",
    RECHARGE_HEADER_TEXT: "Recharge Supr Access",
    MEMBERSHIP_BENEFIT_TITLE: "Membership Benefits",
    FAQ_SECTION_TITLE: "Frequently asked questions",
    RECOMMENDED_PLAN_TEXT: "RECOMMENDED",
    DAYS_LEFT: "Days left",
    VALID_TILL: "Valid till",
    SKIP_BTN_TEXT: "No, continue without supr access",
    RECHARGE_BTN_TEXT: "RECHARGE",
    SEE_PLANS: "See Plans",
    VIEW_ALL_FAQS: "VIEW ALL FAQS",
    AUTO_DEBIT_PAYMENT_MODES_TITLE: "Select mode for Auto-Debit",
    AUTO_DEBIT_PAYMENT_MODES_SUBTITLE:
        "Subscription will be automatically renewed after 1 month. You need to give one time-permission.",
    ACTIVITY_TITLE: "Membership Details",
    VIEW_ALL_ACTIVITY: "VIEW ALL ACTIVITY",
    DAY: "Day",
    CANCEL: "CANCEL",
    VIEW_DETAILS: "View Details",
    SELECT_PLAN: "Select Plan",
    BUY_SUPR_ACCESS: "Buy Supr Access",
    CANCEL_MANDATE: "Cancel Mandate",
    DONT_CANCEL: "Don't Cancel",
    SAVINGS_TITLE: "Supr Access Savings",
    LIFETIME_SAVINGS: "Lifetime Savings",
    MONTHLY_SAVINGS: "30 Days Savings",
    MONTH_TEXT: "30 Days",
};

export const FAQ_ITEMS_TO_SHOW = 2;

export const ANALYTICS_OBJECT_NAME = {
    IMPRESSION: {
        SUPR_PASS_SCREEN_VIEW: "supr-pass-detail-view",
        MEMBER_DETAILS_VIEW: "membership-details-view",
    },
    CLICK: {
        PLAN_CLICK: "supr-pass-plan",
        BUY: "supr-pass-buy",
        SEE_PLANS: "supr-pass-see-plans",
        SKIP: "supr-pass-skip",
        RECHARGE: "supr-pass-recharge",
        CANCEL_MANDATE: "auto-debit-cancel-mandate",
        DONT_CANCEL_MANDATE: "auto-debit-dont-cancel",
    },
    CONTEXT: {
        CART_PROMPT: "cart-prompt",
        EXPIRED_SUPR_PASS_MEMBER: "expired-supr-pass-member",
    },
};

export const ACTIVITY_PAGE_URL = "/supr-pass/activity";

export const ANALYTICS_OBJECT_VALUE = {
    MEMBER: "member",
    NON_MEMBER: "non-member",
};

export const PAGE_URL_FRAGMENT = {
    HOME: "home",
    CART: "cart",
    SUBSCRIPTION: "subscription",
};

export const STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-pass-header-bg",
    },
    {
        attributeName: "warningText.textColor",
        attributeStyleVariableName: "--supr-pass-warning-icon-color",
    },
    {
        attributeName: "warningText.fontSize",
        attributeStyleVariableName: "--supr-pass-warning-icon-font-size",
    },
];

export const ACTIVITY_CONTENT_STYLE_LIST = [
    {
        attributeName: "tag.bgColor",
        attributeStyleVariableName: "--supr-activity-days-color",
    },
];

export enum SavingsFilters {
    LIFETIME = "Lifetime",
    MONTH = "Month",
}

export enum ACTION_TYPES {
    ROUTE = "route",
    SCROLL = "scroll",
}

export const SAVINGS_FILTERS = [
    { key: SavingsFilters.MONTH, label: TEXTS.MONTH_TEXT },
    { key: SavingsFilters.LIFETIME, label: SavingsFilters.LIFETIME },
];
