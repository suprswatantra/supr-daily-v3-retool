import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { SharedModule } from "@shared/shared.module";

import { SCREEN_NAMES } from "@constants";

import { SuprPassLayout } from "./layouts/supr-pass.layout";
import { SuprPassFaqLayoutComponent } from "./layouts/supr-pass-faq.layout";
import { SuprPassActivityLayoutComponent } from "./layouts/supr-pass-activity.layout";
import { SuprPassSavingsLayoutComponent } from "./layouts/supr-pass-savings.layout";
import { SuprPassComponents } from "./components";
import { SuprPassContainer } from "./containers/supr-pass.container";
import { SuprPassFaqPageContainer } from "./containers/supr-pass-faq.container";
import { SuprPassActivityContainer } from "./containers/supr-pass-activity.container";
import { SuprPassSavingsContainer } from "./containers/supr-pass-savings.container";
import { SuprPassAdapter } from "./services/supr-pass.adapter";

const routes: Routes = [
    {
        path: "",
        component: SuprPassContainer,
    },
    {
        path: "faq",
        component: SuprPassFaqPageContainer,
        data: { screenName: SCREEN_NAMES.SUPR_PASS_FAQ },
    },
    {
        path: "activity",
        component: SuprPassActivityContainer,
        data: { screenName: SCREEN_NAMES.SUPR_PASS_ACTIVITY },
    },
    {
        path: "savings",
        component: SuprPassSavingsContainer,
        data: { screenName: SCREEN_NAMES.SUPR_PASS_SAVINGS },
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ...SuprPassComponents,
        SuprPassContainer,
        SuprPassFaqPageContainer,
        SuprPassActivityContainer,
        SuprPassSavingsContainer,
        SuprPassLayout,
        SuprPassFaqLayoutComponent,
        SuprPassActivityLayoutComponent,
        SuprPassSavingsLayoutComponent,
    ],
    providers: [SuprPassAdapter],
})
export class SuprPassModule {}
