import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { ActivityBlock, SuprPassInfo } from "@shared/models";

import { SuprPassAdapter as Adapter } from "../services/supr-pass.adapter";

@Component({
    selector: "supr-pass-activity-container",
    template: `
        <supr-pass-activity-layout
            [suprPassInfo]="suprPassInfo$ | async"
            [activity]="suprPassActivity$ | async"
        ></supr-pass-activity-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassActivityContainer implements OnInit {
    suprPassActivity$: Observable<ActivityBlock[]>;
    suprPassInfo$: Observable<SuprPassInfo>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.suprPassActivity$ = this.adapter.suprPassActivity$;
        this.suprPassInfo$ = this.adapter.suprPassInfo$;
    }
}
