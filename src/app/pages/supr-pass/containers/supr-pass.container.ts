import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Params } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import {
    SuprPassInfo,
    CartItem,
    AutoCheckoutQueryParams,
    CartMeta,
    Subscription,
    AutoDebitPaymentMode,
} from "@shared/models";

import { SuprPassAdapter as Adapter } from "../services/supr-pass.adapter";

import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-pass-container",
    template: `
        <supr-pass-layout
            [suprPassInfo]="suprPassInfo$ | async"
            [isFetchingSuprPassInfo]="isFetchingSuprPassInfo$ | async"
            [isCancelingSuprPassAutoDebit]="
                isCancelingSuprPassAutoDebit$ | async
            "
            [autoCheckoutInfo]="autoCheckoutInfo$ | async"
            [cartError]="cartError$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [cartMeta]="cartMeta$ | async"
            [cartItems]="cartItems$ | async"
            [subscriptions]="subscriptions$ | async"
            [autoDebitPaymentModes]="autoDebitPaymentModes$ | async"
            (handleFetchSuprPassInfo)="handleFetchSuprPassInfo()"
            (handleAddSuprPass)="addSuprPass($event)"
            (handleFetchSubscription)="fetchSubscriptions()"
            (handleFetchAutoDebitPaymentModes)="fetchAutoDebitPaymentModes()"
            (handleCancelSuprPassAutoDebit)="cancelSuprPassAutoDebit()"
        ></supr-pass-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassContainer implements OnInit {
    suprPassInfo$: Observable<SuprPassInfo>;
    isFetchingSuprPassInfo$: Observable<boolean>;
    isCancelingSuprPassAutoDebit$: Observable<boolean>;
    cartError$: Observable<any>;
    autoCheckoutInfo$: Observable<AutoCheckoutQueryParams>;
    cartCurrentState$: Observable<CartCurrentState>;
    cartMeta$: Observable<CartMeta>;
    cartItems$: Observable<CartItem[]>;
    subscriptions$: Observable<Subscription[]>;
    autoDebitPaymentModes$: Observable<AutoDebitPaymentMode[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.suprPassInfo$ = this.adapter.suprPassInfo$;
        this.isFetchingSuprPassInfo$ = this.adapter.isFetchingSuprPassInfo$;
        this.isCancelingSuprPassAutoDebit$ = this.adapter.isCancelingSuprPassAutoDebit$;
        this.cartError$ = this.adapter.cartError$;
        this.autoCheckoutInfo$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params)
        );
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.cartItems$ = this.adapter.cartItems$;
        this.subscriptions$ = this.adapter.subscriptions$;
        this.autoDebitPaymentModes$ = this.adapter.autoDebitPaymentModes$;
    }

    handleFetchSuprPassInfo() {
        this.adapter.fetchSuprPassInfo();
    }

    addSuprPass(suprPass: CartItem) {
        this.adapter.updateItemInCart(suprPass);
    }

    fetchSubscriptions() {
        this.adapter.fetchSubscriptions();
    }

    fetchAutoDebitPaymentModes() {
        this.adapter.fetchAutoDebitPaymentModes();
    }

    cancelSuprPassAutoDebit() {
        this.adapter.cancelSuprPassAutoDebit();
    }
}
