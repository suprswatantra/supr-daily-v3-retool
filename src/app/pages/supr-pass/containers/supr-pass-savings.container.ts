import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Params } from "@angular/router";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SuprPassSavingsInfo } from "@shared/models";

import { SuprPassAdapter as Adapter } from "../services/supr-pass.adapter";

import { SavingsFilters } from "../constants";

@Component({
    selector: "supr-pass-savings-container",
    template: `
        <supr-pass-savings-layout
            [suprPassSavings]="suprPassSavings$ | async"
            [selectedTabName]="selectedTabName$ | async"
            [isFetchingSuprPassSavingsInfo]="
                isFetchingSuprPassSavingsInfo$ | async
            "
            (handleFetchSuprPassSavingsInfo)="handleFetchSuprPassSavingsInfo()"
        ></supr-pass-savings-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSavingsContainer implements OnInit {
    suprPassSavings$: Observable<SuprPassSavingsInfo>;
    isFetchingSuprPassSavingsInfo$: Observable<boolean>;
    selectedTabName$: Observable<SavingsFilters>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.suprPassSavings$ = this.adapter.suprPassSavings$;
        this.isFetchingSuprPassSavingsInfo$ = this.adapter.isFetchingSuprPassSavingsInfo$;
        this.selectedTabName$ = this.adapter.urlParams$.pipe(
            map((params: Params) => params["selectedTabName"])
        );
    }

    handleFetchSuprPassSavingsInfo() {
        this.adapter.fetchSuprPassSavingsInfo();
    }
}
