import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { FaqItem } from "@shared/models";

import { SuprPassAdapter as Adapter } from "../services/supr-pass.adapter";

@Component({
    selector: "supr-pass-faq-container",
    template: `
        <supr-pass-faq-layout
            [faqs]="suprPassFaqs$ | async"
        ></supr-pass-faq-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassFaqPageContainer implements OnInit {
    suprPassFaqs$: Observable<FaqItem[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.suprPassFaqs$ = this.adapter.suprPassFaqs$;
    }
}
