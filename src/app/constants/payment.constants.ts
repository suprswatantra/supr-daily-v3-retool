export const RAZORPAY_OPTIONS = {
    CURRENCY: "INR",
    COLOR: "#52c2bb",
};

export enum RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE {
    PAYMENT_PROCESSING_ERROR = "paymentProcessingError",
    PRE_PROCESSING_ERROR = "preProcessingError",
    PAYMENT_PROCESSED = "paymentProcessed",
}

export enum RAZORPAY_CHECKOUT_TYPE {
    STANDARD = "standard",
    CUSTOM = "custom",
}

export const RAZORPAY_CHECKOUT_URL =
    "https://checkout.razorpay.com/v1/checkout.js";

export const RAZORPAY_CUSTOM_CHECKOUT_URL =
    "https://checkout.razorpay.com/v1/razorpay.js";

export const AUTO_RECHARGE_FLAG_KEY = "auto_recharge_upon_add_money";

export const MONEY_OPTIONS = [500, 1000, 2000];

export const RAZORPAY_KEY_FETCH_FAILURE_MESSGAE =
    "Unable to fetch razorpay key. Settings api failed.";

export const RAZORPAY_KEY_NOT_PRESENT_ERROR =
    "Razorpay key not present in settings data";

export const RAZORPAY_KEY_ERROR = "Razorpay key error";

export const RAZORPAY_KEY_ERROR_MODAL_DATA = {
    title: "Unable to complete transaction",
    subTitle:
        "We are unable to complete your transaction at the moment. Please try again after some time. If the problem persists, please contact support.",
};

export enum AUTODEBIT_TOKEN_STATUS {
    INITIATED = "initiated",
    CONFIRMED = "confirmed",
    REJECTED = "rejected",
}

export enum PAYMENT_METHODS_V2 {
    UPI = "upi",
    CARD = "card",
    WALLET = "wallet",
    PREFERRED = "preferred",
    NETBANKING = "netbanking",
}

export enum PAYMENT_METHODS_CUSTOM_OPTIONS {
    NEW_VPA = "newVPA",
    NEW_CARD = "newCard",
}

export const RAZORPAY_CUSTOM_CHECKOUT_BACKEND_WEBVIEW_ENDPOINT =
    "v1/payment/checkout/";

export enum CARD_TYPES {
    AMEX = "amex",
    VISA = "visa",
    MAESTRO = "maestro",
}

export enum SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES {
    UNKNOWN = "unknown",
    INVALID_VPA = "invalidVPA",
    NO_SELECTION = "noSelection",
    INVALID_VALUES = "invalidValues",
    MISSING_FIELDS = "missingFields",
}

export const SELECTED_PAYMENT_METHODS_VALIDATION_TOASTS = {
    [SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.NO_SELECTION]:
        "Please select a mode of payment.",
    [SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.UNKNOWN]:
        "Sorry, something seems to have gone wrong. Please try again.",
    [SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.INVALID_VPA]:
        "The UPI address you have entered seems to be wrong.",
    [SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.MISSING_FIELDS]:
        "Please ensure you have entered all the mandatory fields.",
    [SELECTED_PAYMENT_METHODS_VALIDATION_ERROR_TYPES.INVALID_VALUES]:
        "Please ensure you have entered all valid values.",
};

export enum PAYMENT_METHOD_VERSIONS {
    V1 = 1,
    V2 = 2,
}

export const CVV_LENGTH = {
    AMEX: 4,
    OTHER: 3,
};
