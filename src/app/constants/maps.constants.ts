export const MapsSettings = {
    latitude: 20.5937,
    longitude: 78.9629,
    zoomLevel: 18,
    tilt: 30,
    compass: false,
    myLocationButton: true,
    myLocation: true,
    zoomControl: false,
    animationDuration: 500,
};

export const GoogleMaps = {
    id: "google-maps",
    url:
        "https://maps.googleapis.com/maps/api/js?key=AIzaSyCDrbb0P5E9INfPvF7gdqqtqmWk0cPTfwQ",
};

export const GoogleMapsStyles = ` 
    #google_map {
        height: 1200px;
        width: 1600px;
    }
    .gm-style-iw * {
        display: block;
        width: 100%;
    }
    .gm-style .gm-style-iw {
        max-width: 230px !important;
    }
    .gm-style-iw h4,
    .gm-style-iw p {
        margin: 0;
        padding: 0;
    }
    .gm-style-iw a {
        color: #4272db;
    }
`;
