export const SKU_ITEM_CONFIG = {
    itemType: "sku_square_with_add_button",
    data: {
        cta: {
            events: {
                click: {
                    trigger: "skuPreview",
                },
            },
        },
        image: {
            style: {
                "border-radius": "4px",
                "box-shadow": "0px 1px 4px rgba(0, 0, 0, 0.08)",
            },
        },
        style: {
            "margin-right": "16px",
            "margin-bottom": "16px",
        },
        preferredMode: "direct_add",
    },
};

export const ALTERNATES_WIDGET_BASE_CONFIG = {
    widgetType: "all_in_one_horizontal_scroll",
    data: {
        type: "sku",
        style: {
            "margin-top": "16px",
            "background-color": "#F9F9F9",
        },
        title: {
            text: "Items available for tomorrow",
            textType: "subheading",
            style: {
                color: "#409A95",
            },
        },
        rowCount: 2,
        analytics: {
            saImpression: {
                objectName: "",
                objectValue: "",
            },
        },
        groupTypeId: 1,
        includeOutOfStock: false,
        skuCategorizationType: "linear",
    },
};
