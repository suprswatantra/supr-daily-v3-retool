export const FRESHBOT_SCRIPT = {
    ENABLED: true,
    ID: "spd-busns-spt",
    ASYNC: true,
    DATA_SELF_INIT: "false",
    DATA_INIT_TYPE: "delayed",
    SRC: "https://cdn.freshbots.ai/assets/share/js/freshbots.min.js",
    DATA_BOT_VERSION: "10",
    DATA_ENV: "prod",
    DATA_REGION: "",
};

export const FRESHBOT_SCRIPT_V3 = {
    ENABLED: true,
    ID: "spd-busns-spt",
    ASYNC: true,
    DATA_SELF_INIT: "false",
    DATA_INIT_TYPE: "delayed",
    SRC: "https://cdn.freshbots.ai/assets/share/js/freshbots.min.js",
    DATA_BOT_VERSION: "15",
    DATA_ENV: "prod",
    DATA_REGION: "",
};

export const CLIENT_PARAMS = {
    AUTH: "cstmr::xtrInfrmtn:User_Auth",
    TODAY_DATE: "today_date",
    TODAYS_DATE: "cstmr::xtrInfrmtn:Todays_Date",
    PREVIOUS_DATE: "cstmr::xtrInfrmtn:Previous_Date",
    USR_NAME: "cstmr::nm",
    USER_NUMBER: "cstmr::phn",
    USER_ID: "cstmr::xtrInfrmtn:User_id",
};
