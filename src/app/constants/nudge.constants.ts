export const NUDGE_TYPES = {
    ADDRESS: "address_retro",
    RATING: "rating",
    AUTO_DEBIT_PAYMENT_FAILURE: "supr_access_auto_debit_payment_failure",
    EXIT_CART: "exit_cart",
    CAMPAIGN_POPUP: "campaign_info",
    ORDER_FEEDBACK: "overall_order",
    GENERIC: "generic_nudge",
};

export const NUDGE_SETTINGS_KEYS = {
    [NUDGE_TYPES.ADDRESS]: "addressNudge",
    [NUDGE_TYPES.RATING]: "ratingNudge",
    [NUDGE_TYPES.EXIT_CART]: "exitCartNudge",
    [NUDGE_TYPES.CAMPAIGN_POPUP]: "campaignPopupNudge",
    [NUDGE_TYPES.ORDER_FEEDBACK]: "orderFeedbackNudge",
    [NUDGE_TYPES.GENERIC]: "genericNudge",
    [NUDGE_TYPES.AUTO_DEBIT_PAYMENT_FAILURE]: "autoDebitPaymentFailureNudge",
    COOL_OFF_TIME: "nudgeCoolOffTime",
};

export const DEFAULT_COOL_OFF_TIME_IN_MINS = 2 * 24 * 60;
