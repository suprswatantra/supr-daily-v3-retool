export const ONE_DAY = 1000 * 60 * 60 * 24;
export const FIRST_DAY_OF_WEEK = 1; // Monday
export const DAY_NAMES = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
export const DAY_LETTERS = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
export const WEEKDAY_LETTERS = ["mon", "tue", "wed", "thu", "fri"];
export const WEEK_END_LETTERS = ["sat", "sun"];
export const DAY_NAMES_MAP = {
    SUNDAY: {
        SHORT: "Sun",
        FULL: "Sunday",
    },
    MONDAY: {
        SHORT: "Mon",
        FULL: "Monday",
    },
    TUESDAY: {
        SHORT: "Tue",
        FULL: "Tuesday",
    },
    WEDNESDAY: {
        SHORT: "Wed",
        FULL: "Wednesday",
    },
    THURSDAY: {
        SHORT: "Thu",
        FULL: "Thursday",
    },
    FRIDAY: {
        SHORT: "Fri",
        FULL: "Friday",
    },
    SATURDAY: {
        SHORT: "Sat",
        FULL: "Saturday",
    },
};
export const DAY_NAMES_FULL = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
];
export const MONTH_NAMES = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC",
];
export const MONTH_NAMES_FULL = [
    "JANUARY",
    "FEBRUARY",
    "MARCH",
    "APRIL",
    "MAY",
    "JUNE",
    "JULY",
    "AUGUST",
    "SEPTEMBER",
    "OCTOBER",
    "NOVEMBER",
    "DECEMBER",
];

export const DEFAULT_LOCALE = "en-US";
