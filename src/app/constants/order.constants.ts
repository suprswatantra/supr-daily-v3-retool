export enum NEW_ORDER_STATUS_TYPES {
    ORDER_SCHEDULED = "ORDER_SCHEDULED",
    PICKED_UP = "PICKED_UP",
    DELIVERED_PARTIAL = "DELIVERED_PARTIAL",
    NO_DELIVERY = "NO_DELIVERY",
    DELAYED = "DELAYED",
    DELIVERED = "DELIVERED",
    REFUND_FULL = "REFUND_FULL",
    REFUND_PARTIAL = "REFUND_PARTIAL",
    RESCHEDULED = "RESCHEDULED",
}

export enum REFUND_TYPES {
    WALLET_REFUND = "WALLET_REFUND",
    CARRY_FORWARD = "CARRY_FORWARD",
}

export enum ORDER_STATUS_TYPES {
    SCHEDULED = "Scheduled",
    PROCESSED = "Processed",
    DELIVERED = "Delivered",
    CANCELLED = "Cancelled",
}

export const ORDER_STATUS_ICONS = {
    [NEW_ORDER_STATUS_TYPES.ORDER_SCHEDULED]: "lock",
    [NEW_ORDER_STATUS_TYPES.PICKED_UP]: "delivery",
    [NEW_ORDER_STATUS_TYPES.DELAYED]: "error_octagon",
    [NEW_ORDER_STATUS_TYPES.NO_DELIVERY]: "error_octagon",
    [NEW_ORDER_STATUS_TYPES.DELIVERED_PARTIAL]: "delivery",
    [NEW_ORDER_STATUS_TYPES.REFUND_PARTIAL]: "error_octagon",
    [NEW_ORDER_STATUS_TYPES.REFUND_FULL]: "error_octagon",
};

export const DISPLAY_TEXT = {
    DELIVERED: "We delivered your orders successfully.",
    ORDER_SCHEDULED: "Arriving at your doorstep.",
    PICKED_UP: "Your delivery is on the way.",
    NO_DELIVERY: "We could not deliver your orders.",
    DELIVERED_PARTIAL: "We could not deliver some orders.",
    REFUND_PARTIAL:
        "We could not deliver some orders. We have updated your wallet balance and subscriptions as applicable.",
    REFUND_FULL:
        "We could not deliver your orders. We have updated your wallet balance and subscriptions as applicable.",
    DELAYED: "Your order is delayed. We are looking into it.",
};

export const EDIT_DELIVERY_ACTIONS = {
    DONE: "done",
    CHANGE_DATE: "changeDate",
    USE_BALANCE: "useBalance",
    RECHARGE_SUBSCRIPTION: "rechargeSubscription",
    UPDATE: "update",
    CANCEL: "cancel",
    ADJUSTMENT: "adjustment",
    REFUND: "refund",
    DEBIT: "debit",
    WALLET_RECHARGE: "walletRecharge",
    DELIVER_ONCE: "deliverOnce",
};
