import { SwiperOptions } from "swiper";

export const SLIDE_CHANGE_TIME = 150;
export const SLIDE_CHANGE_TIME_SLOW = 250;
export const SLIDE_CHANGE_TIME_INCREMENT = 75;

export const SLIDER_OPTIONS: SwiperOptions = {
    speed: SLIDE_CHANGE_TIME,
    uniqueNavElements: false,
    resistanceRatio: 0,
    preloadImages: true,
    lazy: false,
    simulateTouch: true,
};

export interface SliderOptionsConfig {
    speed?: number;
    pagination?: string;
    resistanceRatio?: number;
    preloadImages?: boolean;
    lazyLoading?: boolean;
    simulateTouch?: boolean;
    initialSlide?: number;
    width?: number;
}
