export const SETTINGS = {
    LOW_DELIVERIES_NDAYS_COVER: "low_deliveries_ndays_cover",
    RECHARGE_TRIAL_MESSAGE: "recharge_trial_message",
    RECHARGE_OPTIONS: "recharge_options",
    ADD_MONEY_OPTIONS: "add_money_options",
    CONTACT_NUMBER: "contact",
    DEFAULT_RECHARGE_OPTION: "default_recharge_option",
    RAZORPAY_LIVE: "razorpay_live",
    RAZORPAY_TEST: "razorpay_test",
    MRP_STRIKE_DISPLAY_ENABLED: "mrp_strike_display_enabled",
    FABRIC_ENABLED: "fabric_enabled",
    APPS_FLYER_ENABLED: "apps_flyer_integration_enabled",
    ONE_SIGNAL_ENABLED: "one_signal_enabled",
    SCORE_BASED_SEARCH_ENABLED: "score_based_search_enabled",
    SEARCH_SCORE_PARAMS: "search_score_params",
    SEARCH_PRODUCTS_ENABLED: "search_products_enabled",
    GMAPS_ENABLED: "google_maps_enabled",
    SUPPORT_BUTTONS: "support_buttons",
    APPS_TRACKING_ENABLED: "appsTrackingEnabled",
    TRACKED_APPS: "trackedApps",
    PAST_ORDER_COLLECTION: "pastOrderCollection",
    ADD_SUPR_ACCESS_TO_CART_CONFIG: "addSuprAccessToCartConfig",
    SHOW_SUPR_PASS_AUTO_DEBIT: "showSuprAccessAutoDebit",
    AUTO_DEBIT_MODES_MODAL_DATA: "autoDebitModesModalData",
    SEARCHBOX_PLACEHOLDER: "searchBoxPlaceholder",
    CANCEL_AUTO_DEBIT_MODAL_DATA: "cancelAutoDebitModalData",
    EXCLUDE_EVENTS_DICT: "excludeEventsDict",
    IMPRESSION_THROTTLE_CONFIG: "impressionThrottleConfig",
    RECHARGE_THROUGH_CART: "rechargeThroughCart",
    SHOW_NEW_PAYMENT_FOOTER: "showNewPaymentFooter",
    CUT_OFF_TIME: "cutOffTime",
    TIME_BASED_EXPERIMENT_DATA: "timeBasedSuprPassExperimentData",
    HIDE_FIRST_SUBCATEGORY: "hideFirstSubcategory",
    PAYMENT_FAILURE_MODAL_DATA: "paymentFailureModalData",
    SENTRY_FILTERS: "sentryFilters",
    SEGMENT_ERROR_FILTERS: "segmentErrorFilters",
    SUPR_PASS_CARD_DEFAULTS: "suprPassCardDefaults",
    SHOW_SUPR_PASS_SIDEBAR_CARD: "showSuprPassSidebarCard",
    SHOW_SUPR_PASS_SAVINGS_BANNER_ON_CART: "showSuprPassSavingsBannerOnCart",
    SHOW_SUPR_PASS_HOMEPAGE_CARD: "showSuprPassHomepageCard",
    WALLET_TNC: "walletTnc",
    REWARD_SKU_INFO: "rewardSkuInfo",
    SEARCH_MIN_CHAR_LENGTH: "searchMinCharLength",
    SKU_SEARCH_FILTERS: "searchFilterEnabled",
    BACKEND_SEARCH: "backendSearch",
    COUPON_TNC_DISPLAY_CONFIG: "couponTncDisplayConfig",
    ORDER_REFUND_TYPES: "orderRefundTypes",
    ORDER_REFUND_STATES: "orderRefundStates",
    ORDER_TOOLTIP_STATES: "orderTooltipStates",
    RAZORPAY_MODAL_AVAILABLE_MODES_TEXT: "razorpayModalAvailableModesText",
    PAYMENT_OFFER_OTHER_MODES_TEXT: "paymentOfferOtherModesText",
    RUDDERSTACK_SETTING_KEYS: "rudderStackSettingKeys",
    SEGMENT_SESSION_THROTLLING_ENABLED: "segmentSessionThrottlingEnabled",
    APP_AUDIO: "appAudio",
    ORDER_REFUND_MODAL_CONFIG: "orderRefundModalConfig",
    USE_OFFERS_PAGE_ON_CART: "useOffersPageOnCart",
    CART_COUPON_BLOCK_CONFIG: "cartCouponBlockConfig",
    POD_FEEDBACK_TEXT_CONFIG: "podFeedbackTextConfig",
    REPORT_POD_CONFIG: "reportPODConfig",
};

export const FEATURE_FLAG_KEYS = {
    CART_EXIT_PROMPT_ENABLED: "cartExitPromptEnabled",
};

export const SUPR_PASS_BLOCK_ACCESS = {
    RESTRICT_SUBSCRIPTION_CREATION: "restrict_subscription_creation",
    RESTRICT_FUTURE_DELIVERIES: "restrict_future_deliveries",
    RESTRICT_SUBSCRIPTION_ACTIVITY_TIME_BASED:
        "restrict_subscription_activity_time_based",
};

export const SETTINGS_KEYS_DEFAULT_VALUE = {
    suprPassCardDefaults: {
        membership: "Access membership",
        startingAt: "Starting at",
        divide: "/",
        month: "month",
        months: "months",
        daysLeft: "days left",
        seeMore: "Tap to see more",
        viewCta: "VIEW DETAILS",
        getSuprPassCta: "GET ACCESS NOW",
        rechargeCta: "RECHARGE NOW",
        states: {
            active: "ACTIVE",
            isEndingSoon: "ENDING SOON",
            isExpired: "EXPIRED",
        },
    },
    showSuprPassSavingsBannerOnCart: true,
    showSuprPassHomepageCard: true,
    showSuprPassSidebarCard: true,
    addressNudge: {
        type: "address_retro",
        img:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Address+Retro+Nudge.png",
        title: "Supr goodness coming your way!",
        subTitle:
            "We are expanding rapidly and need a few more details about your address to bring you better products and offers relevant to you",
        showSkip: true,
        primaryButton: { text: "Okay, take me there!", action: true },
        nudgeCoolOffTime: 2880,
        nudgeDisplayPages: ["home"],
    },

    timeBasedSuprPassExperimentData: {
        text:
            "Subscribe to Access to schedule or modify orders for next day after ",
    },

    campaignPopupNudge: {
        type: "campaign_info",
        enabled: true,
        img:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/PopUp_SUPR400_1.png",
        nudgeDisplayPages: ["home"],
    },

    ratingNudge: {
        enabled: true,
        text: {
            prompt: {
                title: "Are you enjoying your Supr Daily experience?",
                primaryText: "Yes, I am",
                secondaryText: "Not really",
                neverAskText: "DON'T ASK ME AGAIN",
            },
            thankyou: {
                title: "Glad you are enjoying Supr Daily!",
                subTitle: "Take a moment to rate us on Play Store",
                subTitleIos: "Take a moment to rate us on App Store",
                primaryText: "Sure",
                secondaryText: "Not now",
            },
            feedback: {
                title: "We are on a mission to serve you better",
                subTitle: "How can we improve further?",
                primaryText: "Submit",
                secondaryText: "Not now",
                placeHolderText: "Anything in your mind. Please let us know.",
            },
            toastText:
                "Thank you for your feedback. We will do our best to fix this soon.",
        },
        triggers: {
            order_completed: { delayTime: 3000, enabled: true },
            cancel_addon: { delayTime: 3000, enabled: true },
            cancel_subscription: { delayTime: 3000, enabled: true },
            cancel_vacation: { delayTime: 3000, enabled: true },
            update_subscription: { delayTime: 3000, enabled: true },
            update_subscription_instruction: {
                delayTime: 3000,
                enabled: true,
            },
            recharge_subscription: { delayTime: 3000, enabled: true },
            successful_referral: { delayTime: 1000, enabled: true },
        },
        nudgeCoolOffTime: 10080,
    },

    exitCartNudge: {
        type: "exit_cart",
        vibrationEnabled: true,
        vibrationTimeInMs: 200,
        title: "Did you forget something?",
        subTitle:
            "There are items in your cart. Complete your purchase to ensure delivery.",
        primaryButton: { text: "Go to cart", action: true },
        secondaryButton: { text: "Leave" },
    },

    autoDebitPaymentFailureNudge: {
        type: "supr_access_auto_debit_payment_failure",
        title: "Your auto-debit setup has been cancelled!",
        subTitle:
            "Your scheduled payment for Supr Access renewal has failed. We have cancelled your mandate. Please recharge your Supr Access Membership today to avoid paying service fee on all future orders.",
        noteText:
            "NOTE: If service fee is already deducted because of Supr Access expiry, you will get the refund on pending deliveries after renewing Supr Access.",
        primaryButton: { text: "RECHARGE NOW!", action: true },
        secondaryButton: { text: "RECHARGE LATER" },
    },

    cartExitPromptEnabled: true,
    urgencyCollection: {
        viewId: 244,
        path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
        bgColor: "#FFF",
        fullUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/85018461_192018915536311_6766392603123384320_n.png",
    },
    pastOrderCollection: {
        tnc: null,
        type: "sku",
        items: [],
        title: { text: "Your regular shopping needs", color: "#000000" },
        subtitle: {
            text: "Based on what you buy regularly",
            color: "rgba(0, 0, 0, 0.4)",
        },
        seeAll: null,
        viewId: 101,
        background: { color: "#f1f6f5", image: null },
        maxAllowed: 12,
        displayType: "horizontal_list",
        skuCategorizationType: "linear",
    },
    banner: {
        CART: {
            TITLE: "Earliest delivery date",
            SUBTITLE:
                "Thank you for choosing Supr Daily. We take a day to verify your address on your first delivery to ensure smooth deliveries in the future.",
        },
        SUBSCRIPTION: {
            TITLE: "Next day deliveries available after your first order.",
        },
        AVAILABLE_AFTER_FIRST_DELIVERY: "( Available after first delivery )",
    },
    freshbotConfig: {
        ENABLED: true,
        ID: "spd-busns-spt",
        ASYNC: true,
        DATA_SELF_INIT: false,
        DATA_INIT_TYPE: "delayed",
        SRC: "https://cdn.freshbots.ai/assets/share/js/freshbots.min.js",
        DATA_BOT_VERSION: "5",
        DATA_ENV: "prod",
        DATA_REGION: "",
    },
    legacyAddToCart: false,
    enableSmartlook: false,
    skusProductNotAvailableTexts: {
        title: "Items unavailable",
        subtitle1: "The products you were looking for is currently unavailable",
        subtitle2:
            "Check after sometime to check if the product is available. You may browse different products meanwhile",
        btnText: "OKAY GOT IT",
    },
    alternativeProductsHeader: "Items available for tomorrow",
    cartReviewDateComponentConfig: {
        icon: "error",
        enabled: false,
        title: "Review delivery date(s)",
        subtitle: "Due to high demand, some items may not be available earlier",
    },
    supportConfig: {
        call: "Call us",
        chat: "Chat with us",
        help: "Need help with anything?",
        number: "9699000035",
        isCall: true,
        isChat: true,
        archive: {
            id: 201,
            nodeType: "button",
            name: "All Issues",
            description: "",
            data: {
                icon: "subscription",
                title: "Past Issues",
            },
        },
        calendar: {
            orderDateNumber: 3,
            orderDateLimit: 14,
            title: "Help with your orders",
            enabled: true,
            call: "Call us",
            chat: "Chat with us",
            calendarNavigationText: "Help with older orders?",
            calendarNavigationLinkText: "PREVIOUS ORDERS",
        },
        ivr_popup: {
            enabled: true,
            header: "Contact Us",
            title:
                "Please create a ticket so that we can assign an agent to you",
            subTitle:
                "Once you create a ticket you can contact our agents for immediate assistance",
            listHeader:
                "How to create a query / compliant ticket for my delivery?",
            list: [
                "Find the order with issue on the Order calendar",
                "Click on 'Get help’",
                "Select issue type and follow the steps",
                "Customer support executive will reach out to you if required",
            ],
            nonOrderButton: "Raise an non-order related issue",
            previousOrderButton: "Issue with a previous order",
            todayOrderButton: "Issue with today’s order",
        },
    },
    scheduleStatusConfig: {
        ORDER_SCHEDULED: {
            status: "ORDER_SCHEDULED",
        },
        CANCELED: {
            status: "CANCELED",
            message: "Order cancelled & refund issued",
        },
        RESCHEDULED: {
            status: "RESCHEDULED",
            message: "Updated subscription balance",
        },
    },
    refundTypesConfig: {
        WALLET_REFUND: {
            statusType: "info",
            statusIcon: "approve",
            status: "WALLET_REFUND",
            statusText: "Amount Refunded",
        },
        CARRY_FORWARD: {
            statusType: "info",
            statusIcon: "approve",
            status: "CARRY_FORWARD",
            statusText: "Updated subscription balance",
        },
    },
    orderStatusConfig: {
        SCHEDULED: {
            status: "Scheduled",
            statusType: "warning",
            statusIcon: "stop_watch",
            statusText: "Delivering Soon",
        },
        PROCESSED: {
            status: "Processed",
            statusType: "warning",
            statusIcon: "stop_watch",
            statusText: "Delivering Soon",
        },
        DELIVERED: {
            statusType: "text",
            status: "Delivered",
            statusIcon: "approve",
            statusText: "Delivered",
        },
        CANCELLED: {
            status: "Cancelled",
            statusType: "warning",
            statusText: "Cancelled",
            statusIcon: "error_octagon",
        },
    },
    orderStatusNewConfig: {
        ORDER_SCHEDULED: {
            statusType: "frequency",
            status: "ORDER_SCHEDULED",
        },
        PICKED_UP: {
            status: "PICKED_UP",
            statusType: "frequency",
        },
        DELIVERED_PARTIAL: {
            statusType: "warning",
            statusIcon: "stop_watch",
            status: "DELIVERED_PARTIAL",
            statusText: "Delivering Soon",
        },
        NO_DELIVERY: {
            status: "NO_DELIVERY",
            statusType: "warning",
            statusIcon: "stop_watch",
            statusText: "Delivering Soon",
        },
        DELAYED: {
            status: "DELAYED",
            statusType: "warning",
            statusIcon: "stop_watch",
            statusText: "Delivering Soon",
        },
        DELIVERED: {
            statusType: "text",
            status: "DELIVERED",
            statusIcon: "approve",
            statusText: "Delivered",
        },
        REFUND_FULL: {
            status: "REFUND_FULL",
        },
        REFUND_PARTIAL: {
            status: "REFUND_PARTIAL",
        },
        CANCELED: {
            status: "CANCELED",
            statusType: "info",
            statusIcon: "approve",
            statusText: "Order cancelled & refund issued",
        },
        RESCHEDULED: {
            statusType: "info",
            statusIcon: "approve",
            status: "RESCHEDULED",
            statusText: "Updated subscription balance",
        },
        AWAITING_DELIVERY_CONFIRMATION: {
            statusType: "text",
            statusIcon: "error_octagon",
            statusText: "Update pending",
            status: "AWAITING_DELIVERY_CONFIRMATION",
        },
    },
    orderTooltipStates: ["REFUND_FULL", "REFUND_PARTIAL", "RESCHEDULED"],
    orderRefundTypes: ["WALLET_REFUND", "CARRY_FORWARD"],
    orderRefundStates: ["REFUND_FULL", "REFUND_PARTIAL"],
    deliverySlotTypes: ["REGULAR_DELIVERY", "ALTERNATE_DELIVERY"],
    cartAlternateDeliveryBannerDetails: {
        type: "caution",
        icon: "stop_watch",
        ctaIcon: "chevron_right",
        title: "New shipment details",
        subtitle: "We now have 2 delivery slots so we can serve you better!",
        showMoreText: "LEARN MORE",
        modalInfo: {
            image:
                "https://staging-suprdaily.s3.ap-south-1.amazonaws.com/media/ad_image/ad_new_07052020.png",
            url: "",
            outsideRedirection: false,
        },
        enabled: false,
    },
    oosNotifyTexts: {
        notifySection: {
            notify_v2: "You will be notified when it's back in stock",
            notify: "Click on Notify and get an alert when it's back in stock",
            oos: "Click on Notify and get an alert when it's back in stock",
        },
        alertSection: {
            getAnAlert: "Get an alert if this item is available earlier",
            willNotifyYou: "We will notify you",
        },
    },
    impressionThrottleConfig: {
        keyGenerationMethod: "stringify", // "stringify" || "concat",
        excludedEventKeys: [],
    },
    deliverySummaryVacationBanner: {
        title: "DELIVERIES TEMPORARILY PAUSED",
        subtitle:
            "Due to the current lockdown we are unable to serve you at the moment. We will notify you once we are back on track.",
    },
    deliverySummaryVacationTexts: {
        vacation: {
            title: "Vacation is active. We have paused your orders till ",
            subtitle: "MODIFY VACATION ",
        },
        systemVacation: {
            title: "Orders paused due to the govt. lockdown till ",
            subtitle: "LEARN MORE",
        },
    },
    cutOffTime: { value: "23:00:00" },
    orderSuccessADModal: {
        enabled: false,
        impressionLimit: 3,
        title: "Delivery update on your order for ",
        subtitle: "Your bulk grocery order will arrive between 7am to 11am.",
        lineItems: [
            "You will receive an OTP for this delivery.",
            "Our delivery executive will contact you on arrival, please be available.",
            "Share the OTP with our delivery executive once you verify your order.",
        ],
    },
    orderSuccessWhatsappModal: {
        enabled: false,
        impressionDayOffset: 7,
        impressionTotalLimit: 3,
        impressionPeriodicLimit: 1,
    },
    showMoreSimilarProductsBtn: false,
    cartErrorCodes: {
        showPlusMinusErrorCode: [5],
        takeMessageFromBackendErrorCode: [5],
        errorCodeObjectName: {
            5: "bulk-order-error",
        },
    },
    paymentMethods: {
        preferred: {
            icon: "star_outline",
        },
        other: {
            icon: "card",
        },
    },
    addSuprAccessToCartConfig: {
        isEnabled: false,
        coolOffTime: 86400000, // 1 day in milliseconds
        maxCount: 3,
    },
    autoDebitModesModalData: {
        default: {
            title: "Select mode for Auto-Debit",
            subtitle:
                "Subscription will be automatically renewed. You need to give one-time permission.",
        },
        change: {
            title: "Change mode for Auto-Debit",
            subtitle:
                "Subscription will be automatically renewed. You need to give one-time permission.",
        },
    },
    cancelAutoDebitModalData: {
        title: "Your auto-debit setup will be cancelled!",
        subtitle:
            "Recurring payment helps you keep your Supr Access membership active, without any hassle of remembering the expiry date.",
    },
    searchBoxPlaceholder: {
        default: "Fruits, Vegetables & more",
        searchPage: "Fruits, Vegetables & more",
    },
    timelineSlotMap: {
        REGULAR_DELIVERY: "morning_slot",
        ALTERNATE_DELIVERY: "day_slot",
    },
    rechargeThroughCart: true,
    /** excludeEventsDict sample with data  */
    // excludeEventsDict: {
    //     "product-viewed": {
    //         allowedTraits: {
    //             context: ["out-of-stock-sku"],
    //         },
    //         notAllowedTraits: {
    //             currentScreen: ["category"],
    //         },
    //     },
    // },
    excludeEventsDict: {},
    // Rudderstack sample for config data -> use same like segment
    rudderStackSettingKeys: {
        impressionThrottleConfig: {
            sessionThottling: false,
            keyGenerationMethod: "stringify", // "stringify" || "concat",
            excludedEventKeys: [],
        },
        excludeEventsDict: {},
    },
    sourceLink: null,
    utmParams: null,
    showNewPaymentFooter: false,
    hideFirstSubcategory: false,
    paymentMethodsComponentMapping: {
        upi: {
            type: "upi",
        },
        card: {
            type: "card",
        },
        wallet: {
            type: "tile",
        },
        preferred: {
            type: "list",
        },
        netbanking: {
            type: "tile",
            ctaText: "VIEW ALL",
            header: "Netbanking",
        },
    },
    paymentFailureModalData: {
        titleIcon: "error_triangle",
        title: "Transaction failed",
        subtitle: "Any amount deducted will be refunded within 4-7 days.",
        amountToPayText: "Amount to pay:",
        primaryBtnText: "TRY OTHER MODES",
        secondaryBtnText: "RETRY",
    },
    freeItemsFaq: [
        "Free gifts are valid for limited users only",
        "There are no hidden charges for the gift. Its Free!",
        "We will send you a short survey. Your feedback on the gift will be valuable to us.",
    ],
    showSuprPassCard: false,
    backendSearch: false,
    searchInstrumentationEnabled: false,
    walletTnc: {
        text: "One time offer",
    },
    rewardSkuInfo: {
        SAMPLE: {
            tagName: "FREE",
            style: {
                tagBgColor: "#fbebba",
                tagTextColor: "#a98411",
                rewardIconColor: "#a98411",
                infoIconColor: "#a98411",
            },
            offerDescription: [
                "Free rewards are valid for limited users only.",
                "There are no hidden charges for the gift. Its Free!",
                "We will be sending you a short survey. Your feedback on the gift will be valuable to us.",
            ],
        },
        BASKET_OFFER_REWARD: {
            tagName: "OFFER",
            style: {
                tagBgColor: "#fbebba",
                tagTextColor: "#a98411",
                rewardIconColor: "#a98411",
                infoIconColor: "#a98411",
            },
            offerDescription: [
                "Free rewards are valid after successful offer redemption.",
                "There are no hidden charges for the gift. Its Free!",
            ],
        },
    },
    searchMinCharLength: 2,
    searchFilterEnabled: false,
    milestones_rewards: {
        SUPRACCESS: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/2020-10-07_205236_access",
            path:
                "media/milestone_images/reward_images/2020-10-07_205236_access",
            bgColor: "#FFF",
            compressed_url: {
                "200_200": {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/200_200/2020-10-07_205237_access",
                },
            },
        },
        SUPRCREDIT: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/2020-10-07_210117_credit",
            path:
                "media/milestone_images/reward_images/2020-10-07_210117_credit",
            bgColor: "#FFF",
            compressed_url: {
                "200_200": {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/200_200/2020-10-07_210118_credit",
                },
            },
        },
        COUPONCODE: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/2020-10-07_210354_coupon",
            path:
                "media/milestone_images/reward_images/2020-10-07_210354_coupon",
            bgColor: "#FFF",
            compressed_url: {
                "200_200": {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/200_200/2020-10-07_210355_coupon",
                },
            },
        },
        COMPLETED: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/2020-10-07_215424_star",
            path: "media/milestone_images/reward_images/2020-10-07_215424_star",
            bgColor: "#FFF",
            compressed_url: {
                "200_200": {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/milestone_images/reward_images/200_200/2020-10-07_215425_star",
                },
            },
        },
    },
    couponTncDisplayConfig: {
        cart: { useBottomSheet: true },
        wallet: { hideTnc: true },
    },
    razorpayModalAvailableModesText:
        "Available payment options for offer applied",
    paymentOfferOtherModesText: {
        title: "Other modes",
        subtitle:
            "NOTE: Applied coupon code is not applicable for other payment options. Please remove coupon to proceed.",
    },
    segmentSessionThrottlingEnabled: true,
    appAudio: { volume: 0.9, isEnabled: true },
    orderRefundModalConfig: {
        WALLET_REFUND: {
            addon: {
                ordered: "Units Ordered",
                delivered: "Units Delivered",
                qtyAdjusted: "Units Refunded",
                amountAdjusted: "Amount refunded",
            },
            subscription: {
                ordered: "Units Ordered",
                delivered: "Units Delivered",
                qtyAdjusted: "Units Refunded",
                amountAdjusted: "Amount refunded",
            },
        },
        CARRY_FORWARD: {
            subscription: {
                ordered: "Units Ordered",
                delivered: "Units Delivered",
                qtyAdjusted: "Subscription balance (units) updated",
                amountAdjusted: "Subscription balance amount updated",
            },
        },
        RESCHEDULED: {
            addon: {
                ordered: "Units Ordered",
                delivered: "Units Delivered",
                qtyAdjusted: "Units Rescheduled for",
                footerConfig: {
                    title: "About Auto Reschedule",
                    subtitle:
                        "We now automatically reschedule an order missed due to operational challenges to next day.",
                },
            },
        },
    },
    useOffersPageOnCart: true,
    cartCouponBlockConfig: {
        title: "Apply coupon",
        subtitle: "View all offers",
        showChevIcon: true,
    },
    cartBannerSkuKnowMore: [
        "Applicable only on non-milk items purchases only.",
        "Offer valid for limited users only.",
    ],
    analyticsConfig: {
        segment: {
            event_type: {
                click: false,
                impression: false,
                screenview: false,
                modalview: false,
                modalclose: false,
                swipe: false,
                api_call: false,
                error: false,
            },
        },
        rudderstack: {
            integration_conf: {
                All: false,
                MoEngage: true,
                Snowflake: true,
                Amplitude: false,
                Redshift: true,
            },
            event_type: {
                click: true,
                impression: true,
                screenview: true,
                modalview: true,
                modalclose: true,
                swipe: true,
                api_call: true,
                error: true,
            },
        },
    },
};
