export const SUBSCRIPTION_EXPIRY_DAYS_LIMIT = -7;
export const SUBSCRIPTION_RECHARGE_DAYS_LIMIT = 7;
export const SUPR_PASS_RECHARGE_FREQUENCY = {
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: true,
    sun: true,
};
