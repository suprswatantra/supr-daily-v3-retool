export enum AUDIO_KEYS {
    APP_LAUNCH_AUDIO = "APP_LAUNCH_AUDIO",
    PAYMENT_DELIVERY_CONFIRMATION_AUDIO = "PAYMENT_DELIVERY_CONFIRMATION_AUDIO",
    PN_AUDIO = "PN_AUDIO",
}

export const AUDIO_ASSETS = {
    APP_LAUNCH_AUDIO: "assets/audio/supr_mogo_app_launch_v6.wav",
    PAYMENT_DELIVERY_CONFIRMATION_AUDIO:
        "assets/audio/supr_mogo_app_launch_v6.wav",
    PN_AUDIO: "assets/audio/supr_push_notification_v6.wav",
};

export const PRELOAD_AUDIO_LIST = [
    {
        key: AUDIO_KEYS.APP_LAUNCH_AUDIO,
        asset: AUDIO_ASSETS.APP_LAUNCH_AUDIO,
    },
    {
        key: AUDIO_KEYS.PAYMENT_DELIVERY_CONFIRMATION_AUDIO,
        asset: AUDIO_ASSETS.PAYMENT_DELIVERY_CONFIRMATION_AUDIO,
    },
    {
        key: AUDIO_KEYS.PN_AUDIO,
        asset: AUDIO_ASSETS.PN_AUDIO,
    },
];
