export const ANIMATION_CLASSES = {
    BUBBLE: "bubble",
    BUBBLE_TEXT: "bubble-text",
    BG_LIGHT: "lightning",
    SLIDE_RIGHT: "slide-right",
};
