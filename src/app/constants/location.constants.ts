export const LOCATION_AUTHORIZATION_STATUS = {
    ios: {
        DISABLED: "disabled",
        AUTHORIZED: "authorized",
        UNAUTHORIZED: "unauthorized",
    },
    android: {
        DENIED: "denied",
    },
};
