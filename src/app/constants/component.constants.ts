export const INPUT = {
    TYPES: {
        TEXT: "text",
        PASSWORD: "password",
        NUMBER: "tel",
    },
    DEBOUNCE_TIME: 300,
};

export const OTP_INPUT = {
    DEBOUNCE_TIME: 0,
};

export const BUTTON_TYPES = {
    FILL: "fill",
    OUTLINE: "outline",
};

export const NUMBER_COUNTER_TYPES = {
    FLAT: "flat",
    DEFAULT: "default",
};

export const CITY_LIST = [
    {
        id: 6,
        name: "bangalore",
        value: "Bengaluru",
        state: "Karnataka",
        location: {
            lat: 12.97945,
            lng: 77.58721,
        },
    },
    {
        id: 1,
        name: "mumbai",
        value: "Mumbai",
        state: "Maharashtra",
        location: {
            lat: 19.083444,
            lng: 72.879126,
        },
    },
    {
        id: 3,
        name: "navi-mumbai",
        value: "Navi Mumbai",
        state: "Maharashtra",
        location: {
            lat: 19.035264,
            lng: 73.02874,
        },
    },
    {
        id: 2,
        name: "thane",
        value: "Thane",
        state: "Maharashtra",
        location: {
            lat: 19.217439,
            lng: 72.979144,
        },
    },
    {
        id: 4,
        name: "pune",
        value: "Pune",
        state: "Maharashtra",
        location: {
            lat: 18.524918,
            lng: 73.857817,
        },
    },
    {
        id: 5,
        name: "pimpri_chinchwad",
        value: "Pimpri Chinchwad",
        state: "Maharashtra",
        location: {
            lat: 18.630694,
            lng: 73.798102,
        },
    },
    {
        id: 8,
        name: "delhi",
        value: "Delhi",
        state: "",
        location: {
            lat: 28.62645,
            lng: 77.194229,
        },
    },
    {
        id: 9,
        name: "noida",
        value: "Noida",
        state: "Uttar Pradesh",
        location: {
            lat: 28.53271,
            lng: 77.391616,
        },
    },
    {
        id: 12,
        name: "ghaziabad",
        value: "Ghaziabad",
        state: "Uttar Pradesh",
        location: {
            lat: 28.673676,
            lng: 77.450968,
        },
    },
    {
        id: 10,
        name: "gurugram",
        value: "Gurugram",
        state: "Haryana",
        location: {
            lat: 28.462757,
            lng: 77.025876,
        },
    },
    {
        id: 7,
        name: "hyderabad",
        value: "Hyderabad",
        state: "Telangana",
        location: {
            lat: 17.389253,
            lng: 78.480137,
        },
    },
    {
        id: 11,
        name: "chennai",
        value: "Chennai",
        state: "Tamil Nadu",
        location: {
            lat: 13.086211,
            lng: 80.272233,
        },
    },
];

export const WEEK_DAYS_SELECTION_FORMAT = {
    DAILY: "Daily",
    WEEK_ENDS: "Weekends",
    WEEK_DAYS: "Weekdays",
    CUSTOM: "custom",
};

export const FREQUENCY_OPTIONS_MAP = {
    daily: {
        TEXT: "Daily",
        FREQUENCY: {
            sun: true,
            mon: true,
            tue: true,
            wed: true,
            thu: true,
            fri: true,
            sat: true,
        },
    },
    weekdays: {
        TEXT: "Weekdays",
        FREQUENCY: {
            sun: false,
            mon: true,
            tue: true,
            wed: true,
            thu: true,
            fri: true,
            sat: false,
        },
    },
    weekends: {
        TEXT: "Weekends",
        FREQUENCY: {
            sun: true,
            mon: false,
            tue: false,
            wed: false,
            thu: false,
            fri: false,
            sat: true,
        },
    },
};

export const FREQUENCY_OPTIONS = [
    {
        type: "daily",
        TEXT: "Daily",
        FREQUENCY: {
            sun: true,
            mon: true,
            tue: true,
            wed: true,
            thu: true,
            fri: true,
            sat: true,
        },
    },
    {
        type: "weekdays",
        TEXT: "Weekdays",
        FREQUENCY: {
            sun: false,
            mon: true,
            tue: true,
            wed: true,
            thu: true,
            fri: true,
            sat: false,
        },
    },
    {
        type: "weekends",
        TEXT: "Weekends",
        FREQUENCY: {
            sun: true,
            mon: false,
            tue: false,
            wed: false,
            thu: false,
            fri: false,
            sat: true,
        },
    },
    {
        type: "custom",
        TEXT: "Custom",
        FREQUENCY: {
            sun: false,
            mon: false,
            tue: false,
            wed: false,
            thu: false,
            fri: false,
            sat: false,
        },
    },
];

export const RECHARGE_OPTIONS = [3, 6, 15, 30, 60, 90];

export const RECHARGE_OPTIONS_MAP = {
    3: {
        label: "(Try us!)",
        labelType: "alert",
    },
    30: {
        label: "(Popular for this product)",
        labelType: "primary",
    },
};

export const DEFAULT_RECHARGE_OPTION = 30;

export const PROFILE_VALIDATION_TEXTS = {
    NAME_EMPTY: "First name can't be blank",
    EMAIL_EMPTY: "Email can't be blank",
    NAME_INVALID: "Invalid name",
    EMAIL_INVALID: "Invalid email",
};

export const SKU_PREFERRED_MODE = {
    ADDON: "ADDON",
    SUBSCRIPTION: "SUBSCRIPTION",
    DIRECT_SUBSCRIBE: "direct_subscribe",
    DIRECT_ADD: "direct_add",
};

export const QTY_SUBTEXTS = {
    ADDON: "Deliver once",
    SUBSCRIPTION: "Subscription",
};

export const DELIVERY_EDIT_MODAL_TEXTS = {
    EDIT: "Edit delivery",
    SCHEDULE: "Schedule using active subscription",
};

export const COMPONENT_WIDGET_TYPE = {
    all_in_one_horizontal_scroll: "supr-v3-container-carousel-widget",
    all_in_one_vertical_scroll: "supr-v3-container-grid-widget",

    sku_square_with_add_button: "supr-v3-sku-tile-variant-a-container",
    sku_circle_with_add_icon: "supr-v3-sku-tile-variant-b-container",
    sku_rectangle_with_item_of_the_data: "supr-v3-sku-tile-variant-c-container",

    collection_bar_with_image: "supr-v3-tile-variant-a",
    collection_square_with_bottom_text: "supr-v3-tile-variant-b",
    collection_square_with_image: "supr-v3-tile-variant-c",
    collection_circle_with_image: "supr-v3-tile-variant-d",
    collection_square_with_supr_day_timer: "supr-v3-tile-variant-e",
    collection_square_with_supr_day: "supr-v3-tile-variant-f",
    collection_square_with_supr_salary_day: "supr-v3-tile-variant-g",
};

export const DOM_SCROLL_ELEM = {
    catNavigateElem: "group-",
    catSkuElem: "sku-",
};

export enum FEATURED_CARD_CONSTANTS {
    HOME = "home",
    CATEGORY_DETAIL = "category_detail",
}

export const IMAGE_UPLOAD = {
    imageUrl:
        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/imageUpload.png",
};

export const AUTO_SCROLL_INFO = {
    prefix_collection: "collection-",
    prefix_home_category: "home-category_section",
    prefix_home_feature: "home-feature_section",
    pages: {
        home: "home",
    },
    query_param: {
        page: "page",
        type: "type",
        id: "id",
    },
};

export const SCROLL_TYPE = {
    collection: "collection",
    feature: "feature",
    category: "category",
};
