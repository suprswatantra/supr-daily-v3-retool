export const COMPLAINTS = {
    COMPLAINT_ACTION: "REACH US",
    COMPLAINT_ACTION_LABLE: "Need immediate help ?",
    COMPLAINT_SUPPORT_TITLE: "Need immediate help ?",
    COMPLAINT_SUPPORT_SUBTITLE:
        "Please reach out to our customer care agents to send relevant information to help you",
    CALL: "Call us",
    CHAT: "Chat with us",
    HELP: "Need help with anything?",
    ENABLED: true,
    NEW: "NEW",
    ACTIVE_COMPLAINT_STATUS: ["Reopened", "Open", "In-progress", "Resolved"],
    ACTIVE_COMPLAINT_NUMBER: 2,
    ACTIVE_COMPLAINT_DAYS: 3,
    ARCHIVE_COMPLAINT_COUNT: 20,
    COMPLAINT_SUPPORT: {
        DAYS: 2,
        PRE_DATE: {
            IS_CALL: false,
            IS_CHAT: false,
        },
        POST_DATE: {
            IS_CALL: false,
            IS_CHAT: false,
        },
    },
    HIDE_COMPLAINT_ACTIONS: false,
};
