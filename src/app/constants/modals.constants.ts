export const MODAL_TYPES = {
    BOTTOM_SHEET: "bottom-sheet",
    NO_WRAPPER: "no-wrapper",
};

export const ANIMATION_DELAY_IN_MS = 300;
