export const ADDRESS_TYPE = {
    INDIVIDUAL_HOUSE: "individualHouse",
    KNOWN_SOCIETY: "knownSociety",
    UNKNOWN_SOCIETY: "unknownSociety",
    SKIP: "skip",
};

export const INITIAL_ADDRESS_SERVICE_DATA = {
    placeId: "",
    searchTerm: "",
    location: null,
    societyId: null,
    formTowerId: null,
    apartmentType: "",
    searchResults: [],
    savedAddress: null,
    formAddressType: null,
    formattedAddress: null,
    societySearchTerm: "",
    apartmentFormData: null,
    societySearchResults: [],
    individualHouseFormData: null,
    reverseGeocode: null,
};

export const EMPTY_ADDRESS_DATA = {
    id: 0,
    type: "",
    floorNumber: "",
    societyName: "",
    buildingName: "",
    doorNumber: "",
    streetAddress: "",
    location: {
        latLong: {
            latitude: 0,
            longitude: 0,
        },
    },
    premiseId: 0,
    city: {
        id: 0,
        name: "",
    },
    instructions: "",
    societyId: 0,
    buildingId: 0,
    doorId: 0,
    from: "",
};

export const ADDRESS_FROM_PARAM = {
    CART: "cart",
    NUDGE: "nudge",
    CITY_SELECTION: "citySelection",
};
