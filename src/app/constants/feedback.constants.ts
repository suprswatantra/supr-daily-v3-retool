export const ORDER_FEEDBACK = {
    enabled: false,
    title: "How was today's order ?",
    subtitle: "Please rate your order below",
    addComment: "Add comment",
    commentLable: "Tell us more",
    commentPlaceHolder: "Type here",
    submit: "SUBMIT",
    proceed: "PROCEED",
    thumbsUp: "up",
    thumbsDown: "down",
    negativeTitle: "Sorry to hear that",
    contactSupport: "Raise a complaint",
    negativeSubtitle:
        "Please take a moment to tell us more about what went wrong",

    up: {
        defaultOptionsPosHeader: "Tell us what you liked about your order",
        optionsHeader: "What went well?",
    },
    down: {
        optionsHeader: "What can be improved?",
        defaultOptionsPosHeader: "Tell us what went wrong about your order",
    },
    feedbackCalenderTitle: "Tell us what went wrong",
    feedbackCalenderSubtitle:
        "Rate the items below to help us improve our science",
    feedbackCalenderThanks: "Thank you! We will work on your feedback",
    channel: "Customer_Experience",
    feedbackSuccess: "Thank you for the feedback",
    successTimer: 3000,
};

export const FEEDBACK_ANALYTICS_OBJECT_NAMES = {
    IMPRESSION: {
        ATTRIBUTE: "attribute",
        SKIP: "skip",
        THUMBS_UP: "thumbs-up",
        THUMBS_DOWN: "thumbs-down",
        SUBMIT: "submit",
        ADD_COMMENT: "add-comment",
        VIEW_COMMENT: "view-comment",
        CLOSE_COMMENT: "close-comment",
        PROCEED: "proceed",
        COMMENT: "comment-box",
        CALENDER_RATING: "tell-us-what-went-wrong",
        CONTACT_SUPPORT: "contact-support",
        THANK_YOU_FEEDBACK: "thank-you-for-feedback",
    },
};
