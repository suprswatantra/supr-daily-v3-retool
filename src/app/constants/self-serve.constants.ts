export const SELF_SERVE = {
    enabled: false,
    DATA_BOT_VERSION: 15,
    orderCalendarDays: 7,
    orderCalendarToSelfServeDays: 7,
    experiments: ["pseudoDropdown"],
    pseudoDropdown: "Select issue type",
    defaultDropdown: "none",
    summaryPage: {
        summaryHeader: "Review and Submit",
        editButton: "Edit",
        proceedButton: "Proceed",
        successToast: "Complaint(s) raised successfully",
    },
    selfServePage: {
        proceedButton: "Proceed",
        complaintTypesOptionsTitle: "Choose issue type",
        feedbackTitle: "What quality issue did u face?",
        otherQueries: "OTHER QUERIES",
        otherQueriesTitle: "Need help with something else ?",
        proceedButtonTitle: "Raise an issue",
    },
    deliveryPopup: {
        title: "Your delivery is on the way!",
        desc:
            "Our promise to you is delivery before 9 AM everyday and usually delay does not happen. If you don’t recieve your delivery by then, please raise an issue.",
        buttonText: "CLOSE",
    },
    agentComplaintMessage:
        "Thanks for sharing the above details. Please wait while we connect you to an agent, rest assured your issue will be resolved",
    agentFallbackMessage:
        "Please wait while we connect you to an agent, rest assured your issue will be resolved",
    issueRaised: "We have raised an issue",
    bannerConfig: {
        icon: "approve",
        subtitle:
            "Our team is working the issue. We assure you that we will address the issue in our best capacity.",
        title: "We have raised and issue(s)",
        type: "success",
    },
    imageUploadPage: {
        proceedButton: "Proceed",
        header: "Add image",
        title: "Images",
        subTitle: "Additional info required",
        textTitle: "Upload images for the following",
        textSubtitle:
            "Kindly highlight the issue in the uploaded image for faster resolution",
        success: "Image uploaded successfully. Please proceed",
        error: "Error in uploading. Redirecting to CC agent",
    },
    resumeChat: {
        resumeChatText: "Continue chat with agent",
        resumeChatCountMessage: "Unread messages",
        agentTransferResumeMinutes: 240,
        resumeChatEnabled: false,
    },
    faq: {
        chatEnabled: true,
        chat: "Chat with us",
        header: "Support & FAQs",
        chatheader: "Need more help",
        footerTitle: "Didn’t find what you were looking for?",
        title: "Need help with placing order",
        subTitle: "Please refer to these answers",
        chatTitle: "How can we help ?",
        chatSubTitle: "Please describe your issue below for faster resolution",
        description: "Describe issue*",
        uploadTitle: "Upload Images (Optional)",
        submit: "Submit",
        error: "This field is a mandatory field",
    },
    supportPage: {
        otherQueriesHeader: "Help with other queries",
    },
    isStaticFlowEnabled: true,
};
