export const T_PLUS_ONE = {
    CART: {
        ICON: "error",
        TITLE: "Earliest delivery date",
        SUBTITLE:
            "Thank you for choosing Supr Daily. We take a day to ensure smooth deliveries for every first order.",
    },
    SUBSCRIPTION: {
        TITLE: "Next day deliveries available after your first order.",
    },
    AVAILABLE_AFTER_FIRST_DELIVERY: "( Available after first delivery )",
};
