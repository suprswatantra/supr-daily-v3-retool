export enum SEARCH_INSTRUMENTATION_EVENT_TYPE {
    CLICK = "CLICK",
    CONVERSION = "CONVERSION",
}

export enum SEARCH_INSTRUMENTATION_EVENT_NAME {
    CLICK = "Product Added to Cart",
    CONVERSION = "Product Purchased",
}

export enum SEARCH_RESULT_TYPES {
    EXACT = "EXACT",
    ALTERNATE = "ALTERNATE",
}
