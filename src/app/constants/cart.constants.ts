export enum CART_ITEM_TYPES {
    ADDON = "addon",
    SUBSCRIPTION_NEW = "subscription_new",
    SUBSCRIPTION_RECHARGE = "subscription_recharge",
}

export enum CART_ITEM_ERROR_CODES {
    ITEM_UNAVAILABLE = 1,
    CUT_OFF_TIME_OVER = 2,
    INVALID_DATE = 3,
    BULK_ORDER = 5,
    SYSTEM_VACATION = 6,
}

export const LOCAL_CART_KEY = "_suprCart";

export enum CART_DISCOUNT_TYPES {
    DISCOUNT = "discount",
    CASH_BACK = "cashback",
}

export enum CART_LAYPUT_ELEMENT_IDS {
    ITEMS_LIST = "items-list",
    SUMMARY = "summary",
}

export enum CART_FOOTER_ACTION_TYPES {
    IN_APP_NAVIGATION = "inAppNavigation",
    OUTSIDE_NAVIGATION = "outsideNavigation",
    SCROLL = "scroll",
}
