export const BENEFITS_BANNER_CTA_ACTION_TYPES = {
    SHARE: "social-share",
    ROUTE: "route",
};

export const SOCIAL_PLATFORMS = {
    WHATS_APP: {
        NAME: "whatsapp",
        PACKAGE_NAME_IOS: "com.apple.social.whatsapp",
        DISPLAY_NAME: "WhatsApp",
    },
    FACEBOOK: {
        NAME: "facebook",
        PACKAGE_NAME_IOS: "com.apple.social.facebook",
        DISPLAY_NAME: "Facebook",
    },
    SMS: {
        NAME: "messages",
        DISPLAY_NAME: "SMS",
    },
    EMAIL: {
        NAME: "mail",
        DISPLAY_NAME: "Email",
    },
    TWITTER: {
        NAME: "twitter",
        PACKAGE_NAME_IOS: "com.apple.social.twitter",
        DISPLAY_NAME: "Twitter",
    },
    INSTAGRAM: {
        NAME: "instagram",
        PACKAGE_NAME_IOS: "com.apple.social.instagram",
        DISPLAY_NAME: "Instagram",
    },
};

export const COUNTRY_CODE = "+91";

export const SHARE_ERROR_MESSAGE =
    "Oops...something went wrong. Please try again.";
export const PLATFORM_ERROR_MESSAGE =
    " app not found. Please try another app to share.";
