import { TestBed } from "@angular/core/testing";

import { CallNumber } from "@ionic-native/call-number/ngx";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { AuthService } from "@services/data/auth.service";
import { PlatformService } from "@services/util/platform.service";
import { ErrorService } from "@services/integration/error.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import {
    BaseUnitTestImportsWithStoreIonic,
    BaseUnitTestProvidersIonic,
} from "../../unitTest.conf";
import { HttpReqInterceptor } from "./http.interceptor";

class CallNumberMock {}


describe("HttpInterceptor", () => {
    let httpReqInterceptor: HttpReqInterceptor;

    let authService: AuthService;
    let utilService: UtilService;
    let errorService: ErrorService;
    let platformService: PlatformService;
    let analyticsService: AnalyticsService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                ApiService,
                AuthService,
                UtilService,
                ErrorService,
                ...BaseUnitTestProvidersIonic,
                { provide: CallNumber, useClass: CallNumberMock },
            ],
        });

        authService = TestBed.get(AuthService);
        utilService = TestBed.get(UtilService);
        errorService = TestBed.get(ErrorService);
        platformService = TestBed.get(PlatformService);
        analyticsService = TestBed.get(AnalyticsService);


        httpReqInterceptor = new HttpReqInterceptor(
            authService,
            platformService,
            errorService,
            utilService,
            analyticsService
        );
    });

    // const httpCode = 200;
    // const method = "GET";
    // const responseTime = 2504;
    // const statusCode = 0;
    // const url =
    //     "http://development.pkzbqgwu2y.ap-south-1.elasticbeanstalk.com/v3/collection-views/?component=home_section_3";

    it("#should create http Interceptor instance", () => {
        expect(httpReqInterceptor).toBeTruthy();
    });

    // it("#should not call segment service trackApiEvent method when silent is true", () => {
    //     spyOn(segmentService, "trackApiEvent");
    //     (httpReqInterceptor as any).trackApiEvent(
    //         method,
    //         url,
    //         httpCode,
    //         responseTime,
    //         { statusCode },
    //         true
    //     );
    //     expect(segmentService.trackApiEvent).not.toHaveBeenCalled();
    // });

    // it("#should call segment service trackApiEvent method when silent is false", () => {
    //     spyOn(segmentService, "trackApiEvent");
    //     (httpReqInterceptor as any).trackApiEvent(
    //         method,
    //         url,
    //         httpCode,
    //         responseTime,
    //         { statusCode },
    //         false
    //     );
    //     expect(segmentService.trackApiEvent).toHaveBeenCalled();
    // });
});
