import { TestBed } from "@angular/core/testing";

import { UtilService } from "@services/util/util.service";
import { DbService } from "@services/data/db.service";
import { PlatformService } from "@services/util/platform.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";

import { ReferralService } from "./referral.service";

import {
    BaseUnitTestImportsIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class DbServiceMock {}
class PlatformServiceMock {}
class RouterServiceMock {}
class ToastServiceMock {}
describe("ReferralService", () => {
    let referralService: ReferralService;
    let utilService: UtilService;
    let dbService: DbService;
    let platformService: PlatformService;
    let routerService: RouterService;
    let toastService: ToastService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsIonic],

            providers: [
                { provide: DbService, useClass: DbServiceMock },
                { provide: PlatformService, useClass: PlatformServiceMock },
                { provide: RouterService, useClass: RouterServiceMock },
                { provide: ToastService, useClass: ToastServiceMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        platformService = TestBed.get(PlatformService);
        utilService = TestBed.get(UtilService);
        dbService = TestBed.get(DbService);
        routerService = TestBed.get(RouterService);
        toastService = TestBed.get(ToastService);

        referralService = new ReferralService(
            utilService,
            dbService,
            platformService,
            null,
            routerService,
            toastService
        );
    });

    it("#should create util service instance", () => {
        expect(referralService).toBeTruthy();
    });

    it("#should return phone number in correct format", () => {
        expect(
            referralService.getPhoneNumberWithCountryCode("08871815950")
        ).toBe("+918871815950");

        expect(
            referralService.getPhoneNumberWithCountryCode("8871815950")
        ).toBe("+918871815950");
    });
});
