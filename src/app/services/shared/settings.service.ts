import { Injectable } from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { Settings } from "@models";

import { AudioService } from "@services/util/audio.service";
import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { StoreService } from "@services/data/store.service";
import { ErrorService } from "@services/integration/error.service";
import { SegmentService } from "@services/integration/segment.service";
import { ExperimentsService } from "@services/shared/experiments.service";
import { RudderStackService } from "@services/integration/rudder-stack.service";
import { AnalyticsService } from "@services/integration/analytics.service";

@Injectable({
    providedIn: "root",
})
export class SettingsService {
    private settingsData: any;

    constructor(
        private audioService: AudioService,
        private apiService: ApiService,
        private utilService: UtilService,
        private errorService: ErrorService,
        private storeService: StoreService,
        private experimentsService: ExperimentsService,
        private segmentService: SegmentService,
        private rudderStackService: RudderStackService,
        private analyticsService: AnalyticsService
    ) {}

    updateSettings(settings: Settings) {
        this.settingsData = settings;
    }

    getSettingsData(): Settings {
        return this.settingsData;
    }

    getSettingsValue(key: string, defaultValue?: any): any {
        try {
            const experimentsData = this.experimentsService.getExperimentsData();
            const experimentsValue = this.utilService.getNestedValue(
                experimentsData,
                key
            );

            if (experimentsValue) {
                return experimentsValue;
            }

            const setttingsValue = this.utilService.getNestedValue(
                this.settingsData,
                key
            );

            if (setttingsValue) {
                return JSON.parse(setttingsValue);
            }

            if (defaultValue) {
                return defaultValue;
            }

            return SETTINGS_KEYS_DEFAULT_VALUE[key];
        } catch (_error) {
            this.errorService.logSentryError(_error, {
                message: "Error parsing settings config",
            });
            if (defaultValue) {
                return defaultValue;
            }

            return SETTINGS_KEYS_DEFAULT_VALUE[key];
        }
    }

    addSingleSettingBlock(key: string, value: any) {
        if (!key || !value) {
            return;
        }

        const settingsData = this.settingsData || {};
        settingsData[key] = JSON.stringify(value);

        this.updateSettings(settingsData);
    }

    fetchSettings() {
        return this.apiService
            .fetchSettings()
            .subscribe((settings: Settings) => {
                this.updateSettings(settings);
                this.storeService.setSettingsFetched(true);
                if (settings && settings[SETTINGS.EXCLUDE_EVENTS_DICT]) {
                    this.segmentService.setExcludeEventsDict(
                        JSON.parse(settings[SETTINGS.EXCLUDE_EVENTS_DICT])
                    );
                }
                if (settings && settings[SETTINGS.IMPRESSION_THROTTLE_CONFIG]) {
                    this.segmentService.setImpressionThrottleConfig(
                        JSON.parse(
                            settings[SETTINGS.IMPRESSION_THROTTLE_CONFIG]
                        )
                    );
                }

                this.setRudderstackData(settings);
                this.setAppAudioConfigs(settings);
                this.setErrorServiceConfigs();
                this.setAnalyticsConfig();
            });
    }

    private setRudderstackData(settings: Settings) {
        if (settings && settings[SETTINGS.RUDDERSTACK_SETTING_KEYS]) {
            const rudderStackKeyVal = this.getSettingsValue(
                SETTINGS.RUDDERSTACK_SETTING_KEYS
            );

            const excludeEvents = this.utilService.getValue(
                rudderStackKeyVal,
                SETTINGS.EXCLUDE_EVENTS_DICT
            );

            const impressionThrottlingConfig = this.utilService.getValue(
                rudderStackKeyVal,
                SETTINGS.IMPRESSION_THROTTLE_CONFIG
            );

            if (!this.utilService.isEmpty(excludeEvents)) {
                this.rudderStackService.setExcludeEventsDict(excludeEvents);
            }

            if (!this.utilService.isEmpty(impressionThrottlingConfig)) {
                this.rudderStackService.setImpressionThrottleConfig(
                    impressionThrottlingConfig
                );
            }
        }
    }

    private setAppAudioConfigs(settings) {
        if (settings && settings[SETTINGS.APP_AUDIO]) {
            const appAudio = JSON.parse(settings[SETTINGS.APP_AUDIO]);
            if (appAudio && appAudio.volume) {
                this.audioService.setSoundVolume(appAudio.value);
            }

            if (
                appAudio &&
                (appAudio.isEnabled || appAudio.isEnabled === false)
            ) {
                this.audioService.setAppAudioEnabledFlag(appAudio.isEnabled);
            }
        }
    }

    private setErrorServiceConfigs() {
        const sentryFilters = this.getSettingsValue(SETTINGS.SENTRY_FILTERS);
        const segmentFilters = this.getSettingsValue(
            SETTINGS.SEGMENT_ERROR_FILTERS
        );

        this.errorService.setSentryFiltersConfig(sentryFilters);
        this.errorService.setSegmentFiltersConfig(segmentFilters);
    }

    private setAnalyticsConfig() {
        const config = this.getSettingsValue("analyticsConfig", {});
        this.analyticsService.setAnalyticsConf(config);
    }
}
