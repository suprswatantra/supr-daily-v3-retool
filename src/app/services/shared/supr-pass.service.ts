import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class SuprPassService {
    private isSuprAccessMember = false;
    private suprAccessValidity = 0;

    setIsSuprAccessMember(isSuprAccessMember: boolean = false) {
        this.isSuprAccessMember = isSuprAccessMember;
    }

    getIsSuprAccessMember(): boolean {
        return this.isSuprAccessMember;
    }

    setSuprAccessValidity(suprAccessValidity: number = 0) {
        this.suprAccessValidity = suprAccessValidity;
    }

    getSuprAccessValidity(): number {
        return this.suprAccessValidity;
    }
}
