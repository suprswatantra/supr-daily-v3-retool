import { Injectable } from "@angular/core";

import { map, switchMap } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { ADDRESS_TYPE } from "@constants";

import { Address } from "@models";

import { UtilService } from "@services/util/util.service";
import { LocationService } from "@services/util/location.service";

import { SuprApi, SuprMaps } from "@types";

@Injectable({
    providedIn: "root",
})
export class MapService {
    private step: number;
    private address: Address;

    constructor(
        private locationService: LocationService,
        private utilService: UtilService
    ) {}

    getDefaultLocation(
        address: Address,
        step = 1
    ): Observable<SuprMaps.Location> {
        this.address = address;
        this.step = step;
        const searchTerm = this.getSearchTerm();

        if (!searchTerm) {
            return of(this.sendCityLocation());
        }

        return this.reverseGeoCode({ address: searchTerm }).pipe(
            switchMap((searchResponse: SuprMaps.Location) => {
                if (!searchResponse) {
                    return this.getDefaultLocation(this.address, this.step + 1);
                }
                return of(searchResponse);
            })
        );
    }

    private getSearchTerm(): string {
        if (this.address.type === ADDRESS_TYPE.UNKNOWN_SOCIETY) {
            return this.getUnknownSocietySearchTerm();
        }

        return this.getIndividualHouseSearchTerm();
    }

    private getUnknownSocietySearchTerm(): string {
        switch (this.step) {
            case 1:
                return `${this.address.doorNumber || ""}+${this.address
                    .buildingName || ""}+${this.address.societyName ||
                    ""}+${this.address.streetAddress || ""}+${
                    this.address.city.name
                }`;
            case 2:
                return `${this.address.buildingName || ""}+${this.address
                    .societyName || ""}+${this.address.streetAddress || ""}+${
                    this.address.city.name
                }`;
            case 3:
                return `${this.address.societyName || ""}+${this.address
                    .streetAddress || ""}+${this.address.city.name}`;
            case 4:
                return `${this.address.streetAddress || ""}+${
                    this.address.city.name
                }`;
            default:
                return null;
        }
    }

    private getIndividualHouseSearchTerm(): string {
        switch (this.step) {
            case 1:
                return `${this.address.floorNumber || ""}+${this.address
                    .doorNumber || ""}+${this.address.streetAddress || ""}+${
                    this.address.city.name
                }`;
            case 2:
                return `${this.address.doorNumber || ""}+${this.address
                    .streetAddress || ""}+${this.address.city.name}`;
            case 3:
                return `${this.address.streetAddress || ""}+${
                    this.address.city.name
                }`;
            default:
                return null;
        }
    }

    private reverseGeoCode(
        geocodeInput: SuprMaps.GeocodeInput
    ): Observable<SuprMaps.Location> {
        return this.locationService.reverseGeoCode(geocodeInput).pipe(
            map((geocodeResponse: SuprApi.GeoCodeRes) => {
                return this.utilService.getNestedValue(
                    geocodeResponse,
                    "data.results.0.geometry.location"
                );
            })
        );
    }

    private sendCityLocation(): SuprMaps.Location {
        const cityId = this.utilService.getNestedValue(this.address, "city.id");

        return this.locationService.getCityLocation(cityId);
    }
}
