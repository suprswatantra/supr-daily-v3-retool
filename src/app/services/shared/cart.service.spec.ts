import { TestBed } from "@angular/core/testing";

import { addDays, format } from "date-fns";

import { CartService } from "@services/shared/cart.service";
import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";
import { DbService } from "@services/data/db.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { NudgeService } from "@services/layout/nudge.service";
import { BlockAccessService } from "./block-access.service";

import { StoreService } from "../data/store.service";

import SkuData from "@stubs/sku.stub";

import {
    BaseUnitTestImportsIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class DateServiceMock {
    textFromDate() {}
    getTomorrowDate() {}
}
class CalendarServiceMock {
    isDeliveryDateValid() {}
}
class DbServiceMock {
    getData(_key) {
        return Promise.resolve(null);
    }
}
class StoreServiceMock {}
class SettingsServiceMock {}
class UtilServiceMock {}
class SkuServiceMock {
    isSkuAvailableOnGivenDate() {}
}
class NudgeServiceMock {}

describe("CartStoreEffects", () => {
    let cartService: CartService;

    let dateService: DateService;
    let calendarService: CalendarService;
    let dbService: DbService;
    let storeService: StoreService;
    let settingsService: SettingsService;
    let utilsService: UtilService;
    let skuService: SkuService;
    let nudgeService: NudgeService;
    let blockAccessService: BlockAccessService;

    let spy: any;

    console.log(spy);

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsIonic],

            providers: [
                { provide: DateService, useClass: DateServiceMock },
                { provide: CalendarService, useClass: CalendarServiceMock },
                { provide: DbService, useClass: DbServiceMock },
                { provide: StoreService, useClass: StoreServiceMock },
                { provide: SettingsService, useClass: SettingsServiceMock },
                { provide: UtilService, useClass: UtilServiceMock },
                { provide: SkuService, useClass: SkuServiceMock },
                { provide: NudgeService, useClass: NudgeServiceMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        dateService = TestBed.get(DateService);
        calendarService = TestBed.get(CalendarService);
        dbService = TestBed.get(DbService);
        storeService = TestBed.get(StoreService);
        settingsService = TestBed.get(SettingsService);
        utilsService = TestBed.get(UtilService);
        skuService = TestBed.get(SkuService);
        nudgeService = TestBed.get(NudgeService);
        blockAccessService = TestBed.get(BlockAccessService);

        cartService = new CartService(
            dateService,
            calendarService,
            dbService,
            storeService,
            settingsService,
            utilsService,
            skuService,
            nudgeService,
            blockAccessService
        );
    });

    const tomorrowDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
    const cartItems = [
        {
            type: "addon",
            sku_id: 1,
            delivery_date: tomorrowDate,
            quantity: 1,
            coupon_code: "ADD1",
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 44,
                post_discount: 1,
                discount: 0,
            },
        },
        {
            recharge_quantity: 6,
            sku_id: 2539,
            subscription_id: 1915518,
            type: "subscription_recharge",
        },
    ];
    const skuDict = {
        1: SkuData[0],
        2539: SkuData[1],
    };

    it("#should create cart service instance", () => {
        expect(cartService).toBeTruthy();
    });

    it("#autoPushDeliveryDates should return cart items", () => {
        spyOn(calendarService, "isDeliveryDateValid").and.returnValue(true);
        spyOn(skuService, "isSkuAvailableOnGivenDate").and.returnValue(true);
        const newCartItems = cartService.autoPushDeliveryDates(
            cartItems,
            null,
            skuDict
        );
        expect(newCartItems).toEqual(cartItems);
    });

    it("#autoPushDeliveryDates should add tomorrow's date to items missing delivery/start date \
     and log error to sentry", () => {
        spyOn(calendarService, "isDeliveryDateValid").and.returnValue(true);
        spyOn(skuService, "isSkuAvailableOnGivenDate").and.returnValue(true);
        spyOn(dateService, "textFromDate").and.returnValue(tomorrowDate);
        const cartItemsWithoutDate = [
            {
                type: "addon",
                sku_id: 1,
                quantity: 1,
                coupon_code: "ADD1",
                validation: {
                    is_valid: true,
                    message: "Ok",
                },
                payment_data: {
                    pre_discount: 44,
                    post_discount: 1,
                    discount: 0,
                },
            },
        ];
        const newCartItems = cartService.autoPushDeliveryDates(
            cartItemsWithoutDate,
            null,
            skuDict
        );
        const expectedCartItems = [
            {
                type: "addon",
                sku_id: 1,
                delivery_date: tomorrowDate,
                quantity: 1,
                coupon_code: "ADD1",
                validation: {
                    is_valid: true,
                    message: "Ok",
                },
                payment_data: {
                    pre_discount: 44,
                    post_discount: 1,
                    discount: 0,
                },
            },
        ];
        expect(newCartItems).toEqual(expectedCartItems);
    });
});
