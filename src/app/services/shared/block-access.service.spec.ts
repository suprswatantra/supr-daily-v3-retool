import { TestBed } from "@angular/core/testing";

import { StoreService } from "@services/data/store.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "./settings.service";
import { BlockAccessService } from "./block-access.service";

import {
    BaseUnitTestImportsWithStoreIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class StoreServiceMock {}

describe("BlockAccessService", () => {
    let blockAccessService: BlockAccessService;

    let utilService: UtilService;

    let settingsService: SettingsService;

    let storeService: StoreService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                UtilService,
                SettingsService,

                { provide: StoreService, useClass: StoreServiceMock },

                ...BaseUnitTestProvidersIonic,
            ],
        });

        utilService = TestBed.get(UtilService);

        storeService = TestBed.get(StoreService);

        settingsService = TestBed.get(SettingsService);

        blockAccessService = new BlockAccessService(
            storeService,
            utilService,
            settingsService
        );
    });

    it("#should create block access service instance", () => {
        expect(blockAccessService).toBeTruthy();
    });

    it("#should not block access in case of future deliveries when exp name restrict_future_deliveries and supr pass is active", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_future_deliveries",
        });
        blockAccessService.setIsSuprPassActive(true);
        console.log(blockAccessService.getBlockAccessExpData());
        expect(blockAccessService.isFutureDeliveryAccessRestricted()).toEqual(
            false
        );
    });

    it("#should block access in case of future deliveries when exp name restrict_future_deliveries and supr pass is not active", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_future_deliveries",
        });
        blockAccessService.setIsSuprPassActive(false);
        expect(blockAccessService.isFutureDeliveryAccessRestricted()).toEqual(
            true
        );
    });

    it("#should not block Future deliveries access in case of exp data is absent", () => {
        blockAccessService.setBlockAccessExpData(null);
        blockAccessService.setIsSuprPassActive(false);
        expect(blockAccessService.isFutureDeliveryAccessRestricted()).toEqual(
            false
        );
    });

    it("#should not block access in case of subscriptions when exp name restrict_subscription_creation and supr pass is active", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_subscription_creation",
        });
        blockAccessService.setIsSuprPassActive(true);
        expect(blockAccessService.isSubscriptionAccessRestricted()).toEqual(
            false
        );
    });

    it("#should block access in case of subscriptions when exp name restrict_subscription_creation and supr pass is not active", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_subscription_creation",
        });
        blockAccessService.setIsSuprPassActive(false);
        expect(blockAccessService.isSubscriptionAccessRestricted()).toEqual(
            true
        );
    });

    it("#should not block Subscription access in case of exp data is absent", () => {
        blockAccessService.setBlockAccessExpData(null);
        blockAccessService.setIsSuprPassActive(false);
        expect(blockAccessService.isSubscriptionAccessRestricted()).toEqual(
            false
        );
    });

    it("#should be true in case of time based exp is active", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_subscription_activity_time_based",
        });
        blockAccessService.setIsSuprPassActive(false);
        expect(blockAccessService.isTimeBasedExperimentActive()).toEqual(true);
    });

    it("#should be false in case of time based exp is active, but user has supr access", () => {
        blockAccessService.setBlockAccessExpData({
            name: "restrict_subscription_activity_time_based",
        });
        blockAccessService.setIsSuprPassActive(true);
        expect(blockAccessService.isTimeBasedExperimentActive()).toEqual(false);
    });
});
