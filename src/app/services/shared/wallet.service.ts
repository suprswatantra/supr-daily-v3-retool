import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class WalletService {
    private walletBalance = 0;
    private scBalance = 0;

    setWalletBalance(balance: number = 0) {
        this.walletBalance = balance;
    }

    getWalletBalance(): number {
        return this.walletBalance;
    }

    setSuprCreditsBalance(balance: number = 0) {
        this.scBalance = balance;
    }

    getSuprCreditsBalance(): number {
        return this.scBalance;
    }
}
