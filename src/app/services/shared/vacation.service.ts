import { Injectable } from "@angular/core";

import { Vacation, SystemVacation } from "@shared/models";
import { DateService } from "@services/date/date.service";

@Injectable({
    providedIn: "root",
})
export class VacationService {
    constructor(private dateService: DateService) {}

    isValidVacation(vacation: Vacation | SystemVacation): boolean {
        return !!(vacation && vacation.start_date && vacation.end_date);
    }

    isDateInVacation(
        date: Date | string,
        vacation: Vacation | SystemVacation
    ): boolean {
        if (!this.isValidVacation(vacation)) {
            return false;
        }

        let dateToCheck: Date;

        const endDate = this.dateService.dateFromText(vacation.end_date);
        const startDate = this.dateService.dateFromText(vacation.start_date);

        if (typeof date === "string") {
            dateToCheck = this.dateService.dateFromText(date);
        } else {
            dateToCheck = date as Date;
        }

        const startDiff = this.dateService.daysBetweenTwoDates(
            startDate,
            dateToCheck
        );

        const endDiff = this.dateService.daysBetweenTwoDates(
            dateToCheck,
            endDate
        );

        return startDiff >= 0 && endDiff >= 0;
    }

    isTomorrowInVacation(vacation: Vacation | SystemVacation): boolean {
        if (!this.isValidVacation(vacation)) {
            return false;
        }

        const tomorrow = this.dateService.getTomorrowDate();

        return this.isDateInVacation(tomorrow, vacation);
    }
}
