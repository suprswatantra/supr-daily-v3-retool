import { Injectable } from "@angular/core";

import {
    SUPR_PASS_BLOCK_ACCESS,
    SETTINGS_KEYS_DEFAULT_VALUE,
    SETTINGS,
} from "@constants";

import {
    SuprPassExperimentInfo,
    SuprPassExperimentCutOffTime,
} from "@shared/models";

import { StoreService } from "@services/data/store.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "./settings.service";

@Injectable({
    providedIn: "root",
})
export class BlockAccessService {
    private blockAccessExpData: SuprPassExperimentInfo;
    private expName: string;
    private isSuprPassActive: boolean;

    constructor(
        private storeService: StoreService,
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    getBlockAccessExpData(): SuprPassExperimentInfo {
        return this.blockAccessExpData;
    }

    /**
     * All the access restrictions are based on expInfo of Supr Pass
     *
     * @param {SuprPassExperimentInfo} expInfo
     * @memberof BlockAccessService
     */
    setBlockAccessExpData(expInfo: SuprPassExperimentInfo) {
        this.blockAccessExpData = expInfo;
        this.expName = this.utilService.getNestedValue(
            this.blockAccessExpData,
            "name",
            null
        );
    }

    /**
     * No Access block in case user already has an active Supr Pass
     *
     * @param {boolean} isActive
     * @memberof BlockAccessService
     */
    setIsSuprPassActive(isActive: boolean) {
        this.isSuprPassActive = isActive;
    }

    /**
     * for all create subscriptions, recharge existing subscriptions flows
     *
     * @returns {boolean}
     * @memberof BlockAccessService
     */
    isSubscriptionAccessRestricted(): boolean {
        if (!this.expName || this.isSuprPassActive) {
            return false;
        }

        return (
            this.expName ===
            SUPR_PASS_BLOCK_ACCESS.RESTRICT_SUBSCRIPTION_CREATION
        );
    }

    /**
     * For all OOS flows where next_delivery_date is present, future deliveries in calendar
     * and start date restrictions in create subscription flow
     *
     * @returns {boolean}
     * @memberof BlockAccessService
     */
    isFutureDeliveryAccessRestricted(): boolean {
        if (!this.expName || this.isSuprPassActive) {
            return false;
        }

        return (
            this.expName === SUPR_PASS_BLOCK_ACCESS.RESTRICT_FUTURE_DELIVERIES
        );
    }

    /**
     * Checks for user's validity for new cutoff time driven by experiment
     *
     * @returns {boolean}
     * @memberof BlockAccessService
     */
    isTimeBasedExperimentActive(): boolean {
        if (!this.expName || this.isSuprPassActive) {
            return false;
        }

        return (
            this.expName ===
            SUPR_PASS_BLOCK_ACCESS.RESTRICT_SUBSCRIPTION_ACTIVITY_TIME_BASED
        );
    }

    /**
     * Called by components where info text is shown with new cutoff time
     *
     * @returns {string}
     * @memberof BlockAccessService
     */
    getCalendarInfoText(): string {
        if (!this.isTimeBasedExperimentActive()) {
            return "";
        }

        const timeBasedSuprPassExperimentData = this.settingsService.getSettingsValue(
            SETTINGS.TIME_BASED_EXPERIMENT_DATA,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.TIME_BASED_EXPERIMENT_DATA]
        );

        const cutOffTimeString = this.utilService.getNestedValue(
            this.blockAccessExpData,
            "cutOffTime.value",
            null
        );

        const timeText = this.utilService.timeConvert(cutOffTimeString);

        return timeBasedSuprPassExperimentData.text + " " + timeText;
    }

    /**
     * This take priority over the normal app cutoff time, used by DateService
     *
     * @returns {SuprPassExperimentCutOffTime}
     * @memberof BlockAccessService
     */
    getExperimentDrivenCutOffTime(): SuprPassExperimentCutOffTime {
        if (!this.isTimeBasedExperimentActive()) {
            return null;
        }

        const cutOffTime: SuprPassExperimentCutOffTime = this.utilService.getNestedValue(
            this.blockAccessExpData,
            "cutOffTime",
            null
        );

        return cutOffTime;
    }

    showBlockAcessNudge(showSkipBtn = false) {
        this.storeService.showBlockAccessNudge(showSkipBtn);
    }
}
