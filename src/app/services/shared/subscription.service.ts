import { Injectable } from "@angular/core";

import {
    CART_ITEM_TYPES,
    SUBSCRIPTION_EXPIRY_DAYS_LIMIT,
    SUBSCRIPTION_RECHARGE_DAYS_LIMIT,
    FREQUENCY_OPTIONS_MAP,
} from "@constants";
import {
    CartItem,
    Subscription,
    SubscriptionSplit,
    SubscriptionInfoPriorityChip,
    SuprPassPlan,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";

import { SuprApi, SubscriptionServiceData } from "@types";

@Injectable({
    providedIn: "root",
})
export class SubscriptionService {
    private subscriptionData: SubscriptionServiceData = {};

    constructor(
        private dateService: DateService,
        private utilService: UtilService
    ) {}

    public splitSubscriptionsIntoActiveAndPast(
        subscriptions: Subscription[]
    ): SubscriptionSplit {
        const split = {
            active: [],
            expired: [],
        };

        if (this.utilService.isEmpty(subscriptions)) {
            return split;
        }

        subscriptions.forEach((subscription) => {
            const isExpired = this.isExpiredSubscription(subscription);
            if (isExpired) {
                split.expired.push(subscription);
            } else {
                split.active.push(subscription);
            }
        });

        this.sortActiveSubscriptions(split.active);
        this.sortExpiredSubscriptions(split.expired);

        return split;
    }

    public splitSubscriptionsBasedOnStatus(
        subscriptions: Subscription[]
    ): SubscriptionSplit {
        const split = {
            active: [],
            rechargePending: [],
            ended: [],
            expired: [],
        };

        subscriptions.forEach((subscription) => {
            const daysRemaining = this.getSubscriptionDaysRemaining(
                subscription
            );

            if (this.isExpired(daysRemaining)) {
                split.expired.push(subscription);
            } else if (this.isEnded(daysRemaining)) {
                split.ended.push(subscription);
            } else if (this.isRechargeReady(daysRemaining)) {
                split.rechargePending.push(subscription);
            } else {
                split.active.push(subscription);
            }
        });

        this.sortActiveSubscriptions(split.active);
        this.sortActiveSubscriptions(split.rechargePending);
        this.sortExpiredSubscriptions(split.expired);

        return split;
    }

    public isExpiredSubscription(subscription: Subscription): boolean {
        const daysRemaining = this.getSubscriptionDaysRemaining(subscription);
        return daysRemaining <= SUBSCRIPTION_EXPIRY_DAYS_LIMIT;
    }

    public isRechargeReadySubscription(subscription: Subscription): boolean {
        const daysRemaining = this.getSubscriptionDaysRemaining(subscription);
        return (
            daysRemaining <= SUBSCRIPTION_RECHARGE_DAYS_LIMIT &&
            daysRemaining > SUBSCRIPTION_EXPIRY_DAYS_LIMIT
        );
    }

    public getSubscriptionType(subscription: Subscription): string {
        if (this.utilService.isEmpty(subscription)) {
            return "";
        } else if (this.isExpiredSubscription(subscription)) {
            return "expired";
        } else if (this.isRechargeReadySubscription(subscription)) {
            return "recharge";
        } else {
            return "active";
        }
    }

    public getExpireDaysMessageText(subscription: Subscription): string {
        const daysRemaining = this.getSubscriptionDaysRemaining(subscription);
        if (daysRemaining === 0) {
            return "Ending today";
        }

        const dayLiteral = Math.abs(daysRemaining) === 1 ? "day" : "days";
        if (daysRemaining > 0) {
            return `Ending in ${daysRemaining} ${dayLiteral}`;
        }

        return `Ended ${Math.abs(daysRemaining)} ${dayLiteral} ago`;
    }

    public getSubscriptionDaysRemaining(subscription: Subscription): number {
        const lastDate = this.utilService.getNestedValue(
            subscription,
            "delivery_dates.last"
        );

        if (!lastDate) {
            return SUBSCRIPTION_EXPIRY_DAYS_LIMIT;
        }

        const lastDeliveryDate = this.dateService.dateFromText(lastDate);
        return this.dateService.daysFromToday(lastDeliveryDate);
    }

    public getSubscriptionRechargeCartItem(
        subscription: Subscription,
        rechargeQty: number,
        isSuprPassSku?: boolean,
        selectedPlan?: SuprPassPlan
    ): CartItem {
        if (!subscription || !rechargeQty) {
            return null;
        }
        const item: CartItem = {
            type: CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE,
            subscription_id: subscription.id,
            recharge_quantity: rechargeQty * subscription.quantity,
            sku_id: subscription.sku_id,
        };

        if (isSuprPassSku) {
            item.plan_id = this.utilService.getNestedValue(selectedPlan, "id");
            item.recharge_quantity = this.utilService.getNestedValue(
                selectedPlan,
                "durationDays"
            );
            item.sku_id = this.utilService.getNestedValue(
                subscription,
                "sku_id"
            );
            item.type = CART_ITEM_TYPES.SUBSCRIPTION_NEW;
            item.quantity = 1;
            item.start_date = this.getSuprPassStartDate(subscription);
            item.frequency = FREQUENCY_OPTIONS_MAP.daily.FREQUENCY;
        }

        return item;
    }

    private getSuprPassStartDate(subscription: Subscription): string {
        const suprPassEndDateString = this.utilService.getNestedValue(
            subscription,
            "delivery_dates.last",
            null
        );

        if (!suprPassEndDateString) {
            return null;
        }

        const suprPassEndDate = this.dateService.dateFromText(
            suprPassEndDateString
        );
        const noOfDaysToAdd = 1;

        const startDate = this.dateService.addDays(
            suprPassEndDate,
            noOfDaysToAdd
        );
        return this.dateService.textFromDate(startDate);
    }

    public getSubscriptionRechargeValidateCartBody(
        subscription: Subscription,
        rechargeQty: number,
        couponCode?: string,
        isSuprPassSku?: boolean,
        selectedPlan?: SuprPassPlan
    ): SuprApi.CartBody {
        const item = this.getSubscriptionRechargeCartItem(
            subscription,
            rechargeQty,
            isSuprPassSku,
            selectedPlan
        );

        const items = [item];
        const body = { items } as SuprApi.CartBody;

        if (couponCode) {
            body.coupon_code = couponCode;
        }

        return body;
    }

    public getPrioritySubcriptionInfoChip(
        subscriptionSplit: SubscriptionSplit
    ): SubscriptionInfoPriorityChip | null {
        if (
            this.utilService.isLengthyArray(subscriptionSplit.rechargePending)
        ) {
            return this.getSubscriptionChipData(
                subscriptionSplit.rechargePending,
                "pending"
            );
        } else if (this.utilService.isLengthyArray(subscriptionSplit.ended)) {
            return this.getSubscriptionChipData(
                subscriptionSplit.ended,
                "ended"
            );
        } else if (this.utilService.isLengthyArray(subscriptionSplit.active)) {
            return this.getSubscriptionChipData(
                subscriptionSplit.active,
                "active"
            );
        } else {
            return null;
        }
    }

    public setSubscriptionDetailsInfo(subscription: Subscription) {
        this.subscriptionData.details = subscription;
    }

    public getSubscriptionDetailsInfo(): Subscription {
        return this.subscriptionData.details;
    }

    private getSubscriptionChipData(
        subscriptionList: Subscription[],
        type: "ended" | "pending" | "active"
    ): SubscriptionInfoPriorityChip {
        const subscriptionLength = subscriptionList.length;
        const chipData = {
            type,
            count: subscriptionLength,
            message: "",
            iconName: "",
        };

        const pluralStr = subscriptionLength > 1 ? "s" : "";

        if (type === "ended") {
            chipData.message = `${subscriptionLength} Subscription${pluralStr} ended`;
            chipData.iconName = "error";
        } else if (type === "pending") {
            chipData.message = `${subscriptionLength} Subscription${pluralStr} ending`;
            chipData.iconName = "error_triangle";
        } else {
            chipData.message = `${subscriptionLength} Active subscription${pluralStr}`;
            chipData.iconName = "";
        }

        return chipData;
    }

    private sortActiveSubscriptions(subscriptions: Subscription[]) {
        subscriptions.sort((a, b) => {
            return (
                a.deliveries_remaining - b.deliveries_remaining ||
                this.compareSubscriptionsByLastDeliveryDate(a, b)
            );
        });
    }

    private sortExpiredSubscriptions(subscriptions: Subscription[]) {
        subscriptions.sort((a, b) => {
            return this.compareSubscriptionsByLastDeliveryDate(a, b);
        });
    }

    private compareSubscriptionsByLastDeliveryDate(
        a: Subscription,
        b: Subscription
    ): number {
        const aDaysRemaining = this.getSubscriptionDaysRemaining(a);
        const bDaysRemaining = this.getSubscriptionDaysRemaining(b);
        return aDaysRemaining - bDaysRemaining;
    }

    private isExpired(daysRemaining: number): boolean {
        return daysRemaining < SUBSCRIPTION_EXPIRY_DAYS_LIMIT;
    }

    private isEnded(daysRemaining: number): boolean {
        return (
            daysRemaining >= SUBSCRIPTION_EXPIRY_DAYS_LIMIT && daysRemaining < 0
        );
    }

    private isRechargeReady(daysRemaining: number): boolean {
        return (
            daysRemaining <= SUBSCRIPTION_RECHARGE_DAYS_LIMIT &&
            daysRemaining > SUBSCRIPTION_EXPIRY_DAYS_LIMIT
        );
    }
}
