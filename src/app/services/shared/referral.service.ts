import { Injectable } from "@angular/core";

import { SocialSharing } from "@ionic-enterprise/social-sharing/ngx";
import { Contact } from "@ionic-enterprise/contacts";

import {
    STORAGE_DB_DATA_KEYS,
    BENEFITS_BANNER_CTA_ACTION_TYPES,
    SOCIAL_PLATFORMS,
    PLATFORM_ERROR_MESSAGE,
    COUNTRY_CODE,
    SHARE_ERROR_MESSAGE,
} from "@constants";

import {
    ReferralInfo,
    ReferralMessage,
    BenefitsBannerCtaAction,
    ContactCard,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { DbService } from "@services/data/db.service";
import { PlatformService } from "@services/util/platform.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";

@Injectable()
export class ReferralService {
    constructor(
        private utilService: UtilService,
        private dbService: DbService,
        private platformService: PlatformService,
        private socialSharing: SocialSharing,
        private routerService: RouterService,
        private toastService: ToastService
    ) {}

    async canOpenReferralSuccessModal(
        referralInfo: ReferralInfo
    ): Promise<boolean> {
        const successfulReferralCountFromDb = await this.getSuccessfulReferralsFromDb();
        const successfulReferralCountFromApi = this.getNumberOfSuccessfulReferrals(
            referralInfo
        );

        if (successfulReferralCountFromApi > successfulReferralCountFromDb) {
            return true;
        }

        return false;
    }

    async getSuccessfulReferralsFromDb(): Promise<number> {
        const successfulReferralCount = await this.dbService.getData(
            STORAGE_DB_DATA_KEYS.SUCCESSFUL_REFERRALS_COUNT
        );

        return successfulReferralCount || 0;
    }

    async setSuccessfulReferralsToDb(referralInfo: ReferralInfo) {
        const successfulReferralCount = this.getNumberOfSuccessfulReferrals(
            referralInfo
        );

        if (!successfulReferralCount) {
            return;
        }

        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.SUCCESSFUL_REFERRALS_COUNT,
            successfulReferralCount
        );
    }

    /**
     * Opens native share bottomsheet
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    genericShare(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        this.socialSharing
            .share(
                sharingOptions.message,
                sharingOptions.subject,
                null,
                sharingOptions.url
            )
            .then(() => {})
            .catch((_error) => {
                this.toastService.present(SHARE_ERROR_MESSAGE);
            });
    }

    /**
     * Shares message to a specific whatsapp contact. If platform is available, error handling [no number]
     * is handled by platform itself
     * Graceful exception handeling
     *
     * @param {ReferralInfo} referralInfo
     * @param {string} phoneNumber
     * @returns
     * @memberof ReferralService
     */
    shareViaWhatsAppToContact(referralInfo: ReferralInfo, reciever: string) {
        if (!this.platformService.isCordova()) {
            return;
        }

        if (!reciever) {
            this.shareViaWhatsApp(referralInfo);
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        if (this.platformService.isIOS()) {
            this.socialSharing
                .shareViaWhatsAppToReceiver(
                    reciever,
                    sharingOptions.message,
                    null,
                    sharingOptions.url
                )
                .then(() => {})
                .catch((_error) => {
                    this.toastService.present(SHARE_ERROR_MESSAGE);
                });
        } else {
            const phoneNumberWithCountryCode = this.getPhoneNumberWithCountryCode(
                reciever
            );

            this.socialSharing
                .canShareVia(SOCIAL_PLATFORMS.WHATS_APP.NAME)
                .then(() => {
                    this.socialSharing
                        .shareViaWhatsAppToReceiver(
                            phoneNumberWithCountryCode,
                            sharingOptions.message,
                            null,
                            sharingOptions.url
                        )
                        .then(() => {})
                        .catch((_error) => {
                            this.toastService.present(SHARE_ERROR_MESSAGE);
                        });
                })
                .catch((_error) => {
                    this.toastService.present(
                        this.getPlatformErrorMessage(
                            SOCIAL_PLATFORMS.WHATS_APP.DISPLAY_NAME
                        )
                    );
                });
        }
    }

    /**
     * Open share mode in whatsapp
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaWhatsApp(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        if (this.platformService.isIOS()) {
            this.socialSharing
                .shareViaWhatsApp(
                    sharingOptions.message,
                    null,
                    sharingOptions.url
                )
                .then(() => {})
                .catch((_error) => {
                    this.toastService.present(SHARE_ERROR_MESSAGE);
                });
        } else {
            this.socialSharing
                .canShareVia(SOCIAL_PLATFORMS.WHATS_APP.NAME)
                .then(() => {
                    this.socialSharing
                        .shareViaWhatsApp(
                            sharingOptions.message,
                            null,
                            sharingOptions.url
                        )
                        .then(() => {})
                        .catch((_error) => {
                            this.toastService.present(SHARE_ERROR_MESSAGE);
                        });
                })
                .catch((_error) => {
                    this.toastService.present(
                        this.getPlatformErrorMessage(
                            SOCIAL_PLATFORMS.WHATS_APP.DISPLAY_NAME
                        )
                    );
                });
        }
    }

    /**
     * Share msg via native sms client
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaSms(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        this.socialSharing
            .shareViaSMS(sharingOptions.message, null)
            .then(() => {})
            .catch((_error) => {
                this.toastService.present(
                    this.getPlatformErrorMessage(
                        SOCIAL_PLATFORMS.SMS.DISPLAY_NAME
                    )
                );
            });
    }

    /**
     * Checks if sharing via email is possible, shares email
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaEmail(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        this.socialSharing
            .canShareViaEmail()
            .then(() => {
                this.socialSharing
                    .shareViaEmail(
                        sharingOptions.message,
                        sharingOptions.subject,
                        null
                    )
                    .then(() => {})
                    .catch((_error) => {
                        this.toastService.present(SHARE_ERROR_MESSAGE);
                    });
            })
            .catch((_error) => {
                this.toastService.present(
                    this.getPlatformErrorMessage(
                        SOCIAL_PLATFORMS.EMAIL.DISPLAY_NAME
                    )
                );
            });
    }

    /**
     * Share on FB
     * Note: Message is trimmed at FBs end is not allowed, for now we are just using the link in share block
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaFacebook(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        if (this.platformService.isIOS()) {
            this.socialSharing
                .shareViaFacebook(
                    sharingOptions.message,
                    null,
                    sharingOptions.url
                )
                .then(() => {})
                .catch((_error) => {
                    this.toastService.present(SHARE_ERROR_MESSAGE);
                });
        } else {
            this.socialSharing
                .canShareVia(SOCIAL_PLATFORMS.FACEBOOK.NAME)
                .then(() => {
                    this.socialSharing
                        .shareViaFacebook(
                            sharingOptions.message,
                            null,
                            sharingOptions.url
                        )
                        .then(() => {})
                        .catch((_error) => {
                            this.toastService.present(SHARE_ERROR_MESSAGE);
                        });
                })
                .catch((_error) => {
                    this.toastService.present(
                        this.getPlatformErrorMessage(
                            SOCIAL_PLATFORMS.FACEBOOK.DISPLAY_NAME
                        )
                    );
                });
        }
    }

    /**
     * Share via twiiter.
     * Check character count whenever this method is getting used
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaTwitter(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        if (this.platformService.isIOS()) {
            this.socialSharing
                .shareViaTwitter(
                    sharingOptions.message,
                    null,
                    sharingOptions.url
                )
                .then(() => {})
                .catch((_error) => {
                    this.toastService.present(SHARE_ERROR_MESSAGE);
                });
        } else {
            this.socialSharing
                .canShareVia(SOCIAL_PLATFORMS.TWITTER.NAME)
                .then(() => {
                    this.socialSharing
                        .shareViaTwitter(
                            sharingOptions.message,
                            null,
                            sharingOptions.url
                        )
                        .then(() => {})
                        .catch((_error) => {
                            this.toastService.present(SHARE_ERROR_MESSAGE);
                        });
                })
                .catch((_error) => {
                    this.toastService.present(
                        this.getPlatformErrorMessage(
                            SOCIAL_PLATFORMS.TWITTER.DISPLAY_NAME
                        )
                    );
                });
        }
    }

    /**
     * Can share file links.
     *
     * @param {ReferralInfo} referralInfo
     * @returns
     * @memberof ReferralService
     */
    shareViaInstagram(referralInfo: ReferralInfo) {
        if (!this.platformService.isCordova()) {
            return;
        }

        const sharingOptions = this.getReferralMessage(referralInfo);

        if (this.platformService.isIOS()) {
            this.socialSharing
                .shareViaInstagram(sharingOptions.message, null)
                .then(() => {})
                .catch((_error) => {
                    this.toastService.present(SHARE_ERROR_MESSAGE);
                });
        } else {
            this.socialSharing
                .canShareVia(SOCIAL_PLATFORMS.INSTAGRAM.NAME)
                .then(() => {
                    this.socialSharing
                        .shareViaInstagram(sharingOptions.message, null)
                        .then(() => {})
                        .catch((_error) => {
                            this.toastService.present(SHARE_ERROR_MESSAGE);
                        });
                })
                .catch(() => {
                    this.toastService.present(
                        this.getPlatformErrorMessage(
                            SOCIAL_PLATFORMS.INSTAGRAM.DISPLAY_NAME
                        )
                    );
                });
        }
    }

    handleBenefitCtaClick(
        referralInfo: ReferralInfo,
        ctaAction: BenefitsBannerCtaAction
    ) {
        const ctaActionType = this.utilService.getNestedValue(
            ctaAction,
            "action"
        );

        if (!ctaActionType) {
            return;
        }

        switch (ctaActionType) {
            case BENEFITS_BANNER_CTA_ACTION_TYPES.ROUTE:
                const route = this.utilService.getNestedValue(
                    ctaAction,
                    "appUrl"
                );

                if (!route) {
                    return;
                }

                this.routerService.goToUrl(route);
                return;

            case BENEFITS_BANNER_CTA_ACTION_TYPES.SHARE:
                this.genericShare(referralInfo);
                return;

            default:
                break;
        }
    }

    handleFaqClick(referralInfo: ReferralInfo) {
        const hasFaq = this.utilService.getNestedValue(referralInfo, "faqs");

        if (!hasFaq) {
            return;
        }

        this.routerService.goToReferralFaqPage();
    }

    /**
     * Filters out contacts based on availabily of a contact in our platform
     * Sorts the results alphabetically
     *
     * @param {Contact[]} phoneContacts
     * @param {string[]} filteredPhoneNumbers
     * @returns {ContactCard[]}
     * @memberof ReferralService
     */
    filterAndSortContacts(
        phoneContacts: Contact[],
        filteredPhoneNumbers: string[]
    ): ContactCard[] {
        const filteredContactList = phoneContacts
            .filter((contact: Contact) => {
                return this.hasRequiredContactFields(
                    filteredPhoneNumbers,
                    contact
                );
            })
            .map((contact: Contact) => {
                return this.getContactCard(contact);
            });

        if (!this.utilService.isLengthyArray(filteredContactList)) {
            return null;
        }

        return filteredContactList.sort(this.utilService.dynamicSort("name"));
    }

    /**
     * Prepare raw contact list to be sent to BE
     * Trims all special chars from contact
     *
     * @param {Contact[]} phoneContacts
     * @returns {string[]}
     * @memberof ReferralService
     */
    getRawPrimaryPhoneNumbers(phoneContacts: Contact[]): string[] {
        if (!this.utilService.isLengthyArray(phoneContacts)) {
            return null;
        }

        const rawPhoneNumbers = phoneContacts
            .filter((contact: Contact) => {
                return this.hasPhoneNumberAndContactName(contact);
            })
            .map((contact: Contact) => {
                return this.utilService.getTrimmedPhoneNumber(
                    contact.phoneNumbers[0].value
                );
            });

        if (!this.utilService.isLengthyArray(rawPhoneNumbers)) {
            return null;
        }

        return rawPhoneNumbers;
    }

    getPhoneNumberWithCountryCode(phoneNumber: string): string {
        let phoneNumberWithCountryCode = "";

        const formattedPhoneNumber = this.utilService.getTrimmedPhoneNumber(
            phoneNumber
        );

        if (
            formattedPhoneNumber.slice(0, 1) === "0" &&
            formattedPhoneNumber.length === 11
        ) {
            phoneNumberWithCountryCode =
                COUNTRY_CODE + formattedPhoneNumber.slice(1);
        }

        if (formattedPhoneNumber.length === 10) {
            phoneNumberWithCountryCode = COUNTRY_CODE + formattedPhoneNumber;
        }

        return phoneNumberWithCountryCode || formattedPhoneNumber;
    }

    private getNumberOfSuccessfulReferrals(referralInfo: ReferralInfo): number {
        return this.utilService.getNestedValue(
            referralInfo,
            "numberOfSuccessfulReferrals",
            0
        );
    }

    private getReferralMessage(referralInfo: ReferralInfo): ReferralMessage {
        return {
            subject: this.utilService.getNestedValue(
                referralInfo,
                "shareBlock.subject",
                null
            ),
            message: this.utilService.getNestedValue(
                referralInfo,
                "shareBlock.message",
                null
            ),
            files: this.utilService.getNestedValue(
                referralInfo,
                "shareBlock.file",
                null
            ),
            url: this.utilService.getNestedValue(
                referralInfo,
                "shareBlock.url",
                null
            ),
        };
    }

    private getPlatformErrorMessage(platform: string) {
        if (!platform) {
            return;
        }

        return platform + PLATFORM_ERROR_MESSAGE;
    }

    private getContactCard(contact: Contact): ContactCard {
        if (this.platformService.isIOS()) {
            return {
                name: (contact.name && contact.name.formatted) || "",
                phone: contact.phoneNumbers[0].value,
                id: contact.id,
            };
        }

        return {
            name: contact.displayName || "",
            phone: contact.phoneNumbers[0].value,
        };
    }

    private hasPhoneNumberAndContactName(contact: Contact): boolean {
        if (!this.utilService.isLengthyArray(contact.phoneNumbers)) {
            return false;
        }

        if (
            (contact.displayName || (contact.name && contact.name.formatted)) &&
            contact.phoneNumbers[0].value
        ) {
            return true;
        }

        return false;
    }

    private hasRequiredContactFields(
        filteredPhoneNumbers: string[],
        contact: Contact
    ): boolean {
        if (!this.utilService.isLengthyArray(contact.phoneNumbers)) {
            return false;
        }

        if (
            contact.phoneNumbers[0].value &&
            filteredPhoneNumbers.indexOf(
                this.utilService.getTrimmedPhoneNumber(
                    contact.phoneNumbers[0].value
                )
            ) > -1 &&
            (contact.displayName || (contact.name && contact.name.formatted))
        ) {
            return true;
        }

        return false;
    }
}
