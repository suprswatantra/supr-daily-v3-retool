import { Injectable } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";

import { filter } from "rxjs/operators";

import {
    CITY_LIST,
    ADDRESS_TYPE,
    EMPTY_ADDRESS_DATA,
    LOCAL_DB_DATA_KEYS,
    ADDRESS_FROM_PARAM,
    INITIAL_ADDRESS_SERVICE_DATA,
} from "@constants";

import { Address, Building, Door, City } from "@models";

import { DbService } from "@services/data/db.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { NudgeService } from "@services/layout/nudge.service";

import { SuprMaps, SuprApi } from "@types";

import { AddressSearchFormattedResult } from "@pages/address/type";
import { SA_OBJECT_CONTEXT } from "@pages/address/constants/analytics.constants";
import {
    MANDATORY_FORM_FIELDS_CHARACTERS_LIMIT,
    APARTMENT_FORM_INPUT,
    FORM_ERRORS,
    INDIVIDUAL_FORM_INPUT,
    SEARCH_INPUT_MAX_LENGTH,
} from "@pages/address/constants";

@Injectable({
    providedIn: "root",
})
export class AddressService {
    private addressData = { ...INITIAL_ADDRESS_SERVICE_DATA };
    private saAddressContext = SA_OBJECT_CONTEXT.CREATE_ADDRESS;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private dbService: DbService,
        private utilService: UtilService,
        private routerService: RouterService,
        private nudgeService: NudgeService
    ) {}

    initialize() {
        this.handleAddressRetroNudge();
    }

    getAddressText(address: Address, excludeCity?: boolean): string {
        let addressText = "";

        if (!this.utilService.isEmpty(address)) {
            if (this.isOutDeliveryAddress(address)) {
                addressText = "ADD AN ADDRESS";
            } else {
                if (
                    address.type === ADDRESS_TYPE.INDIVIDUAL_HOUSE &&
                    address["floorNumber"] &&
                    address["floorNumber"].trim()
                ) {
                    addressText = this.attachFloorNumberToAddressText(
                        addressText,
                        address
                    );
                }

                if (address["doorNumber"]) {
                    addressText = this.attachDoorNumberToAddressText(
                        addressText,
                        address
                    );
                }

                if (address["buildingName"] && address["buildingName"].trim()) {
                    addressText = this.attachBuildingNameToAddressText(
                        addressText,
                        address
                    );
                }

                if (this.checkNull(address.societyName)) {
                    addressText = this.attachSocietyNameToAddressText(
                        addressText,
                        address
                    );
                }

                if (this.checkNull(address.streetAddress)) {
                    addressText = this.attachStreetNameToAddressText(
                        addressText,
                        address
                    );
                }

                if (!excludeCity) {
                    addressText = this.attachCityNameToAddressText(
                        addressText,
                        address
                    );
                }
            }
        }

        return addressText;
    }

    getAddressSummary(address: Address) {
        let addressText = "";
        const _city = CITY_LIST.map((city) => {
            if (
                city.id === this.utilService.getNestedValue(address, "city.id")
            ) {
                return city;
            }
        }).filter((city) => city !== undefined)[0];
        const addressType = this.addressData.formAddressType || address.type;

        if (addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            addressText = this.getAddressTextForIndependent(address, _city);
        } else if (addressType === ADDRESS_TYPE.KNOWN_SOCIETY) {
            addressText = this.getAddressTextForKnowSociety(address, _city);
        } else if (addressType === ADDRESS_TYPE.UNKNOWN_SOCIETY) {
            addressText = this.getAddressTextForUnKnowSociety(address, _city);
        }

        return addressText;
    }

    updateAddressContext() {
        this.saAddressContext = SA_OBJECT_CONTEXT.UPDATE_ADDRESS;
    }

    getAddressContext(): string {
        return this.saAddressContext;
    }

    setTempAddressData(data: any, key?: string) {
        console.log({ data, key });
    }

    getTempAddressData(path?: string): any {
        console.log({ path });
    }

    resetTempAddressData() {}

    setInitialAddressData(address: Address) {
        this.resetAddressData("placeId");
        this.resetAddressData("location");
        this.resetAddressData("searchTerm");
        this.resetAddressData("savedAddress");
        this.resetAddressData("searchResults");
        this.resetAddressData("reverseGeocode");
        this.resetAddressData("formattedAddress");
        this.resetAddressData("societySearchTerm");
        this.setAddressData(address, "savedAddress");
        this.resetAddressData("societySearchResults");
    }

    setInitialFormData(address: Address) {
        const addressType = this.utilService.getNestedValue(address, "type");
        const serviceDataKey = this.getFormDataTypeKey(addressType);

        this.resetAddressData("individualHouseFormData");
        this.resetAddressData("apartmentFormData");

        this.setAddressData(addressType, "formAddressType");
        if (addressType === ADDRESS_TYPE.SKIP) {
            this.setAddressData(ADDRESS_TYPE.KNOWN_SOCIETY, "apartmentType");
        } else if (addressType !== ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            this.setAddressData(addressType, "apartmentType");
        } else {
            this.setAddressData(ADDRESS_TYPE.KNOWN_SOCIETY, "apartmentType");
        }

        this.setAddressData(address, serviceDataKey);
    }

    setAddressData(data: any, key?: string) {
        if (key) {
            this.addressData = { ...this.addressData, [key]: data };
            return;
        }

        this.addressData = data;
    }

    getAddressData(path?: string): any {
        if (path) {
            return this.utilService.getNestedValue(this.addressData, path);
        }

        return this.addressData;
    }

    resetAddressData(key?: string) {
        if (key) {
            this.addressData = {
                ...this.addressData,
                [key]: INITIAL_ADDRESS_SERVICE_DATA[key],
            };
            return;
        }

        this.addressData = { ...INITIAL_ADDRESS_SERVICE_DATA };
    }

    getLocationFromAddress(address: Address): SuprMaps.Location {
        const location = this.utilService.getNestedValue(
            address,
            "location.latLong",
            {}
        );

        return {
            lat: location.latitude,
            lng: location.longitude,
        };
    }

    /* [TODO_RITESH] Add type for argument */

    formatLocationSearchResults(
        results: Array<any>
    ): Array<AddressSearchFormattedResult> {
        if (!results) {
            return [];
        }

        return results.map((result) => ({
            title: this.utilService.getNestedValue(
                result,
                "structured_formatting.main_text",
                ""
            ),
            subtitle: this.utilService.getNestedValue(
                result,
                "structured_formatting.secondary_text",
                ""
            ),
            id: this.utilService.getNestedValue(result, "place_id"),
        }));
    }

    formatSocietySearchResults(
        results: SuprApi.PremiseSearchRes[]
    ): Array<AddressSearchFormattedResult> {
        if (!results) {
            return [];
        }

        return results.map((result: SuprApi.PremiseSearchRes) => ({
            id: result.id,
            title: result.name,
            subtitle: result.streetAddress,
        }));
    }

    formatBuildingsSearchResult(
        buildings: Building[]
    ): Array<AddressSearchFormattedResult> {
        return buildings
            .map((building: Building) => ({
                id: building.id,
                title: building.name,
            }))
            .sort(this.compare);
    }

    formatFlatsSearchResult(flats: Door[]) {
        return flats
            .map((flat: Door) => ({
                id: flat.id,
                title: flat.name,
            }))
            .sort(this.compare);
    }

    getFormattedAddressForMapFooter(
        geocodeResponse: SuprApi.GeoCodeRes
    ): SuprMaps.MapSearchAddressInfo {
        const addressText = this.getFormattedAddressFromGeoCodeRes(
            geocodeResponse
        );

        return {
            addressText,
        };
    }

    getLocationFromGeoCodeRes(
        geocodeResponse: SuprApi.GeoCodeRes
    ): SuprMaps.Location {
        const results = this.selectGeocodeResults(geocodeResponse);

        return this.utilService.getNestedValue(
            results,
            "0.geometry.location",
            null
        );
    }

    getBuildingsFromPremiseDetails(
        premiseDetails: SuprApi.PremiseAddressRes
    ): Building[] {
        return this.utilService.getNestedValue(premiseDetails, "buildings", []);
    }

    getFlatsFromPremiseDetails(
        premiseDetails: SuprApi.PremiseAddressRes,
        towerData: Building
    ): Door[] {
        let selectedTower: Building;
        const towersList = this.getBuildingsFromPremiseDetails(premiseDetails);

        if (towerData.id) {
            selectedTower = towersList.find(
                (tower: Building) => tower.id === towerData.id
            );
        } else {
            selectedTower = towersList.find(
                (tower: Building) => tower.name === towerData.name
            );
        }

        return this.utilService.getNestedValue(selectedTower, "doors", []);
    }

    validateForm(mandatoryFields: {}, formData: {}, addressType: string) {
        if (!formData) {
            return mandatoryFields;
        }

        for (const mandatoryField in mandatoryFields) {
            if (formData[mandatoryField] !== "" && formData[mandatoryField]) {
                this.resetErroForm(addressType, mandatoryField);

                if (
                    !this.isMoreThanMaxCharFormInput(
                        formData[mandatoryField]
                    ) &&
                    MANDATORY_FORM_FIELDS_CHARACTERS_LIMIT[mandatoryField]
                ) {
                    this.setCharErrorOnForm(addressType, mandatoryField);
                } else {
                    delete mandatoryFields[mandatoryField];
                }
            }
        }

        return mandatoryFields;
    }

    getCityInfoFromLocationStatusRes(
        locationStatusRes: SuprApi.LocationStatusRes
    ): City {
        return this.utilService.getNestedValue(locationStatusRes, "data.city");
    }

    getStateInformationFromCity(city: City): string {
        if (!city) {
            return "";
        }

        const selectedCity = CITY_LIST.find(
            (cityData: City) => cityData.id === city.id
        );
        return selectedCity.state;
    }

    getAddressRequestData(address: Address) {
        const type = address.type;

        if (type === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            return this.getIndividualHouseReqData(address);
        } else if (type === ADDRESS_TYPE.KNOWN_SOCIETY) {
            return this.getKnownSocietyReqData(address);
        } else if (type === ADDRESS_TYPE.UNKNOWN_SOCIETY) {
            return this.getUnknownSocietyReqData(address);
        }
    }

    getModifiedAddress(buildings?: Building[]): Address {
        const _addressType = this.getAddressData("formAddressType");
        const formData = this.getFormData(_addressType);
        const tempAddress: Address = { ...EMPTY_ADDRESS_DATA };

        const formattedAddress = this.getAddressData("formattedAddress");

        tempAddress.type = _addressType;

        tempAddress.id = this.getAddressData("savedAddress").id;

        tempAddress.doorNumber = formData.doorNumber;
        tempAddress.doorId = formData.doorId;

        tempAddress.buildingName = formData.buildingName;
        tempAddress.buildingId = formData.buildingId;

        tempAddress.floorNumber = formData.floorNumber;

        tempAddress.societyId = formData.premiseId;
        tempAddress.societyName = formData.societyName;

        tempAddress.instructions = formData.instructions;

        // if (_addressType === ADDRESS_TYPE.KNOWN_SOCIETY) {
        //     tempAddress.streetAddress = formattedAddress.addressText;
        // } else {
        //     tempAddress.streetAddress = formData.streetAddress;
        // }

        tempAddress.streetAddress = formData.streetAddress;

        tempAddress.city = this.utilService.getNestedValue(
            formattedAddress,
            "city"
        );

        tempAddress.location = {
            latLong: {
                latitude: this.getAddressData("location").lat,
                longitude: this.getAddressData("location").lng,
            },
            reverseGeocode: this.getAddressData("reverseGeocode"),
        };

        this.appendTowerAndFlatInfo(tempAddress, buildings);

        return tempAddress;
    }

    getFormDataTypeKey(addressType: string): string {
        if (addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE) {
            return "individualHouseFormData";
        }

        return "apartmentFormData";
    }

    setRedirectFlag(from: string) {
        this.dbService.setLocalData(
            LOCAL_DB_DATA_KEYS.ADDRESS_REDIRECTION,
            from
        );
    }

    redirect() {
        const from = this.getRedirectFlag();

        switch (from) {
            case ADDRESS_FROM_PARAM.CART:
                return this.routerService.goToCartPage({ replaceUrl: true });
            case ADDRESS_FROM_PARAM.NUDGE:
            case ADDRESS_FROM_PARAM.CITY_SELECTION:
                return this.routerService.goToHomePage({ replaceUrl: true });
            default:
                this.routerService.goToProfilePage({ replaceUrl: true });
        }
    }

    getRedirectFlag(): string {
        return this.dbService.getLocalData(
            LOCAL_DB_DATA_KEYS.ADDRESS_REDIRECTION
        );
    }

    private isMoreThanMaxCharFormInput(input: string) {
        return input.trim().length <= SEARCH_INPUT_MAX_LENGTH;
    }

    private setCharErrorOnForm(addressType: string, key) {
        if (
            addressType === ADDRESS_TYPE.KNOWN_SOCIETY ||
            addressType === ADDRESS_TYPE.UNKNOWN_SOCIETY
        ) {
            APARTMENT_FORM_INPUT[key].error =
                FORM_ERRORS.FLOOR_FLAT_TOWER_20_CHAR;
        } else {
            INDIVIDUAL_FORM_INPUT[key].error =
                FORM_ERRORS.FLOOR_FLAT_TOWER_20_CHAR;
        }
    }

    private resetErroForm(addressType: string, key) {
        if (
            addressType === ADDRESS_TYPE.KNOWN_SOCIETY ||
            addressType === ADDRESS_TYPE.UNKNOWN_SOCIETY
        ) {
            APARTMENT_FORM_INPUT[key].error =
                FORM_ERRORS[addressType][key].error;
        } else {
            INDIVIDUAL_FORM_INPUT[key].error =
                FORM_ERRORS[addressType][key].error;
        }
    }

    private getFormattedAddressFromGeoCodeRes(
        geocodeResponse: SuprApi.GeoCodeRes
    ): string {
        const results = this.selectGeocodeResults(geocodeResponse);

        return this.utilService.getNestedValue(
            results,
            "0.formatted_address",
            ""
        );
    }

    private selectGeocodeResults(
        geocodeResponse: SuprApi.GeoCodeRes
    ): SuprMaps.GeoCodeResult[] {
        return this.utilService.getNestedValue(
            geocodeResponse,
            "data.results",
            []
        );
    }

    private isOutDeliveryAddress(address: Address): boolean {
        if (this.utilService.isEmpty(address)) {
            return false;
        }

        const { type } = address;
        if (!type) {
            return true;
        }

        return false;
    }

    private checkNull(data: any): string {
        if (data) {
            return data;
        }

        return "";
    }

    private attachFloorNumberToAddressText(
        addressText: string,
        address: Address
    ) {
        addressText += this.checkNull(address.floorNumber);
        return addressText;
    }

    private attachDoorNumberToAddressText(
        addressText: string,
        address: Address
    ) {
        if (addressText) {
            addressText += " - " + address.doorNumber;
        } else {
            addressText += address.doorNumber;
        }
        return addressText;
    }

    private attachBuildingNameToAddressText(
        addressText: string,
        address: Address
    ) {
        if (addressText) {
            addressText += " - " + address.buildingName;
        } else {
            addressText += address.buildingName;
        }
        return addressText;
    }

    private attachSocietyNameToAddressText(
        addressText: string,
        address: Address
    ) {
        if (addressText) {
            addressText += " - " + address.societyName;
        } else {
            addressText += address.societyName;
        }
        return addressText;
    }

    private attachStreetNameToAddressText(
        addressText: string,
        address: Address
    ) {
        if (addressText) {
            addressText += " - " + address.streetAddress;
        } else {
            addressText += address.streetAddress;
        }
        return addressText;
    }

    private attachCityNameToAddressText(addressText: string, address: Address) {
        if (addressText) {
            addressText += ", " + address.city.name;
        } else {
            addressText += address.city.name;
        }
        return addressText;
    }

    private getAddressTextForIndependent(address: Address, city: City) {
        let addressText = "";

        if (!this.utilService.isEmpty(address)) {
            if (address["floorNumber"] && address["floorNumber"].trim()) {
                addressText += "Floor: " + address["floorNumber"];
            }

            if (address["doorNumber"] && address["doorNumber"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["doorNumber"];
                } else {
                    addressText += address["doorNumber"];
                }
            }

            if (this.checkNull(address.streetAddress)) {
                if (addressText) {
                    addressText += "," + " " + address["streetAddress"];
                } else {
                    addressText += address["streetAddress"];
                }
            }

            addressText += ", " + address.city.name;

            if (!this.utilService.isEmpty(city)) {
                addressText += ", " + city["state"];
            }
        }

        return addressText;
    }

    private getAddressTextForKnowSociety(address: Address, city: City) {
        let addressText = "";

        if (!this.utilService.isEmpty(address)) {
            if (address["doorNumber"] && address["doorNumber"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["doorNumber"];
                } else {
                    addressText += address["doorNumber"];
                }
            }

            if (address["buildingName"] && address["buildingName"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["buildingName"];
                } else {
                    addressText += address["buildingName"];
                }
            }

            if (address["societyName"] && address["societyName"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["societyName"];
                } else {
                    addressText += address["societyName"];
                }
            }

            if (this.checkNull(address.streetAddress)) {
                if (addressText) {
                    addressText += "," + " " + address["streetAddress"];
                } else {
                    addressText += address["streetAddress"];
                }
            }

            addressText += ", " + address.city.name;

            if (!this.utilService.isEmpty(city)) {
                addressText += ", " + city["state"];
            }
        }

        return addressText;
    }

    private getAddressTextForUnKnowSociety(address: Address, city: City) {
        let addressText = "";

        if (!this.utilService.isEmpty(address)) {
            if (address["doorNumber"] && address["doorNumber"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["doorNumber"];
                } else {
                    addressText += address["doorNumber"];
                }
            }

            if (address["buildingName"] && address["buildingName"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["buildingName"];
                } else {
                    addressText += address["buildingName"];
                }
            }

            if (address["societyName"] && address["societyName"].trim()) {
                if (addressText) {
                    addressText += "," + " " + address["societyName"];
                } else {
                    addressText += address["societyName"];
                }
            }

            if (this.checkNull(address.streetAddress)) {
                if (addressText) {
                    addressText += "," + " " + address["streetAddress"];
                } else {
                    addressText += address["streetAddress"];
                }
            }

            addressText += ", " + address.city.name;

            if (!this.utilService.isEmpty(city)) {
                addressText += ", " + city["state"];
            }
        }

        return addressText;
    }

    private getIndividualHouseReqData(address: Address) {
        return {
            type: address.type,
            floorNumber: address.floorNumber,
            doorNumber: address.doorNumber,
            streetAddress: address.streetAddress,
            cityId: address.city.id ? address.city.id : 0,
            location: {
                latLong: {
                    latitude: address.location.latLong.latitude,
                    longitude: address.location.latLong.longitude,
                },
                reverseGeocode: address.location.reverseGeocode,
            },
            instructions: address.instructions,
        };
    }

    private getKnownSocietyReqData(address: Address) {
        return {
            type: address.type,
            societyId: address.societyId,
            buildingId: address.buildingId,
            doorId: address.doorId,
        };
    }

    private getUnknownSocietyReqData(address: Address) {
        return {
            type: "unknownSociety",
            societyName: address.societyName,
            buildingName: address.buildingName,
            doorNumber: address.doorNumber,
            streetAddress: address.streetAddress,
            cityId: this.utilService.getNestedValue(address, "city.id"),
            location: {
                latLong: {
                    latitude: this.utilService.getNestedValue(
                        address,
                        "location.latLong.latitude"
                    ),
                    longitude: this.utilService.getNestedValue(
                        address,
                        "location.latLong.longitude"
                    ),
                },
                reverseGeocode: this.utilService.getNestedValue(
                    address,
                    "location.reverseGeocode"
                ),
            },
            instructions: address.instructions,
        };
    }

    private getFormData(addressType: string) {
        const serviceDataKey =
            addressType === ADDRESS_TYPE.INDIVIDUAL_HOUSE
                ? "individualHouseFormData"
                : "apartmentFormData";

        return this.getAddressData(serviceDataKey);
    }

    private appendTowerAndFlatInfo(
        address: Address,
        buildings?: Building[]
    ): Address {
        if (
            !buildings ||
            address.type !== ADDRESS_TYPE.KNOWN_SOCIETY ||
            (address.buildingId && address.doorId)
        ) {
            return address;
        }

        const buildingData = buildings.find(
            (building: Building) => building.name === address.buildingName
        );

        address.buildingId = this.utilService.getNestedValue(
            buildingData,
            "id"
        );

        const doors = this.utilService.getNestedValue(
            buildingData,
            "doors",
            []
        );
        const doorData = doors.find(
            (door: Door) => door.name === address.doorNumber
        );

        address.doorId = this.utilService.getNestedValue(doorData, "id");

        return address;
    }

    private compare(a, b) {
        if (isNaN(a.title) || isNaN(b.title)) {
            return a.title.toUpperCase() > b.title.toUpperCase() ? 1 : -1;
        } else {
            return parseInt(a.title) > parseInt(b.title) ? 1 : -1;
        }
    }

    private handleAddressRetroNudge() {
        this.router.events
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    // Traverse the active route tree
                    let snapshot = this.activatedRoute.snapshot;
                    let activated = this.activatedRoute.firstChild;

                    while (activated != null) {
                        snapshot = activated.snapshot;
                        activated = activated.firstChild;
                    }

                    // Get the screen name
                    const screenName = this.utilService.getNestedValue(
                        snapshot,
                        "data.screenName"
                    );

                    // Send nudge
                    this.nudgeService.sendAddressRetroNudge(screenName);
                }
            });
    }
}
