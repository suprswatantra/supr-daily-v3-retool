import { Injectable } from "@angular/core";

import { Sku, CartItem } from "@shared/models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";

import { SkuDict } from "@types";

export const SUPR_PASS_TEXT = "Supr Access";

@Injectable({
    providedIn: "root",
})
export class SkuService {
    constructor(
        private utilsService: UtilService,
        private dateService: DateService
    ) {}

    filterOOSSkuFromList(skuList: number[], skuDict: SkuDict): number[] {
        let filteredList = skuList;

        if (
            this.utilsService.isLengthyArray(skuList) &&
            !this.utilsService.isEmpty(skuDict)
        ) {
            filteredList = skuList.filter(
                (skuId) =>
                    skuDict[skuId] &&
                    (!skuDict[skuId].out_of_stock ||
                        (skuDict[skuId].out_of_stock &&
                            skuDict[skuId].next_available_date))
            );
        }

        return filteredList;
    }

    isSuprPassSku(sku: Sku): boolean {
        const isVirtualSku = this.utilsService.getNestedValue(
            sku,
            "is_virtual_sku",
            false
        );
        const virtualSkuType = this.utilsService.getNestedValue(
            sku,
            "virtual_sku_info.type"
        );

        if (isVirtualSku && virtualSkuType === SUPR_PASS_TEXT) {
            return true;
        }

        return false;
    }

    convertSkuListToDict(skuList: Sku[]): SkuDict {
        return skuList.reduce((dict, sku) => {
            dict[sku.id] = { ...sku };
            return dict;
        }, {});
    }

    filterCartItemsFromPastOrderList(
        pastOrderSkuList: number[],
        cartItems: CartItem[]
    ): number[] {
        return pastOrderSkuList.filter(
            (skuId) => !cartItems.find((cartItem) => skuId === cartItem.sku_id)
        );
    }

    getOOSDates(sku: Sku) {
        if (sku && sku.out_of_stock && sku.next_available_date) {
            const today = this.dateService.getTodayDate();
            const availableDate = this.dateService.dateFromText(
                sku.next_available_date
            );
            const disabledDates = this.dateService.getDatesBetweenRange(
                today,
                availableDate
            );
            const oosDates = disabledDates.map((date) =>
                this.dateService.textFromDate(date)
            );
            /* remove last date as sku is available from then.
             getDatesBetweenRange returned array includes both start and end date */
            oosDates.pop();
            return oosDates;
        }
        return null;
    }

    // Given method assumes the delieryDate provide is not before current date
    isSkuAvailableOnGivenDate(sku: Sku, deliveryDate: string) {
        if (sku && deliveryDate) {
            const { out_of_stock, next_available_date } = sku;

            /* if not out of stock then sku is always available */
            if (!out_of_stock) {
                return true;
            }

            /* if out of stock and no availability info is there then not available */
            if (out_of_stock && !next_available_date) {
                return false;
            }

            /* if out of stock but next availability is known
            check if the asked date is on or after that */
            if (out_of_stock && next_available_date) {
                /*if next availability date is tomorrow and cut off time has paases, return false */
                if (
                    this.dateService.textFromDate(
                        this.dateService.getTomorrowDate()
                    ) === deliveryDate &&
                    this.dateService.hasTodaysCutOffTimePassed()
                ) {
                    return false;
                }
                const daysBetween = this.dateService.daysBetweenTwoDates(
                    this.dateService.dateFromText(sku.next_available_date),
                    this.dateService.dateFromText(deliveryDate)
                );
                return daysBetween >= 0;
            }
        } else {
            return false;
        }
    }

    // Filter PastOrder Sku's from Sku's List
    filterPastOrderskuFromList(skuList: number[], skuDict: SkuDict): number[] {
        let filteredList = skuList;
        if (
            this.utilsService.isLengthyArray(skuList) &&
            !this.utilsService.isEmpty(skuDict)
        ) {
            filteredList = skuList.filter(
                (skuId) => skuDict[skuId] && skuDict[skuId].active
            );
        }

        return filteredList;
    }
}
