import { Injectable } from "@angular/core";

import { CITY_LIST } from "@constants";

import { City } from "@models";

@Injectable({
    providedIn: "root",
})
export class UserService {
    private isAnonymousUser = true;
    private anonymousUserCityInfo: City;

    setAnonymousUserCityInfo(id: number) {
        const city = CITY_LIST.find((item) => item.id === id);
        this.anonymousUserCityInfo = city;
    }

    getAnonymousUserCityInfo(): City {
        return this.anonymousUserCityInfo;
    }

    setIsAnonymousUser(isAnonymous: boolean) {
        this.isAnonymousUser = isAnonymous;
    }

    getIsAnonymousUser() {
        return this.isAnonymousUser;
    }
}
