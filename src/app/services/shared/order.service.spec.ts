import { TestBed } from "@angular/core/testing";

import {
    BaseUnitTestProvidersIonic,
    BaseUnitTestImportsWithStoreIonic,
} from "./../../../unitTest.conf";

import { Sku, Order } from "@shared/models";

import { OrderService } from "./order.service";
import { SettingsService } from "./settings.service";
import { DbService } from "@services/data/db.service";
import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { ErrorService } from "@services/integration/error.service";
import { QuantityService } from "@services/util/quantity.service";

describe("OrderService", () => {
    let dbService: DbService;
    let utilService: UtilService;
    let dateService: DateService;
    let orderService: OrderService;
    let errorService: ErrorService;
    let quantityService: QuantityService;
    let settingsService: SettingsService;

    const sku = {
        item: {
            unit: {
                name: "pouch",
            },
            unit_quantity: 1,
        },
    } as Sku;
    const fulfilledOrderA = {
        delivered_quantity: 1,
        scheduled_quantity: 1,
    } as Order;
    const fulfilledOrderB = {
        delivered_quantity: 2,
        scheduled_quantity: 1,
    } as Order;
    const unfulfilledOrder = {
        delivered_quantity: 0,
        scheduled_quantity: 1,
    } as Order;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                DbService,
                DateService,
                UtilService,
                ErrorService,
                QuantityService,
                SettingsService,
                ...BaseUnitTestProvidersIonic,
            ],
        });

        dbService = TestBed.get(DbService);
        dateService = TestBed.get(DateService);
        utilService = TestBed.get(UtilService);
        errorService = TestBed.get(ErrorService);
        quantityService = TestBed.get(QuantityService);
        settingsService = TestBed.get(SettingsService);

        orderService = new OrderService(
            dbService,
            dateService,
            utilService,
            errorService,
            quantityService,
            settingsService
        );
    });

    it("#should create order service instance", () => {
        expect(orderService).toBeTruthy();
    });

    it("#should return empty string when sku is undefined or null or is an empty object", () => {
        /* sku is undefined */
        expect(
            orderService.getOrderHistoryTileScheduledQtyText(
                undefined,
                {} as Order
            )
        ).toEqual("");

        /* sku is null */
        expect(
            orderService.getOrderHistoryTileScheduledQtyText(null, {} as Order)
        ).toEqual("");

        /* sku is an empty object */
        expect(
            orderService.getOrderHistoryTileScheduledQtyText(
                {} as Sku,
                {} as Order
            )
        ).toEqual("");
    });

    it("#should return empty string when scheduled quantity is less than or equal to delivered quantity", () => {
        /* scheduled quantity is equal to delivered quantity */
        expect(
            orderService.getOrderHistoryTileScheduledQtyText(
                {} as Sku,
                fulfilledOrderA
            )
        ).toEqual("");

        /* scheduled quantity is less than delivered quantity */
        expect(
            orderService.getOrderHistoryTileScheduledQtyText(
                {} as Sku,
                fulfilledOrderB
            )
        ).toEqual("");
    });

    it("#should return a non-empty string when scheduled quantity is greater than delivered quantity", () => {
        const deliveredQtyText = quantityService.toText(sku, 1);

        expect(
            orderService.getOrderHistoryTileScheduledQtyText(
                sku,
                unfulfilledOrder
            )
        ).toEqual(deliveredQtyText);
    });
});
