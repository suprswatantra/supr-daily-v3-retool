import { Injectable } from "@angular/core";

import {
    DAY_NAMES_FULL,
    NEW_ORDER_STATUS_TYPES,
    ORDER_STATUS_TYPES,
    PAGE_ROUTES,
} from "@constants";

import {
    Vacation,
    Schedule,
    ScheduleItem,
    OrderHistory,
    DeliveryTimelines,
    OrderDeliverySlotMap,
    ScheduleDeliverySlotMap,
    ScheduleDeliverySlotItem,
    DeliveryTimelineSlot,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import { SCHEDULE_DAYS } from "@pages/schedule/constants/schedule.constants";

@Injectable({
    providedIn: "root",
})
export class ScheduleService {
    private thankYouFlag = false;
    private customScheduleStartDate: string;
    private customScheduleScrollDate: string;
    showOrderFeedback = false;

    constructor(
        private dateService: DateService,
        private utilService: UtilService,
        private settingsService: SettingsService
    ) {}

    isDateInVacation(vacation: Vacation, dateToCheck: string): boolean {
        const daysBetweenStart = this.dateService.daysBetweenTwoDates(
            new Date(vacation.start_date),
            new Date(dateToCheck)
        );

        const daysBetweenEnd = this.dateService.daysBetweenTwoDates(
            new Date(vacation.end_date),
            new Date(dateToCheck)
        );

        return daysBetweenStart >= 0 && daysBetweenEnd <= 0;
    }

    isThankYouPageUrl(url: string): boolean {
        const isUrlMatch = url && url.indexOf(PAGE_ROUTES.THANKYOU.PATH) !== -1;
        return isUrlMatch;
    }

    getScheduleDay(date: string): string {
        const scheduleDate = this.dateService.dateFromText(date);
        const daysBetween = this.dateService.daysFromToday(scheduleDate);

        switch (daysBetween) {
            case 0:
                return SCHEDULE_DAYS.TODAY;
            case 1:
                return SCHEDULE_DAYS.TOMORROW;
            default:
                return DAY_NAMES_FULL[this.getDayIndex(scheduleDate)];
        }
    }

    getPreviousScheduleDay(date: string): string {
        const previousDate = this.dateService.getPreviousDateFromString(date);
        const previousDay = this.dateService.textFromDate(previousDate);

        return this.getScheduleDay(previousDay);
    }

    getThankYouFlag(): boolean {
        return this.thankYouFlag;
    }

    setThankYouFlag(flag: boolean) {
        this.thankYouFlag = flag;
    }

    getCustomScheduleScrollDate(): string {
        return this.customScheduleScrollDate;
    }

    setCustomScheduleScrollDate(date: string) {
        this.customScheduleScrollDate = date;
    }

    getCustomScheduleStartDate(): string {
        return this.customScheduleStartDate;
    }

    setCustomScheduleStartDate(date: string) {
        this.customScheduleStartDate = date;
    }

    setOrderFeedback(value: boolean = false) {
        this.showOrderFeedback = value;
    }

    getOrderFeedback() {
        return this.showOrderFeedback;
    }

    hasScheduledDeliveries(
        schedule: Schedule,
        includeEmpty: boolean = false
    ): boolean {
        if (!schedule) {
            return false;
        }

        const addons = (schedule && schedule.addons) || [],
            subscriptions = (schedule && schedule.subscriptions) || [];

        let hasSubscriptions: boolean;

        if (includeEmpty) {
            hasSubscriptions = subscriptions.length > 0;
        } else {
            hasSubscriptions = !!subscriptions.find(
                (subscription: ScheduleItem) => subscription.quantity > 0
            );
        }

        return !!addons.length || hasSubscriptions;
    }

    /* Converts schedule to an order history format; used post cut off time
    when order for tomorrow is still being processed */
    getOrderDataFromSchedule(scheduleData: Schedule): OrderHistory {
        if (!scheduleData) {
            return;
        }

        let addOnOrders = [],
            subscriptionOrders = [];

        if (scheduleData.addons) {
            addOnOrders = scheduleData.addons.map((addOn: ScheduleItem) => ({
                id: null,
                type: "addon",
                statusContext: {},
                addon_id: addOn.id,
                sku_id: addOn.sku_id,
                delivered_quantity: addOn.quantity,
                delivery_type: addOn.delivery_type,
                status: ORDER_STATUS_TYPES.SCHEDULED,
                statusNew: NEW_ORDER_STATUS_TYPES.ORDER_SCHEDULED,
            }));
        }

        if (scheduleData.subscriptions) {
            subscriptionOrders = scheduleData.subscriptions.map(
                (subscription: ScheduleItem) => ({
                    id: null,
                    statusContext: {},
                    type: "subscription",
                    addon_id: subscription.id,
                    sku_id: subscription.sku_id,
                    status: ORDER_STATUS_TYPES.SCHEDULED,
                    delivered_quantity: subscription.quantity,
                    delivery_type: subscription.delivery_type,
                    statusNew: NEW_ORDER_STATUS_TYPES.ORDER_SCHEDULED,
                })
            );
        }

        return {
            date: scheduleData.date,
            orders: [...addOnOrders, ...subscriptionOrders],
            delivery_timeline: scheduleData.delivery_timeline,
            delivery_summary: {
                message: scheduleData.message,
                slot_info: scheduleData.slot_info,
                status: NEW_ORDER_STATUS_TYPES.ORDER_SCHEDULED,
            },
        };
    }

    /* Converts schedule delivery slot map to an order history slot map; used post
    cut off time when order for tomorrow is still being processed */
    getOrderDataFromScheduleDelivery(
        scheduleDelivery: ScheduleDeliverySlotMap
    ): OrderDeliverySlotMap {
        if (!scheduleDelivery) {
            return;
        }

        const {
            date,
            message,
            slot_info,
            delivery_timeline,
            ...deliveryMap
        } = scheduleDelivery;
        const scheduleDeliveryMap = deliveryMap as {
            [type: string]: ScheduleDeliverySlotItem;
        };
        let orderDeliveryItem: any = {};

        for (const deliveryType in scheduleDeliveryMap) {
            if (scheduleDeliveryMap[deliveryType]) {
                const schedule: Schedule = {
                    date,
                    message,
                    slot_info,
                    delivery_timeline,
                    addons: scheduleDeliveryMap[deliveryType].addons,
                    subscriptions:
                        scheduleDeliveryMap[deliveryType].subscriptions,
                };

                const orderHistory = this.getOrderDataFromSchedule(schedule);
                const orders = [
                    ...(orderDeliveryItem[deliveryType] || []),
                    ...orderHistory.orders,
                ];
                const slotInfo = this.utilService.getNestedValue(
                    orderHistory,
                    "delivery_summary.slot_info"
                );

                orderDeliveryItem = {
                    ...orderDeliveryItem,
                    slot_info: slotInfo,
                    [deliveryType]: orders,
                    delivery_summary: orderHistory.delivery_summary,
                };
            }
        }

        return {
            date,
            ...orderDeliveryItem,
        };
    }

    getSlotTimelineData(
        deliveryTimelines: DeliveryTimelines
    ): DeliveryTimelines {
        let slotTimeLines: DeliveryTimelines;
        const deliverySlots = this.settingsService.getSettingsValue(
            "deliverySlotTypes"
        );

        const timelineSlotMap = this.settingsService.getSettingsValue(
            "timelineSlotMap"
        );

        if (!deliveryTimelines || !deliverySlots || !timelineSlotMap) {
            return;
        }

        // tslint:disable-next-line: forin
        for (const slotIndex in deliverySlots) {
            const slot = deliverySlots[slotIndex];
            const timelineSlot = timelineSlotMap[slot];
            const timelineDetails =
                timelineSlot && deliveryTimelines[timelineSlot];

            if (this.isValidTimelineData(timelineDetails)) {
                slotTimeLines = slotTimeLines || {};
                slotTimeLines[slot] = timelineDetails;
            }
        }

        return slotTimeLines;
    }

    /**
     * Handle scroll to certain date on calendar view
     *
     * @param {string} date
     * @param {number} additionalOffset
     * @returns {void}
     * @memberof ScheduleService
     */
    scrollScheduleIntoView(date: string, additionalOffset: number = 0): void {
        const scheduleItem = document.getElementById(date);

        if (scheduleItem) {
            let headerHeight = 0;
            const headerHeightProperty = getComputedStyle(
                document.documentElement
            ).getPropertyValue("--supr-page-header-height");

            if (headerHeightProperty) {
                headerHeight = Number(headerHeightProperty.split("px")[0]);
            }

            const offset =
                scheduleItem.offsetTop -
                Number(headerHeight) -
                additionalOffset;

            this.scrollScheduleWrapper(offset);
        }
    }

    /**
     * Handle scroll - utility method
     *
     * @param {number} offset
     * @returns {void}
     * @memberof ScheduleService
     */
    scrollScheduleWrapper(offset: number): void {
        const wrapper = document.getElementById("scheduleContentWrapper");
        if (wrapper) {
            wrapper.scrollTop = offset;
        }
    }

    private getDayIndex(date: Date): number {
        const day = date.getDay();
        return (day + 6) % 7;
    }

    private isValidTimelineData(
        timelineDetails: DeliveryTimelineSlot
    ): boolean {
        return (
            !this.utilService.isEmpty(timelineDetails) &&
            timelineDetails.global_status &&
            this.utilService.isLengthyArray(timelineDetails.timeline)
        );
    }
}
