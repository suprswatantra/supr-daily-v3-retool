import { Injectable } from "@angular/core";

import {
    EVENT_TYPES,
    SKU_ITEM_CONFIG,
    CART_ITEM_TYPES,
    ANALYTICS_OBJECT_NAMES,
    ALTERNATES_WIDGET_BASE_CONFIG,
} from "@constants";

import { Sku, CartItem, V3Layout, OutOfStockPreferredFlowType } from "@models";

import { CalendarService } from "@services/date/calendar.service";
import { SegmentService } from "@services/integration/segment.service";
import { RudderStackService } from "@services/integration/rudder-stack.service";

import { Segment } from "@types";

import {
    SA_OBJECT_NAMES,
    ANALYTICS_TEXTS,
    SA_CONTEXT_VALUES,
} from "@pages/oos-alternates/constants/analytics.constants";
import { DateService } from "@services/date/date.service";

@Injectable({
    providedIn: "root",
})
export class OOSInfoService {
    saContextList: Segment.ContextListItem[] = [];

    constructor(
        private segment: SegmentService,
        private dateService: DateService,
        private calendarService: CalendarService,
        private rudderStackService: RudderStackService
    ) {}

    trackAnalytics(
        sku: Sku,
        cartItem: CartItem,
        notifiedState = 0,
        eventType = "click"
    ) {
        if (sku) {
            this.setAnalyticsData(sku, cartItem, notifiedState);
            this.trackEvent(sku.id, eventType);
        }
    }

    getWidgetDataFromAlternatives(
        skuIds: number[],
        parentSkuId?: number
    ): V3Layout {
        if (!skuIds || !skuIds.length) {
            return;
        }

        let titleText = ALTERNATES_WIDGET_BASE_CONFIG.data.title.text;
        const nextAvailableDate = this.calendarService.getNextAvailableDate();
        const isTomorrowAvailable = this.calendarService.isTomorrowAvailable(
            null,
            nextAvailableDate
        );

        if (!isTomorrowAvailable) {
            titleText = titleText.replace(
                "tomorrow",
                this.dateService.getDeliveryDateText(nextAvailableDate)
            );
        }

        return {
            ...ALTERNATES_WIDGET_BASE_CONFIG,
            data: {
                ...ALTERNATES_WIDGET_BASE_CONFIG.data,
                title: {
                    ...ALTERNATES_WIDGET_BASE_CONFIG.data.title,
                    text: titleText,
                },
                items: skuIds.map((skuId: number) => ({
                    ...SKU_ITEM_CONFIG,
                    data: {
                        ...SKU_ITEM_CONFIG.data,
                        entityId: skuId,
                        parent_sku_id: parentSkuId,
                        analytics: {
                            saImpression: {
                                context:
                                    ANALYTICS_OBJECT_NAMES.IMPRESSION
                                        .OUT_OF_STOCK_SKU,
                                contextList: this.saContextList,
                                objectValue: `${skuId}`,
                                objectName:
                                    ANALYTICS_OBJECT_NAMES.IMPRESSION
                                        .PRODUCT_TILE,
                            },
                            saClick: {
                                context:
                                    ANALYTICS_OBJECT_NAMES.IMPRESSION
                                        .OUT_OF_STOCK_SKU,
                                contextList: this.saContextList,
                                objectValue: `${skuId}`,
                                objectName:
                                    ANALYTICS_OBJECT_NAMES.CLICK.ADD_PRODUCT,
                            },
                        },
                    },
                })),
            },
        };
    }

    private setAnalyticsData(sku: Sku, cartItem: CartItem, notifiedState = 0) {
        if (sku) {
            this.saContextList = this.getOOSAnalytics(
                sku,
                cartItem,
                notifiedState
            );
        }
    }

    private getOOSAnalytics(
        sku: Sku,
        cartItem: CartItem,
        notifiedState: number
    ): Segment.ContextListItem[] {
        const contextList: Segment.ContextListItem[] = [];

        if (sku && cartItem) {
            contextList.push(
                {
                    name: ANALYTICS_TEXTS.UNIT_MRP,
                    value: sku.unit_mrp,
                },
                {
                    name: ANALYTICS_TEXTS.UNIT_PRICE,
                    value: sku.unit_price,
                },
                {
                    name: ANALYTICS_TEXTS.TYPE,
                    value: cartItem.type,
                }
            );

            if (cartItem.type === CART_ITEM_TYPES.ADDON) {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.SCHEDULE_DATE,
                        value: cartItem.delivery_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QUANTITY,
                        value: cartItem.quantity,
                    }
                );
            } else {
                contextList.push(
                    {
                        name: ANALYTICS_TEXTS.START_DATE,
                        /* For subscription flow, we won't be having the delivery data mostly.
                        Hence use the default date provided by cart service */
                        value: cartItem.start_date || cartItem.delivery_date,
                    },
                    {
                        name: ANALYTICS_TEXTS.QTY_PER_DELIVERY,
                        value: cartItem.quantity,
                    }
                );
            }

            contextList.push(
                {
                    name: ANALYTICS_TEXTS.RECHARGE_QTY,
                    value: cartItem.recharge_quantity || 0,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_SKU_ID,
                    value: sku.id,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_PARENT_ID,
                    value: `${sku.id}_${Date.now()}`,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_PREFERRED_FLOW,
                    value: this.getPreferredFlow(sku, cartItem),
                },
                {
                    name: ANALYTICS_TEXTS.OOS_AVAILABLE_DATE,
                    value: sku.next_available_date,
                },
                {
                    name: ANALYTICS_TEXTS.OOS_NOTIFIED,
                    value: notifiedState,
                },
                {
                    name: ANALYTICS_TEXTS.NOTIFY_AVAILABLE,
                    value: sku.notify_enabled ? "1" : "0",
                }
            );
        }

        return contextList;
    }

    private getPreferredFlow(
        sku: Sku,
        cartItem: CartItem
    ): OutOfStockPreferredFlowType | "" {
        if (sku && sku.out_of_stock_preferred_flow && cartItem) {
            return cartItem.type === CART_ITEM_TYPES.ADDON
                ? sku.out_of_stock_preferred_flow.addon
                : sku.out_of_stock_preferred_flow.subscription;
        }
        return "";
    }

    private getContextListItems() {
        if (!this.saContextList || !this.saContextList.length) {
            return [];
        }

        const obj = {};
        this.saContextList.forEach((item, index) => {
            obj[`context_${index + 1}_name`] = item.name;
            obj[`context_${index + 1}_value`] = item.value;
        });

        return obj;
    }

    private trackEvent(skuId: number, eventType: string) {
        const eventBody = {
            objectName: SA_OBJECT_NAMES.IMPRESSION.OUT_OF_STOCK_SKU,
            objectValue: String(skuId),
            context: SA_CONTEXT_VALUES.PARENT_PRODUCT,
            ...this.getContextListItems(),
        };

        if (eventType === EVENT_TYPES.CLICK_EVENT) {
            this.segment.trackClick(eventBody);
            this.rudderStackService.trackClick(eventBody);
        } else if (eventType === "impression") {
            this.segment.trackImpression(eventBody);
            this.rudderStackService.trackImpression(eventBody);
        }
    }
}
