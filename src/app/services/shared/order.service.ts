import { Injectable } from "@angular/core";

import {
    SETTINGS,
    LOCAL_DB_DATA_KEYS,
    NEW_ORDER_STATUS_TYPES,
} from "@constants";

import { Order, CartItem, CartItemDeliveryType, Sku } from "@models";

import { DbService } from "@services/data/db.service";
import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { QuantityService } from "@services/util/quantity.service";
import { ErrorService } from "@services/integration/error.service";
import { SettingsService } from "@services/shared/settings.service";

import {
    OrderSuccessADModalConfig,
    OrderSuccessWhatsappConfig,
    OrderRefundModalAdjustmentInfo,
    OrderSuccessWhatsappImpressionData,
} from "@types";

@Injectable({
    providedIn: "root",
})
export class OrderService {
    private refundModalConfig: any = {};

    constructor(
        private dbService: DbService,
        private dateService: DateService,
        private utilService: UtilService,
        private errorService: ErrorService,
        private quantityService: QuantityService,
        private settingsService: SettingsService
    ) {}

    getOrderItemStatus(
        order: Order
    ): {
        status?: string;
        statusType?: string;
        statusText?: string;
        statusIcon?: string;
    } {
        const status = order.statusNew || order.status;
        const REFUND_TYPES_CONFIG = this.settingsService.getSettingsValue(
            "refundTypesConfig"
        );
        const ORDER_STATUS_CONFIG = this.settingsService.getSettingsValue(
            "orderStatusConfig"
        );
        const ORDER_REFUND_STATES = this.settingsService.getSettingsValue(
            "orderRefundStates"
        );
        const ORDER_STATUS_NEW_CONFIG = this.settingsService.getSettingsValue(
            "orderStatusNewConfig"
        );

        if (ORDER_REFUND_STATES.indexOf(status) > -1) {
            const refundType = this.utilService.getNestedValue(
                order,
                "statusContext.refundType"
            );

            return (refundType && REFUND_TYPES_CONFIG[refundType]) || {};
        }

        return (
            ORDER_STATUS_NEW_CONFIG[status] || ORDER_STATUS_CONFIG[status] || {}
        );
    }

    /* Check whether AD Modal post order is enabled and needs to be shown to user based
    on impression limitation and whether order has AD deliveries or not */
    async canShowADModal(
        adEligible: boolean,
        cartItems: CartItem[]
    ): Promise<boolean> {
        if (!adEligible || !this.getFirstADDeliveryFromCart(cartItems)) {
            return false;
        }

        let userImpressionCount = 0;
        const config: OrderSuccessADModalConfig = this.settingsService.getSettingsValue(
            "orderSuccessADModal"
        );

        try {
            userImpressionCount = await this.dbService.getData(
                LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_AD_MODAL_IMPRESSION
            );
        } catch (err) {
            this.errorService.logSentryError(err, {
                message: "Error reading Order Success AD Modal impression",
            });
        }

        return (
            config &&
            config.enabled &&
            config.impressionLimit > userImpressionCount
        );
    }

    async updateADModalImpression() {
        try {
            const impressionCount =
                (await this.dbService.getData(
                    LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_AD_MODAL_IMPRESSION
                )) || 0;

            this.dbService.setData(
                LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_AD_MODAL_IMPRESSION,
                impressionCount + 1
            );
        } catch (err) {
            this.errorService.logSentryError(err, {
                message:
                    "Error reading/updating Order Success AD Modal impression",
            });
        }
    }

    async canShowWhatsappModal(hasOptedIn: boolean): Promise<boolean> {
        if (hasOptedIn) {
            return false;
        }

        const config: OrderSuccessWhatsappConfig = this.settingsService.getSettingsValue(
            "orderSuccessWhatsappModal"
        );

        if (!config || !config.enabled) {
            return false;
        }

        let impressionData: OrderSuccessWhatsappImpressionData;

        try {
            impressionData = await this.dbService.getData(
                LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_WHATSAPP_IMPRESSION
            );
        } catch (err) {
            this.errorService.logSentryError(err, {
                message:
                    "Error reading Order Success Whatsapp Modal impression",
            });
        }

        /* don't show modal if unable to read and validate current impression data */
        if (this.utilService.isEmpty(impressionData)) {
            return config.enabled;
        }

        const daysSinceLastImpression = this.dateService.daysFromToday(
            impressionData.date
        );

        /* check if last impression was before n days or if the permissible limit
        within a period of n days isn't exceeded, total impression limit isn't exceeded
        and flag is enabled */
        return (
            config.enabled &&
            (impressionData.totalCount || 0) < config.impressionTotalLimit &&
            (Math.abs(daysSinceLastImpression) > config.impressionDayOffset ||
                (impressionData.periodicCount || 0) <
                    config.impressionPeriodicLimit)
        );
    }

    /* Updates the date of latest impression, overall impression count and
    impression count within the offset period or resets it if last window is over
    e.g: if window is 7 days and last impression was before 7 days, we reset the
    periodic impression to 1 else we increment it by 1 */
    async updateWhatsappModalImpression() {
        try {
            let newImpressionData: OrderSuccessWhatsappImpressionData = {
                totalCount: 1,
                periodicCount: 1,
                date: this.dateService.getTodayDate(),
            };
            const config: OrderSuccessWhatsappConfig = this.settingsService.getSettingsValue(
                "orderSuccessWhatsappModal"
            );
            const impressionData: OrderSuccessWhatsappImpressionData = await this.dbService.getData(
                LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_WHATSAPP_IMPRESSION
            );

            if (impressionData) {
                const daysSinceLastImpression = this.dateService.daysFromToday(
                    impressionData.date
                );
                newImpressionData = {
                    ...newImpressionData,
                    totalCount: (impressionData.totalCount || 0) + 1,
                    periodicCount:
                        Math.abs(daysSinceLastImpression) >
                        config.impressionDayOffset
                            ? 1
                            : (impressionData.periodicCount || 0) + 1,
                };
            }

            this.dbService.setData(
                LOCAL_DB_DATA_KEYS.ORDER_SUCCESS_WHATSAPP_IMPRESSION,
                newImpressionData
            );
        } catch (err) {
            this.errorService.logSentryError(err, {
                message:
                    "Error reading/updating Order Success Whatsapp Modal impression",
            });
        }
    }

    /* Check whether cart items contain atleast one AD Order and return the same */
    getFirstADDeliveryFromCart(cartItems: CartItem[]): CartItem {
        if (!this.utilService.isLengthyArray(cartItems)) {
            return;
        }

        const firstADDelivery = cartItems.find(
            (cartItem: CartItem) =>
                cartItem.delivery_type ===
                CartItemDeliveryType.ALTERNATE_DELIVERY
        );

        return firstADDelivery;
    }

    /**
     * Check if order needs to show a tooltip for details
     *
     * @param {Order} order
     * @returns {boolean}
     * @memberof OrderService
     */
    isOrderTooltipRequired(order: Order): boolean {
        const status = this.utilService.getNestedValue(order, "statusNew");
        const tooltipStates =
            this.settingsService.getSettingsValue(
                SETTINGS.ORDER_TOOLTIP_STATES
            ) || [];

        return tooltipStates.indexOf(status) > -1;
    }

    /**
     * Get scheduled quantity text for Order History Tile
     *
     * @param {Sku} sku
     * @param {Order} order
     * @returns {string}
     * @memberof OrderService
     */
    getOrderHistoryTileScheduledQtyText(sku: Sku, order: Order): string {
        if (this.utilService.isEmpty(sku)) {
            return "";
        }

        const deliveredQuantity = this.utilService.getNestedValue(
            order,
            "delivered_quantity",
            0
        );
        const scheduledQuantity = this.utilService.getNestedValue(
            order,
            "scheduled_quantity",
            0
        );

        return scheduledQuantity <= deliveredQuantity
            ? ""
            : this.quantityService.toText(sku, scheduledQuantity);
    }

    /**
     * Get sa context Order History Refund Tooltip
     *
     * @param {Order} order
     * @returns {string}
     * @memberof OrderService
     */
    getRefundTooltipSaContext(order: Order): string {
        const isSubscription = this.utilService.getNestedValue(
            order,
            "subscription_id"
        );
        const status = this.utilService.getNestedValue(order, "statusNew");

        switch (status) {
            case NEW_ORDER_STATUS_TYPES.REFUND_FULL:
                return isSubscription ? "full-sub" : "full-addon";
            case NEW_ORDER_STATUS_TYPES.REFUND_PARTIAL:
                return isSubscription ? "partial-sub" : "partial-addon";
            default:
                return "";
        }
    }

    /**
     * Get Order Refund Modal adjustment text info
     *
     * @param {Sku} sku
     * @param {Order} order
     * @returns {OrderRefundModalAdjustmentInfo}
     * @memberof OrderService
     */
    getOrderRefundModalAdjustmentInfo(
        sku: Sku,
        order: Order
    ): OrderRefundModalAdjustmentInfo[] {
        this.setRefundModalConfig();

        const isSubscription = this.utilService.getNestedValue(
            order,
            "subscription_id"
        );
        const orderType = isSubscription ? "subscription" : "addon";
        const refundQty = this.getOrderRefundedQty(sku, order);
        const isOrderInRefundState = this.isOrderInRefundState(order);
        const status = isOrderInRefundState
            ? this.utilService.getNestedValue(order, "statusContext.refundType")
            : this.utilService.getNestedValue(order, "statusNew");
        const scheduledQty = this.utilService.getNestedValue(
            order,
            "scheduled_quantity"
        );
        const deliveredQty = this.utilService.getNestedValue(
            order,
            "delivered_quantity"
        );
        const refundAmount = this.utilService.getNestedValue(
            order,
            "statusContext.refundAmount"
        );
        const rescheduledDate = this.utilService.getNestedValue(
            order,
            "rescheduled_date"
        );

        const refundText = this.quantityService.toText(sku, refundQty);
        const scheduledText = this.quantityService.toText(sku, scheduledQty);
        const deliveredText = this.quantityService.toText(sku, deliveredQty);

        const orderedTitle = this.utilService.getNestedValue(
            this.refundModalConfig,
            `${status}.${orderType}.ordered`
        );
        const deliveredTitle = this.utilService.getNestedValue(
            this.refundModalConfig,
            `${status}.${orderType}.delivered`
        );
        const qtyAdjustedTitle = this.utilService.getNestedValue(
            this.refundModalConfig,
            `${status}.${orderType}.qtyAdjusted`
        );

        const modalInfo = [];

        if (orderedTitle) {
            modalInfo.push({
                info: scheduledText,
                title: orderedTitle,
            });
        }
        if (deliveredTitle) {
            modalInfo.push({
                info: deliveredText,
                title: deliveredTitle,
            });
        }

        /* show refunded units for addons and those legacy subscriptions which don't have refund amount */
        if (qtyAdjustedTitle && (!isSubscription || !refundAmount)) {
            modalInfo.push({
                info: refundText,
                title: qtyAdjustedTitle,
                date:
                    this.dateService.isValidDateString(rescheduledDate) &&
                    this.dateService.formatDate(rescheduledDate, "dd LLL"),
            });
        }

        return modalInfo;
    }

    /**
     * Get Order Refund Modal refund text info
     *
     * @param {Order} order
     * @returns {OrderRefundModalAdjustmentInfo}
     * @memberof OrderService
     */
    getOrderRefundModalRefundInfo(
        order: Order
    ): OrderRefundModalAdjustmentInfo {
        this.setRefundModalConfig();
        const orderType = this.utilService.getNestedValue(
            order,
            "subscription_id"
        )
            ? "subscription"
            : "addon";
        const isOrderInRefundState = this.isOrderInRefundState(order);
        const status = isOrderInRefundState
            ? this.utilService.getNestedValue(order, "statusContext.refundType")
            : this.utilService.getNestedValue(order, "statusNew");
        const refundAmount = this.utilService.getNestedValue(
            order,
            "statusContext.refundAmount"
        );

        if (!this.isOrderInRefundState(order) || !refundAmount) {
            return;
        }

        return {
            info: refundAmount,
            title: this.utilService.getNestedValue(
                this.refundModalConfig,
                `${status}.${orderType}.amountAdjusted`
            ),
        };
    }

    /**
     * Get Order Refund Modal footer info
     *
     * @param {Order} order
     * @returns {OrderRefundModalAdjustmentInfo}
     * @memberof OrderService
     */
    getOrderRefundModalFooterInfo(
        order: Order
    ): OrderRefundModalAdjustmentInfo {
        this.setRefundModalConfig();
        const orderType = this.utilService.getNestedValue(
            order,
            "subscription_id"
        )
            ? "subscription"
            : "addon";
        const status = this.utilService.getNestedValue(order, "statusNew");
        const footerConfig = this.utilService.getNestedValue(
            this.refundModalConfig,
            `${status}.${orderType}.footerConfig`
        );

        return (
            !this.utilService.isEmpty(footerConfig) && {
                title: footerConfig.title,
                subtitle: footerConfig.subtitle,
            }
        );
    }

    /**
     * Check if order is in one of the refund states
     *
     * @param {Order} order
     * @returns {boolean}
     * @memberof OrderService
     */
    isOrderInRefundState(order: Order): boolean {
        const status = this.utilService.getNestedValue(order, "statusNew");
        const refundStates = this.settingsService.getSettingsValue(
            SETTINGS.ORDER_REFUND_STATES
        );

        return refundStates.indexOf(status) > -1;
    }

    /**
     * Get refund quantity for order History Tile
     *
     * @param {Sku} sku
     * @param {Order} order
     * @returns {number}
     * @memberof OrderService
     */

    getOrderRefundedQty(sku: Sku, order: Order): number {
        if (this.utilService.isEmpty(sku)) {
            return;
        }

        const deliveredQuantity = this.utilService.getNestedValue(
            order,
            "delivered_quantity",
            0
        );
        const scheduledQuantity = this.utilService.getNestedValue(
            order,
            "scheduled_quantity",
            0
        );

        return scheduledQuantity - deliveredQuantity;
    }

    /**
     * Initialize refund modal config
     * @returns {void}
     * @memberof OrderService
     */
    setRefundModalConfig(): void {
        if (this.utilService.isEmpty(this.refundModalConfig)) {
            this.refundModalConfig = this.settingsService.getSettingsValue(
                SETTINGS.ORDER_REFUND_MODAL_CONFIG
            );
        }
    }
}
