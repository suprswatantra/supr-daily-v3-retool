import { Injectable } from "@angular/core";

import { take } from "rxjs/operators";

import { Experiments, ExperimentConfigs } from "@models";

import { DbService } from "@services/data/db.service";
import { ApiService } from "@services/data/api.service";
import { StoreService } from "@services/data/store.service";
import { PlatformService } from "@services/util/platform.service";
import { UtilService } from "@services/util/util.service";
import { AnalyticsService } from "@services/integration/analytics.service";

@Injectable({
    providedIn: "root",
})
export class ExperimentsService {
    constructor(
        private dbService: DbService,
        private apiService: ApiService,
        private storeService: StoreService,
        private platformService: PlatformService,
        private utilService: UtilService,
        private analyticsService: AnalyticsService
    ) {}

    private updateExperiments(experimentConfigs: ExperimentConfigs) {
        this.dbService.setLocalData("experimentConfigs", experimentConfigs);
    }

    private setSampledEventFlag(experimentConfigs: ExperimentConfigs) {
        const isSampledEvent = this.utilService.getNestedValue(
            experimentConfigs,
            "sampledEvent",
            true
        );

        this.analyticsService.setSampledEvent(isSampledEvent);
    }

    getExperimentsData(): ExperimentConfigs {
        return this.dbService.getLocalData("experimentConfigs");
    }

    fetchExperiments(userId: number) {
        return this.apiService
            .fetchExperiments({
                platform: this.platformService.getPlatform(),
                version: this.platformService.getAppCheckVersion(),
                userId,
            })
            .pipe(take(1))
            .subscribe(
                (experiments: Experiments) => {
                    this.updateExperiments(experiments.configs);
                    this.setSampledEventFlag(experiments.configs);
                    this.analyticsService.setActiveExperimentNames(
                        experiments.activeExperiments.toString()
                    );
                    this.storeService.setExperimentsFetched(true);
                },
                (_err) => {
                    this.storeService.setExperimentsFetched(true);
                }
            );
    }
}
