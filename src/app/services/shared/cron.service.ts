import { Injectable } from "@angular/core";

import { AuthService } from "@services/data/auth.service";
import { StoreService } from "@services/data/store.service";

import {
    DELIVERY_STATUS_REFRESH_INTERVAL_DURATION,
    SKU_REFRESH_INTERVAL_DURATION,
} from "@constants";

@Injectable({
    providedIn: "root",
})
export class CronService {
    constructor(
        private authService: AuthService,
        private storeService: StoreService
    ) {}

    initHomePageCrons() {
        this.initializeDeliveryStatusRefreshTimer();
        this.initializeSkuRefreshTimer();
    }

    private initializeDeliveryStatusRefreshTimer() {
        window.setInterval(() => {
            if (this.canFireRequest()) {
                this.storeService.fetchDeliveryStatus(true);
            }
        }, DELIVERY_STATUS_REFRESH_INTERVAL_DURATION);
    }

    private initializeSkuRefreshTimer() {
        window.setInterval(() => {
            if (this.canFireRequest()) {
                this.storeService.fetchSkuList(true);
                this.storeService.fetchSkuAttributes(true);
                this.storeService.fetchSkuV3CollectionList(true);
            }
        }, SKU_REFRESH_INTERVAL_DURATION);
    }

    private canFireRequest(): boolean {
        return this.authService.isAuthenticated && navigator.onLine;
    }
}
