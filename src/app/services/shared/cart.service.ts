import { BlockAccessService } from "./block-access.service";
import { Injectable } from "@angular/core";

import {
    CART_ITEM_TYPES,
    LOCAL_DB_DATA_KEYS,
    STORAGE_DB_DATA_KEYS,
    NUDGE_TYPES,
    NUDGE_SETTINGS_KEYS,
    FEATURE_FLAG_KEYS,
} from "@constants";

import { CartItem, Sku, Vacation, CartMeta, CartPrompt } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";

import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";
import { DbService } from "@services/data/db.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { NudgeService } from "@services/layout/nudge.service";

import { StoreService } from "../data/store.service";

import { SuprApi, SkuDict, ExitCartSetting } from "@types";

@Injectable({
    providedIn: "root",
})
export class CartService {
    private deliveryDate: string;
    // Keeping this flag in memory so that we know that cart prompt is shown in a session.
    private cartPromptShownInSession = false;

    constructor(
        private dateService: DateService,
        private calendarService: CalendarService,
        private dbService: DbService,
        private storeService: StoreService,
        private settingsService: SettingsService,
        private utilsService: UtilService,
        private skuService: SkuService,
        private nudgeService: NudgeService,
        private blockAccessService: BlockAccessService
    ) {
        this.updateStoreWithCart();
    }

    /**
     * Has 4 level checks to show cart prompt
     * 1. Supr Access auto add to cart prompt should not be shown
     * 2. Cart prompt should be shown only once in a session. Keeping a flag in memory
     * 3. Cart prompt should be shown only once for a unique cart id (uuid)
     * 4. There is a global max count cap for the user. The shown count should not exceed that.
     *
     * @param {CartMeta} cartMeta
     * @param {string} cartId
     * @returns {Promise<boolean>}
     * @memberof CartService
     */
    async canShowCartPrompt(
        cartMeta: CartMeta,
        cartId: string
    ): Promise<boolean> {
        // basic check
        if (!this.isCartPromptDataPresent(cartMeta)) {
            return false;
        }

        // Check #1
        if (this.isSuprAccessPropmtShown(cartMeta)) {
            return false;
        }

        // Check #2
        if (this.isCartPromptShownInSession()) {
            return false;
        }

        // Check #3
        if (await this.isCartPromptShownForCurrentCartId(cartId)) {
            return false;
        }

        // Check #4
        if (await this.isCartPromptMaxCountBreached(cartMeta)) {
            return false;
        }

        return true;
    }

    async setCartPromptShownProps(cartId: string) {
        this.cartPromptShownInSession = true;

        const currentData = (await this.getCartPromptData()) || {};
        const shownCount = (currentData["shownCount"] || 0) + 1;
        const newData = { shownCount, cartId };

        return this.dbService.setData(this.getCartPromptKey(), newData);
    }

    getCartItemFromSku(sku: Sku, qty = 1, vacation?: Vacation): CartItem {
        let cartItemDate =
            this.deliveryDate ||
            this.calendarService.getNextAvailableDate(vacation, sku);

        /* if cartItemDate is set from this.deliveryDate(schedule flow)
        then make sure it does't collide with sku out of stock */
        if (
            cartItemDate === this.deliveryDate &&
            sku &&
            sku.out_of_stock &&
            sku.next_available_date
        ) {
            const daysBeforeSkuAvailability = this.dateService.daysBetweenTwoDates(
                this.dateService.getTodayDate(),
                this.dateService.dateFromText(sku.next_available_date)
            );
            const daysBeforeNextAvailableDate = this.dateService.daysBetweenTwoDates(
                this.dateService.getTodayDate(),
                this.dateService.dateFromText(cartItemDate)
            );

            /* If cartItemDate is before sku.next_available_date, then set cartItemDate
            to sku.next_available_date else let it be */
            cartItemDate =
                daysBeforeSkuAvailability > daysBeforeNextAvailableDate
                    ? sku.next_available_date
                    : cartItemDate;
        }

        this.handleSuprPassPostCutOffNudge();

        return {
            type: CART_ITEM_TYPES.ADDON,
            sku_id: sku.id,
            delivery_date: this.dateService.formatDate(cartItemDate),
            quantity: qty,
        };
    }

    getAddonItemFromSubscriptionItem(cartItem: CartItem): CartItem {
        if (this.utilsService.isEmpty(cartItem)) {
            return;
        }

        return {
            type: CART_ITEM_TYPES.ADDON,
            sku_id: cartItem.sku_id,
            delivery_date: cartItem.start_date,
            quantity: cartItem.quantity,
        };
    }

    updateCartItemWithQty(cartItem: CartItem, quantity: number): CartItem {
        if (this.isAddonCartItem(cartItem)) {
            return { ...cartItem, quantity };
        }

        // For subscription need to update recharge qty too
        const { recharge_quantity: rechargeQty, quantity: _qty } = cartItem;

        const noOfDeliveries = rechargeQty / _qty;
        const newRechargeQty = noOfDeliveries * quantity;

        return { ...cartItem, recharge_quantity: newRechargeQty, quantity };
    }

    setCartInDB(cartBody: SuprApi.CartBody) {
        this.dbService.setData(STORAGE_DB_DATA_KEYS.CART, cartBody);
    }

    clearCartFromDB() {
        this.dbService.clearData(STORAGE_DB_DATA_KEYS.CART);
    }

    setAutoCheckoutFlag() {
        this.dbService.setLocalData(LOCAL_DB_DATA_KEYS.AUTO_CHECKOUT, true);
    }

    getAutoCheckoutFlag(): boolean {
        return this.dbService.getLocalData(LOCAL_DB_DATA_KEYS.AUTO_CHECKOUT);
    }

    async setSuprPassRemovedInfo() {
        const existingSuprPassRemovedInfo = await this.getSuprPassRemovedInfo();
        let count = 1;
        if (existingSuprPassRemovedInfo && existingSuprPassRemovedInfo.count) {
            count = existingSuprPassRemovedInfo.count + 1;
        }
        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.SUPR_ACCESS_REMOVAL_FROM_CART_INFO,
            { removed: true, timestamp: new Date().getTime(), count }
        );
    }

    async removeReferralCouponFromCart() {
        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.IS_REFERRAL_COUPON_REMOVED_FROM_CART,
            true
        );
    }

    getIsReferralCouponRemovedFromCart() {
        return this.dbService.getData(
            STORAGE_DB_DATA_KEYS.IS_REFERRAL_COUPON_REMOVED_FROM_CART
        );
    }

    getSuprPassRemovedInfo() {
        return this.dbService.getData(
            STORAGE_DB_DATA_KEYS.SUPR_ACCESS_REMOVAL_FROM_CART_INFO
        );
    }

    clearAutoCheckoutFlag() {
        this.dbService.clearLocalData(LOCAL_DB_DATA_KEYS.AUTO_CHECKOUT);
    }

    setDeliveryDate(date: string) {
        this.deliveryDate = date;
    }

    getDeliveryDate(): string {
        return this.deliveryDate;
    }

    clearDeliveryDate() {
        this.deliveryDate = null;
    }

    restoreCartFromDb() {
        this.updateStoreWithCart();
    }

    autoPushDeliveryDates(
        cartItems: Array<CartItem>,
        vacation: Vacation,
        skuDict: SkuDict
    ): Array<CartItem> {
        const updatedItems: Array<CartItem> = cartItems
            .filter((i) => i)
            .reduce((accm, cartItem) => {
                const { delivery_date, start_date, type } = cartItem;
                if (type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE) {
                    return [...accm, { ...cartItem }];
                }
                const isAddon = this.isAddonCartItem(cartItem);
                let deliveryDate = isAddon ? delivery_date : start_date;

                // If date not found for some reason, use tomorrow's date and log to sentry
                if (!deliveryDate) {
                    deliveryDate = this.dateService.textFromDate(
                        this.dateService.getTomorrowDate()
                    );
                    if (isAddon) {
                        cartItem["delivery_date"] = deliveryDate;
                    } else {
                        cartItem["start_date"] = deliveryDate;
                    }
                }

                const isValid = this.calendarService.isDeliveryDateValid(
                    deliveryDate,
                    vacation
                );
                const sku = skuDict[cartItem.sku_id];

                // cheange delivery date if it falls during vacation or sku is unavailable
                if (
                    !isValid ||
                    !this.skuService.isSkuAvailableOnGivenDate(
                        sku,
                        deliveryDate
                    )
                ) {
                    const nextAvblDate = this.calendarService.getNextAvailableDate(
                        vacation,
                        sku
                    );
                    const nextAvblDateText = this.dateService.textFromDate(
                        nextAvblDate
                    );

                    if (isAddon) {
                        return [
                            ...accm,
                            { ...cartItem, delivery_date: nextAvblDateText },
                        ];
                    }

                    return [
                        ...accm,
                        { ...cartItem, start_date: nextAvblDateText },
                    ];
                }

                return [...accm, { ...cartItem }];
            }, []);
        return updatedItems;
    }

    sendExitCartPromptNudge() {
        this.storeService.showExitCartModal();
    }

    showAlternatesFlow(sku: Sku, key = "addon") {
        if (sku) {
            /* If coming from schedule calendar flow and sku is available on that day,
            don't show alternates flow */
            if (
                this.deliveryDate &&
                this.skuService.isSkuAvailableOnGivenDate(
                    sku,
                    this.deliveryDate
                )
            ) {
                return false;
            }

            if (sku.out_of_stock && sku.out_of_stock_preferred_flow) {
                const showAlternateProducts =
                    sku.out_of_stock_preferred_flow[key] === "alternates" ||
                    (sku.out_of_stock_preferred_flow[key] === "schedule" &&
                        !sku.next_available_date);

                return showAlternateProducts;
            }

            return false;
        }
        return false;
    }

    async canShowCartExitPrompt(): Promise<boolean> {
        // Check whether feature is enabled
        const flagKey = FEATURE_FLAG_KEYS.CART_EXIT_PROMPT_ENABLED;
        const isEnabled = this.settingsService.getSettingsValue(flagKey);

        if (!isEnabled) {
            return false;
        }

        // Check whether settings are defined
        const nudgeType = NUDGE_TYPES.EXIT_CART;
        const key = NUDGE_SETTINGS_KEYS[nudgeType];

        const nudgeSettings = <ExitCartSetting.Nudge>(
            this.settingsService.getSettingsValue(key)
        );

        if (this.utilsService.isEmpty(nudgeSettings)) {
            return false;
        }

        // Check whether cart is present
        const cart = await this.getCart();
        if (this.utilsService.isEmpty(cart)) {
            return false;
        }

        return true;
    }

    cleanUpCartSkus(cartItems: CartItem[], skuList: Sku[]): CartItem[] {
        if (!cartItems || !skuList) {
            return [];
        }

        return cartItems.filter((cartItem: CartItem) => {
            return skuList.find(
                (sku: Sku) => sku.id === (cartItem && cartItem.sku_id)
            );
        });
    }

    isCheckoutDisabled(userCheckoutInfo: CheckoutInfo): boolean {
        if (userCheckoutInfo && userCheckoutInfo.checkoutDisabled) {
            this.nudgeService.sendCheckoutDisabledNudge();
            return true;
        }

        return false;
    }

    private async handleSuprPassPostCutOffNudge() {
        /* 
            Check if cutoff time has passed and time based experiment is active from blockaccess service
            if yes, show nudge
        */

        if (
            !this.blockAccessService.isTimeBasedExperimentActive() ||
            !this.dateService.hasTodaysCutOffTimePassed()
        ) {
            return;
        }

        // Show nudge only if cart is empty, i.e user is adding first item
        const cart = await this.getCart();
        if (this.utilsService.isEmpty(cart)) {
            this.blockAccessService.showBlockAcessNudge(true);
        }
        return;
    }

    private getCart(): Promise<SuprApi.CartBody> {
        return this.dbService.getData(STORAGE_DB_DATA_KEYS.CART);
    }

    private isAddonCartItem(item: CartItem): boolean {
        return item.type === CART_ITEM_TYPES.ADDON;
    }

    private updateStoreWithCart() {
        this.dbService
            .getData(STORAGE_DB_DATA_KEYS.CART)
            .then((cartBody) => {
                if (cartBody && cartBody.items && cartBody.items.length) {
                    this.storeService.updateCartInStore(cartBody);
                } else {
                    this.storeService.clearCart();
                }
            })
            .catch(() => {
                /** */
            });
    }

    private getCartPromptData(): Promise<any> {
        return this.dbService.getData(this.getCartPromptKey());
    }

    private getCartPromptKey(): string {
        return `${STORAGE_DB_DATA_KEYS.CART_PROMPT}`;
    }

    private isCartPromptDataPresent(cartMeta: CartMeta): boolean {
        return this.utilsService.getNestedValue(cartMeta, "cart_prompt", false);
    }

    private isSuprAccessPropmtShown(cartMeta: CartMeta): boolean {
        return !!this.utilsService.getNestedValue(
            cartMeta,
            "show_supr_pass_prompt",
            false
        );
    }

    private isCartPromptShownInSession(): boolean {
        return this.cartPromptShownInSession;
    }

    private async isCartPromptShownForCurrentCartId(
        cartId: string
    ): Promise<boolean> {
        const dbData = (await this.getCartPromptData()) || {};
        if (cartId === dbData["cartId"]) {
            return true;
        }

        return false;
    }

    private async isCartPromptMaxCountBreached(
        cartMeta: CartMeta
    ): Promise<boolean> {
        const dbData = (await this.getCartPromptData()) || {};

        const promptData: CartPrompt = this.utilsService.getNestedValue(
            cartMeta,
            "cart_prompt",
            null
        );

        if (!promptData) {
            return false;
        }

        if (
            promptData.maxCount &&
            dbData["shownCount"] >= promptData.maxCount
        ) {
            return true;
        }

        return false;
    }
}
