import { AudioService } from "./util/audio.service";
import { DbService } from "@services/data/db.service";
import { ApiService } from "@services/data/api.service";
import { AuthService } from "@services/data/auth.service";
import { StoreService } from "@services/data/store.service";

import { DateService } from "@services/date/date.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { CalendarService } from "@services/date/calendar.service";

import { ErrorService } from "@services/integration/error.service";
import { PaymentService } from "@services/integration/payment.service";
import { FreshChatService } from "@services/integration/freshchat.service";
import { SegmentService } from "@services/integration/segment.service";
import { SmartlookService } from "@services/integration/smartlook.service";
import { SmsService } from "@services/integration/sms.service";
import { FirebaseService } from "@services/integration/firebase.service";
import { MoengageService } from "@services/integration/moengage.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { FreshBotService } from "@services/integration/freshbot.service";
import { RudderStackService } from "@services/integration/rudder-stack.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { ModalService } from "@services/layout/modal.service";
import { NudgeService } from "@services/layout/nudge.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { ToastService } from "@services/layout/toast.service";
import { ComponentService } from "@services/layout/component.service";
import { HomeLayoutService } from "@services/layout/home-layout.service";
import { SupportService } from "@services/layout/support.service";

import { CartService } from "@services/shared/cart.service";
import { AddressService } from "@services/shared/address.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { OrderService } from "@services/shared/order.service";
import { SubscriptionService } from "@services/shared/subscription.service";
import { MapService } from "@services/shared/map.service";
import { WalletService } from "@services/shared/wallet.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { SettingsService } from "@services/shared/settings.service";
import { CronService } from "@services/shared/cron.service";
import { ExperimentsService } from "@services/shared/experiments.service";
import { SkuService } from "@services/shared/sku.service";
import { VacationService } from "@services/shared/vacation.service";
import { OOSInfoService } from "./shared/oos-info.service";
import { UserService } from "./shared/user.service";
import { BlockAccessService } from "./shared/block-access.service";
import { ReferralService } from "./shared/referral.service";

import { UtilService } from "@services/util/util.service";
import { ImageService } from "@services/util/image.service";
import { RouterService } from "@services/util/router.service";
import { LocationService } from "@services/util/location.service";
import { PlatformService } from "@services/util/platform.service";
import { InitializeService } from "@services/util/initialize.service";
import { NativeDeeplinksService } from "@services/util/deeplinks.service";
import { FreshbotUtils } from "@services/util/freshbotUtil.service";

export const providers = [
    DbService,
    ApiService,
    AuthService,
    StoreService,
    SettingsService,
    ExperimentsService,

    DateService,
    SuprDateService,
    CalendarService,

    ErrorService,
    PaymentService,
    FreshChatService,
    SegmentService,
    SmartlookService,
    SmsService,
    FirebaseService,
    MoengageService,
    AppsFlyerService,
    FreshBotService,
    RudderStackService,
    AnalyticsService,

    ModalService,
    NudgeService,
    ToastService,
    FeedbackService,
    ComponentService,
    HomeLayoutService,
    SupportService,

    SkuService,
    CartService,
    ScheduleService,
    AddressService,
    OrderService,
    CronService,
    MapService,
    WalletService,
    SuprPassService,
    VacationService,
    OOSInfoService,
    UserService,
    BlockAccessService,
    ReferralService,

    UtilService,
    ImageService,
    RouterService,
    PlatformService,
    LocationService,
    InitializeService,
    SubscriptionService,
    FreshbotUtils,
    NativeDeeplinksService,
    AudioService,
];
