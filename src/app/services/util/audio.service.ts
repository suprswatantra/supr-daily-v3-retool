import { Injectable } from "@angular/core";
import { NativeAudio } from "@ionic-native/native-audio/ngx";

import { PRELOAD_AUDIO_LIST } from "@constants";
import { PlatformService } from "@services/util/platform.service";
import { UtilService } from "./util.service";

interface Sound {
    key: string;
    asset: string;
}

@Injectable({
    providedIn: "root",
})
export class AudioService {
    private sounds: Sound[] = [];
    private audioPlayer: HTMLAudioElement = new Audio();
    private forceWebAudio = true;
    private soundVolume = 0.9;
    private isEnabled = true;

    constructor(
        private platformService: PlatformService,
        private utilService: UtilService,
        private nativeAudio: NativeAudio
    ) {}

    initialize() {
        PRELOAD_AUDIO_LIST.forEach((item: Sound) => {
            this.preload(item.key, item.asset);
        });
    }

    preload(key: string, asset: string) {
        if (!this.isEnabled) {
            return;
        }
        if (this.useNative()) {
            this.nativeAudio.preloadComplex(key, asset, this.soundVolume, 0, 0);
        } else {
            const audio = new Audio();
            audio.src = asset;
        }
        this.sounds.push({
            key: key,
            asset: asset,
        });
    }

    play(key: string): void {
        if (!this.isEnabled) {
            return;
        }
        const soundToPlay = this.sounds.find((sound) => {
            return sound.key === key;
        });

        if (this.utilService.isEmpty(soundToPlay)) {
            return;
        }

        if (this.useNative()) {
            this.nativeAudio.play(soundToPlay.key).then(
                (res) => {
                    console.log("asset loaded: ", res);
                },
                (err) => {
                    console.log("asset loading failed: ", err);
                }
            );
        } else {
            this.audioPlayer.src = soundToPlay.asset;
            this.audioPlayer.volume = this.soundVolume;
            this.audioPlayer.play();
        }
    }

    setSoundVolume(volume: number) {
        if (volume && volume >= 0.0 && volume <= 1.0) {
            this.soundVolume = volume;
            if (this.utilService.isLengthyArray(this.sounds)) {
                this.updateVolumeOfLoadedAssets();
            }
        }
    }

    setAppAudioEnabledFlag(value: boolean) {
        this.isEnabled = value;
    }

    private updateVolumeOfLoadedAssets() {
        if (this.utilService.isLengthyArray(this.sounds)) {
            this.sounds.forEach((item) => {
                this.nativeAudio.setVolumeForComplexAsset(
                    item.key,
                    this.soundVolume
                );
            });
        }
    }

    private useNative() {
        return this.platformService.isCordova() && !this.forceWebAudio;
    }
}
