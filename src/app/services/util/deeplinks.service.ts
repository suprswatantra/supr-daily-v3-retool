import { Injectable } from "@angular/core";

import { Subscription } from "rxjs";

import { Deeplinks } from "@ionic-enterprise/deeplinks/ngx";

import { StoreService } from "@services/data/store.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { UtilService } from "./util.service";
import { PlatformService } from "./platform.service";
import { UTM_PARAMS } from "@constants";

@Injectable({
    providedIn: "root",
})
export class NativeDeeplinksService {
    private deeplinkSub: Subscription;

    constructor(
        private deeplinks: Deeplinks,
        private utilService: UtilService,
        private storeService: StoreService,
        private platformService: PlatformService,
        private analyticsService: AnalyticsService
    ) {}

    handleNativeDeepLinks() {
        if (!this.isEnabled()) {
            return;
        }

        this.deeplinkSub = this.deeplinks.route({}).subscribe(
            (_match) => {},
            (noMatch) => {
                let url: string;
                const path = this.utilService.getNestedValue(
                    noMatch,
                    "$link.path",
                    null
                );

                const queryString = this.utilService.getNestedValue(
                    noMatch,
                    "$link.queryString",
                    null
                );

                const urlString = this.utilService.getNestedValue(
                    noMatch,
                    "$link.url",
                    null
                );

                const isNativeDeeplink =
                    urlString &&
                    urlString.indexOf("suprapp://suprdaily.com") !== -1;

                if ((!path && !queryString) || !isNativeDeeplink) {
                    this.unsubscribeDeeplinkAndSubscribeAgain();
                    return;
                }

                if (queryString) {
                    url = path + "?" + queryString;
                    this.setUtmFromQueryParams(queryString);
                } else {
                    url = path;
                }

                this.storeService.setPNData({
                    pnType: "deeplink",
                    url: url,
                    source: "native",
                });

                this.unsubscribeDeeplinkAndSubscribeAgain();
            }
        );
    }

    private unsubscribeDeeplinkAndSubscribeAgain() {
        if (this.deeplinkSub && !this.deeplinkSub.closed) {
            this.deeplinkSub.unsubscribe();
        }

        this.handleNativeDeepLinks();
    }

    private isEnabled(): boolean {
        return this.platformService.isCordova();
    }

    private setUtmFromQueryParams(query = "") {
        if (!query) {
            return;
        }

        const searchParams = new URLSearchParams(query);
        const utmParams = {};

        searchParams.forEach((value, key) => {
            if (UTM_PARAMS.indexOf(key) !== -1) {
                utmParams[key] = value;
            }
        });

        this.analyticsService.setUtmParams(utmParams);
    }
}
