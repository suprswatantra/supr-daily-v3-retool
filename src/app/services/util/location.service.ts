import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { Diagnostic } from "@ionic-native/diagnostic/ngx";
import { Geolocation } from "@ionic-enterprise/geolocation/ngx";
import { LocationAccuracy } from "@ionic-native/location-accuracy/ngx";

import { CITY_LIST, LOCATION_AUTHORIZATION_STATUS } from "@constants";

import { City } from "@models";

import { ApiService } from "@services/data/api.service";

import { UtilService } from "./util.service";
import { PlatformService } from "./platform.service";

import { SuprApi, SuprMaps } from "@types";

import { MAP } from "@pages/address/constants";

@Injectable({
    providedIn: "root",
})
export class LocationService {
    constructor(
        private diagnostic: Diagnostic,
        private apiService: ApiService,
        private geolocation: Geolocation,
        private utilService: UtilService,
        private platformService: PlatformService,
        private locationAccuracy: LocationAccuracy
    ) {}

    async getLocationStatus(): Promise<string> {
        /* [TODO_RITESH] Do an RCA on why this plugin is not being installed on certain devices
        Refer Sentry Fixes Doc for error links */
        if (!this.diagnostic) {
            return;
        }

        const locationEnabled = await this.diagnostic.isLocationEnabled();
        if (!locationEnabled) {
            return "disabled";
        }
        return this.getLocationAuthorization();
    }

    async getLocationAccess(): Promise<boolean> {
        if (this.platformService.isIOS()) {
            return true;
        } else {
            try {
                /* Requesting High Accuracy Location on Android only */
                await this.locationAccuracy.request(
                    this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY
                );
                return true;
            } catch (err) {
                return false;
            }
        }
    }

    async getCurrentLocation(): Promise<SuprMaps.Location> {
        const position = await this.geolocation.getCurrentPosition({
            enableHighAccuracy: true,
        });
        const lat = this.utilService.getNestedValue(
            position,
            "coords.latitude"
        );
        const lng = this.utilService.getNestedValue(
            position,
            "coords.longitude"
        );

        return { lat, lng };
    }

    getSearchSuggestions(
        searchTerm: string
    ): Observable<SuprApi.PlaceSuggestionRes> {
        return this.apiService.getLocationSuggestions(searchTerm);
    }

    reverseGeoCode(geocodeInput: SuprMaps.GeocodeInput) {
        return this.apiService.reverseGeocode(geocodeInput);
    }

    getCityLocation(cityId: number): SuprMaps.Location {
        const cityData = CITY_LIST.find((city: City) => city.id === cityId);

        if (cityData) {
            return cityData.location;
        }

        return MAP.default;
    }

    compareLocations(
        locationA: SuprMaps.Location,
        locationB: SuprMaps.Location,
        precision: number
    ) {
        if (
            !this.isValidLocation(locationA) ||
            !this.isValidLocation(locationB)
        ) {
            return false;
        }

        return (
            locationA.lat.toFixed(precision) ===
                locationB.lat.toFixed(precision) &&
            locationA.lng.toFixed(precision) ===
                locationB.lng.toFixed(precision)
        );
    }

    isValidLocation(location: SuprMaps.Location): boolean {
        return !!(location && location.lat && location.lng);
    }

    private async getLocationAuthorization(): Promise<string> {
        /* [TODO_RITESH] Do an RCA on why this plugin is not being installed on certain devices
        Refer Sentry Fixes Doc for error links */
        if (!this.diagnostic) {
            return;
        }

        const locationAuthorization = await this.diagnostic.isLocationAuthorized();
        return locationAuthorization
            ? LOCATION_AUTHORIZATION_STATUS.ios.AUTHORIZED
            : LOCATION_AUTHORIZATION_STATUS.ios.UNAUTHORIZED;
    }
}
