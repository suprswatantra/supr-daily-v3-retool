import { Injectable } from "@angular/core";
import { User } from "@shared/models";
import { CLIENT_PARAMS } from "@constants";

@Injectable({
    providedIn: "root",
})
export class FreshbotUtils {
    freshbotApis(token, dates, user) {
        const { todaysDate, previousDate } = dates;
        const {
            previousComplaintDate,
            complaintPayload,
            ticketIdAndTypes,
            fetchingQuantity,
            orderSummaryCustomisedButtons,
            ondComplaintPayload,
            getClientParams,
        } = this;
        return {
            getClientParams: getClientParams(
                user,
                token,
                todaysDate,
                previousDate
            ),
            previousComplaintDate,
            complaintPayload,
            ticketIdAndTypes,
            fetchingQuantity,
            orderSummaryCustomisedButtons,
            ondComplaintPayload,
        };
    }

    formattedDate(date: string): string {
        if (date) {
            const dateArray = date.split("-");
            const [year, month, day] = dateArray;
            const formattedDate = new Date(
                parseInt(year),
                parseInt(month) - 1,
                parseInt(day)
            );
            const shortMonth = formattedDate.toLocaleString("default", {
                month: "short",
            });
            const weekday = formattedDate.toLocaleDateString("default", {
                weekday: "short",
            });
            return `${weekday}, ${day} ${shortMonth}`;
        }
        return "";
    }

    private getClientParams(
        user: User,
        token: string,
        todaysDate: string,
        previousDate: string
    ) {
        return {
            [CLIENT_PARAMS.AUTH]: token,
            [CLIENT_PARAMS.TODAY_DATE]: todaysDate,
            [CLIENT_PARAMS.TODAYS_DATE]: todaysDate,
            [CLIENT_PARAMS.PREVIOUS_DATE]: previousDate,
            [CLIENT_PARAMS.USR_NAME]: user.lastName
                ? user.firstName + " " + user.lastName
                : user.firstName,
            [CLIENT_PARAMS.USER_NUMBER]: user.mobileNumber,
            [CLIENT_PARAMS.USER_ID]: `${user.id}`,
        };
    }

    private ondComplaintPayload(orders, date: string) {
        try {
            const orderData = JSON.parse(orders);
            const { order_history = [], sku_details = [] } =
                orderData && orderData.data;

            const orderArray =
                (order_history &&
                    order_history.length > 0 &&
                    order_history.find((order) => {
                        return order.date === date;
                    })) ||
                [];

            let skuObj = {};
            sku_details &&
                sku_details.forEach((sku) => {
                    skuObj[sku.id] = sku.sku_name;
                });

            const ordersPayload =
                (orderArray &&
                    orderArray.orders &&
                    orderArray.orders.length > 0 &&
                    orderArray.orders.map((order) => {
                        return {
                            order_id: parseInt(order.id),
                            sku_id: parseInt(order.sku_id),
                            sku_name: skuObj[order.sku_id],
                            scheduled_quantity: parseInt(
                                order.delivered_quantity
                            ),
                            service_recovery_quantity: parseInt(
                                order.delivered_quantity
                            ),
                            refund_quantity: 0,
                        };
                    })) ||
                [];
            return {
                orders: ordersPayload || [],
            };
        } catch (error) {
            return { orders: [] };
        }
    }

    private previousComplaintDate(date: string) {
        try {
            if (!date) {
                return "";
            }
            const [dateString = "", time = ""] = date.split("T");
            if (!dateString || !time) {
                return "";
            }
            const dateArray = dateString.split("-");
            const [hours = "", minutes = ""] = time.split(":");
            const formattedHours = hours && ((parseInt(hours) + 24) % 12 || 12);
            const timeZone = formattedHours > 12 ? "PM" : "AM";
            const [year = "", month = "", day = ""] = dateArray;
            const formattedDate = new Date(
                parseInt(year),
                parseInt(month) - 1,
                parseInt(day)
            );
            const shortMonth = formattedDate.toLocaleString("default", {
                month: "short",
            });
            const weekday = formattedDate.toLocaleDateString("default", {
                weekday: "short",
            });
            return {
                date: `${weekday}, ${day} ${shortMonth} ${formattedHours}: ${minutes} ${timeZone}`,
            };
        } catch (error) {
            return { date: "" };
        }
    }

    private complaintPayload(
        orders,
        id: string,
        skus,
        service_recovery_quantity: string
    ) {
        try {
            const newOrders = JSON.parse(orders);
            const newSkus = JSON.parse(skus);
            const order =
                (newOrders &&
                    newOrders.orders &&
                    newOrders.orders.find((order) => {
                        return order.order_id === id;
                    })) ||
                {};
            const sku =
                (newSkus &&
                    newSkus.skus &&
                    newSkus.skus.find((sku) => {
                        return sku.id === order.sku_id;
                    })) ||
                {};

            const { sku_id = "", delivered_quantity = "" } = order;
            const { sku_name = "" } = sku;

            return {
                orders: [
                    {
                        order_id: id && parseInt(id),
                        sku_id: sku_id && parseInt(sku_id),
                        sku_name,
                        scheduled_quantity:
                            delivered_quantity && parseInt(delivered_quantity),
                        service_recovery_quantity:
                            service_recovery_quantity &&
                            parseInt(service_recovery_quantity),
                        refund_quantity: 0,
                    },
                ],
            };
        } catch (error) {
            return { orders: [] };
        }
    }

    private ticketIdAndTypes(tickets, type: string) {
        try {
            const newTickets = JSON.parse(tickets);
            const newType = JSON.parse(type);
            const returnTicket =
                (newTickets &&
                    newTickets.tickets &&
                    newTickets.tickets.find(
                        (ticket) => ticket.type === newType.type
                    )) ||
                {};
            const {
                id = "",
                type: ticketType = "",
                custom_description_text = "",
            } = returnTicket;
            return {
                id,
                type: ticketType,
                custom_description_text,
            };
        } catch (error) {
            return {};
        }
    }

    private fetchingQuantity(orders, id: string) {
        try {
            const newOrders = JSON.parse(orders);
            const order =
                (newOrders &&
                    newOrders.orders &&
                    newOrders.orders.find((order) => {
                        return order.order_id === id;
                    })) ||
                {};

            const { delivered_quantity = "" } = order;
            const formatteDeliveryQuantity =
                delivered_quantity && parseInt(delivered_quantity);
            const quantityDropDown =
                Array.from(
                    { length: formatteDeliveryQuantity },
                    (v, k: number) => {
                        v = k + 1;
                        return v;
                    }
                ) || [];

            return {
                quantity: formatteDeliveryQuantity,
                quantityDropDown,
            };
        } catch (error) {
            return {};
        }
    }

    private orderSummaryCustomisedButtons(orders, skus, type: string) {
        try {
            const newOrders = JSON.parse(orders);
            const newSkus = JSON.parse(skus);

            const modifiedDate =
                newOrders &&
                newOrders.orders &&
                newOrders.orders.map((item) => {
                    const sku =
                        (newSkus &&
                            newSkus.skus &&
                            newSkus.skus.find((sku) => {
                                return sku.id === item.sku_id;
                            })) ||
                        [];

                    const {
                        date = "",
                        order_id = "",
                        delivered_quantity = 0,
                    } = item;
                    const { sku_name = "" } = sku;
                    if (type === "orderSummary") {
                        return {
                            value: date,
                            display: date && this.formattedDate(item.date),
                        };
                    } else {
                        return {
                            value: order_id,
                            fullUrl: sku.fullUrl,
                            display: `${sku_name} X  ${Math.floor(
                                delivered_quantity
                            )}`,
                        };
                    }
                });
            return {
                response: modifiedDate.reverse(),
                isOrders: modifiedDate && modifiedDate.length > 0,
            };
        } catch (error) {
            return { response: [] };
        }
    }
}
