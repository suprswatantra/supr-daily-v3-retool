import { Injectable } from "@angular/core";

import { UtilService } from "@services/util/util.service";

@Injectable({
    providedIn: "root",
})
export class CurrencyService {
    // Rupee sign
    rupee = String.fromCharCode(8377);

    constructor(private utilService: UtilService) {}

    toText(
        amountVal: number | string,
        decimalPlaces?: number,
        isSymbol?: boolean
    ): string {
        const amount = this.utilService.toValue(amountVal, decimalPlaces);
        const symbol = this.getSymbol(amount, isSymbol);
        const text = this.getText(amount, decimalPlaces);

        return symbol + text;
    }

    private getSymbol(amount: number, isSymbol: boolean): string {
        let symbol = "";
        if (amount < 0) {
            symbol = "-";
        }
        if (isSymbol !== false) {
            symbol += this.rupee;
        }
        return symbol;
    }

    private getText(amount: number, decimalPlaces?: number): string {
        let removeTrailingZero = false;
        if (decimalPlaces === undefined) {
            decimalPlaces = 2;
            removeTrailingZero = true;
        }

        const absAmount = Math.abs(amount);

        let text = absAmount.toFixed(decimalPlaces);
        const textInteger = text.split(".")[0];
        const textFraction = text.split(".")[1];

        // Remove extra zeroes
        if (removeTrailingZero) {
            if (textFraction === "00") {
                text = textInteger;
            }
        }

        // Add comma
        if (textInteger.length > 3) {
            text =
                text.slice(0, textInteger.length - 3) +
                "," +
                text.slice(textInteger.length - 3);
        }
        return text;
    }
}
