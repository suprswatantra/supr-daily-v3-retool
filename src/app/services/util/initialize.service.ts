import { Injectable } from "@angular/core";

import { map } from "rxjs/operators";
import { Observable, forkJoin, noop } from "rxjs";

import {
    // ADDRESS_TYPE,
    // LOCAL_DB_DATA_KEYS,
    // ADDRESS_FROM_PARAM,
    GoogleMaps,
} from "@constants";

import {
    User,
    Address,
    Sku,
    VacationApiRes,
    ScheduleApiData,
    SkuAttributes,
} from "@models";

import { ApiService } from "@services/data/api.service";
import { StoreService } from "@services/data/store.service";
import { CalendarService } from "@services/date/calendar.service";
// import { ErrorService } from "@services/integration/error.service";
import { SegmentService } from "@services/integration/segment.service";
import { PaymentService } from "@services/integration/payment.service";
import { FreshChatService } from "@services/integration/freshchat.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";
// import { FirebaseService } from "@services/integration/firebase.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { MoengageService } from "@services/integration/moengage.service";
import { PlatformService } from "@services/util/platform.service";
import { NativeDeeplinksService } from "@services/util/deeplinks.service";
// import { DbService } from "@services/data/db.service";
// import { NudgeService } from "@services/layout/nudge.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { AddressService } from "@services/shared/address.service";
import { RudderStackService } from "@services/integration/rudder-stack.service";
import { AudioService } from "@services/util/audio.service";

@Injectable({
    providedIn: "root",
})
export class InitializeService {
    constructor(
        private routerService: RouterService,
        private utilService: UtilService,
        private apiService: ApiService,
        private storeService: StoreService,
        private calendarService: CalendarService,
        private freshChatService: FreshChatService,
        private segmentService: SegmentService,
        // private errorService: ErrorService,
        private paymentService: PaymentService,
        // private firebaseService: FirebaseService,
        private moengageService: MoengageService,
        private appsFlyerService: AppsFlyerService,
        private platformService: PlatformService,
        // private dbService: DbService,
        // private nudgeService: NudgeService,
        private addressService: AddressService,
        private feedbackService: FeedbackService,
        private nativeDeeplinksService: NativeDeeplinksService,
        private rudderStackService: RudderStackService,
        private audioService: AudioService
    ) {}

    async initialize() {
        /* Native Deeplinks listener/handler */
        this.nativeDeeplinksService.handleNativeDeepLinks();

        /* Initialize device id */
        await this.platformService.setDeviceId();

        /* Initialize the MoEngage Service */
        this.moengageService.initialize();

        /* Initialize AppsFlyer */
        this.appsFlyerService.initialize();
    }

    async handleUserFlow(user?: User, address?: Address, isHome?: boolean) {
        // this.setUserContext(user);
        // this.onSessionInit(user);
        console.log(user, address, isHome);
        // if (!this.isSessionPresent(user)) {
        //     this.routeToLoginPage();
        // } else if (!this.isProfileSetup(user)) {
        //     this.moengageService.trackSignup();
        //     this.routeToProfileSetupPage();
        // } else if (!this.isAddressSetup(address)) {
        //     this.routeToCitySelectionPage();
        // } else if (await this.isAddressTypeSkip(address)) {
        //     this.routerToAddressPage();
        // } else if (!isHome) {
        this.routeToHomePage();
        // }
    }

    delayedInitialize() {
        this.utilService.loadDynamicScripts(
            GoogleMaps.id,
            GoogleMaps.url,
            noop
        );
        /* Initialize Segment */
        this.segmentService.initialize();

        /* Initialize RudderStack */
        this.rudderStackService.initialize();

        /* Initialize Feedback service */
        this.feedbackService.initialize();

        /* Initialize address service */
        this.addressService.initialize();

        setTimeout(() => {
            this.postLoadinitialize();
        }, 2000);
    }

    getLaunchData(isAnonymousUser = false): Observable<any> {
        const observablesInput = isAnonymousUser
            ? [this.fetchSkuList(), this.fetchSkuAttributes()]
            : [
                  this.fetchVacation(),
                  this.fetchSkuList(),
                  this.fetchSchedule(),
                  this.fetchSkuAttributes(),
              ];
        return forkJoin(observablesInput);
    }

    private postLoadinitialize() {
        /* Preload app audios */
        this.audioService.initialize();

        /* Initialize Razorpay for web only */
        this.paymentService.initialize();

        /* Initialize FreshChat */
        this.freshChatService.initialize();
    }

    // private setUserContext(user: User) {
    //     if (!this.isSessionPresent(user)) {
    //         return;
    //     }

    //     this.segmentService.setUser(user);
    //     this.rudderStackService.setUser(user);
    //     this.errorService.setContext(user);
    //     // this.smartlookService.setUserContext(user.id);
    //     this.firebaseService.setUserContext(user.id);
    //     this.moengageService.setUserContext(user);
    //     this.appsFlyerService.setAppUserId(user.id);
    //     this.checkTpEnabled(user.tPlusOneEnabled);
    // }

    // private checkTpEnabled(isEnabled: boolean) {
    //     this.calendarService.setTpEnabled(isEnabled);
    // }

    // private onSessionInit(user: User) {
    //     if (!this.isSessionPresent(user)) {
    //         return;
    //     }

    //     this.nudgeService.initialize(user.id);
    // }

    private fetchSkuList(): Observable<void> {
        return this.apiService.fetchSkuList().pipe(
            map((skuList: Sku[]) => {
                this.storeService.updateSkuStore(skuList);
            })
        );
    }

    private fetchSkuAttributes() {
        return this.apiService.fetchSkuAttributes().pipe(
            map((skuAttributes: SkuAttributes) => {
                this.storeService.updateSkuAttributesStore(skuAttributes);
            })
        );
    }

    private fetchVacation(): Observable<void> {
        return this.apiService.fetchVacation().pipe(
            map((res: VacationApiRes) => {
                const validatedVacation = this.calendarService.validateVacation(
                    res
                );

                return this.storeService.updateVacationStore(validatedVacation);
            })
        );
    }

    private fetchSchedule(): Observable<void> {
        return this.apiService.fetchSchedule().pipe(
            map((scheduleApiData: ScheduleApiData) => {
                this.storeService.updateSchedule(scheduleApiData);
            })
        );
    }

    // private isSessionPresent(user: User): boolean {
    //     return !this.utilService.isEmpty(user) && user.hasOwnProperty("id");
    // }

    // private isProfileSetup(user: User): boolean {
    //     const isSetup =
    //         !this.utilService.isEmpty(user.firstName) &&
    //         !this.utilService.isEmpty(user.email);

    //     return isSetup;
    // }

    // private isAddressSetup(address: Address): boolean {
    //     return (
    //         !this.utilService.isEmpty(address) && address.hasOwnProperty("id")
    //     );
    // }

    // private async isAddressTypeSkip(address: Address): Promise<boolean> {
    //     const skipImpressionFlag = await this.dbService.getData(
    //         LOCAL_DB_DATA_KEYS.ADDRESS_SKIP_IMPRESSION_V1
    //     );

    //     return address.type === ADDRESS_TYPE.SKIP && !skipImpressionFlag;
    // }

    // private routeToLoginPage() {
    //     this.routerService.goToLoginPage();
    // }

    // private routeToProfileSetupPage() {
    //     this.routerService.goToRegisterPage();
    // }

    // private routeToCitySelectionPage() {
    //     this.routerService.goToCitySelectionPage();
    // }

    // private routerToAddressPage() {
    //     this.routerService.goToAddressMapPage({
    //         queryParams: {
    //             from: ADDRESS_FROM_PARAM.CITY_SELECTION,
    //         },
    //     });
    // }

    private routeToHomePage() {
        this.routerService.goToHomePage();
    }
}
