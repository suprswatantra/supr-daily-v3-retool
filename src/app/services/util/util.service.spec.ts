import { TestBed } from "@angular/core/testing";

import { PlatformService } from "@services/util/platform.service";
import { UtilService } from "@services/util/util.service";

import {
    BaseUnitTestImportsIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class PlatformServiceMock {}

describe("UtilService", () => {
    let utilService: UtilService;

    let platformService: PlatformService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsIonic],

            providers: [
                { provide: PlatformService, useClass: PlatformServiceMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        platformService = TestBed.get(PlatformService);

        utilService = new UtilService(platformService);
    });

    it("#should create util service instance", () => {
        expect(utilService).toBeTruthy();
    });

    it("#should return time in correct format", () => {
        expect(utilService.timeConvert("16:00:00")).toBe("4:00PM");

        expect(utilService.timeConvert("12:00:00")).toBe("12:00PM");

        expect(utilService.timeConvert("00:00:00")).toBe("12:00AM");

        expect(utilService.timeConvert("04:00:00")).toBe("4:00AM");

        expect(utilService.timeConvert("11:00:00")).toBe("11:00AM");

        expect(utilService.timeConvert("09:00:00")).toBe("9:00AM");

        expect(utilService.timeConvert("22:00:00")).toBe("10:00PM");
    });

    it("#should return trimmed contact", () => {
        expect(utilService.getTrimmedPhoneNumber("+91 88718 15950")).toBe(
            "+918871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("+91 (88718) 15950")).toBe(
            "+918871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("+91 (887)18 15950")).toBe(
            "+918871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("+91-88718-15950")).toBe(
            "+918871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("(+91)-(887)18-15950")).toBe(
            "+918871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("0 88718 15950")).toBe(
            "08871815950"
        );

        expect(utilService.getTrimmedPhoneNumber("(088)(718) 15950")).toBe(
            "08871815950"
        );
    });

    it("#should create sort array based on object key", () => {
        const arrToTest1 = [
            { name: "Nishant" },
            { name: "Nishant Gaurav" },
            { name: "Swatantra" },
            { name: "Swatantra Mishra" },
            { name: "Manish" },
            { name: "Manish Mishra" },
            { name: "Ritesh" },
            { name: "Ritesh Jena" },
            { name: "Devang" },
            { name: "Devang Paliwal" },
            { name: "Salman" },
            { name: "Salman Shariff" },
            { name: "Saumitra" },
            { name: "Saumitra Khanwalker" },
        ];

        const correctResult1 = [
            { name: "Devang" },
            { name: "Devang Paliwal" },
            { name: "Manish" },
            { name: "Manish Mishra" },
            { name: "Nishant" },
            { name: "Nishant Gaurav" },
            { name: "Ritesh" },
            { name: "Ritesh Jena" },
            { name: "Salman" },
            { name: "Salman Shariff" },
            { name: "Saumitra" },
            { name: "Saumitra Khanwalker" },
            { name: "Swatantra" },
            { name: "Swatantra Mishra" },
        ];

        const correctResult2 = [
            { name: "Swatantra Mishra" },
            { name: "Swatantra" },
            { name: "Saumitra Khanwalker" },
            { name: "Saumitra" },
            { name: "Salman Shariff" },
            { name: "Salman" },
            { name: "Ritesh Jena" },
            { name: "Ritesh" },
            { name: "Nishant Gaurav" },
            { name: "Nishant" },
            { name: "Manish Mishra" },
            { name: "Manish" },
            { name: "Devang Paliwal" },
            { name: "Devang" },
        ];

        expect(arrToTest1.sort(utilService.dynamicSort("name"))).toEqual(
            correctResult1
        );

        expect(arrToTest1.sort(utilService.dynamicSort("-name"))).toEqual(
            correctResult2
        );
    });
});
