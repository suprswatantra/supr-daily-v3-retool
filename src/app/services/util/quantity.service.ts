import { Injectable } from "@angular/core";

import { UtilService } from "./util.service";

import { Sku, TransactionSku } from "@models";
import { SKU_UNITS } from "@constants";

@Injectable({
    providedIn: "root",
})
export class QuantityService {
    constructor(private utilService: UtilService) {}

    toText(sku: Sku, scheduleQty?: number, deliveries?: number): string {
        if (!sku || !sku.item) {
            return "";
        }

        const quantity = isNaN(scheduleQty)
            ? sku.item.unit_quantity
            : scheduleQty;

        const unitName = this.getUnitName(sku.item.unit.name, quantity);

        /* [TODO] Handle Plurals */
        if (deliveries && deliveries > 1) {
            return `${deliveries} X ${quantity} ${unitName}`;
        }
        return `${quantity} ${unitName}`;
    }

    toTextTxn(sku: TransactionSku, deliveries?: number | string): string {
        if (!sku || !sku.item) {
            return "";
        }

        /* Init */
        let unit: string, _conversion: number, _deliveries: string;

        const quantity = this.utilService.toValue(deliveries);
        _conversion = this.getUnitConversion(sku);

        /* Sub Unit vs Unit */
        if (this.isFractionalQty(quantity, _conversion)) {
            unit = sku.item.sub_unit_name;
            _deliveries = `${quantity}`;
        } else {
            /* To avoid (s) at end of singular quantities */
            if (this.isSingularQty(quantity, _conversion)) {
                unit = sku.item.sub_unit_name;
            } else {
                unit = sku.item.unit_name;
                _conversion = 1;
            }

            /* Quantity */
            _deliveries = this.getQtyByRemovingExtraZeros(quantity);
        }

        /* Return text */
        return `${_deliveries}x ${_conversion}${unit}`;
    }

    private getUnitConversion(sku: TransactionSku): number {
        /* [TODO_RITESH] Check if we should send a default value */
        const conversion = this.utilService.getNestedValue(
            sku,
            "item.sub_unit_conversion"
        );

        if (isNaN(conversion)) {
            return;
        }

        return Math.round(conversion);
    }

    private isFractionalQty(qty: number, conversion: number): boolean {
        return qty < 1 && qty > 0 && conversion !== 1;
    }

    private isSingularQty(qty: number, conversion: number): boolean {
        return qty === 1 && conversion === 1;
    }

    private getQtyByRemovingExtraZeros(quantity: number): string {
        let qty = quantity.toFixed(2);

        /* Remove extra zeroes */
        const qtyFraction = qty.split(".")[1];
        if (qtyFraction === "00") {
            qty = quantity.toFixed(0);
        } else if (qtyFraction.charAt(1) === "0") {
            qty = quantity.toFixed(1);
        }

        return qty;
    }

    private getUnitName(skuUnitName: string, quantity: number) {
        let unit: string;

        if (quantity <= 1) {
            unit = this.utilService.getNestedValue(
                SKU_UNITS,
                `${skuUnitName}.singular`,
                "unit"
            );
        } else {
            unit = this.utilService.getNestedValue(
                SKU_UNITS,
                `${skuUnitName}.plural`,
                "units"
            );
        }

        return this.utilService.toTitleCase(unit);
    }
}
