import { Injectable, ElementRef } from "@angular/core";
import { noop } from "rxjs";

import { SwiperOptions } from "swiper";

import * as uuid from "uuid";

import { PlatformService } from "@services/util/platform.service";

import { SuprMaps, Color, Frequency, UrlData } from "@types";
import {
    SLIDER_OPTIONS,
    DAY_LETTERS,
    WEEK_DAYS_SELECTION_FORMAT,
    WEEKDAY_LETTERS,
    WEEK_END_LETTERS,
    DOM_SCROLL_ELEM,
    APP_IMAGE_ASSETS_PATH,
    GoogleMapsStyles,
} from "@constants";

import { Subscription } from "@models";

export interface Coordinates {
    x: number;
    y: number;
}

@Injectable({
    providedIn: "root",
})
export class UtilService {
    constructor(private platformService: PlatformService) {}

    // ======================
    // Public methods
    // ======================

    getNestedValue(object: any, propertyName: string, defaultValue?: any) {
        const value = propertyName.split(".").reduce(this.getValue, object);
        return value !== undefined ? value : defaultValue;
    }

    chunkArray(arr: Array<any>, n: number): Array<any> {
        return arr.reduce((all, cur, i) => {
            const ch = Math.floor(i / n);
            all[ch] = [].concat(all[ch] || [], cur);
            return all;
        }, []);
    }

    isValidEmail(email: string): boolean {
        const pattern = /^[a-z0-9._'%+-]{1,64}@(?:[a-z0-9-]{1,63}\.){1,125}[a-z]{2,63}$/;

        return pattern.test(email.toLowerCase());
    }

    isValidName(name: string): boolean {
        const pattern = /^[a-z 0-9_ ,.'-]+$/i;
        return pattern.test(name.toUpperCase());
    }

    getValue(object: any, propertyName: string): any {
        if (!propertyName) {
            throw new Error("Impossible to set null property");
        }
        return object === null || typeof object === "undefined"
            ? undefined
            : object[propertyName];
    }

    capitalizeFirstLetter(string = ""): string {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    capitalizeOnlyFirstLetter(string = ""): string {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    isNumber(obj: any) {
        return obj !== undefined && typeof obj === "number" && !isNaN(obj);
    }

    isEmpty(value: any): boolean {
        switch (typeof value) {
            case "string":
                return value.length === 0;
            case "number":
            case "boolean":
                return !value;
            case "undefined":
                return true;
            case "object":
                return value === null || Object.keys(value).length === 0
                    ? true
                    : false; // handling for null.
            case "function":
                return false;
            default:
                return !value ? true : false;
        }
    }

    toValue(num: string | number, decimals = 2): number {
        if (num === undefined || num === null) {
            return NaN;
        }

        const value = typeof num === "string" ? Number(num) : num;
        return this.round(value, decimals);
    }

    getSwiperOptions(sliderName: string, initialSlide = 1): SwiperOptions {
        const simulateTouch = !this.platformService.isCordova();
        if (sliderName === "month") {
            return {
                ...SLIDER_OPTIONS,
                simulateTouch,
                initialSlide,
            };
        }

        return SLIDER_OPTIONS;
    }

    executeWhenIdle(callback: () => void, timeout = 0) {
        if (
            typeof window !== "undefined" &&
            (<any>window).requestIdleCallback !== undefined
        ) {
            return (<any>window).requestIdleCallback(callback, { timeout });
        } else {
            return setTimeout(callback, timeout);
        }
    }

    getUrlData(url: string): UrlData {
        const parser = document.createElement("a");
        parser.href = url;

        const queryString = parser.search.substring(1);
        const queryParamsCollection =
            queryString.length > 1 && queryString.split("&");
        const queryParams = {};

        if (Array.isArray(queryParamsCollection)) {
            for (let i = 0; i < queryParamsCollection.length; i++) {
                const pair = queryParamsCollection[i].split("=");
                queryParams[pair[0]] = decodeURIComponent(pair[1]);
            }
        }

        return {
            path: parser.pathname,
            queryParams,
        };
    }

    compareLocations(
        locationA: SuprMaps.Location,
        locationB: SuprMaps.Location,
        precision: number
    ) {
        return (
            locationA.lat.toFixed(precision) ===
                locationB.lat.toFixed(precision) &&
            locationA.lng.toFixed(precision) ===
                locationB.lng.toFixed(precision)
        );
    }

    round(value: number, decimals = 0) {
        const roundedValue = Math.round(Number(`${value}e${decimals}`));
        return Number(`${roundedValue}e-${decimals}`);
    }

    perfNow(): number {
        if (
            window &&
            window.performance !== undefined &&
            window.performance.now !== undefined
        ) {
            return performance.now();
        }

        return Date.now();
    }

    createPerformanceMark(name: string) {
        if (
            window &&
            window.performance !== undefined &&
            window.performance.mark !== undefined
        ) {
            window.performance.mark(name);
        }
    }

    hexToHSL(hex: string): Color.HSL {
        // Convert hex to RGB first
        let r = 0,
            g = 0,
            b = 0;

        if (hex.length === 4) {
            r = parseInt(hex[1] + hex[1], 16);
            g = parseInt(hex[2] + hex[2], 16);
            b = parseInt(hex[3] + hex[3], 16);
        } else if (hex.length === 7) {
            r = parseInt(hex[1] + hex[2], 16);
            g = parseInt(hex[3] + hex[4], 16);
            b = parseInt(hex[5] + hex[6], 16);
        }

        // Then to HSL
        r /= 255;
        g /= 255;
        b /= 255;

        const cmin = Math.min(r, g, b),
            cmax = Math.max(r, g, b),
            delta = cmax - cmin;

        let h = 0,
            s = 0,
            l = 0;

        if (delta === 0) {
            h = 0;
        } else if (cmax === r) {
            h = ((g - b) / delta) % 6;
        } else if (cmax === g) {
            h = (b - r) / delta + 2;
        } else {
            h = (r - g) / delta + 4;
        }

        h = Math.round(h * 60);

        if (h < 0) {
            h += 360;
        }

        l = (cmax + cmin) / 2;
        s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
        s = +(s * 100).toFixed(1);
        l = +(l * 100).toFixed(1);

        return { h, s, l };
    }

    getWeekDaysFormatFromDays(selectedDays: Frequency): string {
        if (!selectedDays) {
            return;
        }

        const days = DAY_LETTERS;
        const weekDays = WEEKDAY_LETTERS;
        const weekends = WEEK_END_LETTERS;

        let selectedDaysCount = 0;
        for (const day of days) {
            if (selectedDays[day]) {
                selectedDaysCount++;
            }
        }

        if (selectedDaysCount === 7) {
            return WEEK_DAYS_SELECTION_FORMAT.DAILY;
        }

        let count = 0;
        for (const day of weekDays) {
            if (selectedDays[day]) {
                count++;
            }
        }

        if (count === weekDays.length && count === selectedDaysCount) {
            return WEEK_DAYS_SELECTION_FORMAT.WEEK_DAYS;
        }

        count = 0;
        for (const day of weekends) {
            if (selectedDays[day]) {
                count++;
            }
        }

        if (count === weekends.length && count === selectedDaysCount) {
            return WEEK_DAYS_SELECTION_FORMAT.WEEK_ENDS;
        }

        return WEEK_DAYS_SELECTION_FORMAT.CUSTOM;
    }

    getSelectedDaysFromSubscription(subscription: Subscription): Frequency {
        if (!subscription) {
            return;
        }

        const days = DAY_LETTERS;
        const selectedDays = {};

        for (const day of days) {
            if (subscription[day]) {
                selectedDays[day] = true;
            }
        }

        return selectedDays;
    }

    debounce(delay: number, callback: Function) {
        let timeout = null;
        const debounced = function () {
            const context = this;
            const args = arguments;

            //
            // if a timeout has been registered before then
            // cancel it so that we can setup a fresh timeout
            //
            if (timeout !== null) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function () {
                callback.apply(context, args);
                timeout = null;
            }, delay);
        };

        debounced.cancel = function () {
            clearTimeout(timeout);
            timeout = null;
        };

        return debounced;
    }

    uuid(): string {
        return uuid.v4();
    }

    getQueryParams(object: any): string {
        if (this.isEmpty(object)) {
            return "";
        }

        return Object.keys(object)
            .map((key) => {
                return (
                    encodeURIComponent(key) +
                    "=" +
                    encodeURIComponent(object[key])
                );
            })
            .join("&");
    }

    toTitleCase(input: string): string {
        const words = input.toLowerCase().split(" ");
        const final = [];

        for (const word of words) {
            final.push(word.charAt(0).toUpperCase() + word.slice(1));
        }

        return final.join(" ");
    }

    isLengthyArray(item): boolean {
        return Array.isArray(item) && item.length > 0;
    }

    setStyles(data: any, styleList: any[], wrapperEl: ElementRef) {
        styleList.forEach((styleAttribute) => {
            const attributeValue = this.getStyleAttributeValue(
                data,
                styleAttribute.attributeName
            );

            if (attributeValue) {
                this.setStyleAttribute(
                    wrapperEl,
                    styleAttribute.attributeStyleVariableName,
                    attributeValue
                );
            }
        });

        if (data && data.iconType === "svg" && data.iconName) {
            wrapperEl.nativeElement.style.setProperty(
                "--supr-configurable-svg-path",
                `url('${APP_IMAGE_ASSETS_PATH}${data.iconName}.svg')`
            );
        }
    }

    scrollToElementId(elemId: string, isSmooth = true) {
        if (!elemId) {
            return;
        }

        const elem = document.getElementById(elemId);
        if (!elem) {
            return;
        }
        let config: any = {
            block: "center",
        };
        if (isSmooth) {
            Object.assign(config, {
                behavior: "smooth",
            });
        }

        elem.scrollIntoView(config);
    }

    isElementInViewport(el: any): boolean {
        if (!el) {
            return false;
        }

        const rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <=
                (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <=
                (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    setStyleAttribute(
        wrapperElementReference: ElementRef,
        styleVariableName: string,
        styleVariableValue: string
    ) {
        if (styleVariableName && styleVariableValue) {
            wrapperElementReference.nativeElement.style.setProperty(
                styleVariableName,
                styleVariableValue
            );
        }
    }

    private getStyleAttributeValue(
        attributeMap: any,
        attributeName: string
    ): string {
        if (attributeName) {
            return this.getNestedValue(attributeMap, attributeName, null);
        }

        return null;
    }

    scrollToPageSection(
        elemId: string,
        pageScrollContent = "homeScrollContent",
        isHeaderAvailable = true
    ) {
        setTimeout(() => {
            if (elemId) {
                const wrapper = document.getElementById(`${elemId}`);

                if (wrapper) {
                    let offset = 0;
                    if (isHeaderAvailable) {
                        const headerHeight = this.getHeaderHeight();
                        offset = wrapper.offsetTop - Number(headerHeight) - 80;
                    } else {
                        offset = wrapper.offsetTop;
                    }

                    const scrollWrapper = document.getElementById(
                        pageScrollContent
                    );

                    if (scrollWrapper) {
                        scrollWrapper.scrollTo({
                            top: offset,
                            behavior: "smooth",
                        });
                    }
                }
            }
        }, 100);
    }

    scrollTo(navigateId: number, from: string, skuId = null) {
        setTimeout(() => {
            if (navigateId) {
                const wrapper = document.getElementById(
                    `${DOM_SCROLL_ELEM[from]}${navigateId}`
                );

                if (wrapper) {
                    const headerHeight = this.getHeaderHeight();
                    let offset = wrapper.offsetTop - Number(headerHeight);

                    if (skuId) {
                        const containerHeight = Math.floor(
                            this.getSuprContainerHeight() / 2
                        );
                        offset = offset - containerHeight + 100;
                    }

                    const scrollWrapper = document.getElementById(
                        "categoryScrollContent"
                    );

                    if (scrollWrapper) {
                        scrollWrapper.scrollTop = offset;
                    }
                }
            }
        }, 100);
    }

    /**
     * Converts 24 hour string to 12 hour format
     * "16:00:00" --> 4:00PM, Accurate till minutes, trimming the seconds
     *
     * @param {string} time
     * @returns {string}
     * @memberof BlockAccessService
     */
    timeConvert(time: string): string {
        if (!time || time.length !== 8) {
            return "";
        }

        const timeWithoutMilliSeconds = time.substr(0, 5);
        // Check correct time format and split into components
        let timeArray: any[] = timeWithoutMilliSeconds
            .toString()
            .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (timeArray.length > 1) {
            // If time format correct
            timeArray = timeArray.slice(1); // Remove full string match value
            timeArray[5] = +timeArray[0] < 12 ? "AM" : "PM"; // Set AM/PM
            timeArray[0] = +timeArray[0] % 12 || 12; // Adjust hours
        }
        return timeArray.join(""); // return adjusted time or original string
    }

    /**
     * Function to sort alphabetically an array of objects by some specific key.
     * USAGE: ArrayLikeStructure.sort(dynamicSort("name")), ArrayLikeStructure.sort(dynamicSort("-name")) --> descending
     *
     * @param {string} property Key of the object to sort.
     */
    dynamicSort(property: string): any {
        let sortOrder = 1;

        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }

        return (a, b) => {
            if (sortOrder === -1) {
                return b[property].localeCompare(a[property]);
            } else {
                return a[property].localeCompare(b[property]);
            }
        };
    }

    /**
     * Trimming to remove few unwanted chars from phone numbers
     *
     * @param {string} str
     * @returns {string}
     * @memberof ReferralService
     */
    getTrimmedPhoneNumber(phoneNumber: string): string {
        if (!phoneNumber) {
            return "";
        }

        return phoneNumber.replace(/[\s, (, ), -]+/g, "");
    }

    /**
     * Gets the distance b/w two coordinates, usied to clear the canvas till this offset
     *
     * @private
     * @param {Coordinates} point1
     * @param {Coordinates} point2
     * @returns {number}
     */
    distanceBetweenCoordinates(
        point1: Coordinates,
        point2: Coordinates
    ): number {
        return Math.sqrt(
            Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2)
        );
    }

    /**
     * Gets angle b/w two points in a plane.
     *
     * @private
     * @param {Coordinates} point1
     * @param {Coordinates} point2
     * @returns {number}
     */
    angleBetweenCoordinates(point1: Coordinates, point2: Coordinates): number {
        return Math.atan2(point2.x - point1.x, point2.y - point1.y);
    }

    private getHeaderHeight(): number {
        const computedStyle = getComputedStyle(document.documentElement);
        const property = computedStyle
            ? computedStyle.getPropertyValue("--supr-page-header-height")
            : null;

        if (property) {
            return Number(property.split("px")[0]);
        }

        return 0;
    }

    private getSuprContainerHeight(): number {
        const computedStyle = getComputedStyle(document.documentElement);
        const property = computedStyle ? computedStyle.height : null;

        if (property) {
            return Number(property.split("px")[0]);
        }

        return 0;
    }

    randomAlphaNumeric(digits: number = 8) {
        try {
            const rand = Math.random().toString(36).replace("0.", "");
            return rand.slice(0, digits);
        } catch (error) {
            return null;
        }
    }

    getDateFromTimestamp(timestamp: string) {
        try {
            const [date = ""] = timestamp.split(/[ T,\:]+/);
            return date;
        } catch (error) {
            return null;
        }
    }

    loadDynamicScripts(id: string, url: string, callback = noop) {
        const existingScript = document.getElementById(id);

        if (!existingScript) {
            this.loadDynamicStyles();
            const script = document.createElement("script");
            script.src = url;
            script.id = id;
            document.body.appendChild(script);

            script.onload = () => {
                if (callback) callback();
            };
        }

        if (existingScript && callback) callback();
    }

    loadDynamicStyles() {
        var style = document.createElement("style");
        style.innerHTML = GoogleMapsStyles;
        document.head.appendChild(style);
    }
}
