import { Injectable } from "@angular/core";

import { Router, NavigationStart, NavigationExtras } from "@angular/router";

import { filter } from "rxjs/operators";

import { Platform, MenuController, NavController } from "@ionic/angular";

import { ErrorService } from "@services/integration/error.service";
import { ModalService } from "@services/layout/modal.service";
import { CartService } from "@services/shared/cart.service";
import { PlatformService } from "@services/util/platform.service";

import { PAGE_ROUTES, EXIT_PATHS, PATH_TYPES } from "@constants";

@Injectable({
    providedIn: "root",
})
export class RouterService {
    private currentUrl: string;
    private previousUrl: string;
    private appCanExitOnBackButton: boolean;
    private ignoreBackButton: boolean;
    private lastRouteId: string;

    constructor(
        private router: Router,
        private navController: NavController,
        private platform: Platform,
        private modalService: ModalService,
        private menuController: MenuController,
        private platformService: PlatformService,
        private cartService: CartService,
        private errorService: ErrorService
    ) {
        this.initialize();
    }

    public setAppExitFlag(exit = true) {
        this.appCanExitOnBackButton = exit;
    }

    public setIgnoreBackButton(ignore = true) {
        this.ignoreBackButton = ignore;
    }

    public getPreviousUrl() {
        return this.previousUrl;
    }

    public getCurrentUrl() {
        return this.currentUrl;
    }

    public goToUrl(url, extras: NavigationExtras = {}) {
        const queryParamStartIndex = url.indexOf("?");
        const queryParams = extras.queryParams || {};
        let baseUrl = url;

        /* Angular router encodes query params if we use it directly in url.
         As such we need to separate base path and create query params from the other relevant part of url */
        if (queryParamStartIndex > -1) {
            baseUrl = url.slice(0, queryParamStartIndex);
            try {
                const queryParamsString = url.slice(
                    queryParamStartIndex,
                    url.length
                );
                const query: any = new URLSearchParams(queryParamsString);
                for (const [key, value] of query) {
                    queryParams[key] = value;
                }
            } catch (err) {
                this.errorService.logSentryError(err, {
                    context: { url },
                    message: "Query params generation error",
                });
            }
        }

        this.router.navigate([baseUrl], { ...extras, queryParams });
    }

    public openExternalUrl(url) {
        window.open(url, "_system", "location=yes");
    }

    public navigateUrlByType(
        url: string,
        type: string,
        extras?: NavigationExtras
    ) {
        if (type === PATH_TYPES.EXTERNAL) {
            this.openExternalUrl(url);
        } else if (type === PATH_TYPES.INTERNAL) {
            this.goToUrl(url, extras);
        }
    }

    public goToLoginPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.LOGIN.PATH], extras);
    }

    public goToPasswordPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.PASSWORD.PATH], extras);
    }

    public goToCitySelectionPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.CITY_SELECTION.PATH], extras);
    }

    public goToHomePage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.HOME.PATH], extras);
    }

    public goToMileStonePage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.MILESTONE.PATH], extras);
    }

    public goToMileStoneRewardPage(extras: NavigationExtras = {}) {
        const milestoneFaqPath = PAGE_ROUTES.MILESTONE.PATH;
        const rewardsPath = PAGE_ROUTES.MILESTONE.CHILDREN.REWARDS.PATH;

        this.router.navigate([`${milestoneFaqPath}/${rewardsPath}`], extras);
    }

    public goToCategoryPage(categoryId: number, extras: NavigationExtras = {}) {
        const path = PAGE_ROUTES.CATEGORY.PATH.replace(
            ":categoryId",
            categoryId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToCategoryEssentialsPage(
        categoryId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.CATEGORY_ESSENTIALS.PATH.replace(
            ":categoryId",
            categoryId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSubCategoryPage(
        categoryId: number,
        filterId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.SUB_CATEGORY.PATH.replace(
            ":categoryId",
            categoryId.toString()
        ).replace(":filterId", filterId.toString());

        this.router.navigate([path], extras);
    }

    public goToCollectionPage(viewId: number, extras: NavigationExtras = {}) {
        const path = PAGE_ROUTES.COLLECTION.PATH.replace(
            ":viewId",
            viewId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSubscriptionCreatePage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SUBSCRIPTION_CREATE.PATH], extras);
    }

    public goToSubscriptionListPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SUBSCRIPTION_LIST_V2.PATH], extras);
    }

    public goToSubscriptionDetailsPage(
        subscriptionId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.SUBSCRIPTION_DETAILS_V2.PATH.replace(
            ":subscriptionId",
            subscriptionId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSubscriptionSummaryPage(
        subscriptionId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.SUBSCRIPTION_SUMMARY.PATH.replace(
            ":subscriptionId",
            subscriptionId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSubscriptionRechargePage(
        subscriptionId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.SUBSCRIPTION_RECHARGE.PATH.replace(
            ":subscriptionId",
            subscriptionId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSubscriptionSchedulePage(
        subscriptionId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.SUBSCRIPTION_SCHEDULE.PATH.replace(
            ":subscriptionId",
            subscriptionId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToWalletPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.WALLET.PATH], extras);
    }

    public goToWalletTransactionsPage(extras: NavigationExtras = {}) {
        const walletPath = PAGE_ROUTES.WALLET.PATH;
        const txnPath = PAGE_ROUTES.WALLET.CHILDREN.TRANSACTIONS.PATH;

        this.router.navigate([`${walletPath}/${txnPath}`], extras);
    }

    public goToWalletTransactionsV2Page(extras: NavigationExtras = {}) {
        const walletPath = PAGE_ROUTES.WALLET.PATH;
        const txnPath = PAGE_ROUTES.WALLET.CHILDREN.TRANSACTIONS_V2.PATH;

        this.router.navigate([`${walletPath}/${txnPath}`], extras);
    }

    public goToProfilePage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.PROFILE.PATH], extras);
    }

    public goToAddressPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.ADDRESS.PATH], extras);
    }

    public goToAddressFormPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const formPath = PAGE_ROUTES.ADDRESS.CHILDREN.FORM.PATH;

        this.router.navigate([`${addressPath}/${formPath}`], extras);
    }

    public goToAddressSearchPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const searchPath = PAGE_ROUTES.ADDRESS.CHILDREN.ADDRESS_SEARCH.PATH;

        this.router.navigate([`${addressPath}/${searchPath}`], extras);
    }

    public goToAddressMapPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const mapPath = PAGE_ROUTES.ADDRESS.CHILDREN.MAP.PATH;

        this.router.navigate([`${addressPath}/${mapPath}`], extras);
    }

    public goToAddressMapEditPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const mapPath = PAGE_ROUTES.ADDRESS.CHILDREN.MAP_EDIT.PATH;

        this.router.navigate([`${addressPath}/${mapPath}`], extras);
    }

    public goToAddressReviewPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS_DEP.PATH;
        const reviewPath = PAGE_ROUTES.ADDRESS_DEP.CHILDREN.REVIEW.PATH;

        this.router.navigate([`${addressPath}/${reviewPath}`], extras);
    }

    public goToSocietySearchPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const societySearchPath =
            PAGE_ROUTES.ADDRESS.CHILDREN.SOCIETY_SEARCH.PATH;

        this.router.navigate([`${addressPath}/${societySearchPath}`], extras);
    }

    public goToSocietyDetailSearchPage(extras: NavigationExtras = {}) {
        const addressPath = PAGE_ROUTES.ADDRESS.PATH;
        const premiseDetailsSearchPath =
            PAGE_ROUTES.ADDRESS.CHILDREN.SOCIETY_DETAILS_SEARCH.PATH;

        this.router.navigate(
            [`${addressPath}/${premiseDetailsSearchPath}`],
            extras
        );
    }

    public goToSearchPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SEARCH.PATH], extras);
    }

    public goToSupportPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SUPPORT.PATH], extras);
    }

    public gotoSupportPageRoot(extras: NavigationExtras = {}) {
        this.navController.navigateRoot([PAGE_ROUTES.SUPPORT.PATH], extras);
    }

    public goToComplaintsPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.COMPLAINTS.PATH], extras);
    }

    public goToComplaintsDetailsPage(
        extras: NavigationExtras = {},
        complaintId: number
    ) {
        const complaintPath = PAGE_ROUTES.COMPLAINTS.PATH;
        const detailsPath = PAGE_ROUTES.COMPLAINTS.CHILDREN.DETAILS.PATH;

        this.router.navigate(
            [`${complaintPath}/${detailsPath}/${complaintId}`],
            extras
        );
    }

    public goToFaqPage(extras: NavigationExtras = {}, id: number) {
        const supportPath = PAGE_ROUTES.SUPPORT.PATH;
        const faqPath = PAGE_ROUTES.SUPPORT.CHILDREN.FAQ.PATH;

        this.router.navigate([`${supportPath}/${faqPath}/${id}`], extras);
    }

    public goToSummaryPage(extras: NavigationExtras = {}) {
        const supportPath = PAGE_ROUTES.SUPPORT.PATH;
        const selfServePath = PAGE_ROUTES.SUPPORT.CHILDREN.SELF_SERVE.PATH;
        const summaryPath = PAGE_ROUTES.SUPPORT.CHILDREN.SUMMARY.PATH;

        this.router.navigate(
            [`${supportPath}/${selfServePath}/${summaryPath}`],
            extras
        );
    }

    public goToSelfServePage(extras: NavigationExtras = {}, date: string) {
        const supportPath = PAGE_ROUTES.SUPPORT.PATH;
        const selfServePath = PAGE_ROUTES.SUPPORT.CHILDREN.SELF_SERVE.PATH;

        this.router.navigate(
            [`${supportPath}/${selfServePath}/${date}`],
            extras
        );
    }

    public goToImageUploadPage(extras: NavigationExtras = {}) {
        const supportPath = PAGE_ROUTES.SUPPORT.PATH;
        const selfServePath = PAGE_ROUTES.SUPPORT.CHILDREN.SELF_SERVE.PATH;
        const imageUploadPath = PAGE_ROUTES.SUPPORT.CHILDREN.IMAGE_UPLOAD.PATH;

        this.router.navigate(
            [`${supportPath}/${selfServePath}/${imageUploadPath}`],
            extras
        );
    }

    public goToCartPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.CART.PATH], extras);
    }

    public goToCartOffersPage(extras: NavigationExtras = {}) {
        const cartPath = PAGE_ROUTES.CART.PATH;
        const offersPath = PAGE_ROUTES.CART.CHILDREN.OFFERS.PATH;
        this.router.navigate([`${cartPath}/${offersPath}`], extras);
    }

    public goToThankyouPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.THANKYOU.PATH], extras);
    }

    public goToWalletRechargeThankyouPage(extras: NavigationExtras = {}) {
        const walletPath = PAGE_ROUTES.WALLET.PATH;
        const thankyouPath = PAGE_ROUTES.WALLET.CHILDREN.THANK_YOU.PATH;

        this.router.navigate([`${walletPath}/${thankyouPath}`], extras);
    }

    public goToWalletOffersPage(amount: number, extras: NavigationExtras = {}) {
        const walletPath = PAGE_ROUTES.WALLET.PATH;
        const offersPath = PAGE_ROUTES.WALLET.CHILDREN.OFFERS.PATH.replace(
            ":amount",
            amount.toString()
        );

        this.router.navigate([`${walletPath}/${offersPath}`], extras);
    }

    public goToSchedulePage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SCHEDULE.PATH], extras);
    }

    public goToRegisterPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.REGISTER.PATH], extras);
    }

    public goToSuperCreditsOnboardingPage(extras: NavigationExtras = {}) {
        this.router.navigate(
            [PAGE_ROUTES.SUPER_CREDITS_ONBOARDING.PATH],
            extras
        );
    }

    public goToOOSAlternatesPage(skuId: number, extras: NavigationExtras = {}) {
        const path = PAGE_ROUTES.OOS_ALTERNATES.PATH.replace(
            ":skuId",
            skuId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToSuprPassPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.SUPR_PASS.PATH], extras);
    }

    public goToSuprPassFaqPage(extras: NavigationExtras = {}) {
        const suprPassPath = PAGE_ROUTES.SUPR_PASS.PATH;
        const faqPath = PAGE_ROUTES.SUPR_PASS.CHILDREN.FAQ.PATH;

        this.router.navigate([`${suprPassPath}/${faqPath}`], extras);
    }

    public goToSuprPassActivityPage(extras: NavigationExtras = {}) {
        const suprPassPath = PAGE_ROUTES.SUPR_PASS.PATH;
        const activityPath = PAGE_ROUTES.SUPR_PASS.CHILDREN.ACTIVITY.PATH;

        this.router.navigate([`${suprPassPath}/${activityPath}`], extras);
    }

    public goToSuprPassSavingsPage(extras: NavigationExtras = {}) {
        const suprPassPath = PAGE_ROUTES.SUPR_PASS.PATH;
        const savingsPath = PAGE_ROUTES.SUPR_PASS.CHILDREN.SAVINGS.PATH;

        this.router.navigate([`${suprPassPath}/${savingsPath}`], extras);
    }

    public goToReferralPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.REFERRAL.PATH], extras);
    }

    public goToReferralFaqPage(extras: NavigationExtras = {}) {
        const referralPath = PAGE_ROUTES.REFERRAL.PATH;
        const faqPath = PAGE_ROUTES.REFERRAL.CHILDREN.FAQ.PATH;

        this.router.navigate([`${referralPath}/${faqPath}`], extras);
    }

    public goToReferralRewardsPage(extras: NavigationExtras = {}) {
        const referralPath = PAGE_ROUTES.REFERRAL.PATH;
        const rewardsPath = PAGE_ROUTES.REFERRAL.CHILDREN.REWARDS.PATH;

        this.router.navigate([`${referralPath}/${rewardsPath}`], extras);
    }

    public goToReferralInvitePage(extras: NavigationExtras = {}) {
        const referralPath = PAGE_ROUTES.REFERRAL.PATH;
        const invitePath = PAGE_ROUTES.REFERRAL.CHILDREN.INVITE.PATH;

        this.router.navigate([`${referralPath}/${invitePath}`], extras);
    }

    public goToRewardsPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.REWARDS.PATH], extras);
    }

    public goToRewardsFaqPage(extras: NavigationExtras = {}) {
        const rewardsPath = PAGE_ROUTES.REWARDS.PATH;
        const faqPath = PAGE_ROUTES.REWARDS.CHILDREN.FAQ.PATH;

        this.router.navigate([`${rewardsPath}/${faqPath}`], extras);
    }

    public goToRewardsActivityPage(extras: NavigationExtras = {}) {
        const rewardsPath = PAGE_ROUTES.REWARDS.PATH;
        const activityPath = PAGE_ROUTES.REWARDS.CHILDREN.ACTIVITY.PATH;

        this.router.navigate([`${rewardsPath}/${activityPath}`], extras);
    }

    public goToRewardsScratchCardsPage(extras: NavigationExtras = {}) {
        const rewardsPath = PAGE_ROUTES.REWARDS.PATH;
        const scratchCardsPath =
            PAGE_ROUTES.REWARDS.CHILDREN.SCRATCH_CARDS.PATH;

        this.router.navigate([`${rewardsPath}/${scratchCardsPath}`], extras);
    }

    public goToRewardsScratchCardDetailsPage(
        scratchCardId: number,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.REWARDS.CHILDREN.SCRATCH_CARD_DETAILS.PATH.replace(
            ":scratchCardId",
            scratchCardId.toString()
        );

        this.router.navigate([path], extras);
    }

    public goToVacationPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.VACATION.PATH], extras);
    }

    goToDeliveryProofPage(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.DELIVERY_PROOF.PATH], extras);
    }

    goToDeliveryProofV2Page(extras: NavigationExtras = {}) {
        this.router.navigate([PAGE_ROUTES.DELIVERY_PROOF_V2.PATH], extras);
    }

    goToDeliveryProofReportPage(extras: NavigationExtras = {}) {
        const deliveryProofsPath = PAGE_ROUTES.DELIVERY_PROOF_V2.PATH;

        this.router.navigate(
            [
                `${deliveryProofsPath}/${PAGE_ROUTES.DELIVERY_PROOF_V2.CHILDREN.REPORT.PATH}`,
            ],
            extras
        );
    }

    public isAlternatesScreen(): boolean {
        const currentUrl = this.getCurrentUrl();
        const alternatesPath = PAGE_ROUTES.OOS_ALTERNATES.BASE;
        return currentUrl && currentUrl.indexOf(alternatesPath) > -1;
    }

    public isHomeScreen(): boolean {
        const currentUrl = this.getCurrentUrl();
        const homePath = PAGE_ROUTES.HOME.PATH;
        return currentUrl && currentUrl.indexOf(homePath) > -1;
    }

    public goBack(routeId?: string) {
        if (routeId && this.lastRouteId && this.lastRouteId === routeId) {
            return;
        }

        this.lastRouteId = routeId;

        if (this.isScheduleThankyouFlow()) {
            this.goToHomePage();
            return;
        }

        this.navController.pop();
    }

    public goToOfferDetailsPage(
        couponCode: string,
        extras: NavigationExtras = {}
    ) {
        const path = PAGE_ROUTES.OFFER_DETAILS.PATH.replace(
            ":offerName",
            couponCode
        );

        this.router.navigate([path], extras);
    }

    private initialize() {
        this.platform.ready().then(() => {
            this.registerBackButtonListener();
            this.subscribeForRouterEvents();
        });
    }

    private subscribeForRouterEvents() {
        this.currentUrl = this.router.url;

        this.router.events
            .pipe(filter((event) => event instanceof NavigationStart))
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    /* Modify the current & previous urls */
                    this.previousUrl = this.currentUrl;
                    this.currentUrl = event.url;
                }
            });
    }

    private registerBackButtonListener() {
        this.platform.backButton.subscribeWithPriority(1000, () =>
            this.handleBackButtonClick()
        );
    }

    private async handleBackButtonClick() {
        try {
            // Force not to handle
            if (this.ignoreBackButton) {
                return;
            }

            // Sidemenu check
            const isSideMenuOpen = await this.menuController.isOpen();
            if (isSideMenuOpen) {
                this.menuController.close();
                return;
            }

            // Modal open check
            const isModalOpen = this.modalService.isModalOpened();
            if (isModalOpen) {
                this.modalService.closeModal();
                return;
            }

            // Can exit the app
            if (this.canExitApp()) {
                const showCartPrompt = await this.cartService.canShowCartExitPrompt();

                if (showCartPrompt) {
                    this.cartService.sendExitCartPromptNudge();
                    return;
                }

                // Finally exit the
                this.platformService.exitApp();
                return;
            }

            if (this.isMapSkipPage()) {
                this.goToHomePage({ replaceUrl: true });
                return;
            }

            // Finally go back
            this.goBack();
        } catch (e) {}
    }

    private isMapSkipPage(): boolean {
        const mapUrl = `${PAGE_ROUTES.ADDRESS.CHILDREN.MAP.PATH}`;

        return (
            this.currentUrl.includes(mapUrl) &&
            this.currentUrl.includes("citySelection")
        );
    }

    private canExitApp(): boolean {
        if (this.appCanExitOnBackButton) {
            return true;
        }

        for (let i = 0; i < EXIT_PATHS.length; i++) {
            if (this.currentUrl.match(new RegExp(EXIT_PATHS[i]))) {
                return true;
            }
        }

        return false;
    }

    private isSchedulePage(): boolean {
        return (
            this.currentUrl.match(new RegExp(PAGE_ROUTES.SCHEDULE.PATH)) &&
            this.currentUrl.indexOf("subscription") === -1
        );
    }

    private isScheduleThankyouFlow(): boolean {
        return (
            this.previousUrl.match(new RegExp(PAGE_ROUTES.THANKYOU.PATH)) &&
            this.isSchedulePage()
        );
    }
}
