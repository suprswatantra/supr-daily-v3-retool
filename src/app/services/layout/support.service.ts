import { Injectable } from "@angular/core";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";

import { ComplaintViewV2, ComplaintCreatedList } from "@models";
import { ComplaintSetting } from "@types";

@Injectable({
    providedIn: "root",
})
export class SupportService {
    constructor(
        private utilService: UtilService,
        private dateService: DateService
    ) {}

    getIvrCallData(orders, today) {
        const slots = orders.reduce((slots, order) => {
            if (order && order.time_slot && !slots.includes(order.time_slot)) {
                slots.push(order.time_slot);
            }
            return slots;
        }, []);

        let isTodayOrders = true;
        if (slots && slots.length > 1) {
            isTodayOrders = false;
        }
        return {
            isTodayOrders,
            todayDate: today,
            slot: slots[0] || "",
        };
    }

    getActiveComplaints(
        complaintsList: ComplaintViewV2[],
        complaintConfig: ComplaintSetting,
        newComplaints: ComplaintCreatedList[]
    ) {
        let complaints =
            (complaintsList &&
                complaintsList.slice(
                    0,
                    complaintConfig.ACTIVE_COMPLAINT_NUMBER
                )) ||
            [];
        complaints = complaints.filter((complaint) => {
            const {
                complaint_creation_time,
                complaint_status_timeline,
            } = complaint;
            if (
                complaint_status_timeline &&
                complaint_status_timeline.complaint_status
            ) {
                const date = this.utilService.getDateFromTimestamp(
                    complaint_creation_time
                );
                const complaintDate = this.dateService.dateFromText(date);
                const days = this.dateService.daysFromToday(complaintDate);
                if (
                    Math.abs(days) <= complaintConfig.ACTIVE_COMPLAINT_DAYS &&
                    complaintConfig.ACTIVE_COMPLAINT_STATUS.includes(
                        complaint_status_timeline.complaint_status
                    )
                ) {
                    const isNewComplaint =
                        !this.utilService.isEmpty(newComplaints) &&
                        newComplaints.find(
                            (newComplaint) =>
                                newComplaint.complaint_id ===
                                complaint.complaint_id
                        );
                    if (!this.utilService.isEmpty(isNewComplaint)) {
                        complaint["is_new"] = true;
                    }
                    return true;
                }
            }
            return false;
        });
        return complaints;
    }
}
