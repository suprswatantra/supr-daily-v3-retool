import { Injectable, ComponentFactoryResolver } from "@angular/core";

import { COMPONENT_WIDGET_TYPE } from "@constants";
import { V3Component } from "@shared/models";
import { DynamicComponent } from "@shared/components/supr-dynamic-component/supr-dynamic-component";

import { V3CarouselWidgetComponent } from "@shared/components/supr-v3-containers/supr-v3-carousel-widget/supr-v3-carousel-widget.component";
import { V3GridWidgetComponent } from "@shared/components/supr-v3-containers/supr-v3-grid-widget/supr-v3-grid-widget.component";

import { V3SkuTileVariantBContainer } from "@shared/components/supr-v3-elements/supr-v3-sku-tile-variant-b/supr-v3-sku-tile-variant-b-container";
import { V3SkuTileVariantAContainer } from "@shared/components/supr-v3-elements/supr-v3-sku-tile-variant-a/supr-v3-sku-tile-variant-a-container";
import { V3SkuTileVariantCContainer } from "@shared/components/supr-v3-elements/supr-v3-sku-tile-variant-c/supr-v3-sku-tile-variant-c-container";

import { V3TileVariantA } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-a/supr-v3-tile-variant-a.component";
import { V3TileVariantB } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-b/supr-v3-tile-variant-b.component";
import { V3TileVariantC } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-c/supr-v3-tile-variant-c.component";
import { V3TileVariantD } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-d/supr-v3-tile-variant-d.component";
import { V3TileVariantE } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-e/supr-v3-tile-variant-e.component";
import { V3TileVariantF } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-f/supr-v3-tile-variant-f.component";
import { V3TileVariantG } from "@shared/components/supr-v3-elements/supr-v3-tile-variant-g/supr-v3-tile-variant-g.component";

@Injectable({
    providedIn: "root",
})
export class ComponentService {
    componentsConstantWidget = COMPONENT_WIDGET_TYPE;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

    COMPONENTS_WIDGET = {
        "supr-v3-container-carousel-widget": V3CarouselWidgetComponent,
        "supr-v3-container-grid-widget": V3GridWidgetComponent,

        "supr-v3-sku-tile-variant-a-container": V3SkuTileVariantAContainer,
        "supr-v3-sku-tile-variant-b-container": V3SkuTileVariantBContainer,
        "supr-v3-sku-tile-variant-c-container": V3SkuTileVariantCContainer,

        "supr-v3-tile-variant-a": V3TileVariantA,
        "supr-v3-tile-variant-b": V3TileVariantB,
        "supr-v3-tile-variant-c": V3TileVariantC,
        "supr-v3-tile-variant-d": V3TileVariantD,
        "supr-v3-tile-variant-e": V3TileVariantE,
        "supr-v3-tile-variant-f": V3TileVariantF,
        "supr-v3-tile-variant-g": V3TileVariantG,
    };

    loadComponent(type: any, data: any, host: any) {
        let componentFactory = null;
        const comps = this.getComponent(type);

        if (comps && this.componentsConstantWidget[type]) {
            const cmp = this.getComps(comps, data);
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(
                cmp.component
            );

            const viewContainerRef = host.viewContainerRef;
            viewContainerRef.clear();

            const componentRef = viewContainerRef.createComponent(
                componentFactory
            );

            (<V3Component>componentRef.instance).data = data;
        }
    }

    private getComponent(type: string) {
        const comps = this.COMPONENTS_WIDGET[
            this.componentsConstantWidget[type]
        ];

        if (!comps) {
            return null;
        }

        return comps;
    }

    private getComps(comps, data = null): any {
        return new DynamicComponent(comps, data);
    }
}
