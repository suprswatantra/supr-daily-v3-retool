import { Injectable } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";

import { filter, finalize } from "rxjs/operators";
import { Subscription } from "rxjs";

import { UtilService } from "@services/util/util.service";
import { NudgeService } from "@services/layout/nudge.service";
import { ApiService } from "@services/data/api.service";
import { SettingsService } from "@services/shared/settings.service";

import { ORDER_FEEDBACK } from "@constants";
import { OrderFeedbackSetting } from "@types";

@Injectable({
    providedIn: "root",
})
export class FeedbackService {
    private apiSubscription: Subscription;
    orderFeedbackSetting: OrderFeedbackSetting.Nudge;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private utilService: UtilService,
        private nudgeService: NudgeService,
        private apiService: ApiService,
        private settingService: SettingsService
    ) {}
    initialize() {
        this.handleFeedbackNudge();
    }

    fetchOrderFeedbackSettings() {
        const orderFeedbackSetting = <OrderFeedbackSetting.Nudge>(
            this.settingService.getSettingsValue(
                "orderFeedbackNudge",
                ORDER_FEEDBACK
            )
        );
        this.orderFeedbackSetting = orderFeedbackSetting || ORDER_FEEDBACK;
    }

    getOrderFeedbackSettings(): OrderFeedbackSetting.Nudge {
        return this.orderFeedbackSetting;
    }

    private handleFeedbackNudge() {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe(event => {
                if (event instanceof NavigationEnd) {
                    // Traverse the active route tree
                    let snapshot = this.activatedRoute.snapshot;
                    let activated = this.activatedRoute.firstChild;

                    while (activated != null) {
                        snapshot = activated.snapshot;
                        activated = activated.firstChild;
                    }

                    // Get the screen name
                    const screenName = this.utilService.getNestedValue(
                        snapshot,
                        "data.screenName"
                    );

                    // Send nudge
                    this.nudgeService.sendFeedbackNudge(screenName);
                }
            });
    }

    private unsubscribeApiCall() {
        if (this.apiSubscription && !this.apiSubscription.closed) {
            this.apiSubscription.unsubscribe();
        }
    }

    private handleThumbs(primaryResponse) {
        return primaryResponse === ORDER_FEEDBACK.thumbsUp ? 1 : 0;
    }

    private handleFeedbackParameters(params) {
        let arr = [];
        Object.entries(params).forEach(([key, value]) => {
            if (value) {
                arr.push(key);
            }
        });
        return arr;
    }

    submitOrderFeedbackResponse(data) {
        const {
            primaryResponse,
            positiveParamsObject,
            negativeParamsObject,
            feedback_request_id,
            comment: comments,
        } = data;
        const feedback_value = this.handleThumbs(primaryResponse);
        const posFeedbackParams = this.handleFeedbackParameters(
            positiveParamsObject
        );
        const negFeedbackParams = this.handleFeedbackParameters(
            negativeParamsObject
        );
        const feedbackResponse = {
            feedback_request_id,
            feedback_value,
            params:
                feedback_value === 1 ? posFeedbackParams : negFeedbackParams,
            comments,
            channel: ORDER_FEEDBACK.channel,
        };
        this.apiSubscription = this.apiService
            .submitFeedbackResponse(feedbackResponse)
            .pipe(
                finalize(() => {
                    this.unsubscribeApiCall();
                })
            )
            .subscribe();
    }
}
