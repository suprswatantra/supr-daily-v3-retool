import { Injectable } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";

import { Subscription } from "rxjs";
import { finalize, filter } from "rxjs/operators";

import {
    DEFAULT_COOL_OFF_TIME_IN_MINS,
    NUDGE_TYPES,
    NUDGE_SETTINGS_KEYS,
    STORAGE_DB_DATA_KEYS,
    ORDER_FEEDBACK,
} from "@constants";
import { Nudge } from "@models";

import { ApiService } from "@services/data/api.service";
import { DateService } from "@services/date/date.service";
import { DbService } from "@services/data/db.service";
import { ErrorService } from "@services/integration/error.service";
import { SettingsService } from "@services/shared/settings.service";
import { StoreService } from "@services/data/store.service";
import { UtilService } from "@services/util/util.service";

import {
    AddressRetroSetting,
    RatingSetting,
    CampaignPopupSetting,
    OrderFeedbackSetting,
    GenericNudgeSetting,
} from "@types";

interface NudgeDict {
    [key: string]: Nudge;
}

export const GENERIC_NUDGE_KEYS = {
    API_KEY: "generic_nudge",
    SETTINGS_KEY: "genericNudge",
    TYPE: "type",
};

@Injectable({
    providedIn: "root",
})
export class NudgeService {
    private nudgeDict: NudgeDict = {};
    private fetchingNudgeState = 0;
    private apiSubscription: Subscription;
    private userId: number;
    private screenName: string;

    constructor(
        private apiService: ApiService,
        private dateService: DateService,
        private dbService: DbService,
        private errorService: ErrorService,
        private settingsService: SettingsService,
        private storeService: StoreService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private utilService: UtilService
    ) {
        this.subscribeToRouterEvents();
    }

    initialize(userId: number) {
        if (this.fetchingNudgeState > 0) {
            return;
        }

        this.userId = userId;
        this.fetchingNudgeState = 1;
        this.apiSubscription = this.apiService
            .fetchNudgeList()
            .pipe(
                finalize(() => {
                    this.fetchingNudgeState = 0;
                    this.unsubscribeApiCall();
                })
            )
            .subscribe(
                (nudgeList) => this.setNudgeList(nudgeList),
                (err: any) =>
                    this.errorService.logSentryError(err, {
                        message: "Fetch nudge list error",
                    })
            );
    }

    sendGenericNudge(screenName: string) {
        const nudgeType = NUDGE_TYPES.GENERIC;

        const displayPages = this.utilService.getNestedValue(
            this.nudgeDict,
            "generic_nudge.nudgeDisplayPages",
            []
        );

        if (!displayPages.includes(screenName)) {
            return;
        }

        if (this.canSendGenericNudge()) {
            setTimeout(() => {
                this.sendNudge(nudgeType);
            }, 3000);
        }
    }

    sendAutoDebitPaymentFailureNudge() {
        setTimeout(() => {
            this.sendNudge(NUDGE_TYPES.AUTO_DEBIT_PAYMENT_FAILURE);
        });
    }

    sendRatingNudge(nudgeTrigger: string) {
        const nudgeType = NUDGE_TYPES.RATING;
        const key = NUDGE_SETTINGS_KEYS[nudgeType];

        const nudgeSettings = <RatingSetting.Nudge>(
            this.settingsService.getSettingsValue(key, {})
        );

        const triggers = nudgeSettings.triggers || {};
        const trigger = triggers[nudgeTrigger];
        if (!trigger || !trigger.enabled) {
            return;
        }

        setTimeout(() => {
            this.sendNudge(nudgeType, { trigger: nudgeTrigger });
        }, trigger.delayTime);
    }

    sendAddressRetroNudge(screenName: string) {
        const nudgeType = NUDGE_TYPES.ADDRESS;
        const key = NUDGE_SETTINGS_KEYS[nudgeType];

        const nudgeSettings = <AddressRetroSetting.Nudge>(
            this.settingsService.getSettingsValue(key, {})
        );

        const displayPages = nudgeSettings["nudgeDisplayPages"] || [];
        if (displayPages.includes(screenName)) {
            setTimeout(() => {
                this.sendNudge(nudgeType);
            }, 3000);
        }
    }

    sendCampaignPopupNudge(screenName: string) {
        const nudgeType = NUDGE_TYPES.CAMPAIGN_POPUP;
        const key = NUDGE_SETTINGS_KEYS[nudgeType];
        const nudgeSettings = <CampaignPopupSetting.Nudge>(
            this.settingsService.getSettingsValue(key, {})
        );

        const displayPages = nudgeSettings["nudgeDisplayPages"] || [];

        if (
            displayPages.includes(screenName) &&
            nudgeSettings &&
            nudgeSettings.enabled
        ) {
            setTimeout(() => {
                this.sendNudge(nudgeType);
            }, 3000);
        }
    }

    sendFeedbackNudge(screenName: string) {
        const nudgeType = NUDGE_TYPES.ORDER_FEEDBACK;
        const key = NUDGE_SETTINGS_KEYS[nudgeType];
        const nudgeSettings = <OrderFeedbackSetting.Nudge>(
            this.settingsService.getSettingsValue(key, ORDER_FEEDBACK)
        );

        const displayPages = nudgeSettings["nudgeDisplayPages"] || [];

        if (
            displayPages.includes(screenName) &&
            nudgeSettings &&
            nudgeSettings.enabled
        ) {
            setTimeout(() => {
                this.sendNudge(nudgeType);
            }, 3000);
        }
    }

    sendCheckoutDisabledNudge() {
        this.handleCheckoutDisabledNudge();
    }

    async sendNudge(nudgeType: string, meta = {}) {
        // Valid nudge
        if (!this.nudgeDict.hasOwnProperty(nudgeType)) {
            return;
        }

        if (!(await this.canSendNudge(nudgeType))) {
            return;
        }

        // Save the updated counters in DB
        this.saveNudgeData(nudgeType);

        // handle nudge
        switch (nudgeType) {
            case NUDGE_TYPES.ADDRESS:
                await this.handleAddressNudge();
                break;
            case NUDGE_TYPES.RATING:
                await this.handleRatingNudge(meta);
                break;
            case NUDGE_TYPES.CAMPAIGN_POPUP:
                this.handleCampaignPopup();
                break;
            case NUDGE_TYPES.ORDER_FEEDBACK:
                await this.handleFeedbackNudge();
                break;
            case NUDGE_TYPES.AUTO_DEBIT_PAYMENT_FAILURE:
                await this.handleAutoDebitPaymentFailureNudge();
                break;
            case NUDGE_TYPES.GENERIC:
                await this.handleGenericNudge();
                break;
        }
    }

    private async handleAddressNudge() {
        this.storeService.showAddressRetro();
    }

    private async handleGenericNudge() {
        this.storeService.showGenericNudge();
    }

    private handleCampaignPopup() {
        this.storeService.showCampaignPopup();
    }

    private handleCheckoutDisabledNudge() {
        this.storeService.showCheckoutDisabledNudge();
    }

    private async handleFeedbackNudge() {
        this.storeService.showFeedback();
    }

    private async handleAutoDebitPaymentFailureNudge() {
        this.storeService.showAutoDebitPaymentFailureNudge();
    }

    private async handleRatingNudge(meta = {}) {
        this.storeService.showAppRatingModal(meta["trigger"]);
    }

    private async canSendNudge(nudgeType: string): Promise<boolean> {
        // Get the DB data
        const displayData = await this.getNudgeDisplayData(nudgeType);
        if (!displayData) {
            return true;
        }

        // Limit check
        if (await this.isMaxLimitOver(nudgeType, displayData["shownCount"])) {
            return false;
        }

        // Cool off check
        if (
            !(await this.isCoolOffTimeOver(
                nudgeType,
                displayData["lastShownTime"]
            ))
        ) {
            return false;
        }

        return true;
    }

    private async isMaxLimitOver(
        nudgeType: string,
        currentCount = 0
    ): Promise<boolean> {
        const nudge = this.nudgeDict[nudgeType];
        return currentCount >= nudge.maxCount;
    }

    private async isCoolOffTimeOver(
        nudgeType: string,
        _lastShownTime: string
    ): Promise<boolean> {
        if (!_lastShownTime) {
            return true;
        }

        const lastShownTime = new Date(_lastShownTime);
        const soFarMinutes = this.dateService.minutesToToday(lastShownTime);
        const coolOffTimeInMinutes = this.getNudgeCoolOffTime(nudgeType);

        return soFarMinutes >= coolOffTimeInMinutes;
    }

    private getNudgeDisplayData(nudgeType: string): Promise<number> {
        const key = `${STORAGE_DB_DATA_KEYS.NUDGE_DISPLAY}${this.userId}${nudgeType}`;
        return this.dbService.getData(key);
    }

    private getNudgeCoolOffTime(nudgeType: string): number {
        const key = NUDGE_SETTINGS_KEYS[nudgeType];
        const nudgeCoolOffTimeFromApi = this.utilService.getNestedValue(
            this.nudgeDict[nudgeType],
            NUDGE_SETTINGS_KEYS.COOL_OFF_TIME
        );

        if (nudgeCoolOffTimeFromApi) {
            return nudgeCoolOffTimeFromApi;
        }

        const nudgeSettings = this.settingsService.getSettingsValue(key, {});

        return (
            nudgeSettings[NUDGE_SETTINGS_KEYS.COOL_OFF_TIME] ||
            DEFAULT_COOL_OFF_TIME_IN_MINS
        );
    }

    private async saveNudgeData(nudgeType: string) {
        const currentData = (await this.getNudgeDisplayData(nudgeType)) || {};
        const shownCount = (currentData["shownCount"] || 0) + 1;
        const lastShownTime = new Date().toString();
        const newData = { shownCount, lastShownTime };
        const key = `${STORAGE_DB_DATA_KEYS.NUDGE_DISPLAY}${this.userId}${nudgeType}`;

        return this.dbService.setData(key, newData);
    }

    private setNudgeList(nudges: Nudge[]) {
        this.nudgeDict = nudges.reduce((acc, nudge) => {
            if (nudge.isGeneric) {
                acc[GENERIC_NUDGE_KEYS.API_KEY] = nudge;
            } else {
                acc[nudge.type] = nudge;
            }

            return acc;
        }, {});

        if (this.nudgeDict[GENERIC_NUDGE_KEYS.API_KEY]) {
            const nudgeType = this.nudgeDict[GENERIC_NUDGE_KEYS.API_KEY].type;
            const genericNudge = this.nudgeDict[GENERIC_NUDGE_KEYS.API_KEY]
                .context;

            genericNudge[GENERIC_NUDGE_KEYS.TYPE] = nudgeType;

            this.settingsService.addSingleSettingBlock(
                GENERIC_NUDGE_KEYS.SETTINGS_KEY,
                genericNudge
            );

            this.sendGenericNudge(this.screenName);
        }
    }

    private unsubscribeApiCall() {
        if (this.apiSubscription && !this.apiSubscription.closed) {
            this.apiSubscription.unsubscribe();
        }
    }

    private subscribeToRouterEvents() {
        this.router.events
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    // Traverse the active route tree
                    let snapshot = this.activatedRoute.snapshot;
                    let activated = this.activatedRoute.firstChild;

                    while (activated != null) {
                        snapshot = activated.snapshot;
                        activated = activated.firstChild;
                    }

                    // Get the screen name
                    this.screenName = this.utilService.getNestedValue(
                        snapshot,
                        "data.screenName"
                    );

                    // Send nudge
                    if (this.canSendGenericNudge()) {
                        this.sendGenericNudge(this.screenName);
                    }
                }
            });
    }

    private canSendGenericNudge(): boolean {
        const key = NUDGE_SETTINGS_KEYS[NUDGE_TYPES.GENERIC];
        const nudgeSettings = <GenericNudgeSetting.Nudge>(
            this.settingsService.getSettingsValue(key, {})
        );

        const isEmptySettings = this.utilService.isEmpty(nudgeSettings);

        if (isEmptySettings) {
            return false;
        }

        if (this.nudgeDict && this.nudgeDict[GENERIC_NUDGE_KEYS.API_KEY]) {
            return true;
        }

        return false;
    }
}
