import { Injectable } from "@angular/core";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import {
    V3LayoutItemData,
    V3LayoutEventClick,
    V3LayoutEventClickNavigation,
    Sku,
    V3LayoutCta,
    Featured,
} from "@shared/models";
import { FEATURED_ITEM } from "@pages/home/constants/home.constants";

@Injectable({
    providedIn: "root",
})
export class HomeLayoutService {
    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    handleEvents(data: V3LayoutItemData | V3LayoutCta) {
        const clickEventData: V3LayoutEventClick =
            this.getNestedValue(data, "cta.events.click", null) ||
            this.getNestedValue(data, "events.click", null);

        this.handleNavigation(clickEventData);
    }

    calculatePercentage(sku: Sku) {
        return Math.ceil(
            (100 * (sku.unit_mrp - sku.unit_price)) / sku.unit_mrp
        );
    }

    getPosition(row: number, col: number): number {
        const _row = row + 1;
        const _col = col + 1;
        const pos = 2 * _row;

        return _col === 2 ? pos : pos - 1;
    }

    // It will make 2D array with given data and n number of row
    groupArray<T>(data: Array<T>, n: number): Array<T[]> {
        return data
            .map(function (_, i) {
                return i % n === 0 ? data.slice(i, i + n) : null;
            })
            .filter(function (e) {
                return e;
            });
    }

    private handleNavigation(clickEventData: V3LayoutEventClick) {
        const navigationObj: V3LayoutEventClickNavigation = this.getNestedValue(
            clickEventData,
            "navigation",
            null
        );

        if (this.utilService.isEmpty(navigationObj)) {
            return null;
        }

        if (navigationObj.source === "in_app") {
            this.routerService.goToUrl(navigationObj.link);
        } else if (navigationObj.source === "out_side_app") {
            window.open(String(navigationObj.link), "_system", "location=yes");
        }
    }

    private getNestedValue(object: any, propertyName: any, defaultValue: any) {
        if (this.utilService.isEmpty(object)) {
            return null;
        }

        return this.utilService.getNestedValue(
            object,
            propertyName,
            defaultValue
        );
    }

    handleFeaturedItemClick(item: Featured.Item) {
        const action = item.action;
        if (!action || action.type === FEATURED_ITEM.ACTION_TYPES.STATIC) {
            return;
        }

        if (action.type === FEATURED_ITEM.ACTION_TYPES.NAVIGATION) {
            this.getFeaturedItemNavigationUrl(item);
        }
    }

    private getFeaturedItemNavigationUrl(item: Featured.Item) {
        const {
            pageType,
            paramValue,
            filterValue,
        } = item.action.navigationParams;

        switch (pageType) {
            case FEATURED_ITEM.NAVIGATION_PAGES.CATEGORY:
                this.goToCategoryPage(paramValue, filterValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.ESSENTIAL:
                this.goToEssentialPage(paramValue, filterValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.COLLECTION:
                // Param Value is string and need to send Int value so that used +paramValue
                this.goToCollectionPage(+paramValue);
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.WALLET_RECHARGE:
                this.goToWalletPage();
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.INAPP_URL:
                // paramValue is url in this case
                this.routerService.goToUrl(String(paramValue));
                break;
            case FEATURED_ITEM.NAVIGATION_PAGES.OUTSIDE_URL:
                // paramValue is url in this case
                window.open(String(paramValue), "_system", "location=yes");
                break;
        }
    }

    private goToCategoryPage(categoryId: number, filterValue?: number) {
        const queryParams = {};
        if (filterValue) {
            queryParams["filterId"] = filterValue;
        }

        this.routerService.goToCategoryPage(categoryId, {
            queryParams,
        });
    }

    private goToEssentialPage(categoryId: number, filterValue?: number) {
        const queryParams = {};
        if (filterValue) {
            queryParams["filterId"] = filterValue;
        }

        this.routerService.goToCategoryEssentialsPage(categoryId, {
            queryParams,
        });
    }

    private goToCollectionPage(viewId: number) {
        this.routerService.goToCollectionPage(viewId);
    }

    private goToWalletPage() {
        this.routerService.goToWalletPage();
    }
}
