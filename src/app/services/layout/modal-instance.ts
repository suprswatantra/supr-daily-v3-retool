import { ModalComponent } from "../../shared/components/supr-modal/supr-modal.component";

export class ModalInstance {
    public id: string;
    public modal: ModalComponent;
}
