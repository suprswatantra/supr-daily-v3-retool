import { Injectable } from "@angular/core";
import { ModalComponent } from "../../shared/components/supr-modal/supr-modal.component";

import { AnalyticsService } from "@services/integration/analytics.service";

@Injectable({
    providedIn: "root",
})
export class ModalService {
    modalInstance: ModalComponent;

    constructor(private analyticsService: AnalyticsService) {}

    public addModal(newInstance: ModalComponent) {
        if (this.modalInstance && !this.modalInstance.stackingAllowed) {
            // Close the previous modal if its open still
            this.modalInstance.closeModal(0);
        }

        // Store the latest modal instance
        setTimeout(() => {
            this.modalInstance = newInstance;
            this.analyticsService.setModalName(newInstance.modalName);
        }, 0);
    }

    public removeModal() {
        if (!this.modalInstance) {
            return;
        }

        this.modalInstance = null;
        this.analyticsService.setModalName(null);
    }

    public closeModal(closeDelayTime?: number, force?: boolean) {
        if (!this.modalInstance) {
            return;
        }

        const canCloseModal = force || this.modalInstance.canModalBeClosed();
        if (canCloseModal) {
            this.modalInstance.closeModal(closeDelayTime);
            this.removeModal();
        }
    }

    public isModalOpened(): boolean {
        return !!this.modalInstance;
    }

    public getModalName(): string {
        return this.modalInstance && this.modalInstance.modalName;
    }
}
