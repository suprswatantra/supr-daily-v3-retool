import { TestBed } from "@angular/core/testing";

import { DateService } from "./date.service";

import { SettingsService } from "@services/shared/settings.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import {
    BaseUnitTestImportsWithStoreIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class SettingsServiceMock {}

describe("DateService", () => {
    let dateService: DateService;

    let settingsService: SettingsService;
    let blockAccessService: BlockAccessService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],
            providers: [
                BlockAccessService,
                { provide: SettingsService, useClass: SettingsServiceMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        settingsService = TestBed.get(SettingsService);

        blockAccessService = TestBed.get(BlockAccessService);

        dateService = new DateService(settingsService, blockAccessService);
    });

    it("#adds number of days to given date", () => {
        const baseDate = new Date(2020, 5, 10);

        const daysToAdd = 5;

        const expectedDate = new Date(2020, 5, 15);

        expect(dateService.addDays(baseDate, daysToAdd).getTime()).toBe(
            expectedDate.getTime()
        );
    });

    it("#returns proper date suffix", () => {
        expect(dateService.getDateSuffix("2020-05-10")).toBe("th");

        expect(dateService.getDateSuffix("2020-05-21")).toBe("st");

        expect(dateService.getDateSuffix("2020-05-3")).toBe("rd");

        expect(dateService.getDateSuffix("2020-05-22")).toBe("nd");
    });
});
