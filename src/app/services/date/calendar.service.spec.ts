import { TestBed } from "@angular/core/testing";

import { CalendarService } from "./calendar.service";

import { ScheduleService } from "@services/shared/schedule.service";

import { SuprDateService } from "./supr-date.service";

import { DateService } from "./date.service";

import { UtilService } from "@services/util/util.service";

import { addDays, format } from "date-fns";

import {
    BaseUnitTestImportsWithStoreIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

describe("CalendarService", () => {
    let calendarService: CalendarService;

    let utilService: UtilService;

    let dateService: DateService;

    let suprDateService: SuprDateService;

    let scheduleService: ScheduleService;

    let spy: any;

    console.log(spy);

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                UtilService,

                DateService,

                SuprDateService,

                ScheduleService,

                ...BaseUnitTestProvidersIonic,
            ],
        });

        utilService = TestBed.get(UtilService);

        dateService = TestBed.get(DateService);

        suprDateService = TestBed.get(SuprDateService);

        scheduleService = TestBed.get(ScheduleService);

        calendarService = new CalendarService(
            utilService,

            dateService,

            suprDateService,

            scheduleService
        );
    });

    function removeTimeInfoFromDate(date: Date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    it("#should create calendar service instance", () => {
        expect(calendarService).toBeTruthy();
    });

    it("#should getFutureDaysFromDate inclusive of starting date", () => {
        expect(calendarService.getFutureDaysFromDate("2020-05-10", 3)).toEqual([
            "2020-05-10",

            "2020-05-11",

            "2020-05-12",
        ]);
    });

    it("#should get next available date when there is no vacation", () => {
        calendarService.setTpEnabled(false);

        spy = spyOn(dateService, "hasTodaysCutOffTimePassed").and.returnValue(
            false
        );

        const expectedDate = removeTimeInfoFromDate(addDays(new Date(), 1));

        expect(calendarService.getNextAvailableDate()).toEqual(expectedDate);
    });

    it("#should get next available date when there is no vacation and hub cutoff time has passed", () => {
        calendarService.setTpEnabled(false);

        spy = spyOn(dateService, "hasTodaysCutOffTimePassed").and.returnValue(
            true
        );

        const expectedDate = removeTimeInfoFromDate(addDays(new Date(), 2));

        expect(calendarService.getNextAvailableDate()).toEqual(expectedDate);
    });

    it("#should get next available date when there is vacation and hub cutoff time has not passed", () => {
        calendarService.setTpEnabled(false);

        spy = spyOn(dateService, "hasTodaysCutOffTimePassed").and.returnValue(
            false
        );

        const today = new Date();

        const vacationEnd = addDays(today, 3);

        const nextAvailableDate = removeTimeInfoFromDate(addDays(today, 4));

        const vacation = {
            start_date: format(today, "yyyy-MM-dd"),

            end_date: format(vacationEnd, "yyyy-MM-dd"),
        };

        expect(calendarService.getNextAvailableDate(vacation)).toEqual(
            nextAvailableDate
        );
    });
});
