import { Injectable } from "@angular/core";
import { formatDate } from "@angular/common";

import {
    isDate,
    format,
    endOfWeek,
    startOfWeek,
    eachDayOfInterval,
} from "date-fns";

import {
    ONE_DAY,
    FIRST_DAY_OF_WEEK,
    DEFAULT_LOCALE,
    DAY_NAMES_FULL,
    SETTINGS,
} from "@constants";

import { SettingsService } from "@services/shared/settings.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { TEXTS } from "@pages/cart/constants";

@Injectable({
    providedIn: "root",
})
export class DateService {
    constructor(
        private settingsService: SettingsService,
        private blockAccessService: BlockAccessService
    ) {}
    // Get todays date (client side)
    getTodayDate(): Date {
        return new Date();
    }

    getTomorrowDate(): Date {
        const today = this.getTodayDate();
        return this.addDays(today, 1);
    }

    addNDaysToToday(n: number): Date {
        const today = this.getTodayDate();
        return this.addDays(today, n);
    }

    getTodayDateText(): string {
        const date = new Date();
        const todayText = this.textFromDate(date);
        return todayText;
    }

    getDateSuffix(dateString: string): string {
        const date = new Date(dateString).getDate();
        if (date === 1 || date === 21 || date === 31) {
            return "st";
        } else if (date === 2 || date === 22) {
            return "nd";
        } else if (date === 3 || date === 23) {
            return "rd";
        }

        return "th";
    }

    /* Get date from year, month, date */
    newDate(year: number, month: number, date: number): Date {
        return new Date(year, month, date);
    }

    /* Get date from text yyyy-mm-dd */
    dateFromText(dateText: string): Date {
        if (!dateText) {
            return;
        }

        const date = new Date(dateText.split("T")[0]);
        const timezoneOffset = date.getTimezoneOffset();

        if (timezoneOffset > 0) {
            /* Setting time to 12 Midnight to handle negative timezones creating previous day's date */
            date.setMinutes(date.getTimezoneOffset());
            return date;
        }

        return this.newDate(
            date.getFullYear(),
            date.getMonth(),
            date.getDate()
        );
    }

    // Get text yyyy-mm-dd from date
    textFromDate(date: Date): string {
        return formatDate(date, "yyy-MM-dd", DEFAULT_LOCALE);
    }

    // Adjusted week day
    dayOfWeek(getDay: number) {
        return (getDay - FIRST_DAY_OF_WEEK + 7) % 7;
    }

    // Calculate day of the year
    dayOfYear(date: Date) {
        return Math.floor(
            (date.getTime() - new Date(date.getFullYear(), 0, 0).getTime()) /
                ONE_DAY
        );
    }

    // Calculate days from time
    timeToDays(time: number): number {
        return Math.floor(time / ONE_DAY);
    }

    // Add days to a date
    addDays(refDate: Date, days: number): Date {
        // New date
        let date = new Date(refDate.getTime() + days * ONE_DAY);
        if (date.getTimezoneOffset() > refDate.getTimezoneOffset()) {
            date = new Date(date.getTime() + ONE_DAY);
        }

        // Create a new date from year, month, date to reset the time
        return this.newDate(
            date.getFullYear(),
            date.getMonth(),
            date.getDate()
        );
    }

    getPreviousDateFromString(date: string): Date {
        return this.addDays(new Date(date), -1);
    }

    // Add months to a date
    addMonths(refDate: Date, months: number): Date {
        let year = refDate.getFullYear();
        let month = refDate.getMonth() + months;
        while (month > 11) {
            month -= 12;
            year++;
        }

        const lastDate = this.lastDateOfMonth(month, year).getDate();
        if (refDate.getDate() > lastDate) {
            // Last day of next month e.g. 31st Jan to 28th Feb
            return this.newDate(year, month, lastDate);
        } else {
            // Reduce 1 day e.g. 28th Feb to 27th Mar
            return this.addDays(
                this.newDate(year, month, refDate.getDate()),
                -1
            );
        }
    }

    // Add years to a date
    addYears(refDate: Date, years: number): Date {
        // New date
        const date = this.newDate(
            refDate.getFullYear() + years,
            refDate.getMonth(),
            refDate.getDate()
        );

        // Reduce 1 day
        return this.addDays(date, -1);
    }

    // Get the adjusted start date for the week which contains the date: date + offsetDays
    startDateOfWeek(date: Date, offsetDays?: number) {
        if (offsetDays === undefined) {
            offsetDays = 0;
        }

        return this.addDays(date, -this.dayOfWeek(date.getDay()) + offsetDays);
    }

    // Convert time to string with formatting
    timeToText(timeText: string): string {
        return formatDate(timeText, "hh:mm a", DEFAULT_LOCALE);
    }

    // Return time from date text
    timeFromText(dateText: string): number {
        let timeText = dateText.split("T");

        if (timeText.length < 2) {
            return 0;
        }
        timeText = timeText[1].split(":");

        let time = 0;
        const multiplier = [3600, 60, 1];
        for (let i = 0; i < 3; ++i) {
            if (i < timeText.length) {
                time += parseInt(timeText[i], 0) * multiplier[i];
            }
        }

        return time;
    }

    daysFromToday(date: Date): number {
        const today = this.getTodayDate();
        return this.daysBetweenTwoDates(today, date);
    }

    minutesToToday(date: Date): number {
        const today = this.getTodayDate();
        return this.minutesBetweenTwoDates(date, today);
    }

    daysBetweenTwoDates(date1: Date, date2: Date): number {
        const _date1Text = this.textFromDate(date1);
        const _date2Text = this.textFromDate(date2);
        const _date1 = new Date(_date1Text);
        const _date2 = new Date(_date2Text);

        // Get 1 day in milliseconds
        const oneDay = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        const date1Ms = _date1.getTime();
        const date2Ms = _date2.getTime();

        // Calculate the difference in milliseconds
        const differenceMs = date2Ms - date1Ms;

        // Convert back to days and return
        return Math.ceil(differenceMs / oneDay);
    }

    minutesBetweenTwoDates(date1: Date, date2: Date): number {
        // Get 1 minute in milliseconds
        const oneMinute = 1000 * 60;

        // Convert both dates to milliseconds
        const date1Ms = date1.getTime();
        const date2Ms = date2.getTime();

        // Calculate the difference in milliseconds
        const differenceMs = date2Ms - date1Ms;

        // Convert back to minutes and return
        return Math.ceil(differenceMs / oneMinute);
    }

    sortDateStrings(dates: string[]): string[] {
        return dates.sort(
            (a, b) => new Date(a).getTime() - new Date(b).getTime()
        );
    }

    getDayNameOfDate(date: Date): string {
        const day = date.getDay();
        const index = (day + 6) % 7;

        return DAY_NAMES_FULL[index];
    }

    formatDate(date: Date | string | number, formatStr = "yyy-MM-dd"): string {
        return formatDate(date, formatStr, DEFAULT_LOCALE);
    }

    getDatesBetweenRange(startDate: Date, stopDate: Date): Date[] {
        const dateArray = [];
        let currentDate = startDate;

        while (currentDate <= stopDate) {
            dateArray.push(currentDate);
            currentDate = this.addDays(currentDate, 1);
        }

        return dateArray;
    }

    attachTimeToDate(timeText: string, _date?: Date): Date {
        const date = _date || this.getTodayDate();
        const dateText = this.textFromDate(date);
        const dateTimeText = `${dateText}T${timeText}`;

        return new Date(dateTimeText);
    }

    /**
     *
     * @param {*} startTime <"01:00">
     * @param {*} endTime   <"16:45">
     * @memberof DateService
     */
    isCurrentTimeBetweenGivenTimes(startTime: string, endTime: string) {
        const startTimeSplit = startTime.split(":");
        const endTimeSplit = endTime.split(":");

        const currentTime = new Date();
        const currentHours = currentTime.getHours();
        const currentMins = currentTime.getMinutes();

        if (
            Number(startTimeSplit[0]) < currentHours &&
            currentHours < Number(endTimeSplit[0])
        ) {
            return true;
        } else if (Number(startTimeSplit[0]) === currentHours) {
            return Number(startTimeSplit[1]) <= currentMins;
        } else if (currentHours === Number(endTimeSplit[0])) {
            return currentMins <= Number(endTimeSplit[1]);
        } else {
            return false;
        }
    }

    getDeliveryDateText(deliveryDate: string | Date): string {
        let _deliveryDate: Date;

        if (typeof deliveryDate === "string") {
            _deliveryDate = this.dateFromText(deliveryDate);
        } else {
            _deliveryDate = deliveryDate as Date;
        }

        const noOfDaysFromToday = this.daysFromToday(_deliveryDate);

        if (noOfDaysFromToday === 1) {
            return TEXTS.TOMORROW;
        } else {
            return formatDate(deliveryDate, "EEE, d LLL", DEFAULT_LOCALE);
        }
    }

    getEachDayOfGivenInterval(interval: {
        start: Date;
        end: Date;
    }): Array<Date> {
        return eachDayOfInterval(interval);
    }

    /* Mon-Sun week => weekStartOn 1
    Sun-Sat week => weekStartsOn 0 */
    getEachDayOfWeek(
        weekStartsOn: 0 | 1 | 2 | 3 | 4 | 5 | 6 = 1,
        startDate?: Date
    ): Array<Date> {
        const date = startDate || this.getTodayDate();
        const start = startOfWeek(date, { weekStartsOn });
        const end = endOfWeek(date, { weekStartsOn });

        return this.getEachDayOfGivenInterval({ start, end });
    }

    getDayOfWeek(date: Date, dayFormat: "i" | "io" | "ii" | "iii" | "iiii") {
        return format(date, dayFormat);
    }

    dateFnsFormatDate(date = new Date(), dateFormat = "yyyy-MM-dd") {
        return format(date, dateFormat);
    }

    hasTodaysCutOffTimePassed(): boolean {
        const today = this.getTodayDate();
        const cutOffTime = this.getCutOffTime();
        const todayWithCutoff = this.attachTimeToDate(cutOffTime.value);
        const currentTime = today.getTime();
        const hubCutOffTime = todayWithCutoff.getTime();
        return currentTime >= hubCutOffTime;
    }

    /**
     * Fetch cutoff time based on priority
     *
     * @returns
     * @memberof DateService
     */
    getCutOffTime() {
        return (
            this.blockAccessService.getExperimentDrivenCutOffTime() ||
            this.settingsService.getSettingsValue(SETTINGS.CUT_OFF_TIME)
        );
    }

    isTodaysDate(date: string | Date): boolean {
        let dateObject: Date;

        if (!date) {
            return;
        }
        if (typeof date === "string") {
            dateObject = this.dateFromText(date);
        } else {
            dateObject = date;
        }

        return this.daysFromToday(dateObject) === 0;
    }

    isNthDayFromToday(date: string | Date, n: number): boolean {
        let dateObject: Date;

        if (!date) {
            return;
        }
        if (typeof date === "string") {
            dateObject = this.dateFromText(date);
        } else {
            dateObject = date;
        }

        return this.daysFromToday(dateObject) === n;
    }

    /**
     * Check if date string is a valid date
     *
     * @param {string} date
     * @returns {boolean}
     * @memberof DateService
     */
    isValidDateString(date: string): boolean {
        const dateObject = this.dateFromText(date);

        return isDate(dateObject);
    }

    private lastDateOfMonth(month: number, year: number): Date {
        // Next month
        month++;

        // Adjust year
        if (month > 11) {
            month -= 12;
            year++;
        }

        // 1 day before from 1st of next month
        return this.addDays(this.newDate(year, month, 1), -1);
    }
}
