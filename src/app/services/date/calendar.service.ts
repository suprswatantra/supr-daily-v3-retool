import { Injectable } from "@angular/core";

import { Vacation, VacationApiRes, Subscription, Sku } from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { Calendar, ScheduleDict } from "@types";

@Injectable({
    providedIn: "root",
})
export class CalendarService {
    private yearData: Calendar.YearData = {};
    private threeMonthData: Calendar.MonthData[] = [];
    private tpEnabled = false;

    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private suprDateService: SuprDateService,
        private scheduleService: ScheduleService
    ) {
        const todaySuprDate = suprDateService.todaySuprDate();
        this.loadThreeMonthDataForDate(todaySuprDate);
    }

    loadThreeMonthDataForDate(
        requiredDate: Calendar.DayData
    ): Calendar.MonthData[] {
        // Check if data reload is required
        if (!this.utilService.isEmpty(this.threeMonthData)) {
            // Last date of second row will indicate the current month always
            if (
                this.threeMonthData[1][1][7].month === requiredDate.month &&
                this.threeMonthData[1][1][7].year === requiredDate.year
            ) {
                return this.threeMonthData;
            }
        }

        // Reset data
        this.threeMonthData = [];

        this.addPreviousMonth(requiredDate);
        this.addCurrentMonth(requiredDate);
        this.addNextMonth(requiredDate);

        return this.threeMonthData;
    }

    /* Get date range for creating or updating a vacation */
    getVacationDaysRange(
        offset: number,
        startDate?: string,
        endDate?: string
    ): Calendar.DayData[] {
        let daysData = [],
            rangeStart: Date,
            rangeEnd: Date;

        /* When fetching next set of dates on scroll, increment the start date by 1
        as it is already rendered */
        if (startDate) {
            rangeStart = this.dateService.addDays(new Date(startDate), 1);
        } else {
            const today = new Date();
            rangeStart =
                today.getHours() < 23
                    ? this.dateService.addDays(today, 1)
                    : this.dateService.addDays(today, 2);
        }

        if (endDate) {
            rangeEnd = this.dateService.addDays(new Date(endDate), offset);
        } else {
            rangeEnd = this.dateService.addDays(rangeStart, offset);
        }

        daysData = this.dateService.getDatesBetweenRange(rangeStart, rangeEnd);

        return daysData.map((date) => this.suprDateService.suprDate(date));
    }

    getNextAvailableDate(vacation?: Vacation, sku?: Sku): Date {
        const today = this.dateService.getTodayDate();
        /* Initialize next available date as today and no. of days to add as 1 */
        let fromDate: Date = today,
            noOfDaysToAdd = 1;

        if (this.dateService.hasTodaysCutOffTimePassed() || this.tpEnabled) {
            noOfDaysToAdd = 2;
        }

        /* If sku is out of stock and we have its next available date, then set that date as start date */
        if (sku && sku.out_of_stock && sku.next_available_date) {
            const skuNexAvblDate = this.dateService.dateFromText(
                sku.next_available_date
            );
            const tempNextDeliveryDate = this.dateService.addDays(
                fromDate,
                noOfDaysToAdd
            );

            /* If sku is available on or after the (from data + no. of days), schedule it for skuNexAvblDate
            else make no change to from date as well as no. of days to be added to from date */
            if (
                this.dateService.daysBetweenTwoDates(
                    tempNextDeliveryDate,
                    skuNexAvblDate
                ) >= 0
            ) {
                noOfDaysToAdd = 0;
                fromDate = skuNexAvblDate;
            }
        }

        if (vacation && vacation.start_date && vacation.end_date) {
            const isVacationDate = this.scheduleService.isDateInVacation(
                vacation,
                this.dateService.textFromDate(fromDate)
            );

            if (isVacationDate) {
                fromDate = this.dateService.dateFromText(vacation.end_date);
                noOfDaysToAdd = 1;
            } else {
                const daysBeforeStart = this.dateService.daysBetweenTwoDates(
                    fromDate,
                    this.dateService.dateFromText(vacation.start_date)
                );

                /* if no. of days to be added to startDate leads to a vacation date change start date. */
                if (daysBeforeStart - noOfDaysToAdd <= 0) {
                    fromDate = this.dateService.dateFromText(vacation.end_date);
                    noOfDaysToAdd = 1;
                }
            }
        }
        return this.dateService.addDays(fromDate, noOfDaysToAdd);
    }

    getLatestOrderDate(): Date {
        const today = this.dateService.getTodayDate();
        const cutOffTime = this.dateService.getCutOffTime();
        const todayWithCutOff = this.dateService.attachTimeToDate(
            cutOffTime.value
        );

        const currentTime = today.getTime();
        const hubCutOffTime = todayWithCutOff.getTime();
        const noOfDaysToAdd = currentTime > hubCutOffTime ? 1 : 0;

        return this.dateService.addDays(
            this.dateService.getTodayDate(),
            noOfDaysToAdd
        );
    }

    isTomorrowAvailable(
        vacation?: Vacation,
        nextAvailableDate?: Date
    ): boolean {
        const _nextAvailableDate =
            nextAvailableDate || this.getNextAvailableDate(vacation);
        const tomorrow = this.dateService.getTomorrowDate();
        const daysDiff = this.dateService.daysBetweenTwoDates(
            tomorrow,
            _nextAvailableDate
        );

        return daysDiff === 0;
    }

    /* Dates are inclusive of starting date */
    getFutureDaysFromDate(
        fromDateText: string,
        numberOfDays: number
    ): string[] {
        let dateText: string;
        const fromDate = this.dateService.dateFromText(fromDateText),
            days: string[] = [];

        for (let i = 0; i < numberOfDays; i++) {
            dateText = this.dateService.textFromDate(
                this.dateService.addDays(fromDate, i)
            );
            days.push(dateText);
        }

        return days;
    }

    /* Dates are inclusive of starting date */
    getPastDaysFromDate(fromDateText: string, numberOfDays: number): string[] {
        let dateText: string;
        const fromDate = this.dateService.dateFromText(fromDateText),
            days: string[] = [];

        for (let i = 0; i < numberOfDays; i++) {
            dateText = this.dateService.textFromDate(
                this.dateService.addDays(fromDate, -i)
            );
            days.push(dateText);
        }

        return days;
    }

    validateVacation(res: VacationApiRes): VacationApiRes {
        const { vacation, system_vacation, system_vacation_message } = res;
        if (!vacation || !vacation.start_date || !vacation.end_date) {
            return {
                vacation: null,
                system_vacation,
                system_vacation_message,
            };
        } else {
            const endDate = this.dateService.dateFromText(vacation.end_date);
            const daysBetweenTodayAndEndDate = this.dateService.daysFromToday(
                endDate
            );
            return {
                vacation: daysBetweenTodayAndEndDate > 0 ? vacation : null,
                system_vacation,
                system_vacation_message,
            };
        }
    }

    isDeliveryDateValid(dateText: string, vacation?: Vacation): boolean {
        if (
            vacation &&
            this.scheduleService.isDateInVacation(vacation, dateText)
        ) {
            return false;
        }

        const date = this.dateService.dateFromText(dateText);
        const daysBetweenTodayAndDate = this.dateService.daysFromToday(date);

        const today = this.dateService.getTodayDate();
        const cutOffTime = this.dateService.getCutOffTime();
        const todayWithCutOff = this.dateService.attachTimeToDate(
            cutOffTime.value
        );

        const currentTime = today.getTime();
        const hubCutOffTime = todayWithCutOff.getTime();

        return (
            (daysBetweenTodayAndDate > 1 ||
                (daysBetweenTodayAndDate === 1 &&
                    currentTime < hubCutOffTime)) &&
            !this.tpEnabled
        );
    }

    getQuantityRemaining(
        subscription: Subscription,
        dateText: string,
        scheduleData: ScheduleDict,
        currentDateTime: number
    ): number {
        let dateTime = null;
        const startDateTime = currentDateTime;
        const schedule = scheduleData;

        // Starting at dateText, search till start date in decreasing order

        do {
            // Found entry in schedule
            if (schedule.hasOwnProperty(dateText)) {
                const scheduleForDate = schedule[dateText];

                // Check for subscription id
                const order = scheduleForDate.subscriptions.find(
                    (o) => o.id === subscription.id
                );
                if (order) {
                    return this.utilService.toValue(order.remaining_quantity);
                }
            }

            // Go back one day
            const date = this.dateService.addDays(
                this.dateService.dateFromText(dateText),
                -1
            );
            dateText = this.dateService.textFromDate(date);
            dateTime = date.getTime();
        } while (dateTime >= startDateTime);

        // If no virtual orders found till today, return subscription balance
        return subscription.balance;
    }

    getSuprPassStartDate() {
        const today = this.dateService.getTodayDate();
        let noOfDaysToAdd = 1;
        if (this.dateService.hasTodaysCutOffTimePassed() || this.tpEnabled) {
            noOfDaysToAdd = 2;
        }

        const startDate = this.dateService.addDays(today, noOfDaysToAdd);
        return this.dateService.textFromDate(startDate);
    }

    setTpEnabled(tpEnabled: boolean = false) {
        this.tpEnabled = tpEnabled;
    }

    clearTPEnabled() {
        this.tpEnabled = false;
    }

    isTpEnabled(): boolean {
        return this.tpEnabled;
    }

    isTodayLastDayOfWeek(currentWeek?: Array<Date>): boolean {
        const today = this.dateService.getTodayDate();
        const currentWeekDates =
            currentWeek || this.dateService.getEachDayOfWeek();

        return (
            this.dateService.daysBetweenTwoDates(
                today,
                currentWeekDates[currentWeekDates.length - 1]
            ) === 0
        );
    }

    // Get data for the nWeeks starting at specified indx of the specified year
    private getWeekData(
        year: number,
        startIndx: number,
        nWeeks: number
    ): Calendar.WeekData[] {
        // Check if year data is present or not
        this.addYearToCalendar(year);

        // Get year data
        const yearData = this.yearData[year];
        const len = yearData.length;

        // Check if previous or next years will be needed
        let otherYearData: Calendar.WeekData[];
        let otherYearAnchorIndx = 0;

        // Check previous year
        if (startIndx < 0) {
            // Add last year
            this.addYearToCalendar(year - 1);

            // Get data
            otherYearData = this.yearData[year - 1];

            // Sometimes last week of last year and first week of this year could be same
            // Find the last indx for which the entire week belongs to the other year
            otherYearAnchorIndx = otherYearData.length - 1;
            while (
                otherYearData[otherYearAnchorIndx][1].year !==
                otherYearData[otherYearAnchorIndx][7].year
            ) {
                --otherYearAnchorIndx;
            }
        }

        // Check next year
        if (startIndx + nWeeks - 1 >= len) {
            // Add next year
            this.addYearToCalendar(year + 1);

            // Get data
            otherYearData = this.yearData[year + 1];

            // Sometimes last week of last year and first week of this year could be same
            // Find the first indx for which the entire week belongs to the other year
            otherYearAnchorIndx = 0;
            while (
                otherYearData[otherYearAnchorIndx][1].year !==
                otherYearData[otherYearAnchorIndx][7].year
            ) {
                ++otherYearAnchorIndx;
            }
        }

        // Load week data
        const weeksData: Calendar.WeekData[] = [];
        for (let w = 0; w < nWeeks; ++w) {
            // Last year
            if (startIndx + w < 0) {
                weeksData.push(
                    otherYearData[startIndx + w + otherYearAnchorIndx + 1]
                );
            } else if (startIndx + w < len) {
                weeksData.push(yearData[startIndx + w]);
            } else {
                weeksData.push(
                    otherYearData[startIndx + w - len + otherYearAnchorIndx]
                );
            }
        }

        return weeksData;
    }

    private addPreviousMonth(requiredDate: Calendar.DayData) {
        const { year, month } = requiredDate;
        if (month === 0) {
            // If in Jan, add Dec of last year
            this.threeMonthData.push(this.getMonthData(year - 1, 11));
        } else {
            this.threeMonthData.push(this.getMonthData(year, month - 1));
        }
    }

    private addCurrentMonth(requiredDate: Calendar.DayData) {
        const { year, month } = requiredDate;
        this.threeMonthData.push(this.getMonthData(year, month));
    }

    private addNextMonth(requiredDate: Calendar.DayData) {
        const { year, month } = requiredDate;
        if (month === 11) {
            // If in dec, add Jan of next year
            this.threeMonthData.push(this.getMonthData(year + 1, 0));
        } else {
            this.threeMonthData.push(this.getMonthData(year, month + 1));
        }
    }

    // Get month data for the specified month & year, month data is array of 6 weeks
    private getMonthData(year: number, month: number): Calendar.WeekData[] {
        // Find week indx
        const startDate = this.dateService.newDate(year, month, 1);
        const startDateOfWeek = this.dateService.startDateOfWeek(startDate);
        let startIndx = this.findWeekIndx(startDateOfWeek);

        // Add one week on top if 1st is the first day of week
        if (this.dateService.dayOfWeek(startDate.getDay()) === 0) {
            startIndx--;
        }

        // Get 6 week data
        return this.getWeekData(startDateOfWeek.getFullYear(), startIndx, 6);
    }

    private findWeekIndx(startDateOfWeek: Date): number {
        const year = startDateOfWeek.getFullYear();

        // Make sure data for this year is present
        this.addYearToCalendar(year);

        // Get year data
        const yearData = this.yearData[year];

        const startDateTime = startDateOfWeek.getTime();
        let weekIndex = 0;
        while (startDateTime !== yearData[weekIndex][1].time) {
            weekIndex++;
        }

        return weekIndex;
    }

    private addYearToCalendar(year: number, reset = false) {
        // Year has already been created
        if (reset !== true && this.yearData.hasOwnProperty(year)) {
            return;
        }

        // Year data
        const currentYearData = [];

        // Start date of first week: 1st Jan, year
        let startDateOfWeek = this.dateService.startDateOfWeek(
            this.dateService.dateFromText(year + "-01-01")
        );

        // Populate year data with week elements
        let lastWeek = false;
        while (!lastWeek) {
            // Create a new week
            const week = [];
            for (let d = -1; d <= 7; ++d) {
                week.push(
                    this.suprDateService.suprDate(
                        this.dateService.addDays(startDateOfWeek, d)
                    )
                );
            }

            // Add to year data
            currentYearData.push(week);

            // Next week
            startDateOfWeek = this.dateService.startDateOfWeek(
                startDateOfWeek,
                7
            );

            // Check for break: start date has crossed to next year
            if (startDateOfWeek.getFullYear() > year) {
                lastWeek = true;
            }
        }

        // Add to calendarData
        if (this.yearData === undefined) {
            this.yearData = {};
        }
        this.yearData[year] = currentYearData;
    }
}
