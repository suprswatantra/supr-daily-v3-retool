import { Injectable } from "@angular/core";

import { DateService } from "@services/date/date.service";

import { Calendar } from "@types";
import { DAY_NAMES, MONTH_NAMES } from "@constants";

@Injectable({
    providedIn: "root",
})
export class SuprDateService {
    constructor(private dateService: DateService) {}

    todaySuprDate(): Calendar.DayData {
        const today = this.dateService.getTodayDate();
        return this.suprDate(today);
    }

    suprDate(jsDate: Date): Calendar.DayData {
        const date = jsDate.getDate();
        const month = jsDate.getMonth();
        const year = jsDate.getFullYear();
        const day = this.dateService.dayOfWeek(jsDate.getDay());
        const dateText = this.getDateText(jsDate);

        return {
            date,
            month,
            year,
            day,
            time: jsDate.getTime(),
            dateText,
            dayName: this.getDayName(day),
            monthYear: this.getMonthYearFormat(month, year),
            fullDateText: this.getFullDateText(dateText),
            jsDate,
        };
    }

    suprDateFromText(dateText?: string): Calendar.DayData {
        if (!dateText) {
            return null;
        }

        const date = this.dateService.dateFromText(dateText);
        return this.suprDate(date);
    }

    /**
     * Returns DDD xx MMM yyyy
     *
     * @memberof SuprDateService
     */
    getFullDateText(dateText: string): string {
        const jsDate = this.dateService.dateFromText(dateText);

        const day = this.dateService.dayOfWeek(jsDate.getDay());
        const date = jsDate.getDate();
        const month = jsDate.getMonth();
        const year = jsDate.getFullYear();

        return `${DAY_NAMES[day]} - ${date} ${MONTH_NAMES[month]} ${year}`;
    }

    suprDatesEqual(
        suprDate1: Calendar.DayData,
        suprDate2: Calendar.DayData
    ): boolean {
        if (!suprDate1 || !suprDate2) {
            return false;
        }

        return suprDate1.time === suprDate2.time;
    }
    /**
     * Return xx MMM yyyy format from a given text
     *
     * @memberof SuprDateService
     */
    dateMonthYear(dateText: string): string {
        const jsDate = this.dateService.dateFromText(dateText);

        return `${jsDate.getDate()}  ${
            MONTH_NAMES[jsDate.getMonth()]
        } ${jsDate.getFullYear()}`;
    }

    getMonthFromDate(day: Calendar.DayData) {
        return MONTH_NAMES[day.month];
    }

    getPreviousMonthDate(fromDate: Calendar.DayData): Calendar.DayData {
        let { month, year } = fromDate;

        month--;
        if (month < 0) {
            month = 11;
            year--;
        }

        const newDate = this.dateService.newDate(year, month, 1);
        return this.suprDate(newDate);
    }

    getNextMonthDate(fromDate: Calendar.DayData): Calendar.DayData {
        let { month, year } = fromDate;

        month++;
        if (month > 11) {
            month = 0;
            year++;
        }

        const newDate = this.dateService.newDate(year, month, 1);
        return this.suprDate(newDate);
    }

    private getDateText(date: Date): string {
        return this.dateService.textFromDate(date);
    }

    private getDayName(day: number): string {
        return DAY_NAMES[day];
    }

    private getMonthYearFormat(month: number, year: number): string {
        return `${MONTH_NAMES[month]} - ${year}`;
    }
}
