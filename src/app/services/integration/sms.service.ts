import { Injectable } from "@angular/core";

import { SmsRetriever } from "@ionic-native/sms-retriever/ngx";

import { UtilService } from "@services/util/util.service";
import { PlatformService } from "@services/util/platform.service";

@Injectable({
    providedIn: "root",
})
export class SmsService {
    constructor(
        private smsRetriever: SmsRetriever,
        private utilService: UtilService,
        private platformService: PlatformService
    ) {}

    async setListener(cb: Function) {
        if (!cb || !this.isEnabled()) {
            return;
        }

        try {
            const res = await this.smsRetriever.startWatching();
            const otp = this.getOtp(res);
            if (otp) {
                cb(otp);
            }
        } catch (_err) {}
    }

    private getOtp(data: any): string {
        const message = this.extractOptMessage(data);
        const otpMatches = message.match(/\d{6}/g);
        return otpMatches ? otpMatches[0] : null;
    }

    private isEnabled() {
        return this.platformService.isAndroid();
    }

    private extractOptMessage(data: any): string {
        const message = this.utilService.getNestedValue(data, "Message");
        return this.utilService.isEmpty(message) ? "" : message;
    }
}
