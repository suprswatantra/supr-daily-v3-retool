import { Injectable } from "@angular/core";

import { ApiInfo, ErrorInfo, Segment } from "@types";

import { RudderStackService } from "./rudder-stack.service";
import { SegmentService } from "./segment.service";
import { UtilService } from "@services/util/util.service";
import { EVENT_TYPES } from "@constants";
import { environment } from "@environments/environment";

@Injectable({
    providedIn: "root",
})
export class AnalyticsService {
    private analyticsConfig = {};

    constructor(
        private rudderStackService: RudderStackService,
        private segmentService: SegmentService,
        private utilService: UtilService
    ) {}

    trackApiEvent(info: ApiInfo) {
        if (this.checkForSegment(EVENT_TYPES.API_EVENT)) {
            this.segmentService.trackApiEvent(info);
        }

        if (this.checkForRudderstack(EVENT_TYPES.API_EVENT)) {
            this.rudderStackService.trackApiEvent(info);
        }
    }

    trackSegmentApiEvent(info: ApiInfo) {
        if (this.checkForSegment(EVENT_TYPES.API_EVENT)) {
            this.segmentService.trackApiEvent(info);
        }
    }

    trackRudderstackApiEvent(info: ApiInfo) {
        if (this.checkForRudderstack(EVENT_TYPES.API_EVENT)) {
            this.rudderStackService.trackApiEvent(info);
        }
    }

    trackError(info: ErrorInfo) {
        if (this.checkForSegment(EVENT_TYPES.ERROR_EVENT)) {
            this.segmentService.trackError(info);
        }

        if (this.checkForRudderstack(EVENT_TYPES.ERROR_EVENT)) {
            this.rudderStackService.trackError(info);
        }
    }

    trackSegmentScreenView(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.SCREEN_VIEW_EVENT)) {
            this.segmentService.trackScreenView(traits);
        }
    }

    trackRudderstackScreenView(traits: Segment.Traits) {
        if (this.checkForRudderstack(EVENT_TYPES.SCREEN_VIEW_EVENT)) {
            this.rudderStackService.trackScreenView(traits);
        }
    }

    trackCustom(eventName: string, traits = {}) {
        this.segmentService.trackCustom(eventName, traits);
        this.rudderStackService.trackCustom(eventName, traits);
    }

    trackClick(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.CLICK_EVENT)) {
            this.segmentService.trackClick(traits);
        }

        if (this.checkForRudderstack(EVENT_TYPES.CLICK_EVENT)) {
            this.rudderStackService.trackClick(traits);
        }
    }

    trackImpression(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.IMPRESSION_EVENT)) {
            this.segmentService.trackImpression(traits);
        }

        if (this.checkForRudderstack(EVENT_TYPES.IMPRESSION_EVENT)) {
            this.rudderStackService.trackImpression(traits);
        }
    }

    trackSegmentImpression(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.IMPRESSION_EVENT)) {
            this.segmentService.trackImpression(traits);
        }
    }

    trackRudderstackImpression(traits: Segment.Traits) {
        if (this.checkForRudderstack(EVENT_TYPES.IMPRESSION_EVENT)) {
            this.rudderStackService.trackImpression(traits);
        }
    }

    clearUser() {
        this.segmentService.clearUser();
        this.rudderStackService.clearUser();
    }

    setUtmParams(params: object = {}) {
        this.segmentService.setUtmParams(params);
        this.rudderStackService.setUtmParams(params);
    }

    trackMoEngageEvent(eventName: string, eventTraits: any = {}) {
        const defaultIntegration = {
            All: false,
            MoEngage: true,
        };

        const integrationConf = this.utilService.getNestedValue(
            this.analyticsConfig,
            "rudderstack.integration_conf",
            defaultIntegration
        );

        this.segmentService.trackMoEngageEvent(eventName, eventTraits);

        this.rudderStackService.trackMoEngageEvent(
            eventName,
            eventTraits,
            integrationConf
        );
    }

    setModalName(modalName: string) {
        this.segmentService.setModalName(modalName);
        this.rudderStackService.setModalName(modalName);
    }

    setSampledEvent(val: boolean) {
        this.segmentService.setSampledEvent(val);
        this.rudderStackService.setSampledEvent(val);
    }

    setActiveExperimentNames(experimentNames: string) {
        this.segmentService.setActiveExperimentNames(experimentNames);
        this.rudderStackService.setActiveExperimentNames(experimentNames);
    }

    trackSwipe(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.SWIPE_EVENT)) {
            this.segmentService.trackSwipe(traits);
        }

        if (this.checkForRudderstack(EVENT_TYPES.SWIPE_EVENT)) {
            this.rudderStackService.trackSwipe(traits);
        }
    }

    trackModalClose(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.MODAL_CLOSE_EVENT)) {
            this.segmentService.trackModalClose(traits);
        }

        if (this.checkForRudderstack(EVENT_TYPES.MODAL_CLOSE_EVENT)) {
            this.rudderStackService.trackModalClose(traits);
        }
    }

    trackModalView(traits: Segment.Traits) {
        if (this.checkForSegment(EVENT_TYPES.MODAL_VIEW_EVENT)) {
            this.segmentService.trackModalView(traits);
        }

        if (this.checkForRudderstack(EVENT_TYPES.MODAL_VIEW_EVENT)) {
            this.rudderStackService.trackModalView(traits);
        }
    }

    setSuprCreditsFlag(isEnabled: boolean) {
        this.segmentService.setSuprCreditsFlag(isEnabled);
        this.rudderStackService.setSuprCreditsFlag(isEnabled);
    }

    setAnalyticsConf(analyticsConfig = {}) {
        this.analyticsConfig = analyticsConfig;
    }

    // private Methods

    private checkForSegment(eventType: string) {
        if (eventType) {
            const eventObj = this.utilService.getNestedValue(
                this.analyticsConfig,
                "segment.event_type",
                {}
            );

            if (this.checkForSegmentVersion()) {
                if (
                    !this.utilService.isEmpty(eventObj) &&
                    eventObj.hasOwnProperty(eventType)
                ) {
                    if (eventObj[eventType]) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        return true;
    }

    private checkForRudderstack(eventType: string) {
        if (eventType) {
            const eventObj = this.utilService.getNestedValue(
                this.analyticsConfig,
                "rudderstack.event_type",
                {}
            );

            if (this.checkForRudderstackVersion()) {
                if (
                    !this.utilService.isEmpty(eventObj) &&
                    eventObj.hasOwnProperty(eventType)
                ) {
                    if (eventObj[eventType]) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        return true;
    }

    private checkForSegmentVersion() {
        const currentVersion = Number(environment.version);

        const minVersion = Number(
            this.utilService.getNestedValue(
                this.analyticsConfig,
                "segment.min_version",
                0
            )
        );

        const maxVersion = Number(
            this.utilService.getNestedValue(
                this.analyticsConfig,
                "segment.max_version",
                0
            )
        );

        if (minVersion && maxVersion) {
            if (currentVersion <= maxVersion && currentVersion >= minVersion) {
                return false;
            }
        } else if (minVersion) {
            if (currentVersion >= minVersion) {
                return false;
            }
        } else if (maxVersion) {
            if (currentVersion <= maxVersion) {
                return false;
            }
        }

        return true;
    }

    private checkForRudderstackVersion() {
        const currentVersion = Number(environment.version);

        const minVersion = Number(
            this.utilService.getNestedValue(
                this.analyticsConfig,
                "rudderstack.min_version",
                0
            )
        );

        const maxVersion = Number(
            this.utilService.getNestedValue(
                this.analyticsConfig,
                "rudderstack.max_version",
                0
            )
        );

        if (minVersion && maxVersion) {
            if (currentVersion <= maxVersion && currentVersion >= minVersion) {
                return false;
            }
        } else if (minVersion) {
            if (currentVersion >= minVersion) {
                return false;
            }
        } else if (maxVersion) {
            if (currentVersion <= maxVersion) {
                return false;
            }
        }

        return true;
    }
}
