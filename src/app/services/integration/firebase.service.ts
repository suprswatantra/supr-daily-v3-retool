import { Injectable } from "@angular/core";

import { FirebaseX } from "@ionic-native/firebase-x/ngx";

import { environment } from "@environments/environment.prod";
import { PlatformService } from "@services/util/platform.service";

@Injectable({
    providedIn: "root",
})
export class FirebaseService {
    constructor(
        private firebase: FirebaseX,
        private platformService: PlatformService
    ) {}

    setUserContext(userId: number) {
        if (this.isEnabled() && userId) {
            this.firebase.setCrashlyticsUserId(userId.toString());
        }
    }

    private isEnabled() {
        return environment.production && this.platformService.isCordova();
    }
}
