import { Injectable } from "@angular/core";

import { environment } from "@environments/environment";

import { AuthService } from "@services/data/auth.service";
import { DateService } from "@services/date/date.service";
import { SettingsService } from "@services/shared/settings.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import {
    FRESHBOT_SCRIPT_V3,
    ANALYTICS_OBJECT_NAMES,
    SETTINGS_KEYS_DEFAULT_VALUE,
    SELF_SERVE,
} from "@constants";

import { FreshbotSetting } from "@types";

import { User } from "@shared/models";

@Injectable({
    providedIn: "root",
})
export class FreshBotService {
    constructor(
        private authService: AuthService,
        private dateService: DateService,
        private settingService: SettingsService,
        private suprPassService: SuprPassService,
        private analyticsService: AnalyticsService
    ) {}

    isFreshbotInitError = false;
    user: User;
    FRESHBOT_CONFIG: FreshbotSetting;
    freshbotScriptLoaded = false;

    private fetchsupportNumber() {
        const supportConfig = this.settingService.getSettingsValue(
            "supportConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig
        );
        return (
            (supportConfig && supportConfig.number) ||
            SETTINGS_KEYS_DEFAULT_VALUE.supportConfig.number
        );
    }

    getFreshbotError() {
        return this.isFreshbotInitError;
    }

    getFreshbotScriptLoaded() {
        return this.freshbotScriptLoaded;
    }

    initialize(user: User) {
        const isBotEnabled = this.fetchConfig();
        this.user = user;
        if (isBotEnabled) {
            this.loadFreshBotScript();
        }
    }

    initBotChat() {
        window.Freshbots.initiateChat();
        window.Freshbots.showWidget(true);
        this.analyticsService.trackImpression({
            objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.FRESHBOT_OPEN,
        });
    }

    /******************************************
     *            Private methods            *
     *****************************************/

    private fetchConfig(): boolean {
        const freshBotConfig = <FreshbotSetting>(
            this.settingService.getSettingsValue(
                "freshbotConfigV3",
                FRESHBOT_SCRIPT_V3
            )
        );
        this.FRESHBOT_CONFIG = freshBotConfig || FRESHBOT_SCRIPT_V3;
        this.analyticsService.trackImpression({
            objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.FRESHBOT_INIT,
            objectValue: `${JSON.stringify(freshBotConfig)}`,
        });
        return freshBotConfig && freshBotConfig.ENABLED;
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }

    private loadFreshBotScript() {
        try {
            if (!this.freshbotScriptLoaded) {
                const {
                    ID,
                    ASYNC,
                    DATA_SELF_INIT,
                    DATA_INIT_TYPE,
                    SRC,
                    DATA_ENV,
                    DATA_REGION,
                } = this.FRESHBOT_CONFIG;
                const selfServeConfig = this.fetchSelfServeConfig();
                var n = document.getElementsByTagName("script")[0],
                    s = document.createElement("script");
                var loaded = false;
                this.freshbotScriptLoaded = true;
                s.id = ID;
                s.async = ASYNC;
                s.setAttribute("data-self-init", DATA_SELF_INIT);
                s.setAttribute("data-init-type", DATA_INIT_TYPE);
                s.src = SRC;
                s.setAttribute("data-client", environment.freshbot.dataClient);
                s.setAttribute(
                    "data-bot-hash",
                    environment.freshbot.dataBotHash
                );
                s.setAttribute(
                    "data-bot-version",
                    selfServeConfig.DATA_BOT_VERSION
                );
                s.setAttribute("data-env", DATA_ENV);
                s.setAttribute("data-region", DATA_REGION);
                s.onload = () => {
                    if (!loaded) {
                        this.loadWidget();
                    }
                    loaded = true;
                };
                n.parentNode.insertBefore(s, n);
            }
        } catch (error) {
            this.analyticsService.trackImpression({
                objectName:
                    ANALYTICS_OBJECT_NAMES.IMPRESSION.FRESHBOT_INIT_ERROR,
                objectValue: `${JSON.stringify(error)}`,
            });
            this.isFreshbotInitError = true;
        }
    }

    private getDates() {
        const today = this.dateService.getTodayDate();
        const todaysDate = this.dateService.formatDate(today);
        const yesterday = this.dateService.getPreviousDateFromString(
            todaysDate
        );
        const previousDate = this.dateService.formatDate(yesterday);
        const tommorrow = this.dateService.getTomorrowDate();
        const tommorrowDate = this.dateService.dateFnsFormatDate(
            tommorrow,
            "d LLL"
        );
        return { todaysDate, previousDate, tommorrowDate };
    }

    private loadWidget() {
        try {
            const token = `Token ${this.authService.getAuthToken()}`;
            const { todaysDate, previousDate, tommorrowDate } = this.getDates();
            const { firstName, lastName, mobileNumber, id } = this.user;
            const name = lastName ? firstName + " " + lastName : firstName;
            const supportNumber = this.fetchsupportNumber();
            const isSuprAccess = this.suprPassService.getIsSuprAccessMember();

            const freshBotScript = document.createElement("script");
            freshBotScript.text = `
        (function(init) {
            init();
        })(function() {
            window.Freshbots.initiateWidget({
                getClientParams: function() {
                    return {
                        "cstmr::xtrInfrmtn:User_Auth":"${token}",
                        "cstmr::nm": "${name}",
                        "cstmr::phn": "${mobileNumber}",
                        "cstmr::xtrInfrmtn:support_number": "${supportNumber}",
                        "cstmr::xtrInfrmtn:supr_access": "${isSuprAccess}",
                        "cstmr::xtrInfrmtn:User_id": "${id}",
                        today_date: "${todaysDate}",
                        "cstmr::xtrInfrmtn:Todays_Date": "${todaysDate}",
                        "cstmr::xtrInfrmtn:Previous_Date": "${previousDate}",
                        Tomorrow_Date: "${tommorrowDate}",
                    };
                },

                 ondComplaintPayload(orders, date, delivery) {
        try {
            const orderData = JSON.parse(orders);
            const { order_history = [], sku_details = [] } =
                orderData && orderData.data;
            const newDelivery = delivery && JSON.parse(delivery) || null;


            const orderArray =
                (order_history &&
                    order_history.length > 0 &&
                    order_history.find(order => {
                        return order.date === date;
                    })) ||
                [];

            let skuObj = {};
            sku_details &&
                sku_details.forEach(sku => {
                    skuObj[sku.id] = sku.sku_name;
                });

            const ordersPayload =
                (orderArray &&
                    orderArray.orders &&
                    orderArray.orders.length > 0 &&
                    orderArray.orders.map(order => {
                    
                        const deliveryStatus =
                (newDelivery &&
                    newDelivery.delivery &&
                    newDelivery.delivery.find(delivery => {
                        return delivery.id === "" + order.id;
                    })) ||
                {};
                        const { status = "", critical = ""} = deliveryStatus;

                        return {
                            order_id: parseInt(order.id),
                            sku_id: parseInt(order.sku_id),
                            sku_name: skuObj[order.sku_id],
                            scheduled_quantity: parseInt(
                                order.delivered_quantity
                            ),
                            service_recovery_quantity: parseInt(
                                order.delivered_quantity
                            ),
                            refund_quantity: 0,
                            delivery_status_when_filed: status,
                            critical,
                            category_id: order && order.category_id
                        };
                    })) ||
                [];
            return {
                orders: ordersPayload || [],
            };
        } catch (error) {
            return { orders: [] };
        }
    },

    fetchEta(eta) {
        try {
			const today = new Date();
            const todayHours = parseInt(today.getHours());
            const etaHour = todayHours + parseInt(eta);

            const timeZone = etaHour > 12 ? "PM" : "AM";
            const formattedHours = etaHour && ((parseInt(etaHour) + 24) % 12 || 12);
		
            return {
                hours: "" + formattedHours + " " + timeZone
            };
        } catch(error) {
            return {
                hours: ""
            };
        }
    },

     timeToEta(date, etaHour) {
            try {
                if (!date) {
                    return "";
                }
                const [dateString = "", time = ""] = date.split("T");
                if (!dateString) {
                    return "";
                }
                const dateArray = dateString.split("-");


                const [year = "", month = "", day = ""] = dateArray;
                const formattedDate = new Date(
                    parseInt(year),
                    parseInt(month) - 1,
                    parseInt(day)
                );


                const complaintPlusOne =  new Date();
                complaintPlusOne.setDate(formattedDate.getDate() + 1);


                const complaintPlusOneDate = parseInt(complaintPlusOne.getDate());
                const complaintPlusOneMonth = parseInt(complaintPlusOne.getMonth()) + 1;
                const complaintPlusOneYear = parseInt(complaintPlusOne.getFullYear());

                const complaintPlusOneShorthMonth = complaintPlusOne.toLocaleString("default", {
                    month: "long",
                });


                const today = new Date();
                const todayDate = parseInt(today.getDate());
                const todayMonth = parseInt(today.getMonth()) + 1;
                const todayYear = parseInt(today.getFullYear());
                const todayHours = parseInt(today.getHours());


                        
                if(todayDate>complaintPlusOneDate || todayMonth > complaintPlusOneMonth || todayYear > complaintPlusOneYear) {
                    return {
                        isETAExpired: true,
                        expiryDate: "" + complaintPlusOneDate + " " + complaintPlusOneShorthMonth,
                    };
                } else if(todayDate==complaintPlusOneDate && todayMonth == complaintPlusOneMonth && todayYear == complaintPlusOneYear) {
                    if(todayHours > etaHour) {
                        return {
                            isETAExpired: true,
                            expiryDate: "" + complaintPlusOneDate + " " + complaintPlusOneShorthMonth,
                        };
                    } else {
                        return {
                            isETAExpired: false,
                            expiryDate: "" + complaintPlusOneDate + " " + complaintPlusOneShorthMonth,

                        };
                    }
                }
                else {
                    return {
                        isETAExpired: false,
                        expiryDate: "" + complaintPlusOneDate + " " + complaintPlusOneShorthMonth,

                    };
                }

            } catch (error) {
                return {
                    isETAExpired: true,
                    expiryDate: "" + complaintPlusOneDate + " " + complaintPlusOneShorthMonth,

                };
            }
        },


         timeToQeqEta(date, etaHour) {
            try {
                if (!date) {
                    return "";
                }
                const [dateString = "", time = ""] = date.split("T");
                if (!dateString) {
                    return "";
                }
                const dateArray = dateString.split("-");
                const [hours = "", minutes = ""] = time.split(":");


                const [year = "", month = "", day = ""] = dateArray;
                const complaintDate = new Date(
                    parseInt(year),
                    parseInt(month) - 1,
                    parseInt(day)
                );

                const complaintDay = parseInt(complaintDate.getDate());
                const complaintMonth = parseInt(complaintDate.getMonth()) + 1;
                const complaintYear = parseInt(complaintDate.getFullYear());
                const complaintHour = parseInt(hours);



                const complaintPlusOne =  new Date();
                complaintPlusOne.setDate(complaintDate.getDate() + 1);


                const complaintPlusOneDate = parseInt(complaintPlusOne.getDate());
                const complaintPlusOneMonth = parseInt(complaintPlusOne.getMonth()) + 1;
                const complaintPlusOneYear = parseInt(complaintPlusOne.getFullYear());

                const complaintPlusOneShorthMonth = complaintPlusOne.toLocaleString("default", {
                    month: "long",
                });


                const today = new Date();
                const todayDate = parseInt(today.getDate());
                const todayMonth = parseInt(today.getMonth()) + 1;
                const todayYear = parseInt(today.getFullYear());
                const todayHours = parseInt(today.getHours());



               //same date

               if(todayDate == complaintDay && todayMonth == complaintMonth && todayYear == complaintYear ) {
                    // complaint created before 6
                        const estimateHours = complaintHour+etaHour;

                    if(complaintHour <18 && todayHours < estimateHours) {
                        const formattedHours = estimateHours && ((parseInt(estimateHours) + 24) % 12 || 12);
                        const timeZone = estimateHours > 12 ? "PM" : "AM";
                        return {
                            status: 0,
                            time: "" + formattedHours + " " + timeZone + " today",
                        };
                    } else if(complaintHour >= 18) {
                        return {
                            status: 1,
                            time: "12PM tomorrow"
                        };
                    } 

                    return {
                        status: 10,
                        time: ""
                    };
                    
                    
               } else if(todayDate == complaintPlusOneDate && todayMonth== complaintPlusOneMonth && todayYear == complaintPlusOneYear) {
                    if(todayHours< 12) {
                        return {
                            status: 2,
                            time: "12PM today"
                        };
                    }
               }

                return {
                    status: 10,
                    time: ""
                };

            } catch (error) {
                return {
                    status: 10,
                    time: ""
                };
            }
        },

    previousComplaintDate(date) {
        try {
            if (!date) {
                return "";
            }
            const [dateString = "", time = ""] = date.split("T");
            if (!dateString || !time) {
                return "";
            }
            const dateArray = dateString.split("-");
            const [hours = "", minutes = ""] = time.split(":");
            const formattedHours = hours && ((parseInt(hours) + 24) % 12 || 12);
            const timeZone = hours > 12 ? "PM" : "AM";
            const [year = "", month = "", day = ""] = dateArray;
            const formattedDate = new Date(
                parseInt(year),
                parseInt(month) - 1,
                parseInt(day)
            );
            const shortMonth = formattedDate.toLocaleString("default", {
                month: "short",
            });
            const weekday = formattedDate.toLocaleDateString("default", {
                weekday: "short",
            });
           
            return {
                        date:
                        "" +
                        weekday +
                        ", " +
                        day +
                        " " +
                        shortMonth +
                        " " +
                        formattedHours +
                        ":" +
                        minutes +
                        " " +
                        timeZone,
                    };
        } catch (error) {
            return { date: "" };
        }
    },

   
    complaintPayload(
        orders,
        id,
        skus,
        service_recovery_quantity,
        delivery,
        missingOrders
    ) {
        try {
            const newOrders = JSON.parse(orders);
            const newSkus = JSON.parse(skus);
            const missingOrdersObject = missingOrders && JSON.parse(missingOrders);
            const newDelivery = delivery && JSON.parse(delivery) || null;
    
    
            if(missingOrdersObject && Object.keys(missingOrdersObject).length > 0) {
            const ordersArray = [];
            for (let [key, value] of Object.entries(missingOrdersObject)) {
            const deliveryStatus =
                (newDelivery &&
                    newDelivery.delivery &&
                    newDelivery.delivery.find(delivery => {
                        return delivery.id === key;
                    })) ||
                {};
            const order =
                (newOrders &&
                    newOrders.orders &&
                    newOrders.orders.find(order => {
                        return order.order_id === key;
                    })) ||
                {};
            const sku =
                (newSkus &&
                    newSkus.skus &&
                    newSkus.skus.find(sku => {
                        return sku.id === order.sku_id;
                    })) ||
                {};

            const { sku_id = "", delivered_quantity = "", category_id=""} = order;
            const { sku_name = "" } = sku;
            const { status = "", critical = ""} = deliveryStatus;

                ordersArray.push({
                        order_id: key && parseInt(key),
                        sku_id: sku_id && parseInt(sku_id),
                        sku_name,
                        scheduled_quantity:
                            delivered_quantity && parseInt(delivered_quantity),
                        service_recovery_quantity:
                            value.compute &&
                            parseInt(value.compute),
                        refund_quantity: 0,
                        delivery_status_when_filed: status,
                        critical,
                        category_id
                    });
            }
            return {
                orders: ordersArray,
            };
            
            } else {
            
            const deliveryStatus =
                (newDelivery &&
                    newDelivery.delivery &&
                    newDelivery.delivery.find(delivery => {
                        return delivery.id === id;
                    })) ||
                {};
            const order =
                (newOrders &&
                    newOrders.orders &&
                    newOrders.orders.find(order => {
                        return order.order_id === id;
                    })) ||
                {};
            const sku =
                (newSkus &&
                    newSkus.skus &&
                    newSkus.skus.find(sku => {
                        return sku.id === order.sku_id;
                    })) ||
                {};

            const { sku_id = "", delivered_quantity = "", category_id="" } = order;
            const { sku_name = "" } = sku;
            const { status = "", critical = ""} = deliveryStatus;

            return {
                orders: [
                    {
                        order_id: id && parseInt(id),
                        sku_id: sku_id && parseInt(sku_id),
                        sku_name,
                        scheduled_quantity:
                            delivered_quantity && parseInt(delivered_quantity),
                        service_recovery_quantity:
                            service_recovery_quantity &&
                            parseInt(service_recovery_quantity),
                        refund_quantity: 0,
                        delivery_status_when_filed: status,
                        critical,
                        category_id
                    },
                ],
            };
      
        }
            
        } catch (error) {
            return { orders: [] };
        }
    },
    
    ticketIdAndTypes(tickets, type) {
        try {
            const newTickets = JSON.parse(tickets);
            const newType = JSON.parse(type);
            const returnTicket =
                (newTickets &&
                    newTickets.tickets &&
                    newTickets.tickets.find(
                        ticket => ticket.type === newType.type
                    )) ||
                {};
            const {
                id = "",
                type: ticketType = "",
                custom_description_text = "",
            } = returnTicket;
            return {
                id,
                type: ticketType,
                custom_description_text,
            };
        } catch (error) {
            return {};
        }
    },

    fetchingQuantity(orders, id) {
        try {
            const newOrders = JSON.parse(orders);
            const order =
                (newOrders &&
                    newOrders.orders &&
                    newOrders.orders.find(order => {
                        return order.order_id === id;
                    })) ||
                {};

            const { delivered_quantity = "" } = order;
            const formatteDeliveryQuantity =
                delivered_quantity && parseInt(delivered_quantity);
            const quantityDropDown =
                Array.from(
                    { length: formatteDeliveryQuantity },
                    (v, k) => {
                        v = k + 1;
                        return v;
                    }
                ) || [];

            return {
                quantity: formatteDeliveryQuantity,
                quantityDropDown,
            };
        } catch (error) {
            return {};
        }
    },

    multiselectLoop: function(data, dataObj, quantitySelected, updatedObj, id, skip) {
            const newOrders = JSON.parse(data);
            let newDataObj = JSON.parse(dataObj);
            const newUpdatedObj = updatedObj && JSON.parse(updatedObj);
        
        if(Object.keys(newUpdatedObj).length > 0) {
            newDataObj = newUpdatedObj;
        }
        let currOrderId = "";
        let display = "";
        let finish = true;
        let fullUrl = "";
        for (let [key, value] of Object.entries(newDataObj)) {
                if(id && quantitySelected && id === key) {
                    newDataObj[key].compute = quantitySelected;
                }
                if(skip ==="skip" && key === id) {
                    value.compute = skip;
                }
            
                if(!value.compute) {
                    currOrderId = key;
                    display = value.display;
                    fullUrl = value.fullUrl
                }
        };
        if(currOrderId) {
            finish = false
        }
        return {
            currOrderId,
            updatedObj: JSON.stringify(newDataObj),
            finish,
            display,
            fullUrl
        };
    },

multiselectConditional: function(dataObj) {
        const newDataObj = JSON.parse(dataObj);
        const ordersLength = Object.keys(newDataObj).length;
        if(ordersLength > 0) {
            for (let [key, value] of Object.entries(newDataObj)) {
                if(value && (value.compute === "skip" || value.compute === "")) {
                    delete newDataObj[key];
                }
            }
        const complaintsLength = Object.keys(newDataObj).length;

        if(complaintsLength > 0) {
                return {
                    isComplaint: true,
                    missingOrders: JSON.stringify(newDataObj),
                    complaintFlow: "complaint",
                }
            } else if (ordersLength > 0 && complaintsLength <=0) {
                return {
                    isComplaint: true,
                    missingOrders: JSON.stringify(newDataObj),
                    complaintFlow: "reselect",
                }
            }
        } 
        return {
            isComplaint: false,
            complaintFlow: "refunded",
        };
    },


    orderSummaryCustomisedButtons(orders, skus, type) {
        try {
            const newOrders = JSON.parse(orders);
            const newSkus = JSON.parse(skus);
            
            
            function dateChecker(year, month, day) {
                    const today = new Date();
                    const todayDate = parseInt(today.getDate());
                    const todayMonth = parseInt(today.getMonth()) + 1;
                    const todayYear = parseInt(today.getFullYear());

                    const yesterday = new Date();
                    yesterday.setDate(yesterday.getDate()-1);
                    const yesterdayDate = parseInt(yesterday.getDate());
                    const yesterdayMonth = parseInt(yesterday.getMonth()) +1;
                    const yesterdayYear = parseInt(yesterday.getFullYear());
                    
                    if(todayYear === year && todayMonth === month && todayDate === day) {
                        return "Today's order";
                    } else if (yesterdayYear === year && yesterdayMonth === month && yesterdayDate === day) {
                        return "Yesterday's order";
                    }
                    return null;
            }

            function formattedDate(date) {
                if (date) {
                    const dateArray = date.split("-");
                    const [year, month, day] = dateArray;
                    const formattedDate = new Date(
                        parseInt(year),
                        parseInt(month) - 1,
                        parseInt(day)
                    );
                    const shortMonth = formattedDate.toLocaleString("default", {
                        month: "short",
                    });
                    const weekday = formattedDate.toLocaleDateString(
                        "default",
                        {
                            weekday: "short",
                        }
                    );
                    
                    
                    const currentDate = dateChecker(
                        parseInt(year),
                        parseInt(month),
                        parseInt(day)
                    );
                    if(currentDate) {
                        return currentDate;
                    }
                    return "" + weekday + ", " + day + " " + shortMonth;
                }
                return "";
            }
            
            let newDataObj = {};

            const modifiedDate =
                newOrders &&
                newOrders.orders &&
                newOrders.orders.map(item => {
                    const sku =
                        (newSkus &&
                            newSkus.skus &&
                            newSkus.skus.find(sku => {
                                return sku.id === item.sku_id;
                            })) ||
                        [];

                    const {
                        date = "",
                        order_id = "",
                        delivered_quantity = 0,
                    } = item;
                    const { sku_name = "" } = sku;
                    
                    if(Math.floor(delivered_quantity) > 0 ) {
                        newDataObj[order_id] = {
                            display:
                                 "" +
                                 sku_name +
                                 " X " +
                                 Math.floor(delivered_quantity),
                            date: date && formattedDate(item.date),
                            compute: "",
                            fullUrl: sku.fullUrl,
                        };
                    }
                    
                    if (type === "orderSummary") {
                        return {
                            value: date,
                            display: date && formattedDate(item.date),
                        };
                    } else {
                        return {
                            value: order_id,
                            fullUrl: sku.fullUrl,
                            display:
                                 "" +
                                 sku_name +
                                 " X " +
                                 Math.floor(delivered_quantity),
                        };
                    }
                });
            return {
                response: modifiedDate.reverse(),
                isOrders: modifiedDate && modifiedDate.length > 0,
                newDataObj: JSON.stringify(newDataObj),
            };
        } catch (error) {
            return { response: [] };
        }
    },
    
    callAction: function(number) {
        document.location.href = "tel: " + number;
        return {
            'message' : 'success'
        };
    },
    
    getDeliveryStatus: function(orders, date, todaysDate) {
            const orderData = JSON.parse(orders);
                   const { order_history = [], sku_details = [] } =
                orderData && orderData.data;
                let orderDate = date;
                if(!orderDate && todaysDate) {
                    orderDate = todaysDate;
                }

            const order =
                (order_history &&
                    order_history.length > 0 &&
                    order_history.find(order => {
                        return order.date === orderDate;
                    })) ||
                [];
                if(order && order.delivery_summary && order.delivery_summary.status) {
                    return {status: order.delivery_summary.status};
                }
                return {status: ""};
            }
            },
            function() {},
            function() {}
        );
        });
        
        `;
            document.head.appendChild(freshBotScript);
        } catch (error) {
            this.analyticsService.trackImpression({
                objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.FRESHBOT_JS_ERROR,
                objectValue: `${JSON.stringify(error)}`,
            });
            this.isFreshbotInitError = true;
        }
    }
}
