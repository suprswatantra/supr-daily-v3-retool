import { Injectable } from "@angular/core";
import {
    Router,
    NavigationEnd,
    ActivatedRoute,
    ActivatedRouteSnapshot,
} from "@angular/router";

import { Network } from "@ionic-native/network/ngx";
import { Device } from "@ionic-enterprise/device/ngx";

import { filter } from "rxjs/operators";

import {
    EVENT_TYPES,
    SCREEN_NAMES,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { environment } from "@environments/environment";

import { User } from "@models";

import { UtilService } from "@services/util/util.service";
import { PlatformService } from "@services/util/platform.service";
import { WalletService } from "@services/shared/wallet.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { UserService } from "@services/shared/user.service";

import { Segment, ApiInfo, ErrorInfo, IdfaInfo } from "@types";

declare const cordova: any;
@Injectable({
    providedIn: "root",
})
export class SegmentService {
    private deviceInfo: any;
    private commonData: Segment.Options = {};
    private currentScreenName = "-";
    private previousScreenName = "-";
    private modalName = "-";
    private advertisingId: string;
    private isSCEnabled: boolean;
    private activeExperimentNames: string;
    private isSampledEvent = true;
    private firedImpressionSet = new Set();
    private impressionThrottleConfig =
        SETTINGS_KEYS_DEFAULT_VALUE.impressionThrottleConfig;
    private sessionId: string;
    private excludeEventsDict = {};
    private utm = {};

    constructor(
        private device: Device,
        private router: Router,
        private network: Network,
        private utilService: UtilService,
        private activatedRoute: ActivatedRoute,
        private platformService: PlatformService,
        private walletService: WalletService,
        private suprPassService: SuprPassService,
        private userService: UserService
    ) {}

    async initialize() {
        if (this.isAllowed()) {
            this.loadSegmentScript();
            this.subscribeForRouterEvents();
            this.setDeviceInfo();
            this.createNewSession();
            await this.setAdvertisingId();
            this.setSegmentCommonOptions();
        }
    }

    setUser(user: User) {
        const userData = this.getSegmentUserData(user);
        this.trackUser(userData);
    }

    setModalName(modalName: string) {
        this.modalName = modalName || "-";
    }

    setSuprCreditsFlag(isEnabled = false) {
        this.isSCEnabled = isEnabled;
    }

    setActiveExperimentNames(experimentNames: string) {
        this.activeExperimentNames = experimentNames;
    }

    setSampledEvent(val: boolean) {
        this.isSampledEvent = val;
    }

    addToFiredImpressionSet(key: string) {
        this.firedImpressionSet.add(key);
    }

    isKeyPresentInFiredImpressionSet(key: string) {
        return this.firedImpressionSet.has(key);
    }

    setImpressionThrottleConfig(
        impressionThrottleConfig = SETTINGS_KEYS_DEFAULT_VALUE.impressionThrottleConfig
    ) {
        this.impressionThrottleConfig = impressionThrottleConfig;
    }

    setExcludeEventsDict(excludedEventsDict: object) {
        this.excludeEventsDict = excludedEventsDict;
    }

    getImpressionThrottleKeyGenerationMethod() {
        return this.impressionThrottleConfig.keyGenerationMethod;
    }

    getImpressionThrottleExcludedEvents() {
        return this.impressionThrottleConfig.excludedEventKeys;
    }

    isImpressionKeyPresentInExcludedEvents(key: string) {
        if (
            !this.utilService.isLengthyArray(
                this.impressionThrottleConfig.excludedEventKeys
            )
        ) {
            return;
        }

        return (
            this.impressionThrottleConfig &&
            this.impressionThrottleConfig.excludedEventKeys.indexOf(key) > -1
        );
    }

    clearUser() {
        this.resetUser();
    }

    trackClick(traits: Segment.Traits) {
        this.track(EVENT_TYPES.CLICK_EVENT, traits);
    }

    trackSwipe(traits: Segment.Traits) {
        this.track(EVENT_TYPES.SWIPE_EVENT, traits);
    }

    trackImpression(traits: Segment.Traits) {
        this.track(EVENT_TYPES.IMPRESSION_EVENT, traits);
    }

    trackScreenView(traits: Segment.Traits) {
        this.track(EVENT_TYPES.SCREEN_VIEW_EVENT, traits);
    }

    trackModalView(traits: Segment.Traits) {
        this.track(EVENT_TYPES.MODAL_VIEW_EVENT, traits);
    }

    trackModalClose(traits: Segment.Traits) {
        this.track(EVENT_TYPES.MODAL_CLOSE_EVENT, traits);
    }

    trackCustom(eventName: string, traits = {}) {
        this.track(eventName, traits);
    }

    trackApiEvent(info: ApiInfo) {
        this.track(EVENT_TYPES.API_EVENT, info);
    }

    trackError(info: ErrorInfo) {
        this.track(EVENT_TYPES.ERROR_EVENT, info);
    }

    trackMoEngageEvent(eventName: string, traitsData: any = {}) {
        const finalTraits = this.getMoengageFinalTraits(traitsData);

        if (window.analytics) {
            window.analytics.track(eventName, finalTraits, {
                ...this.commonData,
                integrations: {
                    All: false,
                    MoEngage: true,
                },
            });
        }
    }

    setUtmParams(params: object = {}) {
        this.utm = params;
    }

    /******************************************
     *            Private methods            *
     *****************************************/

    private track(eventName: string, traitsData: any = {}) {
        if (window.analytics && this.isAllowed()) {
            const traits = this.getFinalTraits(traitsData);
            if (this.isEventAllowed(traits)) {
                window.analytics.track(eventName, this.sanitizeObject(traits), {
                    ...this.commonData,
                    integrations: {
                        MoEngage: false,
                    },
                });
            }
        }
    }

    private isEventAllowed(eventData: any = {}): boolean {
        try {
            let isAllowed = true;
            const { objectName = "" } = eventData;

            /** If objectName exists in exclusion dicts, it "might" not be allowed  */
            if (
                objectName &&
                this.excludeEventsDict &&
                this.excludeEventsDict[objectName]
            ) {
                isAllowed = false;
                const excludeEventObj = this.excludeEventsDict[objectName];

                /** If no further rules are present, event is not allowed  */

                if (
                    !excludeEventObj.allowedTraits &&
                    !excludeEventObj.notAllowedTraits
                ) {
                    return isAllowed;
                }

                /** If any rule matches from this allowedTraits obj, then event is allowed */
                if (excludeEventObj.allowedTraits) {
                    isAllowed = this.allowedTraitsCheckForExcludedEvent(
                        excludeEventObj,
                        eventData
                    );

                    /** If allowed at this point then no need to check further
                     * as allowed rule supersedes everything */
                    if (isAllowed) {
                        return isAllowed;
                    }
                }

                /** If notAllowedTraits obj is present,
                 * then by default event is allowed until one of the rule matches for not allowing.
                 * check notAllowedTraitsCheckForExcludedEvent method called for implementation  */
                if (excludeEventObj.notAllowedTraits) {
                    isAllowed = this.notAllowedTraitsCheckForExcludedEvent(
                        excludeEventObj,
                        eventData
                    );
                }

                return isAllowed;
            }

            return isAllowed;
        } catch (_error) {
            // If something goes wrong in the calculation, allow event
            return true;
        }
    }

    /** allowedTraitsCheckForExcludedEvent && notAllowedTraitsCheckForExcludedEvent have ~90% same code
     * but we are not extracting the logic because of readability */
    private allowedTraitsCheckForExcludedEvent(
        excludeEventObj: any = {},
        eventData: any = {}
    ): boolean {
        let isAllowed = false;
        Object.keys(excludeEventObj.allowedTraits).some((trait) => {
            if (
                this.doesEventHaveTraitAndValueAllowed(
                    eventData,
                    trait,
                    excludeEventObj.allowedTraits[trait]
                )
            ) {
                isAllowed = true;
                return true;
            }
        });
        return isAllowed;
    }

    private notAllowedTraitsCheckForExcludedEvent(
        excludeEventObj: any = {},
        eventData: any = {}
    ): boolean {
        let isAllowed = true;
        Object.keys(excludeEventObj.notAllowedTraits).some((trait) => {
            if (
                this.doesEventHaveTraitAndValueAllowed(
                    eventData,
                    trait,
                    excludeEventObj.notAllowedTraits[trait]
                )
            ) {
                isAllowed = false;
                return true;
            }
        });
        return isAllowed;
    }

    private doesEventHaveTraitAndValueAllowed(
        eventData: any = {},
        trait: string = "",
        allowedTraitValues = []
    ): boolean {
        return (
            eventData.hasOwnProperty(trait) &&
            eventData[trait] &&
            allowedTraitValues.indexOf(eventData[trait]) > -1
        );
    }

    private trackUser(userData: Segment.User) {
        if (window.analytics) {
            window.analytics.identify(
                userData.id,
                {
                    ...this.sanitizeObject(userData),
                    platforms: [{ platform: "ANDROID", active: "true" }],
                },
                this.commonData
            );
        }
    }

    private resetUser() {
        if (window.analytics) {
            window.analytics.reset();
        }
    }

    private trackRoute(snapshot: ActivatedRouteSnapshot) {
        this.setScreenName(snapshot);

        // Get screen impression traits
        let traits: Segment.Traits = {};
        switch (this.currentScreenName) {
            case SCREEN_NAMES.CATEGORY:
                traits = {
                    ...traits,
                    objectValue: this.getCategoryIdFromRouteSnapshot(snapshot),
                };
                break;
            case SCREEN_NAMES.COLLECTION:
                traits = {
                    ...traits,
                    objectValue: this.getCollectionIdFromRouteSnapshot(
                        snapshot
                    ),
                };
                break;
            case SCREEN_NAMES.OFFER_DETAILS:
                traits = {
                    ...traits,
                    objectValue: this.getOfferNameFromRouteSnapshot(snapshot),
                };
                break;
            case SCREEN_NAMES.ADDRESS_FORM:
            case SCREEN_NAMES.ADDRESS_MAP:
            case SCREEN_NAMES.ADDRESS_SEARCH:
            case SCREEN_NAMES.ADDRESS_REVIEW:
                traits = {
                    ...traits,
                    context: this.getAddressTypeFromRouteSnapshot(snapshot),
                };
                break;
        }

        this.trackScreenView(traits);
    }

    private subscribeForRouterEvents() {
        this.router.events
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    // Traverse the active route tree
                    let snapshot = this.activatedRoute.snapshot;
                    let activated = this.activatedRoute.firstChild;

                    while (activated != null) {
                        snapshot = activated.snapshot;
                        activated = activated.firstChild;
                    }

                    // Track the screen view from here
                    this.trackRoute(snapshot);
                }
            });
    }

    private setDeviceInfo() {
        this.deviceInfo = this.device || {};
    }

    private setSegmentCommonOptions() {
        this.commonData = this.sanitizeObject({
            context: {
                device: {
                    id: this.getDeviceId(),
                    advertisingId: this.advertisingId,
                    manufacturer: this.deviceInfo.manufacturer || "",
                    model: this.deviceInfo.model || "",
                    type: this.deviceInfo || "",
                },
                os: {
                    name: this.deviceInfo.platform || "",
                    version: this.deviceInfo.version || "",
                },
                app: {
                    name: environment.name,
                    version: this.platformService.getAppVersion(),
                    namespace: this.platformService.getAppPackageName(),
                },
                /* [TODO] Get device timezone */
                timezone: "Asia/Kolkata",
            },
        });
    }

    private getFinalTraits(traits: Segment.Traits = {}): Segment.Traits {
        return {
            modalName: this.modalName,
            ...traits,
            networkType: this.utilService.getNestedValue(this.network, "type"),
            currentScreen: this.currentScreenName,
            referralScreen: this.previousScreenName,
            isSuprCreditsEnabled: this.isSCEnabled,
            activeExperimentNames: this.activeExperimentNames,
            isSampledEvent: this.isSampledEvent,
            suprCreditsBalance: this.walletService.getSuprCreditsBalance(),
            suprWalletBalance: this.walletService.getWalletBalance(),
            suprAccessMember: this.suprPassService.getIsSuprAccessMember(),
            suprAccessValidity: this.suprPassService.getSuprAccessValidity(),
            ionicPatchVersion: environment.ionic_patch_version,
            ionicBuild: environment.ionic_build,
            sessionId: this.sessionId,
            isAnonymousUser: this.userService.getIsAnonymousUser(),
            sourceType: this.getSourceLinkData("type"),
            sourceUrl: this.getSourceLinkData("url"),
            ...this.utm,
        };
    }

    private getMoengageFinalTraits(traits: any): any {
        const sanitizedTraits = this.sanitizeObject(traits);

        return {
            ...sanitizedTraits,
            isSuprCreditsEnabled: this.isSCEnabled,
            suprCreditsBalance: this.walletService.getSuprCreditsBalance(),
            suprWalletBalance: this.walletService.getWalletBalance(),
            supr_moengage: true,
        };
    }

    private sanitizeObject(params: object): object {
        const newParams = {};

        for (const propName in params) {
            if (
                params.hasOwnProperty(propName) &&
                params[propName] !== null &&
                params[propName] !== undefined
            ) {
                const propValue = params[propName];
                if (typeof propValue === "object") {
                    newParams[propName] = this.sanitizeObject(propValue);
                } else {
                    newParams[propName] = propValue;
                }
            }
        }

        return newParams;
    }

    private getSegmentUserData(user: User): Segment.User {
        const { id, email, firstName, lastName, mobileNumber, address } = user;
        const city = this.utilService.getNestedValue(address, "city.name", "");
        const cityId = this.utilService.getNestedValue(address, "city.id", "");
        return {
            id: id.toString(),
            email,
            firstName,
            lastName,
            phone: mobileNumber,
            city,
            cityId,
        };
    }

    private isAllowed(): boolean {
        return environment.production && this.platformService.isCordova();
    }

    private setScreenName(snapshot: ActivatedRouteSnapshot) {
        const currentScreenName = this.getScreeNameFromRouteSnapshot(snapshot);
        this.previousScreenName = this.currentScreenName;
        this.currentScreenName = currentScreenName;
    }

    private getScreeNameFromRouteSnapshot(
        snapshot: ActivatedRouteSnapshot
    ): string {
        return this.utilService.getNestedValue(snapshot, "data.screenName");
    }

    private getCategoryIdFromRouteSnapshot(
        snapshot: ActivatedRouteSnapshot
    ): string {
        return this.utilService.getNestedValue(snapshot, "params.categoryId");
    }

    private getCollectionIdFromRouteSnapshot(
        snapshot: ActivatedRouteSnapshot
    ): string {
        return this.utilService.getNestedValue(snapshot, "params.viewId");
    }

    private getOfferNameFromRouteSnapshot(
        snapshot: ActivatedRouteSnapshot
    ): string {
        return this.utilService.getNestedValue(snapshot, "params.offerName");
    }

    private getAddressTypeFromRouteSnapshot(
        snapshot: ActivatedRouteSnapshot
    ): string {
        return this.utilService.getNestedValue(
            snapshot,
            "queryParams.selectionType"
        );
    }

    private async setAdvertisingId() {
        try {
            const info = await cordova.plugins.idfa.getInfo();

            if (!info.limitAdTracking) {
                this.advertisingId = this.getPlatformSpecificAdvertisingId(
                    info
                );
            }
        } catch (e) {
            this.advertisingId = "";
        }
    }

    private getPlatformSpecificAdvertisingId(info: IdfaInfo): string {
        if (this.platformService.isIOS()) {
            return info.idfa;
        }

        return info.aaid;
    }

    private getDeviceId(): string {
        const deviceId = this.platformService.getDeviceId();
        return deviceId || (this.deviceInfo && this.deviceInfo.uuid) || "";
    }

    /**
     * Loading the Segment JS script.
     */
    private loadSegmentScript() {
        const segmentScript = document.createElement("script");

        // tslint:disable:max-line-length
        segmentScript.text = `
                !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
                    analytics.load("${environment.analytics.segment}");
                    analytics.page("App Launched");
                }}();`;

        document.head.appendChild(segmentScript);
    }

    private createNewSession() {
        this.firedImpressionSet = new Set();
        this.sessionId = this.createSessionId();
    }

    private createSessionId() {
        return new Date().getTime() + "--" + this.getDeviceId();
    }

    private getSourceLinkData(sourceType = "") {
        if (sourceType === "type") {
            return this.utilService.getNestedValue(
                SETTINGS_KEYS_DEFAULT_VALUE,
                "sourceLink.type",
                null
            );
        } else if (sourceType === "url") {
            return this.utilService.getNestedValue(
                SETTINGS_KEYS_DEFAULT_VALUE,
                "sourceLink.url",
                null
            );
        }
    }
}
