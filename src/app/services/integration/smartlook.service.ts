import { Injectable } from "@angular/core";
// import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";

// import { Platform } from "@ionic/angular";
// import { Device } from "@ionic-enterprise/device/ngx";
// import {
//     Smartlook,
//     SmartlookSetupConfig,
//     SmartlookUserIdentifier,
//     SmartlookCustomEvent,
// } from "@ionic-native/smartlook/ngx";

// import { filter } from "rxjs/operators";

// import { PLATFORM, SMARTLOOK_EVENT_TYPES } from "@constants";
// import { environment } from "@environments/environment";

// import { UtilService } from "@services/util/util.service";
// import { PlatformService } from "@services/util/platform.service";

// import { DeviceMeta } from "@types";

@Injectable({
    providedIn: "root",
})
export class SmartlookService {
    // constructor(
    //     private router: Router,
    //     private device: Device,
    //     private platform: Platform,
    //     private smartlook: Smartlook,
    //     private utilService: UtilService,
    //     private activatedRoute: ActivatedRoute,
    //     private platformService: PlatformService
    // ) {
    //     if (this.isRecordingEnabled()) {
    //         this.subscribeToAppEvents();
    //         this.subscribeToRouterEvents();
    //     }
    // }
    // initialize() {
    //     if (this.isRecordingEnabled()) {
    //         this.setupSmartlook();
    //     }
    // }
    // setUserContext(userId: number) {
    //     if (this.isRecordingEnabled() && userId) {
    //         const deviceData = this.getDeviceData();
    //         this.smartlook.setUserIdentifier(
    //             new SmartlookUserIdentifier(String(userId), deviceData)
    //         );
    //     }
    // }
    // startRecording() {
    //     this.smartlook.startRecording();
    // }
    // stopRecording() {
    //     this.smartlook.stopRecording();
    // }
    // private getDeviceData(): DeviceMeta {
    //     if (!this.device) {
    //         return {};
    //     }
    //     const deviceData = {
    //         uuid: this.device.uuid,
    //         model: this.device.model,
    //         type: this.device.platform,
    //         osName: this.device.platform,
    //         osVersion: this.device.version,
    //         userAgent: navigator.userAgent,
    //         manufacturer: this.device.manufacturer,
    //         appVersion: this.platformService.getAppVersion(),
    //         platform: this.platformService.isIOS()
    //             ? PLATFORM.IOS
    //             : PLATFORM.ANDROID,
    //     };
    //     return deviceData;
    // }
    // private setupSmartlook() {
    //     this.smartlook.setup(
    //         new SmartlookSetupConfig(environment.smartlook.apiKey)
    //     );
    // }
    // private sendScreenViewEvent(url: string, screenName = "") {
    //     this.smartlook.trackCustomEvent(
    //         new SmartlookCustomEvent(SMARTLOOK_EVENT_TYPES.SCREEN_VIEW_EVENT, {
    //             url,
    //             screenName,
    //         })
    //     );
    // }
    // private subscribeToAppEvents() {
    //     this.platform.pause.subscribe(() => {
    //         this.stopRecording();
    //     });
    //     this.platform.resume.subscribe(() => {
    //         this.startRecording();
    //     });
    // }
    // private subscribeToRouterEvents() {
    //     this.router.events
    //         .pipe(filter(event => event instanceof NavigationEnd))
    //         .subscribe((event: NavigationEnd) => {
    //             const screenName = this.getScreenName();
    //             this.sendScreenViewEvent(event.url, screenName);
    //         });
    // }
    // private getScreenName(): string {
    //     // Traverse the active route tree
    //     let snapshot = this.activatedRoute.snapshot;
    //     let activated = this.activatedRoute.firstChild;
    //     while (activated != null) {
    //         snapshot = activated.snapshot;
    //         activated = activated.firstChild;
    //     }
    //     return this.utilService.getNestedValue(snapshot, "data.screenName", "");
    // }
    // private isRecordingEnabled(): boolean {
    //     return environment.production && this.platformService.isCordova();
    // }
}
