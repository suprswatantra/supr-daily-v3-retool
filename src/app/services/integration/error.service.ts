import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import * as Sentry from "@sentry/browser";
import { Device } from "@ionic-enterprise/device/ngx";

import { ERROR_CODES, ERROR_TEXTS, PLATFORM } from "@constants";

import { UtilService } from "@services/util/util.service";
import { StoreService } from "@services/data/store.service";
import { PlatformService } from "@services/util/platform.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { User } from "@models";

import {
    ErrorType,
    DeviceMeta,
    GlobalError,
    SentryFilter,
    SentryLoggerExtraInfo,
} from "@types";

@Injectable({
    providedIn: "root",
})
export class ErrorService {
    private userData: User;
    private deviceData: DeviceMeta;
    private sentryFilters: SentryFilter[] = [];
    private segmentFilters: SentryFilter[] = [];

    constructor(
        private device: Device,
        private utilService: UtilService,
        private storeService: StoreService,
        private platformService: PlatformService,
        private analyticsService: AnalyticsService
    ) {
        this.setDeviceData();
    }

    setContext(userData: User) {
        this.userData = userData;
    }

    // TODO: create message/info logger @manish
    logSentryError(
        error: Error,
        extraInfo: SentryLoggerExtraInfo,
        logToSegment = true
    ) {
        if (!this.platformService.isCordova() || !error) {
            return;
        }

        const logToSentry = this.shouldErrorBeLoggedToSentry(error);
        const _logToSegment =
            logToSegment && this.shouldErrorBeLoggedToSegment(error);

        if (logToSentry) {
            Sentry.withScope((scope) => {
                if (this.userData && this.userData.id) {
                    scope.setUser({
                        ...this.userData,
                        id: this.userData.id.toString(),
                        ...this.deviceData,
                    });
                }

                if (extraInfo && !this.utilService.isEmpty(extraInfo.context)) {
                    scope.setExtras(extraInfo.context);
                }

                if (extraInfo && extraInfo.message) {
                    scope.setExtra("supr_err_msg", extraInfo.message);
                }

                Sentry.captureException(error);
            });
        }

        /* Send to analytics */
        if (_logToSegment) {
            const errorContextForTracking = {
                type: ErrorType.CLIENT,
                name: this.utilService.getNestedValue(error, "name", ""),
                description: this.utilService.getNestedValue(
                    error,
                    "message",
                    ""
                ),
                message: extraInfo && extraInfo.message,
            };

            this.analyticsService.trackError(errorContextForTracking);
        }
    }

    handleNoInternetError() {
        const { NO_INTERNET } = ERROR_TEXTS;

        const errorObj = {
            statusCode: ERROR_CODES.NO_INTERNET,
            title: NO_INTERNET.TITLE,
            subTitle: NO_INTERNET.SUB_TITLE,
            actionText: NO_INTERNET.ACTION_TEXT,
            errorType: ErrorType.NO_INTERNET,
        };

        this.storeService.setGlobalError(errorObj);
    }

    handleTimeOutError(url?: string) {
        const { TIMEOUT } = ERROR_TEXTS;

        const errorObj = {
            statusCode: ERROR_CODES.TIMEOUT_ERROR,
            title: TIMEOUT.TITLE,
            subTitle: TIMEOUT.SUB_TITLE,
            actionText: TIMEOUT.ACTION_TEXT,
            errorType: ErrorType.TIMEOUT,
            errorUrl: url,
        };

        this.storeService.setGlobalError(errorObj);
    }

    handleApiError(error: HttpErrorResponse, url?: string) {
        if (!error || !error.error) {
            return;
        }

        const { API_ERROR } = ERROR_TEXTS;

        const data = error.error;
        const message = this.parseApiError(data);

        const errorObj = {
            statusCode: ERROR_CODES.API_ERROR,
            title: API_ERROR.TITLE,
            subTitle: message || API_ERROR.SUB_TITLE,
            actionText: API_ERROR.ACTION_TEXT,
            errorType: ErrorType.API,
            errorUrl: url,
        };

        this.storeService.setGlobalError(errorObj);
    }

    handleGenericError(url?: string, errObj?: Partial<GlobalError>) {
        const { GENERIC_ERROR } = ERROR_TEXTS;

        let errorObj = {
            statusCode: ERROR_CODES.GENERIC_ERROR,
            title: GENERIC_ERROR.TITLE,
            subTitle: GENERIC_ERROR.SUB_TITLE,
            actionText: GENERIC_ERROR.ACTION_TEXT,
            errorType: ErrorType.API_GENERIC,
            errorUrl: url,
        };

        if (errObj) {
            errorObj = {
                ...errorObj,
                ...errObj,
            };
        }

        this.storeService.setGlobalError(errorObj);

        /* Log the error at Sentry */
        /* this.logSentryError(
            new Error(JSON.stringify(error.error)),
            `${error.status}`
        ); */
    }

    handleCustomError(error: GlobalError) {
        const { GENERIC_ERROR } = ERROR_TEXTS;

        const errorObj = {
            statusCode: ERROR_CODES.GENERIC_ERROR,
            title: error.title || GENERIC_ERROR.TITLE,
            subTitle: error.subTitle || GENERIC_ERROR.SUB_TITLE,
            actionText: error.actionText || GENERIC_ERROR.ACTION_TEXT,
            errorType: error.errorType || ErrorType.CLIENT,
        };

        this.storeService.setGlobalError(errorObj);
    }

    setSentryFiltersConfig(config: SentryFilter[]) {
        this.sentryFilters = config;
    }

    setSegmentFiltersConfig(config: SentryFilter[]) {
        this.segmentFilters = config;
    }

    private getSentryFiltersConfig(): SentryFilter[] {
        return this.sentryFilters;
    }

    private getSegmentFiltersConfig(): SentryFilter[] {
        return this.segmentFilters;
    }

    private parseApiError(data: any): string {
        let result = "";

        if (data && data.statusMessage) {
            return data.statusMessage;
        }

        Object.keys(data).forEach((key) => {
            const prop = data[key];
            if (this.utilService.isLengthyArray(prop)) {
                prop.forEach((message: string) => {
                    const messageArr = message
                        .toLowerCase()
                        .split(".")
                        .filter((msg) => msg !== "");
                    const capsMsgArr = messageArr.map((msg) =>
                        this.utilService.capitalizeFirstLetter(msg.trim())
                    );
                    const finalMessage = capsMsgArr.join(". ") + ".";
                    result += finalMessage;
                });
            }
        });

        return result;
    }

    private setDeviceData() {
        if (this.platformService.isCordova() && this.device) {
            this.deviceData = {
                uuid: this.device.uuid,
                manufacturer: this.device.manufacturer,
                model: this.device.model,
                type: this.device.platform,
                osName: this.device.platform,
                osVersion: this.device.version,
                platform: this.platformService.isIOS()
                    ? PLATFORM.IOS
                    : PLATFORM.ANDROID,
                userAgent: navigator.userAgent,
                appVersion: this.platformService.getAppVersion(),
            };
        } else {
            this.deviceData = {};
        }
    }

    private getMatchingErrorFilter(
        error: Error,
        sentryFilters: SentryFilter[]
    ): SentryFilter {
        if (
            !this.utilService.getNestedValue(error, "message") ||
            !this.utilService.isLengthyArray(sentryFilters)
        ) {
            return;
        }

        return sentryFilters.find((filter: SentryFilter) => {
            /* no match if filter has missing fields */
            if (
                this.utilService.isEmpty(filter) ||
                !filter.pattern ||
                isNaN(filter.allowedPercentage)
            ) {
                return;
            }

            /* check for pattern match if filter has regexMatch enabled */
            if (filter.regexMatch) {
                const regex = new RegExp(filter.pattern, "gi");

                return error.message.match(regex);
            }

            /* default the check to a sub-string check */
            return error.message.indexOf(filter.pattern) >= 0;
        });
    }

    /* return a random number between 0 to 100 */
    private getRandomNumber(): number {
        return Math.random() * 100;
    }

    private shouldErrorBeLoggedToSentry(error: Error): boolean {
        const sentryFilters = this.getSentryFiltersConfig();
        const matchedFilter = this.getMatchingErrorFilter(error, sentryFilters);

        return this.shouldAllowEvent(matchedFilter);
    }

    private shouldErrorBeLoggedToSegment(error: Error): boolean {
        const segmentFilters = this.getSegmentFiltersConfig();
        const matchedFilter = this.getMatchingErrorFilter(
            error,
            segmentFilters
        );

        return this.shouldAllowEvent(matchedFilter);
    }

    private shouldAllowEvent(matchedFilter: SentryFilter): boolean {
        if (!matchedFilter) {
            return true;
        }

        const randomNum = this.getRandomNumber();

        const allowedPercentage = this.utilService.getNestedValue(
            matchedFilter,
            "allowedPercentage"
        );

        return matchedFilter && allowedPercentage > randomNum;
    }
}
