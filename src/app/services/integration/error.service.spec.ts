import { TestBed } from "@angular/core/testing";

import { Device } from "@ionic-enterprise/device/ngx";

import { ErrorService } from "./error.service";
import { UtilService } from "@services/util/util.service";
import { StoreService } from "@services/data/store.service";
import { PlatformService } from "@services/util/platform.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SentryFilter } from "@types";

import {
    BaseUnitTestProvidersIonic,
    BaseUnitTestImportsWithStoreIonic,
} from "./../../../unitTest.conf";

describe("ErrorService", () => {
    let device: Device;
    let utilService: UtilService;
    let errorService: ErrorService;
    let storeService: StoreService;
    let platformService: PlatformService;
    let analyticsService: AnalyticsService;

    const sampleErrorToBeLogged: any = {
        message:
            "Unexpected Error. This is a sample event that should be logged to Sentry",
    };

    const sampleErrorToBeThrottled: any = {
        message:
            "Unexpected Error. This is a sample event that should be throttled at 10%",
    };

    const sentryStringFilters: SentryFilter[] = [
        {
            allowedPercentage: 10,
            pattern: "should be throttled",
        },
    ];

    const sentryRegexFilters: SentryFilter[] = [
        {
            regexMatch: true,
            allowedPercentage: 10,
            pattern: "should be throttled",
        },
    ];

    const invalidSentryFilters: SentryFilter[] = [
        {
            pattern: undefined,
            allowedPercentage: undefined,
        },
    ];

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithStoreIonic],

            providers: [
                Device,
                UtilService,
                StoreService,
                PlatformService,
                AnalyticsService,

                ...BaseUnitTestProvidersIonic,
            ],
        });

        device = TestBed.get(Device);
        utilService = TestBed.get(UtilService);
        storeService = TestBed.get(StoreService);
        platformService = TestBed.get(PlatformService);
        analyticsService = TestBed.get(AnalyticsService);

        errorService = new ErrorService(
            device,
            utilService,
            storeService,
            platformService,
            analyticsService
        );
    });

    it("#should create error service instance", () => {
        expect(errorService).toBeTruthy();
    });

    it("#should return undefined when error message is undefined or isn't a string or is an empty string", () => {
        /* error message is an empty string */
        expect(
            (errorService as any).getMatchingErrorFilter(
                { message: "" } as Error,
                sentryStringFilters
            )
        ).toEqual(undefined);

        /* error message is undefined */
        expect(
            (errorService as any).getMatchingErrorFilter(
                { message: undefined } as Error,
                sentryStringFilters
            )
        ).toEqual(undefined);

        /* error is undefined */
        expect(
            (errorService as any).getMatchingErrorFilter(
                undefined,
                sentryStringFilters
            )
        ).toEqual(undefined);
    });

    it("#should return undefined when filters are undefined or invalid", () => {
        /* no filters have been defined */
        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeThrottled,
                undefined
            )
        ).toEqual(undefined);

        /* filters is an empty array */
        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeThrottled,
                []
            )
        ).toEqual(undefined);

        /* filter has missing mandatory fields */
        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeThrottled,
                invalidSentryFilters
            )
        ).toEqual(undefined);
    });

    it("#should return the matching filter based on string/regex match; else it should return undefined", () => {
        /* error message doesn't match any filter patterns */
        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeLogged,
                sentryStringFilters
            )
        ).toEqual(undefined);

        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeLogged,
                sentryRegexFilters
            )
        ).toEqual(undefined);

        /* error message matches one of the filter patterns */
        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeThrottled,
                sentryStringFilters
            )
        ).toBe(sentryStringFilters[0]);

        expect(
            (errorService as any).getMatchingErrorFilter(
                sampleErrorToBeThrottled,
                sentryRegexFilters
            )
        ).toBe(sentryRegexFilters[0]);
    });

    it("#should return true when none of the string filters match", () => {
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeLogged,
                sentryStringFilters
            )
        ).toEqual(true);
    });

    it("#should return true when none of the regex filters match", () => {
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryRegexFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeLogged,
                sentryRegexFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return true when there are matching string filters and allowed percentage is greater than generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(8);
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeThrottled,
                sentryStringFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return true when there are matching regex filters and allowed percentage is greater than generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(8);
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeThrottled,
                sentryRegexFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return false when there are matching string filters but allowed percentage is less than/equals to generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(15);
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeThrottled,
                sentryStringFilters
            )
        ).toEqual(false);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return false when there are matching regex filters but allowed percentage is less than/equals to generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(15);
        spyOn(errorService as any, "getSentryFiltersConfig").and.returnValue(
            sentryRegexFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSentry(
                sampleErrorToBeThrottled,
                sentryRegexFilters
            )
        ).toEqual(false);
    });

    it("#should return true when none of the string filters match", () => {
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeLogged,
                sentryStringFilters
            )
        ).toEqual(true);
    });

    it("#should return true when none of the regex filters match", () => {
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryRegexFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeLogged,
                sentryRegexFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return true when there are matching string filters and allowed percentage is greater than generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(8);
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeThrottled,
                sentryStringFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return true when there are matching regex filters and allowed percentage is greater than generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(8);
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeThrottled,
                sentryRegexFilters
            )
        ).toEqual(true);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return false when there are matching string filters but allowed percentage is less than/equals to generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(15);
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryStringFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeThrottled,
                sentryStringFilters
            )
        ).toEqual(false);
    });

    // tslint:disable-next-line: max-line-length
    it("#should return false when there are matching regex filters but allowed percentage is less than/equals to generated random number", () => {
        spyOn(errorService as any, "getRandomNumber").and.returnValue(15);
        spyOn(errorService as any, "getSegmentFiltersConfig").and.returnValue(
            sentryRegexFilters
        );

        expect(
            (errorService as any).shouldErrorBeLoggedToSegment(
                sampleErrorToBeThrottled,
                sentryRegexFilters
            )
        ).toEqual(false);
    });
});
