import { Injectable } from "@angular/core";

import { Push, PushObject, PushOptions } from "@ionic-native/push/ngx";

import { Subscription } from "rxjs";
import { finalize } from "rxjs/operators";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { environment } from "@environments/environment";

import { PlatformService } from "@services/util/platform.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

import { User } from "@models";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { DbService } from "@services/data/db.service";

import { SelfServeStore } from "@store/self-serve";
import { FreshchatOptions } from "@types";
import { LOCAL_DB_DATA_KEYS } from "@constants";

@Injectable({
    providedIn: "root",
})
export class FreshChatService {
    constructor(
        private platformService: PlatformService,
        private dbService: DbService,
        private callNumber: CallNumber,
        private routerService: RouterService,
        private suprPassService: SuprPassService,
        public selfServeStore: SelfServeStore,
        private dateService: DateService,
        private utilService: UtilService,
        private push: Push
    ) {}

    chatRoutingData: any;
    subscription$: Subscription;

    /* Initialize FreshChat */
    initialize() {
        if (!this.platformService.isCordova()) {
            return;
        }
        const options = <FreshchatOptions>{
            appId: environment.freshChat.appId,
            appKey: environment.freshChat.appKey,
            gallerySelectionEnabled: true,
            cameraCaptureEnabled: false,
            teamMemberInfoVisible: false,
        };
        window.Freshchat.init(options);
        this.handlePushNotifications();
    }

    setChatRouting() {
        if (
            !this.chatRoutingData ||
            this.utilService.isEmpty(this.chatRoutingData)
        ) {
            this.subscription$ = this.selfServeStore
                .getChatRoutingData()
                .pipe(
                    finalize(() => {
                        this.unsubscribeApi();
                    })
                )
                .subscribe((data: any) => {
                    this.chatRoutingData = data;
                });
        }
    }

    openConversations() {
        if (this.platformService.isCordova()) {
            this.setConversationDetails();
            window.Freshchat.showConversations({
                tags: ["inbox"],
                filteredViewTitle: "SuprDaily Support",
            });
        }
    }

    clearUserData() {
        if (this.platformService.isCordova()) {
            window.Freshchat.clearUserData();
        }
    }

    setUserInfo(user: User) {
        if (!this.platformService.isCordova()) {
            return;
        }
        const isSuprAccess = this.suprPassService.getIsSuprAccessMember();

        /* Set External ID */
        window.Freshchat.identifyUser({
            externalId: user.id.toString(),
            restoreId: null,
        });

        /* Update User Data */
        const { firstName, lastName, email, mobileNumber } = user;
        let userData = {
            firstName: firstName,
            lastName: lastName,
            email,
            phoneNumber: mobileNumber,
            supr_access: isSuprAccess,
        };
        if (this.chatRoutingData) {
            userData = {
                ...userData,
                ...this.chatRoutingData,
            };
        }

        window.Freshchat.updateUser(userData);
        window.Freshchat.updateUserProperties(userData);
    }

    getRestoreId(cb: (rId: string) => void) {
        if (!this.platformService.isCordova() || !cb) {
            return;
        }

        window.Freshchat.getRestoreID((success: boolean, restoreId: string) => {
            if (success && restoreId !== "null") {
                cb(restoreId);
            } else {
                cb("");
            }
        });
    }

    restoreUser(userId: string, restoreId: string, cb?: () => void) {
        if (!this.platformService.isCordova() || !cb) {
            return;
        }

        window.Freshchat.identifyUser(
            {
                externalId: userId,
                restoreId,
            },
            (success: boolean) => {
                if (success) {
                    cb();
                }
            }
        );
    }

    async openChat(user) {
        try {
            // 1. Get restore id from local db
            const _restoreId = await this.getRestoreIdFromDb();
            if (_restoreId) {
                this.restoreChatUser(_restoreId, user);
            } else {
                // 2. Fetch restore id from Freshchat
                this.getRestoreId((restoreId: string) => {
                    if (restoreId) {
                        // 3. Save restore id in DB and restore User for previous chats
                        this.setUserInfo(user);

                        this.setRestoreIdInDB(restoreId);
                        this.restoreChatUser(restoreId, user);
                    } else {
                        // 4. Save user info at Freshchat for First time user
                        this.setUserInfo(user);
                        // 5. Open chat window
                        this.openConversations();
                    }
                });
            }
        } catch (err) {}
    }

    sendMessage(complaintId: number) {
        if (complaintId) {
            const message = `
                Please wait while we connect you to our customer support executive, rest assured our executive 
                will refer to your issue ID #${complaintId} for details and help resolve as soon as possible`;
            this.sendFreshchatMessage(message);
        }
    }

    openDialer(phoneNo: string) {
        this.callNumber.callNumber(phoneNo, false).catch(() => {
            // Open the dialer - this doesn't need to seek any permission
            window.location.href = `tel:${phoneNo}`;
        });
    }

    callActionButton(number: string) {
        this.openDialer(number);
    }

    chatActionButton(date: string, slot: string) {
        this.routerService.goToSelfServePage(
            {
                state: {
                    slot,
                },
            },
            date
        );
    }

    sendFreshchatMessage(message: string) {
        if (this.platformService.isCordova() && window.Freshchat && message) {
            setTimeout(() => {
                window.Freshchat.sendMessage({
                    tag: "premium",
                    message,
                });
            }, 2000);
        }
    }

    async isResumeChat(limit: number) {
        try {
            const conversationDetails = await this.getConversationDetails();
            if (conversationDetails && conversationDetails.date && limit) {
                const formatDate = new Date(conversationDetails.date);
                const minutes = this.dateService.minutesToToday(formatDate);
                if (minutes < limit) {
                    return true;
                }
            }
            return false;
        } catch (err) {
            return false;
        }
    }

    async getChatCount() {
        try {
            return await this.getFreshchatUnreadCount();
        } catch (err) {
            return 0;
        }
    }

    private getFreshchatUnreadCount(): Promise<number> {
        return new Promise((resolve, reject) => {
            if (!this.platformService.isCordova()) {
                return;
            }
            window.Freshchat.unreadCount(function (success, val) {
                if (success) {
                    resolve(val);
                } else {
                    reject(0);
                }
            });
        });
    }

    private handlePushNotifications() {
        if (!this.platformService.isCordova()) {
            return;
        }
        const options: PushOptions = {
            android: {},
            ios: {
                alert: "true",
                badge: true,
                sound: "false",
            },
            windows: {},
            browser: {
                pushServiceURL: "http://push.api.phonegap.com/v1/push",
            },
        };

        const pushObject: PushObject = this.push.init(options);

        pushObject.on("notification").subscribe((notification: any) => {
            window.Freshchat.isFreshchatPushNotification(
                notification,
                function (success, isFreshchatNotif) {
                    if (success && isFreshchatNotif) {
                        window.Freshchat.handlePushNotification(
                            notification.additionalData
                        );
                    }
                }
            );
        });

        pushObject.on("registration").subscribe((registration: any) => {
            window.Freshchat.updatePushNotificationToken(
                registration.registrationId
            );
        });

        pushObject
            .on("error")
            .subscribe((error) =>
                console.error("Error with Push plugin", error)
            );
    }

    private restoreChatUser(restoreId: string, user: User) {
        this.restoreUser(user.id.toString(), restoreId, () => {
            this.openConversations();
        });
    }

    private setRestoreIdInDB(restoreId: string) {
        try {
            this.dbService.setData(
                LOCAL_DB_DATA_KEYS.FRESH_CHAT_RESTORE_ID_KEY,
                restoreId
            );
        } catch (err) {}
    }

    private getRestoreIdFromDb(): Promise<string> {
        return this.dbService.getData(
            LOCAL_DB_DATA_KEYS.FRESH_CHAT_RESTORE_ID_KEY
        );
    }

    private setConversationDetails() {
        try {
            this.dbService.setData(
                LOCAL_DB_DATA_KEYS.FRESH_CHAT_CONVERSATION_DETAILS,
                {
                    date: new Date(),
                }
            );
        } catch (err) {}
    }

    private getConversationDetails(): Promise<any> {
        return this.dbService.getData(
            LOCAL_DB_DATA_KEYS.FRESH_CHAT_CONVERSATION_DETAILS
        );
    }

    private unsubscribeApi() {
        if (this.subscription$) {
            this.subscription$.unsubscribe();
            this.subscription$ = null;
        }
    }
}
