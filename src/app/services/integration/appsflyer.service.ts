import { Injectable } from "@angular/core";

import { PAGE_ROUTES, CURRENCY, APPS_FLYER_EVENTS } from "@constants";

import { environment } from "@environments/environment";

import { Sku, CartItem } from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { StoreService } from "@services/data/store.service";
import { PlatformService } from "@services/util/platform.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { AppsFlyerOptions, AfTrait, CartMiniItem } from "@types";

@Injectable({
    providedIn: "root",
})
export class AppsFlyerService {
    private serviceEnabled = false;

    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private storeService: StoreService,
        private platformService: PlatformService,
        private scheduleService: ScheduleService,
        private analyticsService: AnalyticsService
    ) {
        this.setIsEnabled();
    }

    /* Initialize AppsFlyer */
    initialize() {
        this.serviceEnabled =
            this.serviceEnabled && window.plugins && window.plugins.appsFlyer;

        if (!this.serviceEnabled) {
            return;
        }

        const options = <AppsFlyerOptions>{
            devKey: environment.appsFlyer.key,
            onInstallConversionDataListener: true,
        };

        if (this.platformService.isIOS()) {
            options.appId = environment.ios.id.toString();
        }

        window.plugins.appsFlyer.initSdk(
            options,
            (successRes) => {
                try {
                    const { data } = JSON.parse(successRes);
                    if (data) {
                        this.setPnData(data);
                    }
                } catch (_error) {
                    console.log(_error);
                }
            },
            (errorRes) => {
                console.log(errorRes);
            }
        );

        // https://github.com/AppsFlyerSDK/appsflyer-cordova-plugin/blob/master/docs/Guides.md#deeplinking
        window.plugins.appsFlyer.registerOnAppOpenAttribution(
            (res) => {
                try {
                    const { data } = JSON.parse(res);
                    if (data) {
                        this.setPnData(data);
                    }
                } catch (_error) {
                    console.log(_error);
                }
            },
            (err) => console.log(err)
        );
    }

    setAppUserId(userId: number) {
        if (this.serviceEnabled) {
            window.plugins.appsFlyer.setAppUserId(userId.toString());
        }
    }

    /** Track Events for appsFlyer */
    trackRegistration(mobileNumber?: string) {
        this.trackEvent(APPS_FLYER_EVENTS.COMPLETE_REGISTRATION, {
            af_registration_method: mobileNumber || null,
        });
    }

    trackInitiatedCheckout(cartMiniItems: CartMiniItem[]) {
        this.trackEvent(
            APPS_FLYER_EVENTS.INITIATED_CHECKOUT,
            this.getInitiatedCheckoutTraits(cartMiniItems)
        );
    }

    trackAddToCart(sku: Sku) {
        this.trackEvent(APPS_FLYER_EVENTS.ADD_TO_CART, this.getCartTraits(sku));
    }

    trackRemoveFromCart(sku: Sku) {
        this.trackEvent(
            APPS_FLYER_EVENTS.REMOVE_FROM_CART,
            this.getCartTraits(sku)
        );
    }

    trackPurchase(cartItems: CartItem[]) {
        this.trackEvent(
            APPS_FLYER_EVENTS.PURCHASE,
            this.getPurchaseTraits(cartItems)
        );
    }

    trackFirstOrder(cartItems: CartItem[]) {
        this.trackEvent(
            APPS_FLYER_EVENTS.FIRST_ORDER,
            this.getPurchaseTraits(cartItems)
        );
    }

    trackCatalogListView(catalogId: number, catalogName: string) {
        this.trackEvent(APPS_FLYER_EVENTS.LIST_VIEW, {
            af_content_id: catalogId,
            af_content_type: catalogName,
        });
    }

    trackSkuImpression(sku: Sku) {
        this.trackEvent(APPS_FLYER_EVENTS.CONTENT_VIEW, {
            af_content_id: sku.id,
            af_content_type: sku.sku_name,
            af_product_identifier: sku.product_identifier,
        });
    }

    private setPnData(data: any) {
        const newUrl = this.getPNurl(data);

        this.setReferralData(data);
        this.setScheduleData(data);

        if (newUrl) {
            this.storeService.setPNData({
                pnType: "deeplink",
                url: newUrl,
                source: "onelink",
                skipLoginCheck: data.skipLoginCheck,
            });
            /* set utm parameters if any */
            this.filterUtmParamsDataFromUrl(newUrl);
        }
    }

    /** Private methods */
    private trackEvent(eventName: string, eventValues = {}) {
        if (!this.serviceEnabled) {
            return;
        }

        window.plugins.appsFlyer.trackEvent(eventName, eventValues);
    }

    private getPurchaseTraits(cartItems: CartItem[]): AfTrait {
        return {
            af_content_id: this.pickDataFromCartItems("sku_id", cartItems),
            af_content_type: this.pickDataFromCartItems("type", cartItems),
            af_quantity: this.pickDataFromCartItems("quantity", cartItems),
            af_coupon_code: this.pickDataFromCartItems(
                "coupon_code",
                cartItems
            ).join(","),
            af_price: this.getPriceItems("pre_discount", cartItems),
            af_revenue: this.getPriceItems("post_discount", cartItems),
            af_currency: CURRENCY.INR,
        };
    }

    private getInitiatedCheckoutTraits(cartItems: CartMiniItem[]): AfTrait {
        return {
            af_content_id: this.pickDataFromMiniCartItems("sku_id", cartItems),
            af_content_type: this.pickDataFromMiniCartItems("type", cartItems),
            af_quantity: this.pickDataFromMiniCartItems("quantity", cartItems),
            af_price: this.getPriceMiniItems(cartItems),
            af_currency: CURRENCY.INR,
        };
    }

    private getCartTraits(sku: Sku): AfTrait {
        return {
            af_price: sku.unit_price,
            af_content_id: sku.id,
            af_content_type: sku.sku_name,
            af_currency: CURRENCY.INR,
            af_product_identifier: sku.product_identifier,
        };
    }

    private pickDataFromCartItems(key: string, cartItems: CartItem[]) {
        return cartItems.map((cartItem) => cartItem[key]);
    }

    private getPriceItems(key: string, cartItems: CartItem[]) {
        return cartItems.map((cartItem) => cartItem.payment_data[key]);
    }

    private pickDataFromMiniCartItems(key: string, cartItems: CartMiniItem[]) {
        return cartItems.map((cartItem) => cartItem.item[key]);
    }

    private getPriceMiniItems(cartItems: CartMiniItem[]) {
        return cartItems.map((cartItem) =>
            this.utilService.getNestedValue(cartItem, "sku.unit_price")
        );
    }

    private setIsEnabled() {
        this.serviceEnabled =
            environment.production && this.platformService.isCordova();
    }

    private setUtmParams(data: {}) {
        if (this.utilService.isEmpty(data)) {
            return;
        }

        const utmParams = {
            utm_source: this.utilService.getNestedValue(
                data,
                "utm_source",
                null
            ),

            utm_medium: this.utilService.getNestedValue(
                data,
                "utm_medium",
                null
            ),

            utm_campaign: this.utilService.getNestedValue(
                data,
                "utm_campaign",
                null
            ),

            utm_term: this.utilService.getNestedValue(data, "utm_term", null),

            utm_content: this.utilService.getNestedValue(
                data,
                "utm_content",
                null
            ),
        };

        this.analyticsService.setUtmParams(utmParams);
    }

    private getUtmParams(url: string = ""): {} {
        // ** Sample url **
        // category/1&utm_source=2&utm_medium=swaa&dfddfd=2232

        const utmData = url
            .split("&")
            .filter((dt) => {
                if (dt.includes("utm_")) {
                    return dt;
                }
            })
            .map((dt) => {
                const d = dt.split("=");
                if (d.length === 2) {
                    const res = {};
                    res[d[0]] = d[1];
                    return res;
                }
            });

        return utmData;
    }

    private filterUtmParamsDataFromUrl(url: string = "") {
        if (this.utilService.isEmpty(url)) {
            return;
        }

        const utmsData = this.getUtmParams(url);
        this.setUtmParams(utmsData);
    }

    private setReferralData(data: any) {
        if (data.referralCode) {
            this.storeService.setReferralCode(data.referralCode);
        }
    }

    private getPNurl(data: any): string {
        if (!data.url) {
            return;
        }

        const url: string = data.url.replaceAll("--", "/");

        if (
            url === PAGE_ROUTES.SUBSCRIPTION_DETAILS_V2.BASE &&
            data.subscriptionId
        ) {
            /* redirection to subscription details page */
            return `${PAGE_ROUTES.SUBSCRIPTION_DETAILS_V2.BASE}/${data.subscriptionId}`;
        } else if (
            /* redirection to subscription recharge page */
            url === PAGE_ROUTES.SUBSCRIPTION_RECHARGE.BASE &&
            data.subscriptionId
        ) {
            return `${PAGE_ROUTES.SUBSCRIPTION_RECHARGE.BASE}/${data.subscriptionId}`;
        }

        return url;
    }

    private setScheduleData(data: any) {
        if (data && this.dateService.isValidDateString(data.scrollToDate)) {
            const scrollToDateObject = this.dateService.dateFromText(
                data.scrollToDate
            );
            const startDateObject = this.dateService.startDateOfWeek(
                scrollToDateObject
            );
            const startDate = this.dateService.textFromDate(startDateObject);

            this.scheduleService.setCustomScheduleStartDate(startDate);
            this.scheduleService.setCustomScheduleScrollDate(data.scrollToDate);
        }
    }
}
