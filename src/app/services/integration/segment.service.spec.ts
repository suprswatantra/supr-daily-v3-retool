import { TestBed } from "@angular/core/testing";
import { Router, ActivatedRoute } from "@angular/router";
import { Device } from "@ionic-enterprise/device/ngx";
import { Network } from "@ionic-native/network/ngx";

import { SegmentService } from "./segment.service";

import { UtilService } from "@services/util/util.service";
import { PlatformService } from "@services/util/platform.service";
import { WalletService } from "@services/shared/wallet.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { UserService } from "@services/shared/user.service";

import {
    BaseUnitTestImportsIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class DeviceMock {}
class RouterMock {}
class ActivatedRouteMock {}
class NetwrokMock {}

describe("SegmentService", () => {
    let segmentService: SegmentService;

    let utilService: UtilService;
    let platformService: PlatformService;
    let walletService: WalletService;
    let suprPassService: SuprPassService;
    let userService: UserService;
    let device: Device;
    let network: Network;
    let router: Router;
    let activatedRoute: ActivatedRoute;

    let spy: any;

    console.log(spy);

    const sampleEventDataForExclusionTest = {
        activeExperimentNames:
            "freshbotConfig,enableSmartlook,sampledEvent,searchBoxPlaceholder",
        context: 1,
        currentScreen: "category",
        ionicBuild: false,
        ionicPatchVersion: 1,
        isAnonymousUser: false,
        isSampledEvent: false,
        isSuprCreditsEnabled: true,
        modalName: "-",
        networkType: null,
        objectName: "product-viewed",
        objectValue: 2659,
        position: undefined,
        referralScreen: "-",
        sessionId: undefined,
        suprAccessMember: false,
        suprAccessValidity: 0,
        suprCreditsBalance: 100,
        suprWalletBalance: 3789,
    };

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsIonic],

            providers: [
                UtilService,
                PlatformService,
                WalletService,
                SuprPassService,
                UserService,
                { provide: Device, useClass: DeviceMock },
                { provide: Router, useClass: RouterMock },
                { provide: Network, useClass: NetwrokMock },
                { provide: ActivatedRoute, useClass: ActivatedRouteMock },
                ...BaseUnitTestProvidersIonic,
            ],
        });

        utilService = TestBed.get(UtilService);

        platformService = TestBed.get(PlatformService);

        walletService = TestBed.get(WalletService);

        suprPassService = TestBed.get(SuprPassService);

        userService = TestBed.get(UserService);

        device = TestBed.get(Device);
        network = TestBed.get(Network);
        router = TestBed.get(Router);
        activatedRoute = TestBed.get(ActivatedRoute);

        segmentService = new SegmentService(
            device,
            router,
            network,
            utilService,
            activatedRoute,
            platformService,
            walletService,
            suprPassService,
            userService
        );
    });

    it("#should create segment service instance", () => {
        expect(segmentService).toBeTruthy();
    });

    //NOTE: segmentService marked as any in following tests to access private methods/variables

    it("#isEventAllowed should return true when no excluded events are present", () => {
        expect(
            (segmentService as any).isEventAllowed(
                sampleEventDataForExclusionTest
            )
        ).toEqual(true);
    });

    it("#isEventAllowed should return true when excluded events are present \
    but fired event object name is not same", () => {
        segmentService.setExcludeEventsDict({
            excludeEventsDict: {
                "product-viewed": {
                    allowedTraits: {
                        context: ["out-of-stock-sku"],
                    },
                    notAllowedTraits: {
                        currentScreen: ["category"],
                    },
                },
            },
        });
        const sampleEvent = {
            ...sampleEventDataForExclusionTest,
            objectName: "product-info",
        };
        expect((segmentService as any).isEventAllowed(sampleEvent)).toEqual(
            true
        );
    });

    it("#isEventAllowed should return false when excluded events are present with no further conditions \
    and event matches", () => {
        segmentService.setExcludeEventsDict({
            "product-viewed": {},
        });
        const sampleEvent = {
            ...sampleEventDataForExclusionTest,
        };
        expect((segmentService as any).isEventAllowed(sampleEvent)).toEqual(
            false
        );
    });

    it("#isEventAllowed should return true when excluded events are present \
    and event matches an inclusion rule from allowedTraits", () => {
        segmentService.setExcludeEventsDict({
            "product-viewed": {
                allowedTraits: {
                    context: ["out-of-stock-sku"],
                },
                notAllowedTraits: {
                    currentScreen: ["category"],
                },
            },
        });
        const sampleEvent = {
            ...sampleEventDataForExclusionTest,
            context: "out-of-stock-sku",
        };
        expect((segmentService as any).isEventAllowed(sampleEvent)).toEqual(
            true
        );
    });

    it("#isEventAllowed should return false when excluded events are present \
    with notAllowed object and event matches an exclusion rule", () => {
        segmentService.setExcludeEventsDict({
            "product-viewed": {
                allowedTraits: {
                    context: ["out-of-stock-sku"],
                },
                notAllowedTraits: {
                    currentScreen: ["category"],
                },
            },
        });
        const sampleEvent = {
            ...sampleEventDataForExclusionTest,
        };
        expect((segmentService as any).isEventAllowed(sampleEvent)).toEqual(
            false
        );
    });

    it("#isEventAllowed should return true when excluded events are present \
    with only notAllowed object and event does not match any exclusion rule", () => {
        segmentService.setExcludeEventsDict({
            "product-viewed": {
                notAllowedTraits: {
                    currentScreen: ["category"],
                },
            },
        });
        const sampleEvent = {
            ...sampleEventDataForExclusionTest,
            currentScreen: "home",
        };
        expect((segmentService as any).isEventAllowed(sampleEvent)).toEqual(
            true
        );
    });
});
