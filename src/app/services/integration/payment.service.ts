import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import {
    InAppBrowser,
    InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";

import { Observable, of, throwError, Subject, forkJoin } from "rxjs";
import { map, tap, take, takeUntil, takeWhile } from "rxjs/operators";

import { environment } from "@environments/environment";

import {
    EVENT_TYPES,
    RAZORPAY_CHECKOUT_TYPE,
    RAZORPAY_CHECKOUT_URL,
    RAZORPAY_CUSTOM_CHECKOUT_BACKEND_WEBVIEW_ENDPOINT,
    RAZORPAY_CUSTOM_CHECKOUT_URL,
    RAZORPAY_KEY_FETCH_FAILURE_MESSGAE,
    RAZORPAY_KEY_NOT_PRESENT_ERROR,
    RAZORPAY_KEY_ERROR,
    RAZORPAY_KEY_ERROR_MODAL_DATA,
    RAZORPAY_OPTIONS,
    RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE,
    SETTINGS,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { User, Settings, RechargeCouponInfo } from "@models";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { ErrorService } from "@services/integration/error.service";
import { PlatformService } from "@services/util/platform.service";
import { SettingsService } from "@services/shared/settings.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import {
    SuprApi,
    ErrorType,
    RazorpayOptions,
    RazorpayAutoDebitOptions,
    RazorpayPaymentMethodInfo,
    RazorpayWebviewCustomMsgObject,
    RazorpayCustomPaymentOrderData,
    RazorpayCustomPaymentSuccessRes,
    RazorpayRecurringPaymentOrderData,
    RazorpayCreateCustomPaymentPayload,
    RazorpayCustomPaymentOrderCreateReq,
} from "@types";

declare const RazorpayCheckout: any;
declare const Razorpay: any;
declare type SuccessCb = (paymentId: string) => void;
declare type RecurringPaymentSuccessCb = (
    orderId: string,
    paymentId: string
) => void;
declare type CustomPaymentSuccessCb = (
    paymentInfo: RazorpayCustomPaymentSuccessRes
) => void;
declare type CustomPaymentFailureCb = (
    errType?: RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE | null,
    err?: Error
) => void;

@Injectable({
    providedIn: "root",
})
export class PaymentService {
    destroy$: Subject<boolean>;

    private razorpay: any;
    private razorpayUrlInjected: string;

    constructor(
        private apiService: ApiService,
        private errorService: ErrorService,
        private inAppBrowser: InAppBrowser,
        private platformService: PlatformService,
        private settingsService: SettingsService,
        private utilService: UtilService,
        private analyticsService: AnalyticsService
    ) {}

    public initialize(type = RAZORPAY_CHECKOUT_TYPE.STANDARD) {
        if (!this.platformService.isCordova()) {
            this.injectRazorpayScript(type, "initialize");
        }
    }

    public verifySuprPassRecurringPayment(
        reqObj: Partial<SuprApi.RecurringPaymentReq>
    ): Observable<SuprApi.RecurringPaymentRes> {
        return this.apiService
            .verifySuprPassRecurringPayment(reqObj)
            .pipe(map((res) => res));
    }

    public async handleRecurringPayment(
        data: { amount: number; user: User; method: string },
        onSuccess: RecurringPaymentSuccessCb,
        modalDismissHandler: () => void
    ) {
        try {
            const observableInputs = [
                this.getAutoDebitCustomerAndOrderId(data.amount),
                this.getRazorpayOptions(data),
            ];
            this.initSubscribeMonitor();
            return forkJoin(observableInputs)
                .pipe(takeUntil(this.destroy$))
                .subscribe(
                    ([autoDebitInfo, options]: [
                        RazorpayRecurringPaymentOrderData,
                        RazorpayAutoDebitOptions
                    ]) => {
                        const { customer_id, order_id } = autoDebitInfo;
                        options = {
                            ...options,
                            customer_id,
                            order_id,
                            recurring: "1",
                        };

                        const onSuccessCb = (payment_id: string) => {
                            onSuccess(order_id, payment_id);
                        };
                        this.startRazorpay(
                            options,
                            onSuccessCb,
                            modalDismissHandler
                        );
                        this.destroySubscribeMonitor();
                    },
                    (err) => {
                        this.handleRazorpayProcessError(
                            err,
                            modalDismissHandler
                        );
                        return throwError(err);
                    }
                );
        } catch (e) {
            throw e;
        }
    }

    public handleCustomCheckout(
        data: RazorpayCreateCustomPaymentPayload,
        onSuccess: CustomPaymentSuccessCb,
        onFailure: CustomPaymentFailureCb
    ) {
        try {
            const observableInputs = [
                this.getCustomCheckoutCustomerAndOrderId(
                    data.amount,
                    data.paymentGateway,
                    data.buySuprCredits,
                    data.couponInfo
                ),
                this.getRazorpayKey(),
            ];

            this.initSubscribeMonitor();

            forkJoin(observableInputs)
                .pipe(takeUntil(this.destroy$))
                .subscribe(
                    ([orderInfo, key]: [
                        RazorpayCustomPaymentOrderData,
                        string
                    ]) => {
                        if (orderInfo && key) {
                            // for web
                            if (!this.platformService.isCordova()) {
                                this.startRazorpayCustomCheckoutWeb(
                                    data,
                                    orderInfo,
                                    key,
                                    onSuccess,
                                    onFailure
                                );
                            } else {
                                this.startRazorpayCustomCheckoutCordova(
                                    data,
                                    orderInfo,
                                    onSuccess,
                                    onFailure
                                );
                            }
                            this.destroySubscribeMonitor();
                        }
                    },
                    (err) => {
                        this.handleRazorpayProcessError(err, onFailure);
                        return throwError(err);
                    }
                );
        } catch (e) {
            throw e;
        }
    }

    public openRazorpay(
        data: { amount: number; user: User; couponInfo: RechargeCouponInfo },
        onSuccess: SuccessCb,
        modalDismissHandler?: () => void
    ) {
        this.initSubscribeMonitor();
        this.getRazorpayOptions(data)
            .pipe(takeUntil(this.destroy$))
            .subscribe(
                (options: RazorpayOptions) => {
                    if (options) {
                        this.startRazorpay(
                            options,
                            onSuccess,
                            modalDismissHandler
                        );
                        this.destroySubscribeMonitor();
                    }
                },
                (err) => {
                    this.handleRazorpayProcessError(err, modalDismissHandler);
                    return throwError(err);
                }
            );
    }

    public async getRazorpayCustomCheckoutInstance(): Promise<any> {
        try {
            await this.injectRazorpayScript(
                RAZORPAY_CHECKOUT_TYPE.CUSTOM,
                "getRazorpayCustomCheckoutInstance"
            );

            if (this.razorpay) {
                return this.razorpay;
            }

            const rzpKey = await this.getRazorpayKey().toPromise();
            this.razorpay = new Razorpay({ key: rzpKey });
            return this.razorpay;
        } catch (error) {
            throw error;
        }
    }

    private getCustomCheckoutDataWeb(
        amount: number,
        user: User,
        orderInfo: RazorpayCustomPaymentOrderData,
        paymentInfo: RazorpayPaymentMethodInfo,
        couponInfo: RechargeCouponInfo
    ) {
        /* Check if coupon is applied and it is a payment partner offer */
        const paymentOfferId = this.utilService.getNestedValue(
            couponInfo,
            "paymentDetails.paymentOfferId",
            null
        );
        if (couponInfo && couponInfo.isApplied && paymentOfferId) {
            paymentInfo = {
                ...paymentInfo,
                offer_id: paymentOfferId,
            } as any;
        }
        const options: any = {
            /* converting to paisa based on razorpay docs */
            amount: amount * 100,
            currency: "INR",
            email: user.email,
            contact: user.mobileNumber,
            customer_id: orderInfo.payment_partner_customer_id,
            order_id: orderInfo.payment_partner_order_id,
            ...paymentInfo,
        };

        return options;
    }

    private getCustomCheckoutDataCordova(
        amount: number,
        user: User,
        orderInfo: RazorpayCustomPaymentOrderData,
        paymentInfo: RazorpayPaymentMethodInfo,
        couponInfo: RechargeCouponInfo
    ) {
        /* Check if coupon is applied and it is a payment partner offer */
        const paymentOfferId = this.utilService.getNestedValue(
            couponInfo,
            "paymentDetails.paymentOfferId",
            null
        );
        if (couponInfo && couponInfo.isApplied && paymentOfferId) {
            paymentInfo = {
                ...paymentInfo,
                offer_id: paymentOfferId,
            } as any;
        }
        return {
            /* converting to paisa based on razorpay docs */
            amount: amount * 100,
            currency: "INR",
            email: user.email,
            contact: user.mobileNumber,
            customer_id: orderInfo.payment_partner_customer_id,
            order_id: orderInfo.payment_partner_order_id,
            payment_info: JSON.stringify(paymentInfo),
        };
    }

    private async startRazorpayCustomCheckoutWeb(
        data: RazorpayCreateCustomPaymentPayload,
        orderInfo: RazorpayCustomPaymentOrderData,
        key: string,
        onSuccess: CustomPaymentSuccessCb,
        onFailure: CustomPaymentFailureCb
    ) {
        try {
            await this.injectRazorpayScript(
                RAZORPAY_CHECKOUT_TYPE.CUSTOM,
                "startRazorpayCustomCheckoutWeb"
            );
            const { amount, user, paymentInfo, couponInfo } = data;

            const razorpayData = this.getCustomCheckoutDataWeb(
                amount,
                user,
                orderInfo,
                paymentInfo,
                couponInfo
            );

            this.razorpay = new Razorpay({ key });

            this.razorpay.createPayment(razorpayData);

            this.razorpay.on("payment.success", (resp) => {
                /* conversion to paisa for capture api */
                onSuccess({
                    order_id: resp.razorpay_order_id,
                    payment_id: resp.razorpay_payment_id,
                    customer_id: orderInfo.payment_partner_customer_id,
                    payment_gateway: data.paymentGateway,
                    amount: amount * 100,
                });
            });

            this.razorpay.on("payment.error", (resp) => {
                console.log({ resp });
                /**  Manually inject proper error type on web for consistency with cordova behaviour */
                const errorStep = this.utilService.getNestedValue(
                    resp,
                    "error.step",
                    null
                );
                if (!errorStep) {
                    onFailure(
                        RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PRE_PROCESSING_ERROR,
                        resp
                    );
                } else {
                    onFailure(
                        RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR,
                        resp
                    );
                }
            });
        } catch (err) {
            throw err;
        }
    }

    private async startRazorpayCustomCheckoutCordova(
        data: RazorpayCreateCustomPaymentPayload,
        orderInfo: RazorpayCustomPaymentOrderData,
        onSuccess: CustomPaymentSuccessCb,
        onFailure: CustomPaymentFailureCb
    ) {
        const { amount, paymentInfo, user, couponInfo } = data;

        const razorpayData = this.getCustomCheckoutDataCordova(
            amount,
            user,
            orderInfo,
            paymentInfo,
            couponInfo
        );

        /** Create query params from razorpay data */
        const query = Object.entries(razorpayData)
            .map(([k, val]) => `${k}=${val}`)
            .join("&");

        const options: InAppBrowserOptions = {
            zoom: "no",
            clearcache: "yes",
            cleardata: "yes",
            clearsessioncache: "yes",
            location: "no",
        };

        const url = encodeURI(
            `${environment.api_host}${RAZORPAY_CUSTOM_CHECKOUT_BACKEND_WEBVIEW_ENDPOINT}?${query}`
        );

        const browser = this.inAppBrowser.create(url, "_blank", options);

        let stopMsgSubscription = false;

        browser
            .on("message")
            .pipe(takeWhile((_event: any) => !stopMsgSubscription))
            .subscribe((event: any) => {
                const {
                    error,
                    successData,
                    dataType,
                    errorData,
                } = this.handleWebviewPaymentMessage(event);
                console.log({ error, errorData });
                if (error) {
                    stopMsgSubscription = true;
                    onFailure(dataType, errorData);
                    browser.close();
                }
                if (!error && successData) {
                    stopMsgSubscription = true;
                    browser.close();
                    /* no conversion to paisa required, capture api does it */
                    onSuccess({
                        order_id: successData.razorpay_order_id,
                        payment_id: successData.razorpay_payment_id,
                        customer_id: orderInfo.payment_partner_customer_id,
                        payment_gateway: data.paymentGateway,
                        amount: amount * 100,
                    });
                }
            });

        browser
            .on("exit")
            .pipe(take(1))
            .subscribe((_event) => onFailure());
    }

    private handleWebviewPaymentMessage(
        event: any
    ): {
        error: boolean;
        errorData: null | Error;
        successData: null | {
            razorpay_order_id: string;
            razorpay_payment_id: string;
        };
        dataType: RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE | null;
    } {
        const receivedData: RazorpayWebviewCustomMsgObject = this.utilService.getNestedValue(
            event,
            "data",
            {}
        );

        const dataType = this.utilService.getNestedValue(
            receivedData,
            "dataType",
            null
        );

        const {
            success,
            razorpay_payment_id,
            razorpay_order_id,
            error: errorData,
        } = receivedData;

        if (this.isWebviewMsgResponseErrorType(dataType)) {
            return { error: true, successData: null, dataType, errorData };
        }

        if (dataType === RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSED) {
            if (!success) {
                return { error: true, successData: null, dataType, errorData };
            } else {
                return {
                    error: false,
                    successData: { razorpay_order_id, razorpay_payment_id },
                    dataType,
                    errorData: null,
                };
            }
        }
        return { error: false, successData: null, dataType, errorData };
    }

    private isWebviewMsgResponseErrorType(
        dataType: RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE
    ) {
        return (
            dataType ===
                RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PRE_PROCESSING_ERROR ||
            dataType ===
                RAZORPAY_WEBVIEW_CUSTOM_MSG_TYPE.PAYMENT_PROCESSING_ERROR
        );
    }

    private startRazorpay(
        options: RazorpayOptions,
        onSuccess: SuccessCb,
        modalDismissHandler?: () => void
    ) {
        if (
            this.platformService.isCordova() &&
            RazorpayCheckout !== undefined
        ) {
            this.openRazorpayForCordova(
                options,
                onSuccess,
                modalDismissHandler
            );
        } else if (Razorpay !== undefined) {
            this.openRazorpayForWeb(options, onSuccess, modalDismissHandler);
        }
    }

    private openRazorpayForCordova(
        options: RazorpayOptions | RazorpayAutoDebitOptions,
        onSuccess: SuccessCb,
        modalDismissHandler?: () => void
    ) {
        RazorpayCheckout.open(
            options,
            (paymentId: string) => onSuccess(paymentId),
            () => this.onRazorpayFailure(modalDismissHandler)
        );
    }

    private async openRazorpayForWeb(
        options: RazorpayOptions | RazorpayAutoDebitOptions,
        onSuccess: SuccessCb,
        modalDismissHandler?: () => void
    ) {
        try {
            await this.injectRazorpayScript(
                RAZORPAY_CHECKOUT_TYPE.STANDARD,
                "openRazorpayForWeb"
            );

            const webOptions = {
                ...options,
                handler: (result: object) => {
                    onSuccess(result["razorpay_payment_id"]);
                },
                modal: {
                    ondismiss: () => {
                        if (modalDismissHandler) {
                            modalDismissHandler();
                        }
                    },
                },
            };

            this.razorpay = new Razorpay(webOptions);
            this.razorpay.open();
        } catch (error) {
            throw error;
        }
    }

    private onRazorpayFailure = (cb?: () => void) => {
        /* [TODO]: Razorpay error handling */
        if (cb) {
            cb();
        }
    };

    private handleRazorpayProcessError(
        err: any,
        modalDismissHandler?: () => void
    ) {
        this.destroySubscribeMonitor();
        if (modalDismissHandler) {
            modalDismissHandler();
        }
        if (err.httpCode) {
            this.handleKeyFetchApiFailure(err);
        } else {
            this.handleKeyNotPresentError();
        }
    }

    private getRazorpayKey(): Observable<string> {
        const razorpayInfo = this.settingsService.getSettingsValue("razorpay");
        if (razorpayInfo && razorpayInfo.key) {
            return of(razorpayInfo.key);
        } else {
            return this.apiService.fetchSettings(true, 2, true).pipe(
                tap((settings: Settings) => {
                    this.settingsService.updateSettings(settings);
                }),
                map(() => {
                    const razorpay = this.settingsService.getSettingsValue(
                        "razorpay"
                    );
                    if (razorpay && razorpay.key) {
                        return razorpay.key;
                    } else {
                        throw new Error("KEY NOT FOUND");
                    }
                })
            );
        }
    }

    private getRazorpayOptions(data: {
        amount: number;
        user: User;
        method?: string;
        couponInfo?: RechargeCouponInfo;
    }): Observable<RazorpayOptions> {
        const { amount, user, method } = data;
        const { email, mobileNumber, firstName, lastName } = user;
        return this.getRazorpayKey().pipe(
            map((key: string) => {
                const options: RazorpayOptions = {
                    key,
                    currency: RAZORPAY_OPTIONS.CURRENCY,
                    theme: { color: RAZORPAY_OPTIONS.COLOR, hide_topbar: true },
                    amount: amount * 100,
                    prefill: {
                        email,
                        method,
                        contact: mobileNumber,
                        name: `${firstName} ${lastName}`,
                    },
                };
                const isCouponApplied = this.utilService.getNestedValue(
                    data,
                    "couponInfo.isApplied",
                    false
                );
                const paymentInstruments = data.couponInfo
                    ? this.utilService.getNestedValue(
                          data.couponInfo,
                          "paymentDetails.instruments",
                          []
                      )
                    : [];

                /* If coupon is applied and specific payment instruments are present 
                which means coupon code only supports those methods,
                then use it to show proper methods in standard checkout */
                if (
                    isCouponApplied &&
                    this.utilService.isLengthyArray(paymentInstruments)
                ) {
                    const displayBlockTitle = this.settingsService.getSettingsValue(
                        SETTINGS.RAZORPAY_MODAL_AVAILABLE_MODES_TEXT,
                        SETTINGS_KEYS_DEFAULT_VALUE[
                            SETTINGS.RAZORPAY_MODAL_AVAILABLE_MODES_TEXT
                        ]
                    );
                    const displayConfig = {
                        blocks: {
                            code: {
                                name: displayBlockTitle,
                                instruments: [...paymentInstruments],
                            },
                        },
                        sequence: ["block.code"], // The sequence in which blocks and methods should be shown
                        preferences: {
                            show_default_blocks: false, // Should Checkout show its default blocks?
                        },
                    };
                    options.config = {
                        display: { ...displayConfig },
                    };
                }
                return options;
            })
        );
    }

    private handleKeyFetchApiFailure(err: HttpErrorResponse) {
        this.analyticsService.trackError({
            type: ErrorType.API,
            url: err.url,
            httpCode: err.status,
            name: RAZORPAY_KEY_ERROR,
            message: RAZORPAY_KEY_FETCH_FAILURE_MESSGAE,
        });

        this.errorService.handleGenericError(
            err.url,
            RAZORPAY_KEY_ERROR_MODAL_DATA
        );
    }

    private handleKeyNotPresentError() {
        this.analyticsService.trackError({
            type: ErrorType.API_GENERIC,
            name: RAZORPAY_KEY_ERROR,
            message: RAZORPAY_KEY_NOT_PRESENT_ERROR,
        });
        this.errorService.handleGenericError(
            null,
            RAZORPAY_KEY_ERROR_MODAL_DATA
        );
    }

    private initSubscribeMonitor() {
        this.destroySubscribeMonitor();
        this.destroy$ = new Subject<boolean>();
    }

    private destroySubscribeMonitor() {
        if (this.destroy$ && !this.destroy$.closed) {
            this.destroy$.next(true);
            this.destroy$.unsubscribe();
        }
    }

    private getAutoDebitCustomerAndOrderId(
        amount: number
    ): Observable<{
        customer_id: string;
        order_id: string;
    }> {
        return this.apiService
            .fetchAutoDebitCustomerAndOrderId(amount)
            .pipe(map((res) => res));
    }

    private getCustomCheckoutCustomerAndOrderId(
        amount: number,
        payment_partner: string,
        buy_supr_credits: boolean,
        couponInfo?: RechargeCouponInfo
    ): Observable<RazorpayCustomPaymentOrderData> {
        /* conversion to paisa for initiate api */
        const reqBody: RazorpayCustomPaymentOrderCreateReq = {
            amount: amount * 100,
            payment_partner,
            buy_supr_credits,
        };
        /* If payment mode specific coupon is applied then pass it to backend for order id generation */
        if (
            couponInfo &&
            couponInfo.isApplied &&
            couponInfo.paymentDetails &&
            couponInfo.paymentDetails.paymentOfferId
        ) {
            reqBody.coupon_code = couponInfo.couponCode;
            reqBody.gateway_offer_id = couponInfo.paymentDetails.paymentOfferId;
        }
        return this.apiService
            .fetchCustomPaymentCustomerAndOrderId(reqBody)
            .pipe(map((res) => res));
    }

    public async injectRazorpayScript(
        type = RAZORPAY_CHECKOUT_TYPE.STANDARD,
        callerMethod: string
    ): Promise<void> {
        return new Promise((resolve, reject) => {
            const urlToBeInjected = this.getRazorpayUrlFromType(type);

            if (this.isGivenRazorpayUrlInjected(urlToBeInjected)) {
                return resolve();
            }

            (window as any).Razorpay = undefined;
            this.razorpay = undefined;
            this.razorpayUrlInjected = undefined;

            this.sendRzpScriptInjectionInitEvent(type, callerMethod);

            const paymentScript = document.createElement("script");
            if (type === RAZORPAY_CHECKOUT_TYPE.STANDARD) {
                paymentScript.setAttribute("src", RAZORPAY_CHECKOUT_URL);
            } else {
                paymentScript.setAttribute("src", RAZORPAY_CUSTOM_CHECKOUT_URL);
            }
            document.head.appendChild(paymentScript);

            paymentScript.onload = () => {
                this.razorpayUrlInjected =
                    type === RAZORPAY_CHECKOUT_TYPE.STANDARD
                        ? RAZORPAY_CHECKOUT_URL
                        : RAZORPAY_CUSTOM_CHECKOUT_URL;
                resolve();
            };
            paymentScript.onerror = () => {
                this.sendRzpScriptInjectionFailEvent(type, callerMethod);
                return reject();
            };
        });
    }

    private isGivenRazorpayUrlInjected(url: string) {
        return this.razorpayUrlInjected && this.razorpayUrlInjected === url;
    }

    private getRazorpayUrlFromType(type: string): string {
        return type === RAZORPAY_CHECKOUT_TYPE.STANDARD
            ? RAZORPAY_CHECKOUT_URL
            : RAZORPAY_CUSTOM_CHECKOUT_URL;
    }

    private sendRzpScriptInjectionInitEvent(
        type: string,
        callerMethod: string
    ) {
        if (type && callerMethod) {
            this.analyticsService.trackCustom(
                EVENT_TYPES.RAZORPAY_SCRIPT_INJECT_INIT,
                {
                    type,
                    caller: callerMethod,
                }
            );
        }
    }
    private sendRzpScriptInjectionFailEvent(
        type: string,
        callerMethod: string
    ) {
        if (type && callerMethod) {
            this.analyticsService.trackCustom(
                EVENT_TYPES.RAZORPAY_SCRIPT_INJECT_FAIL,
                {
                    type,
                    caller: callerMethod,
                }
            );
        }
    }
}
