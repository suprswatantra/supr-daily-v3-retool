import { Injectable } from "@angular/core";

import { environment } from "@environments/environment";

import { NEW_USER_KEY, MOENGAGE_EVENTS } from "@constants";

import { PNData } from "@store/pn/pn.state";

import { User, Subscription, CartItem, CartMeta, Sku } from "@shared/models";

import { DbService } from "@services/data/db.service";
import { PlatformService } from "@services/util/platform.service";
import { StoreService } from "@services/data/store.service";
import { AnalyticsService } from "./analytics.service";
import { UtilService } from "@services/util/util.service";
import { CartMini } from "@types";

declare var MoECordova: any;

@Injectable({
    providedIn: "root",
})
export class MoengageService {
    moe: any;

    private userInitialised = false;

    constructor(
        private dbService: DbService,
        private platformService: PlatformService,
        private utilService: UtilService,
        private storeService: StoreService,
        private analyticsService: AnalyticsService
    ) {}

    initialize() {
        if (this.isEnabled()) {
            this.initSDK();
        }
    }

    setUserContext(userData: User) {
        if (this.isEnabled() && this.moe && userData.id) {
            if (!this.userInitialised) {
                const newUser = this.dbService.getLocalData(NEW_USER_KEY);

                this.moe.setAppStatus(!newUser);
                this.moe.setUserAttribute(
                    "USER_ATTRIBUTE_UNIQUE_ID",
                    userData.id
                );
                this.userInitialised = true;
            }

            this.setUserAttributes(userData);
        }
    }

    setUserAttributes(userData: User) {
        if (this.isEnabled() && this.moe) {
            this.moe.setUserAttribute(
                "USER_ATTRIBUTE_USER_EMAIL",
                userData.email
            );
            this.moe.setUserAttribute(
                "USER_ATTRIBUTE_USER_MOBILE",
                userData.mobileNumber
            );
            this.moe.setUserAttribute(
                "USER_ATTRIBUTE_USER_FIRST_NAME",
                userData.firstName
            );
            this.moe.setUserAttribute(
                "USER_ATTRIBUTE_USER_LAST_NAME",
                userData.lastName
            );
            const city = this.utilService.getNestedValue(
                userData,
                "address.city.name",
                ""
            );
            const cityId = this.utilService.getNestedValue(
                userData,
                "address.city.id",
                ""
            );
            this.moe.setUserAttribute(
                "SUPR_CREDITS_ENABLED",
                !!userData.suprCreditsEnabled
            );
            this.moe.setUserAttribute("USER_CITY", city);
            this.moe.setUserAttribute("USER_CITY_ID", cityId);
        }
    }

    logout() {
        if (this.isEnabled() && this.moe) {
            this.userInitialised = false;
            this.moe.logout();
        }
    }

    trackLogin(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.LOGGED_IN, properties);
    }

    trackSignup(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.SIGNED_UP, properties);
    }

    trackProfileCompleted(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.PROFILE_COMPLETED, properties);
    }

    trackAddressChange(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.LOGGED_IN, properties);
    }

    trackWalletRecharge(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.WALLET_RECHARGE, properties);
    }

    trackAddOn(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.DELIVER_ONCE, properties);
    }

    trackDeliveryCancellation(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.DELIVERY_CANCELLED, properties);
    }

    trackInitiateSubscription(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.INITIATE_SUBSCRIPTION, properties);
    }

    trackSubscriptionAddToCart(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.SUBSCRIPTION_ADD_TO_CART, properties);
    }

    trackSubscriptionStart(subscription: Subscription) {
        this.trackEvent(MOENGAGE_EVENTS.SUBSCRIPTION_STARTED, {
            sku_id: subscription.sku_id,
            product_identifier: this.getProductIdentifier(
                subscription.sku_data
            ),
            subscription_id: subscription.id,
            last_delivery: subscription.delivery_dates.last,
            first_delivery: subscription.delivery_dates.start,
            pending_deliveries: subscription.deliveries_remaining,
            status: "active",
            // Amount Spent
        });
    }

    trackSubscriptionCancellation(subscription: Subscription) {
        this.trackEvent(MOENGAGE_EVENTS.SUBSCRIPTION_CANCELLED, {
            sku_id: subscription.sku_id,
            product_identifier: this.getProductIdentifier(
                subscription.sku_data
            ),
            subscription_id: subscription.id,
            last_delivery: subscription.delivery_dates.last,
            first_delivery: subscription.delivery_dates.start,
            pending_deliveries: subscription.deliveries_remaining,
            status: "inactive",
        });
    }

    trackSubscriptionUpdate(subscription: Subscription) {
        this.trackEvent(MOENGAGE_EVENTS.SUBSCRIPTION_UPDATED, {
            sku_id: subscription.sku_id,
            product_identifier: this.getProductIdentifier(
                subscription.sku_data
            ),
            subscription_id: subscription.id,
            last_delivery: subscription.delivery_dates.last,
            first_delivery: subscription.delivery_dates.start,
            pending_deliveries: subscription.deliveries_remaining,
            status: "active",
        });
    }

    trackVacationUpdate(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.VACATION_UPDATED, properties);
    }

    trackProductListView(
        categoryId: number,
        categoryName: string,
        subcategoryId?: number,
        subcategoryName?: string,
        skuList?: Array<number>
    ) {
        this.trackEvent(MOENGAGE_EVENTS.VIEWED_PRODUCT_LIST, {
            category_id: categoryId,
            category_name: categoryName,
            subcategory_id: subcategoryId,
            subcategory_name: subcategoryName,
            sku_list: skuList,
        });
    }

    trackAddToCart(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.ADD_TO_CART, properties);
    }

    trackRemoveFromCart(properties: any = {}) {
        this.trackEvent(MOENGAGE_EVENTS.REMOVE_FROM_CART, properties);
    }

    trackInitiateCheckout(
        cart: CartMini,
        walletBalance: number,
        suprCreditsBalance: number = 0
    ) {
        const traits = this.getInitiateCheckoutTraits(
            cart,
            walletBalance,
            suprCreditsBalance
        );

        this.trackEvent(MOENGAGE_EVENTS.INITIATE_CHECKOUT, traits);
    }

    trackCompletePurchaseSummary(
        cartItems: CartItem[],
        cartMeta: CartMeta,
        isFirstPurchase?: boolean
    ) {
        const properties = this.getCompletePurchaseSummaryTraits(
            cartItems,
            cartMeta,
            isFirstPurchase
        );

        this.trackEvent(MOENGAGE_EVENTS.COMPLETE_PURCHASE_SUMMARY, properties);
    }

    trackCompletePurchaseDetails(cartItem: CartItem) {
        const properties = this.getCompletePurchaseDetailsTraits(cartItem);

        this.trackEvent(MOENGAGE_EVENTS.COMPLETE_PURCHASE_DETAILS, properties);
    }

    trackInitiateRecharge(
        subscription: Subscription,
        cartMeta: CartMeta,
        deliveries: number
    ) {
        const properties = this.getInitiateRechargeTraits(
            subscription,
            cartMeta,
            deliveries
        );

        this.trackEvent(MOENGAGE_EVENTS.INITIATE_RECHARGE, properties);
    }

    handleNotificationClick(data: PNData) {
        if (!data) {
            return;
        }

        this.setUtmParams(data);

        const {
            pnType,
            title,
            desc,
            img,
            url,
            buttonText,
            buttonUrl,
            outsideRedirection,
        } = data;
        this.storeService.setPNData({
            pnType,
            title,
            desc,
            img,
            url,
            buttonText,
            buttonUrl,
            outsideRedirection,
            source: "moengage",
        });
    }

    private getProductIdentifier(sku: Sku) {
        if (!sku) {
            return null;
        }

        return this.utilService.getNestedValue(sku, "product_identifier", null);
    }

    private initSDK() {
        this.moe = new MoECordova.init();
        this.registerForPN();
        this.registerForInAppMessaging();
        this.listenToInAppEvents();

        this.moe.on("onPushClick", (data) => {
            const pnCustomData = this.platformService.isIOS()
                ? this.utilService.getNestedValue(
                      data,
                      "clickedAction.payload.kvPair",
                      {}
                  )
                : this.utilService.getNestedValue(data, "payload", {});

            this.handleNotificationClick(pnCustomData);
        });
    }

    private listenToInAppEvents() {
        this.moe.on("onInAppClick", (inAppInfo) => {
            const pnCustomData = this.utilService.getNestedValue(
                inAppInfo,
                "navigation.kvPair",
                {}
            );
            this.handleNotificationClick(pnCustomData);
        });
    }

    private registerForPN() {
        if (this.platformService.isIOS()) {
            this.moe.registerForPushNotification();
        }
    }

    private registerForInAppMessaging() {
        if (this.platformService.isIOS()) {
            this.moe.showInApp();
        }
    }

    private trackEvent(eventName: string, eventTraits: any = {}) {
        if (this.isEnabled()) {
            this.analyticsService.trackMoEngageEvent(eventName, eventTraits);
        }
    }

    private isEnabled(): boolean {
        return environment.production && this.platformService.isCordova();
    }

    private getInitiateCheckoutTraits(
        cart: CartMini,
        walletBalance: number,
        suprCreditsBalance: number
    ): any {
        return {
            cart_size: cart.itemCount,
            cart_total: cart.cartTotal,
            wallet_balance: walletBalance,
            supr_credits_balance: suprCreditsBalance,
        };
    }

    private getCompletePurchaseSummaryTraits(
        cartItems: CartItem[],
        cartMeta: CartMeta,
        isFirstPurchase?: boolean
    ): any {
        return {
            cart_size: cartItems.length,
            cart_total: this.utilService.getNestedValue(
                cartMeta,
                "payment_data.pre_discount"
            ),
            amount_paid: this.utilService.getNestedValue(
                cartMeta,
                "payment_data.post_discount"
            ),
            coupon_code: this.utilService.getNestedValue(
                cartMeta,
                "discount_info.coupon_code"
            ),
            service_fee: this.utilService.getNestedValue(
                cartMeta,
                "payment_data.delivery_fee",
                0
            ),
            is_first_purchase: isFirstPurchase,
        };
    }

    private getCompletePurchaseDetailsTraits(cartItem: CartItem): any {
        return {
            type: cartItem.type,
            sku_id: cartItem.sku_id,
            quantity: cartItem.quantity,
            recharge_qty: cartItem.recharge_quantity,
        };
    }

    private getInitiateRechargeTraits(
        subscription: Subscription,
        cartMeta: CartMeta,
        deliveries: number
    ): any {
        return {
            sku_id: subscription.sku_id,
            product_identifier: this.getProductIdentifier(
                subscription.sku_data
            ),
            subscription_id: subscription.id,
            cart_total: this.utilService.getNestedValue(
                cartMeta,
                "payment_data.post_discount"
            ),
            coupon_code: this.utilService.getNestedValue(
                cartMeta,
                "discount_info.coupon_code"
            ),
            wallet_balance: this.utilService.getNestedValue(
                cartMeta,
                "wallet_info.balance"
            ),
            recharge_qty: deliveries,
        };
    }

    private setUtmParams(data: PNData) {
        const utmParams = {
            utm_source: this.utilService.getNestedValue(
                data,
                "utm_source",
                null
            ),

            utm_medium: this.utilService.getNestedValue(
                data,
                "utm_medium",
                null
            ),

            utm_campaign: this.utilService.getNestedValue(
                data,
                "utm_campaign",
                null
            ),

            utm_term: this.utilService.getNestedValue(data, "utm_term", null),

            utm_content: this.utilService.getNestedValue(
                data,
                "utm_content",
                null
            ),
        };

        this.analyticsService.setUtmParams(utmParams);
    }
}
