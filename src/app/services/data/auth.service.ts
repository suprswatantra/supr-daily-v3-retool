import { Injectable } from "@angular/core";

import { Observable, from } from "rxjs";
import { tap } from "rxjs/operators";

import { DbService } from "@services/data/db.service";
import { RouterService } from "@services/util/router.service";
import { FreshChatService } from "@services/integration/freshchat.service";
import { StoreService } from "@services/data/store.service";
import { MoengageService } from "@services/integration/moengage.service";
import { CalendarService } from "@services/date/calendar.service";
import { UserService } from "@services/shared/user.service";
import { AnalyticsService } from "@services/integration/analytics.service";

@Injectable({
    providedIn: "root",
})
export class AuthService {
    private authToken: string;

    constructor(
        private storeService: StoreService,
        private dbService: DbService,
        private routerService: RouterService,
        private freshChatService: FreshChatService,
        private moengageService: MoengageService,
        private calendarService: CalendarService,
        private userService: UserService,
        private analyticsService: AnalyticsService
    ) {}

    loadSession(): Observable<string> {
        return from(this.loadToken()).pipe(
            tap((token: string) => {
                if (token) {
                    this.authToken = token;
                }
            })
        );
    }

    isAuthenticated(): boolean {
        // Check whether the token is present
        return !!this.authToken;
    }

    getAuthToken(): string {
        return this.authToken;
    }

    setAuthToken(token: string, storeInDB = true) {
        this.authToken = token;

        // Store it in DB too.
        if (storeInDB) {
            this.dbService.setData("authToken", token);
        }
    }

    clearAuthToken() {
        this.clearTokenFromApp();
        this.clearTokenFromDb();
    }

    logout(redirectToLoginPage = true) {
        this.clearTokenFromApp();
        this.storeService.logout();
        this.dbService.clearDB();
        this.dbService.clearLocalState();
        this.freshChatService.clearUserData();
        this.analyticsService.clearUser();
        this.moengageService.logout();
        this.calendarService.clearTPEnabled();
        this.userService.setIsAnonymousUser(true);

        if (redirectToLoginPage) {
            this.routerService.goToLoginPage({ replaceUrl: true });
        }
    }

    private loadToken(): Promise<any> {
        return this.dbService.getData("authToken");
    }

    private clearTokenFromApp() {
        this.authToken = null;
    }

    private clearTokenFromDb() {
        return this.dbService.clearData("authToken");
    }
}
