import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { map, catchError, tap } from "rxjs/operators";
import { Observable, throwError, of, EMPTY } from "rxjs";

import { API_ROUTES, LOCAL_DB_DATA_KEYS } from "@constants";

import { environment } from "@environments/environment";

import * as SuprModels from "@models";

import { DbService } from "@services/data/db.service";
import { UtilService } from "@services/util/util.service";
import { UserService } from "@services/shared/user.service";
import { SuprHttpParams } from "@services/angular/supr-params.service";

import {
    SuprApi,
    SuprMaps,
    SkuSearchParams,
    RazorpayRecurringPaymentOrderData,
    RazorpayCustomPaymentOrderData,
    RazorpayCustomPaymentOrderCreateReq,
} from "@types";

import { SEARCH_TRUNCATE_LENGTH } from "@pages/address/constants";

// import { UI_MERCH_LAT } from "@stubs/ui_merch.stub";
// import { MILESTONE_DONE } from "@stubs/milestone.stub";
// import { MILESTONE_DETAILS } from "@stubs/milestone.stub";

@Injectable({
    providedIn: "root",
})
export class ApiService {
    private static readonly apiHostUrl = environment.api_host;
    private static readonly appApiHostUrl = environment.app_api_host;
    private static readonly cxHostUrl = environment.cx_host;

    constructor(
        private http: HttpClient,
        private utilService: UtilService,
        private dbService: DbService,
        private userService: UserService
    ) {}

    fetchAnonymousToken(
        reqObj: SuprApi.AnonymousTokenReqParams
    ): Observable<SuprApi.AnonymousTokenRes> {
        const url = this.getApiUrl("anonymous_token");
        // return of(ANONYMOUS_USER_STUB);
        return this.http
            .post(url, reqObj)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchSettings(
        silent = true,
        retryCount = 1,
        useCustomErrorHandler = false
    ): Observable<SuprModels.Settings> {
        const url = this.getApiUrl("settings", {
            service: "AppApi",
        });
        return this.http
            .get<SuprModels.Settings>(url, {
                params: new SuprHttpParams({
                    silent,
                    retryCount,
                    useCustomErrorHandler,
                }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchExperiments(
        experimentsFetchObj: SuprApi.FetchExperimentsBody,
        silent = true
    ): Observable<SuprModels.Experiments> {
        const url = this.getApiUrl("experiments", {
            service: "AppApi",
        });
        return this.http
            .post<SuprModels.Experiments>(url, experimentsFetchObj, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchNudgeList(): Observable<SuprModels.Nudge[]> {
        const url = this.getApiUrl("nudge_list");
        return this.http
            .get<SuprModels.Nudge[]>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "nudges")));
    }

    getChatRoutingData(): Observable<any> {
        const url = this.getApiUrl("chat_routing", {}, ApiService.cxHostUrl);

        return this.http
            .get<SuprModels.Nudge[]>(url, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    deleteCampaignPopup(userId: number, type: string): Observable<void> {
        const url = this.getApiUrl("nudge_list");

        const body = {
            customer_id: userId,
            nudge_type: type,
        };

        return this.http.put<void>(url, body, {
            params: new SuprHttpParams({ silent: true }),
        });
    }

    selfServeGet(SelfServeRequest: SuprApi.SelfServeRequest): Observable<void> {
        const url = this.getApiUrl("SELF_SERVE_GET", {}, ApiService.cxHostUrl);

        return this.http
            .post<any>(url, SelfServeRequest, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    complaintCreation(
        complaintRequest: SuprApi.ComplaintCreationRequest
    ): Observable<void> {
        const url = this.getApiUrl(
            "COMPLAINT_CREATION",
            {},
            ApiService.cxHostUrl
        );
        return this.http
            .post<any>(url, complaintRequest, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    isImageUpload(
        complaintRequest: SuprApi.ComplaintCreationRequest
    ): Observable<void> {
        const url = this.getApiUrl("IS_IMAGE_UPLOAD", {}, ApiService.cxHostUrl);
        return this.http
            .post<any>(url, complaintRequest, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    imageUpload(file: File, interaction_id: string): Observable<void> {
        const uploadData = new FormData();
        uploadData.append("file", file, file.name);
        uploadData.append("interaction_id", interaction_id);
        const url = this.getApiUrl("IMAGE_UPLOAD", {}, ApiService.cxHostUrl);
        return this.http
            .post<any>(url, uploadData, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    checkVersion(
        vesionCheckObj: SuprApi.VersionCheckBody,
        silent = false
    ): Observable<String> {
        const url = this.getApiUrl("version", {
            service: "AppApi",
        });

        return this.http
            .post<SuprApi.VersionCheckRes>(url, vesionCheckObj, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "status")));
    }

    fetchCategoryList(silent = false): Observable<SuprModels.CategoryMeta[]> {
        const urlKey = this.getUrlKey("category");
        const url = this.getApiUrl(urlKey);
        return this.http
            .get<SuprModels.Category[]>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "categories")));
    }

    fetchCategoryDetails(categoryId: number): Observable<SuprModels.Category> {
        const urlKey = this.getUrlKey("category");
        const url = this.getApiUrl(urlKey, {
            urlParams: categoryId,
        });
        return this.http
            .get<SuprModels.Category>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "category")));
    }

    fetchFeaturedListItems(
        payload: SuprModels.Featured.Payload
    ): Observable<SuprModels.Featured.List> {
        const { component, component_id, silent = false } = payload;
        const urlKey = this.getUrlKey("featured_list_items");
        const url = this.getApiUrl(urlKey, {
            queryParams: {
                component,
                component_id,
            },
        });

        return this.http
            .get<SuprApi.FeaturedRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "featured")));
    }

    fetchEssentialList(silent = false): Observable<SuprModels.Category[]> {
        const urlKey = this.getUrlKey("essential_categories");
        const url = this.getApiUrl(urlKey);
        return this.http
            .get<SuprApi.EssentialsRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "essentials")));
    }

    fetchCollectionList(
        componentName = "home",
        silent = false
    ): Observable<SuprModels.Collection[]> {
        const urlKey = this.getUrlKey("collection_list");
        const url = this.getApiUrl(urlKey, {
            queryParams: {
                component: componentName,
            },
        });

        return this.http
            .get<SuprModels.Collection[]>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "collections")));
    }

    fetchHomeLayoutList(
        componentName = "home",
        silent = false
    ): Observable<SuprModels.V3Layout[]> {
        const urlKey = this.getUrlKey("home_layout");
        const url = this.getApiUrl(urlKey, {
            queryParams: {
                component: componentName,
            },
        });

        // console.log({ url }, { silent });

        // return of(this.fetchModelDataFromRes(UI_MERCH_LAT, "collections"));

        return this.http
            .get<SuprModels.V3Layout[]>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "collections")));
    }

    fetchCollectionDetails(
        viewId: number
    ): Observable<SuprModels.CollectionDetail> {
        const urlKey = this.getUrlKey("collection_details");
        const url = this.getApiUrl(urlKey, {
            urlParams: viewId,
        });
        // return of(this.fetchModelDataFromRes(UI_MERCH_LAT, "collection"));

        return this.http
            .get<SuprModels.CollectionDetail>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "collection")));
    }

    fetchMileStone(): Observable<SuprModels.MileStoneHome> {
        const urlKey = this.getUrlKey("milestone_summary");
        const url = this.getApiUrl(urlKey);

        return this.http
            .get<SuprModels.MileStoneHome>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "milestone")));
    }

    fetchMileStoneRewardsHistory(): Observable<SuprModels.MilestonePastRewards> {
        const urlKey = this.getUrlKey("milestone_rewards_history");
        const url = this.getApiUrl(urlKey);

        return this.http
            .get<SuprModels.MilestonePastRewards>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchMileStoneDetail(): Observable<SuprModels.MileStoneRewards> {
        const urlKey = this.getUrlKey("milestone_details");
        const url = this.getApiUrl(urlKey);
        // return of(this.fetchModelDataFromRes(MILESTONE_DETAILS, "milestone"));
        return this.http
            .get<SuprModels.MileStoneHome>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchPastOrderSkuList(): Observable<Array<number>> {
        const url = this.getApiUrl("past_order");
        return this.http
            .get<SuprModels.CollectionDetail>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "skuList")));
    }

    postStartMilestone(
        startObj: SuprApi.MilestoneStartActionBody
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("start_milestone");
        const body = startObj;
        return this.http.post<SuprApi.ApiRes>(url, body);
    }

    createAddon(addonObj: SuprApi.AddonBody): Observable<SuprApi.AddonRes> {
        const url = this.getApiUrl("addon");
        const body = addonObj;
        return this.http
            .post<SuprApi.AddonRes>(url, body)
            .pipe(map((res) => this.convertAddonRes(res)));
    }

    cancelAddon(addonId: number): Observable<SuprModels.Addon> {
        const url = this.getApiUrl("addon", {
            urlParams: addonId,
        });

        return this.http
            .delete<SuprApi.CancelAddonRes>(url)
            .pipe(map((res) => this.fetchCancelAddonFromRes(res)));
    }

    updateAddon(
        addonId: number,
        quantity: number
    ): Observable<SuprModels.Addon> {
        const url = this.getApiUrl("addon", {
            urlParams: addonId,
        });

        return this.http
            .put<SuprApi.UpdateAddonRes>(url, { quantity })
            .pipe(map((res) => this.fetchUpdateAddonFromRes(res)));
    }

    fetchSubscriptions(): Observable<SuprModels.Subscription[]> {
        const url = this.getApiUrl("subscription", {
            queryParams: {
                active: true,
            },
        });
        return this.http
            .get<SuprApi.SubscriptionRes[]>(url)
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "subscriptions"))
            );
    }

    fetchSubscriptionTransactions(
        subscriptionId: number
    ): Observable<SuprModels.SubscriptionTransaction[]> {
        const url = this.getApiUrl("subscription_transactions", {
            urlParams: subscriptionId,
        });
        return this.http
            .get<SuprApi.SubscriptionTransactionsRes[]>(url)
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "transactions"))
            );
    }

    updateSubscription(
        subscriptionId: number,
        data: SuprApi.SubscriptionBody
    ): Observable<SuprModels.Subscription> {
        const body = data,
            url = this.getApiUrl("subscription", {
                urlParams: subscriptionId,
            });

        return this.http
            .put<SuprApi.SubscriptionRes>(url, body)
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "subscription"))
            );
    }

    cancelSubscription(
        subscriptionId: number
    ): Observable<SuprModels.Subscription> {
        const url = this.getApiUrl("subscription", {
            urlParams: subscriptionId,
        });

        return this.http
            .delete<SuprApi.SubscriptionRes>(url)
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "subscription"))
            );
    }

    updateSubscriptionInstruction(
        subscriptionId: number,
        data: SuprApi.SubscriptionInstructionBody
    ): Observable<SuprApi.UpdateSubscriptionInstructionResData> {
        const url = this.getApiUrl("subscription_instruction", {
            urlParams: subscriptionId,
        });
        const body = data;
        return this.http
            .put<SuprApi.SubscriptionInstructionRes>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchVacation(): Observable<SuprModels.VacationApiRes> {
        const url = this.getApiUrl("vacation");
        return this.http
            .get<SuprApi.VacationRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    createVacation(
        vacationObj: SuprModels.Vacation
    ): Observable<SuprModels.Vacation> {
        const url = this.getApiUrl("vacation");

        return this.http
            .post<SuprApi.VacationRes>(url, vacationObj)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "vacation")));
    }

    cancelVaction(): Observable<SuprModels.Vacation> {
        const url = this.getApiUrl("vacation");

        return this.http
            .delete<SuprApi.VacationRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "vacation")));
    }

    fetchOrderHistory(
        toDate: string,
        days: number
    ): Observable<SuprApi.OrderHistoryData> {
        const url = this.getApiUrl("order_history", {
            queryParams: {
                to_date: toDate,
                num_days: "" + days,
            },
        });

        return this.http
            .get<SuprApi.OrderHistoryRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    validateCart(cartObj: SuprApi.CartBody): Observable<SuprApi.CartRes> {
        const url = this.getApiUrl("cart_validate");
        return this.http.post<SuprApi.CartRes>(url, cartObj);
    }

    checkoutCart(cartObj: SuprApi.CartBody): Observable<SuprApi.CheckoutRes> {
        const url = this.getApiUrl("cart_checkout");
        return this.http.post<SuprApi.CheckoutRes>(url, cartObj);
    }

    fetchAddress(): Observable<SuprModels.Address> {
        const url = this.getApiUrl("address");
        return this.http
            .get<SuprApi.AddressRes>(url)
            .pipe(map((res) => this.fetchAddressFromRes(res)));
    }

    createAddress(
        addressObj: SuprApi.AddressBody,
        meta = {}
    ): Observable<SuprModels.Address> {
        const url = this.getApiUrl("create_address");
        const body = addressObj;
        return this.http
            .post<SuprApi.AddressRes>(url, body, {
                params: new SuprHttpParams({ meta }),
            })
            .pipe(map((res) => this.fetchAddressFromRes(res)));
    }

    searchPremise(
        premiseObj: SuprApi.PremiseSearchReq
    ): Observable<SuprApi.PremiseSearchRes[]> {
        const cacheData = <SuprApi.PremiseSearchRes[]>(
            this.dbService.getLocalData(LOCAL_DB_DATA_KEYS.PREMISE_SEARCH_LIST)
        );

        const cacheParams = <
            {
                cityId: number;
                latitude?: number;
                longitude?: number;
                searchTerm?: string;
            }
        >this.dbService.getLocalData(LOCAL_DB_DATA_KEYS.PREMISE_SEARCH_PARAMS);

        if (
            cacheData &&
            cacheParams.cityId === premiseObj.cityId &&
            cacheParams.latitude === premiseObj.latitude &&
            cacheParams.longitude === premiseObj.longitude &&
            cacheParams.searchTerm.trim() === premiseObj.searchTerm.trim()
        ) {
            return of(cacheData);
        }

        const queryParams = {
            cityId: premiseObj.cityId,
            latitude: premiseObj.latitude,
            longitude: premiseObj.longitude,
            searchTerm: premiseObj.searchTerm,
        };

        const url = this.getApiUrl("premise_search", {
            queryParams,
        });

        return this.http.get<SuprApi.PremiseSearchRes>(url).pipe(
            map((res) => this.searchPremiseSearch(res)),
            tap((itemList: SuprApi.PremiseSearchRes[]) => {
                this.dbService.setLocalData(
                    LOCAL_DB_DATA_KEYS.PREMISE_SEARCH_PARAMS,
                    queryParams
                );

                this.dbService.setLocalData(
                    LOCAL_DB_DATA_KEYS.PREMISE_SEARCH_LIST,
                    itemList
                );
            })
        );
    }

    searchPopularPremise(
        premiseObj: SuprApi.PremiseSearchReq
    ): Observable<SuprApi.PremiseSearchRes[]> {
        const cacheParams = <{ cityId: number }>(
            this.dbService.getLocalData(
                LOCAL_DB_DATA_KEYS.PREMISE_SEARCH_PARAMS
            )
        );

        const cacheData = <SuprApi.PremiseSearchRes[]>(
            this.dbService.getLocalData(
                LOCAL_DB_DATA_KEYS.POPULAR_PREMISE_SEARCH_LIST
            )
        );

        if (
            cacheData &&
            this.utilService.getNestedValue(cacheParams, "cityId") ===
                this.utilService.getNestedValue(premiseObj, "cityId")
        ) {
            return of(cacheData);
        }

        const url = this.getApiUrl("premise_popular_search", {
            queryParams: {
                cityId: this.utilService.getNestedValue(premiseObj, "cityId"),
            },
        });

        return this.http.get<SuprApi.PremiseSearchRes>(url).pipe(
            map((res) => this.searchPremiseSearch(res)),
            tap((itemList: SuprApi.PremiseSearchRes[]) =>
                this.dbService.setLocalData(
                    LOCAL_DB_DATA_KEYS.POPULAR_PREMISE_SEARCH_LIST,
                    itemList
                )
            )
        );
    }

    searchPremiseAddress(id: number): Observable<SuprApi.PremiseAddressRes> {
        const url = this.getApiUrl("premise_address") + id;
        return this.http
            .get<SuprApi.PremiseAddressRes>(url)
            .pipe(map((res) => this.searchPremiseAddressSearch(res)));
    }

    fetchSchedule(
        subscriptionId?: number
    ): Observable<SuprModels.ScheduleApiData> {
        const queryParams = subscriptionId
            ? { subscription_id: subscriptionId }
            : {};
        const url = this.getApiUrl("schedule", {
            queryParams,
        });

        return this.http
            .get<SuprApi.ScheduleRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchSkuList(silent = false): Observable<SuprModels.Sku[]> {
        const urlKey = this.getUrlKey("sku");
        const url = this.getApiUrl(urlKey);

        return this.http
            .get<SuprModels.Sku[]>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "skuList")));
    }

    fetchProfile(
        queryParams: SuprApi.ProfileQueryParams = {}
    ): Observable<SuprApi.ProfileResData> {
        const url = this.getApiUrl("profile", {
            queryParams: { ...queryParams },
        });

        return this.http.get<SuprApi.ProfileResData>(url).pipe(
            map((res) => this.fetchModelDataFromRes(res)),
            catchError((error: HttpErrorResponse) => {
                if (error && error.status === 401) {
                    return of(EMPTY);
                }

                return throwError(error);
            })
        );
    }

    updateProfile(
        userObj: SuprModels.User,
        mobileNo: string
    ): Observable<SuprApi.ProfileResData> {
        const url = this.getApiUrl("profile", { urlParams: mobileNo });

        const body = userObj as SuprModels.User;
        return this.http.put<SuprModels.User>(url, body).pipe(
            map((res) => this.fetchModelDataFromRes(res)),
            catchError((error: HttpErrorResponse) => {
                if (error && error.status === 401) {
                    return of(EMPTY);
                }

                return throwError(error);
            })
        );
    }

    createRequest(
        data: SuprApi.CreateRequestBody,
        silent: boolean = true
    ): Observable<SuprApi.CreateRequestRes> {
        const url = this.getApiUrl("request");
        const body = data;
        return this.http.post<SuprApi.CreateRequestRes>(url, body, {
            params: new SuprHttpParams({ silent }),
        });
    }

    updateSkuOOSInfo(
        data: SuprApi.UpdateSkuOOSInfoRequestBody,
        silent: boolean = true
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("sku_notify");
        const body = data;
        return this.http.post<SuprApi.ApiRes>(url, body, {
            params: new SuprHttpParams({ silent }),
        });
    }

    fetchWalletBalance(): Observable<number> {
        const url = this.getApiUrl("wallet_balance");
        return this.http.get<number>(url).pipe(map((res) => res[0]));
    }

    fetchWalletBalanceV2(): Observable<SuprApi.BalanceRes> {
        const url = this.getApiUrl("wallet_balance_v2");
        return this.http
            .get<SuprApi.BalanceRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchWalletTransactions(): Observable<SuprModels.Transaction[]> {
        const url = this.getApiUrl("wallet_transactions");
        return this.http.get<SuprModels.Transaction[]>(url);
    }

    fetchWalletTransactionDetails(): Observable<SuprModels.TransactionDetails> {
        const url = this.getApiUrl("wallet_transactions_v2");
        return this.http
            .get<SuprModels.TransactionDetails>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchWalletOffers(
        amount = 0,
        useCustomErrorHandler = true
    ): Observable<SuprModels.OffersList> {
        const url = this.getApiUrl("payment_offers");
        const body = { amount };

        return this.http
            .post<SuprApi.WalletOffersDiscoveryRes>(url, body, {
                params: new SuprHttpParams({ useCustomErrorHandler }),
            })
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "offer_sections"))
            );
    }

    fetchCustomPaymentCustomerAndOrderId(
        reqBody: RazorpayCustomPaymentOrderCreateReq,
        useCustomErrorHandler = false
    ): Observable<RazorpayCustomPaymentOrderData> {
        const url = this.getApiUrl("custom_payment_initiate");
        const body = { ...reqBody };
        return this.http
            .post<SuprApi.ApiRes>(url, body, {
                params: new SuprHttpParams({ useCustomErrorHandler }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    captureCustomPayment(
        body: SuprApi.CustomPaymentCaptureReq
    ): Observable<SuprApi.PaymentRes> {
        const url = this.getApiUrl("custom_payment_capture");
        return this.http.post<SuprApi.PaymentRes>(url, body);
    }

    fetchAutoDebitModes(
        silent = true
    ): Observable<SuprModels.AutoDebitPaymentModes> {
        const url = this.getApiUrl("autodebit_payment_method");
        return this.http
            .get<SuprApi.ApiRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchAutoDebitCustomerAndOrderId(
        amount: number
    ): Observable<RazorpayRecurringPaymentOrderData> {
        const url = this.getApiUrl("autodebit_payment_order");
        const body = { amount };
        return this.http
            .post<SuprApi.ApiRes>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    verifySuprPassRecurringPayment(
        reqObj: Partial<SuprApi.RecurringPaymentReq>
    ): Observable<SuprApi.RecurringPaymentRes> {
        const url = this.getApiUrl("autodebit_setup");
        return this.http
            .post<SuprApi.ApiRes>(url, reqObj)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    cancelSuprPassAutoDebit() {
        const url = this.getApiUrl("cancel_autodebit");
        return this.http
            .post<SuprApi.ApiRes>(url, {})
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    createPayment(
        paymentId: string,
        amount: number,
        couponCode?: string,
        buySuprCredits?: boolean
    ): Observable<SuprApi.PaymentRes> {
        const url = this.getApiUrl("payment_gateway");
        const body = {
            payment_id: paymentId,
            amount: this.utilService.toValue(amount),
            coupon_code: couponCode,
            razorpay: "NEW",
            buy_supr_credits: buySuprCredits,
        };
        return this.http.post<SuprApi.PaymentRes>(url, body);
    }

    applyRechargeCoupon(
        couponCode: string,
        amount: number
    ): Observable<SuprModels.RechargeCouponInfo> {
        const url = this.getApiUrl("recharge_coupon_validate");
        const body = {
            couponCode: couponCode,
            amount: this.utilService.toValue(amount),
        };
        return this.http
            .post<SuprModels.RechargeCouponInfo>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchRechargeCouponOffers(): Observable<SuprModels.RechargeCouponOffer[]> {
        const url = this.getApiUrl("recharge_coupon_offers");
        return this.http
            .get<SuprModels.RechargeCouponOffer[]>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    sendLoginOtp(
        body: SuprApi.PasswordReqBody
    ): Observable<SuprApi.PasswordRes> {
        const url = this.getApiUrl("password");

        return this.http.post<SuprApi.PasswordRes>(url, body);
    }

    verifyLoginOtp(mobileNo: string, otp: string): Observable<SuprModels.Auth> {
        const url = this.getApiUrl("login");
        const body = { username: mobileNo, otp };
        return this.http.post<SuprModels.Auth>(url, body);
    }

    fetchSelfServeStaticFlows(): Observable<SuprModels.Faq> {
        const url = this.getApiUrl("SELF_SERVE_STATIC", {
            service: "AppApi",
        });

        return this.http
            .get<SuprApi.FaqRes>(url, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map(this.fetchFaqFromRes));
    }

    getLocationStatus(
        location: SuprMaps.Location
    ): Observable<SuprApi.LocationStatusRes> {
        const url = this.getApiUrl("location_status", {
            queryParams: {
                latitude: location.lat,
                longitude: location.lng,
            },
        });
        return this.http.get<SuprApi.LocationStatusRes>(url);
    }

    reverseGeocode(
        geocodeInput: SuprMaps.GeocodeInput
    ): Observable<SuprApi.GeoCodeRes> {
        const queryParams = {
            latlng: geocodeInput.latlng,
            place_id: geocodeInput.place_id,
            address: geocodeInput.address,
        };
        const url = this.getApiUrl("reverse_geocode", {
            service: "AppApi",
            queryParams,
        });
        return this.http.get<SuprApi.GeoCodeRes>(url);
    }

    getLocationSuggestions(
        input: string
    ): Observable<SuprApi.PlaceSuggestionRes> {
        const url = this.getApiUrl("location_suggestions", {
            service: "AppApi",
            queryParams: {
                input,
            },
        });
        return this.http.get<SuprApi.PlaceSuggestionRes>(url);
    }

    fetchDeliveryStatus(silent = false): Observable<SuprModels.DeliveryStatus> {
        const url = this.getApiUrl("delivery_status");

        return this.http
            .get<SuprApi.DeliveryStatusRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchRatingOptions(): Observable<SuprModels.RatingOption[]> {
        const url = this.getApiUrl("feedback_options");
        return this.http
            .get<SuprApi.RatingOptionsRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "options")));
    }

    saveRatingFeedback(
        ratingActionObj: SuprApi.RatingFeedbackBody
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("feedback");
        const body = ratingActionObj;
        return this.http.post<SuprApi.ApiRes>(url, body);
    }

    updateRatingAction(
        ratingActionObj: SuprApi.RatingActionBody
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("feedback_action");
        const body = ratingActionObj;
        return this.http.post<SuprApi.ApiRes>(url, body);
    }

    fetchOverallOrderFeedbackRequest(userId: number) {
        const url = this.getApiUrl(
            "OVERALL_ORDER_FEEDBACK",
            {
                queryParams: {
                    customer_id: userId,
                    feedback_entity_type: "Overall_Order",
                },
            },
            ApiService.cxHostUrl
        );
        return this.http
            .get<SuprModels.Feedback.deliveryFeedbackRequest[]>(url, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchOrderFeedbackRequest(userId: number) {
        const url = this.getApiUrl(
            "ORDER_FEEDBACK",
            {
                queryParams: {
                    customer_id: userId,
                    feedback_entity_type: "Order",
                },
            },
            ApiService.cxHostUrl
        );
        return this.http
            .get<SuprModels.Feedback.OrderFeedback[]>(url, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    submitFeedbackResponse(data: SuprModels.Feedback.FeedbackSubmit) {
        const url = this.getApiUrl(
            "ORDER_FEEDBACK_RESPONSE",
            {},
            ApiService.cxHostUrl
        );
        return this.http.post<any>(url, data, {
            params: new SuprHttpParams({ silent: true }),
        });
    }

    fetchSimilarProducts(skuId: number): Observable<number[]> {
        const url = this.getApiUrl("similar_products", {
            urlParams: skuId,
        });

        return this.http
            .get<SuprApi.SimilarProductsRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "skuList")));
    }

    fetchSkuAttributes(): Observable<SuprModels.SkuAttributes> {
        const url = this.getApiUrl("sku_attributes");

        return this.http
            .get<SuprModels.SkuAttributes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res, "skuList")));
    }

    fetchSuprPassInfo(silent = false): Observable<SuprModels.SuprPassInfo> {
        const url = this.getApiUrl("supr_pass_info");

        return this.http
            .get<SuprApi.SuprPassInfoRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchSuprPassSavingsDetails(
        silent = false
    ): Observable<SuprModels.SuprPassSavingsInfo> {
        const url = this.getApiUrl("supr_pass_savings_info");

        return this.http
            .get<SuprApi.SuprPassInfoRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchReferralInfo(silent = false): Observable<SuprModels.ReferralInfo> {
        const url = this.getApiUrl("referral");
        return this.http
            .get<SuprApi.ReferralInfoRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchRewardsInfo(): Observable<SuprModels.RewardsInfo> {
        const url = this.getApiUrl("rewards");
        return this.http
            .get<SuprApi.RewardsInfoRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchOfferDetails(offerName: string): Observable<SuprModels.OfferDetails> {
        const url = this.getApiUrl("offer_details", {
            queryParams: { offerName },
        });
        return this.http
            .get<SuprApi.OfferDetailsRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchScratchCardBenefit(
        id: number
    ): Observable<SuprModels.ScratchedCardDetails> {
        const url = this.getApiUrl("rewards");
        const body = {
            id: id,
        };
        return this.http
            .post<SuprModels.ScratchedCardDetails>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    applyReferralcode(
        customerId: number,
        referralCode: string
    ): Observable<SuprApi.ApplyReferralCodeRes> {
        const url = this.getApiUrl("referral");
        const body = {
            customerId: customerId,
            referralCode: referralCode,
        };
        return this.http
            .post<SuprApi.ApplyReferralCodeRes>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    filterReferralContacts(
        contacts: string[]
    ): Observable<SuprApi.FilterReferralContactsRes> {
        const url = this.getApiUrl("referral_filter_contacts");
        const body = {
            contacts: contacts,
        };
        return this.http
            .post<SuprApi.ApplyReferralCodeRes>(url, body)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchComplaintsDataV2(date: string, num_days: number, type?: string) {
        const url = this.getApiUrl(
            "COMPLAINT_LIST_V2",
            {
                queryParams: {
                    date,
                    num_days,
                    type,
                },
            },
            ApiService.cxHostUrl
        );
        return this.http
            .get<SuprApi.ComplaintListV2Res>(url, {
                params: new SuprHttpParams({ silent: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchContactSupport() {
        const url = this.getApiUrl("CONTACT_SUPPORT", {}, ApiService.cxHostUrl);
        return this.http
            .get<SuprApi.ComplaintListV2Res>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    fetchPaymentMethods(
        useCustomErrorHandler = false,
        reqBody: SuprApi.PaymentMethodsReqParams = {}
    ): Observable<SuprApi.PaymentMethodsResData> {
        const url = this.getApiUrl("payment_methods", {
            queryParams: {
                ...reqBody,
            },
        });

        return this.http
            .get<SuprApi.PaymentMethodsRes>(url, {
                params: new SuprHttpParams({ useCustomErrorHandler }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    searchForSkus(
        searchParams: SkuSearchParams,
        silent = false
    ): Observable<SuprModels.SkuSearchResponse> {
        const url = this.getApiUrl("search_sku", {
            queryParams: { ...searchParams },
        });

        return this.http
            .get<SuprApi.SkuSeachRes>(url, {
                params: new SuprHttpParams({ silent }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    sendSearchInstrumentation(data: SuprModels.SearchInstrumentation) {
        const url = this.getApiUrl("search_instrumentation");

        return this.http.post<any>(url, data, {
            params: new SuprHttpParams({ silent: true }),
        });
    }

    fetchCartOffers(
        cartObj: SuprApi.CartBody,
        useCustomErrorHandler = true
    ): Observable<SuprModels.OffersList> {
        const url = this.getApiUrl("cart_offer_discovery");

        return this.http
            .post<SuprApi.CartOffersDiscoveryRes>(url, cartObj, {
                params: new SuprHttpParams({ useCustomErrorHandler }),
            })
            .pipe(
                map((res) => this.fetchModelDataFromRes(res, "offer_sections"))
            );
    }

    fetchPopularSkuSearches(): Observable<SuprModels.PopularSkuSearch[]> {
        const url = this.getApiUrl("popular_sku_search");

        return this.http
            .get<any>(url, {
                params: new SuprHttpParams({ useCustomErrorHandler: true }),
            })
            .pipe(map((res) => this.fetchModelDataFromRes(res, "searches")));
    }

    fetchDeliveryProof(
        queryParams: SuprApi.DeliveryProofQueryParams
    ): Observable<SuprModels.DeliveryProof[]> {
        const url = this.getApiUrl("delivery_proof", {
            queryParams: { ...queryParams },
        });

        return this.http
            .get<SuprApi.DeliveryProofRes>(url)
            .pipe(map((res) => this.fetchModelDataFromRes(res)));
    }

    sendDeliveryProofFeedback(
        data: SuprApi.DeliveryProofFeedbackPayload
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("delivery_proof_feedback");

        return this.http.post<SuprApi.ApiRes>(url, data, {
            params: new SuprHttpParams({ silent: true }),
        });
    }

    reportDeliveryProofImage(
        data: SuprApi.DeliveryProofReportPayload
    ): Observable<SuprApi.ApiRes> {
        const url = this.getApiUrl("delivery_proof_report");

        return this.http.post<SuprApi.ApiRes>(url, data);
    }

    private isAnonymousUser() {
        return this.userService.getIsAnonymousUser();
    }

    // THis method decides whether to use browse keys or default keys
    private getUrlKey(key: string) {
        return this.isAnonymousUser() ? `browse_${key}` : key;
    }

    private getApiUrl(
        key: string,
        extras?: SuprApi.ReqExtras,
        baseUrl?: string
    ): string {
        const urlEndpoint = API_ROUTES[key.toUpperCase()];
        let paramsStr = "";
        let queryStr = "";

        if (extras) {
            if (extras.urlParams) {
                paramsStr = extras.urlParams + "/";
            }

            if (extras.queryParams) {
                const params = extras.queryParams;
                queryStr = Object.keys(params)
                    .map((name) =>
                        params[name]
                            ? `${name}=${
                                  Array.isArray(params[name])
                                      ? params[name].toString()
                                      : params[name]
                              }`
                            : ""
                    )
                    .filter((kV) => !!kV)
                    .join("&");
                queryStr = "?" + queryStr;
            }

            if (extras.service === "AppApi") {
                return (
                    ApiService.appApiHostUrl +
                    urlEndpoint +
                    paramsStr +
                    queryStr
                );
            }
        }

        if (baseUrl) {
            return baseUrl + urlEndpoint + paramsStr + queryStr;
        }

        return ApiService.apiHostUrl + urlEndpoint + paramsStr + queryStr;
    }

    private convertAddonRes(res: any): SuprApi.AddonRes {
        const { wallet_balance, collect_feedback, ...schedule } = res;
        return {
            wallet_balance,
            schedule,
            collect_feedback,
        };
    }

    private fetchFaqFromRes(res: SuprApi.FaqRes): SuprModels.Faq {
        if (!res || res.statusCode !== 0) {
            const message =
                res && res.statusMessage
                    ? res.statusMessage
                    : "Oops something went wrong";
            throw { statusCode: res.statusCode, message };
        } else {
            return res.data;
        }
    }

    private fetchAddressFromRes(res: SuprApi.AddressRes): SuprModels.Address {
        if (!res || res.statusCode !== 0) {
            const message =
                res && res.statusMessage
                    ? res.statusMessage
                    : "Oops something went wrong";

            throw { statusCode: res.statusCode, message };
        } else {
            return res.data;
        }
    }

    private fetchModelDataFromRes(res: any, key?: string): any {
        if (this.isSuccessResponse(res)) {
            return key ? res.data[key] : res.data;
        }
    }

    private fetchCancelAddonFromRes(
        res: SuprApi.CancelAddonRes
    ): SuprModels.Addon {
        if (this.isSuccessResponse(res)) {
            return res.data.addon;
        }
    }

    private fetchUpdateAddonFromRes(
        res: SuprApi.UpdateAddonRes
    ): SuprModels.Addon {
        if (this.isSuccessResponse(res)) {
            return res.data.addon;
        }
    }

    private isSuccessResponse(res: any): boolean {
        if (!res || res.statusCode !== 0) {
            const message =
                res && res.statusMessage
                    ? res.statusMessage
                    : "Oops something went wrong";

            throw { statusCode: res.statusCode, message };
        }

        return true;
    }

    private searchPremiseSearch(res: any) {
        if (this.isSuccessResponse(res)) {
            if (res.data.premises.length > SEARCH_TRUNCATE_LENGTH) {
                return res.data.premises.slice(0, SEARCH_TRUNCATE_LENGTH);
            }

            return res.data.premises;
        } else {
            return [];
        }
    }

    private searchPremiseAddressSearch(res) {
        if (this.isSuccessResponse(res)) {
            return res.data.premise;
        } else {
            return {};
        }
    }
}
