import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import * as SuprModels from "@models";
import { SuprApi, GlobalError } from "@types";

import { StoreState } from "@store/store.state";
import * as SkuStoreActions from "@store/sku/sku.actions";
import * as SkuV3CollectionStoreActions from "@store/home-layout/home-layout.actions";
import * as VacationStoreActions from "@store/vacation/vacation.actions";
import * as DeliveryStatusStoreActions from "@store/delivery-status/delivery-status.actions";
import * as CartStoreActions from "@store/cart/cart.actions";
import * as AuthStoreActions from "@store/auth/auth.actions";
import * as MiscStoreActions from "@store/misc/misc.actions";
import * as PnStoreActions from "@store/pn/pn.actions";
import * as ErrorStoreActions from "@store/error/error.actions";
import * as ScheduleStoreActions from "@store/schedule/schedule.actions";
import * as AddressStoreActions from "@store/address/address.actions";
import * as RatingStoreActions from "@store/rating/rating.actions";
import * as UserStoreActions from "@store/user/user.actions";
import * as FeedbacktoreActions from "@store/feedback/feedback.actions";
import * as ReferralStoreActions from "@store/referral/referral.actions";
import { PNData } from "@store/pn/pn.state";

@Injectable({
    providedIn: "root",
})
export class StoreService {
    constructor(private store: Store<StoreState>) {}

    updateSkuStore(skuList: SuprModels.Sku[]) {
        this.store.dispatch(
            new SkuStoreActions.FetchSkuListRequestSuccessAction(skuList)
        );
    }

    updateSkuAttributesStore(skuAttributes: SuprModels.SkuAttributes) {
        this.store.dispatch(
            new SkuStoreActions.FetchSkuAttributesRequestSuccessAction(
                skuAttributes
            )
        );
    }

    updateVacationStore(vacation: SuprModels.VacationApiRes) {
        this.store.dispatch(
            new VacationStoreActions.FetchVacationRequestSuccessAction(vacation)
        );
    }

    updateSchedule(scheduleApiData: SuprModels.ScheduleApiData) {
        this.store.dispatch(
            new ScheduleStoreActions.GetScheduleDataSuccessAction(
                scheduleApiData
            )
        );
    }

    togglePauseModal() {
        this.store.dispatch(new VacationStoreActions.ToggleVacationModal());
    }

    fetchDeliveryStatus(silent = false) {
        this.store.dispatch(
            new DeliveryStatusStoreActions.FetchDeliveryStatusAction({
                silent,
            })
        );
    }

    fetchSkuList(silent?: boolean) {
        this.store.dispatch(
            new SkuStoreActions.FetchSkuListRequestAction({
                refresh: true,
                silent,
            })
        );
    }

    fetchSkuV3CollectionList(silent?: boolean) {
        this.store.dispatch(
            new SkuV3CollectionStoreActions.FetchHomeLayoutListRequestAction(
                silent
            )
        );
    }

    fetchSkuAttributes(silent?: boolean) {
        this.store.dispatch(
            new SkuStoreActions.FetchSkuAttributesRequestAction({
                refresh: true,
                silent,
            })
        );
    }

    updateCartInStore(cartBody: SuprApi.CartBody) {
        this.store.dispatch(
            new CartStoreActions.UpdateCartAction({ cartBody })
        );
    }

    clearCart() {
        this.store.dispatch(new CartStoreActions.ClearCartAction());
    }

    logout() {
        this.store.dispatch(new AuthStoreActions.LogoutAction());
    }

    toggleAppLaunch() {
        this.store.dispatch(new MiscStoreActions.ToggleAppLaunchDone());
    }

    setSettingsFetched(settingsFetched: boolean) {
        this.store.dispatch(
            new MiscStoreActions.SetSettingsFetched(settingsFetched)
        );
    }

    setExperimentsFetched(experimentsFetched: boolean) {
        this.store.dispatch(
            new MiscStoreActions.SetExperimentsFetched(experimentsFetched)
        );
    }

    setGlobalError(error: GlobalError) {
        this.store.dispatch(new ErrorStoreActions.SetGlobalError({ error }));
    }

    checkCartDates() {
        this.store.dispatch(new CartStoreActions.AutoPushCartItems());
    }

    showAddressRetro() {
        this.store.dispatch(new AddressStoreActions.ShowAddressRetroModal());
    }

    showCampaignPopup() {
        this.store.dispatch(new MiscStoreActions.ShowCampaignPopupNudge());
    }

    hideCampaignPopup() {
        this.store.dispatch(new MiscStoreActions.HideCampaignPopupNudge());
    }

    showGenericNudge() {
        this.store.dispatch(new MiscStoreActions.ShowGenericNudge());
    }

    showAutoDebitPaymentFailureNudge() {
        this.store.dispatch(
            new MiscStoreActions.ShowAutoDebitPaymentFailureNudge()
        );
    }

    hideAutoDebitPaymentFailureNudge() {
        this.store.dispatch(
            new MiscStoreActions.HideAutoDebitPaymentFailureNudge()
        );
    }

    showBlockAccessNudge(showSkipBtn = false) {
        this.store.dispatch(
            new MiscStoreActions.ShowBlockAccessNudge({ showSkipBtn })
        );
    }

    hideBlockAccessNudge() {
        this.store.dispatch(new MiscStoreActions.HideBlockAccessNudge());
    }

    hideGenericNudge() {
        this.store.dispatch(new MiscStoreActions.HideGenericNudge());
    }

    showFeedback() {
        this.store.dispatch(new FeedbacktoreActions.setDeliveryFeedbackFlag());
    }

    showExitCartModal() {
        this.store.dispatch(new CartStoreActions.ShowExitCartModal());
    }

    showCheckoutDisabledNudge() {
        this.store.dispatch(new UserStoreActions.ShowCheckoutDisabledNudge());
    }

    showAppRatingModal(trigger: string) {
        this.store.dispatch(
            new RatingStoreActions.ShowRatingModal({ trigger })
        );
    }

    setPNData(data: PNData) {
        this.store.dispatch(new PnStoreActions.SetPNData(data));
    }

    setReferralCode(code: string) {
        this.store.dispatch(
            new ReferralStoreActions.SetReferralCode({ referralCode: code })
        );
    }
}
