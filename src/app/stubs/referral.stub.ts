import {
    ReferralInfo,
    Banner,
    ActivityBlock,
    BenefitsBanner,
} from "@shared/models";

export const COUPON_APPLIED_BANNER: Banner = {
    bgColor: null,
    content: [
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            text:
                "Use coupon code to get 50% off, upto Rs 100 on your first order.",
            title: "Success! Here’s your welcome code",
            textColor: "#333333",
            titleColor: "#F28D18",
            copyBlock: {
                copyText: {
                    text: "PROMOCODE",
                    textColor: "#962819",
                    fontSize: "12px",
                },
                bgColor: "#FEF3E7",
                iconColor: "#EE917C",
                borderColor: "#EA755B",
            },
        },
    ],
    title: null,
    border: {
        size: "1px",
        color: "#F2BC18",
        type: "solid",
        radius: "12px",
    },
    outerHeading: null,
    boxShadow: null,
};

export const BANNER = {
    bgColor: "#FFFDFB",
    content: [
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            text: "On placing their first successful order",
            title: "Your friend gets Rs 100 cashback",
            textColor: "#7C7C7C",
            titleColor: "#000000",
            highlightedText: "100 cashback",
            highlightedTextColor: "#EA755B",
        },
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            text: "On placing their first successful order",
            title: "Your friend gets Rs 100 cashback",
            textColor: "#7C7C7C",
            titleColor: "#000000",
            highlightedText: "100 cashback",
            highlightedTextColor: "#EA755B",
        },
    ],
    title: null,
    border: null,
    outerHeading: {
        text: "How referral works?",
        textColor: "#333333",
        fontSize: "16px",
    },
    boxShadow: "0px 1px 6px rgba(0, 0, 0, 0.15)",
};

export const BENEFITS: BenefitsBanner = {
    header: null,
    bgColor: null,
    content: [
        {
            header: {
                text: {
                    text: "You have referred 1 friend successfully!",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#50C1BA",
            },
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "You have been rewarded with",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "30 days",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            lowerFoldText: {
                text: "of free deliveries with Supr Access membership",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "view all rewards",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            ctaAction: {
                action: "route",
                appUrl: "/referral/rewards",
            },
            tag: {
                bgColor: "#6AC259",
                text: "Refund",
                textColor: "#fff",
                fontSize: "12px",
            },
            tagPosition: "right",
        },
        {
            header: {
                text: {
                    text: "Waiting for 6 friend(s) to place their 1st order",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#EA755B",
            },
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "Good going! Nishant",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "7 friend(s)",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            lowerFoldText: {
                text: "signed up with your code",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "Invite more friends",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            ctaAction: {
                action: "social-share",
            },
            tag: {
                bgColor: "#6AC259",
                text: "Refund",
                textColor: "#fff",
                fontSize: "12px",
            },
            tagPosition: "left",
        },
    ],
};

export const COMMON_BENEFITS = {
    header: {
        title: {
            text: "Why get Supr Access?",
            textColor: "#000000",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        subTitle: {
            text: "Here are other great reasons to get Supr Access",
            textColor: "#7C7C7C",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
        },
    },
    bgColor: "#EBFFF9",
    content: null,
    banner: {
        bgColor: null,
        content: [
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
                text:
                    "Order as many products as many times as you want, all for zero service fees.",
                title: "Enjoy unlimited free deliveries",
                textColor: "#7C7C7C",
                titleColor: "#000000",
                highlightedText: "100 cashback",
                highlightedTextColor: "#EA755B",
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
                text: "Supr Access members get priority access to stock!",
                title: "Reserved stock just for you",
                textColor: "#7C7C7C",
                titleColor: "#000000",
                highlightedText: "Supr Access",
                highlightedTextColor: "#EA755B",
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
                text:
                    "Supr Access members get 1 day Early Access to Sale Days and Offers",
                title: "Exclusive Offers",
                textColor: "#7C7C7C",
                titleColor: "#000000",
                highlightedText: "Supr Access",
                highlightedTextColor: "#EA755B",
            },
        ],
        title: null,
        border: null,
        outerHeading: null,
        boxShadow: null,
    },
};

export const FAQ = [
    {
        question: "Why should I get Supr Access?",
        answer:
            "If you’re a regular Supr Daily user, Supr Access is the best way to save on service fees. For as little as ₹99 for one month (that’s just ₹3 per day!), you can order as many groceries as you need, as many times as you want, with no service fees. Signing up for 3 or 12 months can help you save even more!",
    },
    {
        question:
            "I only order milk from Supr Daily. Do I need to get Supr Access?",
        answer:
            "If you order 1 litre of milk from us every day, that’s ₹60 in service fees per month. In addition to this, you would be charged ₹20 for each grocery delivery you may need. Instead, at just ₹99 per month, there will be no service fee on milk orders, or any groceries, fruits & veggies you may need to purchase. You can make Supr Daily your one stop solution for all your grocery needs, without having to worry about service fees again!",
    },
    {
        question:
            "I have already paid service fees for deliveries which are scheduled for the next month. What happens when I sign up for Supr Access?",
        answer:
            "When you sign up, service fees for any deliveries during the Supr Access period will automatically be refunded to your Supr Wallet. Let's say you have a subscription for 1 litre of milk daily, and you still have 15 deliveries remaining. If you sign up for Supr Access, the service fees for the remaining deliveries (₹30 in this case), will be automatically refunded into your Supr Wallet. Similarly, if you have already scheduled any one time deliveries for the duration of Supr Access, the service fees on all those orders will also be refunded automatically.",
    },
];

export const ACTIVITY_BLOCKS_PAST: ActivityBlock[] = [
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
    },
];

export const ACTIVITY_BLOCKS_UPCOMING = [
    {
        tag: {
            bgColor: "#F28D18",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Get 1 month membership on referring 5 friends",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: null,
        totalDays: null,
    },
];

export const REWARDS = {
    upcoming: ACTIVITY_BLOCKS_UPCOMING,
    past: ACTIVITY_BLOCKS_PAST,
};

export const REFERRAL_INFO: ReferralInfo = {
    eligibleToRefer: true,
    referralCouponBlock: {
        showCouponBlock: true,
        benefitText:
            "Enter the referral code here to get 100 Supr Credits instantly",
        // referralCodeAppliedBanner: COUPON_APPLIED_BANNER,
    },
    header: {
        bgColor: "rgba(249, 221, 140, 0.2)",
        logo: "",
        title: {
            text: "Refer your friends",
            textColor: "#EA755B",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        subTitle: {
            text: "Unlock savings together",
            textColor: "#333333",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
    },
    benefits: BENEFITS,
    banner: BANNER,
    faqs: FAQ,
    commonBenefits: COMMON_BENEFITS,
    rewards: REWARDS,
    numberOfSuccessfulReferrals: 5,
    activeReferralCode: "9988776655",
    shareBlock: {
        subject: "Share SuprDaily",
        message: "please share SuprDaily with your friends",
        url: "https://supr.onelink.me/gv4K/1846aebf",
        file: null,
    },
    successfulReferralMessage: {
        imgUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
        title: "Congratulations!",
        description: "You have successfully referred 5 friends.",
    },
};
