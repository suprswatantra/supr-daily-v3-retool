export const AUTO_DEBIT_PAYMENT_MODE_INFO = {
    methods: [
        {
            title: "Credit / Debit / ATM cards",
            subtitle: "Visa, MasterCard, Rupay & more",
            tags: [],
            icon: "card",
            method: "emandate",
        },
        {
            title: "Netbanking",
            subtitle: "All Indian banks",
            tags: [],
            icon: "receipt",
            method: "emandate",
        },
        {
            title: "Wallets",
            subtitle: "PhonePe & more",
            tags: [],
            icon: "wallet",
            method: "emandate",
        },
    ],
};
