import { SUPR_PASS_BLOCK_ACCESS } from "./../constants/settings.constants";
import { SuprPassInfo } from "@shared/models";

export const ACTIVE_PLAN_DETAILS = {
    purchaseId: 120113,
    endDate: "2020-06-19",
    daysLeft: 5,
    plan: {
        id: 1,
        durationDays: 30,
        durationMonths: 1,
        unitPrice: 99.0,
        unitMrp: 150.0,
        discountText: "SAVE 34%",
        isRecommended: false,
    },
};

export const BANNER_CONTENT = [
    {
        imgUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
        text:
            "Order as many times as you want. Other members pay ₹20 per delivery",
        title: "Unlimited Free Deliveries",
        textColor: "#7C7C7C",
        titleColor: "#000000",
        highlightedText: "₹20",
        highlightedTextColor: "#F28D18",
        tag: {
            bgColor: "",
            text: "",
            textColor: "",
            fontSize: "",
        },
    },
    {
        imgUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
        text: "Supr Access members gets served first on priority",
        title: "Reserved stock for you",
        textColor: "#7C7C7C",
        titleColor: "#000000",
        highlightedText: "",
        highlightedTextColor: "#F28D18",
        tag: {
            bgColor: "",
            text: "",
            textColor: "",
            fontSize: "",
        },
    },
    {
        imgUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
        text:
            "Supr Access members get 1 day Early Access to Sale Days and Offers",
        title: "Exclusive Offers",
        textColor: "#7C7C7C",
        titleColor: "#000000",
        highlightedText: "",
        highlightedTextColor: "#F28D18",
        tag: {
            bgColor: "#FBEBBA",
            text: "Coming Soon",
            textColor: "#B69C0C",
            fontSize: "10px",
        },
    },
];

export const BANNER = {
    bgColor: "#FFFDFB",
    content: BANNER_CONTENT,
    title: null,
    border: null,
    outerHeading: {
        text: "Perks you are enjoying",
        textColor: "#333333",
        fontSize: "16px",
    },
    boxShadow: "0px 1px 6px rgba(0, 0, 0, 0.15)",
};

export const ALERT_BANNER = {
    bgColor: "rgba(248, 235, 233, 0.7)",
    content: [
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            text:
                "You have been put on vacation for 2 days. Hence your Supr Access membership has been extended by 2 days.",
            title: null,
            textColor: "#333333",
            titleColor: null,
            highlightedText: null,
            highlightedTextColor: null,
            tag: null,
        },
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            text:
                "You have been put on vacation for 2 days. Hence your Supr Access membership has been extended by 2 days.",
            title: null,
            textColor: "#333333",
            titleColor: null,
            highlightedText: null,
            highlightedTextColor: null,
            tag: null,
        },
    ],
    title: null,
    border: null,
};

export const SUPR_PASS_PLANS = [
    {
        id: 1,
        durationDays: 30,
        durationMonths: 1,
        unitPrice: 99.0,
        unitMrp: 150.0,
        discountText: "SAVE 34%",
        isRecommended: false,
    },
    {
        id: 2,
        durationDays: 90,
        durationMonths: 3,
        unitPrice: 279.0,
        unitMrp: 450.0,
        discountText: "SAVE 38%",
        isRecommended: true,
    },
    {
        id: 3,
        durationDays: 365,
        durationMonths: 12,
        unitPrice: 999.0,
        unitMrp: 1800.0,
        discountText: "SAVE 45%",
        isRecommended: false,
    },
];

export const SUPR_PASS_BENEFITS = {
    header: {
        title: {
            text: "More reasons to buy",
            textColor: "#000000",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        subTitle: {
            text: "Here are other great reasons to become a member",
            textColor: "#7C7C7C",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
        },
    },
    bgColor: "#EBFFF9",
    content: [
        {
            header: {
                text: {
                    text: "You have had 7 deliveries in the last 30 days",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#EA755B",
            },
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "You have paid service fee of",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "₹590",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            lowerFoldText: {
                text: "In the last 30 days",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "BUY TO SAVE 90% FROM TODAY",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            tag: null,
        },
        {
            header: {
                text: {
                    text: "You’ve 2 active subscriptions & 4 one-time orders",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#50C1BA",
            },
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "You will be refunded",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "₹360",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            lowerFoldText: {
                text: "For exisiting subscription & orders",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "BUY TO SAVE 90% FROM TODAY",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            tag: {
                bgColor: "#6AC259",
                text: "Refund",
                textColor: "#fff",
                fontSize: "12px",
            },
        },
        {
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "Highest amount saved is",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "₹360",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            lowerFoldText: {
                text: "In your city",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "BUY TO SAVE 90% FROM TODAY",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
        },
    ],
};

export const FAQ = [
    {
        question: "Why should I get Supr Access?",
        answer:
            "If you’re a regular Supr Daily user, Supr Access is the best way to save on service fees. For as little as ₹99 for one month (that’s just ₹3 per day!), you can order as many groceries as you need, as many times as you want, with no service fees. Signing up for 3 or 12 months can help you save even more!",
    },
    {
        question:
            "I only order milk from Supr Daily. Do I need to get Supr Access?",
        answer:
            "If you order 1 litre of milk from us every day, that’s ₹60 in service fees per month. In addition to this, you would be charged ₹20 for each grocery delivery you may need. Instead, at just ₹99 per month, there will be no service fee on milk orders, or any groceries, fruits & veggies you may need to purchase. You can make Supr Daily your one stop solution for all your grocery needs, without having to worry about service fees again!",
    },
    {
        question:
            "I have already paid service fees for deliveries which are scheduled for the next month. What happens when I sign up for Supr Access?",
        answer:
            "When you sign up, service fees for any deliveries during the Supr Access period will automatically be refunded to your Supr Wallet. Let's say you have a subscription for 1 litre of milk daily, and you still have 15 deliveries remaining. If you sign up for Supr Access, the service fees for the remaining deliveries (₹30 in this case), will be automatically refunded into your Supr Wallet. Similarly, if you have already scheduled any one time deliveries for the duration of Supr Access, the service fees on all those orders will also be refunded automatically.",
    },
];

export const SUPR_PASS_AUTO_DEBIT_INFO = {
    isActive: false,
    selectionDataBlock: {
        title: "Recurring billing. Cancel anytime",
        description:
            "Subscription will be automatically renewed after 3 months. You’ll need to give one time-permission.",
        offers: [
            {
                offerText: "Get additional 10% off",
                offerAppliedText: "Additional 10% off applied",
                offerMrp: 90,
                planId: 1,
            },
            {
                offerText: "Get additional 10% off",
                offerAppliedText: "Additional 10% off applied",
                offerMrp: 225,
                planId: 2,
            },
            {
                offerText: "Get additional 10% off",
                offerAppliedText: "Additional 10% off applied",
                offerMrp: 900,
                planId: 3,
            },
        ],
    },
};
export const SUPR_PASS_ACTIVITY = [
    {
        tag: {
            bgColor: "#BC321F",
            textColor: "#FFFFFF",
            text: "Membership Cancelled",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been cancelled.",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-09",
        totalDays: -43,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "Vacation days added",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#BC321F",
            textColor: "#FFFFFF",
            text: "Membership Cancelled",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been cancelled.",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-02-09",
        totalDays: -43,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "Vacation days added",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-05-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#BC321F",
            textColor: "#FFFFFF",
            text: "Membership Cancelled",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been cancelled.",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-05-09",
        totalDays: -43,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "Vacation days added",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-09",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#BC321F",
            textColor: "#FFFFFF",
            text: "Membership Cancelled",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been cancelled.",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-02-09",
        totalDays: -43,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "Vacation days added",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-01-08",
        totalDays: 23,
    },
];

export const SUPR_PASS_SAVINGS_STUB = {
    totalSavings: 530,
    savingsList: [...SUPR_PASS_ACTIVITY],
};

export const SUPR_PASS_INFO: SuprPassInfo = {
    isActive: true,
    skuId: 9530,
    plans: SUPR_PASS_PLANS,
    activePlanDetails: ACTIVE_PLAN_DETAILS,
    expiredSuprPassData: null,
    // expiredSuprPassData: {
    //     endDate: "2020-06-14",
    //     purchaseId: 12345,
    // },
    faqs: FAQ,
    header: {
        bgColor: "#EBFFF9",
        logo: "",
        title: {
            text: "Introducing Supr Access",
            textColor: "#409A95",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        subTitle: {
            text: "No Service Fee. Period.",
            textColor: "#333333",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        infoText: {
            text: "Get membership at ₹99/month",
            textColor: "#333333",
            fontSize: "16px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        warningText: {
            text: "Ending soon",
            textColor: "#BC321F",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
        },
    },
    banner: BANNER,
    alertBanner: ALERT_BANNER,
    planSelectionText: {
        text: "Choose Plan. Become a member",
        textColor: "#000000",
        fontSize: "20px",
        highlightedText: "",
        highlightedTextColor: "",
    },
    benefits: SUPR_PASS_BENEFITS,
    autoDebitData: SUPR_PASS_AUTO_DEBIT_INFO,
    activity: SUPR_PASS_ACTIVITY,
};

export const SUPR_PASS_INFO_TIME_BASED_EXPERIMENT = {
    name: SUPR_PASS_BLOCK_ACCESS.RESTRICT_SUBSCRIPTION_ACTIVITY_TIME_BASED,
    headerText: {
        text: "Block access test",
    },
    faqs: [
        {
            question: "Why should I get Supr Access?",
            answer:
                "If you’re a regular Supr Daily user, Supr Access is the best way to save on service fees. For as little as ₹99 for one month (that’s just ₹3 per day!), you can order as many groceries as you need, as many times as you want, with no service fees. Signing up for 3 or 12 months can help you save even more!",
        },
        {
            question:
                "I only order milk from Supr Daily. Do I need to get Supr Access?",
            answer:
                "If you order 1 litre of milk from us every day, that’s ₹60 in service fees per month. In addition to this, you would be charged ₹20 for each grocery delivery you may need. Instead, at just ₹99 per month, there will be no service fee on milk orders, or any groceries, fruits & veggies you may need to purchase. You can make Supr Daily your one stop solution for all your grocery needs, without having to worry about service fees again!",
        },
        {
            question:
                "I have already paid service fees for deliveries which are scheduled for the next month. What happens when I sign up for Supr Access?",
            answer:
                "When you sign up, service fees for any deliveries during the Supr Access period will automatically be refunded to your Supr Wallet. Let's say you have a subscription for 1 litre of milk daily, and you still have 15 deliveries remaining. If you sign up for Supr Access, the service fees for the remaining deliveries (₹30 in this case), will be automatically refunded into your Supr Wallet. Similarly, if you have already scheduled any one time deliveries for the duration of Supr Access, the service fees on all those orders will also be refunded automatically.",
        },
    ],
    banner: {
        bgColor: "#FFFDFB",
        content: [
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Exclusive-offer-benefit-1.png",
                text:
                    "Order as many products as many times as you want, all for zero service fees.",
                highlightedText: "₹20",
                highlightedTextColor: "#F28D18",
                title: "Enjoy unlimited free deliveries",
                titleColor: "#000000",
                textColor: "#7C7C7C",
                fontSize: null,
                titleWeight: null,
                textWeight: null,
                tag: {
                    bgColor: "",
                    textColor: "",
                    text: "",
                    fontSize: "",
                },
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Exclusive-offer-benefit-2.png",
                text: "Supr Access members get priority access to stock!",
                title: "Reserved stock just for you",
                titleColor: "#000000",
                textColor: "#7C7C7C",
                fontSize: null,
                titleWeight: null,
                textWeight: null,
                tag: {
                    bgColor: "",
                    textColor: "",
                    text: "",
                    fontSize: "",
                },
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Exclusive-offer-benefit-3.png",
                text:
                    "Supr Access members get access to offers and discounts 1 day before everyone else!",
                title: "Exclusive Offers",
                titleColor: "#000000",
                textColor: "#7C7C7C",
                fontSize: null,
                titleWeight: null,
                textWeight: null,
                tag: {
                    bgColor: "#FBEBBA",
                    textColor: "#B69C0C",
                    text: "Coming Soon",
                    fontSize: "10px",
                },
            },
        ],
        title: null,
        boxShadow: "0px 1px 6px rgba(0, 0, 0, 0.15)",
        outerHeading: {
            text: "Perks you will enjoy",
            fontSize: "16px",
            textColor: "#333333",
        },
    },
    benefits: {
        content: [
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Refunded.png",
                header: null,
                upperFoldText: {
                    text: "You will be refunded",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                middleFoldText: {
                    text: "₹20",
                    textColor: "#333333",
                    fontSize: "36px",
                    highlightedTextColor: "",
                },
                lowerFoldText: {
                    text: "For service fee charged before",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                tag: {
                    bgColor: "#6AC259",
                    textColor: "#fff",
                    text: "Refund",
                    fontSize: "12px",
                },
                cta: null,
                isDefault: false,
                showInBlockAccess: false,
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Savings.png",
                header: {
                    bgColor: "#EA755B",
                    text: {
                        text: "You were a Supr Access member for 13 days",
                        textColor: "#FFFFFF",
                        fontSize: "12px",
                        highlightedText: "",
                    },
                    highlightedTextColor: "",
                },
                upperFoldText: {
                    text: "You have saved service fee worth",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                middleFoldText: {
                    text: "₹723",
                    textColor: "#333333",
                    fontSize: "36px",
                    highlightedTextColor: "",
                },
                lowerFoldText: {
                    text: "During your Supr Access membership",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                isDefault: false,
                showInBlockAccess: false,
            },
            {
                imgUrl:
                    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/City-savings.png",
                upperFoldText: {
                    text: "Highest amount saved is",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                middleFoldText: {
                    text: "₹360",
                    textColor: "#333333",
                    fontSize: "36px",
                    highlightedTextColor: "",
                },
                lowerFoldText: {
                    text: "In your city",
                    textColor: "#333333",
                    fontSize: "14px",
                    highlightedTextColor: "",
                },
                cta: {
                    text: "Get Supr Access today & save upto 45% more!",
                    textColor: "#50C1BA",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                showInBlockAccess: true,
                isDefault: true,
            },
        ],
    },
    autoDebitData: SUPR_PASS_AUTO_DEBIT_INFO,
    alertBanner: null,
    expiredSuprPassData: { endDate: "2020-07-18", purchaseId: 244853 },
    plans: [
        {
            id: 1,
            durationDays: 30,
            durationMonths: 1,
            unitPrice: 99.0,
            unitMrp: 150.0,
            discountText: "SAVE 34%",
            isRecommended: false,
        },
        {
            id: 2,
            durationDays: 90,
            durationMonths: 3,
            unitPrice: 279.0,
            unitMrp: 450.0,
            discountText: "SAVE 38%",
            isRecommended: true,
        },
        {
            id: 3,
            durationDays: 365,
            durationMonths: 12,
            unitPrice: 999.0,
            unitMrp: 1800.0,
            discountText: "SAVE 45%",
            isRecommended: false,
        },
    ],
    skuId: 9530,
    activePlanDetails: null,
    isActive: false,
    planSelectionText: {
        text: "Choose Plan. Become a member",
        textColor: "#000000",
        fontSize: "20px",
        highlightedText: "",
        highlightedTextColor: "",
    },
    activity: [
        {
            tag: {
                bgColor: "#6AC259",
                textColor: "#FFFFFF",
                text: "Auto Debit",
                fontSize: "12px",
            },
            detailsText: {
                text: "Auto Debited ₹90 for Supr Access Membership",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            date: "2020-07-05",
            totalDays: 13,
        },
    ],
    experimentInfo: {
        banner: {
            bgColor: "#FFFDFB",
            content: [
                {
                    imgUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/Exclusive-offer-benefit-1.png",
                    text:
                        "Subscribe to Access to schedule or modify orders for next day after 3pm",
                    textColor: "#333333",
                    fontSize: "14px",
                    link: {
                        text: "Get Supr Access now",
                        appUrl: "/supr-pass",
                        textColor: "#50C1BA",
                    },
                },
            ],
            title: null,
            boxShadow: "0px 1px 6px rgba(0, 0, 0, 0.15)",
        },
        name: "restrict_subscription_activity_time_based",
        cutOffTime: {
            value: "10:00:00",
        },
        calendarText:
            "Subscribe to Access to schedule orders for next day after 3pm!",
    },
};
