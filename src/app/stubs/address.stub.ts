import { Address } from "@models";

const ADDRESS = <Address>{
    id: 2,
    city: {
        id: 0,
        name: "Bangalore",
    },
    hub: {
        id: 154,
        name: "Sheikh Sarai",
        deliveryFee: 0,
        cutOffTime: "23:00:00",
    },
    type: "knownSociety",
    floorNumber: "9th",
    societyName: "Purva Skywood",
    buildingName: "Doulug",
    doorNumber: "902",
    streetAddress: "Silver County, Harlur",
    location: {
        latLong: {
            latitude: 12.9177386,
            longitude: 77.6542,
        },
    },
    instructions: "Keep safe",
    nudge: {
        maxCount: 5,
        show: true,
    },
};

export default ADDRESS;
