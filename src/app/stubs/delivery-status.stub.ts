export const DELIVERY_STATUS_STUB_EMPTY = {
    orderSummary: null,
    scheduleSummary: null,
};

// Order scheduled
export const DELIVERY_STATUS_STUB_SCHEDULE_ONLY = {
    orderSummary: null,
    scheduleSummary: {
        status: "SCHEDULED",
        message: "",
        date: "2020-01-24",
        subscriptions: [
            {
                remaining_quantity: 12,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 225063,
            },
            {
                remaining_quantity: 12,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 225063,
            },
        ],
        addons: [
            {
                remaining_quantity: 0,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 1951373,
            },
        ],
        statusContext: {},
    },
    skuData: [
        {
            id: 387,
            sku_name: "Thirumala Toned Milk",
            item: {
                id: 331,
                name: "Thirumala Toned Milk",
                unit: {
                    id: 5,
                    name: "L",
                    sub_unit: "ml",
                    conversion: "1000",
                },
                unit_quantity: 1.0,
                minimum_quantity: 0.5,
                step_quantity: 0.5,
            },
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku_387__26-01-2019-01h59m14s.jpg",
                path: "product_images/sku_387__26-01-2019-01h59m14s.jpg",
                bgColor: "#FFF",
            },
            unit_price: 40.0,
            unit_mrp: 40.0,
            preferred_mode: "ADDON",
            search_terms: [
                {
                    term: "thirumala",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 10,
                },
                {
                    term: "milk",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
        },
    ],
};

// Order generated. Non-terminal state
export const DELIVERY_STATUS_STUB_ORDER_GENERATED = {
    orderSummary: {
        date: "2020-01-22",
        status: "DELIVERED_PARTIAL",
        message:
            "Some items could not be delivered, we have refunded the amount",
        orders: [
            {
                sku_id: 387,
                id: 15273371,
                subscription_id: 563130,
                status: "DELIVERED",
                type: "subscription",
                scheduled_quantity: 0.5,
                delivered_quantity: 0.5,
                message: "",
                statusContext: {
                    statusTimestamp: "2020-01-06 15:00:00",
                },
            },
            {
                sku_id: 387,
                id: 15271262,
                subscription_id: 225063,
                status: "DELIVERED",
                type: "subscription",
                scheduled_quantity: 1,
                delivered_quantity: 1,
                message: "",
                statusContext: {
                    statusTimestamp: "2020-01-06 15:00:00",
                },
            },
            {
                sku_id: 387,
                id: 15963130,
                addon_id: 1781252,
                status: "DELAYED",
                type: "addon",
                scheduled_quantity: 1,
                delivered_quantity: 1,
                statusContext: {
                    statusTimestamp: "2020-01-06 15:00:00",
                },
            },
        ],
        statusContext: {
            statusTimestamp: "2020-01-06 15:00:00",
        },
    },
    scheduleSummary: null,
    skuData: [
        {
            id: 387,
            sku_name: "Thirumala Toned Milk",
            item: {
                id: 331,
                name: "Thirumala Toned Milk",
                unit: {
                    id: 5,
                    name: "L",
                    sub_unit: "ml",
                    conversion: "1000",
                },
                unit_quantity: 1.0,
                minimum_quantity: 0.5,
                step_quantity: 0.5,
            },
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku_387__26-01-2019-01h59m14s.jpg",
                path: "product_images/sku_387__26-01-2019-01h59m14s.jpg",
                bgColor: "#FFF",
            },
            unit_price: 40.0,
            unit_mrp: 40.0,
            preferred_mode: "ADDON",
            search_terms: [
                {
                    term: "thirumala",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 10,
                },
                {
                    term: "milk",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
        },
    ],
};

// Order terminal state. Scchedule for next day might or might not be there
export const DELIVERY_STATUS_STUB_ORDER_PRESENT = {
    orderSummary: {
        date: "2020-01-22",
        status: "DELIVERED",
        message: "All items delivered",
        orders: [
            {
                sku_id: 387,
                id: 15273371,
                subscription_id: 563130,
                status: "DELIVERED",
                type: "subscription",
                scheduled_quantity: 0.5,
                delivered_quantity: 0.5,
                message: "Delivered",
                statusContext: {
                    statusTimestamp: "2020-01-06 07:00:00",
                },
            },
            {
                sku_id: 387,
                id: 15271262,
                subscription_id: 225063,
                status: "REFUND_FULL",
                type: "subscription",
                scheduled_quantity: 1,
                delivered_quantity: 1,
                message: "Delivered",
                statusContext: {
                    statusTimestamp: "2019-10-09 07:00:00",
                },
            },
            {
                sku_id: 387,
                id: 15963130,
                addon_id: 1781252,
                status: "Delivered",
                type: "addon",
                scheduled_quantity: 1,
                delivered_quantity: 1,
                statusContext: {
                    statusTimestamp: "2020-01-06 07:00:00",
                },
            },
            {
                sku_id: 387,
                id: 15963130,
                addon_id: 1781252,
                status: "Delivered",
                type: "addon",
                scheduled_quantity: 1,
                delivered_quantity: 1,
                statusContext: {
                    statusTimestamp: "2020-01-06 07:00:00",
                },
            },
        ],
        statusContext: {
            statusTimestamp: "2020-01-06 07:00:00",
        },
    },
    scheduleSummary: {
        status: "SCHEDULED",
        message: "",
        date: "2020-01-23",
        subscriptions: [
            {
                remaining_quantity: 12,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 225063,
            },
            {
                remaining_quantity: 0,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 1951373,
            },
        ],
        addons: [
            {
                remaining_quantity: 12,
                sku_id: 387,
                modified: false,
                quantity: 1,
                id: 225063,
            },
        ],
        statusContext: {},
    },
    skuData: [
        {
            id: 387,
            sku_name: "Thirumala Toned Milk",
            item: {
                id: 331,
                name: "Thirumala Toned Milk",
                unit: {
                    id: 5,
                    name: "L",
                    sub_unit: "ml",
                    conversion: "1000",
                },
                unit_quantity: 1.0,
                minimum_quantity: 0.5,
                step_quantity: 0.5,
            },
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku_387__26-01-2019-01h59m14s.jpg",
                path: "product_images/sku_387__26-01-2019-01h59m14s.jpg",
                bgColor: "#FFF",
            },
            unit_price: 40.0,
            unit_mrp: 40.0,
            preferred_mode: "ADDON",
            search_terms: [
                {
                    term: "thirumala",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 10,
                },
                {
                    term: "milk",
                    score: 10,
                },
                {
                    term: "toned",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
        },
    ],
};

export const DELIVERY_STATUS_STUB = DELIVERY_STATUS_STUB_SCHEDULE_ONLY;
