export const PROFILE_DATA = {
    id: 249014,
    first_name: "Swatantra",
    last_name: "Mishra",
    email: "swatantramisha1@gmail.com",
    mobile_number: "9538406805",
    gender: null,
    birth_date: null,
    photo: null,
    credit_limit: null,
    credit_negative: false,
    app_version: "235",
    lat_long_required: false,
};

export const PROFILE_DATA_WITH_SC_ENABLED = {
    customer: {
        id: 1433548,
        firstName: "Nishant",
        lastName: "Gaurav",
        email: "nishantrunning@gmail.com",
        mobileNumber: "8871815950",
        bellRing: false,
        isWhatsAppOptIn: true,
        address: {
            id: 1901350,
            type: "skip",
            doorNumber: null,
            floorNumber: null,
            premiseId: null,
            instructions: null,
            societyName: null,
            buildingName: null,
            streetAddress: null,
            location: null,
            city: { id: 6, name: "Bangalore" },
            hub: {
                id: 101,
                name: "HSR",
                deliveryFee: 0.0,
                cutOffTime: "10:45:25",
            },
            customerId: 1433548,
        },
    },
    walletBalance: 100.0,
    subscriptions: [],
    tPlusOneEnabled: true,
    walletVersion: "V2",
    suprCreditsEnabled: true,
    suprCreditsBalance: 250,
};

export const ANONYMOUS_USER_STUB = {
    user_type: "unregistered",
    key: "d8222e09f28555813a2494737368d394b635b2af",
};
