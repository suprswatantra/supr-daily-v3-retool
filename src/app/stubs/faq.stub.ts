export const FAQ_LIST = {
    statusCode: 0,
    data: {
        id: 1,
        type: "category",
        title: "FAQ",
        children: [
            {
                id: 1,
                type: "faq",
                title: "Can packet milk also be adulterated?",
                description:
                    "Yes. While your favorite brand takes all precautions and safety measures to ensure quality, a lot of middle men in the supply chain tamper with milk packets to make extra money. This has been covered extensively in media as well.",
            },
            {
                id: 2,
                type: "faq",
                title: "Which milk brands do you deliver?",
                description:
                    "We deliver all common milk brands including Amul, Gokul, Mother Dairy, Mahananda, Aarey, Warna and many more. You can see the full list on our app or contact us on 9699000035.",
            },
            {
                id: 3,
                type: "faq",
                title: "How does the subscription work?",
                description:
                    "Its simple. You choose your favorite brand, quantity and frequency. You make a payment for as little as 3 deliveries (trial) or a full month (30 deliveries). Sit back and enjoy products delivered right to your doorstep every morning.",
            },
            {
                id: 4,
                type: "faq",
                title:
                    "How can I change the quantity for next day? What if I am going on a vacation?",
                description:
                    "You can simply change the quantity for next day on our app or pause all deliveries for your vacation duration. Our transparent service has been created so that you only pay for what you consume.",
            },
            {
                id: 5,
                type: "faq",
                title: "What payment options can I use?",
                description:
                    "You can make payments using our app, or a payment link sent to your phone, or swipe debit/credit cards with our payment executive, or request for a cash pickup. We support all common cards, internet banking and many wallets.",
            },
            {
                id: 6,
                type: "faq",
                title: "What's your cancellation policy?",
                description:
                    "You can cancel anytime you like with just a few taps on the app.",
            },
            {
                id: 7,
                type: "faq",
                title: "How can I get the cool Supr bag I see outside doors?",
                description:
                    "Simply subscribe for 1 month or more and get a free Supr bag which you can hang on your door to receive morning deliveries. This bag is fully insulated and protects the milk by keeping it cold for hours.",
            },
        ],
    },
    statusMessage: "FAQ fetched successfully",
};
