export default [
    {
        id: 21,
        name: "Milk & Dairy Products",
        rank: 445,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#64b053",
        },
    },
    {
        id: 22,
        name: "Fruits & Vegetables",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#fddfd8",
        },
    },
    {
        id: 14,
        name: "Breakfast Essentials",
        rank: 355,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#0000ff",
        },
    },
    {
        id: 9,
        name: "Navratri Ingredients",
        rank: 445,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#52c2ba",
        },
    },
    {
        id: 1,
        name: "Packaged Snacks",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#f6f0da",
        },
    },
    {
        id: 14,
        name: "Daily Pooja Needs",
        rank: 355,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#fddfd8",
        },
    },
    {
        id: 9,
        name: "Cooking Essentials",
        rank: 445,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#f6f0da",
        },
    },
    {
        id: 1,
        name: "Cleaning Essentials",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            bgColor: "#fddfd8",
        },
    },
];
