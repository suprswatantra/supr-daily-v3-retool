export const MILESTONE_LOCKED = {
    statusCode: 0,
    statusMessage: "success",
    data: {
        milestone: {
            state: "locked",
            image: {
                path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                compressed_url: {
                    "200_200": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                    "400_400": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                },
            },
            cta: {
                link: "/category/1",
                source: "in_app",
            },
        },
    },
};

export const MILESTONE_IN_PROCESS = {
    statusCode: 0,
    statusMessage: "success",
    data: {
        milestone: {
            state: "inProgress",
            details: {
                header_text: {
                    text: "Get 25 Supr Credits, 15 Days Supr Access & more!",
                    textColor: "#ffffff",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy a Fruit + more Rounak",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "3 Days Left",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                completion_percentage: "80",
            },
        },
    },
};

export const MILESTONE_START = {
    statusCode: 0,
    statusMessage: "success",
    data: {
        milestone: {
            state: "start",
            image: null,
            cta: {
                link: "/category/1",
                source: "in_app",
            },

            details: {
                header_text: {
                    text: "Get 25 Supr Credits, 15 Days Supr Access & more!",
                    textColor: "#ffffff",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                image: null,
                title: {
                    text: "Start now to win amazing rewards!",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Unlocked",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                completion_percentage: "",
            },
        },
    },
};

export const MILESTONE_DONE = {
    statusCode: 0,
    statusMessage: "success",
    data: {
        milestone: {
            state: "done",
            image: null,
            cta: {
                link: "/category/1",
                source: "in_app",
            },

            details: {
                header_text: {
                    text: "Get 25 Supr Credits, 15 Days Supr Access & more!",
                    textColor: "#ffffff",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                image: null,
                title: {
                    text: "All task done! Rewards credited soon",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Pending",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                completion_percentage: "",
            },
        },
    },
};

// Header Changes
const HEADER_TEXT_LOCKED = {
    state: "locked",
    title: {
        text: "Play & Win Exciting Rewards",
        textColor: "#FFFEA9",
        fontSize: "24px",
        highlightedText: "",
        highlightedTextColor: "",
        bgColor: "",
    },
    sub_title: {
        text: "Unlock value worth ₹700 +",
        textColor: "#ffffff",
        fontSize: "16px",
        highlightedText: "",
        highlightedTextColor: "",
        bgColor: "",
    },
    rewards_earned: [
        {
            image: {
                path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                compressed_url: {
                    "200_200": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                    "400_400": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                },
            },
            title: {
                text: "115",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
            sub_title: {
                text: "Days Supr Access",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
        {
            image: {
                path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                compressed_url: {
                    "200_200": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                    "400_400": {
                        fullUrl:
                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                    },
                },
            },
            title: {
                text: "115",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
            sub_title: {
                text: "Days Supr Access",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
    ],
};

// const HEADER_TEXT_UNLOCKED = {
//     state: "unLocked",
//     title: {
//         text: "240",
//         textColor: "#FFFEA9",
//         fontSize: "24px",
//         highlightedText: "",
//         highlightedTextColor: "",
//         bgColor: "",
//     },
//     sub_title: {
//         text: "Worth of rewards won till now!",
//         textColor: "#FFFFFF",
//         fontSize: "16px",
//         highlightedText: "",
//         highlightedTextColor: "",
//         bgColor: "",
//     },
//     rewards_earned: [
//         {
//             image: {
//                 path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 bgColor: "#FFF",
//                 fullUrl:
//                     "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 compressed_url: {
//                     "200_200": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                     "400_400": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                 },
//             },
//             title: {
//                 text: "115",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//             sub_title: {
//                 text: "Days Supr Access",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//         },
//         {
//             image: {
//                 path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 bgColor: "#FFF",
//                 fullUrl:
//                     "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 compressed_url: {
//                     "200_200": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                     "400_400": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                 },
//             },
//             title: {
//                 text: "115",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//             sub_title: {
//                 text: "Days Supr Access",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//         },
//         {
//             image: {
//                 path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 bgColor: "#FFF",
//                 fullUrl:
//                     "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 compressed_url: {
//                     "200_200": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                     "400_400": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                 },
//             },
//             title: {
//                 text: "115",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//             sub_title: {
//                 text: "Days Supr Access",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//         },
//         {
//             image: {
//                 path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 bgColor: "#FFF",
//                 fullUrl:
//                     "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
//                 compressed_url: {
//                     "200_200": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                     "400_400": {
//                         fullUrl:
//                             "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
//                     },
//                 },
//             },
//             title: {
//                 text: "115",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//             sub_title: {
//                 text: "Days Supr Access",
//                 textColor: "",
//                 fontSize: "",
//                 highlightedText: "",
//                 highlightedTextColor: "",
//                 bgColor: "",
//             },
//         },
//     ],
// };

// Milestones State here

// Locked
const MILES_STONES_UN_LOCKED_3_REWARDS = {
    milestone_rewards: {
        title: {
            text: "Earn amazing rewards!",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "Unlocked",
            textColor: "#FFFFFF",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "#33333",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "unLocked",
    milestone_tasks: {
        taskHeaderText: {
            text: "Complete all tasks to win above rewards",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "2",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: null,
                    percentage: "",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
                cta: {
                    text: "Explore Now",
                    link: "url",
                    source: "in_app | out_app",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
                cta: {
                    text: "Explore Now",
                    link: "url",
                    source: "in_app | out_app",
                },
            },
        ],
        state_details: {
            title: {
                text: "Start now to earn amazing rewards !",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title: "Start now to earn amazing rewards !",
    },
};

const MILES_STONES_UN_LOCKED_4_REWARDS = {
    milestone_rewards: {
        title: {
            text: "Earn amazing rewards!",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "Unlocked",
            textColor: "#FFFFFF",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "#33333",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "unLocked",
    milestone_tasks: {
        taskHeaderText: {
            text: "Complete all tasks to win above rewards",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "2",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: null,
                    percentage: "",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
                cta: {
                    text: "Explore Now",
                    link: "url",
                    source: "in_app | out_app",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
                cta: {
                    text: "Explore Now",
                    link: "url",
                    source: "in_app | out_app",
                },
            },
        ],
        state_details: {
            title: {
                text: "Start now to earn amazing rewards !",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title: "Start now to earn amazing rewards !",
    },
};

// InProgress
const MILES_STONES_IN_PROGRESS = {
    milestone_rewards: {
        title: {
            text: "Earn amazing rewards!",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "3 days left",
            textColor: "#333333",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "inProgress",
    milestone_tasks: {
        taskHeaderText: {
            text: "Complete all tasks to win above rewards",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "2",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: "2 more to go",
                    percentage: "70",
                    type: "count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "14 more to go",
                    percentage: "40",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
        ],
        state_details: {
            title: {
                text: "Start now to earn amazing rewards !",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title: "Start now to earn amazing rewards !",
    },
};

// Locked
const MILES_STONES_LOCKED = {
    milestone_rewards: {
        title: {
            text: "Earn amazing rewards!",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "Locked",
            textColor: "#333333",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "locked",
    milestone_tasks: {
        taskHeaderText: {
            text: "Complete all tasks to win above rewards",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "2",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: null,
                    percentage: "",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
        ],
        state_details: {
            title: {
                text: "Complete previous tasks to unlock this ",
                textColor: "",
                fontSize: "",
                highlightedText: "",
                highlightedTextColor: "",
                bgColor: "",
            },
        },
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title: "Complete previous tasks to unlock this ",
        sub_title: "Go to current tasks",
    },
};

// Expired
const MILES_STONES_EXPIRED = {
    milestone_rewards: {
        title: {
            text: "You missed out",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "Expired",
            textColor: "#FFFFFF",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "expired",
    milestone_tasks: {
        taskHeaderText: {
            text: "Tasks completed",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "2",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: null,
                    percentage: "",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
        ],
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title: "Uh-oh! You could not complete all the tasks in the given time.",
        sub_title: "Go to current tasks",
    },
};

// Expired
const MILES_STONES_COMPLETED = {
    milestone_rewards: {
        title: {
            text: "Hurray! You have won",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        sub_title: {
            text: "Completed",
            textColor: "#FFFFFF",
            fontSize: "12px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        rewards_list: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "115",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Days Supr Access",
                    textColor: "",
                    fontSize: "",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
            },
        ],
    },
    current_milestone: true,
    state: "completed",
    milestone_tasks: {
        task_header_text: {
            text: "Tasks completed",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        total: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        completed: {
            text: "3",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "",
            highlightedTextColor: "",
            bgColor: "",
        },
        tasks: [
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                cta: {
                    text: {
                        text: "Explore Now",
                        textColor: "",
                        fontSize: "",
                        highlightedText: "",
                        highlightedTextColor: "",
                        bgColor: "",
                    },
                    link: "/category/1",
                    source: "in_app",
                },
                progress: {
                    text: null,
                    percentage: "",
                    type: "amount",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
            {
                image: {
                    path: "product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1911__14-10-2019-16h37m02s.jpg",
                    compressed_url: {
                        "200_200": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                        "400_400": {
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__1911__02-04-2020-19h07m35s.jpg",
                        },
                    },
                },
                title: {
                    text: "Buy any Vegetable",
                    textColor: "#000000",
                    fontSize: "16px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                sub_title: {
                    text: "Leafy, exotic, daily and more",
                    textColor: "#333333",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                    bgColor: "",
                },
                state: "inProgress",
                progress: {
                    text: "",
                    percentage: "",
                    type: "amount | count",
                },
            },
        ],
    },

    rules: [
        "Spend and its delivery has to happen within the time period",
        "Spend applicable on all products except on milk",
    ],
    state_details: {
        title:
            "All tasks completed for this milestone. You won 25 days supr access and 120 supr credits",
        sub_title: "Go to current tasks",
    },
};

export const MILESTONE_DETAILS = {
    statusCode: 0,
    statusMessage: "success",
    data: {
        milestone: {
            users_rewards: HEADER_TEXT_LOCKED,
            faq: [{}],
            milestones: [
                MILES_STONES_UN_LOCKED_4_REWARDS,
                MILES_STONES_UN_LOCKED_3_REWARDS,
                MILES_STONES_IN_PROGRESS,
                MILES_STONES_LOCKED,
                MILES_STONES_EXPIRED,
                MILES_STONES_COMPLETED,
            ],
            user_milestone_details: {
                state: "inProgress | completed",
                title: "",
                sub_title: "",
            },
        },
    },
};
