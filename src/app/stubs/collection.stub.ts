export const COLLECTION_LIST = [
    {
        title: {
            text: "Free trials",
            color: "#b92c2c",
        },
        background: {
            image: {
                fullUrl:
                    // tslint:disable-next-line: max-line-length
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==",
                path: "",
            },
            color: "#f9e6e6",
        },
        tnc: {
            text: "* Minimum order amount required ₹199",
            color: "#999999",
        },
        type: "sku",
        displayType: "horizontal_list",
        seeAll: null,
        bookmarkTag: {
            title: {
                text: "Free",
                color: "#ffffff",
            },
            bgColor: "#b92c2c",
            subtitle: {
                text: "Only for today",
                color: "#ffffff",
            },
        },
        viewId: 2,
        items: [
            {
                entityId: 14,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
                    path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 367,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__367__14-10-2019-16h35m28s.jpg",
                    path: "product_images/sku__367__14-10-2019-16h35m28s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 827,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__827__14-10-2019-17h06m59s.jpg",
                    path: "product_images/sku__827__14-10-2019-17h06m59s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
            {
                entityId: 1014,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                    path: "product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
        ],
        subtitle: {
            text: "Best of the products, on the house!",
            color: "#ac3421",
        },
    },

    {
        title: {
            text: "Value for money deals",
            color: "#12a5d4",
        },
        background: {
            image: {
                fullUrl: "",
                path: "",
            },
            color: "#e7f8fd",
        },
        tnc: null,
        type: "sku",
        displayType: "horizontal_list",
        seeAll: null,
        bookmarkTag: {
            title: null,
            bgColor: "#12a5d4",
            subtitle: {
                text: "Value for money",
                color: "#ffffff",
            },
        },
        viewId: 2,
        items: [
            {
                entityId: 14,
                tags: [
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
                    path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 367,
                tags: [
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__367__14-10-2019-16h35m28s.jpg",
                    path: "product_images/sku__367__14-10-2019-16h35m28s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 827,
                tags: [
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__827__14-10-2019-17h06m59s.jpg",
                    path: "product_images/sku__827__14-10-2019-17h06m59s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
            {
                entityId: 1014,
                tags: [
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                    path: "product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
        ],
        subtitle: {
            text: "Best of the offers running today!",
            color: "#12a5d4",
        },
    },
    {
        title: {
            text: "Popular subscriptions around you!",
            color: "#000000",
        },
        background: {
            image: {
                fullUrl: "",
                path: "",
            },
            color: "#ffffff",
        },
        tnc: null,
        type: "sku",
        displayType: "horizontal_list",
        seeAll: {
            button: {
                title: {
                    text: "See all",
                    color: "#52c2bb",
                },
                bgColor: "lightyellow",
                borderColor: "#52c2bb",
            },
            card: {
                title: {
                    text: "See all",
                    color: "#52c2bb",
                },
                bgColor: "palegreen",
                borderColor: "#f2f2f2",
            },
        },
        bookmarkTag: null,
        viewId: 2,
        items: [
            {
                entityId: 14,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Popular in your locality",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
                    path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 367,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Popular in your locality",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__367__14-10-2019-16h35m28s.jpg",
                    path: "product_images/sku__367__14-10-2019-16h35m28s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 827,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Popular in your locality",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__827__14-10-2019-17h06m59s.jpg",
                    path: "product_images/sku__827__14-10-2019-17h06m59s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_add",
            },
            {
                entityId: 1014,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Popular in your locality",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                    {
                        type: "ribbon",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "₹10 cashback",
                            color: "#f2bc18",
                        },
                        bgColor: "#fcf1cf",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                    path: "product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_add",
            },
        ],
        subtitle: null,
    },
    {
        title: {
            text: "Recommended for you",
            color: "#000000",
        },
        background: {
            image: {
                fullUrl: "",
                path: "",
            },
            color: "#ffffff",
        },
        tnc: null,
        type: "sku",
        displayType: "horizontal_list",
        seeAll: {
            button: {
                title: {
                    text: "See all",
                    color: "#52c2bb",
                },
                bgColor: "#ffffff",
                borderColor: "#52c2bb",
            },
            card: {
                title: {
                    text: "See all",
                    color: "#52c2bb",
                },
                bgColor: "#ffffff",
                borderColor: "#f2f2f2",
            },
        },
        bookmarkTag: null,
        viewId: 2,
        items: [
            {
                entityId: 14,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Popular in your locality",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
                    path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 367,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Top seller",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__367__14-10-2019-16h35m28s.jpg",
                    path: "product_images/sku__367__14-10-2019-16h35m28s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 827,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Most Viewed",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__827__14-10-2019-17h06m59s.jpg",
                    path: "product_images/sku__827__14-10-2019-17h06m59s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_add",
            },
            {
                entityId: 1014,
                tags: [
                    {
                        type: "banner",
                        icon: {
                            fullUrl: "",
                            path: "",
                            position: "",
                        },
                        title: {
                            text: "Highest Rated",
                            color: "#52c2bb",
                        },
                        bgColor: "#e8f7f6",
                    },
                ],
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                    path: "product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                },
                borderColor: "#f2f2f2",
                preferredMode: "direct_add",
            },
        ],
        subtitle: {
            text: "Based on what’s trending in your area",
            color: "#999999",
        },
    },
    {
        title: {
            text: "Trending today",
            color: "#000000",
        },
        background: {
            image: {
                fullUrl: "",
                path: "",
            },
            color: "#ffffff",
        },
        tnc: {
            text: "* Minimum order amount required ₹199",
            color: "#999999",
        },
        type: "sku",
        displayType: "2_by_2_grid",
        seeAll: null,
        bookmarkTag: null,
        viewId: 2,
        items: [
            {
                entityId: 14,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
                    path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 367,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__367__14-10-2019-16h35m28s.jpg",
                    path: "product_images/sku__367__14-10-2019-16h35m28s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_subscribe",
            },
            {
                entityId: 827,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__827__14-10-2019-17h06m59s.jpg",
                    path: "product_images/sku__827__14-10-2019-17h06m59s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
            {
                entityId: 1014,
                tags: null,
                image: {
                    bgColor: "#FFF",
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                    path: "product_images/sku__1014__14-10-2019-16h41m47s.jpg",
                },
                borderColor: null,
                preferredMode: "direct_add",
            },
        ],
        subtitle: {
            text: "Locally made in Hyderabad. Trending in your neighborhood.",
            color: "#999999",
        },
    },
];

export const COLLECTION_DETAILS_NESTED = {
    viewId: 2,
    title: {
        text: "For Coffee & Tea Lovers",
        color: "#877d70",
    },
    subtitle: {
        text: "Good to the Last Drop!",
        color: "#877d70",
    },
    image: {
        fullUrl:
            "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
        bgColor: "#FFF",
        path: "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
    },
    background: {
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__25__09-10-2019-19h52m22s.jpg",
            path:
                "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
        },
        color: "#dad3cb",
    },
    skuCategorizationType: "structured",
    default: [
        {
            id: 80,
            name: "Chocolates",
            image: {
                fullUrl: null,
                path: null,
                bgColor: "#FFF",
            },
            skuIdList: [14, 367],
        },
        {
            id: 92,
            name: "Cadbury",
            image: {
                fullUrl: null,
                path: null,
                bgColor: "#FFF",
            },
            skuIdList: [14, 367, 1014],
        },
        {
            id: 228,
            name: "Chocolates",
            image: {
                fullUrl: null,
                path: null,
                bgColor: "#FFF",
            },
            skuIdList: [367, 1014],
        },
    ],
    filtered: [],
    skuIdList: [],
    skuList: [
        {
            id: 14,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
        {
            id: 367,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
        {
            id: 1014,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
    ],
};

export const COLLECTION_DETAILS_LINEAR = {
    viewId: 2,
    title: {
        text: "For Coffee & Tea Lovers",
        color: "#877d70",
    },
    subtitle: {
        text: "Good to the Last Drop!",
        color: "#877d70",
    },
    image: {
        fullUrl:
            "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
        bgColor: "#FFF",
        path: "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
    },
    background: {
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__25__09-10-2019-19h52m22s.jpg",
            path:
                "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
        },
        color: "#dad3cb",
    },
    skuCategorizationType: "linear",
    default: [],
    filtered: [],
    skuIdList: [367, 14, 1014],
    skuList: [
        {
            id: 14,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
        {
            id: 367,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
        {
            id: 1014,
            item: {
                id: 14,
                name: "Amul Gold Tetra",
                unit: {
                    id: 3,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: "1.00",
                },
                step_quantity: 1,
                unit_quantity: 1,
                minimum_quantity: 1,
            },
            image: {
                path: "product_images/sku__14__14-10-2019-16h36m05s.jpg",
                bgColor: "#FFF",
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__14__14-10-2019-16h36m05s.jpg",
            },
            sku_name: "Amul Gold Tetrapack (1 L)",
            unit_mrp: 66,
            unit_price: 64,
            search_terms: [
                {
                    term: "amul",
                    score: 10,
                },
                {
                    term: "gold",
                    score: 10,
                },
                {
                    term: "tetrapack",
                    score: 10,
                },
                {
                    term: "tetra",
                    score: 7,
                },
                {
                    term: "milk",
                    score: 5,
                },
            ],
            preferred_mode: "direct_subscribe",
        },
    ],
};

export const COLLECTION_LIST_SINGLE_CARD = [
    {
        tnc: null,
        type: "sku_collection",
        image: null,
        items: null,
        title: null,
        viewId: 186,
        subtitle: null,
        background: null,
        singleCardImg: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/feature_images/featured__413__09-02-2020-13h56m54s.jpg",
            path:
                "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
        },
        maxAllowed: 14,
        bookmarkTag: null,
        displayType: "single_card",
        skuCategorizationType: "linear",
    },
    {
        tnc: null,
        type: "sku_collection",
        image: null,
        items: null,
        title: null,
        viewId: 186,
        subtitle: null,
        background: null,
        singleCardImg: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media/feature_images/featured__413__09-02-2020-13h56m54s.jpg",
            path:
                "collection_images/collection__None__21-10-2019-13h21m57s.jpg",
        },
        maxAllowed: 14,
        bookmarkTag: null,
        displayType: "single_card",
        skuCategorizationType: "linear",
    },
];

export const HERO_GRID_COLLECTION_LIST = [
    {
        type: "sku",
        image: {
            path: "collection_images/popular-recommendations%403x.png",
            bgColor: "#fff",
            fullUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/single-card%403x.png",
        },
        items: null,
        subtitle: null,
        title: null,
        viewId: 17,
        displayType: "single_card",
        groupTypeId: 1,
        singleCardImg: {
            path: "collection_images/popular-recommendations%403x.png",
            bgColor: "#fff",
            fullUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/single-card%403x.png",
        },
        skuCategorizationType: "linear",
    },
    {
        tnc: null,
        type: "sku_collection",
        items: [
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/main-img%403x.png",
                },
                entityId: 1,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-1%403x.png",
                },
                entityId: 2,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-2%403x.png",
                },
                entityId: 4,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-4%403x.png",
                },
                entityId: 6,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-3%403x.png",
                },
                entityId: 5,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-1%403x.png",
                },
                entityId: 2,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-2%403x.png",
                },
                entityId: 4,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-4%403x.png",
                },
                entityId: 6,
                borderColor: "#ffffff",
            },
            {
                image: {
                    path: "collection_images/popular-recommendations%403x.png",
                    bgColor: "#fff",
                    fullUrl:
                        "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/collection_images/item-3%403x.png",
                },
                entityId: 5,
                borderColor: "#ffffff",
            },
        ],
        title: { text: "Cooking Essentials", color: "#000000" },
        viewId: 16,
        subtitle: null,
        background: { color: "#ffffff" },
        maxAllowed: 10,
        displayType: "hero_grid",
        skuCategorizationType: "linear",
    },
];
