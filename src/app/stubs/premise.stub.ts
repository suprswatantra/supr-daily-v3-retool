export const PREMISE_SEARCH_DATA = [
    {
        id: 1,
        name: "My Home",
        streetAddresslocationHelpText:
            "104, 3rd Cross Road, Srr Layout, 3rd Cross Road, Srr Layout, Anjappa Layout, B Narayanapura, Mahadevapura, Bengaluru, Karnataka 560048",
    },
    {
        id: 2,
        name: "Amygo",
        streetAddresslocationHelpText:
            "#17-T, 17th Cross, 19th MAIN, 3rd Sector, H.S.R Layout, Next to nandini cool joint, Bengaluru, Karnataka 560102",
    },
    {
        id: 3,
        name: "Royale Habitate Villas",
        streetAddresslocationHelpText:
            "1732, 27th Cross Rd, Garden Layout, Sector 2, HSR Layout, Bengaluru, Karnataka 560068",
    },
    {
        id: 4,
        name: "Sobha Daffodil",
        streetAddresslocationHelpText:
            "Beside Global Ravindra School, 27th Main Rd, Sector 2, PWD Quarters, 1st Sector, HSR Layout, Bengaluru, Karnataka 560102",
    },
];

export const PREMISE_ADDRESS_DATA = {
    id: 2,
    name: "Amygo",
    streetAddress:
        "#17-T, 17th Cross, 19th MAIN, 3rd Sector, H.S.R Layout, Next to nandini cool joint, Bengaluru, Karnataka 560102",
    buildings: [
        {
            id: 1,
            name: "Tower 1",
            doors: [
                {
                    id: 1,
                    floorNumber: 1,
                    name: "Krishna",
                },
                {
                    id: 2,
                    floorNumber: 2,
                    name: "Vasudev",
                },
                {
                    id: 3,
                    floorNumber: 3,
                    name: "Kutumb",
                },
                {
                    id: 4,
                    floorNumber: 5,
                    name: "Swatantra",
                },
            ],
        },
        {
            id: 2,
            name: "Ganga Tower",
            doors: [
                {
                    id: 6,
                    floorNumber: 1,
                    name: "Ganga Dev",
                },
                {
                    id: 8,
                    floorNumber: 2,
                    name: "Dev",
                },
                {
                    id: 9,
                    floorNumber: 3,
                    name: "Hamara Ghar",
                },
                {
                    id: 10,
                    floorNumber: 5,
                    name: "Your House",
                },
            ],
        },
    ],
};
