export const COMPLAINT_DETAILS_DATA = {
    title: "Issue details",
    subTitle: "Issue id #2461996",
    uploadedImages: [
        "https://suprdaily-cx.s3.amazonaws.com:443/self-serve-customer-complaints/2155096-2020-09-18%2016%3A32%3A58-Screenshot%202020-09-17%20at%2010.21.21%20PM.png?Signature=ajZi6XLJBH6r7mC4fpfFZ75oPrs%3D&Expires=1600858978&AWSAccessKeyId=AKIAJJU4PYHRFXVY3HTQ",
        "https://suprdaily-cx.s3.amazonaws.com:443/self-serve-customer-complaints/2155096-2020-09-18%2016%3A33%3A13-Screenshot%202020-09-17%20at%2010.21.21%20PM.png?Signature=CBxDzRlCrGCffur7EhtYFdw9X2M%3D&Expires=1600858993&AWSAccessKeyId=AKIAJJU4PYHRFXVY3HTQ",
    ],
    notes: "very bad quality",
    orders: [
        {
            order_id: 113251316,
            selected: true,
            addon_id: 35544638,
            category_id: 1,
            delivered_quantity: 0,
            delivery_type: "REGULAR_DELIVERY",
            delivery_type_actual: "REGULAR_DELIVERY",
            id: 113251316,
            sku_id: 2664,
            status: "Processed",
            statusContext: {
                refundType: "WALLET_REFUND",
                refundAmount: 38,
            },
            statusNew: "REFUND_FULL",
            time_slot: "03:00-07:00",
            type: "addon",
            complaint: [
                {
                    disabled: false,
                    id: 12,
                    label: "Missing",
                    selected: true,
                    value: "Missing",
                },
            ],
            quantity: 1,
            feedbackParams: ["quality", "color"],
        },
        {
            order_id: 113251458,
            selected: true,
            addon_id: 35544646,
            category_id: 17,
            delivered_quantity: 1,
            delivery_type: "REGULAR_DELIVERY",
            delivery_type_actual: "REGULAR_DELIVERY",
            id: 113251458,
            sku_id: 9304,
            status: "Delivered",
            statusContext: { statusTimestamp: "2020-09-15 21:56:35" },
            statusNew: "DELIVERED",
            time_slot: "03:00-07:00",
            type: "addon",
            complaint: [
                {
                    disabled: false,
                    id: 14,
                    label: "Quality - not good",
                    selected: true,
                    value: "Quality - not good",
                },
            ],
            quantity: 1,
            feedbackParams: ["Damaged", "Expired", "Dirty"],
        },
    ],

    service: [
        "ring not followed",
        "order not delivered at doorstep",
        "agent did not follow covid guideline",
    ],

    complaint_timeline: {
        timelineItems: [
            {
                checked: true,
                icon: "approve",
                subtitle: {
                    text: "Updated at 11:00 PM, 30 Sep ",
                    highlightText: "",
                },
                title: { text: "Arriving by 7 AM" },
            },
            {
                checked: true,
                icon: "approve",
                subtitle: {
                    text: "Updated at 11:00 PM, 30 Sep ",
                    highlightText: "",
                },
                title: { text: "Arriving by 7 AM" },
            },
            {
                checked: true,
                icon: "approve",
                subtitle: {
                    text: "Updated at 11:00 PM, 30 Sep ",
                    highlightText: "",
                },
                title: { text: "Arriving by 7 AM" },
            },
            {
                checked: true,
                icon: "approve",
                subtitle: {
                    text: "Updated at 11:00 PM, 30 Sep ",
                    highlightText: "",
                },
                title: { text: "Arriving by 7 AM" },
            },
        ],
    },
};
