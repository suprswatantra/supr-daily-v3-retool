// tslint:disable: max-line-length
export const CATEGORY1 = {
    id: 21,
    name: "Milk & Dairy Products",
    rank: 400,
    image: {
        fullUrl:
            "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
        bgColor: "#64b053",
    },
    default: [
        {
            id: 7,
            name: "Toned Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 2, 3, 4],
        },
        {
            id: 8,
            name: "Full Cream Milk",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 28,
            name: "Tetra Packs",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 18,
            name: "Fat Free Milk",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
    ],
    filtered: [
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#64b053",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#32b053",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    // tslint:disable-next-line: max-line-length
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
    ],
};

export const CATEGORY2 = {
    id: 22,
    name: "Fruits & Vegetables",
    rank: 400,
    image: {
        fullUrl:
            "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
        bgColor: "#64b053",
    },
    default: [
        {
            id: 7,
            name: "Toned Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 2, 3, 4],
        },
        {
            id: 8,
            name: "Full Cream Milk",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 28,
            name: "Tetra Packs",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 18,
            name: "Fat Free Milk",
            rank: 36,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h29m15s.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
    ],
    filtered: [
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
        {
            id: 7,
            name: "Amul",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1__28-06-2016-16h32m32s/adb3b1e5db571765203ddaca782559a0.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [1, 3],
        },
        {
            id: 7,
            name: "Milk",
            rank: 34,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_21__28-06-2016-16h42m56s/05f9841acd23b285ef11b56fc7f8f7e3.jpg",
                bgColor: "#FFF",
            },
            skuIdList: [5, 6, 7],
        },
    ],
};
