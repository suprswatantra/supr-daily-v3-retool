import {
    ActivityBlock,
    BenefitsBanner,
    RewardsInfo,
    ScratchCard,
    ScratchedCardDetails,
} from "@shared/models";

export const BENEFITS: BenefitsBanner = {
    header: null,
    bgColor: null,
    content: [
        {
            header: {
                text: {
                    text:
                        "You’ve earned 190 Supr Credits through scratch cards",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#6AC259",
            },
            imgUrl:
                "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/icon-star.png",
            upperFoldText: {
                text: "Total Supr Credits available",
                textColor: "#333333",
                fontSize: "14px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldText: {
                text: "340",
                textColor: "#333333",
                fontSize: "36px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            middleFoldIcon: {
                iconType: "svg",
                iconName: "supr-credits-img",
                iconSize: "36px",
            },
            cta: {
                text: "32 expiring soon. REDEEM NOW",
                textColor: "#849920",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            ctaAction: {
                action: "route",
                appUrl: "/category/1",
            },
        },
        {
            header: {
                text: {
                    text: "How to start winning rewards?",
                    textColor: "#FFFFFF",
                    fontSize: "12px",
                    highlightedText: "",
                    highlightedTextColor: "",
                },
                bgColor: "#50C1BA",
            },
            upperFoldText: {
                text:
                    "Create a milk subscription and win a Scratch card on each successful delivery!",
                textColor: "#333333",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            cta: {
                text: "FIND ITEMS TO SUBSCRIBE",
                textColor: "#50C1BA",
                fontSize: "12px",
                highlightedText: "",
                highlightedTextColor: "",
            },
            ctaAction: {
                action: "route",
                appUrl: "/category/1",
            },
        },
    ],
};

export const FAQ = [
    {
        question: "Why should I get Supr Access?",
        answer:
            "If you’re a regular Supr Daily user, Supr Access is the best way to save on service fees. For as little as ₹99 for one month (that’s just ₹3 per day!), you can order as many groceries as you need, as many times as you want, with no service fees. Signing up for 3 or 12 months can help you save even more!",
    },
    {
        question:
            "I only order milk from Supr Daily. Do I need to get Supr Access?",
        answer:
            "If you order 1 litre of milk from us every day, that’s ₹60 in service fees per month. In addition to this, you would be charged ₹20 for each grocery delivery you may need. Instead, at just ₹99 per month, there will be no service fee on milk orders, or any groceries, fruits & veggies you may need to purchase. You can make Supr Daily your one stop solution for all your grocery needs, without having to worry about service fees again!",
    },
    {
        question:
            "I have already paid service fees for deliveries which are scheduled for the next month. What happens when I sign up for Supr Access?",
        answer:
            "When you sign up, service fees for any deliveries during the Supr Access period will automatically be refunded to your Supr Wallet. Let's say you have a subscription for 1 litre of milk daily, and you still have 15 deliveries remaining. If you sign up for Supr Access, the service fees for the remaining deliveries (₹30 in this case), will be automatically refunded into your Supr Wallet. Similarly, if you have already scheduled any one time deliveries for the duration of Supr Access, the service fees on all those orders will also be refunded automatically.",
    },
];

export const ACTIVITY_BLOCK: ActivityBlock[] = [
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
        benefitIcon: {
            iconType: "svg",
            iconName: "supr-credits-img",
            iconSize: "14px",
        },
        footerText: {
            text: "Supr credits balance ₹430",
            fontSize: "12px",
            textColor: "#999999",
            highlightedText: "₹430",
            highlightedTextColor: "#999999",
        },
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
        footerText: {
            text: "Check cart to reedeem",
            fontSize: "12px",
            textColor: "#999999",
            highlightedText: "₹430",
            highlightedTextColor: "#999999",
        },
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
        benefitIcon: {
            iconType: "svg",
            iconName: "supr-credits-img",
            iconSize: "14px",
        },
        footerText: {
            text: "Kuch bhi",
            fontSize: "12px",
            textColor: "#999999",
            highlightedText: "₹430",
            highlightedTextColor: "#999999",
        },
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "1 Month membership",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: 23,
    },
    {
        tag: {
            bgColor: "#6AC259",
            textColor: "#FFFFFF",
            text: "300 SuprCredits",
            fontSize: "12px",
        },
        detailsText: {
            text: "Your membership has been extended due to system vacation",
            textColor: "#333333",
            fontSize: "14px",
            highlightedText: "txt",
            highlightedTextColor: "green",
        },
        date: "2020-06-08",
        totalDays: null,
        benefitType: "suprCredits",
        totalBenefit: 300,
        benefitIcon: {
            iconType: "svg",
            iconName: "supr-credits-img",
            iconSize: "14px",
        },
    },
];

export const SCRATCH_CARDS: ScratchCard[] = [
    {
        id: 1,
        imgUrl: "./assets/images/app/supr-scratch-card.png",
        isExpired: false,
        isLocked: false,
        expiryDate: "",
        earnDate: "",
        unlockDate: "",
    },
    {
        id: 2,
        imgUrl: "./assets/images/app/supr-scratch-card.png",
        isExpired: false,
        isLocked: false,
        expiryDate: "",
        earnDate: "",
        unlockDate: "",
    },
    {
        id: 3,
        imgUrl: "",
        isExpired: false,
        isLocked: false,
        expiryDate: "",
        earnDate: "",
        unlockDate: "",
    },
];

export const SCRATCHED_CARD_DETAILS: ScratchedCardDetails = {
    id: 1,
    benefitInfo: {
        text: "Kaafi sundar Scratch Card",
        textColor: "#388782",
        highlightedText: "sundar",
        highlightedTextColor: "#388782",
    },
    isError: null,
    isExpired: null,
};

export const SCRATCHED_CARD_DETAILS_1: ScratchedCardDetails = {
    id: 2,
    benefitInfo: null,
    isError: true,
    isExpired: null,
};

export const SCRATCHED_CARD_DETAILS_2: ScratchedCardDetails = {
    id: 3,
    benefitInfo: null,
    isError: null,
    isExpired: true,
};

export const REWARDS_INFO: RewardsInfo = {
    header: {
        bgColor: "rgba(249, 221, 140, 0.2)",
        logo: "",
        title: {
            text: "Supr Rewards",
            textColor: "#EA755B",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
        subTitle: {
            text: "Subscribe and win rewards",
            textColor: "#333333",
            fontSize: "20px",
            highlightedText: "",
            highlightedTextColor: "",
        },
    },
    scratchCards: SCRATCH_CARDS,
    benefits: BENEFITS,
    faqs: FAQ,
    pastRewards: ACTIVITY_BLOCK,
    footer: {
        scExpiry: {
            amount: 300,
            date: "2020-10-17T12:35:35.939028",
        },
        scAvailable: 500,
        redirectionUrl: "/category/1",
    },
};
