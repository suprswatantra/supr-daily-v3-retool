import { SuprApi } from "@types";

const SCHEDULE_LIST: SuprApi.ScheduleRes = {
    statusCode: 0,
    data: {
        schedule: [
            {
                date: "2020-04-26",
                message: "Add or edit your orders before 11 pm Saturday",
                subscriptions: [],
                addons: [
                    {
                        id: 4657449,
                        quantity: 1.0,
                        remaining_quantity: 0,
                        sku_id: 1928,
                        modified: false,
                        status: "ORDER_SCHEDULED",
                    },
                ],
            },
            {
                date: "2020-04-28",
                message: "Add or edit your orders before 11 pm Monday",
                subscriptions: [
                    {
                        id: 1028453,
                        quantity: 1.0,
                        remaining_quantity: 2.0,
                        sku_id: 1915,
                        modified: false,
                        status: "ORDER_SCHEDULED",
                    },
                ],
                addons: [],
            },
            {
                date: "2020-04-29",
                message: "Add or edit your orders before 11 pm Tuesday",
                subscriptions: [
                    {
                        id: 1028453,
                        quantity: 1.0,
                        remaining_quantity: 1.0,
                        sku_id: 1915,
                        modified: false,
                        status: "ORDER_SCHEDULED",
                    },
                ],
                addons: [],
            },
            {
                date: "2020-04-30",
                message: "Add or edit your orders before 11 pm Wednesday",
                subscriptions: [
                    {
                        id: 1028453,
                        quantity: 1.0,
                        remaining_quantity: 0.0,
                        sku_id: 1915,
                        modified: false,
                        status: "ORDER_SCHEDULED",
                    },
                ],
                addons: [],
            },
        ],
        skuData: [
            {
                id: 1915,
                sku_name: "Green Chilli (100 g)",
                item: {
                    id: 2305,
                    name: "Green Chilli Pkt",
                    unit: {
                        id: 3,
                        name: "pkt",
                        sub_unit: "pkt",
                        conversion: "1.00",
                    },
                    unit_quantity: 1.0,
                    minimum_quantity: 1.0,
                    step_quantity: 1.0,
                },
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1915__14-10-2019-16h37m23s.jpg",
                    path: "product_images/sku__1915__14-10-2019-16h37m23s.jpg",
                    bgColor: "#FFF",
                    compressed_url: {
                        "200_200": { fullUrl: null },
                        "400_400": { fullUrl: null },
                    },
                },
                unit_price: 8.0,
                unit_mrp: 9.0,
                preferred_mode: "ADDON",
                search_terms: [
                    { term: "green", score: 10 },
                    { term: "chilli", score: 10 },
                    { term: "100", score: 10 },
                    { term: "vegetables", score: 7 },
                    { term: "fresh", score: 5 },
                    { term: "vegetables", score: 5 },
                ],
                active: true,
                out_of_stock: false,
                next_available_date: null,
                out_of_stock_preferred_flow: {
                    addon: "alternates",
                    subscription: "schedule",
                },
            },
            {
                id: 1928,
                sku_name: "Heritage Curd - (200 g)",
                item: {
                    id: 2318,
                    name: "Heritage Curd Pouch",
                    unit: {
                        id: 11,
                        name: "pouch",
                        sub_unit: "pouch",
                        conversion: "1.00",
                    },
                    unit_quantity: 1.0,
                    minimum_quantity: 1.0,
                    step_quantity: 1.0,
                },
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__1928__14-10-2019-16h37m27s.jpg",
                    path: "product_images/sku__1928__14-10-2019-16h37m27s.jpg",
                    bgColor: "#FFF",
                    compressed_url: {
                        "200_200": { fullUrl: null },
                        "400_400": { fullUrl: null },
                    },
                },
                unit_price: 13.0,
                unit_mrp: 13.0,
                preferred_mode: "SUBSCRIPTION",
                search_terms: [
                    { term: "heritage", score: 10 },
                    { term: "curd", score: 10 },
                    { term: "200", score: 10 },
                    { term: "curd", score: 7 },
                    { term: "dairy", score: 5 },
                    { term: "products", score: 5 },
                ],
                active: true,
                out_of_stock: false,
                next_available_date: null,
                out_of_stock_preferred_flow: {
                    addon: "alternates",
                    subscription: "schedule",
                },
            },
        ],
        message: "No orders for this day",
    },
    statusMessage: "success",
};

export default SCHEDULE_LIST;
