export const ORDER_HISTORY = {
    sku_details: [
        {
            image: {
                fullUrl:
                    "d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                path:
                    "images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                bgColor: "#FFF",
            },
            sku_name: "MTR Bisi Bele Bath (300 g)",
            item: {
                unit: {
                    id: 5,
                    name: "pkt",
                    sub_unit: "pkt",
                    conversion: 1,
                },
                id: 2029,
            },
            id: 1588,
        },
        {
            image: {
                fullUrl:
                    "d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                path:
                    "images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                bgColor: "#FFF",
            },
            sku_name: "Heritage Standardised Milk",
            item: {
                unit: {
                    id: 4,
                    name: "L",
                    sub_unit: "ml",
                    conversion: 1000,
                },
                id: 329,
            },
            id: 356,
        },
        {
            image: {
                fullUrl:
                    "d1dq5xs7rnkn6u.cloudfront.net/media/CACHE/images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                path:
                    "images/product_images/sku_1588__22-04-2019-00h27m16s/7c7355797629be6348259dc3b13da1c0.jpg",
                bgColor: "#FFF",
            },
            sku_name: "Nandini Curd-500 G",
            item: {
                unit: {
                    id: 3,
                    name: "pouch",
                    sub_unit: "pouch",
                    conversion: 1,
                },
                id: 333,
            },
            id: 367,
        },
    ],
    order_history: [
        {
            date: "2019-08-17",
            orders: [
                {
                    sku_id: 1588,
                    id: 15963130,
                    addon_id: 1781252,
                    status: "Delivered",
                    type: "addon",
                    delivered_quantity: 2,
                },
            ],
        },
        {
            date: "2019-08-16",
            orders: [
                {
                    sku_id: 356,
                    id: 15826668,
                    subscription_id: 563130,
                    status: "Cancelled",
                    type: "subscription",
                    delivered_quantity: 4,
                },
                {
                    sku_id: 367,
                    id: 15825407,
                    subscription_id: 225063,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 1,
                },
            ],
        },
        {
            date: "2019-08-15",
            orders: [
                {
                    sku_id: 356,
                    id: 15553305,
                    subscription_id: 563130,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 0.5,
                },
                {
                    sku_id: 367,
                    id: 15551072,
                    subscription_id: 225063,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 1,
                },
            ],
        },
        {
            date: "2019-08-14",
            orders: [],
        },
        {
            date: "2019-08-13",
            orders: [
                {
                    sku_id: 367,
                    id: 15406241,
                    subscription_id: 225063,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 1,
                },
            ],
        },
        {
            date: "2019-08-12",
            orders: [
                {
                    sku_id: 356,
                    id: 15273371,
                    subscription_id: 563130,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 0.5,
                },
                {
                    sku_id: 367,
                    id: 15271262,
                    subscription_id: 225063,
                    status: "Delivered",
                    type: "subscription",
                    delivered_quantity: 1,
                },
            ],
        },
    ],
};
