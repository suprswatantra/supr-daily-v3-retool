export const UI_MERCH_SUPR_SALARY_DAYA = {
    widgetType: "all_in_one_horizontal_scroll",
    data: {
        items: [
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        "margin-bottom": "16px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        "margin-bottom": "16px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        "margin-bottom": "16px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        "margin-bottom": "16px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                        height: "150px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
            {
                itemType: "supr_salary_day",
                data: {
                    type: "tile",
                    title: {
                        text: "Fruits & Vegetables",
                        type: "subtext10",
                        style: {
                            color: "var(--black-80)",
                        },
                    },
                    sub_text_1: {
                        text: "On a min. order of ₹1500",
                        type: "subtext10",
                        style: {
                            color: "var(--black-40)",
                        },
                    },
                    sub_text_2: {
                        text: "30% OFF",
                        type: "montserratExtraBold14",
                        style: {
                            color: "#f26d76",
                        },
                    },
                    image: {
                        data: {
                            path:
                                "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            bgColor: "#FFF",
                            fullUrl:
                                "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                            compressed_url: {
                                "200_200": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/200_200/sku__347__02-04-2020-18h35m24s.jpg",
                                },
                                "400_400": {
                                    fullUrl:
                                        "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/400_400/sku__347__02-04-2020-18h35m25s.jpg",
                                },
                            },
                        },
                        style: {},
                    },
                    style: {
                        "background-color": "#e8fcf1",
                        "border-radius": "4px",
                        "min-width": "100px",
                    },
                    cta: {
                        events: {
                            click: {
                                navigation: {
                                    link: "category/1",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                        saClick: {
                            objectName: "sku Name",
                            objectValue: "1411",
                        },
                    },
                },
            },
        ],
        title: {
            text: "SUPR SALARY DAY",
            type: "montserratExtraBold24",
            style: {
                color: "var(--white-100)",
            },
        },
        cta: {
            text: "1st - 3rd June",
            style: {
                "background-color": "#fce562",
                "border-radius": "16px",
            },
        },
        subtitle: {
            text: "Great prices. Great discounts!",
            type: "caption",
            style: {
                color: "var(--white-100)",
            },
        },
        style: {
            "background-color": "#8c7ee1",
        },
        colCount: null,
        rowCount: 2,
    },
};

export const UI_MERCH = {
    statusCode: 0,
    data: {
        collections: [
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    cta: null,
                    style: {
                        "background-color": "#fef7ef",
                    },
                    title: {
                        text: "Best Offer this week",
                        style: {
                            color: "#333333",
                        },
                    },
                    colCount: null,
                    rowCount: 1,
                    subtitle: {
                        text: "Avail offer for 3 deliveries",
                        style: {
                            color: "#999999",
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    maxAllowed: 6,
                    minAllowed: 1,
                    viewId: 606,
                    type: "sku",
                    items: [
                        {
                            itemType: "sku_circular_with_add_icon",
                            data: {
                                preferredMode: "direct_add",
                                entityId: 1119,
                            },
                            analytics: {
                                saImpression: {
                                    objectName: "collection-v3-item",
                                    objectValue: "{{ItemId}}",
                                    contextList: [
                                        {
                                            name: "collection-v3",
                                            value: "{{WidgetId}}",
                                        },
                                    ],
                                },
                            },
                        },
                        {
                            itemType: "sku_circular_with_add_icon",
                            data: {
                                preferredMode: "direct_add",
                                entityId: 1326,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "sku_circular_with_add_icon",
                            data: {
                                preferredMode: "direct_add",
                                entityId: 1777,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "sku_circular_with_add_icon",
                            data: {
                                preferredMode: "direct_add",
                                entityId: 1782,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "sku_circular_with_add_icon",
                            data: {
                                preferredMode: "direct_add",
                                entityId: 1787,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    cta: {
                        text: "See All",
                        textType: "body",
                        style: {
                            color: "var(--primary-120)",
                        },
                        events: {
                            click: {
                                navigation: {
                                    link: "collection/5",
                                    source: "in_app",
                                },
                            },
                        },
                    },
                    title: {
                        text: "Best Offer this week",
                        style: {
                            color: "var(--primary-120)",
                        },
                    },
                    colCount: null,
                    rowCount: 2,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    style: {
                        "background-color": "#fafafa",
                    },
                    maxAllowed: 6,
                    minAllowed: 1,
                    viewId: 607,
                    type: "sku",
                    items: [
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "//#endregionfafafa",
                                    "margin-bottom": "20px",
                                },
                                preferredMode: "direct_add",
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                entityId: 1119,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "#fafafa",
                                    "margin-bottom": "20px",
                                },
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                preferredMode: "direct_add",
                                entityId: 1326,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "#fafafa",
                                    "margin-bottom": "20px",
                                },
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                preferredMode: "direct_add",
                                entityId: 1777,
                            },
                        },
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "#fafafa",
                                    "margin-bottom": "20px",
                                },
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                preferredMode: "direct_add",
                                entityId: 1782,
                            },
                        },
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "#fafafa",
                                    "margin-bottom": "20px",
                                },
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                preferredMode: "direct_add",
                                entityId: 1787,
                            },
                        },
                        {
                            itemType: "sku_square_with_add_button",
                            data: {
                                style: {
                                    "background-color": "#fafafa",
                                    "margin-bottom": "20px",
                                },
                                image: {
                                    style: {
                                        "background-color": "#ffffff",
                                        "box-shadow":
                                            "0px 1px 4px rgba(0, 0, 0, 0.08)",
                                    },
                                },
                                preferredMode: "direct_add",
                                entityId: 347,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    title: {
                        text: "Fruits & Vegetables",
                        style: {
                            color: "#000000",
                        },
                    },
                    colCount: null,
                    rowCount: 2,
                    maxAllowed: 10,
                    minAllowed: 1,
                    viewId: 608,
                    type: "sku_collection",
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    items: [
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            trigger: "skuPreview",
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        path:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 1119,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                        path:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        path:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        path:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 6,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    title: {
                        text: "Fruits & Vegetables",
                        style: {
                            color: "#000000",
                        },
                    },
                    colCount: null,
                    rowCount: 1,
                    maxAllowed: 10,
                    minAllowed: 1,
                    viewId: 609,
                    type: "sku_collection",
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    items: [
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/606",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        path:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 606,
                                analytics: {
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        objectValue: "{{ItemId}}",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "{{WidgetId}}",
                                            },
                                        ],
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        path:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                        path:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        path:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        bgColor: "#ffffff",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    colCount: 1,
                    rowCount: 1,
                    maxAllowed: 1,
                    minAllowed: 1,
                    viewId: 610,
                    type: "sku",
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    items: [
                        {
                            itemType: "sku_rectangle_with_item_of_the_data",
                            data: {
                                style: {
                                    "background-color": "#f6f4ff",
                                },
                                title: {
                                    text: "ITEM OF THE DAY",
                                    style: {
                                        color: "#8c7ee1",
                                    },
                                },
                                subText1: null,
                                subText2: null,
                                analytics: null,
                                entityId: 347,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    style: {
                        "background-color": "#8c7ee1",
                    },
                    colCount: null,
                    rowCount: 1,
                    maxAllowed: 10,
                    minAllowed: 4,
                    viewId: 611,
                    type: "sku_collection",
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    items: [
                        {
                            itemType: "collection_square_with_supr_day_timer",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/5",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                style: {
                                    height: "155px",
                                    "min-width": "104px",
                                    "border-radius": "4px",
                                    "background-color": "#fff2a5",
                                },
                                title: {
                                    text: "SUPR DAY",
                                    textType: "montserratExtraBold24",
                                    style: {
                                        color: "#f26d76",
                                    },
                                },
                                analytics: {
                                    saClick: {
                                        objectName: "sku Name",
                                        objectValue: "1411",
                                    },
                                    saImpression: {
                                        objectName: "sku Name",
                                        objectValue: "1411",
                                    },
                                },
                                subtitle1: {
                                    text: "Explore Offers",
                                    textType: "subtext10",
                                    style: {
                                        color: "#f26d76",
                                    },
                                },

                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_square_with_supr_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        border:
                                            "solid 1px rgba(0, 255, 115, 0.5)",
                                    },
                                },
                                style: {
                                    height: "155px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Healthy Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "20 items",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_square_with_supr_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        border:
                                            "solid 1px rgba(0, 255, 115, 0.5)",
                                    },
                                },
                                style: {
                                    height: "155px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Healthy Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_square_with_supr_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        border:
                                            "solid 1px rgba(0, 255, 115, 0.5)",
                                    },
                                },
                                style: {
                                    height: "155px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Healthy Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_square_with_supr_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        border:
                                            "solid 1px rgba(0, 255, 115, 0.5)",
                                    },
                                },
                                style: {
                                    height: "155px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Healthy Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 4,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    cta: {
                        text: "1st - 3rd June",
                        style: {
                            color: "#333333",
                            "border-radius": "16px",
                            "background-color": "#fce562",
                            padding: "4px 8px",
                        },
                    },
                    style: {
                        "background-color": "#8c7ee1",
                    },
                    title: {
                        text: "SUPR SALARY DAY",
                        style: {
                            color: "#ffffff",
                        },
                        textType: "montserratExtraBold20",
                    },
                    colCount: null,
                    rowCount: 2,
                    subtitle: {
                        text: "Great prices. Great discounts!",
                        style: {
                            color: "#ffffff",
                        },
                        textType: "caption",
                    },
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    maxAllowed: 10,
                    minAllowed: 6,
                    viewId: 612,
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },

                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },

                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },

                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },

                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },

                        {
                            itemType: "collection_square_with_supr_salary_day",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        path:
                                            "product_images/sku__347__02-04-2020-17h29m58s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    margin: "0 0 16px 0",
                                    "min-width": "112px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fruits & Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                    textType: "caption",
                                },
                                subtitle1: {
                                    text: "On a min. order of ₹1500",
                                    style: {
                                        color: "#999999",
                                    },
                                    textType: "subtext10",
                                },
                                subtitle2: {
                                    text: "30% OFF",
                                    style: {
                                        color: "#f26d76",
                                    },
                                    textType: "montserratExtraBold14",
                                },
                                entityId: 1,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    title: {
                        text: "Shop by brands",
                        style: {
                            color: "#000000",
                        },
                    },
                    colCount: null,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    rowCount: 2,
                    subtitle: {
                        text: "Recently introduced on SuprDaily!",
                        style: {
                            color: "#999999",
                        },
                    },
                    maxAllowed: 10,
                    minAllowed: 6,
                    viewId: 613,
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/19/filter/104",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#fff7e5",
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/24/filter/381",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#feebe7",
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/24/filter/381",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#feebe7",
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/19/filter/104",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#fff7e5",
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/19/filter/104",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#fff7e5",
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/24/filter/381",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#feebe7",
                                },
                                entityId: 6,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/24/filter/381",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__381__13-01-2020-20h12m06s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#feebe7",
                                },
                                entityId: 7,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/19/filter/104",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/brand_images/attribute__104__09-10-2019-18h10m50s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    width: "95px",
                                    height: "95px",
                                    "border-radius": "50%",
                                    "background-color": "#fff7e5",
                                },
                                entityId: 8,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_vertical_scroll",
                data: {
                    title: {
                        text: "Explore more categories",
                        style: {
                            color: "#000000",
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    colCount: 3,
                    rowCount: null,
                    maxAllowed: 14,
                    minAllowed: 6,
                    viewId: 614,
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #50c1ba",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Milk",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #f9fbed",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#6ac259",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Daily needs",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/9",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #e48619",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#f8e0c4",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/14",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #e48619",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#f8e0c4",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Fresh Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #f2bc18",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#fff7e5",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/20",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #a86c24",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#fef3e7",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Bakery & Biscuits",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 6,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #f2bc18",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#fff7e5",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 7,
                            },
                        },
                        {
                            itemType: "collection_circle_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "95px",
                                        border: "1px solid #50c1ba",
                                        height: "95px",
                                        "border-radius": "50%",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "105px",
                                    height: "125px",
                                },
                                title: {
                                    text: "Milk",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 8,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_vertical_scroll",
                data: {
                    title: {
                        text: "Categories",
                        style: {
                            color: "#000000",
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    colCount: 2,
                    rowCount: null,
                    maxAllowed: 14,
                    minAllowed: 4,
                    viewId: 615,
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#ecfbfa",
                                },
                                title: {
                                    text: "Milk",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#e6f6fe",
                                },
                                title: {
                                    text: "Daily needs",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/9",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#f8ebe9",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/14",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#e8fcf1",
                                },
                                title: {
                                    text: "Fresh Vegetables",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#fbf3ea",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/20",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#fef3e7",
                                },
                                title: {
                                    text: "Bakery & Biscuits",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 6,
                            },
                        },
                        {
                            itemType: "collection_bar_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                },
                                style: {
                                    padding: "8px 4px",
                                    "border-radius": "4px",
                                    "background-color": "#fff7e5",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    style: {
                                        color: "#000000",
                                    },
                                },
                                entityId: 7,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_vertical_scroll",
                data: {
                    title: {
                        text: "Top Categories",
                        style: {
                            color: "#000000",
                        },
                    },
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    colCount: 3,
                    rowCount: null,
                    maxAllowed: 15,
                    minAllowed: 9,
                    viewId: 616,
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#ecfbfa",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Milk",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#6ac259",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Daily needs",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/9",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#f8e0c4",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/14",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#f8e0c4",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Fresh Vegetables",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#fff7e5",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/20",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "70px",
                                        padding: "0 14px",
                                        "background-color": "#fef3e7",
                                        "border-top-left-radius": "4px",
                                        "border-top-right-radius": "4px",
                                    },
                                },
                                style: {
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Bakery & Biscuits",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        height: "24px",
                                    },
                                },
                                entityId: 6,
                            },
                        },
                    ],
                },
            },
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    colCount: null,
                    rowCount: 1,
                    maxAllowed: 15,
                    minAllowed: 9,
                    viewId: 617,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-widget",
                            objectValue: "{{WidgetId}}",
                        },
                    },
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "50px",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Milk",
                                    style: {
                                        color: "#000000",
                                        width: "100%",
                                    },
                                },
                                entityId: 1,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "50px",
                                        "background-color": "#6ac259",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Daily needs",
                                    style: {
                                        color: "#000000",
                                        width: "100%",
                                    },
                                },
                                entityId: 2,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/9",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "50px",
                                        "background-color": "#f8e0c4",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    style: {
                                        color: "#000000",
                                        width: "100%",
                                    },
                                },
                                entityId: 3,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/14",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "50px",
                                        "background-color": "#f8e0c4",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Fresh Vegetables",
                                    style: {
                                        color: "#000000",
                                        width: "100%",
                                    },
                                },
                                entityId: 4,
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "category/15",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "50px",
                                        "background-color": "#fff7e5",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    style: {
                                        color: "#000000",
                                        width: "100%",
                                    },
                                },
                                entityId: 5,
                            },
                        },
                    ],
                },
            },
        ],
    },
    statusMessage: "success",
};

export const UI_MERCH_LAT = {
    statusCode: 0,
    data: {
        collections: [
            {
                widgetType: "all_in_one_horizontal_scroll",
                data: {
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "98px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    height: "88px",
                                    "margin-bottom": "4px !important",
                                },
                                title: {
                                    text: "Milk",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 1,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#e6f6fe",
                                    },
                                },
                                style: {
                                    width: "98px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    height: "88px",
                                    "margin-bottom": "4px !important",
                                },
                                title: {
                                    text: "Daily needs",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 2,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/3",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#f8ebe9",
                                    },
                                },
                                style: {
                                    width: "98px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    height: "88px",
                                    "margin-bottom": "4px !important",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 3,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                },
                            },
                        },
                    ],
                    viewId: 617,
                    rowCount: 1,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-see-all",
                            objectValue: "617",
                        },
                    },
                    maxAllowed: 10,
                    minAllowed: 6,
                },
            },
            {
                widgetType: "all_in_one_vertical_scroll",
                data: {
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Milk",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 1,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__2__09-10-2019-19h45m21s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#e6f6fe",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Daily needs",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 2,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/3",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__9__08-10-2019-21h36m01s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#f8ebe9",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Fresh Fruits",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 3,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/4",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__14__08-10-2019-21h37m21s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#e8fcf1",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Fresh Vegetables",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 4,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "4",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "4",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/5",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#fbf3ea",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 5,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "5",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "5",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/6",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__20__09-10-2019-19h48m40s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#fef3e7",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Bakery & Biscuits",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 6,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "6",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "6",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/7",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__15__09-10-2019-19h46m16s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#fff7e5",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Masala & Spices",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 7,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "7",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "7",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/8",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Milk",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 8,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "8",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "8",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_bottom_text",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/10",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://d1dq5xs7rnkn6u.cloudfront.net/media/category_images/category__1__03-03-2020-11h39m15s.jpg",
                                    },
                                    style: {
                                        width: "100%",
                                        height: "64px",
                                        padding: "4px 14px",
                                        "border-radius": "4px 4px 0px 0px",
                                        "background-color": "#ecfbfa",
                                    },
                                },
                                style: {
                                    width: "84px",
                                    height: "68px",
                                    "box-shadow":
                                        "0 1px 4px 0 rgba(0, 0, 0, 0.12)",
                                    "border-radius": "4px",
                                    "margin-bottom": "4px",
                                },
                                title: {
                                    text: "Milk",
                                    textType: "caption",
                                    style: {
                                        color: "#000000",
                                        padding: "2px 0",
                                    },
                                },
                                entityId: 10,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "10",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "616",
                                            },
                                        ],
                                        objectValue: "10",
                                    },
                                },
                            },
                        },
                    ],
                    title: {
                        text: "Top Categories",
                        style: {
                            color: "#000000",
                        },
                    },
                    viewId: 616,
                    colCount: 3,
                    rowCount: null,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-see-all",
                            objectValue: "616",
                        },
                    },
                    maxAllowed: 10,
                    minAllowed: 6,
                },
            },
            {
                widgetType: "all_in_one_vertical_scroll",
                data: {
                    type: "sku_collection",
                    items: [
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/1",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/yYLRdKb/Tile-1-Daily-Veggies.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 600,
                                    },
                                    style: {
                                        height: "400px",
                                    },
                                },
                                entityId: 1,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "1",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/2",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 600,
                                    },
                                    style: {
                                        height: "400px",
                                    },
                                },
                                entityId: 2,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "2",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/3",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/G0vdNcr/Tile-2-Exotic-Fruits-and-Veggies.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 3,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "3",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/4",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 4,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "4",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "4",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/5",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/kG4pMff/Tile-4-Leafy-Veggies.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 5,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "5",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "5",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/8",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 8,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "8",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "8",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/6",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 6,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "6",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "6",
                                    },
                                },
                            },
                        },
                        {
                            itemType: "collection_square_with_image",
                            data: {
                                cta: {
                                    events: {
                                        click: {
                                            navigation: {
                                                link: "collection/7",
                                                source: "in_app",
                                            },
                                        },
                                    },
                                },
                                image: {
                                    data: {
                                        path:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                        bgColor: "#ffffff",
                                        fullUrl:
                                            "https://i.ibb.co/BnBGHbd/Tile-3-Fresh-Fruits.png",
                                    },
                                    dimension: {
                                        width: 600,
                                        height: 400,
                                    },
                                },
                                entityId: 7,
                                analytics: {
                                    saClick: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "7",
                                    },
                                    saImpression: {
                                        objectName: "collection-v3-item",
                                        contextList: [
                                            {
                                                name: "collection-v3",
                                                value: "608",
                                            },
                                        ],
                                        objectValue: "7",
                                    },
                                },
                            },
                        },
                    ],
                    title: {
                        text: "Fruits & Vegetables",
                        style: {
                            color: "#000000",
                        },
                    },
                    viewId: 608,
                    colCount: 2,
                    analytics: {
                        saImpression: {
                            objectName: "collection-v3-see-all",
                            objectValue: "608",
                        },
                    },
                    // maxAllowed: 10,
                    // minAllowed: 6,
                },
            },
        ],
    },
    statusMessage: "success",
};
