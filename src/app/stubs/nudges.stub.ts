export const NUDGE_LIST = [
    {
        type: "address_retro",
        maxCount: 1000,
    },
    {
        type: "app_rating",
        maxCount: 20,
    },
];
