import { Sku } from "@models";

export default <Sku[]>[
    {
        unit_price: 259.0,
        id: 1,
        suprsku: false,
        unit_mrp: 259.0,
        item: {
            unit: {
                id: 1,
                name: "jar",
                sub_unit: "jar",
                conversion: "1",
            },
            step_quantity: 1,
            unit_quantity: 1,
            minimum_quantity: 1,
            name: "Bournvita Women - Jar",
        },
        // tslint:disable: max-line-length
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Bournvita Women (400 g)",
        search_terms: [
            {
                term: "bournvita",
                score: "10",
            },
            {
                term: "women",
                score: "10",
            },
            {
                term: "400",
                score: "10",
            },
        ],
    },
    {
        unit_price: 44.0,
        id: 2,
        suprsku: false,
        unit_mrp: 42.0,
        item: {
            unit: {
                id: 1,
                name: "L",
                sub_unit: "ml",
                conversion: "1000.0",
            },
            step_quantity: 0.5,
            unit_quantity: 1.0,
            minimum_quantity: 0.5,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Amul Taaza",
        search_terms: [
            {
                term: "amul",
                score: "10",
            },
            {
                term: "taaza",
                score: "10",
            },
        ],
    },
    {
        unit_price: 54.0,
        id: 3,
        suprsku: false,
        unit_mrp: 52.0,
        item: {
            unit: {
                id: 2,
                name: "L",
                sub_unit: "ml",
                conversion: "1000.0",
            },
            name: "Amul Gold",
            step_quantity: 0.5,
            unit_quantity: 1.0,
            minimum_quantity: 0.5,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Britannia 100% Whole Wheat Bread",
        search_terms: [
            {
                term: "amul",
                score: "10",
            },
            {
                term: "gold",
                score: "10",
            },
        ],
    },
    {
        unit_price: 280.0,
        id: 4,
        suprsku: false,
        unit_mrp: 280.0,
        item: {
            unit: {
                id: 1056,
                name: "pkt",
                sub_unit: "pkt",
                conversion: "1.0",
            },
            name: "Twinnings Green Tea",
            step_quantity: 1.0,
            unit_quantity: 1.0,
            minimum_quantity: 1.0,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Twinnings Green Tea (25 pcs)",
    },
    {
        unit_price: 396.0,
        id: 5,
        suprsku: false,
        unit_mrp: 396.0,
        item: {
            unit: {
                id: 1061,
                name: "pkt",
                sub_unit: "pkt",
                conversion: "1.0",
            },
            name: "Horlicks Classic - Carton",
            step_quantity: 1.0,
            unit_quantity: 1.0,
            minimum_quantity: 1.0,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Horlicks Classic (1 kg)",
    },
    {
        unit_price: 20.0,
        id: 6,
        suprsku: false,
        unit_mrp: 20.0,
        item: {
            unit: {
                id: 1064,
                name: "cup",
                sub_unit: "cup",
                conversion: "1.0",
            },
            name: "Mother Dairy Fruit Yogurt - Blueberry",
            step_quantity: 1.0,
            unit_quantity: 1.0,
            minimum_quantity: 1.0,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Mother Dairy Fruit Yogurt - Blueberry (100g)",
    },
    {
        unit_price: 20.0,
        id: 7,
        suprsku: false,
        unit_mrp: 20.0,
        item: {
            unit: {
                id: 1066,
                name: "cup",
                sub_unit: "cup",
                conversion: "1.0",
            },
            name: "Mother Dairy Fruit Yogurt - Raspberry",
            step_quantity: 1.0,
            unit_quantity: 1.0,
            minimum_quantity: 1.0,
        },
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            bgColor: "#FFF",
        },
        sku_name: "Mother Dairy Fruit Yogurt - Raspberry (100g)",
    },
];
