export const CART_DATA = {
    uuid: "41006469-278f-42fa-853e-48315c401b6a",
    items: [
        {
            type: "addon",
            sku_id: 1,
            delivery_date: "2019-08-11",
            quantity: 1,
        },
        {
            type: "addon",
            sku_id: 2,
            delivery_date: "2019-08-11",
            quantity: 1,
        },
        {
            type: "subscription_recharge",
            subscription_id: 1,
            recharge_quantity: 10,
        },
        {
            type: "subscription_new",
            recharge_quantity: 10,
            quantity: 1,
            start_date: "2019-08-11",
            sku_id: 1,
            frequency: {
                mon: true,
                tue: true,
                wed: true,
                thu: true,
                fri: true,
                sat: true,
                sun: true,
            },
        },
    ],
    coupon_code: "SUPRB2G2",
};

export const CART_RESPONSE_COUPON = {
    uuid: "41006469-278f-42fa-853e-48315c401b6a",
    items: [
        //   {
        //     "type": "subscription_recharge",
        //     "subscription_id": 368388,
        //     "recharge_quantity": 30,
        //     "coupon_code": null,
        //     "validation": {
        //       "is_valid": true,
        //       "message": "Ok"
        //     },
        //     "payment_data": {
        //       "pre_discount": 1320,
        //       "post_discount": 1320,
        //       "discount": 0
        //     }
        //   },
        {
            type: "addon",
            sku_id: 1,
            delivery_date: "2019-08-19",
            quantity: 1,
            coupon_code: "ADD1",
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 44,
                post_discount: 1,
                discount: 0,
            },
        },
        {
            type: "subscription_new",
            recharge_quantity: 15,
            coupon_code: null,
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 1720,
                post_discount: 1500,
                discount: 220,
            },
            start_date: "2019-09-01",
            quantity: 3,
            sku_id: 3,
            frequency: {
                mon: true,
                tue: false,
                wed: true,
                thu: true,
                fri: false,
                sat: true,
                sun: true,
            },
        },
    ],
    meta: {
        payment_data: {
            pre_discount: 2684,
            post_discount: 2641,
        },
        discount_info: {
            coupon_code: "ADD1",
            is_applied: true,
            discount_amount: 43,
            discount_type: "discount",
            message: null,
        },
        wallet_info: {
            balance: 430,
            is_sufficient: true,
            extra_required: 0,
        },
    },
};

export const CART_RESPONSE_NO_COUPON = {
    uuid: "41006469-278f-42fa-853e-48315c401b6a",
    items: [
        //   {
        //     "type": "subscription_recharge",
        //     "subscription_id": 368388,
        //     "recharge_quantity": 30,
        //     "coupon_code": null,
        //     "validation": {
        //       "is_valid": true,
        //       "message": "Ok"
        //     },
        //     "payment_data": {
        //       "pre_discount": 1320,
        //       "post_discount": 1320,
        //       "discount": 0
        //     }
        //   },
        {
            type: "addon",
            sku_id: 1,
            delivery_date: "2019-08-19",
            quantity: 1,
            coupon_code: "ADD1",
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 44,
                post_discount: 1,
                discount: 0,
            },
        },
        {
            type: "subscription_new",
            recharge_quantity: 30,
            coupon_code: null,
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 1320,
                post_discount: 1320,
                discount: 0,
            },
            start_date: "2019-08-21",
            quantity: 1,
            sku_id: 3,
            frequency: {
                mon: true,
                tue: false,
                wed: true,
                thu: true,
                fri: false,
                sat: true,
                sun: true,
            },
        },
    ],
    meta: {
        payment_data: {
            pre_discount: 2684,
            post_discount: 2641,
        },
        discount_info: {
            coupon_code: "ADD1",
            is_applied: false,
            discount_amount: 43,
            discount_type: "discount",
            message: null,
        },
        wallet_info: {
            balance: 430,
            is_sufficient: true,
            extra_required: 0,
        },
    },
};

export const CART_RESPONSE_WITH_SUPR_CREDITS_WITHOUT_COUPON = {
    statusCode: 0,
    data: {
        cart: {
            uuid: "34d555af-ee27-4410-ac2f-0bf35afc6afa",
            items: [
                {
                    type: "addon",
                    sku_id: 2660,
                    delivery_date: "2020-01-23",
                    quantity: 4.0,
                    coupon_code: null,
                    validation: {
                        is_valid: true,
                        message: "Ok",
                        status_code: 0,
                    },
                    payment_data: {
                        pre_discount: 115.0,
                        post_discount: 115.0,
                        supr_credits_used: 50,
                        real_amount: 65,
                    },
                    discount_info: {},
                },
            ],
            meta: {
                payment_data: {
                    pre_discount: 115.0,
                    post_discount: 115.0,
                    supr_credits_used: 50,
                    real_amount: 65,
                },
                discount_info: {},
                wallet_info: {
                    balance: 100.0,
                    supr_credits: 50,
                    is_sufficient: true,
                    extra_required: 0,
                },
            },
        },
    },
    statusMessage: "success",
};

export const CART_RESPONSE_WITH_SUPR_CREDITS_WITH_COUPON = {
    statusCode: 0,
    data: {
        cart: {
            uuid: "34d555af-ee27-4410-ac2f-0bf35afc6afa",
            items: [
                {
                    type: "addon",
                    sku_id: 2660,
                    delivery_date: "2020-01-23",
                    quantity: 4.0,
                    coupon_code: null,
                    validation: {
                        is_valid: true,
                        message: "Ok",
                        status_code: 0,
                    },
                    payment_data: { pre_discount: 96.0, post_discount: 96.0 },
                    discount_info: {},
                },
            ],
            meta: {
                payment_data: { pre_discount: 96.0, post_discount: 96.0 },
                discount_info: {},
                wallet_info: {
                    balance: 100.0,
                    is_sufficient: true,
                    extra_required: 0,
                },
            },
        },
    },
    statusMessage: "success",
};

export const CART_ERROR_RESPONSE = {
    statusCode: 1,
    statusMessage: "failure",
    data: {
        cart: {
            items: [
                {
                    start_date: "2019-10-05",
                    frequency: {
                        wed: true,
                        sat: true,
                        tue: true,
                        thu: true,
                        fri: true,
                        mon: true,
                        sun: true,
                    },
                    validation: {
                        message: "Item out of stock",
                        status_code: 0,
                        is_valid: true,
                    },
                    coupon_code: "B2G2",
                    sku_id: 1223,
                    quantity: 1,
                    payment_data: {},
                    recharge_quantity: 3,
                    type: "subscription_new",
                },
                {
                    delivery_date: "2019-10-07",
                    validation: {
                        message: "Ok",
                        status_code: 0,
                        is_valid: true,
                    },
                    coupon_code: null,
                    sku_id: 1206,
                    quantity: 1,
                    type: "addon",
                    payment_data: {
                        post_discount: 70,
                        pre_discount: 70,
                    },
                },
                {
                    delivery_date: "2019-10-05",
                    validation: {
                        message: "Delivery unavailable for 5th Oct",
                        status_code: 2,
                        is_valid: false,
                    },
                    coupon_code: null,
                    sku_id: 2536,
                    quantity: 1,
                    type: "addon",
                    payment_data: {},
                },
            ],
            uuid: "ac44b4a8-7393-43f0-a770-2bfa84f980a9",
            meta: {
                wallet_info: {
                    balance: 109,
                    is_sufficient: true,
                    extra_required: 0,
                },
                payment_data: {
                    post_discount: 70,
                    pre_discount: 70,
                },
                discount_info: {
                    coupon_code: "B2G2",
                    is_applied: true,
                    discount_amount: 150,
                    discount_type: "discount",
                    message: null,
                },
            },
        },
    },
};

export const VALIDATE_STUB = {
    statusCode: 0,
    data: {
        cart: {
            uuid: "1711e859-d60e-44f4-ad33-ab8fc9165b77",
            items: [
                {
                    type: "addon",
                    sku_id: 1930,
                    delivery_date: "2020-01-30",
                    quantity: 1.0,
                    coupon_code: null,
                    validation: {
                        is_valid: true,
                        message: "Ok",
                        status_code: 0,
                    },
                    payment_data: {
                        pre_discount: 30.0,
                        post_discount: 30.0,
                        supr_credits_used: 0,
                        real_amount: 0,
                        discount: 0,
                    },
                    discount_info: {},
                },
            ],
            meta: {
                payment_data: {
                    pre_discount: 30.0,
                    post_discount: 30.0,
                    supr_credits_used: 0.0,
                },
                discount_info: {},
                wallet_info: {
                    balance: 100.0,
                    supr_credits_usable: 0,
                    is_sufficient: true,
                    extra_required: 0,
                },
            },
        },
    },
    statusMessage: "success",
};
