export const SETTINGS = {
    // tslint:disable-next-line: max-line-length
    exitCartNudge:
        // tslint:disable-next-line: max-line-length
        '{"type":"exit_cart","vibrationEnabled":true,"vibrationTimeInMs":200,"title":"Did you forget something?","subTitle":"There are items in your cart. Complete your purchase to ensure delivery by 7 am tomorrow.","primaryButton":{"text":"Go to cart","action":true}, "secondaryButton": {"text":"Leave"}}',
    subBalanceBgColor: "#fff6eb",
    cartExitPromptEnabled: "true",
};
