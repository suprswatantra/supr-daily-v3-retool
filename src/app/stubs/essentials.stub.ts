export default [
    {
        id: 1,
        name: "Daily Essentials",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
            path: null,
            bgColor: "#FFF",
        },
        default: [
            {
                id: 6,
                name: "Cow",
                rank: 45,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h27m10s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 5,
                name: "Full Cream 1",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 7,
                name: "Full Cream 2",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 8,
                name: "Full Cream 3",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 9,
                name: "Full Cream 4",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 10,
                name: "Full Cream 5",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [1, 3],
            },
            {
                id: 11,
                name: "Full Cream 6",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [5],
            },
            {
                id: 12,
                name: "Full Cream",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [5],
            },
            {
                id: 13,
                name: "Full Cream",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [5],
            },
            {
                id: 14,
                name: "Full Cream",
                rank: 400,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_None__09-07-2019-12h28m56s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h26m49s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [5],
            },
        ],
    },
    {
        id: 2,
        name: "Weekly Essentials",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            path: null,
            bgColor: "#FFF",
        },
        default: [
            {
                id: 9,
                name: "Cookies",
                rank: 50,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h29m48s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [821, 823, 822],
            },
        ],
    },
    {
        id: 3,
        name: "Monthly Essentials",
        rank: 1,
        image: {
            fullUrl:
                "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
            path: null,
            bgColor: "#FFF",
        },
        default: [
            {
                id: 9,
                name: "Bakery",
                rank: 50,
                image: {
                    fullUrl:
                        "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
                    path:
                        "category_images/category_None__09-07-2019-12h29m48s.jpg",
                    bgColor: "#FFF",
                },
                skuIdList: [821, 823, 822],
            },
        ],
    },
];
