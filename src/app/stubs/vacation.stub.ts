import { Vacation } from "@models";

export const VACATION = <Vacation>{
    start_date: "2019-08-21",
    end_date: "2019-09-02",
};
