import { RatingOption } from "@models";

export default <RatingOption[]>[
    {
        displayName: "NOT REALLY !",
        optionContext: {
            type: "feedback",
            options: {
                type: "radio-button",
                subHeading: "How can we improve further?",
                heading: "We are on a mission to serve you better.",
                list: [
                    {
                        id: 1,
                        displayText: "Better selection",
                    },
                    {
                        id: 2,
                        displayText: "Better delivery",
                    },
                    {
                        id: 3,
                        displayText: "Better pricing and offers",
                    },
                    {
                        id: 4,
                        displayText: "Better customer support",
                    },
                    {
                        id: 5,
                        displayText: "Better app experience",
                    },
                ],
            },
        },
        optionId: 1,
        allowComments: false,
        iconUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/rating_icons/meh.png",
    },
    {
        displayName: "YES, I AM !!",
        optionContext: {
            type: "rating",
            options: {
                displayText:
                    "Glad you are enjoying SuprDaily! Take a moment to rate us on Play Store.",
                type: "text",
            },
        },
        optionId: 2,
        allowComments: false,
        iconUrl:
            "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/rating_icons/supr.png",
    },
];
