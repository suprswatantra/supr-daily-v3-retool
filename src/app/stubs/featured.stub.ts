export default {
    displayName: "Featured",
    itemList: [
        {
            id: 4,
            name: "Information message",
            rank: 1,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
                path: "feature_images/category_None__01-08-2019-16h03m08s.jpg",
                bgColor: "#FFF",
            },
            action: {
                type: "navigation",
                navigationParams: {
                    pageType: "essential",
                    paramValue: 1,
                    filterValue: 5,
                },
            },
        },
        {
            id: 3,
            name: "Buy 2 get 3",
            rank: 5,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
                path: "feature_images/category_None__30-07-2019-12h50m13s.jpg",
                bgColor: "#123",
            },
            action: {
                type: "static",
                navigationParams: null,
            },
        },
        {
            id: 4,
            name: "Information message",
            rank: 1,
            image: {
                fullUrl:
                    "https://d1dq5xs7rnkn6u.cloudfront.net/media_staging/category_images/category_1__01-07-2019-19h34m55s.jpg",
                path: "feature_images/category_None__01-08-2019-16h03m08s.jpg",
                bgColor: "#FFF",
            },
            action: {
                type: "navigation",
                navigationParams: {
                    pageType: "category",
                    paramValue: 2,
                    filterValue: 7,
                },
            },
        },
    ],
};
