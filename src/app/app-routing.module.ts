import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PAGE_ROUTES, SCREEN_NAMES } from "@constants";

const routes: Routes = [
    {
        path: "",
        children: [
            {
                path: "common",
                loadChildren: () =>
                    import("./pages/common/common.module").then(
                        (m) => m.CommonPageModule
                    ),
                data: {},
            },

            {
                path: PAGE_ROUTES.HOME.PATH,
                loadChildren: () =>
                    import("./pages/home/home.module").then(
                        (m) => m.HomePageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.HOME,
                },
            },
            {
                path: PAGE_ROUTES.CATEGORY.PATH,
                loadChildren: () =>
                    import("./pages/category/category.module").then(
                        (m) => m.CategoryPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.CATEGORY,
                },
            },
            {
                path: PAGE_ROUTES.SUB_CATEGORY.PATH,
                loadChildren: () =>
                    import("./pages/category/subcategory.module").then(
                        (m) => m.SubCategoryPageModule
                    ),
                data: { screenName: SCREEN_NAMES.SUGGESTION },
            },
            {
                path: PAGE_ROUTES.CATEGORY_ESSENTIALS.PATH,
                loadChildren: () =>
                    import("./pages/category/essentials.module").then(
                        (m) => m.CategoryEssentialsPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.CATEGORY_ESSENTIALS,
                },
            },
            {
                path: PAGE_ROUTES.CART.PATH,
                loadChildren: () =>
                    import("./pages/cart/cart.module").then(
                        (m) => m.CartPageModule
                    ),
                data: { screenName: SCREEN_NAMES.CART },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_CREATE.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/create/create.module"
                    ).then((m) => m.SubscriptionCreatePageModule),
                data: {
                    screenName: SCREEN_NAMES.SUBSCRIPTION_CREATE,
                },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_LIST_V2.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/list_v2/list.module"
                    ).then((m) => m.SubscriptionListV2PageModule),
                data: {
                    screenName: SCREEN_NAMES.SUBSCRIPTION_LIST,
                },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_SUMMARY.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/summary/subscription-summary.module"
                    ).then((m) => m.SubscriptionSummaryPageModule),
                data: {
                    screenName: SCREEN_NAMES.SUBSCRIPTION_SUMMARY,
                },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_DETAILS_V2.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/details_v2/details.module"
                    ).then((m) => m.SubscriptionDetailsV2PageModule),
                data: {
                    screenName: SCREEN_NAMES.SUBSCRIPTION_DETAILS,
                },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_RECHARGE.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/recharge/recharge.module"
                    ).then((m) => m.SubscriptionRechargePageModule),
                data: {
                    screenName: SCREEN_NAMES.RECHARGE_SUBSCRIPTION,
                },
            },
            {
                path: PAGE_ROUTES.SUBSCRIPTION_SCHEDULE.PATH,
                loadChildren: () =>
                    import(
                        "./pages/subscription/pages/schedule/subscription-schedule.module"
                    ).then((m) => m.SubscriptionSchedulePageModule),
                data: {
                    screenName: SCREEN_NAMES.SUBSCRIPTION_DELIVERY_SCHEDULE,
                },
            },
            {
                path: PAGE_ROUTES.AUTH.PATH,
                loadChildren: () =>
                    import("./pages/auth/auth.module").then(
                        (m) => m.AuthPageModule
                    ),
            },
            {
                path: PAGE_ROUTES.SEARCH.PATH,
                loadChildren: () =>
                    import("./pages/search/search.module").then(
                        (m) => m.SearchPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.SEARCH,
                },
            },
            {
                path: "cityselection",
                loadChildren: () =>
                    import("./pages/cityselection/cityselection.module").then(
                        (m) => m.CitySelectionPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.CITY_SELECTION,
                },
            },
            {
                path: PAGE_ROUTES.WALLET.PATH,
                loadChildren: () =>
                    import("./pages/wallet/wallet.module").then(
                        (m) => m.WalletPageModule
                    ),

                data: {
                    screenName: SCREEN_NAMES.WALLET,
                },
            },
            {
                path: PAGE_ROUTES.ADDRESS.PATH,
                loadChildren: () =>
                    import("./pages/address/address.module").then(
                        (m) => m.AddressPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.ADDRESS,
                },
            },
            {
                path: PAGE_ROUTES.SUPPORT.PATH,
                loadChildren: () =>
                    import("./pages/support/support.module").then(
                        (m) => m.SupportPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.SUPPORT,
                },
            },
            {
                path: PAGE_ROUTES.COMPLAINTS.PATH,
                loadChildren: () =>
                    import("./pages/complaints/complaints.module").then(
                        (m) => m.ComplaintsModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.COMPLAINTS,
                },
            },
            {
                path: PAGE_ROUTES.PROFILE.PATH,
                loadChildren: () =>
                    import("./pages/profile/profile.module").then(
                        (m) => m.ProfilePageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.PROFILE,
                },
            },
            {
                path: PAGE_ROUTES.REGISTER.PATH,
                loadChildren: () =>
                    import("./pages/register/register.module").then(
                        (m) => m.RegisterPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.REGISTER,
                },
            },
            {
                path: PAGE_ROUTES.SCHEDULE.PATH,
                loadChildren: () =>
                    import("./pages/schedule/schedule.module").then(
                        (m) => m.SchedulePageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.SCHEDULE,
                },
            },
            {
                path: PAGE_ROUTES.THANKYOU.PATH,
                loadChildren: () =>
                    import("./pages/thankyou/thankyou.module").then(
                        (m) => m.ThankyouPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.THANK_YOU,
                },
            },
            {
                path: PAGE_ROUTES.COLLECTION.PATH,
                loadChildren: () =>
                    import("./pages/category/collection.module").then(
                        (m) => m.CollectionPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.COLLECTION,
                },
            },
            {
                path: PAGE_ROUTES.SUPER_CREDITS_ONBOARDING.PATH,
                loadChildren: () =>
                    import("./pages/sconboarding/sconboarding.module").then(
                        (m) => m.SuperCreditsOnboardingPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.SUPER_CREDITS_ONBOARDING,
                },
            },
            {
                path: PAGE_ROUTES.OOS_ALTERNATES.PATH,
                loadChildren: () =>
                    import("./pages/oos-alternates/oos-alternates.module").then(
                        (m) => m.OOSAlternatesPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.OOS_ALTERNATES,
                },
            },
            {
                path: PAGE_ROUTES.SUPR_PASS.PATH,
                loadChildren: () =>
                    import("./pages/supr-pass/supr-pass.module").then(
                        (m) => m.SuprPassModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.SUPR_PASS,
                },
            },
            {
                path: PAGE_ROUTES.REFERRAL.PATH,
                loadChildren: () =>
                    import("./pages/referral/referral.module").then(
                        (m) => m.ReferralModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.REFERRAL,
                },
            },
            {
                path: PAGE_ROUTES.REWARDS.PATH,
                loadChildren: () =>
                    import("./pages/rewards/rewards.module").then(
                        (m) => m.RewardsModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.REWARDS,
                },
            },
            {
                path: PAGE_ROUTES.VACATION.PATH,
                loadChildren: () =>
                    import("./pages/vacation/supr-vacation.module").then(
                        (m) => m.SuprVacationModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.VACATION,
                },
            },
            {
                path: PAGE_ROUTES.DELIVERY_PROOF.PATH,
                loadChildren: () =>
                    import(
                        "./pages/delivery-proof/supr-delivery-proof.module"
                    ).then((m) => m.DeliveryProofModule),
                data: {
                    screenName: SCREEN_NAMES.DELIVERY_PROOF,
                },
            },
            {
                path: PAGE_ROUTES.DELIVERY_PROOF_V2.PATH,
                loadChildren: () =>
                    import(
                        "./pages/delivery-proof-v2/supr-delivery-proof.module"
                    ).then((m) => m.DeliveryProofV2Module),
                data: {
                    screenName: SCREEN_NAMES.DELIVERY_PROOF_V2,
                },
            },
            {
                path: PAGE_ROUTES.MILESTONE.PATH,
                loadChildren: () =>
                    import("./pages/milestone/milestone.module").then(
                        (m) => m.LandingPageModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.MILESTONE,
                },
            },
            {
                path: PAGE_ROUTES.OFFER_DETAILS.PATH,
                loadChildren: () =>
                    import("./pages/offer-details/offer-details.module").then(
                        (m) => m.OfferDetailsModule
                    ),
                data: {
                    screenName: SCREEN_NAMES.OFFER_DETAILS,
                },
            },
            {
                path: "**",
                redirectTo: "/",
            },
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            enableTracing: false,
            onSameUrlNavigation: "reload",
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
