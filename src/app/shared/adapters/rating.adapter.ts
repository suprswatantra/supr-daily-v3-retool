import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { SuprApi } from "@types";

import {
    StoreState,
    UserStoreSelectors,
    RatingStoreSelectors,
    RatingStoreActions,
} from "@supr/store";

@Injectable()
export class RatingAdapter {
    ratingOptions$ = this.store.pipe(
        select(RatingStoreSelectors.selectRatingOptions)
    );

    showRatingModal$ = this.store.pipe(
        select(RatingStoreSelectors.selectRatingShowModal)
    );

    ratingTrigger$ = this.store.pipe(
        select(RatingStoreSelectors.selectRatingTrigger)
    );

    isLoggedIn$ = this.store.pipe(select(UserStoreSelectors.selectIsLoggedIn));

    constructor(private store: Store<StoreState>) {}

    fetchRatingOptions() {
        this.store.dispatch(
            new RatingStoreActions.FetchRatingOptionsRequestAction()
        );
    }

    updateRatingAction(payload: SuprApi.RatingActionBody) {
        this.store.dispatch(
            new RatingStoreActions.UpdateRatingActionRequestAction(payload)
        );
    }

    saveFeedback(payload: SuprApi.RatingFeedbackBody) {
        this.store.dispatch(
            new RatingStoreActions.SaveFeedbackRequestAction(payload)
        );
    }

    hideRatingModal() {
        this.store.dispatch(new RatingStoreActions.HideRatingModal());
    }
}
