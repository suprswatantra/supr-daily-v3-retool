import { Injectable } from "@angular/core";

import { Store, select } from "@ngrx/store";

import {
    StoreState,
    SearchStoreActions,
    SearchStoreSelectors,
} from "@supr/store";

@Injectable()
export class SearchAdapter {
    constructor(private store: Store<StoreState>) {}

    currentState$ = this.store.pipe(
        select(SearchStoreSelectors.selectSearchCurrentState)
    );

    popularSearches$ = this.store.pipe(
        select(SearchStoreSelectors.selectPopularSearches)
    );

    checkSearchSubInstrumentation(skuId: number) {
        this.store.dispatch(
            new SearchStoreActions.CheckForSubInstrumentationContextAction(
                skuId
            )
        );
    }

    sendDeliverSubBalInstrumentation(skuId: number) {
        this.store.dispatch(
            new SearchStoreActions.SendDeliverSubBalInstrumentation(skuId)
        );
    }

    fetchPopularSearches() {
        this.store.dispatch(new SearchStoreActions.FetchPopularSearches());
    }
}
