import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { Sku, OutOfStockInfoStateType, SkuAttributes } from "@models";

import { FetchSimilarProductsActionData } from "@types";

import { StoreState, SkuStoreSelectors, SkuStoreActions } from "@supr/store";

@Injectable()
export class SkuAdapter {
    constructor(private store: Store<StoreState>) {}

    skuDict$ = this.store.pipe(select(SkuStoreSelectors.selectSkuDict));

    skuAttributes$ = this.store.pipe(
        select(SkuStoreSelectors.selectSkuAttributes)
    );

    notifiedSku$ = this.store.pipe(
        select(SkuStoreSelectors.selectNotifiedSkuDict)
    );

    getSkuAttributes(skuId: number): Observable<SkuAttributes> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuAlternatesById, { skuId })
        );
    }

    getSkuDetails(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, {
                skuId,
            })
        );
    }

    /* [TODO_RITESH] Deprecate this method and use getOOSInfoStateBySkuId - type isn't string */
    isNotified(skuId: number): Observable<OutOfStockInfoStateType> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuNotified, {
                skuId,
            })
        );
    }

    getOOSInfoStateBySkuId(skuId: number): Observable<OutOfStockInfoStateType> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuNotified, {
                skuId,
            })
        );
    }

    updateSkuOOSInfo(skuId: number, state: OutOfStockInfoStateType) {
        this.store.dispatch(
            new SkuStoreActions.UpdateSkuOosInfo({
                skuId,
                state,
            })
        );
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    updateSkuOosInfo_deprecate(
        skuId: number,
        state: OutOfStockInfoStateType,
        showToastMessage = false,
        singleLineUpdate = ""
    ) {
        this.store.dispatch(
            new SkuStoreActions.UpdateSkuOosInfoDeprecate({
                skuId,
                state,
                showToastMessage,
            })
        );

        this.store.dispatch(
            new SkuStoreActions.UpdateSkuNotify({
                skuId,
                state,
                singleLineUpdate,
            })
        );
    }

    fetchSimilarProducts(data: FetchSimilarProductsActionData) {
        if (data && data.skuId) {
            this.store.dispatch(
                new SkuStoreActions.FetchSimilarProductsRequestAction(data)
            );
        }
    }

    getSimilarProductsBySkuId(skuId: number): Observable<number[]> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSimilarProductsBySkuId, {
                skuId,
            })
        );
    }
}
