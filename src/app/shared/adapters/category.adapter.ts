import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import {
    StoreState,
    CategoryStoreActions,
    CategoryStoreSelectors,
} from "@supr/store";

import { Category } from "@models";

@Injectable()
export class CategoryAdapter {
    categoryList$ = this.store.pipe(
        select(CategoryStoreSelectors.selectCategoryList)
    );

    constructor(private store: Store<StoreState>) {}

    fetchCategoryList() {
        this.store.dispatch(
            new CategoryStoreActions.FetchCategoryListRequestAction()
        );
    }

    getCategoryMetaById(categoryId: number): Observable<Category> {
        return this.store.pipe(
            select(CategoryStoreSelectors.selectCategoryMetaById, {
                categoryId,
            })
        );
    }
}
