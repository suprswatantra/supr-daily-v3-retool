import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    MiscStoreActions,
    MiscStoreSelectors,
    UserStoreSelectors,
} from "@supr/store";

@Injectable()
export class NudgeAdapter {
    constructor(private store: Store<StoreState>) {}

    showCampaignPopupModal$ = this.store.pipe(
        select(MiscStoreSelectors.selectNudgeCampaignPopup)
    );

    userData$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    showCampaignPopupNudge() {
        this.store.dispatch(new MiscStoreActions.ShowCampaignPopupNudge());
    }

    hideCampaignPopupNudge() {
        this.store.dispatch(new MiscStoreActions.HideCampaignPopupNudge());
    }
}
