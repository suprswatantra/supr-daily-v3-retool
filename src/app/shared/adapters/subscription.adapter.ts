import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import {
    StoreState,
    RouterStoreSelectors,
    SubscriptionStoreActions,
    SubscriptionStoreSelectors,
    SuprPassStoreSelectors,
} from "@supr/store";

import { SuprApi } from "@types";
import { Subscription, Sku } from "@shared/models";

@Injectable()
export class SubscriptionAdapter {
    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    isSuprPassActive$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsSuprPassMember)
    );

    subscriptionState$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionState)
    );

    subscriptionError$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionError)
    );

    subscriptionList$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    constructor(private store: Store<StoreState>) {}

    getSubscriptionDetails(subscriptionId: number): Observable<Subscription> {
        return this.store.pipe(
            select(SubscriptionStoreSelectors.selectSubscriptionById, {
                subscriptionId,
            })
        );
    }

    updateSubscription(subscriptionId: number, body: SuprApi.SubscriptionBody) {
        this.store.dispatch(
            new SubscriptionStoreActions.UpdateSubscriptionRequestAction({
                body,
                subscriptionId,
            })
        );
    }

    updateSubscriptionInstruction(
        subscriptionId: number,
        quantity: number,
        date: string,
        instantRefreshSchedule?: boolean
    ) {
        this.store.dispatch(
            new SubscriptionStoreActions.UpdateSubscriptionInstructionRequestAction(
                {
                    subscriptionId,
                    body: {
                        quantity,
                        schedule_date: date,
                    },
                    instantRefreshSchedule,
                }
            )
        );
    }

    getSkuDetailsById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SubscriptionStoreSelectors.selectSkuDetailsById, {
                skuId,
            })
        );
    }

    getActiveSubscriptionBySkuId(skuId: number): Observable<Subscription> {
        return this.store.pipe(
            select(SubscriptionStoreSelectors.selectActiveSubscriptionBySkuId, {
                skuId,
            })
        );
    }

    fetchSubscriptions() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }
}
