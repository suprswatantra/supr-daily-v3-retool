import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    WalletStoreSelectors,
    UserStoreSelectors,
    SubscriptionStoreSelectors,
    VacationStoreSelectors,
    MiscStoreSelectors,
} from "@supr/store";

@Injectable()
export class SideMenuAdapter {
    walletBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletBalance)
    );

    suprCreditsEnabled$ = this.store.pipe(
        select(UserStoreSelectors.selectIsSuprCreditsEnabled)
    );

    isReferralEligible$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForReferral)
    );

    isRewardsEligible$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForRewards)
    );

    canUserApplyReferralCode$ = this.store.pipe(
        select(UserStoreSelectors.selectCanUserApplyReferralCode)
    );

    isExperimentsFetched$ = this.store.pipe(
        select(MiscStoreSelectors.selectExperimentsFetched)
    );

    isVacationActive$ = this.store.pipe(
        select(VacationStoreSelectors.selectIsVacationActive)
    );

    fullName$ = this.store.pipe(select(UserStoreSelectors.selectUserFullName));

    subscriptionList$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    isSuprPassEligible$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForSuprPass)
    );

    activeSuprPassDetails$ = this.store.pipe(
        select(UserStoreSelectors.selectActiveSuprPassDetails)
    );

    isAnonymousUser$ = this.store.pipe(
        select(UserStoreSelectors.isAnonymousUser)
    );

    constructor(private store: Store<StoreState>) {}
}
