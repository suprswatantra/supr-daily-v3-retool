import { CartAdapter } from "./cart.adapter";
import { CategoryAdapter } from "./category.adapter";
import { ScheduleAdapter } from "./schedule.adapter";
import { WalletAdapter } from "./wallet.adapter";
import { SideMenuAdapter } from "./sidemenu.adapter";
import { SubscriptionAdapter } from "./subscription.adapter";
import { AddonAdapter } from "./addon.adapter";
import { SkuAdapter } from "./sku.adapter";
import { PauseAdapter } from "./pause.adapter";
import { ErrorAdapter } from "./error.adapter";
import { RatingAdapter } from "./rating.adapter";
import { AddressAdapter } from "./address.adapter";
import { NudgeAdapter } from "./nudge.adapter";
import { FeedbackAdapter } from "./feedback.adapter";
import { PNAdapter } from "./pn.adapter";
import { UserAdapter } from "./user.adapter";
import { MiscAdapter } from "./misc.adapter";
import { ReferralAdapter } from "./referral.adapter";
import { CollectionAdapter } from "./collection.adapter";
import { SearchAdapter } from "./search.adapter";

export const sharedAdapters = [
    CategoryAdapter,
    CartAdapter,
    ScheduleAdapter,
    WalletAdapter,
    SideMenuAdapter,
    SubscriptionAdapter,
    AddonAdapter,
    SkuAdapter,
    PauseAdapter,
    ErrorAdapter,
    RatingAdapter,
    AddressAdapter,
    NudgeAdapter,
    FeedbackAdapter,
    PNAdapter,
    UserAdapter,
    MiscAdapter,
    ReferralAdapter,
    CollectionAdapter,
    SearchAdapter,
];

export * from "./cart.adapter";
export * from "./category.adapter";
export * from "./schedule.adapter";
export * from "./wallet.adapter";
export * from "./sidemenu.adapter";
export * from "./subscription.adapter";
export * from "./addon.adapter";
export * from "./sku.adapter";
export * from "./pause.adapter";
export * from "./error.adapter";
export * from "./rating.adapter";
export * from "./address.adapter";
export * from "./nudge.adapter";
export * from "./feedback.adapter";
export * from "./pn.adapter";
export * from "./user.adapter";
export * from "./misc.adapter";
export * from "./referral.adapter";
export * from "./collection.adapter";
export * from "./search.adapter";
