import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    UserStoreSelectors,
    FeedbackStoreSelectors,
    FeedbackStoreActions,
} from "@supr/store";

@Injectable()
export class FeedbackAdapter {
    isLoggedIn$ = this.store.pipe(select(UserStoreSelectors.selectIsLoggedIn));

    showDeliveryFeedbackNudge$ = this.store.pipe(
        select(FeedbackStoreSelectors.showDeliveryFeedbackNudge)
    );

    selectDeliveryFeedbackRequest$ = this.store.pipe(
        select(FeedbackStoreSelectors.selectDeliveryFeedbackRequest)
    );

    selectDeliverySkus$ = this.store.pipe(
        select(FeedbackStoreSelectors.selectDeliverySkus)
    );

    constructor(private store: Store<StoreState>) {}

    hideDeliveryFeedbackNudge() {
        this.store.dispatch(
            new FeedbackStoreActions.hideDeliveryFeedbackNudge()
        );
    }
}
