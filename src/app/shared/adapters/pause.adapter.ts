import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Vacation } from "@models";

import {
    StoreState,
    VacationStoreActions,
    VacationStoreSelectors,
} from "@supr/store";

@Injectable()
export class PauseAdapter {
    vacation$ = this.store.pipe(select(VacationStoreSelectors.selectVacation));

    vacationState$ = this.store.pipe(
        select(VacationStoreSelectors.selectVacationCurrentState)
    );

    vacationError$ = this.store.pipe(
        select(VacationStoreSelectors.selectVacationError)
    );

    vacationModalOpen$ = this.store.pipe(
        select(VacationStoreSelectors.selectIsVacationModalOpen)
    );

    isVacationActive$ = this.store.pipe(
        select(VacationStoreSelectors.selectIsVacationActive)
    );

    isSystemVacationActive$ = this.store.pipe(
        select(VacationStoreSelectors.selectIsSystemVacationActive)
    );

    systemVacation$ = this.store.pipe(
        select(VacationStoreSelectors.selectSystemVacation)
    );

    constructor(private store: Store<StoreState>) {}

    createVacation(data: Vacation) {
        this.store.dispatch(
            new VacationStoreActions.CreateVacationRequestAction(data)
        );
    }

    cancelVacation() {
        this.store.dispatch(
            new VacationStoreActions.CancelVacationRequestAction()
        );
    }

    togglePauseModal() {
        this.store.dispatch(new VacationStoreActions.ToggleVacationModal());
    }
}
