import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    PnStoreSelectors,
    MiscStoreSelectors,
    PnStoreActions,
    UserStoreSelectors,
} from "@supr/store";

@Injectable()
export class PNAdapter {
    constructor(private store: Store<StoreState>) {}

    pnType$ = this.store.pipe(select(PnStoreSelectors.selectPNType));

    pnData$ = this.store.pipe(select(PnStoreSelectors.selectPNData));

    appLaunchDone$ = this.store.pipe(
        select(MiscStoreSelectors.selectAppLaunchDone)
    );

    isLoggedIn$ = this.store.pipe(select(UserStoreSelectors.selectIsLoggedIn));

    resetPNData() {
        this.store.dispatch(new PnStoreActions.ResetPNData());
    }
}
