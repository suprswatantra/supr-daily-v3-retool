import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    MiscStoreSelectors,
    MiscStoreActions,
    SuprPassStoreSelectors,
    CartStoreSelectors,
    FeaturedStoreSelectors,
    FeaturedStoreActions,
    RouterStoreSelectors,
} from "@supr/store";
import { Observable } from "rxjs";
import { Featured } from "@shared/models";

@Injectable()
export class MiscAdapter {
    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    showGenericNudge$ = this.store.pipe(
        select(MiscStoreSelectors.selectNudgeGeneric)
    );

    showAutoDebitPaymentFailureNudge$ = this.store.pipe(
        select(MiscStoreSelectors.selectNudgeAutoDebitPaymentFailure)
    );

    showblockAccessNudge$ = this.store.pipe(
        select(MiscStoreSelectors.selectBlockAccessNudge)
    );

    blockAccessNudgeShowSkipBtn$ = this.store.pipe(
        select(MiscStoreSelectors.selectBlockAccessNudgeShowSkipBtn)
    );

    showCart$ = this.store.pipe(
        select(CartStoreSelectors.selectShowFloatingCart)
    );

    miniCart$ = this.store.pipe(select(CartStoreSelectors.selectMiniCart));

    suprPassSubscriptionId$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassSubscriptionId)
    );

    isSuprPassActive$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsSuprPassMember)
    );

    suprPassExperimentInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassExperimentInfo)
    );

    appLaunchDone$ = this.store.pipe(
        select(MiscStoreSelectors.selectAppLaunchDone)
    );

    appInitDone$ = this.store.pipe(
        select(MiscStoreSelectors.selectAppInitDone)
    );

    featuredCurrentState$ = this.store.pipe(
        select(FeaturedStoreSelectors.selectFeaturedCurrentState)
    );

    suprPassBigCardDetails$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassBigCardDetails)
    );

    suprPassSmallCardDetails$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassSmallCardDetails)
    );

    constructor(private store: Store<StoreState>) {}

    hideGenericNudge() {
        this.store.dispatch(new MiscStoreActions.HideGenericNudge());
    }

    hideAutoDebitPaymentFailureNudge() {
        this.store.dispatch(
            new MiscStoreActions.HideAutoDebitPaymentFailureNudge()
        );
    }

    hideBlockAcessNudge() {
        this.store.dispatch(new MiscStoreActions.HideBlockAccessNudge());
    }

    fetchFeaturedListByComponent(
        componentName: string
    ): Observable<Featured.List> {
        return this.store.pipe(
            select(FeaturedStoreSelectors.selectFeaturedDataByComponent, {
                componentName,
            })
        );
    }

    fetchFeaturedList(payload: Featured.Payload) {
        this.store.dispatch(
            new FeaturedStoreActions.FetchFeaturedRequestAction(payload)
        );
    }
}
