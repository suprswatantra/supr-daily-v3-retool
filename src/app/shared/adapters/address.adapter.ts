import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    UserStoreSelectors,
    AddressStoreSelectors,
    AddressStoreActions,
} from "@supr/store";

@Injectable()
export class AddressAdapter {
    showRetroModal$ = this.store.pipe(
        select(AddressStoreSelectors.selectAddressRetroShowModal)
    );

    isLoggedIn$ = this.store.pipe(select(UserStoreSelectors.selectIsLoggedIn));

    constructor(private store: Store<StoreState>) {}

    hideRetroModal() {
        this.store.dispatch(new AddressStoreActions.HideAddressRetroModal());
    }
}
