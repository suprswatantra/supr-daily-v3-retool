import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import {
    Sku,
    Schedule,
    OrderHistory,
    ScheduleDeliverySlotMap,
    OrderDeliverySlotMap,
} from "@models";
import {
    StoreState,
    OrderStoreSelectors,
    ScheduleStoreSelectors,
    ScheduleStoreActions,
    VacationStoreSelectors,
    FeedbackStoreActions,
    FeedbackStoreSelectors,
    RouterStoreSelectors,
    SuprPassStoreSelectors,
} from "@supr/store";

import { FetchScheduleActionPayload } from "@types";

@Injectable()
export class ScheduleAdapter {
    constructor(private store: Store<StoreState>) {}

    refreshSchedule$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleRefreshState)
    );

    isSuprPassActive$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectIsSuprPassMember)
    );

    scheduleCurrentState$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleCurrentState)
    );

    vacation$ = this.store.pipe(select(VacationStoreSelectors.selectVacation));

    scheduleDict$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectScheduleDict)
    );

    orderFeedbackRequest$ = this.store.pipe(
        select(FeedbackStoreSelectors.selectOrderFeedback)
    );

    pausedMsg$ = this.store.pipe(
        select(VacationStoreSelectors.selectVacationMsg)
    );

    pausedBySystemMsg$ = this.store.pipe(
        select(VacationStoreSelectors.selectSystemVacationMsg)
    );

    noSchedulesMessage$ = this.store.pipe(
        select(ScheduleStoreSelectors.selectNoSchedulesMessage)
    );

    noOrdersMessage$ = this.store.pipe(
        select(OrderStoreSelectors.selectNoOrdersMessage)
    );

    queryParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    multiplePOD$ = this.store.pipe(
        select(OrderStoreSelectors.selectMultiplePODFlag)
    );

    deliveryImage$ = this.store.pipe(
        select(OrderStoreSelectors.selectDeliveryImageByDateAndSlot)
    );

    getSchedule(actionPayload: FetchScheduleActionPayload) {
        this.store.dispatch(
            new ScheduleStoreActions.GetScheduleDataAction(actionPayload)
        );
    }

    getScheduleByDate(date: string): Observable<Schedule> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectScheduleByDate, { date })
        );
    }

    getScheduleDeliveryByDate(
        date: string
    ): Observable<ScheduleDeliverySlotMap> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectScheduleDeliveryByDate, {
                date,
            })
        );
    }

    getOrderByDate(date: string): Observable<OrderHistory> {
        return this.store.pipe(
            select(OrderStoreSelectors.selectOrderByDate, { date })
        );
    }

    getOrderDeliveryByDate(date: string): Observable<OrderDeliverySlotMap> {
        return this.store.pipe(
            select(OrderStoreSelectors.selectOrderDeliveryByDate, { date })
        );
    }

    getSkuFromOrderHistory(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(OrderStoreSelectors.selectSkuById, {
                skuId,
            })
        );
    }

    getSkuDetailsFromScheduleApiData(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(ScheduleStoreSelectors.selectScheduleSku, {
                skuId,
            })
        );
    }

    getOrderFeedback() {
        this.store.dispatch(new FeedbackStoreActions.setOrderFeedbackFetch());
    }
}
