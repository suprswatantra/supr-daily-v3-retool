import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";

import { V3Layout } from "@models";
import {
    StoreState,
    CollectionStoreSelectors,
    CollectionStoreActions,
    HomeLayoutStoreSelectors,
    HomeLayoutStoreActions,
} from "@supr/store";

@Injectable()
export class CollectionAdapter {
    collectionList$ = this.store.pipe(
        select(CollectionStoreSelectors.selectCollectionList)
    );

    collectionCurrentState$ = this.store.pipe(
        select(CollectionStoreSelectors.selectCollectionCurrentState)
    );

    collectionLayoutCurrentState$ = this.store.pipe(
        select(HomeLayoutStoreSelectors.selectHomeLayoutCurrentState)
    );

    constructor(private store: Store<StoreState>) {}

    fetchCollectionList() {
        this.store.dispatch(
            new CollectionStoreActions.FetchCollectionListRequestAction()
        );
    }

    fetchCollectionListForPosition(position: string): Observable<V3Layout[]> {
        return this.store.pipe(
            select(HomeLayoutStoreSelectors.selectHomeLayoutListByPosition, {
                position,
            })
        );
    }

    fetchCollectionLayoutList() {
        this.store.dispatch(
            new HomeLayoutStoreActions.FetchHomeLayoutListRequestAction()
        );
    }
}
