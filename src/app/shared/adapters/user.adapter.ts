import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { User } from "@models";

import { StoreState, UserStoreSelectors, UserStoreActions } from "@supr/store";

@Injectable()
export class UserAdapter {
    userCheckoutInfo$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCheckoutInfo)
    );

    showCheckoutDisabledNudge$ = this.store.pipe(
        select(UserStoreSelectors.selectShowCheckoutDisabledNudge)
    );

    whatsAppOptIn$ = this.store.pipe(
        select(UserStoreSelectors.selectWhatsAppOptIn)
    );

    userState$ = this.store.pipe(
        select(UserStoreSelectors.selectUserCurrentState)
    );

    userError$ = this.store.pipe(select(UserStoreSelectors.selectUserError));

    isAnonymousUser$ = this.store.pipe(
        select(UserStoreSelectors.isAnonymousUser)
    );

    user$ = this.store.pipe(select(UserStoreSelectors.selectUser));

    constructor(private store: Store<StoreState>) {}

    hideCheckoutDisabledNudge() {
        this.store.dispatch(new UserStoreActions.HideCheckoutDisabledNudge());
    }

    updateUser(user: User) {
        this.store.dispatch(
            new UserStoreActions.UpdateProfileRequestAction({ user })
        );
    }

    fetchProfileWithAddress() {
        this.store.dispatch(
            new UserStoreActions.LoadProfileWithAddressRequestAction({
                address: true,
                wallet: true,
                t_plus_one: true,
            })
        );
    }
}
