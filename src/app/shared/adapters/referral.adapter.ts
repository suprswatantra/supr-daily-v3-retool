import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { StoreState, ReferralStoreSelectors } from "@supr/store";

@Injectable()
export class ReferralAdapter {
    referralInfo$ = this.store.pipe(
        select(ReferralStoreSelectors.selectReferralInfo)
    );

    constructor(private store: Store<StoreState>) {}
}
