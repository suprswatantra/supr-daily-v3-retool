import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    WalletStoreSelectors,
    WalletStoreActions,
    RouterStoreSelectors,
} from "@supr/store";

@Injectable()
export class WalletAdapter {
    walletBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletBalance)
    );

    urlParams$ = this.store.pipe(select(RouterStoreSelectors.selectUrlParams));

    autoDebitPaymentModes$ = this.store.pipe(
        select(WalletStoreSelectors.selectAutoDebitPaymentModes)
    );

    walletState$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletCurrentState)
    );

    walletError$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletError)
    );

    rechargeCouponInfo$ = this.store.pipe(
        select(WalletStoreSelectors.selectRechargeCouponInfo)
    );

    walletCurrentState$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletCurrentState)
    );

    isFetchingWalletOffers$ = this.store.pipe(
        select(WalletStoreSelectors.selectIsFetchingWalletOffers)
    );

    walletOffersList$ = this.store.pipe(
        select(WalletStoreSelectors.selectWalletOffers)
    );

    isApplyingCouponCode$ = this.store.pipe(
        select(WalletStoreSelectors.selectIsApplyingCouponCode)
    );

    constructor(private store: Store<StoreState>) {}

    fetchAutoDebitPaymentModes(silent = false) {
        this.store.dispatch(
            new WalletStoreActions.FetchAutoDebitPaymentModesInfoRequestAction({
                silent,
            })
        );
    }

    fetchWalletOffers(amount: number) {
        this.store.dispatch(
            new WalletStoreActions.FetchWalletOffersRequestAction({
                amount,
            })
        );
    }

    applyRechargeCoupon(data: { couponCode: string; amount: number }) {
        this.store.dispatch(
            new WalletStoreActions.ApplyRechargeCouponRequestAction(data)
        );
    }
}
