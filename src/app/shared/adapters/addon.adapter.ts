import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import {
    StoreState,
    AddonStoreActions,
    AddonStoreSelectors,
} from "@supr/store";

@Injectable()
export class AddonAdapter {
    currentState$ = this.store.pipe(
        select(AddonStoreSelectors.selectAddonCurrentState)
    );
    constructor(private store: Store<StoreState>) {}

    updateAddOn(addonId: number, quantity: number) {
        this.store.dispatch(
            new AddonStoreActions.UpdateAddonRequestAction({
                addonId,
                quantity,
            })
        );
    }

    cancelAddOn(addonId: number) {
        this.store.dispatch(
            new AddonStoreActions.CancelAddonRequestAction({ addonId })
        );
    }
}
