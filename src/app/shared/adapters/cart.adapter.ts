import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import {
    StoreState,
    CartStoreSelectors,
    CartStoreActions,
    SkuStoreSelectors,
    WalletStoreActions,
    SubscriptionStoreActions,
    AddressStoreSelectors,
    VacationStoreSelectors,
    WalletStoreSelectors,
    CollectionStoreSelectors,
    UserStoreSelectors,
    RouterStoreSelectors,
    SuprPassStoreActions,
    SubscriptionStoreSelectors,
    SuprPassStoreSelectors,
} from "@supr/store";
import { CartItem, Sku } from "@models";
import { SuprApi, CartMini } from "@types";

@Injectable()
export class CartAdapter {
    showExitCartModal$ = this.store.pipe(
        select(CartStoreSelectors.selectExitCartShowModal)
    );

    urlParams$ = this.store.pipe(
        select(RouterStoreSelectors.selectQueryParams)
    );

    subscriptions$ = this.store.pipe(
        select(SubscriptionStoreSelectors.selectSubscriptionList)
    );

    suprPassInfo$ = this.store.pipe(
        select(SuprPassStoreSelectors.selectSuprPassInfo)
    );

    addRewardSku$ = this.store.pipe(
        select(CartStoreSelectors.selectAddRewardSku)
    );

    miniCart$ = this.store.pipe(select(CartStoreSelectors.selectMiniCart));

    miniCartPresent$ = this.store
        .pipe(select(CartStoreSelectors.selectMiniCart))
        .pipe(map((miniCart: CartMini) => miniCart.itemCount > 0));

    suprCreditsBalance$ = this.store.pipe(
        select(WalletStoreSelectors.selectSuprCreditsBalance)
    );

    useSuprCredits$ = this.store.pipe(
        select(CartStoreSelectors.selectUseSuprCredits)
    );

    isSuprPassEligible$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForSuprPass)
    );

    isAlternateDeliveryEligible$ = this.store.pipe(
        select(UserStoreSelectors.selectIsUserEligibleForAlternateDelivery)
    );

    cartId$ = this.store.pipe(select(CartStoreSelectors.selectCartId));

    cartItems$ = this.store.pipe(select(CartStoreSelectors.selectCartItems));

    cartMeta$ = this.store.pipe(select(CartStoreSelectors.selectCartMeta));

    cartOrder$ = this.store.pipe(select(CartStoreSelectors.selectCartOrder));

    cartCurrentState$ = this.store.pipe(
        select(CartStoreSelectors.selectCartCurrentState)
    );

    cartCouponCode$ = this.store.pipe(
        select(CartStoreSelectors.selectCartCouponCode)
    );

    cartError$ = this.store.pipe(select(CartStoreSelectors.selectCartError));

    cartModified$ = this.store.pipe(
        select(CartStoreSelectors.selectCartModified)
    );

    cartReValidate$ = this.store.pipe(
        select(CartStoreSelectors.selectCartReValidate)
    );

    hasAddress$ = this.store.pipe(
        select(AddressStoreSelectors.selectHasAddress)
    );

    isFetchingOffers$ = this.store.pipe(
        select(CartStoreSelectors.selectIsFetchingCartOffers)
    );

    cartOffersList$ = this.store.pipe(
        select(CartStoreSelectors.selectCartOffers)
    );

    vacation$ = this.store.pipe(select(VacationStoreSelectors.selectVacation));

    isApplyingCouponCode$ = this.store.pipe(
        select(CartStoreSelectors.selectIsCouponApplying)
    );

    isCartEmpty$ = this.store.pipe(
        select(CartStoreSelectors.selectCartIsEmpty)
    );

    showCart$ = this.store.pipe(
        select(CartStoreSelectors.selectShowFloatingCart)
    );

    cartMessageContent$ = this.store.pipe(
        select(CartStoreSelectors.selectCartMessageContent)
    );

    city$ = this.store.pipe(select(AddressStoreSelectors.selectCity));

    pastOrderSkuList$ = this.store.pipe(
        select(CollectionStoreSelectors.selectPastOrderSkuList)
    );

    pastOrderSkuListOriginal$ = this.store.pipe(
        select(CollectionStoreSelectors.selectPastOrderSkuListOriginal)
    );

    isFirstPurchase$ = this.store.pipe(
        select(CartStoreSelectors.selectisFirstPurchase)
    );

    orderCalendarBanner$ = this.store.pipe(
        select(CartStoreSelectors.selectOrderCalendarBanner)
    );

    constructor(private store: Store<StoreState>) {}

    getSkuById(skuId: number): Observable<Sku> {
        return this.store.pipe(
            select(SkuStoreSelectors.selectSkuById, { skuId })
        );
    }

    getSkuBySubscriptionId(subscriptionId: number): Observable<Sku> {
        return this.store.pipe(
            select(SubscriptionStoreSelectors.selectSubscriptionSkuDataById, {
                subscriptionId,
            })
        );
    }

    getCartItemById(skuId: number): Observable<CartItem> {
        return this.store.pipe(
            select(CartStoreSelectors.selectCartItemById, { skuId })
        );
    }

    getItemQuantity(skuId: number): Observable<number> {
        return this.store.pipe(
            select(CartStoreSelectors.selectCartItemQtyById, { skuId })
        );
    }

    updateItemInCart(
        item: CartItem,
        validateCart?: boolean,
        addRewardSku?: boolean
    ) {
        this.store.dispatch(
            new CartStoreActions.UpdateItemInCartAction({
                item,
                validateCart,
                addRewardSku,
            })
        );
    }

    updateCouponInCart(couponCode?: string) {
        this.store.dispatch(
            new CartStoreActions.UpdateCouponInCartAction({ couponCode })
        );
    }

    updateAddRewardSku(addRewardSku?: boolean) {
        this.store.dispatch(
            new CartStoreActions.UpdateAddRewardSku(addRewardSku)
        );
    }

    fetchCartOffers() {
        this.store.dispatch(
            new CartStoreActions.FetchCartOffersRequestAction()
        );
    }

    useSuprCreditsInCart(useSuprCredits?: boolean) {
        this.store.dispatch(
            new CartStoreActions.UseSuprCreditsInCartAction({ useSuprCredits })
        );
    }

    validateCart(
        cartBody?: SuprApi.CartBody,
        couponApplying?: boolean,
        addRewardSku?: boolean
    ) {
        this.store.dispatch(
            new CartStoreActions.ValidateCartRequestAction({
                cartBody,
                couponApplying,
                addRewardSku,
            })
        );
    }

    checkoutCart() {
        this.store.dispatch(new CartStoreActions.CheckoutCartRequestAction());
    }

    clearCart() {
        this.store.dispatch(new CartStoreActions.ClearCartAction());
    }

    fetchBalance() {
        this.store.dispatch(new WalletStoreActions.LoadBalanceRequestAction());
    }

    fetchSubscriptions() {
        this.store.dispatch(
            new SubscriptionStoreActions.FetchSubscriptionListRequestAction()
        );
    }

    fetchSuprPassInfo() {
        this.store.dispatch(
            new SuprPassStoreActions.FetchSuprPassRequestAction({
                silent: false,
            })
        );
    }

    toggleFloatingCart() {
        this.store.dispatch(new CartStoreActions.ToggleFloatingCart());
    }

    hideExitCartModal() {
        this.store.dispatch(new CartStoreActions.HideExitCartModal());
    }
}
