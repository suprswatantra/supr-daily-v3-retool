import { TextFragment } from "@shared/models";
import { CART_FOOTER_ACTION_TYPES } from "@constants";
import { Frequency, CartBannerConfig } from "@types";

import { WalletExpiryDetails } from "./transaction.model";
import { Banner } from "./banner.model";
import { RechargeCouponPaymentDetails } from "./wallet.model";

export interface CartOrder {
    addons: number[];
    subscriptions: number[];
    isFirstPurchase?: boolean;
    orderCalendarBanner?: Banner;
    postCheckoutContent?: PostCheckoutContent[];
}

export interface PostCheckoutContent {
    upperFoldText?: TextFragment;
    middleFoldText?: TextFragment;
    lowerFoldText?: TextFragment;
    imageUrl?: string;
    flipImage?: boolean;
}

export interface CartSlotInfo {
    [deliveryType: string]: {
        title: string;
        subtitle?: string;
        delivery_timeslots: string;
    };
}

export interface CartMeta {
    payment_data?: CartPaymentData;
    discount_info?: CartDiscountInfo;
    total_discount_amount?: number;
    wallet_info?: CartWalletInfo;
    supr_pass_applied?: boolean;
    auto_applied_referral_coupon?: boolean;
    show_supr_pass_prompt?: boolean;
    auto_add_supr_pass_prompt?: SuprPassPrompt;
    message_info?: CartMessageInfo;
    delivery_fee_message_info?: DeliveryFeeMessageInfo;
    service_fee_banner?: Banner;
    common_info_banner?: Banner;
    access_savings_banner?: CartBannerConfig;
    slot_info?: CartSlotInfo;
    footer?: CartFooterMeta;
    savings_info?: CartSavingsInfo;
    cart_prompt?: CartPrompt;
    switch_access_price?: boolean;
    reward_sku_banner?: CartRewardSkuBanner;
}

export interface CartRewardSkuBanner {
    reward_sku_id?: number;
    basket_offer_id?: number;
    title?: string;
    description?: string;
    state_info?: {
        state?: CartRewardSkuInfoState;
        text?: string;
        cta?: {
            text?: string;
            link?: string;
            source?: string;
        };
    };
    tnc?: string[];
}

export enum CartRewardSkuInfoState {
    ADD_MORE = "ADD_MORE",
    ADD_TO_CART = "ADD_TO_CART",
    ADDED_TO_CART = "ADDED_TO_CART",
}

export interface SuprPassPrompt {
    title?: string;
    savingsText?: string[];
    logoUrl?: string;
    subTitle?: string;
    warningText?: string;
}

export interface CartPrompt {
    title?: string;
    imgUrl?: string;
    subTitle?: string;
    skipBtnText?: string;
    actionBtnText?: string;
    actionUrl?: string;
    maxCount?: number;
}

export interface CartSavingsInfo {
    total_savings?: number;
    savings_percent?: number;
    service_fee_saving?: number;
    supr_access_discount?: number;
    supr_credits_used?: number;
    item_discount?: number;
    coupon_discount?: number;
}

export interface Cart {
    uuid?: string;
    items: CartItem[];
    meta: CartMeta;
}

export enum CartItemDeliveryType {
    REGULAR_DELIVERY = "REGULAR_DELIVERY",
    ALTERNATE_DELIVERY = "ALTERNATE_DELIVERY",
}

export interface DeliveryFeeMessageInfo {
    title?: string;
    description?: string;
    short_info?: string;
    link_info?: DeliveryFeeLinkInfo;
}

export interface DeliveryFeeLinkInfo {
    url: string;
    outsideRedirection: boolean;
    linkText: string;
}

export interface CartItem {
    type: string;
    quantity?: number;
    sku_id?: number;
    delivery_date?: string;
    subscription_id?: number;
    recharge_quantity?: number;
    start_date?: string;
    end_date?: string;
    plan_id?: number;
    frequency?: Frequency;
    coupon_code?: string;
    payment_data?: CartPaymentData;
    validation?: CartItemValidation;
    delivery_type?: CartItemDeliveryType;
    is_auto_added?: boolean;
    is_supr_access?: boolean;
    is_reward_sku?: boolean;
    sku_footer?: SkuFooter;
    header_text?: string;
    add_reward_sku_to_cart?: boolean;
}

export interface SkuFooter {
    text: string;
    url?: string;
}

export interface CartPaymentData {
    pre_discount?: number;
    post_discount?: number;
    discount?: number;
    supr_credits_used?: number;
    real_amount?: number;
    delivery_fee?: number;
    pre_delivery_fee?: number;
}

export interface CartDiscountInfo {
    benefit_source?: string;
    coupon_code?: string;
    is_applied?: boolean;
    discount_type?: string;
    discount_amount?: number;
    supr_credits_awarded?: number;
    message?: string;
    is_recharge_coupon?: boolean;
    is_payment_coupon?: boolean;
    success_message?: string;
    tnc?: string;
    paymentDetails?: RechargeCouponPaymentDetails;
}

export interface CartWalletInfo {
    balance: number;
    supr_credits_balance?: number;
    updated_supr_credits_balance?: number;
    expiry?: WalletExpiryDetails;
    supr_credits_usable?: number;
    is_sufficient: boolean;
    extra_required: number;
}

export interface CartItemValidation {
    is_valid?: boolean;
    message?: string;
    status_code?: number;
}

export interface CartMessageInfo {
    title: string;
    description: string;
    cta?: CartMessageInfoCta;
}

export interface CartMessageInfoCta {
    link?: string;
    source?: string;
    text?: TextFragment;
}

export interface CheckoutError {
    message_info?: CartMessageInfo;
}

export interface AlternateDeliveryModalInfo {
    image: string;
    url: string;
    outsideRedirection: boolean;
}
export interface AlternateDeliveryBannerInfo {
    type?: string;
    icon?: string;
    title: string;
    enabled: boolean;
    ctaIcon?: string;
    subtitle: string;
    showMoreText: string;
    modalInfo: AlternateDeliveryModalInfo;
}

export interface FooterMetaLineItemPart {
    prefix: FooterIconObj;
    text: FooterTextObj;
    suffix: FooterIconObj;
    action: FooterLineItemAction;
}
export interface FooterLineItemAction {
    type: FooterLineItemActionType;
    url: string;
}

export type FooterLineItemActionType = CART_FOOTER_ACTION_TYPES | "";

export interface FooterIconObj {
    iconName: string;
    iconSize: string;
    iconColor: string;
    iconType: string;
    iconStyle?: object;
}

export interface FooterTextObj {
    text: string;
    textType: string;
    textColor: string;
    textStyle?: object;
}

export interface FooterMetaLineItem {
    parts: FooterMetaLineItemPart[];
    multipleLines: boolean;
}

export interface CartFooterMeta {
    lineItems: FooterMetaLineItem[];
}

export interface AutoAdd {
    suprAccess: boolean;
    referralCode: boolean;
}
