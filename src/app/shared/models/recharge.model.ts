import { WalletPayment } from "./wallet-payment.model";

export class Recharge {
    recharged_date_time: Date;
    recharge_quantity: string;
    unit_price: string;
    subscription: number;
    wallet_payment: WalletPayment;
}
