export interface Hub {
    cut_off_time: string;
    name: string;
    id: number;
    delivery_fee: number;
    cash_pickup: boolean;
}
