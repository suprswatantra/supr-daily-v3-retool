import { Order, Feedback } from "@models";

import { DropdownOptions } from "@types";

export namespace SELFSERVE {
    export interface OrderSelfServe {
        orderData?: Order[];
        selected: boolean;
        isOrderDetails: boolean;
        quantity: number;
        issue: string;
        error: string;
        feedbackParamsObject?: Feedback.ParamsObject;
        feedbackParams?: string[];
        id: number;
        order_id: number;
        disabled: boolean;
        sku_name: string;
        name?: string;
        description?: string;
        icon_image_url?: string;
        has_child?: boolean;
        params?: string[];
        selectedItem?: SelectedItem;
        dropdownList?: DropdownOptions[];
        maxQuantity?: number;
        feedback_request_id?: number;
    }

    interface SelectedItem {
        id: number;
        orderData: Order;
    }

    export interface OND_Template {
        id: number;
        name: string;
        disabled: boolean;
        is_ond: boolean;
        isOndSelected?: boolean;
        description?: string;
        icon_image_url?: string;
        has_child?: boolean;
        params?: string[];
    }

    export interface Node_TYPE {
        id: number;
        name?: string;
        description?: string;
        icon_image_url?: string;
        expand_template?: Node_TYPE[];
        complaint_id?: boolean;
        disabled?: boolean;
        quantity?: number;
        params?: string[];
        ond_template?: OND_Template;
        order_id?: number;
        feedback_params?: string[];
        feedback_request_id: number;
    }

    export interface STATIC_NODE {
        id: number;
        nodeType: string;
        name: string;
        description?: string;
        expand_template?: STATIC_NODE[];
        data?: {
            icon?: string;
            title?: string;
            text?: string;
            textColor?: string;
            fontSize?: string;
            nav?: {
                url?: string;
                type?: string;
            };
            listItems?: string[];
            transferToAgent?: boolean;
        };
    }
}
