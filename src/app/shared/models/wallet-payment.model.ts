export class WalletPayment {
    id: number;
    customer: number;
    payable_amount: string;
    paid_amount: string;
    coupon_code: string;
    type: string;
    payment_type: string;
    sku_id: number;
    quantity: string;
    unit_price: string;
    details?: any;
    discount?: any;
}
