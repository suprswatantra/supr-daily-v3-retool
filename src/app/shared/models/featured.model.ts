import { Image } from "./image.model";

export namespace Featured {
    export interface NavigationParams {
        pageType: string;
        paramValue?: number;
        filterValue?: number;
    }

    export interface Action {
        type: string;
        navigationParams?: NavigationParams;
    }

    export interface Item {
        id: number;
        name: string;
        rank: number;
        image: Image;
        action: Action;
    }

    export interface List {
        displayName: string;
        itemList: Item[];
        version?: number;
    }

    export interface Dict {
        [name: string]: List;
    }

    export interface Payload {
        component: string;
        component_id?: string;
        silent?: boolean;
    }
}
