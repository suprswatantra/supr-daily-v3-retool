export interface TextFragment {
    text: string;
    textColor?: string;
    fontSize?: string;
    highlightedText?: string;
    highlightedTextColor?: string;
    bgColor?: string;
    isNumber?: boolean;
    animationNumValues?: number[];
}
