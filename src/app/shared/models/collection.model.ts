import { SubCategory, Sku } from "@models";

export interface Collection {
    viewId: number;
    title: CollectionText;
    subtitle: CollectionText;
    displayType: string;
    background: CollectionBackground;
    type: string;
    items: CollectionItem[];
    bookmarkTag?: CollectionBookmarkTag;
    tnc?: CollectionText;
    seeAll?: CollectionSeeAll;
    urgencyImage?: CollectionImage;
    pastOrder?: boolean;
    singleCardImg?: CollectionImage;
}

export interface CollectionText {
    text: string;
    color?: string;
}

export interface CollectionBackground {
    image?: CollectionImage;
    color: string;
}

export interface CollectionImage {
    fullUrl?: string;
    path?: string;
    bgColor?: string;
    compressed_url?: {
        "200_200"?: {
            fullUrl: string;
        };

        "400_400"?: {
            fullUrl: string;
        };
    };
}

export interface CollectionBookmarkTag {
    title: CollectionText;
    subtitle?: CollectionText;
    bgColor?: string;
}

export interface CollectionItem {
    entityId: number;
    image: CollectionItemImage;
    borderColor?: string;
    preferredMode: string;
    tags: CollectionItemTag[];
}

export interface CollectionItemImage extends CollectionImage {
    bgColor?: string;
}

export interface CollectionItemTag {
    type: string;
    icon: CollectionItemTagIcon;
    title: CollectionText;
    bgColor?: string;
}

export interface CollectionItemTagIcon extends CollectionImage {
    position?: string;
}

export interface CollectionSeeAll {
    button?: CollectionSeeAllText;
    card?: CollectionSeeAllText;
}

export interface CollectionSeeAllText {
    title: CollectionText;
    bgColor?: string;
    borderColor?: string;
}

export interface CollectionDetail {
    viewId: number;
    title: CollectionText;
    subtitle: CollectionText;
    image: CollectionImage;
    background: CollectionBackground;
    skuCategorizationType: string;
    default: SubCategory[];
    filtered: SubCategory[];
    skuIdList: number[];
    skuList: Sku[];
}

export enum CollectionPositions {
    HOME_SECTION_1 = "home_section_1",
    HOME_SECTION_2 = "home_section_2",
    HOME_SECTION_3 = "home_section_3",
}

export type CollectionListWithPosition = {
    [position in CollectionPositions]: Collection[];
};
