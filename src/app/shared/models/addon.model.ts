export interface Addon {
    id: number;
    sku?: number;
    type?: string;
    quantity?: number;
    deliveryDate?: string;
}
