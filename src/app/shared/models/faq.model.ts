import { Meta } from "./support.model";

export interface Faq {
    id: number;
    type: string;
    title: string;
    description?: string;
    actions?: FaqAction[];
    children?: Faq[];
}

export interface FaqAction {
    type: string;
    meta: Meta;
}
