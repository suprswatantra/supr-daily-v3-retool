export class ContextOptionListItem {
    id: number;
    displayText: string;
}

export class ContextOptions {
    type: string;
    androidDisplayText?: string;
    iosDisplayText?: string;
    heading?: string;
    subHeading?: string;
    list?: ContextOptionListItem[];
}

export interface OptionContext {
    type: string;
    options: ContextOptions;
}

export class RatingOption {
    optionId: number;
    displayName: string;
    optionContext: OptionContext;
    allowComments: boolean;
    iconUrl: string;
}
