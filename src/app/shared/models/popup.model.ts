export interface Popup {
    url?: string;
    title?: string;
    desc?: string;
    img?: string;
    buttonUrl?: string;
    buttonText?: string;
}
