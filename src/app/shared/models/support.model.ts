export enum SUPPORT_ACTION_TYPE {
    Chat = "chat",
    Call = "call",
    Email = "email",
    Faq = "faq",
}

export interface Meta {
    phoneNumber?: string;
    provider?: string;
    email?: string;
}

export interface SupportAction {
    type: SUPPORT_ACTION_TYPE;
    text: string;
    enabled: boolean;
    meta?: Meta;
}
