import { TextFragment } from "./text-fragment.model";

export interface CopyBlock {
    copyText?: TextFragment;
    bgColor?: string;
    iconColor?: string;
    borderColor?: string;
    hideCopyText?: boolean;
}
