import { Image } from "./image.model";
import { Banner } from "./banner.model";

export interface CategoryMeta {
    id: number;
    name: string;
    image: Image;
    rank?: number;
}

export interface SubCategory extends CategoryMeta {
    skuIdList: number[];
}

export interface CategoryOOSBanner {
    isEnabled: boolean;
    banner: Banner;
}
export interface Category extends CategoryMeta {
    default?: SubCategory[];
    filtered?: SubCategory[];
    oosBanner?: CategoryOOSBanner;
}
