import { Image } from "./image.model";
import { SuprPassPlan } from "./supr-pass.model";

/* [TODO] Revert the following fields back to mandatory step_quantity, unit_quantity, minimum_quantity, name */
export interface SkuItem {
    id: number;
    unit?: {
        id?: number;
        name?: string;
        sub_unit?: string;
        conversion?: string;
    };
    step_quantity?: number;
    unit_quantity?: number;
    minimum_quantity?: number;
    name?: string;
}

export interface SkuSearchTerm {
    term: string;
    score: number;
    weight?: number;
}
export type OutOfStockPreferredFlowType = "schedule" | "alternates";
export interface OutOfStockPreferredFlow {
    addon: OutOfStockPreferredFlowType;
    subscription: OutOfStockPreferredFlowType;
}

/* [TODO] Revert the following fields back to mandatory unit_price, unit_mrp, suprsku */
export interface Sku {
    id: number;
    sku_name?: string;
    unit_price?: number;
    unit_mrp?: number;
    access_price?: number;
    non_access_price?: number;
    item?: SkuItem;
    image?: Image;
    suprsku?: boolean;
    weight?: number;
    search_terms?: SkuSearchTerm[];
    preferred_mode?: string;
    out_of_stock?: boolean;
    active?: boolean;
    next_available_date?: string;
    discount_info?: DiscountInfo;
    out_of_stock_preferred_flow?: OutOfStockPreferredFlow;
    is_virtual_sku?: boolean;
    virtual_sku_info?: VirtualSkuInfo;
    oos_info?: OutOfStockInfo;
    product_identifier?: number;
    hub_sku_unit_price?: number;
    pricing_type?: PricingType;
    notify_enabled?: boolean;
}

export interface DiscountInfo {
    text?: string;
    value: string;
    mode: string;
    style?: {
        color?: string;
    };
}

export interface VirtualSkuInfo {
    type?: string;
    data?: VirtualSkuInfoData;
}

export interface VirtualSkuInfoData {
    plans: SuprPassPlan[];
}

export interface OutOfStockInfo {
    navigate?: boolean;
    state?: OutOfStockInfoStateType;
    oos_upfront?: boolean;
}

export type PricingType = "exclusive";

export type OutOfStockInfoStateType =
    | "add"
    | "oos"
    | "notify"

    // When Item is Out of stock and next available date there
    | "unNotify"
    | "notifySl";

export enum SkuRewardTagType {
    SAMPLE = "SAMPLE",
    BASKET_OFFER_REWARD = "BASKET_OFFER_REWARD",
}

export interface SkuRewardInfoInterface {
    tagName: string;
    suprPassTagName?: string;
    style: {
        tagBgColor: string;
        tagTextColor: string;
        rewardIconColor: string;
        infoIconColor: string;
    };
    offerDescription: Array<string>;
}

export interface SkuAttributes {
    [skuId: number]: {
        alternate_sku_ids?: number[];
        out_of_stock?: boolean;
        oos_info?: OutOfStockInfo;
    };
}
