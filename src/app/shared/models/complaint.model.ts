export interface ComplaintViewV2 {
    complaint_id: number;
    complaint_creation_time: string;
    complaint_type: string;
    order_date: string;
    complaintDate?: Date;
    time_slot: string;
    complaint_type_text: string;
    sku_names: string[];
    complaint_status_timeline: ComplaintTimeline;
    is_new?: boolean;
}

export interface ComplaintCreatedList {
    complaint_id: number;
    complaint_type: string;
    ops_inv_req: boolean | null;
}

export interface ComplaintTimeline {
    complaint_status_tag: string;
    complaint_status: string;
    colour: string;
    updated_at: string;
}

export enum ComplaintStatus {
    COMPLAINTS_STATUS_1 = "Open",
    COMPLAINTS_STATUS_2 = "In-progress",
    COMPLAINTS_STATUS_3 = "Resolved",
    COMPLAINTS_STATUS_4 = "Reopened",
}

export type ComplaintsListWithStatusV2 = {
    [status in ComplaintStatus]?: ComplaintViewV2[];
};

export interface SupportOptions {
    chat_access: boolean;
    call_access: boolean;
    ivr_popup: boolean;
}
