import { RazorpayPaymentMethodInfo } from "@types";

import { WalletExpiryDetails } from "./transaction.model";

export class Wallet {
    balance?: number;
    suprCreditsBalance?: number;
    expiry?: WalletExpiryDetails;
    meta?: WalletMeta;
}

export interface WalletMeta {
    rechargeCouponInfo?: RechargeCouponInfo;
    rechargeCouponOffers?: RechargeCouponOffer[];
    rechargeSuccessInfo?: RechargeSuccessInfo;
}

export interface RechargeSuccessInfo {
    amountAddedToWallet?: number;
    amountAddedToSuprCredits?: number;
}

export interface RechargeCoupon {
    amount?: number;
    couponCode?: string;
}

export interface RechargeCouponInfo {
    isApplied?: boolean;
    couponCode?: string;
    message?: string;
    successMessage?: string;
    benefits?: RechargeCouponBenefits;
    paymentDetails?: RechargeCouponPaymentDetails;
}

export interface RechargeCouponPaymentDetails {
    paymentOfferId: string;
    paymentPartner: string;
    instruments: RecahrgeCouponPaymentInstruments[];
}

export interface RecahrgeCouponPaymentInstruments {
    method: string;
    issuers?: string[];
    networks?: string[];
    types?: string[];
}

export interface RechargeCouponOffer {
    headline?: string;
    disclaimer?: string;
    rechargeCouponCode: string;
}

export interface RechargeCouponBenefits {
    cashbackBenefits?: Benefits[];
    discountBenefits?: Benefits[];
    suprCreditsBenefits?: Benefits[];
}

export interface Benefits {
    benefitValue?: number;
    expiryDatetime?: string;
}

export interface PaymentInfo {
    paymentId: string;
    amount: number;
    couponCode?: string;
    buySuprCredits?: boolean;
}

export interface PaymentMethodData {
    icon?: string;
    title: string;
    subtitle: string;
    selected?: boolean;
    methods?: PaymentMethods;
}

export interface PaymentMethodDesc {
    icon?: string;
    title: string;
    image?: string;
    subtitle?: string;
    tags?: Array<string>;
}

export interface PaymentMethodOption {
    id?: number;
    name?: string;
    method?: string;
    network?: string;
    selected?: boolean;
    preference?: number;
    gateway_partner: string;
    desc?: PaymentMethodDesc;
    payment_info: RazorpayPaymentMethodInfo;
}

export interface PaymentMethodDataV2 {
    method?: string;
    isEnabled: boolean;
    offers?: Array<any>;
    desc: PaymentMethodDesc;
    options: Array<PaymentMethodOption>;
    standardCheckout?: boolean;
}

export interface PaymentMethods {
    [type: string]: PaymentMethodData;
}

export interface PaymentMethodsV2 {
    [type: string]: PaymentMethodDataV2;
}

export interface AutoDebitPaymentModes {
    methods: AutoDebitPaymentMode[];
}

export interface AutoDebitPaymentMode {
    title: string;
    subtitle: string;
    tags: string[];
    icon: string;
    method: string;
    note?: string;
}
