import { Sku, SkuRewardTagType } from "./sku.model";
import { DeliveryTimelines } from "./timeline.model";
import { Image } from "./image.model";

export interface ScheduleItem {
    id: number;
    sku_id: number;
    status?: string;
    quantity: number;
    modified: boolean;
    delivery_type?: string;
    remaining_quantity: number;
    meta?: ScheduleItemMeta;
}

export interface ScheduleItemMeta {
    is_sample?: boolean;
    sku_mask_details?: SkuMaskDetails;
    tag: SkuRewardTagType;
}

export interface SkuMaskDetails {
    sku_name: string;
    image: Image;
}

export interface SlotInfo {
    [deliveryType: string]:
        | {
              otp?: string;
              title: string;
              subtitle?: string;
              delivery_proof?: string;
              delivery_timeslots: string;
          }
        | boolean;
    is_on_alternate_delivery?: boolean;
}

export interface SlotInfoMeta {
    otp?: string;
    title: string;
    subtitle?: string;
    delivery_proof?: string;
    delivery_timeslots: string;
}

export interface Schedule {
    date?: string;
    message?: string;
    slot_info?: SlotInfo;
    addons?: ScheduleItem[];
    subscriptions?: ScheduleItem[];
    delivery_timeline?: DeliveryTimelines;
}

export interface ScheduleApiData {
    schedule: Schedule[];
    skuData: Sku[];
    message: string;
}

export interface ScheduleDeliverySlotDict {
    [date: string]: ScheduleDeliverySlotMap;
}

export interface ScheduleDeliverySlotItem {
    addons?: ScheduleItem[];
    delivery_message?: string;
    subscriptions?: ScheduleItem[];
}

export interface ScheduleDeliverySlotMap {
    date?: string;
    message?: string;
    slot_info?: SlotInfo;
    delivery_timeline?: DeliveryTimelines;
    [deliveryType: string]: ScheduleDeliverySlotItem | string | SlotInfo;
}
