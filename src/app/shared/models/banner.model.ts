import { CopyBlock } from "./copy-block.model";

export interface Banner {
    title?: string;
    outerHeading?: BannerOuterHeading;
    bgColor?: string;
    content: BannerContent[];
    border?: BannerBorder;
    boxShadow?: string;
}

export interface BannerContent {
    imgUrl?: string;
    text: string;
    title?: string;
    textColor?: string;
    titleColor?: string;
    fontSize?: string;
    highlightedText?: string;
    highlightedTextColor?: string;
    link?: BannerLink;
    tag?: BannerTag;
    copyBlock?: CopyBlock;
    textWeight?: string;
    titleWeight?: string;
}

export interface BannerBorder {
    size?: string;
    color?: string;
    type?: string;
    radius?: string;
}

export interface BannerTag {
    bgColor?: string;
    text?: string;
    textColor?: string;
    fontSize?: string;
}

export interface BannerOuterHeading {
    text?: string;
    textColor?: string;
    fontSize?: string;
}

export interface BannerLink {
    text?: string;
    outsideUrl?: string;
    appUrl?: string;
    textColor?: string;
}

export interface ImageBanner {
    title?: string;
    bgColor?: string;
    content: ImageBannerContent[];
}

export interface ImageBannerContent {
    imgUrl: string;
    action?: ImageBannerAction;
    appUrl?: string;
    outsideUrl?: string;
}

export type ImageBannerAction =
    | "route"
    | "scroll"
    | "open-bottomsheet"
    | "no-action";
