import { ActivityBlock } from "@models";
import { Image } from "./image.model";
import { TextFragment } from "./text-fragment.model";

export interface MileStoneHome {
    state: MileStoneState;
    image?: Image;
    details?: MileStoneHomeDetail;
    cta?: MileStoneCta;
    user_milestone_id?: number;
    milestone_group_id?: number;
    milestone_id?: number;
    task_id?: number;
}

export enum MileStoneState {
    IN_PROGRESS = "IN_PROGRESS",
    START = "START",
    UNLOCKED = "UNLOCKED",
    EXPIRED = "EXPIRED",
    COMPLETED = "COMPLETED",
    LOCKED = "LOCKED",
}

export enum MileStoneRewardsListType {
    SUPRACCESS = "SUPRACCESS",
    COUPONCODE = "COUPONCODE",
    SUPRCREDIT = "SUPRCREDIT",
}

export enum MileStoneAmountType {
    AMOUNT = "amount",
    COUNT = "count",
}

export interface MileStoneHomeDetail {
    header_text?: TextFragment;
    image?: Image;
    title?: TextFragment;
    sub_title?: TextFragment;
    completion_percentage?: string;
    sub_title_bg_color?: string;
}

export interface MileStoneCta {
    text?: TextFragment;
    link: string;
    source: string;
}

export interface MileStoneRewards {
    users_rewards: UserMileStoneRewards;
    faq?: string[];
    user_milestone_details?: UserMileStoneDetails;
    milestones?: UserMileStone[];
    milestone_group_id?: number;
}

export interface MileStoneRewardsList {
    type?: MileStoneRewardsListType;
    title?: TextFragment;
    sub_title?: TextFragment;
}

export interface UserMileStoneDetails {
    state?: MileStoneState;
    title?: TextFragment;
    sub_title?: TextFragment;
}

export interface UserMileStone {
    current_milestone?: boolean;
    state?: MileStoneState;
    milestone_rewards?: MileStoneRewardsUserCard;
    milestone_tasks?: UserMileStoneTasks;
    rules?: string[];
    user_milestone_id?: number;
    milestone_id?: number;
}

export interface UserMileStoneRewards {
    state: MileStoneState;
    title?: TextFragment;
    sub_title?: TextFragment;
    rewards_earned?: MileStoneRewardsList[];
    center_image?: Image;
    image: Image;
    milestone_complete_state_details: {
        title?: TextFragment;
        sub_title?: TextFragment;
    };
}

export interface MileStoneRewardsUserCard {
    title?: TextFragment;
    sub_title?: TextFragment;
    rewards_list?: MileStoneRewardsList[];
    others_rewards_info?: {
        type?: string;
        text?: TextFragment;
    };
    sub_title_bg_color?: string;
}

export interface UserMileStoneTasks {
    task_header_text?: TextFragment;
    total?: string;
    completed?: string;
    state_details?: {
        title?: TextFragment;
        sub_title?: TextFragment;
    };
    tasks?: UserMileStoneTaskList[];
}

export interface UserMileStoneTaskList {
    image?: Image;
    title?: TextFragment;
    sub_title?: TextFragment;
    state?: MileStoneState;
    cta?: MileStoneCta;
    progress?: UserMileStoneTaskProgress;
    id?: number;
}

export interface UserMileStoneTaskProgress {
    text?: TextFragment;
    percentage?: string;
    type?: MileStoneAmountType;
}

export interface MilestonePastRewards {
    header_card?: {
        title?: TextFragment;
        body?: {
            title?: TextFragment;
            subTitle?: TextFragment;
            bottomText?: TextFragment;
        };
    };
    reward_lists?: ActivityBlock[];
}
