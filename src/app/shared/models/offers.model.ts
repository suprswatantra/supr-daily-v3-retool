import { TextFragment } from "./text-fragment.model";

export interface OfferDetails {
    offerName: string;
    displayInfo: {
        title: TextFragment;
        description: TextFragment;
        bannerImg: string;
    };
    tnc: Array<string>;
    redeemSteps: Array<string>;
    faq: Array<{ question: string; answer: string }>;
}

export interface OfferSectionInfo {
    title: string;
    subtitle: string;
    icon: string;
}

export interface OfferListItem {
    offer_id: string;
    offer_name: string;
    offer_context: string;
    offer_type: string;
    is_success: Boolean;
    failure_reason: string;
    title: string;
    description: string;
    preview_img_url: string;
    tap_to_apply_flag: boolean;
}

export type OffersList = Array<{
    section_info: OfferSectionInfo;
    offers: Array<OfferListItem>;
}>;
