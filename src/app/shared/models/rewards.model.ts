import { BenefitsBanner } from "./benefits-banner.model";
import { FaqItem } from "./faq-item.model";
import { ActivityBlock } from "./activity-block.model";
import { SuprCreditsExpiry } from "./transaction.model";
import { TextFragment } from "./text-fragment.model";

export interface RewardsInfo {
    header: RewardsInfoHeader;
    benefits?: BenefitsBanner;
    faqs?: FaqItem[];
    pastRewards?: ActivityBlock[];
    scratchCards?: ScratchCard[];
    footer: RewardsInfoFooter;
}
export interface RewardsInfoHeader {
    bgColor?: string;
    logo?: string;
    title: TextFragment;
    subTitle?: TextFragment;
    infoText?: TextFragment;
    warningText?: TextFragment;
}
export interface ScratchCard {
    id: number;
    imgUrl: string;
    earnReason?: string;

    isExpired?: boolean;
    isLocked?: boolean;
    expiryDate?: string;
    earnDate?: string;
    unlockDate?: string;
}

export interface ScratchedCardDetails {
    id: number;
    benefitInfo?: TextFragment;
    isExpired?: boolean;
    isError?: boolean;
}

export interface RewardsInfoFooter {
    scExpiry?: SuprCreditsExpiry;
    scAvailable?: number;
    leftSectionDefaultText?: string;
    buttonUpperFoldDefaultText?: string;
    buttonLowerFoldDefaultText?: string;
    redirectionUrl: string;
}

export interface ScratchedCardDict {
    [id: number]: ScratchedCardDetails;
}
