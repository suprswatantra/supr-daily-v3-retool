import { GenericNudgeSetting } from "@types";

export interface Nudge {
    type: string;
    name?: string;
    maxCount: number;
    isGeneric?: boolean;
    nudgeCoolOffTime?: number;
    nudgeDisplayPages?: string[];
    context?: GenericNudgeSetting.Nudge;
}
