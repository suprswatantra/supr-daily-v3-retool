export interface TransactionSkuItem {
    id: number;
    unit_name: string;
    sub_unit_name: string;
    sub_unit_conversion: number;
}
export interface TransactionSku {
    id: number;
    sku_name: string;
    item: TransactionSkuItem;
}

export interface RechargeTransaction {
    id: number;
    paid_amount: number;
    obj_type: string;
    gateway_id: string;
    created: string;
    type: string;
    type_details?: string;
    payment_service: string;
    notes?: string;
    customer: number;
    updated_balance?: number;
    supr_credits_type?: string;
    benefit_uuid?: string;
    balance?: Balance;
}
export interface PaymentTransaction {
    id: number;
    paid_amount: number;
    discount?: number;
    payable_amount: number;
    obj_type: string; // TransactionObjectType;
    payment_type?: string; // PaymentTransactionPaymentType;
    created: string;
    details?: string;
    type: string; // RechargeTransactionType | PaymentTransactionType;
    coupon_code?: string;
    sku_id: number;
    quantity: number;
    unit_price: number;
    sku: TransactionSku;
    updated_balance?: number;
    supr_credits_type?: string;
    payment_details?: PaymentDetail;
    benefit_uuid?: string;
    balance?: Balance;
}

export interface Balance {
    wallet: number;
    supr_credits: number;
}

export interface PaymentDetail {
    supr_credits: number;
    wallet: number;
}

export interface TransactionDetails {
    txn_list: Transaction[];
    expiry?: WalletExpiryDetails;
}

export interface WalletExpiryDetails {
    supr_credits: SuprCreditsExpiry;
}

export interface SuprCreditsExpiry {
    amount: number;
    date: string;
}

export type Transaction = RechargeTransaction | PaymentTransaction;

export type TransactionObjectType = "recharge" | "payment";

export type RechargeTransactionType =
    | "Online"
    | "Paytm"
    | "Cash"
    | "Card"
    | "Credit Card"
    | "Debit Card"
    | "Netbanking"
    | "Wallet"
    | "Emi"
    | "Upi"
    | "Cheque"
    | "Coupon"
    | "Testing"
    | "Promo"
    | "Referral"
    | "Transfer"
    | "Other";

export type PaymentTransactionType = "Subscription recharge" | "Addon";

export type PaymentTransactionPaymentType = "Paid" | "Refund";
