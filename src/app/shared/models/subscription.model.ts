import { Sku } from "./sku.model";

export class Subscription {
    id: number;
    consumed: number;
    balance: number;
    quantity: number;
    deliveries_remaining: number;
    frequency: {
        mon?: boolean;
        tue?: boolean;
        wed?: boolean;
        thu?: boolean;
        fri?: boolean;
        sat?: boolean;
        sun?: boolean;
    };
    sku_id?: number;
    sku_data: Sku;
    delivery_dates: {
        start: string;
        next: string;
        last: string;
    };
    balance_amount: number;
}

export interface SubscriptionSplit {
    active: Subscription[];
    expired: Subscription[];
    ended?: Subscription[];
    rechargePending?: Subscription[];
}

export interface SubscriptionInfoPriorityChip {
    type: "ended" | "pending" | "active";
    count: number;
    message: string;
    iconName: string;
}

export interface SubscriptionTransaction {
    id: number;
    qty: number;
    type: string;
    created: string;
    status: string;
    details: string;
    balance: {
        amount: number;
        consumed: number;
    };
}
