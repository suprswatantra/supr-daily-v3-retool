export interface Address {
    id: number;
    type: string;
    floorNumber?: string;
    societyName?: string;
    buildingName?: string;
    doorNumber?: string;
    streetAddress: string;
    location?: Location;
    premiseId?: number;
    city?: City;
    hub?: Hub;
    instructions?: string;
    societyId?: number;
    buildingId?: number;
    doorId?: number;
    from?: string;
}

export interface City {
    id: number;
    name: string;
}

export interface Hub {
    deliveryFee: number;
    id: number;
    name: string;
    cutOffTime: string;
}

export interface Location {
    latLong: {
        latitude: number;
        longitude: number;
    };
    reverseGeocode?: any;
}

export interface Building {
    id: number;
    name: string;
    doors?: Door[];
}

export interface Door {
    id: number;
    floorNumber: number;
    name: string;
}
