export interface DeliveryTimelineItem {
    otp?: string;
    title: string;
    status: string;
    subtitle: string;
    achieved: boolean;
    delivery_proof?: string;
}

export interface DeliveryTimelineSlot {
    global_status: string;
    timeline: Array<DeliveryTimelineItem>;
}

export interface DeliveryTimelines {
    [slot: string]: DeliveryTimelineSlot;
}

export interface DeliveryProofFeedbackTag {
    key: string;
    value: string;
    /* custom attribute to handle states */
    selected?: boolean;
}

export interface DeliveryProofFeedbackNote {
    title: string;
    subtitle?: string;
}

export interface DeliveryProofFeedbackAnswer {
    key: string;
    icon: string;
    value?: string;
    sa_object_name?: string;
    sa_object_value?: string;
    followup_question?: string;
    tags?: Array<DeliveryProofFeedbackTag>;
}

export interface DeliveryProof {
    image_url?: string;
    image_type?: string;
    primary_question?: string;
    feedback_note?: DeliveryProofFeedbackNote;
    primary_answer?: DeliveryProofFeedbackAnswer;
    possible_answers?: Array<DeliveryProofFeedbackAnswer>;
}
