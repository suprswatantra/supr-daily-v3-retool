export namespace Feedback {
    export interface deliveryFeedbackRequest {
        feedback_request_id: number;
        customer_id: number;
        platform?: string;
        feedback_entity_id: string;
        feedback_entity_type?: string;
        feedback_type?: string;
        params?: PrimaryParam[];
        status?: string;
        feedback_date: string;
        created?: string;
        nudge_required?: boolean;
    }

    export interface PrimaryParam {
        value: number;
        statements: string[];
    }

    export interface OrderFeedback {
        feedback_date: string;
        feedback_info_type: string;
        feedback_entity_id: string;
        feedback_entity_type: string;
        collect_feedback_request?: OrderFeedbackObject;
        feedback_response?: OrderFeedbackObject;
    }

    export interface OrderFeedbackObject {
        id: number;
        feedback_request_id: number;
        customer_id: number;
        feedback_entity_id: string;
        feedback_entity_type: string;
        feedback_type: string;
        feedback_value: number;
        params?: PrimaryParam[];
        channel: string;
        generated_timestamp?: string;
        response_timestamp?: string;
        impression_timestamp?: string;
        comments?: string;
    }

    export interface ParamsObject {
        [data: string]: boolean;
    }

    export interface FeedbackRequestObject {
        [date: string]: OrderFeedback;
    }

    export interface FeedbackSubmit {
        feedback_request_id: number;
        feedback_value: number;
        params?: PrimaryParam[];
        comments?: string;
        channel: string;
    }

    export interface FeedbackResponse {
        primaryResponse: string;
        positiveParamsObject: ParamsObject;
        negativeParamsObject: ParamsObject;
        feedback_request_id: number;
        comment: string;
        feedbackOrderResponse?: FeedbackWrapperResponse;
    }

    export interface FeedbackOrderResponse {
        primaryResponse: string;
        positiveParamsObject: ParamsObject;
        negativeParamsObject: ParamsObject;
    }

    export interface FeedbackWrapperResponse {
        feedbackOrderResponse: FeedbackOrderResponse;
        goToSchedulePage?: boolean;
    }
}
