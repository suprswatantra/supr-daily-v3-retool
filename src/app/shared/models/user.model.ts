import { Address } from "@models";

export interface User {
    id?: number;
    firstName?: string;
    lastName?: string;
    email?: string;
    mobileNumber?: string;
    referralCode?: string;
    referralBenefit?: UserReferralBenefit;
    bellRing?: boolean;
    isWhatsAppOptIn?: boolean;
    address?: Address;
    tPlusOneEnabled?: boolean;
    walletVersion?: string;
    suprCreditsEnabled?: boolean;
    checkoutInfo?: CheckoutInfo;
    suprPassEligible?: boolean;
    suprPassDetails?: ProfileLevelSuprPassDetails;
    referralEligible?: boolean;
    referralDetails?: ReferralDetails;
    rewardsEligible?: boolean;
    rewardsDetails?: RewardsDetails;
    alternateDeliveryEligible?: boolean;
}

export interface ReferralDetails {
    showReferralCodeBlock?: boolean;
}

export interface RewardsDetails {
    scratchCardsEarned?: number;
    onboardingImgUrl?: string;
}

export interface UserReferralBenefit {
    imgUrl?: string;
    title?: string;
    description?: string;
    promoCode?: string;
}

export interface CheckoutInfo {
    checkoutDisabled: boolean;
    content?: CheckoutInfoContent;
}

export interface CheckoutInfoContent {
    imageUrl?: string;
    title?: string;
    description?: string;
}

export interface ProfileLevelSuprPassDetails {
    endDate: string;
    daysLeft: number;
    isAutoDebitActive: boolean;
}
