import { ConfigurableIcon } from "@types";

import { TextFragment } from "./text-fragment.model";

export interface ActivityBlock {
    tag?: ActivityBlockTag;
    detailsText?: TextFragment;
    date?: string;
    totalDays?: number;
    benefitType?: BenefitType;
    totalBenefit?: number;
    benefitIcon?: ConfigurableIcon;
    footerText?: TextFragment;
}

export type BenefitType = "suprCredits" | "rupee";

export interface ActivityBlockTag {
    bgColor?: string;
    text?: string;
    textColor?: string;
    fontSize?: string;
}
