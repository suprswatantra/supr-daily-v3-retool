import {
    SEARCH_RESULT_TYPES,
    SEARCH_INSTRUMENTATION_EVENT_TYPE,
    SEARCH_INSTRUMENTATION_EVENT_NAME,
} from "app/constants/search.constants";

export interface SearchSku {
    sku_id: number;
    object_id: string;
    category_id: number;
    sub_category_id: number;
    search_position: number;
    sub_category_name: string;
}

export interface SearchCategoryMeta {
    id: number;
    numberOfHits: number;
}

export interface SearchMetaData {
    page: number;
    queryId: string;
    hitsPerPage: number;
    numberOfHits: number;
    numberOfPages: number;
    category: SearchCategoryMeta[];
    matchType: {
        type: SEARCH_RESULT_TYPES;
    };
}

export interface SkuSearchResponse {
    items: SearchSku[];
    meta_data: SearchMetaData;
}

export interface SearchInstrumentation {
    queryID: string;
    userToken?: string;
    search_objectIDs: Array<string>;
    search_positions: Array<number>;
    eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE;
    search_eventName: SEARCH_INSTRUMENTATION_EVENT_NAME;
}

export interface PopularSkuSearch {
    query: string;
}
