import { Banner } from "./banner.model";
import { TextFragment } from "./text-fragment.model";
import { ConfigurableIcon } from "@types";

export interface BenefitsBanner {
    banner?: Banner;
    header?: BenefitsBannerHeader;
    bgColor?: string;
    content?: BenefitsBannerContent[];
}

export interface BenefitsBannerHeader {
    title?: TextFragment;
    subTitle?: TextFragment;
}

export interface BenefitsBannerContent {
    header?: BenefitsBannerContentHeader;
    imgUrl?: string;
    upperFoldText?: TextFragment;
    middleFoldText?: TextFragment;
    middleFoldIcon?: ConfigurableIcon;
    lowerFoldText?: TextFragment;
    cta?: TextFragment;
    ctaAction?: BenefitsBannerCtaAction;
    tag?: BenefitsBannerContentTag;
    tagPosition?: BenefitsBannerContentTagPosition;
    isDefault?: boolean;
    showInBlockAccess?: boolean;
}

export interface BenefitsBannerContentHeader {
    bgColor?: string;
    text?: TextFragment;
}

export interface BenefitsBannerContentTag {
    bgColor?: string;
    text?: string;
    textColor?: string;
    fontSize?: string;
}

export interface BenefitsBannerCtaAction {
    action?: BenefitsBannerAction;
    appUrl?: string;
    outsideUrl?: string;
}

export type BenefitsBannerAction =
    | "route"
    | "scroll"
    | "open-bottomsheet"
    | "no-action"
    | "social-share";

export type BenefitsBannerContentTagPosition = "left" | "right";
