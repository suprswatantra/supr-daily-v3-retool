import { Banner, ImageBanner } from "./banner.model";
import { TextFragment } from "./text-fragment.model";
import { BenefitsBanner } from "./benefits-banner.model";
import { ActivityBlock } from "./activity-block.model";
import { FaqItem } from "./faq-item.model";

export interface ReferralInfo {
    referralCouponBlock: ReferralCouponBlock;
    header: ReferralInfoHeader;
    benefits?: BenefitsBanner;
    banner?: Banner;
    alertBanner?: Banner;
    imgBanner?: ImageBanner;
    faqs?: FaqItem[];
    commonBenefits?: BenefitsBanner;
    rewards?: ReferralRewards;
    activeReferralCode?: string;
    numberOfSuccessfulReferrals?: number;
    shareBlock: ReferralShareBlock;
    successfulReferralMessage?: SuccessfulReferralMessage;
    eligibleToRefer: boolean;
    referralRewardInfoText?: string;
    referralDisabledInfoText?: string;
}

export interface SuccessfulReferralMessage {
    imgUrl?: string;
    title?: string;
    description?: string;
}

export interface ReferralShareBlock {
    subject?: string;
    message?: string;
    url?: string;
    file?: string | string[];
}

export interface ReferralInfoHeader {
    bgColor?: string;
    logo?: string;
    title: TextFragment;
    subTitle?: TextFragment;
    infoText?: TextFragment;
    warningText?: TextFragment;
}

export interface ReferralCouponBlock {
    showCouponBlock?: boolean;
    benefitText?: string;
    referralCodeAppliedBanner?: Banner;
    applied?: boolean;
    referralId?: number;
    errorMessage?: string;
}

export interface ReferralRewards {
    upcoming?: ActivityBlock[];
    past?: ActivityBlock[];
}

export interface ContactCard {
    name?: string;
    phone: string;
    id?: string;
}

export interface ReferralMessage {
    message?: string;
    subject?: string;
    files?: ArrayLike<string>;
    url?: string;
    chooserTitle?: string;
    appPackageName?: string;
}
