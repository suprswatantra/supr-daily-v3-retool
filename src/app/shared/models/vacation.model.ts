export class Vacation {
    start_date?: string;
    end_date?: string;
    message?: string;
}

export class SystemVacation extends Vacation {}

export interface SystemVacationMessage {
    description: string;
}

export class VacationApiRes {
    vacation: Vacation;
    system_vacation: SystemVacation;
    system_vacation_message?: SystemVacationMessage;
}
