import { Banner, BannerContent, ImageBanner } from "./banner.model";
import { TextFragment } from "./text-fragment.model";
import { BenefitsBanner } from "./benefits-banner.model";
import { ActivityBlock } from "./activity-block.model";
import { FaqItem } from "./faq-item.model";

export interface SuprPassInfo {
    isActive: boolean;
    isExpiringSoon?: boolean;
    skuId: number;
    plans?: SuprPassPlan[];
    activePlanDetails: SuprPassActivePlanDetails;
    expiredSuprPassData?: ExpiredSuprPassData;
    faqs?: FaqItem[];
    header: SuprPassInfoHeader;
    banner?: Banner;
    alertBanner?: Banner;
    planSelectionText?: TextFragment;
    benefits?: BenefitsBanner;
    imgBanner?: ImageBanner;
    autoDebitData?: AutoDebitData;
    activity?: ActivityBlock[];
    autoDebitAlert?: ImageBanner;
    experimentInfo?: SuprPassExperimentInfo;
    savingsCardInfo?: {
        homepage?: SuprPassCard;
        sidebar?: SuprPassCard;
    };
}

export interface SuprPassCard {
    textBanners?: TextBanner[];
    rightHeadingText?: string;
    ctaInfo?: SuprPassCardCta;
    bgColor?: string;
}

export interface SuprPassCardCta {
    ctaText: string;
    routeUrl: string;
}

export interface TextBanner {
    upperText: string;
    middleText: string;
    lowerText: string;
}

export interface SuprPassExperimentInfo {
    name: string;
    benefits?: BenefitsBanner;
    banner?: Banner;
    cutOffTime?: SuprPassExperimentCutOffTime;
    imageUrl?: string;
    headerText?: TextFragment;
    calendarText?: string;
}

export interface SuprPassExperimentCutOffTime {
    value: string;
}

export interface AutoDebitData {
    texts?: AutoDebitTexts;
    isActive: boolean;
    // Old design of active audo debit banner
    activeDataBlock?: ActiveAutoDebitInfo;
    // Used in old versions
    selectionDataBlock?: {
        title: string;
        description: string;
        offers: AutoDebitOffer[];
    };
    // Associated with Supr pass plan id
    autoDebitPlans?: AutoDebitPlan[];
}

export interface AutoDebitPlan {
    suprPassPlanId: number;
    offerMrp: number;
    offerBanner?: Banner;
    footerTexts: AutoDebitPlanFooterTexts;
}

export interface AutoDebitPlanFooterTexts {
    setup: string;
    skip: string;
}

export interface AutoDebitTexts {
    heading: string;
    subHeading: string;
    infoText: string;
    bulletTexts: string[];
}

export interface ActiveAutoDebitInfo {
    content: BannerContent;
}

export interface AutoDebitOffer {
    offerText: string;
    offerAppliedText: string;
    offerMrp: number;
    planId: number;
}

export interface SuprPassInfoHeader {
    bgColor?: string;
    logo?: string;
    title: TextFragment;
    subTitle?: TextFragment;
    infoText?: TextFragment;
    warningText?: TextFragment;
}

export interface ExpiredSuprPassData {
    endDate: string;
    purchaseId?: number;
    plan?: SuprPassPlan;
}

export interface SuprPassActivePlanDetails {
    endDate: string;
    daysLeft: number;
    purchaseId?: number;
    plan: SuprPassPlan;
}

export interface SuprPassPlan {
    id: number;
    durationMonths: number;
    durationDays: number;
    unitPrice: number;
    unitMrp: number;
    discountText?: string;
    isRecommended?: boolean;
}

export interface AutoCheckoutQueryParams {
    // from url so string
    amount?: string;
    auto?: string;
    rechargeCoupon?: string;
    // from Pass layout
    fromSuprPassPage?: boolean;
}

export interface SuprPassCardDetails {
    isActive?: boolean;
    isExpiringSoon?: boolean;
    isExpired?: boolean;
    subscriptionId?: number;
    activeSuprPassData?: SuprPassActivePlanDetails;
    expiredSuprPassData?: ExpiredSuprPassData;
    firstBenefitText?: SuprPassCardBenefitText;
    secondBenefitText?: SuprPassCardBenefitText;
    basicPlanText?: string;
    basicPlan?: SuprPassPlan;
}

export interface SuprPassCardBenefitText {
    upperFoldText?: string;
    middleFoldText?: string;
    lowerFoldText?: string;
}

export interface SuprPassCardViewTexts {
    firstBlock: SuprPassCardViewTextBlock;
    secondBlock: SuprPassCardViewTextBlock;
    ctaText: string;
    basicPlanText?: string;
}

export interface SuprPassCardViewTextBlock {
    firstLineText: string;
    boldText: string | number;
    supportText: string;
}

export interface SuprPassSavingsInfo {
    totalSavings: number;
    monthlySavings: number;
    savingsList: Array<ActivityBlock>;
}
