export interface Experiments {
    configs: ExperimentConfigs;
    activeExperiments: string[];
}

export interface ExperimentConfigs {
    [key: string]: any;
}
