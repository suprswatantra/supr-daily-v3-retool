import { Building } from "@shared/models";

export class Society {
    id: number;
    name: string;
    locationHelpText: string;
    buildings: Building[];
}
