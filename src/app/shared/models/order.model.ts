import { SlotInfo, SkuMaskDetails } from "./schedule.model";
import { DeliveryTimelines } from "./timeline.model";
import { SkuRewardTagType } from "./sku.model";

export interface Order {
    id: number;
    type: string;
    sku_id: number;
    status?: string;
    meta?: OrderMeta;
    addon_id?: number;
    statusNew?: string;
    delivery_type?: string;
    rescheduled_date: string;
    subscription_id?: number;
    delivered_quantity: number;
    scheduled_quantity: number;
    statusContext?: OrderStatusContext;
}

export interface OrderMeta {
    is_sample?: boolean;
    sku_mask_details?: SkuMaskDetails;
    tag: SkuRewardTagType;
}

export interface OrderStatusContext {
    refundType?: string;
    refundAmount?: number;
    statusTimestamp?: string;
}

export interface DeliverySummary {
    status?: string;
    message?: string;
    slot_info?: SlotInfo;
    statusContext?: OrderStatusContext;
}

export interface OrderHistory {
    date: string;
    orders: Order[];
    delivery_summary?: DeliverySummary;
    delivery_timeline?: DeliveryTimelines;
}

export interface OrderDeliverySlotMap {
    date: string;
    slot_info?: SlotInfo;
    delivery_summary?: DeliverySummary;
    delivery_timeline?: DeliveryTimelines;
    [deliveryType: string]: Order[] | DeliverySummary | string | SlotInfo;
    REGULAR_DELIVERY?: Order[];
}

export interface OrderDeliverySlotDict {
    [date: string]: OrderDeliverySlotMap;
}
