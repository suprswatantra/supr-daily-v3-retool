import { Image } from "./image.model";
import { Sku } from "./sku.model";
import { Segment } from "@types";

export interface V3Layout {
    widgetType: string;
    data: V3LayoutData;
}

export interface V3LayoutData {
    type?: string;
    title?: V3LayoutText;
    items?: V3LayoutItem[];
    cta?: V3LayoutCta;
    bottomCta?: V3LayoutCta;
    subtitle?: V3LayoutText;
    style?: V3LayoutStyle;
    colCount?: number;
    rowCount?: number;
    image?: V3LayoutImage;
    viewId?: number;
    analytics?: {
        [name: string]: V3LayoutAnalytics;
    };
    maxAllowed?: number;
    minAllowed?: number;
    tnc?: V3LayoutItemDataTnc;
}

export interface V3LayoutText {
    text: string;
    type?: string;
    textType?: string;
    style?: V3LayoutStyle;
}

export interface V3LayoutCta {
    text?: string;
    textType?: string;
    icon?: string;
    style?: V3LayoutStyle;
    events?: V3LayoutEvent;
    analytics?: {
        [name: string]: V3LayoutAnalytics;
    };
}

export interface V3LayoutStyle {
    color?: string;
    padding?: string;
    "text-decoration"?: string;
    "background-color"?: string;
    "border-radius"?: string;
    height?: string;
    width?: string;
    "min-width"?: string;
    "max-width"?: string;
    "min-height"?: string;
    "max-height"?: string;
    "margin-top"?: string;
    "margin-bottom"?: string;
    "margin-left"?: string;
    "margin-right"?: string;
    "box-shadow"?: string;
    border?: string;
}

export interface V3LayoutEvent {
    click?: V3LayoutEventClick;
}

export interface V3LayoutEventClick {
    trigger?: string;
    navigation?: V3LayoutEventClickNavigation;
    meta?: V3LayoutEventClickMeta;
}

export interface V3LayoutEventClickNavigation {
    link?: string;
    source?: string;
}

export interface V3LayoutImage {
    data?: Image;
    style?: V3LayoutStyle;
    dimension?: {
        width: number;
        height: number;
    };
}

export interface V3LayoutEventClickMeta {
    preffered_mode?: string;
    type?: string;
    itemId?: number;
    isPreview?: boolean;
}

export interface V3LayoutAnalytics {
    objectName?: string;
    objectValue?: string;
    objectTag?: string;
    context?: string;
    contextList?: Segment.ContextListItem[];
}

export interface V3LayoutItem {
    itemType: string;
    data: V3LayoutItemData;
}

export interface V3LayoutItemData {
    type?: string;
    preferredMode?: string;
    sku?: Sku;
    entityId?: number;
    title?: V3LayoutText;
    subtitle1?: V3LayoutText;
    subtitle2?: V3LayoutText;
    subtitle3?: V3LayoutText;
    image?: V3LayoutImage;
    cta?: V3LayoutCta;
    style?: V3LayoutStyle;
    analytics?: {
        [name: string]: V3LayoutAnalytics;
    };
    parent_sku_id?: number;
}

export interface V3LayoutItemDataTnc {
    image?: V3LayoutImage;
    style?: V3LayoutStyle;
    title?: V3LayoutText;
    subtitle?: V3LayoutText;
    cta?: V3LayoutCta;
}

export interface V3Component {
    data: any;
}

export enum HomeLayoutPositions {
    HOME_SECTION_1 = "home_section_1",
    HOME_SECTION_2 = "home_section_2",
    HOME_SECTION_3 = "home_section_3",
    HOME_SECTION_4 = "home_section_4",
    CART = "cart",
}

export type HomeLayoutListWithPosition = {
    [position in HomeLayoutPositions]: V3Layout[];
};
