import { Sku, SlotInfo } from "@models";
import { SkuMaskDetails } from "./schedule.model";
import { SkuRewardTagType } from "./sku.model";

export interface OrderSummaryItem {
    sku_id: number;
    id: number;
    subscription_id?: number;
    status: string;
    type: string;
    delivered_quantity: number;
    scheduled_quantity: number;
    message?: string;
    statusContext?: any;
    delivery_type: string;
    meta?: ItemMeta;
}

export interface ItemMeta {
    is_sample?: boolean;
    sku_mask_details?: SkuMaskDetails;
    tag: SkuRewardTagType;
}

export interface ScheduleSubscription {
    remaining_quantity: number;
    sku_id: number;
    modified: boolean;
    quantity: number;
    id: number;
    delivery_type: string;
    meta?: ItemMeta;
}

export interface OrderSummary {
    date: string;
    status: string;
    message: string;
    statusContext: any;
    slot_info?: SlotInfo;
    orders: OrderSummaryItem[];
}

export interface ScheduleSummary {
    date: string;
    status: string;
    statusContext: any;
    slot_info?: SlotInfo;
    addons: ScheduleSubscription[];
    subscriptions: ScheduleSubscription[];
    message: { title: string; subtitle?: string };
}

export interface DeliveryStatus {
    orderSummary?: OrderSummary;
    scheduleSummary?: ScheduleSummary;
    skuData?: Sku[];
}

export interface DeliveryStatusProductInfo {
    quantity: number;
    sku: Sku;
    sku_mask_details?: SkuMaskDetails;
}

export interface DeliveryStatusHeaderContent {
    title: string;
    subTitle: string;
    icon: string;
}

export interface DeliveryStatusNewHeaderContent {
    title: string;
    subTitle: string;
    dateToHighlight: string;
}

export interface DelivereyStatusCurrentWeek {
    day: string;
    date: number;
    dateObject: Date;
    formattedDate: string; // 2020-01-06 yyyy-MM-dd
}
