import { Pipe, PipeTransform } from "@angular/core";
import { CurrencyService } from "@services/util/currency.service";

@Pipe({
    name: "rupee",
})
export class RupeePipe implements PipeTransform {
    constructor(private currencyService: CurrencyService) {}

    transform(value: number | string, decimalPlaces?: number): string {
        return this.currencyService.toText(value, decimalPlaces);
    }
}
