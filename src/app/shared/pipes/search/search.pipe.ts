import { Pipe, PipeTransform } from "@angular/core";
export const NUMBER_TYPE = "number";
export const STRING_TYPE = "string";

@Pipe({
    name: "SearchFilter",
})
export class SearchPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        if (!value) {
            return null;
        }

        if (!args) {
            return value;
        }

        if (!(typeof args === STRING_TYPE || typeof args === NUMBER_TYPE)) {
            return value;
        }

        args = args.toString();
        args = args.toLowerCase();

        return value.filter(function (item) {
            return JSON.stringify(item).toLowerCase().includes(args);
        });
    }
}
