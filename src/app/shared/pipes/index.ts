import { RupeePipe } from "./supr-rupee/supr-rupee.pipe";
import { TextTruncatePipe } from "./supr-text-truncate/supr-text-truncate.pipe";
import { HighlightPipe } from "./supr-text-highlight/supr-text-highlight.pipe";
import { SearchPipe } from "./search/search.pipe";

export const sharedPipes = [
    RupeePipe,
    TextTruncatePipe,
    HighlightPipe,
    SearchPipe,
];
