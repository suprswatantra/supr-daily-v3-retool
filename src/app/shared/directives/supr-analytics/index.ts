import { AnalyticsClickDirective } from "./supr-analytics-click.directive";
import { AnalyticsImpressionDirective } from "./supr-analytics-impression.directive";
import { AnalyticsModalViewDirective } from "./supr-analytics-modal-view.directive";

import { StopPropagationDirective } from "./supr-stop-propogation.directive";

export const analyticsDirectives = [
    AnalyticsClickDirective,
    AnalyticsImpressionDirective,
    AnalyticsModalViewDirective,
    StopPropagationDirective,
];
