import {
    AfterViewInit,
    Directive,
    ElementRef,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import {
    DEFAULT_SEGMENT_TRAITS,
    IMPRESSION_VISIBILITY_RATIO,
    SETTINGS,
} from "@constants";

import { SegmentService } from "@services/integration/segment.service";
import { RudderStackService } from "@services/integration/rudder-stack.service";
import { AnalyticsService } from "@services/integration/analytics.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import { Segment } from "@types";

let _io: IntersectionObserver;

const canUseIntersectionObserver = () =>
    window && "IntersectionObserver" in window;

const canCaptureImpression = (entry: IntersectionObserverEntry): boolean =>
    entry &&
    entry.isIntersecting &&
    entry.target &&
    entry.target["_instance"] &&
    entry.target["_instance"].captureImpression;

const handleIntersect = (entries: IntersectionObserverEntry[]) => {
    entries.forEach((entry) => {
        if (canCaptureImpression(entry)) {
            entry.target["_instance"].captureImpression();
        }
    });
};

if (canUseIntersectionObserver()) {
    _io = new IntersectionObserver(handleIntersect, {
        root: null,
        rootMargin: "0px",
        threshold: IMPRESSION_VISIBILITY_RATIO,
    });
}

@Directive({
    selector: "[saImpression]",
})
export class AnalyticsImpressionDirective implements AfterViewInit {
    @Input() saObjectName = DEFAULT_SEGMENT_TRAITS.OBJECT_NAME;
    @Input() saObjectValue = DEFAULT_SEGMENT_TRAITS.OBJECT_VALUE;
    @Input() saContext = DEFAULT_SEGMENT_TRAITS.CONTEXT;
    @Input() saObjectTag = DEFAULT_SEGMENT_TRAITS.OBJECT_TAG;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() saPosition: number;
    @Input() saImpressionEnabled = true;

    @Output() handleSaImpressionCb: EventEmitter<void> = new EventEmitter();

    constructor(
        private element: ElementRef,
        private segmentService: SegmentService,
        private rudderStackService: RudderStackService,
        private utilService: UtilService,
        private settingsService: SettingsService,
        private analyticsService: AnalyticsService
    ) {}

    ngAfterViewInit() {
        if (canUseIntersectionObserver() && this.saImpressionEnabled) {
            this.observe();
        }
    }

    captureImpression() {
        this.captureSegmentImpression();
        this.captureRudderstackImpression();

        if (this.handleSaImpressionCb) {
            this.handleSaImpressionCb.emit();
        }
    }

    /**
     * capture Segment Impression
     * @private
     */
    private captureSegmentImpression() {
        const traits = this.getImpressionTraits();
        const impressionKey = this.getImpressionKey(traits);

        const segmentSessionThrottlingEnabled = this.settingsService.getSettingsValue(
            SETTINGS.SEGMENT_SESSION_THROTLLING_ENABLED
        );

        if (
            !this.segmentService.isKeyPresentInFiredImpressionSet(
                impressionKey
            ) &&
            segmentSessionThrottlingEnabled
        ) {
            // Segment Data
            this.sendDataToSegment(traits);
            this.sendDataToSegment(null, impressionKey);
        } else {
            if (
                this.segmentService.isImpressionKeyPresentInExcludedEvents(
                    impressionKey
                )
            ) {
                this.sendDataToSegment(traits);
            }
        }
    }

    /**
     * capture Rudderstack Impression
     * @private
     */
    private captureRudderstackImpression() {
        const traits = this.getImpressionTraits();
        const impressionKey = this.getImpressionKey(traits);

        const settingKeysValue = this.settingsService.getSettingsValue(
            "rudderStackSettingKeys"
        );

        const isSessionThrottlingEnable = this.utilService.getValue(
            settingKeysValue.impressionThrottleConfig,
            "sessionThottling"
        );

        if (!isSessionThrottlingEnable) {
            this.sendDataToRudderstack(traits);
            this.sendDataToRudderstack(null, impressionKey);
        } else {
            if (
                !this.rudderStackService.isKeyPresentInFiredImpressionSet(
                    impressionKey
                )
            ) {
                this.sendDataToRudderstack(traits);
                this.sendDataToRudderstack(null, impressionKey);
            } else {
                if (
                    this.rudderStackService.isImpressionKeyPresentInExcludedEvents(
                        impressionKey
                    )
                ) {
                    this.sendDataToRudderstack(traits);
                }
            }
        }
    }

    /**
     * Send Data to Segment Service
     *
     * @private
     * @param {Traits} Object
     * @param {ImpressionKey} string
     */
    private sendDataToSegment(traits?: Object, impressionKey?: string) {
        const isTraisEmpty = this.utilService.isEmpty(traits);
        const isImpressionKeyEmpty = this.utilService.isEmpty(impressionKey);

        if (!isTraisEmpty) {
            this.analyticsService.trackSegmentImpression(traits);
        }

        if (!isImpressionKeyEmpty) {
            this.segmentService.addToFiredImpressionSet(impressionKey);
        }
    }

    /**
     * Send Data to Rudderstack Service
     *
     * @private
     * @param {Traits} Object
     * @param {ImpressionKey} string
     */
    private sendDataToRudderstack(traits?: Object, impressionKey?: string) {
        const isTraisEmpty = this.utilService.isEmpty(traits);
        const isImpressionKeyEmpty = this.utilService.isEmpty(impressionKey);

        if (!isTraisEmpty) {
            this.analyticsService.trackRudderstackImpression(traits);
        }

        if (!isImpressionKeyEmpty) {
            this.rudderStackService.addToFiredImpressionSet(impressionKey);
        }
    }

    private getImpressionKey(traits: Object): string {
        let key;
        const keyGenerationType = this.segmentService.getImpressionThrottleKeyGenerationMethod();

        if (keyGenerationType === "stringify") {
            key = JSON.stringify(traits);
        } else {
            key =
                this.saObjectName +
                this.saObjectValue +
                this.saContext +
                this.saPosition;
        }

        return key;
    }

    private getImpressionTraits(): Segment.Traits {
        return {
            objectName: this.saObjectName,
            objectValue: this.saObjectValue,
            objectTag: this.saObjectTag,
            context: this.saContext,
            position: this.saPosition,
            ...this.getContextListItems(),
        };
    }

    private getContextListItems() {
        if (!this.saContextList || !this.saContextList.length) {
            return [];
        }

        const obj = {};
        this.saContextList.forEach((item, index) => {
            obj[`context_${index + 1}_name`] = item.name;
            obj[`context_${index + 1}_value`] = item.value;
        });

        return obj;
    }

    private observe() {
        if (_io && this.element) {
            this.element.nativeElement._instance = this;
            _io.observe(this.element.nativeElement);
        }
    }
}
