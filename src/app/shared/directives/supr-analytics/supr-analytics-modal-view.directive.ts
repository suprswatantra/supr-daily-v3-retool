import { AfterViewInit, Directive, Input, OnDestroy } from "@angular/core";

import { DEFAULT_SEGMENT_TRAITS, SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { AnalyticsService } from "@services/integration/analytics.service";
import { Segment } from "@types";

@Directive({
    selector: "[saModalView]",
})
export class AnalyticsModalViewDirective implements AfterViewInit, OnDestroy {
    @Input() saModalName: string;
    @Input() saObjectName = DEFAULT_SEGMENT_TRAITS.OBJECT_NAME;
    @Input() saObjectValue = DEFAULT_SEGMENT_TRAITS.OBJECT_VALUE;
    @Input() saContext = DEFAULT_SEGMENT_TRAITS.CONTEXT;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() saPosition: number;

    constructor(private analyticsService: AnalyticsService) {}

    ngAfterViewInit() {
        if (this.saModalName) {
            this.captureModalView();
        }
    }

    ngOnDestroy() {
        if (this.saModalName) {
            this.captureModalClose();
        }
    }

    private captureModalClose() {
        const traits = this.getModalViewTraits();
        if (SA_IMPRESSIONS_IS_ENABLED.ANALYTICS_MODAL_VIEW_DIRECTIVE_CLOSE) {
            this.analyticsService.trackModalClose(traits);
        }
    }

    private captureModalView() {
        const traits = this.getModalViewTraits();
        this.analyticsService.trackModalView(traits);
    }

    private getContextListItems() {
        if (!this.saContextList || !this.saContextList.length) {
            return [];
        }

        const obj = {};
        this.saContextList.forEach((item, index) => {
            obj[`context_${index + 1}_name`] = item.name;
            obj[`context_${index + 1}_value`] = item.value;
        });

        return obj;
    }

    private getModalViewTraits(): Segment.Traits {
        return {
            modalName: this.saModalName,
            objectName: this.saObjectName,
            objectValue: this.saObjectValue,
            context: this.saContext,
            position: this.saPosition,
            ...this.getContextListItems(),
        };
    }
}
