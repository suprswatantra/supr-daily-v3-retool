import { Directive, HostListener, Input } from "@angular/core";

import { DEFAULT_SEGMENT_TRAITS } from "@constants";

import { AnalyticsService } from "@services/integration/analytics.service";

import { Segment } from "@types";

@Directive({
    // tslint:disable-next-line: directive-selector
    selector: "[saClick]",
})
export class AnalyticsClickDirective {
    @Input() saObjectName = DEFAULT_SEGMENT_TRAITS.OBJECT_NAME;
    @Input() saObjectValue = DEFAULT_SEGMENT_TRAITS.OBJECT_VALUE;
    @Input() saContext = DEFAULT_SEGMENT_TRAITS.CONTEXT;
    @Input() saObjectTag = DEFAULT_SEGMENT_TRAITS.OBJECT_TAG;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() saPosition: number;
    @Input() saClickedEnabled = true;

    constructor(private analyticsService: AnalyticsService) {}

    private captureClick() {
        const traits = this.getClickTraits();
        this.analyticsService.trackClick(traits);
        // this.segmentService.trackMoEngageEvent(
        //     MOENGAGE_EVENTS.CLICK_EVENTS,
        //     traits
        // );
    }

    private getClickTraits(): Segment.Traits {
        return {
            objectName: this.saObjectName,
            objectValue: this.saObjectValue,
            objectTag: this.saObjectTag,
            context: this.saContext,
            position: this.saPosition,
            ...this.getContextListItems(),
        };
    }

    private getContextListItems() {
        if (!this.saContextList || !this.saContextList.length) {
            return [];
        }

        const obj = {};
        this.saContextList.forEach((item, index) => {
            obj[`context_${index + 1}_name`] = item.name;
            obj[`context_${index + 1}_value`] = item.value;
        });

        return obj;
    }

    @HostListener("click", ["$event"])
    onClick() {
        if (this.saClickedEnabled) {
            this.captureClick();
        }
    }
}
