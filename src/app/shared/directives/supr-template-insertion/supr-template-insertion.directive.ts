import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
    selector: "[suprTemplateInsertion]",
})
export class TemplateInsertionDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
