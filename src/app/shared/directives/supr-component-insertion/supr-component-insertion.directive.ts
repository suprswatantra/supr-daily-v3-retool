import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
    selector: "[saAddComponent]",
})
export class AddComponentDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
