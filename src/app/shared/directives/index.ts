import { SuprImageDirective } from "./supr-image-directive";
import { analyticsDirectives } from "./supr-analytics/";
import { TemplateInsertionDirective } from "./supr-template-insertion/supr-template-insertion.directive";
import { AddComponentDirective } from "./supr-component-insertion/supr-component-insertion.directive";

export const sharedDirectives = [
    ...analyticsDirectives,
    TemplateInsertionDirective,
    SuprImageDirective,
    AddComponentDirective,
];
