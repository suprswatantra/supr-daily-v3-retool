import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { MobxAngularModule } from "mobx-angular";

import { IonicModule } from "@ionic/angular";
import { GoogleMapsAPIWrapper, AgmCoreModule } from "@agm/core";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { Clipboard } from "@ionic-enterprise/clipboard/ngx";
import { Deeplinks } from "@ionic-enterprise/deeplinks/ngx";
import { SocialSharing } from "@ionic-enterprise/social-sharing/ngx";

import { providers } from "app/services";
import { sharedComponents } from "./components";
import { sharedPipes } from "./pipes";
import { sharedDirectives } from "./directives";
import { sharedAdapters } from "./adapters";
import { environment } from "@environments/environment.prod";

@NgModule({
    declarations: [...sharedComponents, ...sharedPipes, ...sharedDirectives],
    entryComponents: [...sharedComponents],
    imports: [
        CommonModule,
        IonicModule,
        AgmCoreModule.forRoot({
            apiKey: environment.mapKey.android,
        }),
        MobxAngularModule,
    ],
    exports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ...sharedComponents,
        ...sharedPipes,
        ...sharedDirectives,
    ],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ...providers,
                ...sharedAdapters,
                GoogleMapsAPIWrapper,
                CallNumber,
                Clipboard,
                Deeplinks,
                SocialSharing,
            ],
        };
    }
}
