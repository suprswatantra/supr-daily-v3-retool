import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Segment } from "@types";

@Component({
    selector: "supr-chip",
    template: `
        <div
            class="chip ion-activatable"
            (click)="onClick($event)"
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
            [saContext]="saContext"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
        >
            <ion-ripple-effect></ion-ripple-effect>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-chip.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipComponent {
    @Input() saObjectName: string;
    @Input() saObjectValue: any;
    @Input() saContext: string;
    @Input() saPosition: number;
    @Input() saContextList: Segment.ContextListItem[] = [];

    @Output() handleClick: EventEmitter<TouchEvent> = new EventEmitter();

    onClick(event: TouchEvent) {
        event.stopPropagation();

        if (this.handleClick) {
            setTimeout(() => this.handleClick.emit(event), 100);
        }
    }
}
