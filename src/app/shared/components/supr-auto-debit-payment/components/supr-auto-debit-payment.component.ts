import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    OnInit,
    EventEmitter,
    NgZone,
    ChangeDetectorRef,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";
import { Subscription as RxSubscription } from "rxjs";

import {
    AutoDebitPaymentMode,
    User,
    SuprPassPlan,
    AutoDebitTexts,
    AutoDebitPlan,
} from "@models";
import { SuprApi } from "@types";

import { PaymentService } from "@services/integration/payment.service";
import { UtilService } from "@services/util/util.service";

import { WalletCurrentState } from "@store/wallet/wallet.state";

@Component({
    selector: "supr-auto-debit-payment",
    template: `
        <ng-container
            *ngIf="
                startPaymentProcess &&
                autoDebitPaymentModes &&
                autoDebitPaymentModes.length
            "
        >
            <!-- payment mode selection -->
            <supr-auto-debit-payment-modes-modal
                [texts]="texts"
                [selectedAutoDebitPlan]="selectedAutoDebitPlan"
                [showModal]="showPaymentModesModal"
                [autoDebitPaymentModes]="autoDebitPaymentModes"
                (handleHideModal)="hidePaymentModesModal()"
                (handlePaymentModeSelection)="
                    handlePaymentModeSelection($event)
                "
                (proceedToCart)="proceedToCart.emit()"
            ></supr-auto-debit-payment-modes-modal>

            <!-- loader -->
            <supr-loader-overlay
                *ngIf="processingAutoDebit || fetchingPaymentModes"
            >
            </supr-loader-overlay>

            <!--Auto debit success screen -->
            <ng-container *ngIf="showAutoDebitPaymentSuccessScreen">
                <supr-pass-auto-debit-payment-success
                    [showSuccessScreen]="showAutoDebitPaymentSuccessScreen"
                    [recurringPaymentRes]="autoDebitPaymentRes"
                    (handleClose)="hideAutoDebitSuccessScreen()"
                    (handleSkip)="handleSkipPaymentFailure()"
                    (handleAutoDebitPaymentRetry)="
                        handleAutoDebitPaymentRetry()
                    "
                    (autoDismissCallback)="autoDebitSuccessScreenCallback()"
                ></supr-pass-auto-debit-payment-success>
            </ng-container>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-auto-debit-payment.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprAutoDebitPayment implements OnInit, OnChanges {
    @Input() autoDebitPaymentModes: AutoDebitPaymentMode[];
    @Input() startPaymentProcess: boolean;
    @Input() texts: AutoDebitTexts;
    @Input() user: User;
    @Input() walletState: WalletCurrentState;
    @Input() walletError: any;
    @Input() selectedAutoDebitPlan: AutoDebitPlan;
    @Input() selectedSuprPassPlan: SuprPassPlan;
    @Input() isActiveSuprPassMember: boolean;

    @Output() handlePaymentSuccess: EventEmitter<void> = new EventEmitter();
    @Output() handlePaymentFailure: EventEmitter<void> = new EventEmitter();
    @Output() cancelPaymentProcess: EventEmitter<void> = new EventEmitter();
    @Output() fetchAutoDebitPaymentModes: EventEmitter<
        void
    > = new EventEmitter();
    @Output() proceedToCart: EventEmitter<void> = new EventEmitter();

    showPaymentModesModal = true;
    processingAutoDebit = false;
    fetchingPaymentModes = false;
    showAutoDebitPaymentSuccessScreen = false;
    autoDebitPaymentRes: SuprApi.RecurringPaymentRes;
    backdropDismiss = false;

    private autoDebitVerificationSubscription: RxSubscription;
    private selectedMode: AutoDebitPaymentMode;

    constructor(
        private paymentService: PaymentService,
        private utilService: UtilService,
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone
    ) {}

    ngOnInit() {
        if (!this.autoDebitPaymentModes) {
            this.fetchAutoDebitPaymentModes.emit();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleWalletStateChange(changes["walletState"]);
    }

    hidePaymentModesModal() {
        this.showPaymentModesModal = false;
        this.cancelPaymentProcess.emit();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    handlePaymentModeSelection(selectedMode: AutoDebitPaymentMode) {
        this.selectedMode = selectedMode;
        this.processAutoDebit(selectedMode);
    }

    autoDebitSuccessScreenCallback() {
        this.hideAutoDebitSuccessScreen();
        this.handlePaymentSuccess.emit();
    }

    handleSkipPaymentFailure() {
        this.hideAutoDebitSuccessScreen();
        this.handlePaymentFailure.emit();
    }

    hideAutoDebitSuccessScreen() {
        this.showAutoDebitPaymentSuccessScreen = false;
    }

    handleAutoDebitPaymentRetry() {
        this.hideAutoDebitSuccessScreen();
        if (this.selectedMode) {
            this.processAutoDebit(this.selectedMode);
            return;
        }
        this.handlePaymentFailure.emit();
    }

    private processAutoDebit(selectedMode: AutoDebitPaymentMode) {
        this.showProcessingAutoDebit();
        const data: any = {
            amount: this.getAutoDebitAmount(selectedMode.method),
            user: this.user,
            method: selectedMode.method,
        };

        try {
            this.paymentService.handleRecurringPayment(
                data,
                this.onRazorpaySuccess,
                this.autoProcessingRazorpayFailure
            );
        } catch (_error) {
            this.hideProcessingAutoDebit();
        }
    }

    private getAutoDebitAmount(method: string) {
        return method === "emandate" ? 0 : 100;
    }

    private showProcessingAutoDebit = () => {
        this.processingAutoDebit = true;
    };

    private hideProcessingAutoDebit = () => {
        this.processingAutoDebit = false;
    };

    private onRazorpaySuccess = (orderId: string, paymentId: string) => {
        this.ngZone.run(() => {
            this.verifySuprPassRecurringPayment(orderId, paymentId);
        });
    };

    private verifySuprPassRecurringPayment(orderId: string, paymentId: string) {
        const {
            id: plan_id,
            unitPrice,
            durationDays,
        } = this.selectedSuprPassPlan;
        const reqObj = {
            razorpay_order_id: orderId,
            razorpay_payment_id: paymentId,
            refund_payment: true,
            create_supr_pass: !this.isActiveSuprPassMember,
            supr_pass_amount: this.utilService.getNestedValue(
                this.selectedAutoDebitPlan,
                "offerMrp",
                unitPrice
            ),
            validity_in_days: durationDays,
            plan_id,
        };

        this.autoDebitVerificationSubscription = this.paymentService
            .verifySuprPassRecurringPayment(reqObj)
            .subscribe(
                (res: SuprApi.RecurringPaymentRes) => {
                    //TODO: Ask pranay for what to do here
                    // NOTE: For now treating it as failure
                    // if (
                    //     res.auto_debit_status ===
                    //     AUTODEBIT_TOKEN_STATUS.INITIATED
                    // ) {
                    //     alert("initiated");
                    // } else {
                    this.handleAutoDebitPaymentSuccess(res);
                    // }
                    this.unsubscribeAutoDebitVerification();
                },
                (_err) => {
                    this.hideProcessingAutoDebit();
                    this.unsubscribeAutoDebitVerification();
                }
            );
    }

    private handleAutoDebitPaymentSuccess(res: SuprApi.RecurringPaymentRes) {
        this.showAutoDebitPaymentSuccessScreen = true;
        this.autoDebitPaymentRes = res;
        this.hideProcessingAutoDebit();
        this.cdr.detectChanges();
    }

    private unsubscribeAutoDebitVerification() {
        this.autoDebitVerificationSubscription.unsubscribe();
    }

    private autoProcessingRazorpayFailure = () => {
        this.hideProcessingAutoDebit();
        this.handlePaymentFailure.emit();
    };

    private handleWalletStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.currentValue === WalletCurrentState.FETCHING_AUTO_DEBIT_MODES
        ) {
            this.fetchingPaymentModes = true;
            return;
        }

        if (
            change.previousValue ===
                WalletCurrentState.FETCHING_AUTO_DEBIT_MODES &&
            change.currentValue === WalletCurrentState.NO_ACTION
        ) {
            this.fetchingPaymentModes = false;
            if (this.walletError) {
                this.cancelPaymentProcess.emit();
            }
        }
    }
}
