import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { AutoDebitPaymentMode, AutoDebitTexts, AutoDebitPlan } from "@models";

import { ANALYTICS_OBJECT_NAMES } from "../constants";
import { ModalService } from "@services/layout/modal.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-auto-debit-payment-modes-modal",
    template: `
        <ng-container
            *ngIf="
                showModal &&
                autoDebitPaymentModes &&
                autoDebitPaymentModes.length
            "
        >
            <supr-modal
                [stackingAllowed]="true"
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.AUTO_DEBIT_PAYMENT_MODES}"
                [saContext]="selectedAutoDebitPlan?.suprPassPlanId"
            >
                <div class="wrapper">
                    <div class="header">
                        <ng-container *ngIf="texts?.heading">
                            <supr-text type="subtitle" class="title">
                                {{ texts?.heading }}
                            </supr-text>
                        </ng-container>

                        <ng-container *ngIf="texts?.subHeading">
                            <div class="divider12"></div>
                            <supr-text type="paragraph" class="subtitle">
                                {{ texts?.subHeading }}
                            </supr-text>
                        </ng-container>

                        <ng-container *ngIf="texts?.infoText">
                            <div class="divider16"></div>
                            <supr-text type="body" class="info">
                                {{ texts?.infoText }}
                            </supr-text>
                        </ng-container>

                        <ng-container
                            *ngIf="
                                texts?.bulletTexts && texts?.bulletTexts.length
                            "
                        >
                            <div class="divider4"></div>
                            <ng-container
                                *ngFor="let bulletText of texts?.bulletTexts"
                            >
                                <div class="divider4"></div>
                                <div class="suprRow top">
                                    <supr-icon name="tick"></supr-icon>
                                    <div class="spacer4"></div>
                                    <supr-text type="regular14">
                                        {{ bulletText }}
                                    </supr-text>
                                </div>
                            </ng-container>
                        </ng-container>

                        <ng-container
                            *ngIf="selectedAutoDebitPlan?.offerBanner"
                        >
                            <div class="divider16"></div>
                            <supr-banner
                                [banner]="selectedAutoDebitPlan?.offerBanner"
                            ></supr-banner>
                        </ng-container>
                    </div>
                    <div class="divider16"></div>
                    <div
                        class="methodWrapper"
                        *ngFor="
                            let paymentMode of autoDebitPaymentModes;
                            trackBy: trackByFn;
                            first as _first;
                            last as _last
                        "
                    >
                        <div *ngIf="_first" class="separator"></div>
                        <supr-section-header
                            chevronRight="true"
                            subTitleType="paragraph"
                            [title]="paymentMode?.title"
                            [icon]="paymentMode?.icon"
                            [subtitle]="paymentMode?.subtitle"
                            [footerText]="paymentMode?.note"
                            (click)="
                                handlePaymentModeSelection.emit(paymentMode)
                            "
                            saClick
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .SELECT_MODE_CLICK}"
                            [saObjectValue]="paymentMode?.method"
                        >
                        </supr-section-header>
                        <div *ngIf="!_last" class="separator"></div>
                    </div>
                    <div class="divider16"></div>
                    <div class="actionButtons">
                        <ng-container
                            *ngIf="selectedAutoDebitPlan?.footerTexts?.skip"
                        >
                            <div class="buttonWrapper">
                                <supr-button
                                    class="remove"
                                    (handleClick)="handleSkipBtnClick()"
                                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                        .PROEED_TO_CART}"
                                    [saContext]="
                                        selectedAutoDebitPlan?.suprPassPlanId
                                    "
                                >
                                    <supr-text type="body">
                                        {{
                                            selectedAutoDebitPlan?.footerTexts
                                                ?.skip
                                        }}
                                    </supr-text>
                                </supr-button>
                            </div>
                        </ng-container>

                        <ng-container
                            *ngIf="selectedAutoDebitPlan?.footerTexts?.setup"
                        >
                            <div class="divider16"></div>
                            <div class="buttonWrapper">
                                <supr-button
                                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                        .SELECT_MODE_CLICK}"
                                    [saObjectValue]="paymentMode?.method"
                                    [saContext]="
                                        selectedAutoDebitPlan?.suprPassPlanId
                                    "
                                    (handleClick)="
                                        handlePaymentModeSelection.emit(
                                            selectedPaymentMode
                                        )
                                    "
                                >
                                    <supr-text type="body">
                                        {{
                                            selectedAutoDebitPlan?.footerTexts
                                                ?.setup
                                        }}
                                    </supr-text>
                                </supr-button>
                            </div>
                        </ng-container>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-auto-debit-payment.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprAutoDebitPaymentModesModal implements OnInit {
    @Input() autoDebitPaymentModes: AutoDebitPaymentMode[];
    @Input() selectedAutoDebitPlan: AutoDebitPlan;
    @Input() showModal: boolean;
    @Input() texts: AutoDebitTexts;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();
    @Output() proceedToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePaymentModeSelection: EventEmitter<
        AutoDebitPaymentMode
    > = new EventEmitter();

    selectedPaymentMode: AutoDebitPaymentMode;

    constructor(
        private modalService: ModalService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.selectDefaultPaymentMode();
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    handleSkipBtnClick() {
        this.modalService.closeModal();
        this.proceedToCart.emit();
    }

    selectDefaultPaymentMode() {
        if (!this.utilService.isLengthyArray(this.autoDebitPaymentModes)) {
            this.selectedPaymentMode = null;
            return;
        }

        this.selectedPaymentMode = this.autoDebitPaymentModes[0];
    }
}
