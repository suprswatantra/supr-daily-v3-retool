export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        SELECT_MODE_CLICK: "setup-auto-debit",
        PROEED_TO_CART: "skip-and-proceed-to-cart",
    },
};
