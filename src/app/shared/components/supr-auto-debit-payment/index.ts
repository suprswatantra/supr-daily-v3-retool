import { SuprAutoDebitPaymentContainer } from "./containers/supr-auto-debit-payment.container";
import { SuprAutoDebitPaymentModesModal } from "./components/payment-modes-modal.component";
import { SuprAutoDebitPayment } from "./components/supr-auto-debit-payment.component";

export const autoDebitComponents = [
    SuprAutoDebitPayment,
    SuprAutoDebitPaymentModesModal,
    SuprAutoDebitPaymentContainer,
];
