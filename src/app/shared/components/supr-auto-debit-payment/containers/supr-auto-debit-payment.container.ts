import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";
import { Observable } from "rxjs";

import {
    AutoDebitPaymentMode,
    User,
    SuprPassPlan,
    AutoDebitTexts,
    AutoDebitPlan,
} from "@models";

import { WalletAdapter } from "@shared/adapters/wallet.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";

import { WalletCurrentState } from "@store/wallet/wallet.state";

@Component({
    selector: "supr-auto-debit-payment-container",
    template: `
        <supr-auto-debit-payment
            [texts]="texts"
            [startPaymentProcess]="startPaymentProcess"
            [autoDebitPaymentModes]="autoDebitPaymentModes$ | async"
            [user]="user$ | async"
            [selectedSuprPassPlan]="selectedSuprPassPlan"
            [selectedAutoDebitPlan]="selectedAutoDebitPlan"
            [isActiveSuprPassMember]="isActiveSuprPassMember"
            (cancelPaymentProcess)="cancelPaymentProcess.emit()"
            (handlePaymentSuccess)="handlePaymentSuccess.emit()"
            (handlePaymentFailure)="handlePaymentFailure.emit()"
            (fetchAutoDebitPaymentModes)="fetchAutoDebitPaymentModes()"
            (proceedToCart)="proceedToCart.emit()"
        ></supr-auto-debit-payment>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprAutoDebitPaymentContainer implements OnInit {
    @Input() startPaymentProcess: boolean;
    @Input() texts: AutoDebitTexts;
    @Input() selectedAutoDebitPlan: AutoDebitPlan;
    @Input() selectedSuprPassPlan: SuprPassPlan;
    @Input() isActiveSuprPassMember: boolean;

    @Output() handlePaymentSuccess: EventEmitter<void> = new EventEmitter();
    @Output() handlePaymentFailure: EventEmitter<void> = new EventEmitter();
    @Output() cancelPaymentProcess: EventEmitter<void> = new EventEmitter();
    @Output() proceedToCart: EventEmitter<void> = new EventEmitter();

    autoDebitPaymentModes$: Observable<AutoDebitPaymentMode[]>;
    user$: Observable<User>;
    walletState$: Observable<WalletCurrentState>;
    walletError$: Observable<any>;

    constructor(
        private userAdapter: UserAdapter,
        private walletAdapter: WalletAdapter
    ) {}

    ngOnInit() {
        this.autoDebitPaymentModes$ = this.walletAdapter.autoDebitPaymentModes$;
        this.user$ = this.userAdapter.user$;
        this.walletState$ = this.walletAdapter.walletState$;
        this.walletError$ = this.walletAdapter.walletError$;
    }

    fetchAutoDebitPaymentModes() {
        this.walletAdapter.fetchAutoDebitPaymentModes();
    }
}
