export const STATIC_MAP = {
    MARKER_COLOR: "red",
    URL: "https://maps.googleapis.com/maps/api/staticmap",
    ZOOM: 18,
    PADDING: 24,
    RELATIVE_DIVIDE_VALUE: 2.25,
    MAP_TYPE: "roadmap",
};

export const RESIDENCE_TYPE = {
    individualHouse: "Independent House",
    knownSociety: "Apartment / Gated Society",
    unknownSociety: "Apartment / Gated Society",
};

export const TEXTS = {
    DELEIVER_TO: "Delivery address",
};
