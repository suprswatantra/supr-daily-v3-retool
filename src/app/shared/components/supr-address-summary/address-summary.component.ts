import { Component, Input, OnInit } from "@angular/core";

import { environment } from "@environments/environment";
import { Address } from "@models";

import { UtilService } from "@services/util/util.service";
import { AddressService } from "@services/shared/address.service";

import { STATIC_MAP, RESIDENCE_TYPE, TEXTS } from "./constants";

@Component({
    selector: "supr-address-summary",
    template: `
        <div class="suprRow content">
            <div class="mapImgWrapper">
                <ion-skeleton-text
                    animated
                    class="mapShimmer"
                    *ngIf="!isImageLoaded"
                ></ion-skeleton-text>
                <img
                    src="{{ url }}"
                    [class.visible]="isImageLoaded"
                    (load)="onImageLoad()"
                />
            </div>

            <div class="spacer24"></div>
            <div class="suprColumn left texts">
                <supr-text type="body">
                    {{ TEXTS.DELEIVER_TO }}
                </supr-text>

                <div class="divider8"></div>

                <supr-text type="body" class="address">
                    {{ addressText }}
                </supr-text>

                <div class="divider8"></div>

                <supr-text type="regular14">
                    {{ address?.instructions }}
                </supr-text>

                <div class="divider16"></div>

                <supr-text type="paragraph" class="type">
                    {{ RESIDENCE_TYPE[address?.type] }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./address-summary.component.scss"],
})
export class AddressSummaryComponent implements OnInit {
    @Input() address: Address;

    constructor(
        private addressService: AddressService,
        private utilService: UtilService
    ) {}

    STATIC_MAP = STATIC_MAP;
    RESIDENCE_TYPE = RESIDENCE_TYPE;
    TEXTS = TEXTS;
    mapKey = environment.mapKey.android;
    isImageLoaded = false;
    lat: number;
    lng: number;
    url: string;
    addressText: string;

    ngOnInit() {
        if (this.address) {
            this.addressText = this.addressService.getAddressSummary(
                this.address
            );

            if (this.address.location) {
                this.lat = this.address.location.latLong.latitude;
                this.lng = this.address.location.latLong.longitude;
            }
        }
        this.setUrl();
    }

    onImageLoad() {
        this.isImageLoaded = true;
    }

    private setUrl() {
        // const width = Math.floor(
        //     this.platformService.getDeviceWidth() - this.STATIC_MAP.PADDING
        // );

        // const height = Math.floor(
        //     width / this.STATIC_MAP.RELATIVE_DIVIDE_VALUE
        // );

        const obj = {
            zoom: this.STATIC_MAP.ZOOM,
            size: `192x192`,
            maptype: this.STATIC_MAP.MAP_TYPE,
            markers: `${this.lat},${this.lng}`,
            key: this.mapKey,
        };

        const params = this.utilService.getQueryParams(obj);

        this.url = `${this.STATIC_MAP.URL}?${params}`;
    }
}
