import { Component, Input, ChangeDetectionStrategy } from "@angular/core";
import { MenuController } from "@ionic/angular";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-side-menu-opener",
    template: `
        <supr-icon
            name="menu"
            (click)="openMenu($event)"
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.HOME_SIDE_MENU}"
        ></supr-icon>
    `,
    styleUrls: ["./supr-sidemenu-opener.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideMenuOpenerComponent {
    @Input() name: string;

    constructor(private menu: MenuController) {}

    openMenu($event) {
        $event.stopPropagation();

        if (this.name) {
            this.menu.enable(true, this.name);
            this.menu.open(this.name);
        }
    }
}
