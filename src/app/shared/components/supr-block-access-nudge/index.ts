import { BlockAccessNudgeComponent } from "./supr-block-access-nudge.component";
import { WrapperComponent } from "./components/wrapper.component";

export const blockAccessNudgeComponents = [
    BlockAccessNudgeComponent,
    WrapperComponent,
];
