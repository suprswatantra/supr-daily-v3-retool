import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";

import { SuprPassExperimentInfo } from "@shared/models";

@Component({
    selector: "supr-block-access-nudge",
    template: `
        <supr-block-access-nudge-wrapper
            [showModal]="showModal$ | async"
            [expInfo]="expInfo$ | async"
            [showSkipBtn]="showSkipBtn$ | async"
            (handleHideModal)="hideBlockAccessNudge()"
        ></supr-block-access-nudge-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlockAccessNudgeComponent implements OnInit {
    showModal$: Observable<boolean>;
    expInfo$: Observable<SuprPassExperimentInfo>;
    showSkipBtn$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.showModal$ = this.adapter.showblockAccessNudge$;
        this.expInfo$ = this.adapter.suprPassExperimentInfo$;
        this.showSkipBtn$ = this.adapter.blockAccessNudgeShowSkipBtn$;
    }

    hideBlockAccessNudge() {
        this.adapter.hideBlockAcessNudge();
    }
}
