import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { SuprPassExperimentInfo } from "@shared/models";

import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";

import { GET_SUPR_ACCESS, SKIN_AND_ORDER } from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-block-access-nudge-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.BLOCK_ACCESS_NUDGE}"
                [saContext]="expInfo?.name"
            >
                <div class="wrapper">
                    <ng-container *ngIf="expInfo?.headerText?.text">
                        <supr-text-fragment
                            [textFragment]="expInfo?.headerText"
                        ></supr-text-fragment>
                    </ng-container>

                    <ng-container *ngIf="expInfo?.imageUrl">
                        <div class="divider12"></div>
                        <div class="suprRow center">
                            <supr-image
                                [src]="expInfo?.imageUrl"
                                [useDirectUrl]="true"
                            ></supr-image>
                        </div>
                    </ng-container>

                    <ng-container *ngIf="expInfo?.benefits">
                        <div class="divider12"></div>
                        <supr-benefits-banner
                            [benefitsBanner]="expInfo?.benefits"
                            [hideHeader]="true"
                            [hideCta]="true"
                        ></supr-benefits-banner>
                    </ng-container>

                    <supr-sticky-wrapper>
                        <div class="footerWrapper">
                            <div class="suprRow">
                                <ng-container *ngIf="showSkipBtn">
                                    <supr-button
                                        (handleClick)="skipBtnClick()"
                                        saObjectName="${SA_OBJECT_NAMES.CLICK
                                            .SKIP_CLICK}"
                                        [saContext]="expInfo?.name"
                                        class="skipBtn"
                                    >
                                        <supr-text type="body">
                                            ${SKIN_AND_ORDER}
                                        </supr-text>
                                    </supr-button>
                                    <div class="spacer16"></div>
                                </ng-container>
                                <supr-button
                                    (handleClick)="ctaClick()"
                                    saObjectName="${SA_OBJECT_NAMES.CLICK
                                        .CTA_CLICK}"
                                    [saContext]="expInfo?.name"
                                >
                                    <supr-text type="body">
                                        ${GET_SUPR_ACCESS}
                                    </supr-text>
                                </supr-button>
                            </div>
                        </div>
                    </supr-sticky-wrapper>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-block-access-nudge.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() showModal: boolean;
    @Input() expInfo: SuprPassExperimentInfo;
    @Input() showSkipBtn: boolean;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();
    @Output() handleButtonClick: EventEmitter<void> = new EventEmitter();

    constructor(
        private modalService: ModalService,
        private routerService: RouterService
    ) {}

    /**
     * Closes current modal instance and routes to supr pass page
     *
     * @memberof WrapperComponent
     */
    ctaClick() {
        this.modalService.closeModal();
        this.routerService.goToSuprPassPage();
    }

    skipBtnClick() {
        this.modalService.closeModal();
    }
}
