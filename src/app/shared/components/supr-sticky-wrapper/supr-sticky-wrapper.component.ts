import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-sticky-wrapper",
    template: `
        <div class="sticky">
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-sticky-wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StickyWrapperComponent {}
