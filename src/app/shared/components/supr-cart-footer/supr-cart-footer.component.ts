import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CartMeta, FooterMetaLineItem } from "@models";
import { Segment } from "@types";

import { CheckoutInfo } from "@shared/models/user.model";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { CartService } from "@services/shared/cart.service";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

import { TEXTS } from "./constants";

@Component({
    selector: "supr-cart-footer",
    templateUrl: "./supr-cart-footer.component.html",
    styleUrls: ["./supr-cart-footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartFooterComponent {
    @Input() cartMeta: CartMeta;
    @Input() fromSuprPassPage: boolean;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() isEligibleForSuprPass: boolean;
    @Input() fromRechargeSubscriptionPage: boolean;

    @Input() set amountToAdd(amount: number) {
        this._amountToAdd = amount;
        this.setButtonInfo();
        this.setAnalyticsData();
    }
    @Input() set amountToPay(amount: number) {
        this._amountToPay = amount;
        this.setButtonInfo();
    }
    @Input() set suprCreditsUsed(amount: number) {
        this._suprCreditsUsed = amount;
        this.setButtonInfo();
    }

    @Output() handlePay: EventEmitter<void> = new EventEmitter();

    get amountToAdd(): number {
        return this._amountToAdd;
    }
    get amountToPay(): number {
        return this._amountToPay;
    }
    get suprCreditsUsed(): number {
        return this._suprCreditsUsed;
    }

    _buttonTitle: string;
    _buttonSubtitle: string;

    _saObjectName: string;
    _saObjectValue: number;
    _saContextList: Segment.ContextListItem[] = [];

    private _amountToAdd: number;
    private _amountToPay: number;
    private _suprCreditsUsed: number;

    constructor(
        private utilService: UtilService,
        private cartService: CartService,
        private routerService: RouterService
    ) {}

    handleButtonClick() {
        if (this.cartService.isCheckoutDisabled(this.userCheckoutInfo)) {
            return;
        }

        const queryParams = {
            amount: this.amountToAdd,
            auto: true,
            rechargeCoupon: "",
            cartPaymentCoupon: "",
        };

        if (this.amountToAdd > 0) {
            if (this.wasRechargeCouponUsed()) {
                queryParams.rechargeCoupon = this.utilService.getNestedValue(
                    this.cartMeta,
                    "discount_info.coupon_code",
                    ""
                );
            }

            if (this.wasPaymentCouponUsed()) {
                queryParams.cartPaymentCoupon = this.utilService.getNestedValue(
                    this.cartMeta,
                    "discount_info.coupon_code",
                    ""
                );
            }

            if (this.canShowSuprPassPrompt()) {
                this.routerService.goToSuprPassPage({ queryParams });
                return;
            }

            this.routerService.goToWalletPage({ queryParams });
        } else {
            if (this.canShowSuprPassPrompt()) {
                this.routerService.goToSuprPassPage({ queryParams });
                return;
            }

            this.handlePay.emit();
        }
    }

    private setButtonInfo() {
        const {
            _amountToAdd: amountToAdd,
            _amountToPay: amountToPay,
            _suprCreditsUsed: suprCreditsUsed,
        } = this;
        if (!isNaN(amountToAdd) && amountToAdd > 0) {
            this._buttonSubtitle = amountToAdd.toString();
            this._buttonTitle = TEXTS.ADD_TO_SUPR_WALLET;
        } else if (!isNaN(amountToPay) && amountToPay > 0) {
            this._buttonSubtitle = amountToPay.toString();
            this._buttonTitle = TEXTS.PAY_USING_SUPR_WALLET;
        } else if (!isNaN(suprCreditsUsed) && suprCreditsUsed > 0) {
            this._buttonSubtitle = suprCreditsUsed.toString();
            this._buttonTitle = TEXTS.PAY_USING_SUPR_CREDITS;
        }
    }

    private canShowSuprPassPrompt(): boolean {
        const showSuprPass = this.utilService.getNestedValue(
            this.cartMeta,
            "show_supr_pass_prompt",
            false
        );

        return (
            !this.fromSuprPassPage &&
            !this.fromRechargeSubscriptionPage &&
            this.isEligibleForSuprPass &&
            showSuprPass
        );
    }

    private wasRechargeCouponUsed(): boolean {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.is_recharge_coupon",
            false
        );
    }

    private wasPaymentCouponUsed(): boolean {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.is_payment_coupon",
            false
        );
    }

    private setAnalyticsData() {
        if (!isNaN(this._amountToAdd)) {
            this.setObjectName();
            this.setObjectValue();
            this.setContextList();
        }
    }

    private setObjectName() {
        if (this._amountToAdd > 0) {
            this._saObjectName = SA_OBJECT_NAMES.CLICK.ADD_TO_WALLET_V2;
        } else {
            this._saObjectName = SA_OBJECT_NAMES.CLICK.PAY_FROM_WALLET_V2;
        }
    }

    private setObjectValue() {
        this._saObjectValue = this._amountToAdd;
    }

    private setContextList() {
        const footerLineItems: Array<any> = this.utilService.getNestedValue(
            this.cartMeta,
            "footer.lineItems",
            []
        );
        const contextList = [];
        if (this.utilService.isLengthyArray(footerLineItems)) {
            footerLineItems.forEach(
                (lineItem: FooterMetaLineItem, idx: number) => {
                    if (this.utilService.isLengthyArray(lineItem.parts)) {
                        const text = lineItem.parts.reduce((accm, item) => {
                            const partText =
                                (item.text && item.text.text) || "";
                            return accm + partText + " ";
                        }, "");
                        contextList.push({
                            name: `footer_line_item_${idx + 1}`,
                            value: text,
                        });
                    }
                }
            );
        }
        if (this.utilService.isLengthyArray(contextList)) {
            this._saContextList = contextList;
        }
    }
}
