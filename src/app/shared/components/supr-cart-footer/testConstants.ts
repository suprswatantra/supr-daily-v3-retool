/**
 * Sample footer config objects with prebuild style configs for future usage
 */

export const footer_3line = {
    lineItems: [
        {
            multipleLines: false,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "Grand Total",
                        textType: "paragraph",
                        textColor: "#00000066",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
                {
                    prefix: {
                        iconName: "rupee",
                        iconSize: "10px",
                        iconColor: "#000000cc",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "700",
                        textType: "paragraph",
                        textColor: "#000000cc",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
            ],
        },
        {
            multipleLines: false,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "Review delivery dates",
                        textType: "paragraph",
                        textColor: "#00000066",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "error",
                        iconSize: "12px",
                        iconColor: "#00000033",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "scroll",
                        url: "items-list",
                    },
                },
            ],
        },
        {
            multipleLines: false,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "Supr Credits",
                        textType: "paragraph",
                        textColor: "#00000066",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "supr-credits-img",
                        iconSize: "14px",
                        iconColor: "",
                        iconType: "svg",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "40",
                        textType: "paragraph",
                        textColor: "#000000cc",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
            ],
        },
    ],
};

export const footer_onlyPrice = {
    lineItems: [
        {
            multipleLines: true,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "Grand Total",
                        textType: "paragraph",
                        textColor: "#00000066",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
                {
                    prefix: {
                        iconName: "rupee",
                        iconSize: "12px",
                        iconColor: "#000000cc",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "700",
                        textType: "action14",
                        textColor: "#000000cc",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
            ],
        },
    ],
};

export const footer_supr_access = {
    lineItems: [
        {
            multipleLines: true,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "You can save",
                        textType: "paragraph",
                        textColor: "#00000066",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "error",
                        iconSize: "12px",
                        iconColor: "#00000033",
                        iconType: "",
                        iconStyle: { "margin-top": "2px" },
                    },
                    action: {
                        type: "scroll",
                        url: "items-list",
                    },
                },
                {
                    prefix: {
                        iconName: "rupee",
                        iconSize: "10px",
                        iconColor: "#000000cc",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "700",
                        textType: "paragraph",
                        textColor: "#000000cc",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "",
                        url: "",
                    },
                },
            ],
        },
        {
            multipleLines: false,
            parts: [
                {
                    prefix: {
                        iconName: "",
                        iconSize: "",
                        iconColor: "",
                        iconType: "",
                        iconStyle: {},
                    },
                    text: {
                        text: "GET SUPR ACCESS",
                        textType: "paragraph",
                        textColor: "#50C1BA",
                        textStyle: {},
                    },
                    suffix: {
                        iconName: "chevron_right",
                        iconSize: "18px",
                        iconColor: "#50C1BA",
                        iconType: "",
                        iconStyle: {},
                    },
                    action: {
                        type: "inAppNavigation",
                        url: "supr-pass",
                    },
                },
            ],
        },
    ],
};
