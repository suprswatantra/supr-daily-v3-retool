import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { CART_FOOTER_ACTION_TYPES } from "@constants";
import {
    CartFooterMeta,
    FooterMetaLineItemPart,
    TextFragment,
    FooterLineItemAction,
} from "@models";
import { ConfigurableIcon } from "@types";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { FOOTER_META_KEYS } from "../../constants";

@Component({
    selector: "supr-cart-footer-content",
    templateUrl: "./footer-content.component.html",
    styleUrls: ["./footer-content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterContentComponent {
    @Input() footerMeta: CartFooterMeta;

    footerMetaKeys = FOOTER_META_KEYS;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    getPartIconConfig(
        part: FooterMetaLineItemPart,
        iconKey: string
    ): ConfigurableIcon {
        if (!part) {
            return;
        }
        return {
            iconType: this.utilService.getNestedValue(
                part,
                `${iconKey}.iconType`
            ),
            iconSize: this.utilService.getNestedValue(
                part,
                `${iconKey}.iconSize`
            ),
            iconColor: this.utilService.getNestedValue(
                part,
                `${iconKey}.iconColor`
            ),
            iconName: this.utilService.getNestedValue(
                part,
                `${iconKey}.iconName`
            ),
        };
    }

    getPartTextConfig(part: FooterMetaLineItemPart): TextFragment {
        if (part && part.text) {
            return { text: part.text.text, textColor: part.text.textColor };
        }
    }

    handleRowClick(action: FooterLineItemAction) {
        const type = this.utilService.getNestedValue(action, "type", null);
        const url = this.utilService.getNestedValue(action, "url", null);
        if (type && url) {
            switch (type) {
                case CART_FOOTER_ACTION_TYPES.IN_APP_NAVIGATION:
                    this.routerService.goToUrl(url);
                    return;
                case CART_FOOTER_ACTION_TYPES.OUTSIDE_NAVIGATION:
                    this.routerService.openExternalUrl(url);
                    return;
                case CART_FOOTER_ACTION_TYPES.SCROLL:
                    this.scrollToId(url);
                    return;
                default:
                    return;
            }
        }
    }

    private scrollToId(elemId) {
        this.utilService.scrollToElementId(elemId);
    }
}
