export const TEXTS = {
    ADD_TO_SUPR_WALLET: "Add to Supr Wallet",
    PAY_USING_SUPR_WALLET: "Pay using Supr Wallet",
    PAY_USING_SUPR_CREDITS: "Pay using Supr Credits",
};

export const FOOTER_META_KEYS = {
    PREFIX: "prefix",
    TEXT: "text",
    SUFFIX: "suffix",
};
