import { CartFooterComponent } from "./supr-cart-footer.component";
import { FooterContentComponent } from "./components/footer-content/footer-content.component";

export const CartFooterComponents = [
    CartFooterComponent,
    FooterContentComponent,
];
