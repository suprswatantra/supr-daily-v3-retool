import {
    Input,
    Component,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { Observable, combineLatest } from "rxjs";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import { Sku, Order, Subscription, Feedback } from "@shared/models";
import { map } from "rxjs/operators";

@Component({
    selector: "supr-order-history-container",
    template: `
        <supr-order-history-product-tile
            [date]="date"
            [order]="order"
            [sku]="skuDetails$ | async"
            [subscription]="subscriptionDetails$ | async"
            [orderFeedback]="orderFeedback"
        ></supr-order-history-product-tile>
    `,
    styleUrls: ["./supr-order-history.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOrderHistoryComponent implements OnInit {
    skuDetails$: Observable<Sku>;
    subscriptionDetails$: Observable<Subscription>;

    @Input() date: string;
    @Input() order: Order;

    @Input() orderFeedback: Feedback.OrderFeedback[];

    constructor(
        private scheduleAdapter: ScheduleAdapter,
        private subscriptionAdapter: SubscriptionAdapter
    ) {}

    ngOnInit() {
        this.fetchSkuDetails();
        this.fetchSubscriptionDetails();
    }

    private fetchSkuDetails() {
        this.skuDetails$ = combineLatest([
            this.scheduleAdapter.getSkuFromOrderHistory(this.order.sku_id),
            this.scheduleAdapter.getSkuDetailsFromScheduleApiData(
                this.order.sku_id
            ),
        ]).pipe(
            map(([orderSku, scheduleSku]) => {
                return orderSku || scheduleSku;
            })
        );
    }

    private fetchSubscriptionDetails() {
        if (this.order.subscription_id) {
            this.subscriptionDetails$ = this.subscriptionAdapter.getSubscriptionDetails(
                this.order.subscription_id
            );
        }
    }
}
