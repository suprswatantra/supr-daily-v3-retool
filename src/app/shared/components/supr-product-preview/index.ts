import { ProductPreviewComponent } from "./supr-product-preview.component";
import { WrapperComponent } from "./components/wrapper.component";
import { InfoComponent } from "./components/info.component";
// import { ImageComponent } from "./components/image.component";
import { DetailsComponent } from "./components/details.component";

export const productPreviewComponents = [
    ProductPreviewComponent,
    WrapperComponent,
    InfoComponent,
    DetailsComponent,
];
