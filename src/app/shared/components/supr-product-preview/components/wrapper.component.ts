import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { Sku, CartItem } from "@models";
import { Segment } from "@types";

@Component({
    selector: "supr-product-preview-wrapper",
    template: `
        <ng-container *ngIf="showPreview">
            <supr-modal
                (handleClose)="handleClose?.emit()"
                modalName="${MODAL_NAMES.PRODUCT_PREVIEW}"
                [saContext]="saContext"
            >
                <supr-product-preview-info
                    [sku]="sku"
                    [mode]="mode"
                    [cartItem]="cartItem"
                    [isCartEmpty]="isCartEmpty"
                    [hideCart]="hideCart"
                    [showOosInfo]="showOosInfo"
                    [saContext]="saContext"
                    [saContextList]="saContextList"
                    [saPosition]="saPosition"
                    (handlePostSubscribe)="handlePostSubscribe.emit()"
                    (handlePostAddToCart)="handlePostAddToCart.emit()"
                ></supr-product-preview-info>
            </supr-modal>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() showPreview: boolean;
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() fromCatalog: boolean;
    @Input() cartItem: CartItem;
    @Input() isCartEmpty: boolean;
    @Input() hideCart: boolean;
    @Input() showOosInfo: boolean;

    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();
}
