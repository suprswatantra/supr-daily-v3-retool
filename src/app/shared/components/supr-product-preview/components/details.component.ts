import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CART_ITEM_TYPES, QTY_SUBTEXTS, SKU_PREFERRED_MODE } from "@constants";

import { Sku, CartItem } from "@models";

import { QuantityService } from "@services/util/quantity.service";

import { Segment } from "@types";

@Component({
    selector: "supr-product-preview-details",
    template: `
        <div class="details">
            <div class="suprRow spaceBetween">
                <div
                    class="suprColumn stretch top left detailsInfoWrapper"
                    [class.fadeIn]="!showOosInfo"
                >
                    <supr-text class="detailsName" type="subtitle">
                        {{ sku?.sku_name }}
                    </supr-text>
                    <supr-text class="detailsQty" type="subheading">
                        {{ displayQty }}
                    </supr-text>

                    <div class="detailsPriceWrapper">
                        <div class="suprRow">
                            <supr-text class="detailsPrice" type="subtitle">
                                {{ sku?.unit_price | rupee: 2 }}
                            </supr-text>
                            <ng-container *ngIf="showPriceStrike">
                                <div class="spacer8"></div>
                                <supr-text
                                    class="detailsPriceStrike"
                                    type="subheading"
                                >
                                    {{ sku?.unit_mrp | rupee: 2 }}
                                </supr-text>
                            </ng-container>
                        </div>
                    </div>
                </div>
                <div class="suprColumn stretch top center detailsActionWrapper">
                    <ng-container *ngIf="showOosInfo; else oosInfo">
                        <supr-add-button
                            [sku]="sku"
                            [mode]="mode"
                            [fromCatalog]="true"
                            [saContext]="saContext"
                            [saContextList]="saContextList"
                            (handlePostAddToCart)="handlePostAddToCart.emit()"
                        ></supr-add-button>
                        <div
                            *ngIf="
                                !cartItem &&
                                mode !==
                                    '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                            "
                            class="divider12"
                        ></div>
                        <supr-subscribe-button
                            [sku]="sku"
                            [mode]="mode"
                            [saContext]="saContext"
                            [saContextList]="saContextList"
                            (handlePostSubscribe)="handlePostSubscribe.emit()"
                        >
                        </supr-subscribe-button>
                        <div class="divider4"></div>
                        <supr-text type="paragraph" class="qtySubtext">
                            {{ _qtySubtext }}
                        </supr-text>
                    </ng-container>
                </div>

                <ng-template #oosInfo>
                    <supr-oos-info [sku]="sku"></supr-oos-info>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../supr-product-preview.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailsComponent implements OnInit {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() showOosInfo: boolean;

    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;
    @Input() set cartItem(item: CartItem) {
        this._cartItem = item;
        this.setQtySubtext(item);
    }
    get cartItem(): CartItem {
        return this._cartItem;
    }

    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();

    displayQty: string;
    _cartItem: CartItem;
    _qtySubtext: string;
    showPriceStrike = false;

    constructor(private qtyService: QuantityService) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.setPrice();
        this.setQty();
    }

    private setPrice() {
        if (!this.sku) {
            return;
        }
        this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
    }

    private setQty() {
        this.displayQty = this.qtyService.toText(this.sku);
    }

    private setQtySubtext(cartItem: CartItem) {
        if (!cartItem) {
            this._qtySubtext = "";
        } else if (cartItem.type === CART_ITEM_TYPES.ADDON) {
            this._qtySubtext = QTY_SUBTEXTS.ADDON;
        } else if (
            cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW ||
            cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        ) {
            this._qtySubtext = QTY_SUBTEXTS.SUBSCRIPTION;
        }
    }
}
