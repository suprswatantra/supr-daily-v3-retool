import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { Sku, CartItem } from "@models";
import { Segment } from "@types";

@Component({
    selector: "supr-product-preview-info",
    template: `
        <div class="preview" [class.bottomPadding]="!isCartEmpty">
            <supr-image
                [withWrapper]="false"
                [lazyLoad]="false"
                [src]="sku?.image?.fullUrl"
                [image]="sku?.image"
                [imgHeight]="${CLODUINARY_IMAGE_SIZE.PREVIEW.HEIGHT}"
                [imgWidth]="${CLODUINARY_IMAGE_SIZE.PREVIEW.WIDTH}"
                [class.fadeIn]="!showOosInfo"
            ></supr-image>
            <div class="divider24"></div>

            <supr-product-preview-details
                [sku]="sku"
                [mode]="mode"
                [cartItem]="cartItem"
                [showOosInfo]="showOosInfo"
                [saContext]="saContext"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
                (handlePostSubscribe)="handlePostSubscribe.emit()"
                (handlePostAddToCart)="handlePostAddToCart.emit()"
            >
            </supr-product-preview-details>
            <div class="divider24"></div>
            <supr-cart *ngIf="!isCartEmpty && !hideCart"></supr-cart>
        </div>
    `,
    styleUrls: ["../supr-product-preview.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() cartItem: CartItem;
    @Input() isCartEmpty: boolean;
    @Input() hideCart: boolean;
    @Input() showOosInfo: boolean;

    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();
}
