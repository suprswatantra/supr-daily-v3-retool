import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Observable } from "rxjs";

import { Sku, CartItem } from "@models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { Segment } from "@types";

@Component({
    selector: "supr-product-preview",
    template: `
        <supr-product-preview-wrapper
            [showPreview]="showPreview"
            [sku]="sku"
            [mode]="mode"
            [fromCatalog]="fromCatalog"
            [isCartEmpty]="isCartEmpty$ | async"
            [cartItem]="cartItem$ | async"
            [hideCart]="hideCart"
            [showOosInfo]="showOosInfo"
            [saContext]="saContext"
            [saContextList]="saContextList"
            [saPosition]="saPosition"
            (handleClose)="handleClose?.emit()"
            (handlePostSubscribe)="handlePostSubscribe.emit()"
            (handlePostAddToCart)="handlePostAddToCart.emit()"
        ></supr-product-preview-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductPreviewComponent implements OnInit {
    @Input() showPreview: boolean;
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() fromCatalog = true;
    @Input() hideCart = false;
    @Input() showOosInfo: boolean;

    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();

    isCartEmpty$: Observable<boolean>;
    cartItem$: Observable<CartItem>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isCartEmpty$ = this.adapter.isCartEmpty$;
        this.cartItem$ = this.adapter.getCartItemById(this.sku.id);
    }
}
