import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { ScheduleItem, Sku } from "@models";

import { Segment } from "@types";

@Component({
    selector: "supr-schedule-addon-container",
    template: `
        <supr-schedule-product-tile
            [date]="date"
            [paused]="paused"
            [schedule]="addOn"
            [editable]="editable"
            [showFrequency]="false"
            [statusText]="statusText"
            [sku]="skuDetails$ | async"
            [saContext]="saContext"
            [saContextList]="saContextList"
            [isSuprPassActive]="isSuprPassActive$ | async"
            [saObjectValueTile]="saObjectValueTile"
            (updateActionListener)="updateActionListener.emit($event)"
        ></supr-schedule-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleAddonContainer implements OnInit {
    skuDetails$: Observable<Sku>;
    isSuprPassActive$: Observable<boolean>;
    saObjectValueTile: number;

    @Input() date: string;
    @Input() paused: boolean;
    @Input() editable = true;
    @Input() statusText?: string;
    @Input() addOn: ScheduleItem;
    @Input() saContext?: string;
    @Input() saContextList?: Segment.ContextListItem[] = [];

    @Output() handleChangeClick: EventEmitter<any> = new EventEmitter();
    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    constructor(private adapter: ScheduleAdapter) {}

    ngOnInit() {
        this.skuDetails$ = this.adapter.getSkuDetailsFromScheduleApiData(
            this.addOn.sku_id
        );

        this.saObjectValueTile = this.addOn.sku_id;
        this.isSuprPassActive$ = this.adapter.isSuprPassActive$;
    }
}
