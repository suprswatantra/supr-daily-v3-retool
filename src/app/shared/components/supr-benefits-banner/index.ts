import { BenefitsComponent } from "./benefits.component";
import { BenefitBlockComponent } from "./components/benefit-block.component";

export const suprBenefitsBannerComponents = [
    BenefitsComponent,
    BenefitBlockComponent,
];
