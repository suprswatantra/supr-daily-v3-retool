export const BENEFITS_WRAPPER_STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-benefits-banner-wrapper-bg",
    },
];

export const BENEFIT_BLOCK_STYLE_LIST = [
    {
        attributeName: "header.bgColor",
        attributeStyleVariableName: "--supr-benefits-banner-block-header-bg",
    },
    {
        attributeName: "cta.textColor",
        attributeStyleVariableName:
            "--supr-benefits-banner-block-cta-icon-color",
    },
    {
        attributeName: "cta.fontSize",
        attributeStyleVariableName:
            "--supr-benefits-banner-block-cta-icon-font-size",
    },
    {
        attributeName: "tag.bgColor",
        attributeStyleVariableName: "--supr-benefits-banner-block-tag-bg",
    },
];
