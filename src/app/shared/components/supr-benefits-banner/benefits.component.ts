import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
} from "@angular/core";

import { BENEFITS_WRAPPER_STYLE_LIST } from "./constants";
import { BenefitsBanner, BenefitsBannerCtaAction } from "@shared/models";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-benefits-banner",
    template: `
        <div
            class="benefitsWrapper"
            [class.noHeader]="!benefitsBanner?.header || hideHeader"
            #benefitsWrapper
        >
            <ng-container *ngIf="benefitsBanner?.header && !hideHeader">
                <ng-container *ngIf="benefitsBanner?.header?.title?.text">
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="benefitsBanner?.header?.title"
                            class="boldText centerAlign"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container *ngIf="benefitsBanner?.header?.subTitle?.text">
                    <div class="divider8"></div>
                    <div class="suprRow center">
                        <supr-text-fragment
                            [textFragment]="benefitsBanner?.header?.subTitle"
                            class="centerAlign"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
            </ng-container>
            <ng-container *ngIf="benefitsBanner?.banner">
                <div class="divider24"></div>
                <supr-banner [banner]="benefitsBanner?.banner"></supr-banner>
            </ng-container>
            <ng-container
                *ngIf="benefitsBanner?.content && benefitsBanner.content.length"
            >
                <div class="divider24" *ngIf="benefitsBanner?.header"></div>
                <div class="benefitsContentWrapper">
                    <ng-container
                        *ngFor="
                            let benefit of benefitsBanner.content;
                            last as isLast
                        "
                    >
                        <ng-container *ngIf="!hideDefaults">
                            <ng-template
                                [ngTemplateOutlet]="benefitsblock"
                            ></ng-template>
                        </ng-container>

                        <ng-container *ngIf="hideDefaults">
                            <ng-container *ngIf="!benefit?.isDefault">
                                <ng-template
                                    [ngTemplateOutlet]="benefitsblock"
                                ></ng-template>
                            </ng-container>
                        </ng-container>

                        <ng-template #benefitsblock>
                            <supr-benefits-banner-block
                                [benefitBlock]="benefit"
                                [isLast]="isLast"
                                [hideCta]="hideCta"
                                (handleBenefitCtaClick)="
                                    handleBenefitCtaClick.emit($event)
                                "
                            ></supr-benefits-banner-block>
                        </ng-template>
                    </ng-container>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["./styles/benefits.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BenefitsComponent implements OnInit {
    @Input() benefitsBanner: BenefitsBanner;
    @Input() hideHeader: boolean;
    @Input() hideCta: boolean;
    /** Hide defaults used to filter out isDefault [Used in recharge subs page] */
    @Input() hideDefaults: boolean;

    @Output() handleBenefitCtaClick: EventEmitter<
        BenefitsBannerCtaAction
    > = new EventEmitter();

    @ViewChild("benefitsWrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.benefitsBanner) {
            return;
        }

        this.utilService.setStyles(
            this.benefitsBanner,
            BENEFITS_WRAPPER_STYLE_LIST,
            this.wrapperEl
        );
    }
}
