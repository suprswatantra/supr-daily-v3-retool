import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
} from "@angular/core";

import { BENEFIT_BLOCK_STYLE_LIST } from "../constants";
import { BenefitsBannerContent, BenefitsBannerCtaAction } from "@shared/models";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-benefits-banner-block",
    template: `
        <div
            class="benefitBlockWrapper"
            [class.isLast]="isLast"
            #benefitBlockWrapper
        >
            <ng-container *ngIf="benefitBlock?.header?.text?.text">
                <div class="benefitBlockHeader">
                    <supr-text-fragment
                        [textFragment]="benefitBlock?.header?.text"
                    ></supr-text-fragment>
                </div>
            </ng-container>
            <div class="suprRow top contentBlock">
                <div class="suprColumn left top">
                    <ng-container *ngIf="benefitBlock?.upperFoldText?.text">
                        <div class="suprRow textRow">
                            <ng-container
                                *ngIf="
                                    benefitBlock?.tag?.text &&
                                    (!benefitBlock?.tagPosition ||
                                        benefitBlock?.tagPosition === 'left')
                                "
                            >
                                <div *ngIf="benefitBlock?.tag?.text">
                                    <div class="tag">
                                        <supr-text-fragment
                                            [textFragment]="benefitBlock?.tag"
                                        ></supr-text-fragment>
                                    </div>
                                </div>
                                <div class="spacer8"></div>
                            </ng-container>
                            <supr-text-fragment
                                [textFragment]="benefitBlock?.upperFoldText"
                            ></supr-text-fragment>
                            <ng-container
                                *ngIf="
                                    benefitBlock?.tag?.text &&
                                    benefitBlock?.tagPosition &&
                                    benefitBlock?.tagPosition === 'right'
                                "
                            >
                                <div class="spacer8"></div>
                                <div *ngIf="benefitBlock?.tag?.text">
                                    <div class="tag">
                                        <supr-text-fragment
                                            [textFragment]="benefitBlock?.tag"
                                        ></supr-text-fragment>
                                    </div>
                                </div>
                            </ng-container>
                        </div>
                    </ng-container>
                    <ng-container *ngIf="benefitBlock?.middleFoldText?.text">
                        <div class="suprRow">
                            <supr-text-fragment
                                [textFragment]="benefitBlock?.middleFoldText"
                                class="boldText middleFold"
                            ></supr-text-fragment>
                            <div class="spacer4"></div>
                            <supr-configurable-icon
                                class="middleFoldIcon"
                                [config]="benefitBlock?.middleFoldIcon"
                            ></supr-configurable-icon>
                        </div>
                    </ng-container>
                    <ng-container *ngIf="benefitBlock?.lowerFoldText?.text">
                        <div
                            class="divider8"
                            *ngIf="!benefitBlock?.middleFoldText?.text"
                        ></div>
                        <supr-text-fragment
                            [textFragment]="benefitBlock?.lowerFoldText"
                        ></supr-text-fragment>
                    </ng-container>
                    <ng-container *ngIf="benefitBlock?.cta?.text && !hideCta">
                        <div class="divider12"></div>
                        <div
                            class="suprRow textRow cta"
                            (click)="
                                handleBenefitCtaClick.emit(
                                    benefitBlock?.ctaAction
                                )
                            "
                        >
                            <supr-text-fragment
                                [textFragment]="benefitBlock?.cta"
                                class="semiBoldText"
                            ></supr-text-fragment>
                            <supr-icon
                                class="ctaIcon"
                                name="chevron_right"
                            ></supr-icon>
                        </div>
                    </ng-container>
                </div>
                <ng-container *ngIf="benefitBlock?.imgUrl">
                    <div class="suprColumn benefitBlockIcon right top">
                        <div class="imgWrapper">
                            <supr-image
                                [src]="benefitBlock.imgUrl"
                            ></supr-image>
                        </div>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../styles/benefit-block.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BenefitBlockComponent implements OnInit {
    @Input() benefitBlock: BenefitsBannerContent;
    @Input() isLast: boolean;
    @Input() hideCta: boolean;

    @Output() handleBenefitCtaClick: EventEmitter<
        BenefitsBannerCtaAction
    > = new EventEmitter();

    @ViewChild("benefitBlockWrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.benefitBlock) {
            return;
        }

        this.utilService.setStyles(
            this.benefitBlock,
            BENEFIT_BLOCK_STYLE_LIST,
            this.wrapperEl
        );
    }
}
