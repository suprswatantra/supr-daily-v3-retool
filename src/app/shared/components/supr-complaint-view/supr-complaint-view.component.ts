import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    ViewChild,
    EventEmitter,
    ElementRef,
} from "@angular/core";

import { ComplaintViewV2 } from "@models";
import { ComplaintSetting } from "@types";
import { ANALYTICS_OBJECT_NAMES, COMPLAINTS } from "@constants";

import { DateService } from "@services/date/date.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-complaint-view",
    template: `
        <div
            class="complaintView"
            [class.new]="complaintV2?.is_new"
            saClick
            saImpression
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.COMPLAINTS_VIEW}"
            [saObjectValue]="complaintV2?.complaint_id"
            [saContext]="
                complaintV2?.complaint_status_timeline?.complaint_status
            "
            #statustag
        >
            <div (click)="handleComplaintClick()">
                <div class="suprRow issueDate">
                    <supr-text>{{ issueDateText }}</supr-text>
                    <div class="spacer8"></div>
                    <supr-text class="newText" *ngIf="complaintV2?.is_new">{{
                        complaintConfig.NEW
                    }}</supr-text>
                </div>
                <div class="divider4"></div>

                <div class="suprRow type">
                    <supr-text
                        >{{ complaintV2?.complaint_type_text }}
                        {{ skuString }}</supr-text
                    >
                </div>
                <div class="divider4"></div>

                <div class="suprRow details">
                    <supr-text
                        >Issue #{{ complaintV2?.complaint_id }}</supr-text
                    >
                    <div class="spacer8"></div>
                    <supr-text> | </supr-text>
                    <div class="spacer8"></div>
                    <supr-text> {{ complaintDateText }}</supr-text>
                </div>
                <div class="divider8"></div>
            </div>

            <div class="suprRow status">
                <supr-tag
                    [ngClass]="
                        complaintV2?.complaint_status_timeline.complaint_status
                    "
                >
                    <supr-text>{{
                        complaintV2?.complaint_status_timeline
                            .complaint_status_tag
                    }}</supr-text>
                </supr-tag>
            </div>
            <div class="divider16"></div>
            <div *ngIf="!hideComplaintActions">
                <div class="separator"></div>

                <div class="suprRow spaceBetween footer">
                    <div class="suprRow helpLable">
                        <supr-icon name="error"></supr-icon>
                        <div class="spacer4"></div>
                        <supr-text>{{ lable }}</supr-text>
                    </div>
                    <div class="suprRow helpLink">
                        <supr-button
                            (handleClick)="reachTrigger()"
                            saClick
                            saImpression
                            [saObjectName]="saObjectNameReachUs"
                            [saObjectValue]="complaintV2.complaint_id"
                            [saContext]="
                                complaintV2?.complaint_status_timeline
                                    .complaint_status
                            "
                            >{{ actionText }}</supr-button
                        >
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-complaint-view.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprComplaintView implements OnInit {
    @Input() complaintV2: ComplaintViewV2;
    @Input() saObjectName: string;

    @Output() handleClick: EventEmitter<ComplaintViewV2> = new EventEmitter();
    @Output() complaintClick: EventEmitter<
        ComplaintViewV2
    > = new EventEmitter();

    @ViewChild("statustag", { static: true }) wrapperEl: ElementRef;

    issueDateText: string;
    complaintDateText: string;
    complaintDate: Date;
    lable: string;
    actionText: string;
    hideComplaintActions: boolean = false;
    complaintConfig: ComplaintSetting;

    constructor(
        private dateService: DateService,
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.setIssueDate();
        this.setComplaintDate();
        this.complaintConfig = this.fetchComplaintConfig();
        this.lable = this.complaintConfig.COMPLAINT_ACTION_LABLE;
        this.actionText = this.complaintConfig.COMPLAINT_ACTION;
        this.hideComplaintActions = this.complaintConfig.HIDE_COMPLAINT_ACTIONS;
    }

    get skuString() {
        return (
            (this.utilService.isLengthyArray(this.complaintV2.sku_names) &&
                this.complaintV2.sku_names.reduce(
                    (acc: string, sku_name: string) => {
                        acc = acc + " - " + sku_name;
                        return acc;
                    },
                    ""
                )) ||
            ""
        );
    }

    get saObjectNameReachUs() {
        return `${this.saObjectName}-${ANALYTICS_OBJECT_NAMES.CLICK.COMPLAINTS_REACH}`;
    }

    handleComplaintClick() {
        // disabled
        //this.complaintClick.emit(this.complaintV2);
    }

    ngAfterViewInit() {
        this.setBgColor();
    }

    private setBgColor() {
        const { colour } = this.complaintV2.complaint_status_timeline;
        if (this.wrapperEl) {
            this.wrapperEl.nativeElement.style.setProperty(
                "--complaint-tag-color",
                colour
            );
        }
    }

    private fetchComplaintConfig() {
        const complaintConfig = this.settingsService.getSettingsValue(
            "complaintConfig",
            COMPLAINTS
        );
        return complaintConfig;
    }

    private setIssueDate() {
        const { order_date } = this.complaintV2;
        const issueDate = this.dateService.dateFromText(order_date);

        if (issueDate) {
            const timeSlot = this.getTimeSlot();
            this.issueDateText = `Order date ${issueDate.getDate()}${this.dateService.getDateSuffix(
                order_date
            )} ${issueDate.toLocaleString("default", {
                month: "long",
            })} ${timeSlot}`;
        }
    }

    private setComplaintDate() {
        try {
            const [
                date = "",
                hours = "",
                minutes = "",
            ] = this.complaintV2.complaint_creation_time.split(/[ T,\:]+/);
            const complaintDate = this.dateService.dateFromText(date);
            this.complaintDate = complaintDate;
            const month = complaintDate.toLocaleString("default", {
                month: "long",
            });
            const day = complaintDate.getDate();
            const year = complaintDate.getFullYear();

            var newDate = new Date(
                `${month} ${day},  ${year} ${hours}:${minutes}`
            );
            var options = {
                hour: "numeric",
                minute: "numeric",
                hour12: true,
            };
            var timeString = newDate.toLocaleString("default", options);

            this.complaintDateText = `Raised on ${month} ${day},  ${timeString}`;
        } catch (err) {
            this.complaintDateText = "";
        }
    }

    reachTrigger() {
        this.handleClick.emit({
            ...this.complaintV2,
            complaintDate: this.complaintDate,
        });
    }

    private getTimeSlot() {
        try {
            const { time_slot } = this.complaintV2;
            if (time_slot) {
                const timearr = time_slot && time_slot.split(/[-,\:]+/);
                let timeSlotAssigned = timearr[2] || "";
                timeSlotAssigned = timeSlotAssigned.startsWith("0")
                    ? timeSlotAssigned.slice(1)
                    : timeSlotAssigned;
                const timeSlot =
                    (timeSlotAssigned && `for ${timeSlotAssigned}am`) || "";
                return timeSlot;
            }
            return "";
        } catch (err) {
            return "";
        }
    }
}
