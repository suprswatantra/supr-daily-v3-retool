import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { DELIVERY_EDIT_MODAL_TEXTS } from "@constants";

import { Sku, Subscription, ScheduleItem, CartItem, Vacation } from "@models";

import { CartAdapter } from "@shared/adapters/cart.adapter";
import { PauseAdapter } from "@shared/adapters/pause.adapter";
import { SearchAdapter } from "@shared/adapters/search.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { UpdateSubInstructionActionPayload } from "@types";

@Component({
    selector: "supr-subscription-edit-card-container",
    template: `
        <supr-schedule-subscription-edit-layout
            [sku]="sku"
            [date]="date"
            [schedule]="schedule"
            [subscription]="subscription"
            [vacation]="vacation$ | async"
            [adjustmentAction]="adjustmentAction"
            [subscriptionState]="subscriptionState$ | async"
            [subscriptionError]="subscriptionError$ | async"
            [showCancelAction]="showCancelAction"
            [showChangeAction]="showChangeAction"
            [showOrderUpdatedToast]="showOrderUpdatedToast"
            [confirmBtnText]="confirmBtnText"
            [showAdjustmentText]="showAdjustmentText"
            [headerText]="headerText"
            [secondaryActionText]="secondaryActionText"
            (handleAddToCart)="addItemToCart($event)"
            (updateSubscriptionInstruction)="
                updateSubscriptionInstruction($event)
            "
            (updateActionListener)="updateActionListener.emit($event)"
            (handleChangeDateClick)="handleChangeDateClick.emit($event)"
        ></supr-schedule-subscription-edit-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSubscriptionEditCardComponent implements OnInit {
    @Input() sku: Sku;
    @Input() date: string;
    @Input() confirmBtnText: string;
    @Input() schedule: ScheduleItem;
    @Input() showCancelAction = true;
    @Input() adjustmentAction: string;
    @Input() showChangeAction = false;
    @Input() showAdjustmentText = true;
    @Input() subscription: Subscription;
    @Input() secondaryActionText?: string;
    @Input() showOrderUpdatedToast = false;
    @Input() instantRefreshSchedule: boolean;
    @Input() headerText = DELIVERY_EDIT_MODAL_TEXTS.EDIT;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();
    @Output() handleChangeDateClick: EventEmitter<void> = new EventEmitter();

    vacation$: Observable<Vacation>;
    subscriptionError$: Observable<any>;
    subscriptionState$: Observable<SubscriptionCurrentState>;

    constructor(
        private cartAdapter: CartAdapter,
        private pauseAdapter: PauseAdapter,
        private searchAdapter: SearchAdapter,
        private subscriptionAdapter: SubscriptionAdapter
    ) {}

    ngOnInit() {
        this.vacation$ = this.pauseAdapter.vacation$;
        this.subscriptionState$ = this.subscriptionAdapter.subscriptionState$;
        this.subscriptionError$ = this.subscriptionAdapter.subscriptionError$;
    }

    updateSubscriptionInstruction(
        updateData: UpdateSubInstructionActionPayload
    ) {
        const { subscriptionId, quantity, date } = updateData;

        this.subscriptionAdapter.updateSubscriptionInstruction(
            subscriptionId,
            quantity,
            date,
            this.instantRefreshSchedule
        );
    }

    addItemToCart(cartItem: CartItem) {
        this.cartAdapter.updateItemInCart(cartItem);
        if (cartItem) {
            this.searchAdapter.sendDeliverSubBalInstrumentation(
                cartItem.sku_id
            );
        }
    }
}
