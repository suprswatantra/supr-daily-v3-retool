import {
    Input,
    Output,
    OnInit,
    OnChanges,
    Component,
    ViewChild,
    ElementRef,
    EventEmitter,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    MODAL_NAMES,
    CLODUINARY_IMAGE_SIZE,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { Sku, Subscription, ScheduleItem } from "@shared/models";

import { QuantityService } from "@services/util/quantity.service";
import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";

import { Segment } from "@types";

import { SA_OBJECT_NAMES as SCHEDULE_SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";

@Component({
    selector: "supr-schedule-product-tile",
    templateUrl: "./supr-schedule-product-tile.component.html",
    styleUrls: ["./supr-schedule-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleProductTileComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() date: string;
    @Input() paused: boolean;
    @Input() editable = true;
    @Input() statusText: string;
    @Input() showFrequency = true;
    @Input() schedule: ScheduleItem;
    @Input() saObjectValueTile: number;
    @Input() subscription?: Subscription;
    @Input() saContext?: string;
    @Input() saContextList?: Segment.ContextListItem[] = [];
    @Input() isSuprPassActive: boolean;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    quantityText: string;
    showEditModal = false;
    scheduleStatusConfig: any;
    changeScheduleModalName: string;
    saObjectValueChangeScheduleModal?: number;
    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;
    saObjectNameChange = SCHEDULE_SA_OBJECT_NAMES.CLICK.CHANGE_BUTTON;

    constructor(
        private cdr: ChangeDetectorRef,
        private settingsService: SettingsService,
        private quantityService: QuantityService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.scheduleStatusConfig = this.settingsService.getSettingsValue(
            "scheduleStatusConfig",
            SETTINGS_KEYS_DEFAULT_VALUE.scheduleStatusConfig
        );
    }

    ngOnChanges() {
        this.setQuantityText();
        this.setAnalyticsData();
    }

    onClick($event) {
        $event.stopPropagation();

        const isMaskedSku = !!this.utilService.getNestedValue(
            this.schedule,
            "meta.sku_mask_details",
            false
        );

        if (this.editable && !isMaskedSku) {
            this.showEditModal = true;
        }
    }

    closeEditModal() {
        this.showEditModal = false;
        this.cdr.detectChanges();
    }

    private setQuantityText() {
        if (this.sku) {
            this.quantityText = this.quantityService.toText(
                this.sku,
                this.schedule.quantity
            );
        }
    }

    private setAnalyticsData() {
        this.changeScheduleModalName = this.subscription
            ? MODAL_NAMES.EDIT_SUBSCRIPTION_DELIVERY
            : MODAL_NAMES.EDIT_ADDON_DELIVERY;

        if (this.subscription) {
            this.saObjectValueChangeScheduleModal = this.subscription.id;
        }
    }
}
