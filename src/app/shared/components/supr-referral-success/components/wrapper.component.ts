import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES, MODAL_TYPES } from "@constants";

import { CopyBlock } from "@shared/models";

import { ModalService } from "@services/layout/modal.service";

import { ANALYTICS_OBJECT_NAMES } from "@shared/components/supr-referral-success/constants/analytics";
import {
    TEXTS,
    DEFAULT_IMG_URL,
} from "@shared/components/supr-referral-success/constants";

@Component({
    selector: "supr-referral-success-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <div class="fullScreenModal">
                <supr-modal
                    class="suprColumn center"
                    [modalType]="type"
                    (handleClose)="handleClose?.emit()"
                    modalName="${MODAL_NAMES.REFERRAL_SUCCESS_MODAL}"
                >
                    <div class="wrapper">
                        <div class="suprRow center">
                            <img [src]="imgUrl || defaultImgUrl" />
                        </div>
                        <div class="suprRow center">
                            <supr-text type="title">
                                {{ title || defaultTitle }}
                            </supr-text>
                        </div>
                        <ng-container *ngIf="description">
                            <div class="divider8"></div>
                            <div class="suprRow center">
                                <supr-text type="subheading">
                                    {{ description }}
                                </supr-text>
                            </div>
                        </ng-container>
                        <ng-container *ngIf="copyBlock">
                            <div class="divider16"></div>
                            <div class="suprRow center">
                                <supr-copy-block
                                    [copyText]="copyBlock?.copyText"
                                    [bgColor]="copyBlock?.bgColor"
                                    [iconColor]="copyBlock?.iconColor"
                                    [borderColor]="copyBlock?.borderColor"
                                    [showHelpText]="true"
                                    [copyCodeClickSaObjectName]="
                                        copyCodeClickSaObjectName
                                    "
                                ></supr-copy-block>
                            </div>
                        </ng-container>
                        <ng-container *ngIf="!copyBlock">
                            <div class="divider16"></div>
                            <div class="suprRow center">
                                <supr-text type="paragraph">
                                    ${TEXTS.KEEP_REFERRING}
                                </supr-text>
                            </div>
                        </ng-container>
                    </div>
                </supr-modal>
                <div class="footerWrapper">
                    <div class="suprRow center">
                        <supr-button
                            (handleClick)="closeModal()"
                            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                .CLOSE_BUTTON}"
                            >${TEXTS.CLOSE_BTN}</supr-button
                        >
                    </div>
                </div>
            </div>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-referral-success.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() showModal: boolean;
    @Input() imgUrl: string;
    @Input() title: string;
    @Input() description: string;
    @Input() copyBlock: CopyBlock;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    defaultTitle = TEXTS.TITLE;
    defaultImgUrl = DEFAULT_IMG_URL;

    constructor(private modalService: ModalService) {}

    texts = TEXTS;
    type = MODAL_TYPES.NO_WRAPPER;
    copyCodeClickSaObjectName = ANALYTICS_OBJECT_NAMES.CLICK.COPY_CODE;

    closeModal() {
        this.modalService.closeModal();
    }
}
