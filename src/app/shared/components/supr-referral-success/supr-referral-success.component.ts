import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { CopyBlock } from "@shared/models";

@Component({
    selector: "supr-referral-success",
    template: `
        <supr-referral-success-wrapper
            [showModal]="showModal"
            [imgUrl]="imgUrl"
            [title]="title"
            [description]="description"
            [copyBlock]="copyBlock"
            (handleClose)="handleClose?.emit()"
        ></supr-referral-success-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralSuccessComponent {
    @Input() showModal: boolean;
    @Input() imgUrl: string;
    @Input() title: string;
    @Input() description: string;
    @Input() copyBlock: CopyBlock;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
}
