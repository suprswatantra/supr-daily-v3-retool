import { ReferralSuccessComponent } from "./supr-referral-success.component";
import { WrapperComponent } from "./components/wrapper.component";

export const referralSuccessComponents = [
    ReferralSuccessComponent,
    WrapperComponent,
];
