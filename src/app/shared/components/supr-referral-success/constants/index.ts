export const TEXTS = {
    CLOSE_BTN: "CLOSE",
    KEEP_REFERRING: "KEEP REFERRING!",
    TITLE: "Congratulations!",
};

export const DEFAULT_IMG_URL = "assets/images/app/supr-referral.svg";
