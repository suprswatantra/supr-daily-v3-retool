import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-c",
    template: `
        <supr-v3-tile-variant-c-content
            [data]="data"
        ></supr-v3-tile-variant-c-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantC {
    @Input() data: V3LayoutItemData;
}
