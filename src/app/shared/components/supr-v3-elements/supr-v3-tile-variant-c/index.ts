import { V3TileVariantC } from "./supr-v3-tile-variant-c.component";
import { TileComponent } from "./components/tile.component";

export const tileCComponents = [TileComponent, V3TileVariantC];
