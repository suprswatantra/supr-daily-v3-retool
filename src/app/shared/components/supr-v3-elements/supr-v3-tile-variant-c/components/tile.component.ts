import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { V3LayoutImage, V3LayoutItemData } from "@shared/models";

import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-tile-variant-c-content",
    template: `
        <div
            class="suprColumn tile center card"
            #tile
            (click)="onClick()"
            saClick
            [saClickedEnabled]="data?.analytics?.saClick"
            [saObjectName]="data?.analytics?.saClick?.objectName"
            [saObjectValue]="data?.analytics?.saClick?.objectValue"
            [saContext]="data?.analytics?.saClick?.context"
            [saContextList]="data?.analytics?.saClick?.contextList"
            [saObjectTag]="data?.analytics?.saClick?.objectTag"
        >
            <div class="tileImage" [ngStyle]="data?.image?.style">
                <supr-image
                    [withWrapper]="true"
                    [src]="data?.image?.data?.fullUrl"
                    [image]="data?.image?.data"
                    [imgHeight]="
                        data?.image?.dimension?.height ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT
                    "
                    [imgWidth]="
                        data?.image?.dimension?.width ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH
                    "
                    [imageStyle]="data?.image?.style"
                ></supr-image>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent implements OnInit {
    @Input() data: V3LayoutItemData;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    ngOnInit() {
        this.setStyles();
    }

    constructor(private homeLayoutService: HomeLayoutService) {}

    private setStyles() {
        this.setImageStyle();
    }

    private setImageStyle() {
        if (!this.styleExists(this.data.image)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-image-width",
            this.data.image.style.width
        );

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-image-height",
            this.data.image.style.height
        );
    }

    private styleExists(image: V3LayoutImage): boolean {
        return !!(image && image.style);
    }

    onClick() {
        this.homeLayoutService.handleEvents(this.data);
    }
}
