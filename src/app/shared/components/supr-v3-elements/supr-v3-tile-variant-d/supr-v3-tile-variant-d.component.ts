import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-d",
    template: `
        <supr-v3-tile-variant-d-content
            [data]="data"
        ></supr-v3-tile-variant-d-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantD {
    @Input() data: V3LayoutItemData;
}
