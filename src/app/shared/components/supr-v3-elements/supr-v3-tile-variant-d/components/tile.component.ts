import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { V3LayoutItemData } from "@shared/models";
import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-tile-variant-d-content",
    template: `
        <div
            class="suprColumn tile center card"
            (click)="handleClick()"
            saClick
            [saClickedEnabled]="data?.analytics?.saClick"
            [saObjectName]="data?.analytics?.saClick?.objectName"
            [saObjectValue]="data?.analytics?.saClick?.objectValue"
            [saContext]="data?.analytics?.saClick?.context"
            [saContextList]="data?.analytics?.saClick?.contextList"
            [saObjectTag]="data?.analytics?.saClick?.objectTag"
        >
            <div class="tileImage" [ngStyle]="data?.image?.style">
                <supr-image
                    [withWrapper]="false"
                    [src]="data?.image?.data?.fullUrl"
                    [image]="data?.image?.data"
                    [imgHeight]="
                        data?.image?.dimension?.height ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT
                    "
                    [imgWidth]="
                        data?.image?.dimension?.width ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH
                    "
                ></supr-image>
            </div>
            <div class="suprColumn center" *ngIf="data?.title">
                <div class="divider8"></div>
                <supr-text [type]="data?.title?.type || 'body'">
                    {{ data?.title?.text }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-d.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent {
    @Input() data: V3LayoutItemData;

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    constructor(private homeLayoutService: HomeLayoutService) {}

    handleClick() {
        this.homeLayoutService.handleEvents(this.data);
    }
}
