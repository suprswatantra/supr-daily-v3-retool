import { TileComponent } from "./components/tile.component";
import { V3TileVariantD } from "./supr-v3-tile-variant-d.component";

export const tileDComponents = [TileComponent, V3TileVariantD];
