import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { V3LayoutText, V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-a-content",
    template: `
        <div
            class="suprRow tile left card"
            #tile
            saClick
            [saObjectName]="data?.analytics?.saClick?.objectName"
            [saObjectValue]="data?.analytics?.saClick?.objectValue"
            [saContext]="data?.analytics?.saClick?.context"
            [saContextList]="data?.analytics?.saClick?.contextList"
        >
            <supr-image
                [withWrapper]="true"
                [src]="item?.image?.data?.fullUrl"
                [image]="item?.image?.data"
                [imgHeight]="
                    data?.image?.dimension?.height ||
                    CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT
                "
                [imgWidth]="
                    data?.image?.dimension?.width ||
                    CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH
                "
            ></supr-image>

            <div class="spacer8"></div>
            <supr-text [type]="item?.title?.textType || 'body'">
                {{ item?.title?.text }}
            </supr-text>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-a.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent implements OnInit {
    @Input() data: V3LayoutItemData;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    item: V3LayoutItemData;
    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    ngOnInit() {
        this.item = this.data;
        this.setStyles();
    }

    private setStyles() {
        this.setTitleStyle();
    }

    private setTitleStyle() {
        if (!this.styleExists(this.item.title)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-title-color",
            this.item.title.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }
}
