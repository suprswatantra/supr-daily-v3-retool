import { V3TileVariantA } from "./supr-v3-tile-variant-a.component";
import { TileComponent } from "./components/tile.component";

export const tileAComponents = [V3TileVariantA, TileComponent];
