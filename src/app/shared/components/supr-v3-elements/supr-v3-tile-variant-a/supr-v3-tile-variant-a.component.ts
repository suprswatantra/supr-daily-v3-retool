import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-a",
    template: `
        <div [ngStyle]="data?.style">
            <supr-v3-tile-variant-a-content
                [data]="data"
                saImpression
                [saImpressionEnabled]="data?.cta?.analytics?.saImpression"
                [saObjectName]="data?.cta?.analytics?.saImpression?.objectName"
                [saObjectValue]="
                    data?.cta?.analytics?.saImpression?.objectValue
                "
                [saObjectTag]="data?.cta?.analytics?.saImpression?.objectTag"
                [saContext]="data?.cta?.analytics?.saImpression?.context"
                [saContextList]="
                    data?.cta?.analytics?.saImpression?.contextList
                "
            ></supr-v3-tile-variant-a-content>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantA {
    @Input() data: V3LayoutItemData;
}
