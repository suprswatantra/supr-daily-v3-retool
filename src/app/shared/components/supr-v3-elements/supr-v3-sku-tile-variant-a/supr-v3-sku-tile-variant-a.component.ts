import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { Sku, SkuAttributes, V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-a",
    template: `
        <div class="suprRow tile top card" *ngIf="data" [ngStyle]="data?.style">
            <supr-v3-sku-tile-variant-a-info
                class="suprColumn left grow"
                [sku]="sku"
                [image]="data?.image"
                [preferredMode]="data?.preferredMode"
                [data]="data"
                [skuAttributes]="skuAttributes"
                [skuNotified]="skuNotified"
            ></supr-v3-sku-tile-variant-a-info>
        </div>
    `,
    styleUrls: ["./supr-v3-tile-variant-a.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuTileVariantA {
    @Input() data: V3LayoutItemData;
    @Input() sku: Sku;
    @Input() skuNotified: string;
    @Input() skuAttributes: SkuAttributes;
}
