import { V3SkuTileVariantA } from "./supr-v3-sku-tile-variant-a.component";

import { V3SkuImageComponent } from "./components/image.component";
import { V3SkuDetailsComponent } from "./components/details.component";
import { V3SkuInfoComponent } from "./components/info.component";
import { V3SkuTileVariantAContainer } from "./supr-v3-sku-tile-variant-a-container";

export const v3SkuTileVariantAComponents = [
    V3SkuTileVariantAContainer,
    V3SkuTileVariantA,
    V3SkuDetailsComponent,
    V3SkuImageComponent,
    V3SkuInfoComponent,
];
