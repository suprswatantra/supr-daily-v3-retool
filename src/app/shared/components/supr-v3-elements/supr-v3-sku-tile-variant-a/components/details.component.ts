import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    ElementRef,
    ViewChild,
} from "@angular/core";

import { Sku } from "@models";
import { QuantityService } from "@services/util/quantity.service";

@Component({
    selector: "supr-v3-sku-tile-a-details",
    template: `
        <div class="details" #details>
            <supr-text class="detailsName" type="body" [truncate]="true">
                {{ sku?.sku_name }}
            </supr-text>
            <div class="divider4"></div>
            <div
                class="suprRow spaceBetween detailsDetails"
                *ngIf="!showOosInfo"
            >
                <div class="suprRow">
                    <supr-text class="detailsPrice" type="body">
                        {{ sku?.unit_price | rupee: 2 }}
                    </supr-text>
                    <ng-container *ngIf="showPriceStrike">
                        <div class="spacer8"></div>
                        <supr-text class="detailsPriceStrike" type="caption">
                            {{ sku?.unit_mrp | rupee: 2 }}
                        </supr-text>
                    </ng-container>
                    <div class="spacer4"></div>
                    <supr-text class="detailsQty" type="caption">
                        •
                    </supr-text>
                    <div class="spacer4"></div>
                    <supr-text class="detailsQty" type="subtext10">
                        {{ displayQty }}
                    </supr-text>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-a.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuDetailsComponent implements OnInit {
    @Input() sku: Sku;
    @Input() categoryId: number;
    @Input() showOosInfo: boolean;

    @ViewChild("details", { static: true }) detailsEl: ElementRef;

    displayQty: string;
    showPriceStrike = false;

    constructor(private qtyService: QuantityService) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.setPrice();
        this.setQty();
        this.setTitleTruncate();
    }

    private setPrice() {
        if (!this.sku) {
            return;
        }
        this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
    }

    private setTitleTruncate() {
        if (!this.showOosInfo) {
            this.detailsEl.nativeElement.style.setProperty(
                "--title-truncate-length",
                "2"
            );
        } else {
            this.detailsEl.nativeElement.style.setProperty(
                "--title-truncate-length",
                "2"
            );
        }
    }

    private setQty() {
        this.displayQty = this.qtyService.toText(this.sku);
    }
}
