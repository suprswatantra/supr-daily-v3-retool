import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { SKU_PREFERRED_MODE } from "@constants";
import { Sku, SkuAttributes, V3LayoutImage, V3LayoutItemData } from "@models";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-v3-sku-tile-variant-a-info",
    template: `
        <div class="suprRow top ">
            <div
                class="tileImage"
                [ngStyle]="image?.style"
                (click)="openPreviewModal()"
                [class.overlay50]="_showOosInfo"
            >
                <supr-v3-sku-tile-variant-a-image
                    [skuImageUrl]="sku?.image?.fullUrl"
                    [compressedImage]="sku?.image"
                ></supr-v3-sku-tile-variant-a-image>
            </div>
            <div class="spacer28"></div>
            <div class="suprColumn spaceBetween left info">
                <div
                    class="suprRow top "
                    (click)="openPreviewModal()"
                    [class.overlay40]="_showOosInfo"
                >
                    <supr-v3-sku-tile-a-details
                        [sku]="sku"
                        [showOosInfo]="_showOosInfo"
                    ></supr-v3-sku-tile-a-details>
                    <div class="divider8"></div>
                </div>

                <supr-add-button
                    *ngIf="
                        !_showOosInfo &&
                        preferredMode !==
                            '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                    "
                    [sku]="sku"
                    [isOosUpfront]="_oosUpfront"
                    [mode]="preferredMode"
                    [saContext]="data?.analytics?.saClick?.context"
                    [saContextList]="data?.analytics?.saClick?.contextList"
                ></supr-add-button>

                <supr-subscribe-button
                    *ngIf="
                        !_showOosInfo &&
                        preferredMode ===
                            '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                    "
                    [sku]="sku"
                    [isOosUpfront]="_oosUpfront"
                    [mode]="preferredMode"
                    [saContext]="data?.analytics?.saClick?.context"
                    [saContextList]="data?.analytics?.saClick?.contextList"
                >
                </supr-subscribe-button>

                <supr-oos-info
                    *ngIf="_showOosInfo"
                    [sku]="sku"
                    [reverse]="true"
                    [saContextList]="data?.analytics?.saImpression?.contextList"
                ></supr-oos-info>
            </div>
        </div>

        <supr-product-preview
            [showPreview]="showPreviewModal"
            [sku]="sku"
            [mode]="preferredMode"
            [hideCart]="true"
            [showOosInfo]="!_showOosInfo"
            [saContext]="data?.analytics?.saImpression?.context"
            [saContextList]="data?.analytics?.saImpression?.contextList"
            (handleClose)="closePreviewModal()"
        ></supr-product-preview>
    `,
    styleUrls: ["../supr-v3-tile-variant-a.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuInfoComponent implements OnChanges {
    @Input() sku: Sku;
    @Input() image: V3LayoutImage;
    @Input() preferredMode: string;
    @Input() data: V3LayoutItemData;
    @Input() skuNotified: string;
    @Input() skuAttributes: SkuAttributes;

    showPreviewModal = false;
    _showOosInfo = false;
    _oosUpfront = false;

    constructor(private utilService: UtilService) {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["skuNotified"];
        const changeSkuAttributes = changes["skuAttributes"];
        if (change && !change.firstChange) {
            this.checkShowOosInfo();
        }

        if (changeSkuAttributes && changeSkuAttributes.currentValue) {
            this._oosUpfront = this.utilService.getNestedValue(
                this.skuAttributes,
                "oos_info.oos_upfront",
                false
            );
        }
    }

    closePreviewModal() {
        this.showPreviewModal = false;
    }

    openPreviewModal() {
        this.showPreviewModal = true;
    }

    private checkShowOosInfo() {
        if (!this.skuNotified) {
            this._showOosInfo = false;
            return;
        }

        this._showOosInfo =
            this.skuNotified === "oos" || this.skuNotified === "notify"
                ? true
                : false;
    }
}
