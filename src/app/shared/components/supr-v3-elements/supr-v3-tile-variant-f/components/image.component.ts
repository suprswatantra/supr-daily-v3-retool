import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-f-image",
    template: `
        <supr-image
            [withWrapper]="false"
            [src]="data?.image?.data?.fullUrl"
            [image]="data?.image?.data"
            [imgHeight]="
                data?.image?.dimension?.height ||
                CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT
            "
            [imgWidth]="
                data?.image?.dimension?.width ||
                CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH
            "
            class="tileImage"
        ></supr-image>
    `,
    styleUrls: ["../supr-v3-tile-variant-f.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuImageComponent {
    @Input() data: V3LayoutItemData;

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;
}
