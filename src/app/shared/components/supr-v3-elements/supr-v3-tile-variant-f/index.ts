import { TileComponent } from "./components/tile.component";
import { V3TileVariantF } from "./supr-v3-tile-variant-f.component";
import { V3SkuImageComponent } from "./components/image.component";

export const tileFComponents = [
    TileComponent,
    V3TileVariantF,
    V3SkuImageComponent,
];
