import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-f",
    template: `
        <supr-v3-tile-variant-f-content
            [data]="data"
        ></supr-v3-tile-variant-f-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantF {
    @Input() data: V3LayoutItemData;
}
