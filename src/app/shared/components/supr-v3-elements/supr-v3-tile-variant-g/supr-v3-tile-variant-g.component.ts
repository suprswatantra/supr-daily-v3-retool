import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-g",
    template: `
        <supr-v3-tile-variant-g-content
            [data]="data"
            saImpression
            [saImpressionEnabled]="data?.cta?.analytics?.saImpression"
            [saObjectName]="data?.cta?.analytics?.saImpression?.objectName"
            [saObjectValue]="data?.cta?.analytics?.saImpression?.objectValue"
            [saContext]="data?.cta?.analytics?.saImpression?.context"
            [saContextList]="data?.cta?.analytics?.saImpression?.contextList"
        ></supr-v3-tile-variant-g-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantG {
    @Input() data: V3LayoutItemData;
}
