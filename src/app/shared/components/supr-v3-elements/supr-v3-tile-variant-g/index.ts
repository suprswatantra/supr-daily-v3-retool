import { TileComponent } from "./components/tile.component";
import { V3TileVariantG } from "./supr-v3-tile-variant-g.component";
import { V3SkuImageComponent } from "./components/image.component";

export const tileGComponents = [
    TileComponent,
    V3TileVariantG,
    V3SkuImageComponent,
];
