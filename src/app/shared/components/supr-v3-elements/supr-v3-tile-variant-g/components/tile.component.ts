import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ElementRef,
    ViewChild,
} from "@angular/core";

import { V3LayoutImage, V3LayoutText, V3LayoutItemData } from "@shared/models";
import { HomeLayoutService } from "@services/layout/home-layout.service";
@Component({
    selector: "supr-v3-tile-variant-g-content",
    template: `
        <div
            class="wrapper suprColumn"
            #tile
            [ngStyle]="data?.style"
            (click)="handleClick()"
            saClick
            [saClickedEnabled]="data?.analytics?.saClick"
            [saObjectName]="data?.analytics?.saClick?.objectName"
            [saObjectValue]="data?.analytics?.saClick?.objectValue"
            [saContext]="data?.analytics?.saClick?.context"
            [saContextList]="data?.analytics?.saClick?.contextList"
            [saObjectTag]="data?.analytics?.saClick?.objectTag"
        >
            <div *ngIf="data?.subtitle2" class="cardBtn suprRow center">
                <supr-text
                    [type]="data?.subtitle2?.textType || 'body'"
                    class="subText2"
                    [truncate]="true"
                >
                    {{ data?.subtitle2?.text }}
                </supr-text>
            </div>

            <div class="image" [ngStyle]="data?.image?.style">
                <supr-v3-sku-tile-variant-g-image
                    [data]="data"
                ></supr-v3-sku-tile-variant-g-image>
            </div>
            <div class="divider8"></div>
            <div class="card suprColumn center" style="height: 36px">
                <supr-text
                    *ngIf="data?.title"
                    [type]="data?.title?.textType || 'body'"
                    [truncate]="true"
                    class="title"
                >
                    {{ data?.title?.text }}
                </supr-text>

                <supr-text
                    *ngIf="data?.subtitle1"
                    [type]="data?.subtitle1?.textType || 'body'"
                    [truncate]="true"
                    class="subText1"
                >
                    {{ data?.subtitle1?.text }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-g.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent implements OnInit {
    @Input() data: V3LayoutItemData;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.setStyles();
    }

    handleClick() {
        this.homeLayoutService.handleEvents(this.data);
    }

    private setStyles() {
        this.setImageStyle();
        this.setTitleStyle();
        this.setSubTextStyle();
        this.setSubText2Style();
    }

    private setImageStyle() {
        if (!this.styleExists(this.data.image)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-image-width",
            this.data.image.style.width
        );

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-image-height",
            this.data.image.style.height
        );
    }

    private styleExists(image: V3LayoutImage): boolean {
        return !!(image && image.style);
    }

    private styleTextExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }

    private setTitleStyle() {
        if (!this.styleTextExists(this.data.title)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-title-color",
            this.data.title.style.color
        );
    }

    private setSubTextStyle() {
        if (!this.styleTextExists(this.data.subtitle1)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-subText1-color",
            this.data.subtitle1.style.color
        );
    }

    private setSubText2Style() {
        if (!this.styleTextExists(this.data.subtitle2)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-subText2-color",
            this.data.subtitle2.style.color
        );
    }
}
