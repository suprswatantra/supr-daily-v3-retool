import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-e",
    template: `
        <supr-v3-tile-variant-e-content
            [data]="data"
        ></supr-v3-tile-variant-e-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantE {
    @Input() data: V3LayoutItemData;
}
