import { TileComponent } from "./components/tile.component";
import { V3TileVariantE } from "./supr-v3-tile-variant-e.component";

export const tileEComponents = [TileComponent, V3TileVariantE];
