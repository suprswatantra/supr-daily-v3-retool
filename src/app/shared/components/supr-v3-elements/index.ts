import { v3SkuTileVariantAComponents } from "./supr-v3-sku-tile-variant-a";
import { v3SkuTileVariantBComponents } from "./supr-v3-sku-tile-variant-b";
import { v3SkuTileVariantCComponents } from "./supr-v3-sku-tile-variant-c";

import { tileAComponents } from "./supr-v3-tile-variant-a";
import { tileBComponents } from "./supr-v3-tile-variant-b";
import { tileCComponents } from "./supr-v3-tile-variant-c";
import { tileDComponents } from "./supr-v3-tile-variant-d";
import { tileEComponents } from "./supr-v3-tile-variant-e";
import { tileFComponents } from "./supr-v3-tile-variant-f";
import { tileGComponents } from "./supr-v3-tile-variant-g";

export const v3Elements = [
    ...v3SkuTileVariantAComponents,
    ...v3SkuTileVariantBComponents,
    ...v3SkuTileVariantCComponents,

    ...tileAComponents,
    ...tileBComponents,
    ...tileCComponents,
    ...tileDComponents,
    ...tileEComponents,
    ...tileFComponents,
    ...tileGComponents,
];
