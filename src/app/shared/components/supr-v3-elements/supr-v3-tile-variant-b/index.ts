import { V3TileVariantB } from "./supr-v3-tile-variant-b.component";
import { TileComponent } from "./components/tile.component";

export const tileBComponents = [TileComponent, V3TileVariantB];
