import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { V3LayoutText, V3LayoutItemData } from "@shared/models";
import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-tile-variant-b-content",
    template: `
        <div
            class="suprColumn tile center card"
            #tile
            (click)="handleEvent()"
            saClick
            [saClickedEnabled]="data?.analytics?.saClick"
            [saObjectName]="data?.analytics?.saClick?.objectName"
            [saObjectValue]="data?.analytics?.saClick?.objectValue"
            [saContext]="data?.analytics?.saClick?.context"
            [saContextList]="data?.analytics?.saClick?.contextList"
            [saObjectTag]="data?.analytics?.saClick?.objectTag"
        >
            <div class="tileImage" [ngStyle]="data?.image?.style">
                <supr-image
                    [withWrapper]="true"
                    [src]="data?.image?.data?.fullUrl"
                    [image]="data?.image?.data"
                    [imgHeight]="
                        data?.image?.dimension?.height ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT
                    "
                    [imgWidth]="
                        data?.image?.dimension?.width ||
                        CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH
                    "
                ></supr-image>
            </div>

            <div class="spacer8"></div>
            <div class="tileText" [ngStyle]="data?.title?.style">
                <supr-text [type]="data?.title?.textType || 'subtext10'">
                    {{ data?.title?.text }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-b.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent implements OnInit {
    @Input() data: V3LayoutItemData;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.setStyles();
    }

    handleEvent() {
        this.homeLayoutService.handleEvents(this.data);
    }

    private setStyles() {
        this.setTitleStyle();
    }

    private setTitleStyle() {
        if (!this.styleExists(this.data.title)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-v3-tile-title-color",
            this.data.title.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }
}
