import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData } from "@shared/models";

@Component({
    selector: "supr-v3-tile-variant-b",
    template: `
        <supr-v3-tile-variant-b-content
            [data]="data"
        ></supr-v3-tile-variant-b-content>
    `,
    styleUrls: ["./supr-v3-tile-variant-b.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3TileVariantB {
    @Input() data: V3LayoutItemData;
}
