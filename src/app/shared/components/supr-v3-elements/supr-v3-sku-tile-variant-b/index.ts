import { V3SkuTileVariantB } from "./supr-v3-sku-tile-variant-b.component";

import { V3SkuImageComponent } from "./components/image.component";
import { V3SkuDetailsComponent } from "./components/details.component";
import { V3SkuInfoComponent } from "./components/info.component";
import { V3SkuTileVariantBContainer } from "./supr-v3-sku-tile-variant-b-container";

export const v3SkuTileVariantBComponents = [
    V3SkuTileVariantBContainer,
    V3SkuTileVariantB,
    V3SkuDetailsComponent,
    V3SkuImageComponent,
    V3SkuInfoComponent,
];
