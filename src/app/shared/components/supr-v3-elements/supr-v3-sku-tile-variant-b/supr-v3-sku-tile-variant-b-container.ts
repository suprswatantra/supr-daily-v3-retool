import { Component, OnInit, Input } from "@angular/core";

import { Observable } from "rxjs";

import { Sku, SkuAttributes, V3LayoutItemData } from "@models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { SkuAdapter } from "@shared/adapters/sku.adapter";

@Component({
    selector: "supr-v3-sku-tile-variant-b-container",
    template: `
        <ng-container *ngIf="sku$ | async">
            <supr-v3-sku-tile-variant-b
                [data]="data"
                [sku]="sku$ | async"
                [oosUpfront]="oosUpfront"
                [skuNotified]="skuNotified$ | async"
            ></supr-v3-sku-tile-variant-b>
        </ng-container>
    `,
})
export class V3SkuTileVariantBContainer implements OnInit {
    @Input() data: V3LayoutItemData;

    sku$: Observable<Sku>;
    skuNotified$: Observable<string>;
    skuAttributes$: Observable<SkuAttributes>;

    oosUpfront = false;

    constructor(private adapter: Adapter, private skuAdapter: SkuAdapter) {}

    ngOnInit() {
        if (this.data.parent_sku_id) {
            this.skuAttributes$ = this.skuAdapter.getSkuAttributes(
                this.data.parent_sku_id
            );
        }
        this.sku$ = this.adapter.getSkuById(this.data.entityId);
        this.skuNotified$ = this.skuAdapter.isNotified(this.data.entityId);
    }
}
