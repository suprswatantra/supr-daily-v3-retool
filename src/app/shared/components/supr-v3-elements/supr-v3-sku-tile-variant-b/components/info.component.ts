import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { Sku, V3LayoutImage, V3LayoutItemData } from "@models";

@Component({
    selector: "supr-v3-sku-tile-variant-b-info",
    template: `
        <div class="suprColumn top left">
            <div
                class="suprColumn center tileImage"
                [ngStyle]="image?.style"
                (click)="openPreviewModal()"
                [class.overlay50]="showOosInfo"
            >
                <supr-v3-sku-tile-variant-b-image
                    [skuImageUrl]="sku?.image?.fullUrl"
                    [compressedImage]="sku?.image"
                ></supr-v3-sku-tile-variant-b-image>
            </div>
            <div class="spacer12"></div>
            <div class="suprColumn left icon">
                <div class="btnIcon">
                    <supr-add-button
                        *ngIf="!showOosInfo"
                        [sku]="sku"
                        [mode]="preferredMode"
                        [showRoundAddBtn]="true"
                        [isOosUpfront]="oosUpfront"
                        [saContext]="data?.analytics?.saImpression?.context"
                        [saContextList]="
                            data?.analytics?.saImpression?.contextList
                        "
                    ></supr-add-button>

                    <supr-oos-info
                        *ngIf="showOosInfo"
                        [showNotifyBtn]="true"
                        [showOosBtn]="false"
                        [sku]="sku"
                        [saContext]="data?.analytics?.saImpression?.context"
                        [saContextList]="
                            data?.analytics?.saImpression?.contextList
                        "
                    ></supr-oos-info>
                </div>
            </div>

            <div class="suprColumn left">
                <div class="divider12"></div>
                <supr-v3-sku-tile-b-details
                    [class.overlay40]="showOosInfo"
                    [sku]="sku"
                    [showOosInfo]="showOosInfo"
                    (click)="openPreviewModal()"
                ></supr-v3-sku-tile-b-details>
                <div class="divider4"></div>
                <supr-oos-info
                    *ngIf="showOosInfo"
                    [showOosBtn]="true"
                    [showNotifyBtn]="false"
                    [sku]="sku"
                    [saContext]="data?.analytics?.saImpression?.context"
                    [saContextList]="data?.analytics?.saImpression?.contextList"
                ></supr-oos-info>
            </div>

            <supr-product-preview
                [showPreview]="showPreviewModal"
                [sku]="sku"
                [mode]="preferredMode"
                [hideCart]="true"
                [showOosInfo]="!showOosInfo"
                [saContext]="data?.analytics?.saImpression?.context"
                [saContextList]="data?.analytics?.saImpression?.contextList"
                (handleClose)="closePreviewModal()"
            ></supr-product-preview>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-b.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuInfoComponent {
    @Input() sku: Sku;
    @Input() image: V3LayoutImage;
    @Input() preferredMode: string;
    @Input() data: V3LayoutItemData;
    @Input() showOosInfo: boolean;
    @Input() skuNotified: string;
    @Input() oosUpfront: boolean;

    showPreviewModal = false;

    closePreviewModal() {
        this.showPreviewModal = false;
    }

    openPreviewModal() {
        this.showPreviewModal = true;
    }
}
