import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
    OnInit,
} from "@angular/core";
import { V3LayoutItemData, Sku } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-b",
    template: `
        <div class="suprRow tile top card" *ngIf="data" [ngStyle]="data?.style">
            <supr-v3-sku-tile-variant-b-info
                class="suprColumn left grow"
                [sku]="sku"
                [image]="data?.image"
                [preferredMode]="data?.preferredMode"
                [data]="data"
                [oosUpfront]="oosUpfront"
                [showOosInfo]="_showOosInfo"
                [skuNotified]="skuNotified"
            ></supr-v3-sku-tile-variant-b-info>
        </div>
    `,
    styleUrls: ["./supr-v3-tile-variant-b.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuTileVariantB implements OnChanges, OnInit {
    @Input() data: V3LayoutItemData;
    @Input() sku: Sku;
    @Input() skuNotified: string;
    @Input() oosUpfront: boolean;

    _showOosInfo = false;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["skuNotified"];
        if (change && !change.firstChange) {
            this.checkShowOosInfo();
        }
    }

    ngOnInit() {
        this.checkShowOosInfo();
    }

    private checkShowOosInfo() {
        if (!this.skuNotified) {
            this._showOosInfo = false;
            return;
        }

        this._showOosInfo =
            this.skuNotified === "oos" || this.skuNotified === "notify"
                ? true
                : false;
    }
}
