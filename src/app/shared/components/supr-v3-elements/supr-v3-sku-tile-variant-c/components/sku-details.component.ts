import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";
import { QuantityService } from "@services/util/quantity.service";
import { Sku, V3LayoutItemData, V3LayoutText } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-c-sku-details",
    template: `
        <div class="skuDetails" #skuDetails>
            <div>
                <div class="suprRow top spaceBetween">
                    <supr-text type="paragraph" [truncate]="true" class="name">
                        {{ sku.sku_name }}
                    </supr-text>

                    <div class="spacer4"></div>

                    <div class="price">
                        <div class="suprRow" *ngIf="!showOosInfo">
                            <ng-container *ngIf="showPriceStrike">
                                <supr-text class="priceStrike" type="caption">
                                    {{ sku.unit_mrp | rupee: 2 }}
                                </supr-text>
                                <div class="spacer4"></div>
                            </ng-container>
                            <supr-text type="body" class="price">
                                {{ sku.unit_price | rupee: 2 }}
                            </supr-text>
                        </div>

                        <supr-v3-sku-tile-variant-c-btn
                            *ngIf="showOosInfo"
                            [showOosInfo]="showOosInfo"
                            [sku]="sku"
                            [data]="data"
                            [showOosBtn]="true"
                            [showNotifyBtn]="false"
                        ></supr-v3-sku-tile-variant-c-btn>
                    </div>
                </div>
                <div class="divider4"></div>
                <div class="suprRow spaceBetween bottom">
                    <supr-text type="subtext10" class="qty">
                        {{ getQty() }}
                    </supr-text>

                    <supr-v3-sku-tile-variant-c-btn
                        [showOosInfo]="showOosInfo"
                        [sku]="sku"
                        [data]="data"
                        [showOosBtn]="false"
                        [showNotifyBtn]="true"
                    ></supr-v3-sku-tile-variant-c-btn>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuDetailsComponent implements OnInit {
    @Input() sku: Sku;
    @Input() data: V3LayoutItemData;
    @Input() showOosInfo: boolean;

    @ViewChild("skuDetails", { static: true }) skuDetailsEl: ElementRef;

    showPriceStrike = true;

    constructor(private qtyService: QuantityService) {}

    ngOnInit() {
        this.setStyles();
    }

    getQty() {
        return this.qtyService.toText(this.sku);
    }

    private setStyles() {
        this.setImageStyle();
    }

    private setImageStyle() {
        if (!this.styleExists(this.data.title)) {
            return;
        }

        this.skuDetailsEl.nativeElement.style.setProperty(
            "--supr-sku-item-the-day-color",
            this.data.title.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }
}
