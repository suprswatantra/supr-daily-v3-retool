import {
    Component,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import { V3LayoutItemData, Sku, V3LayoutText } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-wrapper",
    template: `
        <div class="wrapper" [class.overlay]="_showOosInfo">
            <div class="wrapperImage" #image [ngStyle]="data?.image?.style">
                <supr-v3-sku-tile-variant-c-image
                    [skuImageUrl]="sku?.image?.fullUrl"
                    [compressedImage]="sku?.image"
                >
                </supr-v3-sku-tile-variant-c-image>
            </div>

            <div class="wrapperContent" #wrapperContent>
                <supr-v3-sku-tile-variant-c-content
                    [data]="data"
                    [sku]="sku"
                    [showOosInfo]="_showOosInfo"
                ></supr-v3-sku-tile-variant-c-content>
            </div>

            <supr-product-preview
                [showPreview]="showPreviewModal"
                [sku]="sku"
                [hideCart]="true"
                [showOosInfo]="_showOosInfo"
                [saContext]="data?.analytics?.saImpression?.context"
                [saContextList]="data?.analytics?.saImpression?.contextList"
                (handleClose)="closePreviewModal()"
            ></supr-product-preview>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
})
export class V3SkuWrapperComponent implements OnInit, OnChanges {
    @Input() data: V3LayoutItemData;
    @Input() sku: Sku;
    @Input() skuNotified: string;

    @ViewChild("wrapperContent", { static: true }) tileEl: ElementRef;
    @ViewChild("image", { static: true }) imageTileEl: ElementRef;

    showPreviewModal = false;
    _showOosInfo = false;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["skuNotified"];
        if (change && !change.firstChange) {
            this.checkShowOosInfo();
        }
    }

    ngOnInit() {
        this.setStyles();
        this.checkShowOosInfo();
    }

    private setStyles() {
        this.setWrapperStyle();
        this.setImageStyle();
    }

    private setWrapperStyle() {
        if (!this.styleExists(this.data)) {
            return;
        }

        this.tileEl.nativeElement.style.setProperty(
            "--supr-sku-tile-bg-color",
            this.data.style["background-color"]
        );
    }

    private setImageStyle() {
        if (!this.imageStyleExists(this.data.title)) {
            return;
        }

        this.imageTileEl.nativeElement.style.setProperty(
            "--supr-sku-item-the-day-color",
            this.data.title.style.color
        );
    }

    private styleExists(data: V3LayoutItemData): boolean {
        return !!(data && data.style);
    }

    private imageStyleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }

    closePreviewModal() {
        this.showPreviewModal = false;
    }

    openPreviewModal() {
        this.showPreviewModal = true;
    }

    private checkShowOosInfo() {
        if (!this.skuNotified) {
            this._showOosInfo = false;
            return;
        }

        this._showOosInfo =
            this.skuNotified === "oos" || this.skuNotified === "notify"
                ? true
                : false;
    }
}
