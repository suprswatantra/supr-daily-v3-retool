import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ElementRef,
    ViewChild,
    OnInit,
} from "@angular/core";

import { HomeLayoutService } from "@services/layout/home-layout.service";
import { UtilService } from "@services/util/util.service";

import { V3LayoutItemData, Sku, V3LayoutText } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-c-content",
    template: `
        <div class="wrapperContentDetails" #content>
            <supr-text type="montserratExtraBold16" class="title">
                {{ data?.title.text }}
            </supr-text>
            <div class="divider8"></div>
            <div class="suprRow">
                <div class="wrapperContentDetailsOff">
                    <supr-text type="subtext10" class="off">
                        {{ getOffPercentage() }}
                    </supr-text>
                </div>

                <div class="spacer8"></div>

                <supr-text
                    type="subtext10"
                    class="urgency"
                    *ngIf="data?.subtitle2"
                >
                    {{ data.subtitle2.text }}
                </supr-text>
            </div>
            <div class="divider8"></div>
            <supr-v3-sku-tile-variant-c-sku-details
                [sku]="sku"
                [data]="data"
                [showOosInfo]="showOosInfo"
            ></supr-v3-sku-tile-variant-c-sku-details>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuContentComponent implements OnInit {
    @Input() data: V3LayoutItemData;
    @Input() sku: Sku;
    @Input() showOosInfo: boolean;

    @ViewChild("content", { static: true }) contentEl: ElementRef;

    displayQty = "";

    constructor(
        private homeLayoutService: HomeLayoutService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.setStyles();
    }

    getOffPercentage() {
        const subText1 = this.utilService.getNestedValue(
            this.data,
            "sub_text_1",
            null
        );

        if (subText1) {
            return subText1;
        }

        return this.homeLayoutService.calculatePercentage(this.sku) + "% Off";
    }

    private setStyles() {
        this.setImageStyle();
    }

    private setImageStyle() {
        if (!this.styleExists(this.data.title)) {
            return;
        }

        this.contentEl.nativeElement.style.setProperty(
            "--supr-sku-item-the-day-color",
            this.data.title.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }
}
