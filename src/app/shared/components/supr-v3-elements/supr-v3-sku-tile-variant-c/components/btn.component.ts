import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { SKU_PREFERRED_MODE } from "@constants";
import { Sku, V3LayoutItemData, V3LayoutText } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-c-btn",
    template: `
        <div class="skuBtn" #btn>
            <supr-add-button
                *ngIf="
                    !showOosInfo &&
                    data?.preferredMode !==
                        '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                "
                [sku]="sku"
                [mode]="data?.preferredMode"
                [saContext]="data?.analytics?.saImpression?.context"
                [saContextList]="data?.analytics?.saImpression?.contextList"
            ></supr-add-button>

            <supr-subscribe-button
                *ngIf="
                    !showOosInfo &&
                    data?.preferredMode ===
                        '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                "
                [sku]="sku"
                [mode]="data?.preferredMode"
                [saContext]="data?.analytics?.saImpression?.context"
                [saContextList]="data?.analytics?.saImpression?.contextList"
            >
            </supr-subscribe-button>

            <supr-oos-info
                *ngIf="showOosInfo"
                [sku]="sku"
                [reverse]="true"
                [showOosBtn]="showOosBtn"
                [showNotifyBtn]="showNotifyBtn"
            ></supr-oos-info>
        </div>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuBtnComponent implements OnInit {
    @Input() sku: Sku;
    @Input() data: V3LayoutItemData;
    @Input() showOosInfo: boolean;
    @Input() showOosBtn: boolean;
    @Input() showNotifyBtn: boolean;

    @ViewChild("btn", { static: true }) btnEl: ElementRef;

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        this.setImageStyle();
    }

    private setImageStyle() {
        if (!this.styleExists(this.data.title)) {
            return;
        }

        this.btnEl.nativeElement.style.setProperty(
            "--supr-sku-item-the-day-color",
            this.data.title.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }
}
