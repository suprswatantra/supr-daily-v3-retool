import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { Image } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-c-image",
    template: `
        <supr-image
            [withWrapper]="true"
            [src]="skuImageUrl"
            [image]="compressedImage"
            [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
            [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
            class="wrapperImageImg"
        ></supr-image>
    `,
    styleUrls: ["../supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuImageComponent {
    @Input() skuImageUrl: string;
    @Input() compressedImage: Image;
}
