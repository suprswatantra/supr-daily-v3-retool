import { Component, OnInit, Input } from "@angular/core";

import { Observable } from "rxjs";

import { Sku, V3LayoutItemData } from "@models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { SkuAdapter } from "@shared/adapters/sku.adapter";

@Component({
    selector: "supr-v3-sku-tile-variant-c-container",
    template: `
        <ng-container *ngIf="sku$ | async">
            <supr-v3-sku-tile-variant-c
                [data]="data"
                [sku]="sku$ | async"
                [skuNotified]="skuNotified$ | async"
            ></supr-v3-sku-tile-variant-c>
        </ng-container>
    `,
})
export class V3SkuTileVariantCContainer implements OnInit {
    @Input() data: V3LayoutItemData;

    sku$: Observable<Sku>;
    skuNotified$: Observable<string>;

    constructor(private adapter: Adapter, private skuAdapter: SkuAdapter) {}

    ngOnInit() {
        // this.sku$ = this.adapter.getSkuById(347);
        // this.skuNotified$ = this.skuAdapter.isNotified(347);

        this.sku$ = this.adapter.getSkuById(this.data.entityId);
        this.skuNotified$ = this.skuAdapter.isNotified(this.data.entityId);
    }
}
