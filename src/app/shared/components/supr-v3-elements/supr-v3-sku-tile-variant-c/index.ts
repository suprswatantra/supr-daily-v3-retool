import { V3SkuTileVariantC } from "./supr-v3-sku-tile-variant-c.component";

import { V3SkuWrapperComponent } from "./components/wrapper.component";
import { V3SkuImageComponent } from "./components/image.component";
import { V3SkuContentComponent } from "./components/content.component";
import { V3SkuDetailsComponent } from "./components/sku-details.component";
import { V3SkuTileVariantCContainer } from "./supr-v3-sku-tile-variant-c-container";
import { V3SkuBtnComponent } from "./components/btn.component";

export const v3SkuTileVariantCComponents = [
    V3SkuTileVariantCContainer,
    V3SkuTileVariantC,
    V3SkuWrapperComponent,
    V3SkuImageComponent,
    V3SkuContentComponent,
    V3SkuDetailsComponent,
    V3SkuBtnComponent,
];
