import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3LayoutItemData, Sku } from "@shared/models";

@Component({
    selector: "supr-v3-sku-tile-variant-c",
    template: `
        <div class="suprRow tile top card" *ngIf="data">
            <supr-v3-sku-tile-wrapper
                [data]="data"
                [sku]="sku"
                [skuNotified]="skuNotified"
            >
            </supr-v3-sku-tile-wrapper>
        </div>
    `,
    styleUrls: ["./supr-v3-tile-variant-c.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3SkuTileVariantC {
    @Input() data: V3LayoutItemData;
    @Input() sku: Sku;
    @Input() skuNotified: string;
}
