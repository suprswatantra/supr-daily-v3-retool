import {
    Input,
    OnInit,
    OnChanges,
    Component,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { MODAL_NAMES, ORDER_FEEDBACK, CLODUINARY_IMAGE_SIZE } from "@constants";

import { Sku, Order, Subscription, Feedback } from "@models";

import { UtilService } from "@services/util/util.service";
import { OrderService } from "@services/shared/order.service";
import { RouterService } from "@services/util/router.service";
import { QuantityService } from "@services/util/quantity.service";
import { FeedbackService } from "@services/layout/feedback.service";

import { OrderFeedbackSetting } from "@types";

import { SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";

@Component({
    selector: "supr-order-history-product-tile",
    templateUrl: "./supr-order-history-product-tile.component.html",
    styleUrls: ["./supr-order-history-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderHistoryProductTileComponent implements OnChanges, OnInit {
    comment = "";
    params: any = [];
    statusIcon: string;
    statusType: string;
    statusText: string;
    deliveredQtyText: string;
    scheduledQtyText: string;
    showCommentModal = false;
    showRefundDetailsModal = false;
    isOrderTooltipRequired: boolean;
    orderFeedbackSubmittedResponse = "";
    order_feedback: OrderFeedbackSetting.Nudge;
    _orderFeedbackRequest: Feedback.OrderFeedback[];
    orderFeedbackResponse: Feedback.FeedbackResponse;

    saObjectValue: number;
    refundTooltipSaContext: string;

    readonly FEEDBACK_COMMENT = MODAL_NAMES.FEEDBACK_COMMENT;
    readonly CANCELLATION_INFO = MODAL_NAMES.CANCELLATION_INFO;
    readonly saObjectName = SA_OBJECT_NAMES.CLICK.PRODUCT_TILE;
    readonly refundTooltipObjectName = SA_OBJECT_NAMES.CLICK.REFUND_TOOLTIP;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    @Input() sku: Sku;
    @Input() date: string;
    @Input() order: Order;
    @Input() subscription: Subscription;
    @Input()
    set orderFeedback(orderFeedback: any) {
        if (orderFeedback) {
            this._orderFeedbackRequest = orderFeedback;
            this.setParams(orderFeedback);
        }
    }
    get orderFeedback() {
        return this._orderFeedbackRequest;
    }

    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    constructor(
        private orderService: OrderService,
        private quantityService: QuantityService,
        private feedbackService: FeedbackService,
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.order_feedback = this.feedbackService.getOrderFeedbackSettings();
    }

    ngOnChanges() {
        this.setStatusInfo();
        this.setQuantityText();
        this.setAnalyticsData();
    }

    onSubmit(data: any) {
        this.orderFeedbackResponse = data;
        const { feedbackOrderResponse } = this.orderFeedbackResponse;
        const { feedback_request_id } = this.orderFeedback;
        this.feedbackService.submitOrderFeedbackResponse({
            ...feedbackOrderResponse,
            feedback_request_id,
            comment: this.comment,
        });
    }

    handleCommentModal({ toggle, comment }) {
        this.showCommentModal = toggle;
        this.comment = comment;
        if (!toggle && comment) {
            this.onSubmit(this.orderFeedbackResponse);
        }
    }

    closeCommentModal() {
        this.showCommentModal = false;
    }

    contactSupport() {
        this.routerService.goToSupportPage();
    }

    openRefundDetailsModal() {
        this.showRefundDetailsModal = true;
    }

    closeRefundDetailsModal() {
        this.showRefundDetailsModal = false;
    }

    private setStatusInfo() {
        if (this.order) {
            const {
                statusText,
                statusType,
                statusIcon,
            } = this.orderService.getOrderItemStatus(this.order);

            this.statusType = statusType;
            this.statusText = statusText;
            this.statusIcon = statusIcon;
            this.isOrderTooltipRequired = this.orderService.isOrderTooltipRequired(
                this.order
            );
        }
    }

    private setQuantityText() {
        if (this.sku && this.order) {
            this.deliveredQtyText = this.quantityService.toText(
                this.sku,
                this.order.delivered_quantity
            );

            this.scheduledQtyText =
                /* only show scheduled quantity text when order is in a refund state */
                this.isOrderTooltipRequired &&
                this.orderService.getOrderHistoryTileScheduledQtyText(
                    this.sku,
                    this.order
                );
        }
    }

    private setAnalyticsData() {
        if (this.sku) {
            this.saObjectValue = this.sku.id;
        }

        this.refundTooltipSaContext = this.orderService.getRefundTooltipSaContext(
            this.order
        );
    }

    private setParams(orderFeedback: any) {
        if (!this.utilService.isEmpty(orderFeedback)) {
            const { feedbackAction, isRequest, isResponse } = orderFeedback;
            if (isRequest && feedbackAction && feedbackAction.params) {
                this.params = feedbackAction.params;
            } else if (
                isResponse &&
                !this.utilService.isEmpty(feedbackAction)
            ) {
                const orderFeedbackSubmittedResponse =
                    feedbackAction.feedback_value === 1
                        ? ORDER_FEEDBACK.thumbsUp
                        : ORDER_FEEDBACK.thumbsDown;
                this.orderFeedbackSubmittedResponse = orderFeedbackSubmittedResponse;
            }
        }
    }
}
