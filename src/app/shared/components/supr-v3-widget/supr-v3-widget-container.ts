import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { V3Layout } from "@shared/models";

@Component({
    selector: "supr-v3-widget-container",
    template: `
        <supr-v3-widget-manager [data]="layout"></supr-v3-widget-manager>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3WidgetContainer {
    @Input() layout: V3Layout;
}
