import {
    Component,
    ChangeDetectionStrategy,
    ViewChild,
    Input,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import { ComponentService } from "@services/layout/component.service";

import { AddComponentDirective } from "@shared/directives/supr-component-insertion/supr-component-insertion.directive";
import { V3Layout } from "@shared/models";

@Component({
    selector: "supr-v3-widget-manager",
    template: ` <ng-template saAddComponent></ng-template> `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3WidgetManager implements OnChanges {
    @ViewChild(AddComponentDirective, { static: true })
    addComponent: AddComponentDirective;

    @Input() data: V3Layout;

    constructor(private componentService: ComponentService) {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["data"];

        if (change.currentValue) {
            this.render(change.currentValue);
        }
    }

    private render(data: V3Layout) {
        this.componentService.loadComponent(
            data.widgetType,
            data.data,
            this.addComponent
        );
    }
}
