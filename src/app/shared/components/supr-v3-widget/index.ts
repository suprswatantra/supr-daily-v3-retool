import { V3WidgetManager } from "./supr-v3-widget.component";
import { V3WidgetContainer } from "./supr-v3-widget-container";

export const v3Widgets = [V3WidgetManager, V3WidgetContainer];
