import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Observable } from "rxjs";

import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import { ScheduleItem, Subscription, Sku } from "@models";

import { Segment } from "@types";

@Component({
    selector: "supr-schedule-subscription-container",
    template: `
        <supr-schedule-product-tile
            [date]="date"
            [paused]="paused"
            [editable]="editable"
            [schedule]="subscription"
            [statusText]="statusText"
            [sku]="skuDetails$ | async"
            [showFrequency]="showFrequency"
            [saObjectValueTile]="saObjectValueTile"
            [subscription]="subscriptionDetails$ | async"
            [saContext]="saContext"
            [saContextList]="saContextList"
            [isSuprPassActive]="isSuprPassActive$ | async"
            (updateActionListener)="updateActionListener.emit($event)"
        ></supr-schedule-product-tile>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleSubscriptionContainer implements OnInit {
    saObjectValueTile: number;
    skuDetails$: Observable<Sku>;
    subscriptionDetails$: Observable<Subscription>;
    isSuprPassActive$: Observable<boolean>;

    @Input() date: string;
    @Input() paused: boolean;
    @Input() editable = true;
    @Input() saContext?: string;
    @Input() statusText?: string;
    @Input() showFrequency?: boolean;
    @Input() subscriptionView = false;
    @Input() subscription: ScheduleItem;
    @Input() saContextList: Segment.ContextListItem[] = [];

    @Output()
    updateActionListener: EventEmitter<string> = new EventEmitter();

    constructor(private subscriptionAdapter: SubscriptionAdapter) {}

    ngOnInit() {
        this.subscriptionDetails$ = this.subscriptionAdapter.getSubscriptionDetails(
            this.subscription.id
        );

        this.skuDetails$ = this.subscriptionAdapter.getSkuDetailsById(
            this.subscription.sku_id
        );

        this.isSuprPassActive$ = this.subscriptionAdapter.isSuprPassActive$;

        this.setAnalyticsData();
    }

    setAnalyticsData() {
        if (this.subscriptionView) {
            this.saObjectValueTile = this.subscription.id;
        } else {
            this.saObjectValueTile = this.subscription.sku_id;
        }
    }
}
