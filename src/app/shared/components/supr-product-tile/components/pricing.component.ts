import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { Sku } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import {
    ACCESS_PRICE_TEXTS,
    PRICE_TYPES,
    PRICING_TYPE,
    EXCLUSIVE_PRICE_TEXTS,
} from "../constants";
import { ANALYTICS_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-product-tile-pricing",
    template: `
        <div class="details detailsPricing">
            <ng-container *ngIf="isPricingTypeExclusive; else notExlusive">
                <!-- Exclusive Price  -->
                <div class="suprRow spaceBetween">
                    <div class="suprRow">
                        <!-- Show Unit Price, for pricing type "exclusive" unitPrice is equal to hub_sku_unit_price -->
                        <supr-text class="detailsPrice" type="body">
                            {{ sku?.unit_price | rupee: 2 }}
                        </supr-text>

                        <ng-container *ngIf="showPriceStrike">
                            <div class="spacer8"></div>
                            <supr-text
                                class="detailsPriceStrike"
                                type="caption"
                            >
                                {{ sku?.unit_mrp | rupee: 2 }}
                            </supr-text>
                        </ng-container>

                        <div class="spacer8"></div>
                        <supr-text type="caption" class="lightText">
                            ${EXCLUSIVE_PRICE_TEXTS.EXCLUSIVE_PRICE}
                        </supr-text>
                    </div>
                </div>
            </ng-container>
            <ng-template #notExlusive>
                <!-- Supr Pass Member pricing -->
                <ng-container *ngIf="isSuprPassActive; else nonSuprPassMember">
                    <div class="suprRow spaceBetween">
                        <div class="suprRow">
                            <!-- Show Unit Price, for access members unitPrice is equal to accessPrice -->
                            <supr-text class="detailsPrice" type="body">
                                {{ sku?.unit_price | rupee: 2 }}
                            </supr-text>

                            <!-- Access Price unlocked -->
                            <ng-container
                                *ngIf="
                                    isAccessPriceUnlocked;
                                    else accessPriceNotUnlocked
                                "
                            >
                                <div class="spacer8"></div>

                                <supr-icon
                                    name="unlocked"
                                    class="unlock"
                                ></supr-icon>

                                <div class="spacer8"></div>

                                <supr-text type="caption" class="lightText">
                                    ${ACCESS_PRICE_TEXTS.ACCESS_PRICE_UNLOCKED}
                                </supr-text>
                            </ng-container>

                            <!-- Switch back to old UI if access price cannot make a difference -->
                            <ng-template #accessPriceNotUnlocked>
                                <ng-container *ngIf="showPriceStrike">
                                    <div class="spacer8"></div>
                                    <supr-text
                                        class="detailsPriceStrike"
                                        type="caption"
                                    >
                                        {{ sku?.unit_mrp | rupee: 2 }}
                                    </supr-text>
                                </ng-container>
                            </ng-template>
                        </div>
                    </div>

                    <!-- Shows non access price, only if access price is unlocked for this sku -->
                    <ng-container *ngIf="isAccessPriceUnlocked">
                        <div class="divider4"></div>
                        <div
                            class="suprRow spaceBetween"
                            saImpression
                            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                                .ACCESS_PRICE}"
                        >
                            <div class="suprRow">
                                <supr-text class="lightText" type="body">
                                    {{ sku?.non_access_price | rupee: 2 }}
                                </supr-text>
                                <ng-container *ngIf="showPriceStrike">
                                    <div class="spacer8"></div>
                                    <supr-text
                                        class="detailsPriceStrike"
                                        type="caption"
                                    >
                                        {{ sku?.unit_mrp | rupee: 2 }}
                                    </supr-text>
                                    <div class="spacer8"></div>
                                    <supr-text type="caption" class="lightText">
                                        ${ACCESS_PRICE_TEXTS.SUPR_PRICE}
                                    </supr-text>
                                </ng-container>
                            </div>
                        </div>
                    </ng-container>
                </ng-container>

                <!-- Non Supr Pass member pricing -->
                <ng-template #nonSuprPassMember>
                    <div class="suprRow spaceBetween">
                        <div class="suprRow">
                            <!-- Show Unit Price, for non access members unitPrice can be more or equal to access price -->
                            <supr-text class="detailsPrice" type="body">
                                {{ sku?.unit_price | rupee: 2 }}
                            </supr-text>

                            <ng-container *ngIf="showPriceStrike">
                                <div class="spacer8"></div>
                                <supr-text
                                    class="detailsPriceStrike"
                                    type="caption"
                                >
                                    {{ sku?.unit_mrp | rupee: 2 }}
                                </supr-text>
                            </ng-container>
                        </div>
                    </div>

                    <!-- Show Access price benefits, if applicable -->
                    <ng-container *ngIf="showAccessPrice">
                        <div class="divider4"></div>
                        <div
                            class="suprRow"
                            saImpression
                            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                                .ACCESS_PRICE}"
                        >
                            <supr-text class="lightText" type="body">
                                {{ sku?.access_price | rupee: 2 }}
                            </supr-text>

                            <div class="spacer8"></div>

                            <div
                                class="suprRow"
                                (click)="goToSuprPassPage($event)"
                                saClick
                                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                    .SUPR_PASS}"
                            >
                                <supr-text type="caption" class="lightText">
                                    ${ACCESS_PRICE_TEXTS.ACCESS_PRICE_LOCKED}
                                </supr-text>

                                <div class="spacer4"></div>

                                <supr-icon
                                    name="locked"
                                    class="lock"
                                ></supr-icon>
                                <supr-icon
                                    name="chevron_right"
                                    class="chevron"
                                ></supr-icon>
                            </div>
                        </div>
                    </ng-container>
                </ng-template>
            </ng-template>
        </div>
    `,
    styleUrls: ["../supr-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTilePricingComponent implements OnInit {
    @Input() sku: Sku;
    @Input() isSuprPassActive: boolean;

    showPriceStrike = false;
    showAccessPrice = false;
    isAccessPriceUnlocked = false;

    /**
     * If pricing type is "exclusive hide lock and show text -> exclusive price;
     * instead of non_access_price show hub_sku_unit_price;
     * */
    isPricingTypeExclusive = false;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.initialize();
    }

    goToSuprPassPage(event: TouchEvent) {
        if (event) {
            event.stopPropagation();
        }

        this.routerService.goToSuprPassPage();
    }

    /**
     * Gets all the Pricing informations and decides whether to show access benefits.
     * IMP: Price of a SKU can be 0 [Some offer], thats why all the checks are based on null
     *
     * @private
     * @returns
     * @memberof ProductTilePricingComponent
     */
    private initialize() {
        if (!this.sku) {
            return;
        }

        // Get all prices
        const unitPrice = this.getPrice(PRICE_TYPES.UNIT_PRICE);
        const unitMrp = this.getPrice(PRICE_TYPES.UNIT_MRP);
        const accessPrice = this.getPrice(PRICE_TYPES.ACCESS_PRICE);
        const nonAccessPrice = this.getPrice(PRICE_TYPES.NON_ACCESS_PRICE);

        const pricingType = this.getPricingType(PRICING_TYPE);

        if (EXCLUSIVE_PRICE_TEXTS.EXCLUSIVE === pricingType) {
            this.setPricingTypeExclusive();
        }

        if (unitPrice !== null && unitMrp !== null) {
            this.showPriceStrike = unitPrice < unitMrp;
        }

        if (accessPrice !== null && unitPrice !== null) {
            this.showAccessPrice = accessPrice < unitPrice;
        }

        if (
            accessPrice !== null &&
            unitPrice !== null &&
            nonAccessPrice !== null
        ) {
            this.isAccessPriceUnlocked =
                accessPrice === unitPrice &&
                accessPrice < nonAccessPrice &&
                nonAccessPrice < unitMrp;
        }
    }

    private getPrice(priceType: string) {
        return this.utilService.getNestedValue(this.sku, priceType, null);
    }

    private getPricingType(pricingType: string) {
        return this.utilService.getNestedValue(this.sku, pricingType, null);
    }

    private setPricingTypeExclusive() {
        this.isPricingTypeExclusive = true;
    }
}
