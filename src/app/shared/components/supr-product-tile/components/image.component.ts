import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { Image } from "@shared/models";

@Component({
    selector: "supr-product-tile-image",
    template: `
        <div>
            <supr-image
                [withWrapper]="false"
                [src]="skuImageUrl"
                [image]="compressedImage"
                [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
                [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
            ></supr-image>
        </div>
    `,
    styleUrls: ["../supr-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageComponent {
    @Input() skuImageUrl: string;
    @Input() compressedImage: Image;
}
