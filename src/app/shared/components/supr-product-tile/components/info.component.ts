import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { Sku } from "@models";

@Component({
    selector: "supr-product-tile-info",
    template: `
        <div class="suprRow top">
            <div class="tileImage">
                <supr-product-tile-image
                    [skuImageUrl]="sku?.image?.fullUrl"
                    [compressedImage]="sku?.image"
                ></supr-product-tile-image>
            </div>

            <div class="suprColumn stretch top left">
                <supr-product-tile-details
                    [sku]="sku"
                ></supr-product-tile-details>
            </div>
        </div>
    `,
    styleUrls: ["../supr-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent {
    @Input() sku: Sku;
}
