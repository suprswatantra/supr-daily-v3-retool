import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { QuantityService } from "@services/util/quantity.service";
import { UtilService } from "@services/util/util.service";

import { Sku } from "@models";

import { SKU_DISCOUNT_STYLE_LIST } from "../constants";

@Component({
    selector: "supr-product-tile-details",
    template: `
        <div class="details" #details>
            <supr-text class="detailsName" type="body">
                {{ sku?.sku_name }}
            </supr-text>
            <div class="divider4"></div>
            <div class="suprRow offDetails">
                <supr-text class="qty" type="subtext10">
                    {{ displayQty }}
                </supr-text>

                <ng-container
                    *ngIf="
                        sku?.discount_info && !sku?.discount_info?.style?.color;
                        else divider
                    "
                >
                    <div class="suprColumn detailsDot">
                        <div class="detailsDotItem"></div>
                    </div>
                </ng-container>

                <ng-template #divider>
                    <div class="spacer8"></div>
                </ng-template>

                <div
                    class="offDetailsOff"
                    *ngIf="sku?.discount_info"
                    [class.discount]="sku?.discount_info?.style?.color"
                >
                    <supr-text
                        type="subtextExtraBold10"
                        *ngIf="sku?.discount_info?.mode === 'percentage'"
                    >
                        {{ sku?.discount_info?.value }}% OFF
                    </supr-text>

                    <supr-text
                        type="subtextExtraBold10"
                        *ngIf="sku?.discount_info?.mode === 'amount'"
                    >
                        {{ sku?.discount_info?.value | rupee }} OFF
                    </supr-text>
                </div>
            </div>
            <div class="divider4"></div>
            <div class="suprRow">
                <supr-product-tile-pricing-container
                    [sku]="sku"
                ></supr-product-tile-pricing-container>
            </div>
        </div>
    `,
    styleUrls: ["../supr-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailsComponent implements OnInit {
    @Input() sku: Sku;
    @Input() categoryId: number;

    displayQty: string;

    @ViewChild("details", { static: true }) detailsEl: ElementRef;

    constructor(
        private qtyService: QuantityService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.setQty();
        this.setStyles();
    }

    private setQty() {
        this.displayQty = this.qtyService.toText(this.sku);
    }

    private setStyles() {
        this.setOffStyle();
    }

    private setOffStyle() {
        if (!this.sku) {
            return;
        }

        this.utilService.setStyles(
            this.sku,
            SKU_DISCOUNT_STYLE_LIST,
            this.detailsEl
        );
    }
}
