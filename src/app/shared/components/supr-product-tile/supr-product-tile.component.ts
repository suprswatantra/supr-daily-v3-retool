import {
    Input,
    OnInit,
    Output,
    Component,
    OnChanges,
    OnDestroy,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    AfterViewInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    QTY_SUBTEXTS,
    CART_ITEM_TYPES,
    SKU_PREFERRED_MODE,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { Sku, CartItem, SkuAttributes } from "@models";

import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";
import { AppsFlyerService } from "@services/integration/appsflyer.service";

import { Segment } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "./constants/analytics.constants";

@Component({
    selector: "supr-product-tile",
    template: `
        <div class="suprRow tile top" *ngIf="sku">
            <supr-product-tile-info
                saImpression
                class="suprColumn left grow"
                [class.tileFadeIn]="_showOOSInfo"
                [sku]="_sku"
                [saContext]="saContext"
                [saPosition]="saPosition"
                [saObjectValue]="_sku?.id"
                [saContextList]="saContextList"
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.PRODUCT_TILE}"
                (click)="openPreviewModal()"
                (handleSaImpressionCb)="onSkuImpression()"
                (handlePostAddToCart)="handlePostAddToCart.emit()"
            ></supr-product-tile-info>

            <supr-product-preview
                [sku]="_sku"
                [mode]="_mode"
                [hideCart]="hideCart"
                [showOosInfo]="!_showOOSInfo"
                [showPreview]="_showPreviewModal"
                [saContext]="saContext"
                [saPosition]="saPosition"
                [saContextList]="saContextList"
                (handleClose)="closePreviewModal()"
                (handlePostAddToCart)="handlePostAddToCart.emit()"
                (handlePostSubscribe)="handlePostSubscribe.emit()"
            ></supr-product-preview>
        </div>
        <div class="divider16"></div>
        <div class="suprRow tileAction">
            <ng-template #addSubscribeFlow>
                <div class="suprColumn stretch top tileCounter left">
                    <div class="suprRow">
                        <supr-add-button
                            [sku]="_sku"
                            [mode]="_mode"
                            [fromCatalog]="true"
                            [autoIncrement]="_isAutoIncrement"
                            [saContext]="saContext"
                            [saPosition]="saPosition"
                            [saContextList]="saContextList"
                            (handlePostAddToCart)="handlePostAddToCart.emit()"
                            (handleSubModalStateChange)="
                                handleSubModalStateChange.emit($event)
                            "
                        ></supr-add-button>

                        <ng-container
                            *ngIf="
                                _isOosUpfrontExp;
                                else isWithoutOosTPlusXCheck
                            "
                        >
                            <div class="spacer12"></div>
                            <supr-button class="oos">
                                <div class="suprRow center">
                                    <supr-text type="subtext11">
                                        Out of stock
                                    </supr-text>
                                </div>
                            </supr-button>
                        </ng-container>
                        <ng-template #isWithoutOosTPlusXCheck>
                            <div
                                *ngIf="
                                    !cartItem &&
                                    _mode !==
                                        '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                                "
                                class="spacer12"
                            ></div>
                            <supr-subscribe-button
                                [sku]="_sku"
                                [mode]="_mode"
                                [saContext]="saContext"
                                [saPosition]="saPosition"
                                [saContextList]="saContextList"
                                (handlePostSubscribe)="
                                    handlePostSubscribe.emit()
                                "
                            >
                            </supr-subscribe-button>
                        </ng-template>
                    </div>
                    <div class="divider4"></div>
                    <supr-text type="paragraph" class="qtySubtext">
                        {{ _qtySubtext }}
                    </supr-text>
                </div>
            </ng-template>

            <div *ngIf="_showOOSInfo; else addSubscribeFlow">
                <supr-oos-info
                    [sku]="_sku"
                    [showHorizontalLayout]="true"
                    [saContext]="saContext"
                    [saPosition]="saPosition"
                    [saContextList]="saContextList"
                ></supr-oos-info>
            </div>
        </div>

        <div *ngIf="_showNotifyBlock" class="oosNotifyWrapper">
            <supr-oos-notify
                [skuId]="sku?.id"
                [notifyEnabled]="sku?.notify_enabled"
                [addedToCart]="!!cartItem"
                [date]="cartItem?.delivery_date || sku?.next_available_date"
            ></supr-oos-notify>
        </div>
    `,
    styleUrls: ["./supr-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTileComponent
    implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() skuMeta: Sku;
    @Input() skuNotified: string;
    @Input() skuAttributes: SkuAttributes;

    @Input() hideCart: boolean;
    @Input() navigateId: number;
    @Input() skuNavigationId: number;
    @Input() alternatesV2Experiment = true;
    @Input() set cartItem(item: CartItem) {
        this._cartItem = item;
        this.setQtySubtext(item);
        if (this.alternatesV2Experiment) {
            this.setButtonVisibilityStates();
        }
    }
    get cartItem(): CartItem {
        return this._cartItem;
    }

    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() set saContextList(list: Segment.ContextListItem[]) {
        if (this.skuNavigationId) {
            this._saContextList = [
                ...(list || []),
                {
                    name: "autoAdd",
                    value: this.sku.id === this.skuNavigationId,
                },
            ];
        }
    }

    get saContextList(): Segment.ContextListItem[] {
        return this._saContextList;
    }

    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();
    @Output() handleSubModalStateChange: EventEmitter<
        boolean
    > = new EventEmitter();

    _sku: Sku;
    _mode: string;
    _cartItem: CartItem;
    _qtySubtext: string;
    _showOOSInfo = false;
    _showNotifyBlock = false;
    _isAutoIncrement = false;
    _showPreviewModal = false;
    _showConfirmationModal = false;
    _isOosUpfrontExp = false;

    private _saContextList: Segment.ContextListItem[];

    constructor(
        private utilService: UtilService,
        private settingsService: SettingsService,
        private appsFlyerService: AppsFlyerService
    ) {}

    ngOnInit() {
        this.init(this.sku);
        this.alternatesV2Experiment = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["sku"];
        const changeNotifiedDict = changes["skuNotified"];

        const changeSkuAttributes = changes["skuAttributes"];

        if (this.canHandleChange(change)) {
            this.init(change.currentValue);
        }

        if (this.canHandleSkuNotifiedChange(changeNotifiedDict)) {
            this.setButtonVisibilityStates();
        }

        if (this.canHandleSkuAttributesChange(changeSkuAttributes)) {
            this.setButtonVisibilityStates();
        }
    }

    ngAfterViewInit() {
        this.scrollToGroup();
        if (this.sku && this.sku.id === this.skuNavigationId) {
            this.scrollToSku();
        }
    }

    ngOnDestroy() {
        SETTINGS_KEYS_DEFAULT_VALUE["sourceLink"] = null;
    }

    openPreviewModal() {
        this._showPreviewModal = true;
    }

    closePreviewModal() {
        this._showPreviewModal = false;
    }

    onSkuImpression() {
        if (this.sku) {
            this.appsFlyerService.trackSkuImpression(this.sku);
        }
    }

    private setButtonVisibilityStates() {
        if (this.alternatesV2Experiment) {
            const nextAvailableDate =
                this.utilService.getNestedValue(this.sku, "out_of_stock") &&
                this.utilService.getNestedValue(
                    this.sku,
                    "next_available_date"
                );

            /* Show notify block if the sku has a next available date and state is either oos/notify */
            const cartItemType = this.cartItem && this.cartItem.type;

            this._showNotifyBlock =
                (!this.cartItem || cartItemType === CART_ITEM_TYPES.ADDON) &&
                nextAvailableDate &&
                (this.skuNotified === "oos" || this.skuNotified === "notify");

            /*  Show supr-oos-info if sku has not next available date and state is either oos/notify
            and it hasn't been added to cart */

            // [TODO: Alter this logic when grayed out experiment] ( Swatantra)

            const isOosUpfront = this.utilService.getNestedValue(
                this.skuAttributes,
                "oos_info.oos_upfront",
                false
            );

            // Check for OutofStock upfront
            //  ******* Start ********
            if (isOosUpfront) {
                this._isOosUpfrontExp =
                    this.cartItem &&
                    this.cartItem.type === CART_ITEM_TYPES.ADDON &&
                    (this.skuNotified === "oos" ||
                        this.skuNotified === "notify" ||
                        isOosUpfront);

                if (!this.skuNotified) {
                    this.skuNotified = "oos";
                }

                this._showNotifyBlock =
                    (!this.cartItem ||
                        cartItemType === CART_ITEM_TYPES.ADDON) &&
                    nextAvailableDate;
            }

            // ******* End ********

            this._showOOSInfo =
                !this.cartItem &&
                !nextAvailableDate &&
                (this.skuNotified === "oos" || this.skuNotified === "notify");
        } else {
            /* [TODO_RITESH] Deprecate this flow once alternates experiment is resolved */
            if (!this.skuNotified) {
                this._showOOSInfo = false;
                return;
            }

            this._showOOSInfo =
                this.skuNotified === "oos" || this.skuNotified === "notify";
        }
    }

    private init(sku: Sku) {
        const skuMeta = this.utilService.isEmpty(this.skuMeta)
            ? {}
            : this.skuMeta;

        this._sku = { ...sku, ...skuMeta };
        this._mode = this.mode || this._sku.preferred_mode;
        this.setButtonVisibilityStates();
        this.setProductNotified();
    }

    private setQtySubtext(cartItem: CartItem) {
        if (!cartItem) {
            this._qtySubtext = "";
        } else if (cartItem.type === CART_ITEM_TYPES.ADDON) {
            this._qtySubtext = QTY_SUBTEXTS.ADDON;
        } else if (
            cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW ||
            cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        ) {
            this._qtySubtext = QTY_SUBTEXTS.SUBSCRIPTION;
        }
    }

    private canHandleChange(change: SimpleChange) {
        return change && !change.firstChange && change.currentValue;
    }

    private canHandleSkuNotifiedChange(change: SimpleChange) {
        return change && !change.firstChange;
    }

    private canHandleSkuAttributesChange(change: SimpleChange) {
        return change && !change.firstChange;
    }

    private setProductNotified() {
        if (!this._saContextList) {
            this._saContextList = [];
        }

        if (this._showOOSInfo || this._showNotifyBlock) {
            this._saContextList.push(
                {
                    name: "notify",
                    value: this.skuNotified === "oos" ? 0 : 1,
                },
                {
                    name: "notify-tplusx",
                    value: this.utilService.getNestedValue(
                        this.sku,
                        "next_available_date"
                    ),
                },
                {
                    name: "oos_upfront",
                    value: this.utilService.getNestedValue(
                        this.skuAttributes,
                        "oos_info.oos_upfront"
                    )
                        ? "1"
                        : "0",
                }
            );
        }
    }

    private scrollToGroup() {
        this.utilService.scrollTo(this.navigateId, "catNavigateElem");
    }

    private scrollToSku() {
        setTimeout(() => {
            this.utilService.scrollTo(
                this.skuNavigationId,
                "catSkuElem",
                this.sku.id
            );
        }, 100);

        this.setAutoIncrementTrue();
    }

    private setAutoIncrementTrue() {
        const _suprAddButton = this.settingsService.getSettingsValue(
            "skuAutoAdd",
            false
        );

        if (_suprAddButton) {
            this._isAutoIncrement = true;
        }
    }
}
