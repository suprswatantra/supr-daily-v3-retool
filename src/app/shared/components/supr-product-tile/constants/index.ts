export const SKU_DISCOUNT_STYLE_LIST = [
    {
        attributeName: "discount_info.style.color",
        attributeStyleVariableName: "--supr-product-off-text-color",
    },
];

export const ACCESS_PRICE_TEXTS = {
    ACCESS_PRICE_UNLOCKED: "Access exclusive price",
    ACCESS_PRICE_LOCKED: "Unlock Access price",
    SUPR_PRICE: "Regular price",
};


export const PRICING_TYPE  = "pricing_type";

export const EXCLUSIVE_PRICE_TEXTS = {
    EXCLUSIVE_PRICE: "Exclusive price",
    EXCLUSIVE: "exclusive",
};


export enum PRICE_TYPES {
    UNIT_PRICE = "unit_price",
    UNIT_MRP = "unit_mrp",
    ACCESS_PRICE = "access_price",
    NON_ACCESS_PRICE = "non_access_price",
}
