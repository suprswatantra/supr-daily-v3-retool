export const ANALYTICS_OBJECT_NAMES = {
    IMPRESSION: {
        PRODUCT_TILE: "product-viewed",
        ACCESS_PRICE: "product-tile-access-price",
        OUT_OF_STOCK_SKU: "out-of-stock-sku",
    },
    CLICK: {
        SUPR_PASS: "product-tile-access-price-supr-pass-click",
    },
};
