import { ProductTileComponent } from "./supr-product-tile.component";
import { ImageComponent } from "./components/image.component";
import { DetailsComponent } from "./components/details.component";
import { InfoComponent } from "./components/info.component";
import { ProductTilePricingContainer } from "./containers/pricing-details.container";
import { ProductTilePricingComponent } from "./components/pricing.component";

export const productTileComponents = [
    ProductTileComponent,
    ImageComponent,
    DetailsComponent,
    ProductTilePricingContainer,
    ProductTilePricingComponent,
    InfoComponent,
];
