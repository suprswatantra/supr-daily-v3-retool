import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";

import { Sku } from "@shared/models";

@Component({
    selector: "supr-product-tile-pricing-container",
    template: `
        <supr-product-tile-pricing
            [sku]="sku"
            [isSuprPassActive]="isSuprPassActive$ | async"
        ></supr-product-tile-pricing>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTilePricingContainer implements OnInit {
    @Input() sku: Sku;

    isSuprPassActive$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isSuprPassActive$ = this.adapter.isSuprPassActive$;
    }
}
