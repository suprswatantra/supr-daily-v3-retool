import { PlansComponent } from "./plans.component";
import { PlanItemComponent } from "./plan-item.component";

export const suprPassPlansComponents = [PlansComponent, PlanItemComponent];
