export const ANALYTICS_OBJECT_NAME = {
    CLICK: {
        PLAN_CLICK: "supr-pass-plan",
    },
};

export const TEXTS = {
    RECOMMENDED_PLAN_TEXT: "RECOMMENDED",
};
