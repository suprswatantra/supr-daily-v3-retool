import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { SuprPassPlan } from "@shared/models";

import { TEXTS } from "./constants/analytics.constants";

@Component({
    selector: "supr-pass-plan-item",
    template: `
        <div
            class="planDetailWrapper"
            [class.active]="isSelected"
            [class.recommendedActive]="planInfo?.isRecommended && isSelected"
            (click)="handlePlanSelect.emit(planInfo)"
            saClick
            [saObjectName]="saObjNamePlanClick"
            [saObjectValue]="planInfo?.id"
        >
            <div
                class="recommended"
                *ngIf="planInfo?.isRecommended && isSelected"
            >
                <supr-text class="recommendedText">
                    {{ recommendedText }}
                </supr-text>
            </div>
            <div class="suprColumn">
                <supr-text *ngIf="planInfo?.durationMonths" class="duration"
                    >{{ planInfo.durationMonths }}
                    {{
                        planInfo.durationMonths > 1 ? "Months" : "Month"
                    }}</supr-text
                >
                <supr-text *ngIf="planInfo?.unitPrice" class="unitPrice">{{
                    planInfo.unitPrice | rupee
                }}</supr-text>
                <supr-text *ngIf="planInfo?.unitMrp" class="unitMrp">{{
                    planInfo.unitMrp | rupee
                }}</supr-text>
                <supr-text
                    *ngIf="planInfo?.discountText"
                    class="discountText"
                    >{{ planInfo.discountText }}</supr-text
                >
            </div>
        </div>
    `,
    styleUrls: ["./supr-pass-plans.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlanItemComponent {
    @Input() planInfo: SuprPassPlan;
    @Input() planIndex: number;
    @Input() isSelected: boolean;
    @Input() saObjNamePlanClick: string;

    @Output() handlePlanSelect: EventEmitter<SuprPassPlan> = new EventEmitter();

    recommendedText = TEXTS.RECOMMENDED_PLAN_TEXT;
}
