import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { SuprPassPlan, TextFragment } from "@shared/models";

import { ANALYTICS_OBJECT_NAME } from "./constants/analytics.constants";

@Component({
    selector: "supr-pass-plans",
    template: `
        <div class="plansBlock">
            <div class="divider28"></div>
            <ng-container *ngIf="planSelectionText?.text">
                <div class="suprRow center plansHeader">
                    <supr-text-fragment
                        [textFragment]="planSelectionText"
                        class="boldText"
                    ></supr-text-fragment>
                </div>
                <div class="divider36"></div>
            </ng-container>

            <div class="suprRow center bottom">
                <div
                    *ngFor="
                        let plan of suprPassPlans;
                        let i = index;
                        trackBy: trackByFn
                    "
                >
                    <supr-pass-plan-item
                        [planInfo]="plan"
                        [planIndex]="i"
                        [isSelected]="selectedPlan?.id === plan.id"
                        [saObjNamePlanClick]="saObjNamePlanClick"
                        (handlePlanSelect)="handlePlanSelect.emit($event)"
                    ></supr-pass-plan-item>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-pass-plans.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlansComponent {
    @Input() suprPassPlans: SuprPassPlan[] = [];
    @Input() selectedPlan: SuprPassPlan;
    @Input() planSelectionText: TextFragment;
    @Input() saObjNamePlanClick: string =
        ANALYTICS_OBJECT_NAME.CLICK.PLAN_CLICK;

    @Output() handlePlanSelect: EventEmitter<SuprPassPlan> = new EventEmitter();

    trackByFn(_: any, index: number): number {
        return index;
    }
}
