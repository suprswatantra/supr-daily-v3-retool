import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { AUTO_SCROLL_INFO } from "@constants";

import { Collection } from "@models";
import { DISPLAY_TYPES } from "@shared/components/supr-collection/constants";

@Component({
    selector: "supr-collection-widget",
    template: `
        <div
            class="collection"
            #wrapper
            [class.addPadding]="_addPadding"
            [id]="wrapperId"
        >
            <supr-collection-header
                [title]="collection?.title"
                [displayType]="collection?.displayType"
                [subtitle]="collection?.subtitle"
                [seeAll]="collection?.seeAll?.button"
                [viewId]="collection?.viewId"
                *ngIf="showHeader()"
            ></supr-collection-header>

            <supr-collection-bookmark
                [bookmarkTag]="collection?.bookmarkTag"
            ></supr-collection-bookmark>

            <supr-collection-display-wrapper
                [collection]="collection"
            ></supr-collection-display-wrapper>

            <supr-collection-footer
                [tnc]="collection?.tnc"
            ></supr-collection-footer>
        </div>
    `,
    styleUrls: ["./styles/supr-collection-widget.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollectionWidgetComponent implements OnInit {
    @Input() collection: Collection;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    DISPLAY_TYPES = DISPLAY_TYPES;
    _addPadding = true;
    wrapperId = "";

    ngOnInit() {
        this.setBackground();
        this.setWrapperID();
    }

    showHeader() {
        if (!this.collection) {
            return false;
        }

        return true;
    }

    private setBackground() {
        if (!this.collection.background) {
            // If Collection has no bg color we will not set padding top and bottom
            this._addPadding = false;

            return;
        }

        this.setBgColor();
        this.setBgImage();
    }

    private setBgColor() {
        const bgColor = this.collection.background.color;
        if (!bgColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-collection-bg-color",
            bgColor
        );
    }

    private setBgImage() {
        const bgImage = this.collection.background.image;
        if (!bgImage || !bgImage.fullUrl) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "background-image",
            `url(${bgImage.fullUrl})`
        );
    }

    private setWrapperID() {
        this.wrapperId =
            AUTO_SCROLL_INFO.prefix_collection + this.collection.viewId;
    }
}
