import { CollectionWidgetComponent } from "./supr-collection-widget.component";
import { HeaderComponent } from "./components/header.component";
import { FooterComponent } from "./components/footer.component";
import { BookmarkComponent } from "./components/bookmark.component";

import { DisplayWrapperComponent } from "./components/display/wrapper.component";
import { HorizontalDisplayComponent } from "./components/display/horizontal.component";
import { ImgDisplayComponent } from "./components/display/img-collection.component";
import { GridDisplayComponent } from "./components/display/grid.component";
import { SingleCardDisplayComponent } from "./components/display/single-card.component";
import { HeroGridDisplayComponent } from "./components/display/hero-grid.component";

import { CardTypeComponent } from "./components/cards/card.component";
import { HorizontalCardComponent } from "./components/cards/horizontal-card.component";
import { GridCardComponent } from "./components/cards/grid-card.component";
import { SkuStackedComponent } from "./components/sku-stacked.component";
import { SeeAllComponent } from "./components/see-all.component";
import { SeeAllVerticalComponent } from "./components/see-all-vertical.component";
import { TagRibbonComponent } from "./components/tags/ribbon.component";
import { TagBannerComponent } from "./components/tags/banner.component";
import { VerticalCardComponent } from "./components/cards/vertical-card.component";
import { VerticalDisplayComponent } from "./components/display/vertical.component";
import { SkuWideComponent } from "./components/sku-wide.component";
import { SkuComponent } from "./components/sku.component";
import { SkuContainer } from "./containers/sku.container";

export const collectionWidgetComponents = [
    CollectionWidgetComponent,
    HeaderComponent,
    FooterComponent,
    BookmarkComponent,
    DisplayWrapperComponent,
    ImgDisplayComponent,
    HorizontalDisplayComponent,
    GridDisplayComponent,
    SingleCardDisplayComponent,
    HeroGridDisplayComponent,
    CardTypeComponent,
    HorizontalCardComponent,
    GridCardComponent,
    SkuStackedComponent,
    SeeAllComponent,
    SeeAllVerticalComponent,
    TagRibbonComponent,
    TagBannerComponent,
    VerticalDisplayComponent,
    VerticalCardComponent,
    SkuWideComponent,
    SkuComponent,
    SkuContainer,
];
