import { Component, OnInit, Input } from "@angular/core";

import { Observable } from "rxjs";

import { DISPLAY_TYPES } from "@shared/components/supr-collection/constants";

import { Sku, CollectionItem, CartItem } from "@models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-sku-container",
    template: `
        <ng-container *ngIf="sku$ | async" [ngSwitch]="displayType">
            <supr-collection-sku-stacked
                *ngSwitchCase="DISPLAY_TYPES.HORIZONTAL"
                [collectionItem]="item"
                [sku]="sku$ | async"
                [skuNotified]="skuNotified$ | async"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            ></supr-collection-sku-stacked>

            <supr-collection-sku-wide
                *ngSwitchCase="DISPLAY_TYPES.VERTICAL"
                [collectionItem]="item"
                [sku]="sku$ | async"
                [cartItem]="cartItem$ | async"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            ></supr-collection-sku-wide>
        </ng-container>
    `,
})
export class SkuContainer implements OnInit {
    @Input() item: CollectionItem;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;
    @Input() displayType: string;

    sku$: Observable<Sku>;
    cartItem$: Observable<CartItem>;
    skuNotified$: Observable<string>;

    DISPLAY_TYPES = DISPLAY_TYPES;

    constructor(private adapter: Adapter, private skuAdapter: SkuAdapter) {}

    ngOnInit() {
        this.sku$ = this.adapter.getSkuById(this.item.entityId);
        this.cartItem$ = this.adapter.getCartItemById(this.item.entityId);
        this.skuNotified$ = this.skuAdapter.isNotified(this.item.entityId);
    }
}
