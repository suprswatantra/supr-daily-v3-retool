export const DISPLAY_TYPES = {
    HORIZONTAL: "horizontal_list",
    GRID: "2_by_2_grid",
    VERTICAL: "vertical_list",
    SINGLE_CARD: "single_card",
    HERO_GRID: "hero_grid",
};

export const CARD_TYPES = {
    SKU: "sku",
    SKU_COLLECTION: "sku_collection",
};

export const TAG_TYPES = {
    BANNER: "banner",
    RIBBON: "ribbon",
};

export const SEE_ALL_TYPES = {
    BUTTON: "button",
    CARD: "card",
};

export const WHITE_COLOR_CODES = ["#FFF", "#FFFFFF"];

export { ANALYTICS_OBJECT_NAMES, ANALYTICS_TEXTS } from "./analytics.constants";

export const DIRECT_ADD = "direct_add";
