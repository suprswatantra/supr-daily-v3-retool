export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        ADD_PRODUCT: "add-product",
    },
    IMPRESSION: {
        COLLECTION_ITEM: "collection-item",
        SEE_ALL: "see-all",
        COLLECTION: "collection",
        COLLECTION_HERO_VERTICAL: "hero-collection-vertical-card",
        COLLECTION_HERO_GRID: "hero-collection-grid-card",
    },
};

export const ANALYTICS_TEXTS = {
    COLLECTION: "collection",
    BUTTON: "button",
    CARD: "card",
};
