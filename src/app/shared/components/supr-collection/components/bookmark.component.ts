import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { CollectionBookmarkTag } from "@models";

import { Color } from "@types";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-collection-bookmark",
    template: `
        <div #wrapper>
            <div class="bookmarkWrapper" *ngIf="bookmarkTag">
                <div class="bookmarkContent">
                    <ng-container *ngIf="bookmarkTag?.title?.text">
                        <supr-text type="action14">
                            {{ bookmarkTag?.title?.text }}
                        </supr-text>
                    </ng-container>

                    <ng-container *ngIf="bookmarkTag?.subtitle?.text">
                        <supr-text type="caption" class="subtitle">
                            {{ bookmarkTag?.subtitle?.text }}
                        </supr-text>
                    </ng-container>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/bookmark.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookmarkComponent implements OnInit {
    @Input() bookmarkTag: CollectionBookmarkTag;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    hslData: Color.HSL;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        if (this.bookmarkTag) {
            this.setColors();
        }
    }

    private setColors() {
        this.setBgColor();
        this.setTextColor();
    }

    private setBgColor() {
        const { bgColor } = this.bookmarkTag;

        if (!bgColor) {
            return;
        }

        const { h, s, l } = this.utilService.hexToHSL(bgColor);

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-bookmark-bg-color",
            this.bookmarkTag.bgColor
        );

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-bookmark-notch-bg-color",
            `hsl(${h}, ${s}%, ${l - 10}%)`
        );
    }

    private setTextColor() {
        const { title, subtitle } = this.bookmarkTag;

        if (title) {
            this.wrapperEl.nativeElement.style.setProperty(
                "--supr-bookmark-title-color",
                title.color
            );
        }

        if (subtitle) {
            this.wrapperEl.nativeElement.style.setProperty(
                "--supr-bookmark-subtitle-color",
                subtitle.color
            );
        }
    }
}
