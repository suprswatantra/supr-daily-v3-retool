import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { Collection } from "@models";

import { DISPLAY_TYPES } from "@shared/components/supr-collection/constants";

@Component({
    selector: "supr-collection-display-wrapper",
    template: `
        <ng-container [ngSwitch]="collection?.displayType">
            <supr-collection-horizontal
                *ngSwitchCase="DISPLAY_TYPES.HORIZONTAL"
                [collection]="collection"
            ></supr-collection-horizontal>

            <supr-collection-vertical
                *ngSwitchCase="DISPLAY_TYPES.VERTICAL"
                [collection]="collection"
            ></supr-collection-vertical>

            <supr-collection-grid
                *ngSwitchCase="DISPLAY_TYPES.GRID"
                [collection]="collection"
            ></supr-collection-grid>

            <supr-collection-single-card
                *ngSwitchCase="DISPLAY_TYPES.SINGLE_CARD"
                [collection]="collection"
            ></supr-collection-single-card>

            <supr-collection-hero-grid
                *ngSwitchCase="DISPLAY_TYPES.HERO_GRID"
                [collection]="collection"
            ></supr-collection-hero-grid>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DisplayWrapperComponent {
    @Input() collection: Collection;

    DISPLAY_TYPES = DISPLAY_TYPES;
}
