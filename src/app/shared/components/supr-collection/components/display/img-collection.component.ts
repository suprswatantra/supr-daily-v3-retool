import { Component, Input } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

@Component({
    selector: "supr-collection-img",
    template: `
        <div class="CollectionImage" *ngIf="src">
            <supr-image
                [src]="src"
                lazyLoad="false"
                imgWidth="${CLODUINARY_IMAGE_SIZE.PREVIEW.WIDTH}"
                imgHeight="${CLODUINARY_IMAGE_SIZE.PREVIEW.HEIGHT}"
            ></supr-image>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
})
export class ImgDisplayComponent {
    @Input() src: string;
}
