import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { Collection } from "@models";
import { RouterService } from "@services/util/router.service";
import {
    DISPLAY_TYPES,
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_TEXTS,
} from "@shared/components/supr-collection/constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-vertical",
    template: `
        <div class="vertical">
            <ng-container
                *ngFor="let item of collection?.items; index as position"
            >
                <supr-collection-vertical-card
                    [borderColor]="item?.borderColor"
                    type="item"
                >
                    <supr-collection-card
                        displayType="${DISPLAY_TYPES.VERTICAL}"
                        [cardType]="collection?.type"
                        [item]="item"
                        saImpression
                        [saImpressionEnabled]="
                            ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_CARD}
                        "
                        saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                            .COLLECTION_ITEM}"
                        [saObjectValue]="item?.entityId"
                        [saContextList]="saContextList"
                        [saPosition]="position + 1"
                    >
                    </supr-collection-card>
                </supr-collection-vertical-card>
                <div class="divider8"></div>
            </ng-container>

            <ng-container *ngIf="collection?.seeAll?.button">
                <div class="divider8"></div>

                <supr-collection-see-all-vertical
                    [seeAllText]="collection?.seeAll?.button"
                    [seeAllTitle]="collection?.title"
                    type="button"
                    (click)="goToCollectionPage()"
                    saImpression
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION.SEE_ALL}"
                    saObjectValue="${ANALYTICS_TEXTS.BUTTON}"
                    [saContextList]="saContextList"
                ></supr-collection-see-all-vertical>

                <div class="divider8"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerticalDisplayComponent implements OnInit {
    @Input() collection: Collection;

    saContextList: Segment.ContextListItem[] = [];

    constructor(private routerService: RouterService) {}

    ngOnInit() {
        this.setContextList();
    }

    goToCollectionPage() {
        this.routerService.goToCollectionPage(this.collection.viewId);
    }

    private setContextList() {
        this.saContextList.push({
            name: ANALYTICS_TEXTS.COLLECTION,
            value: this.collection && this.collection.viewId,
        });
    }
}
