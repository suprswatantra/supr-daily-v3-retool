import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { Collection } from "@models";
import { RouterService } from "@services/util/router.service";

import {
    CARD_TYPES,
    TAG_TYPES,
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_TEXTS,
    DISPLAY_TYPES,
} from "@shared/components/supr-collection/constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-horizontal",
    template: `
        <div class="horizontal">
            <ng-container
                *ngFor="
                    let item of collection?.items;
                    index as position;
                    first as firstPosition
                "
            >
                <ng-container *ngIf="firstPosition && src">
                    <supr-collection-img [src]="src"></supr-collection-img>
                </ng-container>
                <supr-collection-horizontal-card
                    [borderColor]="item?.borderColor"
                    [bannerClass]="bannerClass"
                    type="item"
                >
                    <supr-collection-card
                        displayType="${DISPLAY_TYPES.HORIZONTAL}"
                        [cardType]="collection?.type"
                        [item]="item"
                        saImpression
                        [saImpressionEnabled]="
                            ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_CARD}
                        "
                        saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                            .COLLECTION_ITEM}"
                        [saObjectValue]="item?.entityId"
                        [saContextList]="saContextList"
                        [saPosition]="position + 1"
                    >
                    </supr-collection-card>
                </supr-collection-horizontal-card>
            </ng-container>

            <ng-container *ngIf="collection?.seeAll?.card">
                <supr-collection-horizontal-card
                    [borderColor]="collection?.seeAll?.card?.borderColor"
                    [bgColor]="collection?.seeAll?.card?.bgColor"
                    [bannerClass]="bannerClass"
                >
                    <supr-collection-see-all
                        [seeAllText]="collection?.seeAll?.card"
                        type="card"
                        (click)="goToCollectionPage()"
                        saImpression
                        saClick
                        saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                            .SEE_ALL}"
                        saObjectValue="${ANALYTICS_TEXTS.CARD}"
                        [saContextList]="saContextList"
                    ></supr-collection-see-all>
                </supr-collection-horizontal-card>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HorizontalDisplayComponent implements OnInit {
    @Input() collection: Collection;

    bannerClass: string;
    saContextList: Segment.ContextListItem[] = [];
    collectionSetting = {};
    src = "";

    constructor(private routerService: RouterService) {}

    ngOnInit() {
        this.setBannerClass();
        this.setContextList();
        this.setImageSrcForFirstPosition();
    }

    goToCollectionPage() {
        this.routerService.goToCollectionPage(
            this.collection && this.collection.viewId
        );
    }

    private setBannerClass() {
        try {
            if (!this.isSkuItem()) {
                return;
            }

            const firstItem = this.collection.items[0];
            const ribbonTag = firstItem.tags.find(
                (tag) => tag.type === TAG_TYPES.RIBBON
            );
            const bannerTag = firstItem.tags.find(
                (tag) => tag.type === TAG_TYPES.BANNER
            );

            if (ribbonTag && bannerTag) {
                this.bannerClass = "bannerBoth";
            } else if (ribbonTag) {
                this.bannerClass = "bannerTop";
            } else if (bannerTag) {
                this.bannerClass = "bannerBottom";
            }
        } catch (err) {
            /** */
        }
    }

    private setImageSrcForFirstPosition() {
        if (
            this.collection &&
            this.collection.urgencyImage &&
            this.collection.urgencyImage.fullUrl
        ) {
            this.src = this.collection.urgencyImage.fullUrl;
        } else {
            this.src = "";
        }
    }

    private isSkuItem(): boolean {
        return this.collection.type === CARD_TYPES.SKU;
    }

    private setContextList() {
        this.saContextList.push({
            name: ANALYTICS_TEXTS.COLLECTION,
            value: this.collection && this.collection.viewId,
        });
    }
}
