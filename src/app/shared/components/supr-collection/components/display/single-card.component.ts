import { Component, Input } from "@angular/core";

import { Collection } from "@models";

import { RouterService } from "@services/util/router.service";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-collection-single-card",
    template: `
        <div
            class="SingleCardImage"
            *ngIf="collection?.singleCardImg?.fullUrl"
            (click)="goToCollectionPage()"
        >
            <supr-image
                [src]="collection?.singleCardImg?.fullUrl"
                [image]="collection?.singleCardImg"
                [imgWidth]="
                    ${CLODUINARY_IMAGE_SIZE.SINGLE_CARD_COLLECTION.WIDTH}
                "
                [imgHeight]="
                    ${CLODUINARY_IMAGE_SIZE.SINGLE_CARD_COLLECTION.HEIGHT}
                "
            ></supr-image>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
})
export class SingleCardDisplayComponent {
    @Input() collection: Collection;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    goToCollectionPage() {
        const hasVeiwId = this.utilService.getNestedValue(
            this.collection,
            "viewId"
        );

        if (!hasVeiwId) {
            return;
        }

        this.routerService.goToCollectionPage(
            this.collection && this.collection.viewId
        );
    }
}
