import {
    Component,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";

import { Collection, CollectionItem } from "@models";
import { UtilService } from "@services/util/util.service";
import { DISPLAY_TYPES } from "../../constants";

import {
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_TEXTS,
} from "@shared/components/supr-collection/constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-grid",
    template: `
        <div class="grid">
            <ng-container *ngFor="let rowData of gridData; index as row">
                <div class="gridRow suprRow">
                    <ng-container *ngFor="let item of rowData; index as col">
                        <div class="gridRowItem">
                            <supr-collection-grid-card
                                [borderColor]="item?.borderColor"
                            >
                                <supr-collection-card
                                    displayType="${DISPLAY_TYPES.HORIZONTAL}"
                                    [cardType]="collection?.type"
                                    [item]="item"
                                    [cardType]="collection?.type"
                                    [item]="item"
                                    saImpression
                                    [saImpressionEnabled]="
                                        ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_CARD}
                                    "
                                    saObjectName="${ANALYTICS_OBJECT_NAMES
                                        .IMPRESSION.COLLECTION_ITEM}"
                                    [saObjectValue]="item?.entityId"
                                    [saContextList]="saContextList"
                                    [saPosition]="getPosition(row, col)"
                                >
                                </supr-collection-card>
                            </supr-collection-grid-card>
                        </div>
                    </ng-container>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridDisplayComponent implements OnInit, OnChanges {
    @Input() collection: Collection;

    gridData: CollectionItem[][] = [];
    saContextList: Segment.ContextListItem[] = [];

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["collection"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.init();
        }
    }

    getPosition(row: number, col: number): number {
        const _row = row + 1;
        const _col = col + 1;
        const pos = 2 * _row;

        return _col === 2 ? pos : pos - 1;
    }

    private init() {
        this.splitItemsIntoGroups();
        this.setContextList();
    }

    private splitItemsIntoGroups() {
        const items = this.collection.items;
        if (!items || !items.length) {
            return;
        }

        this.gridData = this.utilService.chunkArray(items, 2);
    }

    private setContextList() {
        this.saContextList.push({
            name: ANALYTICS_TEXTS.COLLECTION,
            value: this.collection && this.collection.viewId,
        });
    }
}
