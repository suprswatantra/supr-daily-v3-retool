import {
    Component,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE, SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { Collection, CollectionItem } from "@models";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { ANALYTICS_OBJECT_NAMES } from "../../constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-hero-grid",
    template: `
        <div class="heroGrid" saImpression [saContextList]="saContextList">
            <div
                class="heroGridVertical"
                *ngIf="verticalItemData"
                (click)="goToCollectionPage(verticalItemData?.entityId)"
                saImpression
                [saImpressionEnabled]="
                    ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_CARD}
                "
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                    .COLLECTION_HERO_VERTICAL}"
                [saObjectValue]="verticalItemData?.entityId"
            >
                <div class="heroGridVerticalCard">
                    <supr-image
                        [src]="verticalItemData?.image?.fullUrl"
                        [image]="verticalItemData?.image"
                        [imgWidth]="
                            ${CLODUINARY_IMAGE_SIZE.HERO_GRID.VERTICAL_IMAGE
                                .WIDTH}
                        "
                        [imgHeight]="
                            ${CLODUINARY_IMAGE_SIZE.HERO_GRID.VERTICAL_IMAGE
                                .HEIGHT}
                        "
                        [withWrapper]="false"
                    ></supr-image>
                </div>
            </div>
            <div class="spacer8"></div>
            <ng-container *ngFor="let rowData of gridData; index as row">
                <div class="heroGridHorizontal">
                    <ng-container *ngFor="let item of rowData; index as col">
                        <div
                            class="heroGridHorizontalCard"
                            (click)="goToCollectionPage(item?.entityId)"
                            saImpression
                            [saImpressionEnabled]="
                                ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_CARD}
                            "
                            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                                .COLLECTION_HERO_GRID}"
                            [saObjectValue]="item?.entityId"
                            [saPosition]="col + 1"
                        >
                            <supr-image
                                [src]="item?.image?.fullUrl"
                                [imgWidth]="
                                    ${CLODUINARY_IMAGE_SIZE.HERO_GRID.GRID_IMAGE
                                        .WIDTH}
                                "
                                [imgHeight]="
                                    ${CLODUINARY_IMAGE_SIZE.HERO_GRID.GRID_IMAGE
                                        .HEIGHT}
                                "
                                [withWrapper]="false"
                            ></supr-image>
                        </div>
                    </ng-container>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
})
export class HeroGridDisplayComponent implements OnInit, OnChanges {
    @Input() collection: Collection;

    gridData: CollectionItem[][] = [];
    verticalItemData: CollectionItem;
    saContextList: Segment.ContextListItem[] = [];

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["collection"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.init();
        }
    }

    private init() {
        this.splitItemsIntoGroups();
        this.setContextList();
    }

    private splitItemsIntoGroups() {
        this.verticalItemData = this.collection.items[0];

        const items = this.collection.items.filter((item, index) => {
            return item && index !== 0;
        });

        if (!items || !items.length) {
            return;
        }

        this.gridData = this.utilService.chunkArray(items, 2);
    }

    goToCollectionPage(id: number) {
        if (!id) {
            return;
        }

        this.routerService.goToCollectionPage(id);
    }

    private setContextList() {
        this.saContextList.push({
            name: ANALYTICS_OBJECT_NAMES.IMPRESSION.COLLECTION,
            value: this.collection && this.collection.viewId,
        });
    }
}
