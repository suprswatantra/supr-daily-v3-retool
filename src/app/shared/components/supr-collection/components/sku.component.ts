import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    Sku,
    CartItem,
    CollectionItem,
    Vacation,
    CollectionItemTag,
} from "@models";
import { QuantityService } from "@services/util/quantity.service";

import { TAG_TYPES } from "@shared/components/supr-collection/constants";

import { Segment } from "@types";

@Component({
    selector: "supr-collection-sku",
    template: `
        <div class="sku" #wrapper>
            <div class="suprColumn spaceBetween">
                <div class="skuSection" (click)="openPreviewModal()">
                    <supr-collection-tag-ribbon
                        [tag]="ribbonTag"
                    ></supr-collection-tag-ribbon>

                    <supr-image
                        [src]="sku?.image?.fullUrl"
                        [image]="sku?.image"
                    ></supr-image>
                    <div class="divider12"></div>

                    <supr-text type="body" class="name" [truncate]="true">
                        {{ sku?.sku_name }}
                    </supr-text>
                    <div class="divider4"></div>

                    <supr-text class="qty" type="caption">
                        {{ displayQty }}
                    </supr-text>
                    <div class="divider4"></div>

                    <div class="suprRow">
                        <supr-text type="body" class="price">
                            {{ sku?.unit_price | rupee: 2 }}
                        </supr-text>

                        <ng-container *ngIf="showPriceStrike">
                            <div class="spacer8"></div>
                            <supr-text class="priceStrike" type="caption">
                                {{ sku?.unit_mrp | rupee: 2 }}
                            </supr-text>
                        </ng-container>

                        <div class="spacer8"></div>
                        <supr-text class="qty suprHide" type="caption">
                            {{ displayQty }}
                        </supr-text>
                    </div>
                </div>

                <div class="skuSection">
                    <supr-add-button
                        [sku]="sku"
                        [mode]="collectionItem?.preferredMode"
                        [fromCatalog]="true"
                        [saContextList]="saContextList"
                        [saPosition]="saPosition"
                    ></supr-add-button>

                    <supr-collection-tag-banner
                        [tag]="bannerTag"
                    ></supr-collection-tag-banner>
                </div>
            </div>

            <supr-product-preview
                [showPreview]="showPreviewModal"
                [sku]="sku"
                [mode]="collectionItem?.preferredMode"
                [hideCart]="true"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
                (handleClose)="closePreviewModal()"
            ></supr-product-preview>
        </div>
    `,
    styleUrls: ["../styles/sku.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkuComponent implements OnInit {
    @Input() sku: Sku;
    @Input() collectionItem: CollectionItem;
    @Input() cartItem: CartItem;
    @Input() vacation: Vacation;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    ribbonTag: CollectionItemTag;
    bannerTag: CollectionItemTag;
    displayQty: string;
    showPriceStrike = false;
    showPreviewModal = false;

    constructor(private qtyService: QuantityService) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.setPrice();
        this.setQty();
        this.setTags();
        this.setBorderColor();
    }

    openPreviewModal() {
        this.showPreviewModal = true;
    }

    closePreviewModal() {
        this.showPreviewModal = false;
    }

    private setPrice() {
        if (!this.sku) {
            return;
        }

        this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
    }

    private setQty() {
        this.displayQty = this.qtyService.toText(this.sku);
    }

    private setTags() {
        const { tags } = this.collectionItem;
        if (!tags || !tags.length) {
            return;
        }

        this.ribbonTag = tags.find((tag) => tag.type === TAG_TYPES.RIBBON);
        this.bannerTag = tags.find((tag) => tag.type === TAG_TYPES.BANNER);
    }

    private setBorderColor() {
        const { borderColor } = this.collectionItem;
        if (!borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-sku-border-color",
            borderColor
        );
    }
}
