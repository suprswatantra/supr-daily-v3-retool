import {
    Component,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CollectionItemTag } from "@models";

@Component({
    selector: "supr-collection-tag-ribbon",
    template: `
        <div #wrapper>
            <supr-tag [bgColor]="tag?.bgColor" *ngIf="tag">
                <div class="suprRow tag ribbon">
                    <ng-container *ngIf="tag?.icon?.fullUrl">
                        <supr-image
                            [src]="tag?.icon?.fullUrl"
                            [lazyLoad]="false"
                        ></supr-image>
                        <div class="spacer4"></div>
                    </ng-container>

                    <supr-text type="body">{{ tag?.title?.text }}</supr-text>
                </div>
            </supr-tag>
        </div>
    `,
    styleUrls: ["../../styles/tag.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagRibbonComponent implements OnInit {
    @Input() tag: CollectionItemTag;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    ngOnInit() {
        if (!this.tag) {
            return;
        }

        this.setTextColor();
    }

    private setTextColor() {
        const { title } = this.tag;
        if (!title || !title.color) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-tag-text-color",
            title.color
        );
    }
}
