import {
    Component,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CollectionItemTag } from "@models";

import { WHITE_COLOR_CODES } from "../../constants";

@Component({
    selector: "supr-collection-tag-banner",
    template: `
        <div #wrapper>
            <div
                class="suprRow banner tag center"
                [class.border]="tagBorder"
                *ngIf="tag"
            >
                <ng-container *ngIf="tag?.icon?.fullUrl">
                    <supr-image
                        [src]="tag?.icon?.fullUrl"
                        [lazyLoad]="false"
                    ></supr-image>
                    <div class="spacer4"></div>
                </ng-container>

                <supr-text type="caption">{{ tag?.title?.text }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/tag.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagBannerComponent implements OnInit {
    @Input() tag: CollectionItemTag;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    tagBorder = false;

    ngOnInit() {
        if (!this.tag) {
            return;
        }

        this.setBgColor();
        this.setTextColor();
    }

    private setBgColor() {
        const { bgColor } = this.tag;
        if (this.bgColorPresent(bgColor)) {
            this.wrapperEl.nativeElement.style.setProperty(
                "--supr-tag-bg-color",
                bgColor
            );
        } else {
            this.tagBorder = true;
        }
    }

    private setTextColor() {
        const { title } = this.tag;
        if (!title || !title.color) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-tag-text-color",
            title.color
        );
    }

    private bgColorPresent(bgColor: string) {
        const color = (bgColor || "").toUpperCase();
        return color.length && WHITE_COLOR_CODES.indexOf(color) === -1;
    }
}
