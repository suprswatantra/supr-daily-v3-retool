import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { CollectionSeeAllText } from "@models";

import { SEE_ALL_TYPES } from "@shared/components/supr-collection/constants";

@Component({
    selector: "supr-collection-see-all",
    template: `
        <div class="wrapper" [ngClass]="type" #wrapper>
            <ng-container [ngSwitch]="type">
                <div class="suprRow center" *ngSwitchCase="SEE_ALL_TYPES.CARD">
                    <supr-text type="body">
                        {{ seeAllText?.title?.text | uppercase }}
                    </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>

                <div
                    class="button suprRow"
                    *ngSwitchCase="SEE_ALL_TYPES.BUTTON"
                >
                    <supr-text type="body">
                        {{ seeAllText?.title?.text }}
                    </supr-text>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/see-all.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SeeAllComponent implements OnInit {
    @Input() seeAllText: CollectionSeeAllText;
    @Input() type: string;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    SEE_ALL_TYPES = SEE_ALL_TYPES;

    ngOnInit() {
        this.setColors();
    }

    private setColors() {
        if (!this.seeAllText) {
            return;
        }

        this.setTitleColor();
        this.setBgColor();
        this.setBorderColor();
    }

    private setTitleColor() {
        const { title } = this.seeAllText;
        if (!title || !title.color) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-see-all-text-color",
            title.color
        );
    }

    private setBgColor() {
        const { bgColor } = this.seeAllText;
        if (!bgColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-see-all-bg-color",
            bgColor
        );
    }

    private setBorderColor() {
        const { borderColor } = this.seeAllText;
        if (!borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-see-all-border-color",
            borderColor
        );
    }
}
