import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { CollectionItem } from "@models";
import {
    CARD_TYPES,
    DISPLAY_TYPES,
} from "@shared/components/supr-collection/constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-card",
    template: `
        <ng-container [ngSwitch]="cardType">
            <supr-collection-sku-container
                *ngSwitchCase="CARD_TYPES.SKU"
                [item]="item"
                [displayType]="displayType"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            ></supr-collection-sku-container>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardTypeComponent {
    @Input() cardType = CARD_TYPES.SKU;
    @Input() displayType = DISPLAY_TYPES.HORIZONTAL;
    @Input() item: CollectionItem;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    CARD_TYPES = CARD_TYPES;
}
