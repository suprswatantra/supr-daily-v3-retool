import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-collection-vertical-card",
    template: `
        <div class="verticalCard" #wrapper>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerticalCardComponent implements OnInit {
    @Input() bgColor: string;
    @Input() borderColor: string;
    @Input() type: string;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    ngOnInit() {
        this.setColors();
    }

    private setColors() {
        this.setBgColor();
        this.setBorderColor();
    }

    private setBgColor() {
        if (!this.bgColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-vertical-card-bg-color",
            this.bgColor
        );
    }

    private setBorderColor() {
        if (!this.borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-vertical-card-border-color",
            this.borderColor
        );
    }
}
