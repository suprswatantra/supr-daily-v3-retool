import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-collection-horizontal-card",
    template: `
        <div class="horizontalCard" [ngClass]="cardClasses" #wrapper>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HorizontalCardComponent implements OnInit {
    @Input() bgColor: string;
    @Input() borderColor: string;
    @Input() bannerClass: string;
    @Input() type: string;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    cardClasses = {};

    ngOnInit() {
        this.setColors();
        this.setClasses();
    }

    private setColors() {
        this.setBgColor();
        this.setBorderColor();
    }

    private setBgColor() {
        if (!this.bgColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-horizontal-card-bg-color",
            this.bgColor
        );
    }

    private setBorderColor() {
        if (!this.borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-horizontal-card-border-color",
            this.borderColor
        );
    }

    private setClasses() {
        this.cardClasses = {
            [this.bannerClass]: !!this.bannerClass,
            item: this.type === "item",
        };
    }
}
