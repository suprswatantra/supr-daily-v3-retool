import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-collection-grid-card",
    template: `
        <div class="gridCard" #wrapper>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["../../styles/display.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridCardComponent implements OnInit {
    @Input() borderColor: string;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    ngOnInit() {
        this.setColors();
    }

    private setColors() {
        this.setBorderColor();
    }

    private setBorderColor() {
        if (!this.borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-grid-card-border-color",
            this.borderColor
        );
    }
}
