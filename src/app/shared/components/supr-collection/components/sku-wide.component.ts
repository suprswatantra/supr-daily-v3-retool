import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    Sku,
    CartItem,
    CollectionItem,
    Vacation,
    CollectionItemTag,
} from "@models";

import { TAG_TYPES } from "@shared/components/supr-collection/constants";

import { Segment } from "@types";

@Component({
    selector: "supr-collection-sku-wide",
    template: `
        <div class="sku wide" #wrapper>
            <div class="tileContainer">
                <supr-product-tile
                    [sku]="sku"
                    [hideCart]="true"
                    [cartItem]="cartItem"
                    [mode]="collectionItem?.preferredMode"
                    [saPosition]="saPosition"
                    [saContextList]="saContextList"
                ></supr-product-tile>
            </div>

            <div class="skuSection">
                <supr-collection-tag-banner
                    [tag]="bannerTag"
                ></supr-collection-tag-banner>
            </div>
        </div>
    `,
    styleUrls: ["../styles/sku.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkuWideComponent implements OnInit {
    @Input() sku: Sku;
    @Input() collectionItem: CollectionItem;
    @Input() cartItem: CartItem;
    @Input() vacation: Vacation;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    bannerTag: CollectionItemTag;

    constructor() {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.setTags();
        this.setBorderColor();
    }

    private setTags() {
        const { tags } = this.collectionItem;
        if (!tags || !tags.length) {
            return;
        }

        this.bannerTag = tags.find(tag => tag.type === TAG_TYPES.BANNER);
    }

    private setBorderColor() {
        const { borderColor } = this.collectionItem;
        if (!borderColor) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--supr-sku-border-color",
            borderColor
        );
    }
}
