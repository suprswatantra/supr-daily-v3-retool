import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { CollectionText, CollectionSeeAllText } from "@models";
import { RouterService } from "@services/util/router.service";
import {
    ANALYTICS_OBJECT_NAMES,
    ANALYTICS_TEXTS,
    DISPLAY_TYPES,
} from "@shared/components/supr-collection/constants";
import { Segment } from "@types";

@Component({
    selector: "supr-collection-header",
    template: `
        <div class="header" #header>
            <ng-container *ngIf="title || subtitle || seeAll">
                <div
                    class="suprRow spaceBetween"
                    (click)="goToCollectionPage()"
                >
                    <div class="headerText" [class.wrap]="showSeeAll">
                        <ng-container *ngIf="title">
                            <supr-text type="subtitle">{{
                                title?.text
                            }}</supr-text>
                        </ng-container>

                        <ng-container *ngIf="subtitle">
                            <supr-text type="caption" class="subtitle">
                                {{ subtitle?.text }}
                            </supr-text>
                        </ng-container>
                    </div>
                    <ng-container *ngIf="showSeeAll">
                        <ng-container [ngSwitch]="displayType">
                            <supr-collection-see-all
                                *ngSwitchCase="DISPLAY_TYPES.HORIZONTAL"
                                [seeAllText]="seeAll"
                                type="button"
                                saImpression
                                [saImpressionEnabled]="
                                    ${SA_IMPRESSIONS_IS_ENABLED.COLLECTION_SEE_ALL}
                                "
                                saClick
                                saObjectName="${ANALYTICS_OBJECT_NAMES
                                    .IMPRESSION.SEE_ALL}"
                                saObjectValue="${ANALYTICS_TEXTS.BUTTON}"
                                [saContextList]="saContextList"
                            >
                            </supr-collection-see-all>
                        </ng-container>
                    </ng-container>
                </div>
                <div class="divider16"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    @Input() title: CollectionText;
    @Input() subtitle: CollectionText;
    @Input() seeAll: CollectionSeeAllText;
    @Input() viewId: number;
    @Input() displayType: string;

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    saContextList: Segment.ContextListItem[] = [];
    showSeeAll = false;
    DISPLAY_TYPES = DISPLAY_TYPES;

    constructor(private routerService: RouterService) {}

    ngOnInit() {
        this.setColors();
        this.setContextList();
        this.setSeeAllFlag();
    }

    goToCollectionPage() {
        if (!this.seeAll) {
            return;
        }

        this.routerService.goToCollectionPage(this.viewId);
    }

    private setColors() {
        this.setTitleColor();
        this.setSubtitleColor();
    }

    private setTitleColor() {
        if (!this.colorExists(this.title)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-collection-title-color",
            this.title.color
        );
    }

    private setSubtitleColor() {
        if (!this.colorExists(this.subtitle)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-collection-subtitle-color",
            this.subtitle.color
        );
    }

    private colorExists(text: CollectionText): boolean {
        return !!(text && text.color);
    }

    private setContextList() {
        this.saContextList.push({
            name: ANALYTICS_TEXTS.COLLECTION,
            value: this.viewId,
        });
    }

    private setSeeAllFlag() {
        this.showSeeAll = !!this.seeAll;
    }
}
