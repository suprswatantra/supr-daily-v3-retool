import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { CollectionText } from "@models";

@Component({
    selector: "supr-collection-footer",
    template: `
        <div #tncWrapper>
            <div class="footer" *ngIf="tnc?.text">
                <div class="divider8"></div>
                <supr-text type="caption">{{ tnc?.text }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../styles/footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
    @Input() tnc: CollectionText;

    @ViewChild("tncWrapper", { static: true }) tncEl: ElementRef;

    ngOnInit() {
        this.setColors();
    }

    private setColors() {
        if (!this.tnc || !this.tnc.text || !this.tnc.color) {
            return;
        }

        this.tncEl.nativeElement.style.setProperty(
            "--supr-tnc-color",
            this.tnc.color
        );
    }
}
