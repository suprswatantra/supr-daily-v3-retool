import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DateService } from "@services/date/date.service";
import { ScheduleService } from "@services/shared/schedule.service";

@Component({
    selector: "supr-schedule-day-header",
    templateUrl: "./supr-schedule-day-header.component.html",
    styleUrls: ["./supr-schedule-day-header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleDayHeaderComponent {
    @Input() paused = false;
    @Input() subtitle: string;
    @Input() tpEnabled = false;
    @Input() actionText: string;
    @Input() actionIcon: string;
    @Input() pausedBySystem = false;
    @Input() actionImpression: {
        name: string;
        value: string;
    };
    @Input() set date(d: string) {
        this._date = d;
        this.setDateDetails();
    }
    get date(): string {
        return this._date;
    }

    @Output() handleActionClick: EventEmitter<void> = new EventEmitter();

    _day: string;
    _isTomorrow = false;

    private _date: string;

    constructor(
        private dateService: DateService,
        private scheduleService: ScheduleService
    ) {}

    private setDateDetails() {
        this._day = this.scheduleService.getScheduleDay(this.date);
        this._isTomorrow = this.dateService.isNthDayFromToday(this.date, 1);
    }
}
