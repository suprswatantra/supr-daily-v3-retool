import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

import { QuantityService } from "@services/util/quantity.service";

import { Sku } from "@shared/models";
import { CLODUINARY_IMAGE_SIZE } from "@constants";
@Component({
    selector: "supr-schedule-card-product",
    templateUrl: "./supr-schedule-card-product.component.html",
    styleUrls: ["./supr-schedule-card-product.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleCardProductComponent implements OnInit, OnChanges {
    quantityText: string;

    @Input() sku: Sku;
    @Input() quantity: number;
    @Input() showCounter = true;
    @Input() plusDisabled = false;
    @Input() counterQuantity: number;

    @Input() saObjectValueCounter: number;

    @Output() handleQuantityUpdate: EventEmitter<number> = new EventEmitter();

    showPriceStrike = false;
    CLODUINARY_IMAGE_SIZE = CLODUINARY_IMAGE_SIZE;

    constructor(private quantityService: QuantityService) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(change: SimpleChanges) {
        const skuChange = change["sku"];
        if (skuChange && !skuChange.firstChange) {
            this.initData();
        }
    }

    private initData() {
        if (this.sku) {
            this.quantityText = this.quantityService.toText(this.sku);
            this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
        }
    }

    setQuantityText() {
        if (this.sku) {
            this.quantityText = this.quantityService.toText(this.sku);
        }
    }
}
