import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { NudgeAdapter as Adapter } from "@shared/adapters/nudge.adapter";
import { User } from "@shared/models";

@Component({
    selector: "supr-campaign-popup",
    template: `
        <supr-campaign-popup-wrapper
            [showModal]="showModal$ | async"
            [userData]="userData$ | async"
            (handleHideModal)="hideCampaignPopupModal()"
        ></supr-campaign-popup-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CamapignPopupComponent implements OnInit {
    showModal$: Observable<boolean>;
    userData$: Observable<User>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.showModal$ = this.adapter.showCampaignPopupModal$;
        this.userData$ = this.adapter.userData$;
    }

    hideCampaignPopupModal() {
        this.adapter.hideCampaignPopupNudge();
    }
}
