import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import {
    MODAL_NAMES,
    NUDGE_SETTINGS_KEYS,
    NUDGE_TYPES,
    CLODUINARY_IMAGE_SIZE,
} from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { SettingsService } from "@services/shared/settings.service";
import { ApiService } from "@services/data/api.service";

import { ExitCartSetting, CampaignPopupSetting } from "@types";
import { User } from "@shared/models";
import { Subscription } from "rxjs";
import { finalize } from "rxjs/operators";

@Component({
    selector: "supr-campaign-popup-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="hideModal()"
                modalName="${MODAL_NAMES.CAMPAIGN_POPUP}"
            >
                <div class="wrapper">
                    <supr-image
                        [src]="nudgeData?.img"
                        [imgWidth]="
                            ${CLODUINARY_IMAGE_SIZE.CAMPAIGN_POPUP.WIDTH}
                        "
                        [imgHeight]="
                            ${CLODUINARY_IMAGE_SIZE.CAMPAIGN_POPUP.HEIGHT}
                        "
                        [withWrapper]="false"
                        [lazyLoad]="false"
                    ></supr-image>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-campaign-popup.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() showModal: boolean;
    @Input() userData: User;
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    nudge: ExitCartSetting.Nudge;
    nudgeData: CampaignPopupSetting.Nudge;

    private apiDeleteCampaignPopup: Subscription;

    constructor(
        private modalService: ModalService,
        private settingsService: SettingsService,
        private apiService: ApiService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleShowModalChange(changes["showModal"]);
    }

    hideModal() {
        this.closeModal();
        this.deleteCampaignApi();
        this.handleHideModal.emit();
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.currentValue &&
            change.currentValue !== change.previousValue
        ) {
            this.initialize();
        }
    }

    private initialize() {
        this.nudgeData = this.settingsService.getSettingsValue(
            NUDGE_SETTINGS_KEYS[NUDGE_TYPES.CAMPAIGN_POPUP]
        );
    }

    private closeModal() {
        this.modalService.closeModal();
    }

    private unsubscribeApiCall() {
        if (
            this.apiDeleteCampaignPopup &&
            !this.apiDeleteCampaignPopup.closed
        ) {
            this.apiDeleteCampaignPopup.unsubscribe();
        }
    }

    private deleteCampaignApi() {
        this.apiDeleteCampaignPopup = this.apiService
            .deleteCampaignPopup(this.userData.id, NUDGE_TYPES.CAMPAIGN_POPUP)
            .pipe(
                finalize(() => {
                    this.unsubscribeApiCall();
                })
            )
            .subscribe();
    }
}
