import {
    Input,
    Component,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";
import { Observable } from "rxjs";

import { RouterService } from "@services/util/router.service";

import {
    CartItem,
    CartRewardSkuBanner,
    CartRewardSkuInfoState,
    Sku,
    Vacation,
} from "@shared/models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

import { CartPageService } from "@pages/cart/services/cart.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { Segment } from "@types";

import { CART_BANNER_CONSTANTS } from "./constants";

@Component({
    selector: "supr-cart-reward-sku-banner-container",
    template: ` <supr-cart-reward-sku-banner
        [config]="skuRewardCartInfo"
        [sku]="sku$ | async"
        [isShowModal]="isShowModal"
        (handleActionClick)="handleActionClick()"
        (handleStateChange)="setAddRewardSkuState()"
        saImpression
        saObjectName="${CART_BANNER_CONSTANTS.ANALYTICS.REWARD_CART_BANNER}"
        [saContext]="skuRewardCartInfo?.basket_offer_id"
        [saContextList]="getContextList()"
    ></supr-cart-reward-sku-banner>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartRewardRewardSkuBannerContainer implements OnInit {
    @Input() skuRewardCartInfo: CartRewardSkuBanner;

    sku$: Observable<Sku>;
    vacation$: Observable<Vacation>;
    cartItems$: Observable<CartItem[]>;
    addRewardSku$: Observable<Boolean>;

    skuItem: Sku;
    vacation: Vacation;
    cartItems: CartItem[];
    isShowModal = false;
    addRewardSku = false;
    cartBannerState: string;

    constructor(
        private routerService: RouterService,
        private cartPageService: CartPageService,
        private adapter: Adapter,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.sku$ = this.adapter.getSkuById(
            this.skuRewardCartInfo.reward_sku_id
        );
        this.addRewardSku$ = this.adapter.addRewardSku$;
        this.vacation$ = this.adapter.vacation$;
        this.cartItems$ = this.adapter.cartItems$;

        this.init();
    }

    handleActionClick() {
        const state = this.skuRewardCartInfo.state_info.state;

        if (state === CartRewardSkuInfoState.ADD_MORE) {
            this.handleCta();
        } else if (state === CartRewardSkuInfoState.ADD_TO_CART) {
            this.setAddRewardSkuState(true);
            this.handleAddToCart();
        } else {
            this.isShowModal = !this.isShowModal;
        }

        this.setCartBannerState(state);
        this.handleSaClick();
    }

    getContextList() {
        if (!this.skuItem) {
            return;
        }

        const list = [
            {
                name: CART_BANNER_CONSTANTS.ANALYTICS.REWARD_TYPE,
                value: CART_BANNER_CONSTANTS.ANALYTICS.SKU_REWARD_CART_BANNER,
            },
            {
                name: CART_BANNER_CONSTANTS.ANALYTICS.REWARD_SKU_ID,
                value: this.skuRewardCartInfo.reward_sku_id,
            },
            {
                name: CART_BANNER_CONSTANTS.ANALYTICS.REWARD_STATE,
                value: this.cartBannerState,
            },
        ];

        return list || [];
    }

    setAddRewardSkuState(state = false) {
        this.adapter.updateAddRewardSku(state);
    }

    private handleCta() {
        const cta = this.skuRewardCartInfo.state_info.cta;
        if (cta.source === "in_app") {
            this.routerService.goToUrl(cta.link);
        } else {
            window.open(String(cta.link), "_system", "location=yes");
        }
    }

    private handleAddToCart() {
        const cartBody = this.cartPageService.getValidateCartBody(
            this.cartItems
        );

        this.adapter.validateCart(cartBody, this.addRewardSku);
    }

    private init() {
        this.setSkuData();
        this.setVacationData();
        this.setCartItemData();
        this.setAddRewardSkuData();

        this.setCartBannerState(this.skuRewardCartInfo.state_info.state);
    }

    private setCartBannerState(state: string) {
        this.cartBannerState = state;
    }

    private setSkuData() {
        this.sku$.pipe().subscribe((skuItem) => {
            this.skuItem = skuItem;
        });
    }

    private setVacationData() {
        this.vacation$.pipe().subscribe((vacation) => {
            this.vacation = vacation;
        });
    }

    private setCartItemData() {
        this.cartItems$.pipe().subscribe((cartItems) => {
            this.cartItems = cartItems;
        });
    }

    private setAddRewardSkuData() {
        this.addRewardSku$.pipe().subscribe((state: boolean) => {
            this.addRewardSku = state;
        });
    }

    handleSaClick() {
        const traits = this.setTraits();
        this.analyticsService.trackClick(traits);
    }

    private setTraits(): Segment.Traits {
        const traits: Segment.Traits = {};

        traits.objectName = CART_BANNER_CONSTANTS.ANALYTICS.REWARD_CART_BANNER;
        traits.context = String(this.skuRewardCartInfo.basket_offer_id);
        traits.context_1_name = CART_BANNER_CONSTANTS.ANALYTICS.REWARD_TYPE;
        traits.context_1_value =
            CART_BANNER_CONSTANTS.ANALYTICS.SKU_REWARD_CART_BANNER;
        traits.context_2_name = CART_BANNER_CONSTANTS.ANALYTICS.REWARD_SKU_ID;
        traits.context_2_value = String(this.skuItem.id);
        traits.context_3_name = CART_BANNER_CONSTANTS.ANALYTICS.REWARD_STATE;
        traits.context_3_value = this.cartBannerState;

        return traits;
    }
}
