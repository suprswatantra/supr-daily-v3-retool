import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
    OnChanges,
    SimpleChanges,
    OnInit,
    SimpleChange,
} from "@angular/core";
import {
    CartRewardSkuBanner,
    CartRewardSkuInfoState,
    Sku,
} from "@shared/models";

import { MODAL_NAMES } from "@constants";
import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-cart-reward-sku-banner",
    template: ` <div class="wrapper" *ngIf="config">
        <div class="suprRow wrapperContainer">
            <div
                class="suprColumn stretch top left"
                [class.contentWrapper]="sku?.image"
            >
                <ng-container *ngIf="config?.title">
                    <supr-text type="body" class="title">{{
                        config?.title
                    }}</supr-text>
                </ng-container>

                <div class="divider4"></div>
                <div class="suprColumn left">
                    <div class="suprRow">
                        <ng-container
                            *ngIf="
                                config?.state_info?.state !==
                                    cartRewardSkuInfoState.ADDED_TO_CART;
                                else withoutAddMore
                            "
                        >
                            <div class="suprRow">
                                <supr-text type="caption" class="subTitle">
                                    {{ config?.description }}
                                    <span class="suprRow">
                                        <a
                                            class="knowMore"
                                            (click)="showModal()"
                                        >
                                            {{ knowMoreText }}
                                        </a>
                                        <supr-icon
                                            name="chevron_right"
                                            class="ctaIcon"
                                        ></supr-icon>
                                    </span>
                                </supr-text>
                                <span> </span>
                            </div>
                        </ng-container>
                        <ng-template #withoutAddMore>
                            <supr-text type="caption" class="subTitle">
                                {{ config?.description }}
                            </supr-text>
                        </ng-template>
                    </div>

                    <div class="divider16"></div>

                    <div class="suprRow cta" (click)="handleActionClick.emit()">
                        <supr-text type="caption" class="cta">
                            {{ config.state_info.text }}
                        </supr-text>

                        <supr-icon
                            name="chevron_right"
                            class="ctaIcon"
                        ></supr-icon>
                    </div>
                </div>
            </div>

            <div *ngIf="sku?.image" class="suprColumn stretch top imageWrapper">
                <supr-image [image]="sku?.image"></supr-image>
            </div>
        </div>
        <ng-container *ngIf="isShowModal">
            <supr-modal
                modalName="${MODAL_NAMES.CART_PROMPT_REWARD_SKU_BANNER}"
                (handleClose)="closeModal()"
            >
                <div class="cartBannerModal suprColumn left">
                    <div
                        class="cartBannerModalItem suprRow"
                        *ngFor="let item of cartBannerSkuKnowMore"
                    >
                        <supr-text type="body">- {{ item }}</supr-text>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    </div>`,
    styleUrls: ["../styles/reward-sku-banner.components.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartRewardRewardSkuBanner implements OnChanges, OnInit {
    @Input() config: CartRewardSkuBanner;
    @Input() sku: Sku;
    @Input() isShowModal: boolean;

    @Output() handleActionClick: EventEmitter<void> = new EventEmitter();
    @Output() handleStateChange: EventEmitter<void> = new EventEmitter();

    cartRewardSkuInfoState = CartRewardSkuInfoState;
    cartBannerSkuKnowMore: string[];
    knowMoreText = "Know more";

    constructor(private settingService: SettingsService) {}

    ngOnInit() {
        if (this.config && this.config.tnc) {
            this.cartBannerSkuKnowMore = this.config.tnc;
        } else {
            this.cartBannerSkuKnowMore = this.settingService.getSettingsValue(
                "cartBannerSkuKnowMore",
                []
            );
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["isShowModal"];
        const configChange = changes["config"];

        if (change && !change.firstChange) {
            if (change.currentValue !== undefined) {
                this.showModal();
            }
        }

        this.handleConfigChange(configChange);
    }

    private handleConfigChange(change: SimpleChange) {
        if (change && !change.firstChange) {
            if (
                change.currentValue.state_info.state !==
                CartRewardSkuInfoState.ADDED_TO_CART
            ) {
                this.handleStateChange.emit();
            }
        }
    }

    showModal() {
        this.isShowModal = true;
    }

    closeModal() {
        this.isShowModal = false;
    }
}
