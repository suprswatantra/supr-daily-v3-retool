import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CartBannerConfig, CartBannerTypes } from "@types";

@Component({
    selector: "supr-cart-banner",
    template: `
        <div
            class="wrapper"
            *ngIf="config"
            [class.caution]="config?.type === '${CartBannerTypes.CAUTION}'"
            [class.noBorder]="isIconRightAligned"
            [class.success]="config?.type === '${CartBannerTypes.SUCCESS}'"
        >
            <div class="suprRow">
                <ng-template
                    [ngTemplateOutlet]="iconBlock"
                    *ngIf="!isIconRightAligned"
                ></ng-template>
                <div
                    class="suprColumn stretch top left"
                    [class.contentWrapper]="config?.icon || config?.imageUrl"
                >
                    <ng-container *ngIf="config?.title">
                        <supr-text type="body" class="title">{{
                            config?.title
                        }}</supr-text>
                    </ng-container>

                    <ng-container
                        *ngIf="config?.titles && config.titles.length"
                    >
                        <ng-container *ngFor="let title of config.titles">
                            <div class="suprRow top">
                                <supr-icon
                                    name="tick"
                                    class="tickIcon"
                                ></supr-icon>
                                <supr-text type="body" class="title">
                                    {{ title }}
                                </supr-text>
                            </div>
                            <div class="divider8"></div>
                        </ng-container>
                    </ng-container>

                    <ng-container *ngIf="config?.subtitle">
                        <div class="divider4"></div>
                        <supr-text type="caption" class="subTitle">
                            {{ config?.subtitle }}
                        </supr-text>
                    </ng-container>

                    <ng-container *ngIf="config?.cta">
                        <div class="divider12"></div>

                        <div
                            class="suprRow"
                            (click)="handleActionClick?.emit()"
                        >
                            <supr-text type="subtext10" class="cta">
                                {{ config?.cta }}
                            </supr-text>
                            <supr-icon
                                class="cta"
                                *ngIf="config?.ctaIcon"
                                [name]="config?.ctaIcon"
                            ></supr-icon>
                        </div>
                    </ng-container>
                </div>
                <ng-template
                    [ngTemplateOutlet]="iconBlock"
                    *ngIf="isIconRightAligned"
                ></ng-template>

                <ng-template #iconBlock>
                    <div
                        *ngIf="config?.icon"
                        class="suprColumn stretch top iconWrapper"
                    >
                        <supr-icon [name]="config?.icon"></supr-icon>
                    </div>
                    <div
                        *ngIf="config?.imageUrl"
                        class="suprColumn stretch top imageWrapper"
                    >
                        <supr-image [src]="config?.imageUrl"></supr-image>
                    </div>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["./styles/supr-cart-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCartBannerComponent {
    @Input() config: CartBannerConfig;
    @Input() isIconRightAligned: boolean;

    @Output() handleActionClick: EventEmitter<void> = new EventEmitter();
}
