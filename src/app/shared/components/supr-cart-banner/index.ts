import { CartRewardRewardSkuBanner } from "./components/reward-sku-banner.components";
import { SuprCartBannerComponent } from "./supr-cart-banner.component";
import { CartRewardRewardSkuBannerContainer } from "./supr-cart-banner.container";

export const cartBannersComponents = [
    CartRewardRewardSkuBanner,
    SuprCartBannerComponent,
    CartRewardRewardSkuBannerContainer,
];
