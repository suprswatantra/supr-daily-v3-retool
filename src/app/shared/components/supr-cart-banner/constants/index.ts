export const CART_BANNER_CONSTANTS = {
    ANALYTICS: {
        REWARD_CART_BANNER: "reward-cart-banner",
        REWARD_TYPE: "reward_type",
        SKU_REWARD_CART_BANNER: "sku-reward-cart-banner",
        REWARD_SKU_ID: "reward_sku_id",
        REWARD_STATE: "reward_state",
    },
};
