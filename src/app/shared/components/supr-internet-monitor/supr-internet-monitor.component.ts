import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
    ChangeDetectorRef,
    Renderer2,
} from "@angular/core";

import { fromEvent, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { INTERNET_TEXTS } from "@constants";

const HIDE_BANNER_TIMEOUT = 2 * 1000; // 2 sec
const HANDLE_INTERNET_DELAY = 2 * 1000; // 2 sec
const CHECK_STATUS_POST_RESUME_INTERVAL = 4 * 1000; // 4 sec

@Component({
    selector: "supr-internet-monitor",
    template: `
        <div
            class="wrapper suprColumn center"
            [class.online]="online"
            [class.show]="showBanner"
        >
            <supr-text type="paragraph">{{ message }}</supr-text>
        </div>
    `,
    styleUrls: ["./supr-internet-monitor.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InternetMonitorComponent implements OnInit, OnDestroy {
    message: string;
    showBanner: boolean;
    online: boolean;
    appBackground = false;

    private _onDestroy = new Subject();
    private intervalHandle: number;

    constructor(private cdr: ChangeDetectorRef, private renderer: Renderer2) {}

    ngOnInit() {
        this.registerEvents();
    }

    ngOnDestroy() {
        // Unsubscribe from the subscribed events if any
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    private registerEvents() {
        this.subscribeForOnline();
        this.subscribeForOffline();
    }

    private subscribeForOnline() {
        fromEvent(window, "online")
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                setTimeout(() => this.checkInternet(), HANDLE_INTERNET_DELAY);
            });
    }

    private subscribeForOffline() {
        fromEvent(window, "offline")
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                setTimeout(() => this.checkInternet(), HANDLE_INTERNET_DELAY);
            });
    }

    private handleOnline() {
        this.message = INTERNET_TEXTS.ONLINE;
        this.online = true;
        this.cdr.detectChanges();

        this.hideBanner();
        this.turnOffInternetCheck();
    }

    private handleOffline() {
        this.online = false;
        this.message = INTERNET_TEXTS.OFFLINE;

        this._showBanner();
        this.turnOnInternetCheck();
    }

    private hideBanner() {
        window.setTimeout(() => {
            this._hideBanner();
        }, HIDE_BANNER_TIMEOUT);
    }

    private _showBanner() {
        this.renderer.addClass(document.body, "offline");

        this.showBanner = true;
        this.cdr.detectChanges();
    }

    private _hideBanner() {
        this.renderer.removeClass(document.body, "offline");

        this.showBanner = false;
        this.cdr.detectChanges();
    }

    private turnOnInternetCheck() {
        // Turn off previous one
        this.turnOffInternetCheck();

        // Set the new one
        this.intervalHandle = window.setInterval(
            () => this.checkInternet(),
            CHECK_STATUS_POST_RESUME_INTERVAL
        );
    }

    private turnOffInternetCheck() {
        if (this.intervalHandle) {
            window.clearInterval(this.intervalHandle);
        }
    }

    private checkInternet() {
        if (navigator.onLine) {
            this.handleOnline();
        } else {
            this.handleOffline();
        }
    }
}
