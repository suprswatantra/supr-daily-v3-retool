import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES, CLODUINARY_IMAGE_SIZE } from "@constants";

import { CheckoutInfo } from "@shared/models/user.model";
import { GOT_IT_TEXT } from "../constants";

import { ModalService } from "@services/layout/modal.service";

@Component({
    selector: "supr-checkout-disabled-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.ORDER_THROTTLING}"
            >
                <div class="wrapper">
                    <ng-container *ngIf="userCheckoutInfo?.content?.imageUrl">
                        <div class="suprRow center">
                            <supr-image
                                [lazyLoad]="false"
                                [imgWidth]="
                                    ${CLODUINARY_IMAGE_SIZE.CHECKOUT_DISABLED
                                        .WIDTH}
                                "
                                [src]="userCheckoutInfo?.content?.imageUrl"
                            ></supr-image>
                        </div>
                        <div class="divider20"></div>
                    </ng-container>

                    <supr-text type="subtitle">{{
                        userCheckoutInfo?.content?.title
                    }}</supr-text>
                    <div class="divider8"></div>

                    <supr-text type="body">
                        {{ userCheckoutInfo?.content?.description }}
                    </supr-text>
                    <div class="divider24"></div>

                    <supr-button (handleClick)="closeModal()">
                        <supr-text type="body">
                            ${GOT_IT_TEXT}
                        </supr-text>
                    </supr-button>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-checkout-disabled.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() showModal: boolean;
    @Input() userCheckoutInfo: CheckoutInfo;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    constructor(private modalService: ModalService) {}

    closeModal() {
        this.modalService.closeModal();
    }
}
