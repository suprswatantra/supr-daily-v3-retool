import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { UserAdapter as Adapter } from "@shared/adapters/user.adapter";
import { CheckoutInfo } from "@shared/models/user.model";

@Component({
    selector: "supr-checkout-disabled",
    template: `
        <supr-checkout-disabled-wrapper
            [showModal]="showModal$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            (handleHideModal)="hideCheckoutDisabledNudge()"
        ></supr-checkout-disabled-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckoutDisabledComponent implements OnInit {
    userCheckoutInfo$: Observable<CheckoutInfo>;
    showModal$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.userCheckoutInfo$ = this.adapter.userCheckoutInfo$;
        this.showModal$ = this.adapter.showCheckoutDisabledNudge$;
    }

    hideCheckoutDisabledNudge() {
        this.adapter.hideCheckoutDisabledNudge();
    }
}
