import { CheckoutDisabledComponent } from "./supr-checkout-disabled.component";
import { WrapperComponent } from "./components/wrapper.component";

export const checkoutDisabledComponents = [
    CheckoutDisabledComponent,
    WrapperComponent,
];
