import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

@Component({
    selector: "supr-exit-cart",
    template: `
        <supr-exit-cart-wrapper
            [showModal]="showModal$ | async"
            (handleHideModal)="hideExitCartModal()"
        ></supr-exit-cart-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExitCartComponent implements OnInit {
    showModal$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.showModal$ = this.adapter.showExitCartModal$;
    }

    hideExitCartModal() {
        this.adapter.hideExitCartModal();
    }
}
