import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import { MODAL_NAMES, NUDGE_SETTINGS_KEYS, NUDGE_TYPES } from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";
import { PlatformService } from "@services/util/platform.service";

import { ExitCartSetting } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics";

@Component({
    selector: "supr-exit-cart-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="hideModal()"
                modalName="${MODAL_NAMES.EXIT_CART}"
            >
                <div class="wrapper">
                    <div class="suprRow">
                        <supr-icon name="error"></supr-icon>
                        <div class="spacer8"></div>
                        <supr-text type="subtitle">
                            {{ nudge.title }}
                        </supr-text>
                    </div>
                    <div class="divider12"></div>

                    <supr-text type="body" class="subtitle">
                        {{ nudge.subTitle }}
                    </supr-text>
                    <div class="divider24"></div>

                    <div class="suprRow">
                        <div class="suprColumn">
                            <supr-button
                                class="leave"
                                (handleClick)="secondaryBtnClick()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .LEAVE_BUTTON}"
                            >
                                <supr-text type="body">
                                    {{ nudge.secondaryButton?.text }}
                                </supr-text>
                            </supr-button>
                        </div>
                        <div class="spacer8"></div>

                        <div class="suprColumn">
                            <supr-button
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .GO_TO_CART_BUTTON}"
                                (handleClick)="primaryBtnClick()"
                            >
                                <supr-text type="body">
                                    {{ nudge.primaryButton?.text }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-exit-cart.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() showModal: boolean;
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    nudge: ExitCartSetting.Nudge;

    constructor(
        private modalService: ModalService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private platformService: PlatformService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleShowModalChange(changes["showModal"]);
    }

    hideModal() {
        this.handleHideModal.emit();
    }

    primaryBtnClick() {
        this.closeModal();
        this.goToCartPage();
    }

    secondaryBtnClick() {
        this.platformService.exitApp();
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        this.getExitCartNudgeData();
        this.vibrateDevice();
    }

    private vibrateDevice() {
        if (this.nudge.vibrationEnabled) {
            this.platformService.vibrate(this.nudge.vibrationTimeInMs);
        }
    }

    private getExitCartNudgeData() {
        const key = NUDGE_SETTINGS_KEYS[NUDGE_TYPES.EXIT_CART];
        this.nudge = this.settingsService.getSettingsValue(key, {});
    }

    private closeModal() {
        this.modalService.closeModal();
    }

    private goToCartPage() {
        this.routerService.goToCartPage();
    }
}
