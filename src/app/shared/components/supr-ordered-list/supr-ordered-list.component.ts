import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-ordered-list",
    template: `
        <ol>
            <li *ngFor="let item of list; trackBy: trackByFn">
                <supr-text type="body">{{ item }}</supr-text>
            </li>
        </ol>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["./supr-ordered-list.component.scss"],
})
export class OrderedListComponent {
    @Input() list: string[] = [];

    trackByFn(_: any, index: number): number {
        return index;
    }
}
