import {
    Input,
    OnInit,
    Component,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { UtilService } from "@services/util/util.service";

import { ConfigurableIcon } from "@types";

import { STYLE_LIST } from "./constants";

@Component({
    selector: "supr-configurable-icon",
    template: `<div #iconWrapper class="wrapper">
        <supr-svg *ngIf="config?.iconType === 'svg'; else suprIcon"></supr-svg>

        <ng-template #suprIcon>
            <supr-icon [name]="config?.iconName"> </supr-icon>
        </ng-template>
    </div>`,
    styleUrls: ["./supr-configurable-icon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprConfigurableIconComponent implements OnInit {
    @Input() config: ConfigurableIcon;

    @ViewChild("iconWrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.config) {
            return;
        }

        this.utilService.setStyles(this.config, STYLE_LIST, this.wrapperEl);
    }
}
