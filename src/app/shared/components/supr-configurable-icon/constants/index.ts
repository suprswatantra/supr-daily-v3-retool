export const STYLE_LIST = [
    {
        attributeName: "iconSize",
        attributeStyleVariableName: "--supr-configurable-icon-size",
    },
    {
        attributeName: "iconColor",
        attributeStyleVariableName: "--supr-configurable-icon-color",
    },
];
