import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { SubscriptionAdapter } from "@shared/adapters/subscription.adapter";

import {
    Sku,
    CartItem,
    Vacation,
    CheckoutInfo,
    Subscription,
    OutOfStockInfoStateType,
} from "@models";

import { Segment, FetchSimilarProductsActionData } from "@types";

@Component({
    selector: "supr-add-button",
    template: `
        <supr-add-button-wrapper
            [sku]="sku"
            [mode]="mode"
            [fromCatalog]="fromCatalog"
            [vacation]="vacation$ | async"
            [cartItem]="cartItem$ | async"
            [autoIncrement]="autoIncrement"
            [showRoundAddBtn]="showRoundAddBtn"
            [oosInfoState]="oosInfoState$ | async"
            [subscription]="subscription$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            [isOosUpfront]="isOosUpfront"
            [saContext]="saContext"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
            (handleOosInfoUpdate)="oosInfoUpdate()"
            (handleQuantityUpdate)="updateItemInCart($event)"
            (handlePostAddToCart)="handlePostAddToCart.emit()"
            (handleOosInfoUpdateDep)="oosInfoUpdateDeprecate()"
            (fetchSimilarProducts)="fetchSimilarProducts($event)"
            (handleSubModalStateChange)="handleSubModalStateChange.emit($event)"
        ></supr-add-button-wrapper>
    `,
    styleUrls: ["./supr-add-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddButtonComponent implements OnInit {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() saContext: any;
    @Input() fromCatalog = true;
    @Input() saPosition: number;
    @Input() showRoundAddBtn = false;
    @Input() autoIncrement = false;
    @Input() isOosUpfront = false;
    @Input() saContextList: Segment.ContextListItem[];

    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handleSubModalStateChange: EventEmitter<
        boolean
    > = new EventEmitter();

    cartItem$: Observable<CartItem>;
    vacation$: Observable<Vacation>;
    isAnonymousUser$: Observable<boolean>;
    subscription$: Observable<Subscription>;
    userCheckoutInfo$: Observable<CheckoutInfo>;
    oosInfoState$: Observable<OutOfStockInfoStateType>;

    constructor(
        private adapter: Adapter,
        private skuAdapter: SkuAdapter,
        private userAdapter: UserAdapter,
        private subscriptionAdapter: SubscriptionAdapter
    ) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.vacation$ = this.adapter.vacation$;
        this.cartItem$ = this.adapter.getCartItemById(this.sku.id);
        this.userCheckoutInfo$ = this.userAdapter.userCheckoutInfo$;
        this.oosInfoState$ = this.skuAdapter.isNotified(this.sku.id);
        this.subscription$ = this.subscriptionAdapter.getActiveSubscriptionBySkuId(
            this.sku.id
        );
        this.isAnonymousUser$ = this.userAdapter.isAnonymousUser$;
    }

    updateItemInCart(item: CartItem) {
        this.adapter.updateItemInCart(item);
    }

    oosInfoUpdate() {
        if (this.sku) {
            this.skuAdapter.updateSkuOOSInfo(this.sku.id, "oos");
        }
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    oosInfoUpdateDeprecate() {
        if (this.sku) {
            this.skuAdapter.updateSkuOosInfo_deprecate(
                this.sku.id,
                "oos",
                true
            );
        }
    }

    fetchSimilarProducts(data: FetchSimilarProductsActionData) {
        this.skuAdapter.fetchSimilarProducts(data);
    }
}
