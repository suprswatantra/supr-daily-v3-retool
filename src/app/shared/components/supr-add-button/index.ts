import { AddButtonComponent } from "./supr-add-button.component";
import { WrapperComponent } from "./components/wrapper.component";

export const addButtonComponents = [AddButtonComponent, WrapperComponent];
