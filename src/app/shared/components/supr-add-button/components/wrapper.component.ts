import {
    Input,
    Output,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    ANALYTICS_OBJECT_NAMES,
    CART_ITEM_TYPES,
    EVENT_TYPES,
    OOS_INFO_STATES,
    SKU_PREFERRED_MODE,
} from "@constants";

import {
    Sku,
    CartItem,
    Vacation,
    CheckoutInfo,
    Subscription,
    OutOfStockInfoStateType,
} from "@models";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { AuthService } from "@services/data/auth.service";
import { SkuService } from "@services/shared/sku.service";
import { CartService } from "@services/shared/cart.service";
import { RouterService } from "@services/util/router.service";
import { ModalService } from "@services/layout/modal.service";
import { CalendarService } from "@services/date/calendar.service";
import { OOSInfoService } from "@services/shared/oos-info.service";
import { SettingsService } from "@services/shared/settings.service";
import { MoengageService } from "@services/integration/moengage.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { Segment, FetchSimilarProductsActionData } from "@types";

import { TEXTS } from "@pages/search/constants";

@Component({
    selector: "supr-add-button-wrapper",
    template: `
        <div class="roundBtnContainer" *ngIf="showRoundAddBtn">
            <div class="btnContainer" [ngClass]="{ expand: _showBtn }">
                <div
                    *ngIf="
                        (isAddOnItem() || !isDirectSubscription()) &&
                        !isSubscriptionItem()
                    "
                    class="wrapperCounter"
                >
                    <supr-number-counter
                        class="suprColumn stretch top left tileCounter"
                        [mode]="mode"
                        [quantity]="cartItem?.quantity"
                        [autoIncrement]="autoIncrement"
                        [saContext]="saContext"
                        [saObjectValue]="sku?.id"
                        [saPosition]="saPosition"
                        [saContextList]="saContextList"
                        (handleClick)="_handleClick($event)"
                    >
                    </supr-number-counter>
                </div>
                <supr-subscription-delivery-schedule-container
                    *ngIf="_showScheduleModal"
                    [subscription]="subscription"
                    [showModal]="_showScheduleModal"
                    secondaryActionText="${TEXTS.DELIVER_ONCE}"
                    (handleModalClose)="closeScheduleModal()"
                ></supr-subscription-delivery-schedule-container>
            </div>

            <div
                class="suprColumn center addRoundIcon"
                *ngIf="!_showBtn"
                (click)="_handleAddBtn()"
                saClick
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.ADD_PRODUCT}"
                [saContext]="saContext"
                [saObjectValue]="sku?.id"
                [saPosition]="saPosition"
                [saContextList]="saContextList"
            >
                <supr-icon name="add"></supr-icon>
            </div>
        </div>

        <div *ngIf="!showRoundAddBtn">
            <div
                *ngIf="
                    (isAddOnItem() || !isDirectSubscription()) &&
                    !isSubscriptionItem()
                "
                class="wrapperCounter"
            >
                <supr-number-counter
                    class="suprColumn stretch top left tileCounter"
                    [mode]="mode"
                    [quantity]="cartItem?.quantity"
                    [autoIncrement]="autoIncrement"
                    [saContext]="saContext"
                    [saObjectValue]="sku?.id"
                    [saPosition]="saPosition"
                    [saContextList]="saContextList"
                    (handleClick)="_handleClick($event)"
                >
                </supr-number-counter>
            </div>
            <supr-subscription-delivery-schedule-container
                *ngIf="_showScheduleModal"
                [subscription]="subscription"
                [showModal]="_showScheduleModal"
                secondaryActionText="${TEXTS.DELIVER_ONCE}"
                (handleModalClose)="closeScheduleModal()"
            ></supr-subscription-delivery-schedule-container>
        </div>
    `,
    styleUrls: ["../supr-add-button.wrapper.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() vacation: Vacation;
    @Input() cartItem: CartItem;
    @Input() fromCatalog: boolean;
    @Input() autoIncrement = false;
    @Input() showRoundAddBtn = false;
    @Input() isOosUpfront: boolean;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() oosInfoState: OutOfStockInfoStateType;
    @Input() set subscription(data: Subscription) {
        this._subscription = data;
        this.setSubscriptionFlag(data);
    }
    get subscription(): Subscription {
        return this._subscription;
    }

    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() isAnonymousUser: boolean;
    @Input() saContextList: Segment.ContextListItem[];

    @Output() handlePostAddToCart: EventEmitter<void> = new EventEmitter();
    @Output() handleOosInfoUpdate: EventEmitter<void> = new EventEmitter();
    @Output() handleOosInfoUpdateDep: EventEmitter<void> = new EventEmitter();
    @Output() handleQuantityUpdate: EventEmitter<CartItem> = new EventEmitter();
    @Output() handleSubModalStateChange: EventEmitter<
        boolean
    > = new EventEmitter();
    @Output() fetchSimilarProducts: EventEmitter<
        FetchSimilarProductsActionData
    > = new EventEmitter();

    _showBtn = false;
    _hasSubscription = false;
    _showScheduleModal = false;
    _impressionCalled = false;

    private _subscription: Subscription;

    constructor(
        private skuService: SkuService,
        private cdr: ChangeDetectorRef,
        private dateService: DateService,
        private cartService: CartService,
        private utilService: UtilService,
        private authService: AuthService,
        private modalService: ModalService,
        private routerService: RouterService,
        private oosInfoService: OOSInfoService,
        private calendarService: CalendarService,
        private moengageService: MoengageService,
        private settingsService: SettingsService,
        private blockAccessService: BlockAccessService
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["cartItem"];

        if (this.showRoundAddBtn && !this.cartItem) {
            this._showBtn = false;
        } else if (change && change.currentValue) {
            this._showBtn = true;
        }
    }

    _handleClick(direction: number) {
        if (this.isAnonymousUser) {
            this.authService.logout();
            return;
        }
        if (this.cartService.isCheckoutDisabled(this.userCheckoutInfo)) {
            return;
        }

        this.updateQty(direction);
    }

    _handleAddBtn() {
        if (!this._showBtn) {
            this._handleClick(1);
            this._showBtn = true;
        }
    }

    isAddOnItem(): boolean {
        if (this.utilService.isEmpty(this.cartItem)) {
            return false;
        }

        return (
            this.cartItem.type !== CART_ITEM_TYPES.SUBSCRIPTION_NEW &&
            this.cartItem.type !== CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        );
    }

    isSubscriptionItem(): boolean {
        if (this.utilService.isEmpty(this.cartItem)) {
            return false;
        }

        return (
            this.cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW ||
            this.cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        );
    }

    isDirectSubscription(): boolean {
        return this.mode === SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE;
    }

    closeScheduleModal() {
        this._showScheduleModal = false;
        this.handleSubModalStateChange.emit(false);
    }

    /* Allow user to add from subscription balance only when the intended
    date is on or after the subscription start date */
    private setSubscriptionFlag(subscription: Subscription) {
        const intendedDateText = this.cartService.getDeliveryDate();
        const intendedDate = intendedDateText
            ? this.dateService.dateFromText(intendedDateText)
            : this.calendarService.getNextAvailableDate(
                  this.vacation,
                  this.sku
              );
        const subStartDateText = this.utilService.getNestedValue(
            subscription,
            "delivery_dates.start"
        );
        const subStartDate =
            subStartDateText && this.dateService.dateFromText(subStartDateText);

        this._hasSubscription =
            intendedDate &&
            subStartDate &&
            this.dateService.daysBetweenTwoDates(intendedDate, subStartDate) <=
                0;
    }

    private openScheduleModal() {
        this._showScheduleModal = true;

        this.handleSubModalStateChange.emit(true);
    }

    private updateQty(direction: number) {
        if (direction > 0) {
            this.increment();
        } else if (direction < 0) {
            this.decrement();
        }

        if (this.isOosUpfront) {
            this.trackAnalytics();
        }

        this.cdr.detectChanges();
    }

    private increment() {
        if (!this.sku) {
            return;
        }

        const alternatesV2 = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );

        if (alternatesV2) {
            return this.handleAlternatesV2Flow();
        }

        this.handleAlternatesV1Flow();
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    private handleAlternatesV1Flow() {
        let cartItem: CartItem;
        /* Re-route to alternate products view if product is oos */
        if (
            this.cartService.showAlternatesFlow(this.sku, "addon") &&
            !this.routerService.isAlternatesScreen() &&
            this.sku.oos_info &&
            this.sku.oos_info.navigate
        ) {
            return this.routerService.goToOOSAlternatesPage(this.sku.id, {
                queryParams: {
                    intent: CART_ITEM_TYPES.ADDON,
                },
            });
        }

        /* Allow user to use subscription balance if the product is in stock,
        has a subscription */
        if (this._hasSubscription && this.utilService.isEmpty(this.cartItem)) {
            this.openScheduleModal();
            return;
        }

        if (this.sku.oos_info && !this.sku.oos_info.navigate) {
            this.trackAnalytics();
            this.handleOosInfoUpdateDep.emit();
        } else {
            /* Adding the SKU to cart for first time */
            if (this.utilService.isEmpty(this.cartItem)) {
                /* Block add to cart in case of access restrictions */
                if (
                    this.sku.out_of_stock &&
                    this.sku.next_available_date &&
                    this.blockAccessService.isFutureDeliveryAccessRestricted()
                ) {
                    this.blockAccessService.showBlockAcessNudge();
                } else {
                    this.addToCart();
                }
            } else {
                /* SKU already exists in cart */
                const updatedQty = this.getCurrentSkuQty() + 1;

                cartItem = this.cartService.updateCartItemWithQty(
                    this.cartItem,
                    updatedQty
                );
            }

            if (cartItem) {
                /* Updating store */
                this.handleQuantityUpdate.emit(cartItem);
            }
        }
    }

    private handleAlternatesV2Flow() {
        let cartItem: CartItem;

        if (!this.sku.out_of_stock) {
            /* Allow user to use subscription balance if the product is in stock,
            has a subscription */
            if (
                this._hasSubscription &&
                this.utilService.isEmpty(this.cartItem)
            ) {
                this.openScheduleModal();
                return;
            }
        }

        /* if sku is either available immediately or at a certain date, add it to cart */
        if (!this.sku.out_of_stock || this.sku.next_available_date) {
            /* adding the sku to cart for first time */
            if (this.utilService.isEmpty(this.cartItem)) {
                /* Block add to cart in case of access restrictions */
                if (
                    this.sku.out_of_stock &&
                    this.sku.next_available_date &&
                    this.blockAccessService.isFutureDeliveryAccessRestricted()
                ) {
                    this.blockAccessService.showBlockAcessNudge();
                } else {
                    this.addToCart();
                }

                /* If sku was added for t+x date, send out-of-stock-sku analytics */
                if (this.sku.out_of_stock && this.sku.next_available_date) {
                    this.trackAnalytics();
                }
            } else {
                /* sku already exists in cart */
                const updatedQty = this.getCurrentSkuQty() + 1;

                cartItem = this.cartService.updateCartItemWithQty(
                    this.cartItem,
                    updatedQty
                );
            }

            if (cartItem) {
                /* Updating store */
                this.handleQuantityUpdate.emit(cartItem);
            }
        }

        const showAlternatesFlow = this.cartService.showAlternatesFlow(
            this.sku,
            "addon"
        );
        const deliveryDate = this.cartService.getDeliveryDate();

        if (
            this.sku.out_of_stock &&
            /* Check if user is trying to add for a future date when the sku is available */
            !this.skuService.isSkuAvailableOnGivenDate(
                this.sku,
                deliveryDate
            ) &&
            (!this.oosInfoState || this.oosInfoState === OOS_INFO_STATES.ADD)
        ) {
            /* sku is out of stock, set the oos info state to oos if it's in add state */

            this.trackAnalytics();
            this.handleOosInfoUpdate.emit();
        }

        if (
            this.sku.oos_info &&
            showAlternatesFlow &&
            this.utilService.isEmpty(this.cartItem) &&
            (this.sku.oos_info.navigate || this.sku.next_available_date) &&
            !this.routerService.isHomeScreen() /* don't fetch alternates for collections */
        ) {
            /* Close preview modal if open */
            if (this.modalService.isModalOpened()) {
                this.modalService.closeModal();
            }

            this.fetchSimilarProducts.emit({
                skuId: this.sku.id,
                intent: CART_ITEM_TYPES.ADDON,
            });
        }
    }

    private decrement() {
        const currentQty = this.getCurrentSkuQty();

        if (!currentQty) {
            return;
        }

        const updatedQty = this.cartItem.quantity - 1;
        if (updatedQty === 0) {
            this._showBtn = false;
        }

        const cartItem = this.cartService.updateCartItemWithQty(
            this.cartItem,
            updatedQty
        );

        /* Updating store */
        this.handleQuantityUpdate.emit(cartItem);
        if (!updatedQty) {
            if (this.sku) {
                this.moengageService.trackRemoveFromCart({
                    sku_id: this.sku.id,
                    sku_price: this.sku.unit_price,
                    product_identifier: this.sku.product_identifier,
                });
            }
        }
    }

    private addToCart() {
        const cartItem = this.cartService.getCartItemFromSku(
            this.sku,
            1,
            this.vacation
        );
        this.handleQuantityUpdate.emit(cartItem);
        this.handlePostAddToCart.emit();
        if (this.sku) {
            this.moengageService.trackAddToCart({
                sku_id: this.sku.id,
                sku_price: this.sku.unit_price,
                product_identifier: this.sku.product_identifier,
            });
        }
    }

    private getCurrentSkuQty(): number {
        return (this.cartItem && this.cartItem.quantity) || 0;
    }

    private trackAnalytics() {
        if (this._impressionCalled) {
            return;
        }

        const cartItem = this.getCartItem();
        this.oosInfoService.trackAnalytics(
            this.sku,
            cartItem,
            1,
            EVENT_TYPES.CLICK_EVENT
        );

        this._impressionCalled = !this._impressionCalled;
    }

    private getCartItem() {
        return this.cartService.getCartItemFromSku(this.sku, 1, this.vacation);
    }
}
