import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    OnChanges,
    Input,
    SimpleChanges,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { UtilService } from "@services/util/util.service";
import { QuantityService } from "@services/util/quantity.service";
import { SkuService } from "@services/shared/sku.service";

import { Sku, Subscription } from "@models";

@Component({
    selector: "supr-subscription-product-tile",
    template: `
        <div class="suprRow tile" *ngIf="sku">
            <div class="suprColumn stretch top tileImage">
                <supr-image
                    [src]="sku?.image?.fullUrl"
                    [image]="sku?.image"
                    [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
                    [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
                ></supr-image>
                <div class="spacer8"></div>
            </div>

            <div
                class="suprColumn stretch left tileName"
                [ngClass]="{ center: isSuprPassSku, top: !isSuprPassSku }"
            >
                <supr-text type="body">
                    {{ sku?.sku_name }}
                </supr-text>

                <ng-container *ngIf="!isSuprPassSku">
                    <div
                        class="suprRow"
                        [class.topMargin]="showPrice"
                        *ngIf="showPrice"
                    >
                        <supr-text class="tilePrice" type="paragraph">
                            {{ sku?.unit_price | rupee: 2 }}
                        </supr-text>
                        <ng-container *ngIf="showPriceStrike">
                            <div class="spacer8"></div>
                            <supr-text class="tilePriceStrike" type="caption">
                                {{ sku?.unit_mrp | rupee: 2 }}
                            </supr-text>
                        </ng-container>
                    </div>
                </ng-container>
            </div>

            <ng-container *ngIf="!isSuprPassSku">
                <div class="suprColumn stretch top right tileQty">
                    <supr-text type="paragraph">
                        {{ quantityStr }}
                    </supr-text>
                    <div class="divider2"></div>
                    <div class="suprRow">
                        <supr-icon name="repeat"></supr-icon>
                        <div class="spacer2"></div>
                        <supr-week-days-small
                            [selectedDays]="subscription?.frequency"
                        ></supr-week-days-small>
                    </div>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["./supr-subscription-product-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionProductTileComponent implements OnInit, OnChanges {
    @Input() subscription: Subscription;
    @Input() showPrice = false;

    sku: Sku;
    quantityStr: string;
    showPriceStrike = false;
    isSuprPassSku: boolean;

    constructor(
        private utilService: UtilService,
        private quantityService: QuantityService,
        private skuService: SkuService
    ) {}

    ngOnInit() {
        this.setSku();
        this.setQtyStr();
        this.setPrice();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["subscription"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.setQtyStr();
        }
    }

    private setSku() {
        if (this.subscription && this.subscription.sku_data) {
            this.sku = this.subscription.sku_data;
        }
    }

    private setQtyStr() {
        if (!this.subscription || !this.sku) {
            return;
        }

        this.isSuprPassSku = this.skuService.isSuprPassSku(this.sku);

        if (this.isSuprPassSku && this.subscription.deliveries_remaining) {
            this.quantityStr =
                this.subscription.deliveries_remaining + " days remaining";
            return;
        }

        this.quantityStr = this.quantityService.toText(
            this.sku,
            this.utilService.toValue(this.subscription.quantity)
        );
    }

    private setPrice() {
        if (!this.sku) {
            return;
        }

        this.showPriceStrike = this.sku.unit_price < this.sku.unit_mrp;
    }
}
