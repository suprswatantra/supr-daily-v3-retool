import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { PNAdapter } from "@shared/adapters/pn.adapter";

import { PNType, PNData } from "@store/pn/pn.state";

@Component({
    selector: "supr-pn-component",
    template: `
        <supr-pn-wrapper
            *ngIf="appLaunchDone"
            [type]="type"
            [data]="data"
            [isLoggedIn]="isLoggedIn"
            (handleResetStore)="resetStore()"
        >
        </supr-pn-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PNDataComponent {
    @Input() appLaunchDone: boolean;
    @Input() type: PNType;
    @Input() data: PNData;
    @Input() isLoggedIn: boolean;

    constructor(private adapter: PNAdapter) {}

    resetStore() {
        this.adapter.resetPNData();
    }
}
