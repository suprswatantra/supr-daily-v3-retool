import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    SimpleChanges,
    OnChanges,
    OnInit,
    SimpleChange,
} from "@angular/core";

import { MODAL_NAMES, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { PNType, PNData } from "@store/pn/pn.state";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-pn-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                modalName="${MODAL_NAMES.PN_MODAL}"
                (handleClose)="closeModal()"
            >
                <supr-pn-content
                    [data]="data"
                    (handleClick)="handleButtonClick()"
                ></supr-pn-content>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../supr-pn.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PNWrapperComponent implements OnInit, OnChanges {
    @Input() type: PNType;
    @Input() data: PNData;
    @Input() isLoggedIn: boolean;

    @Output() handleResetStore: EventEmitter<void> = new EventEmitter();

    showModal: boolean;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const typeChange = changes["type"];
        const dataChange = changes["data"];
        const loginChange = changes["isLoggedIn"];
        if (
            this.canHandleChange(typeChange) ||
            this.canHandleChange(dataChange) ||
            this.canHandleChange(loginChange)
        ) {
            this.init();
        }
    }

    closeModal() {
        this.showModal = false;
        this.handleResetStore.emit();
    }

    handleButtonClick() {
        if (this.data && this.data.buttonUrl) {
            this.handleRedirection();
        } else {
            this.closeModal();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private init() {
        if (!this.type) {
            return;
        }

        const canSkipLogin = this.utilService.getNestedValue(
            this.data,
            "skipLoginCheck",
            false
        );

        if (!canSkipLogin && !this.isLoggedIn) {
            return;
        }

        if (this.type === "popup") {
            this.showModal = true;
        }

        if (this.type === "deeplink" || this.type === "richLanding") {
            const url = this.data && this.data.url;
            const type = this.type;
            this.handleResetStore.emit();
            this.setSourceLinkUrl(url, type);
            if (url) {
                type === "deeplink"
                    ? this.routerService.goToUrl(url)
                    : window.open(url, "_system", "location=yes");
            }
        }
    }

    private handleRedirection() {
        const { buttonUrl, outsideRedirection = false } = this.data;
        if (outsideRedirection) {
            this.closeModal();
            window.open(buttonUrl, "_system", "location=yes");
        } else {
            this.closeModal();
            this.routerService.goToUrl(buttonUrl);
        }
    }

    private setSourceLinkUrl(url = "", type = "") {
        if (url) {
            SETTINGS_KEYS_DEFAULT_VALUE["sourceLink"] = {
                type,
                url,
            };
        }
    }
}
