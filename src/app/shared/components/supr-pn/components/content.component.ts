import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { PNData } from "@store/pn/pn.state";

@Component({
    selector: "supr-pn-content",
    template: `
        <div class="container">
            <div *ngIf="data?.title">
                <div class="suprRow">
                    <supr-text type="subtitle" class="title">
                        {{ data.title }}
                    </supr-text>
                </div>
                <div class="divider16"></div>
            </div>

            <div *ngIf="data?.img">
                <supr-image [src]="data.img" [imgWidth]="500"></supr-image>
                <div class="divider16"></div>
            </div>

            <div *ngIf="data?.desc">
                <supr-text type="body" class="subtitle">
                    {{ data.desc }}
                </supr-text>
                <div class="divider16"></div>
            </div>

            <div class="divider24"></div>
            <supr-button
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.PN_GOT_IT}"
                (handleClick)="handleClick.emit()"
            >
                <supr-text type="body">{{
                    (data && data.buttonText) || buttonText
                }}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../supr-pn.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PNContentComponent {
    @Input() data: PNData;
    @Output() handleClick: EventEmitter<void> = new EventEmitter();

    buttonText = "Okay, Got It";
}
