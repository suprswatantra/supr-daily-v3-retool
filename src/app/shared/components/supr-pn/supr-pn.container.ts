import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { PNType, PNData } from "@store/pn/pn.state";

import { PNAdapter as Adapter } from "../../adapters/pn.adapter";

@Component({
    selector: "supr-pn-container",
    template: `
        <supr-pn-component
            [appLaunchDone]="appLaunchDone$ | async"
            [type]="type$ | async"
            [data]="data$ | async"
            [isLoggedIn]="isLoggedIn$ | async"
        ></supr-pn-component>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PNContainer implements OnInit {
    appLaunchDone$: Observable<boolean>;
    type$: Observable<PNType>;
    data$: Observable<PNData>;
    isLoggedIn$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
        this.type$ = this.adapter.pnType$;
        this.data$ = this.adapter.pnData$;
        this.isLoggedIn$ = this.adapter.isLoggedIn$;
    }
}
