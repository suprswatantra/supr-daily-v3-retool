import { PNContainer } from "./supr-pn.container";
import { PNDataComponent } from "./supr-pn.component";
import { PNWrapperComponent } from "./components/wrapper.component";
import { PNContentComponent } from "./components/content.component";

export const pnComponents = [
    PNDataComponent,
    PNWrapperComponent,
    PNContentComponent,
    PNContainer,
];
