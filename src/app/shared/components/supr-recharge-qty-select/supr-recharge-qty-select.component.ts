import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { RECHARGE_OPTIONS } from "@constants";
import { RadioButton } from "@types";

import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-recharge-qty-select",
    template: `
        <supr-radio-button-group
            [options]="_rechargeOptions"
            colSize="2"
            [selectedIndex]="_selectedIndex"
            (handleSelect)="onSelectOption($event)"
            [saObjectName]="saObjectName"
        ></supr-radio-button-group>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RechargeQtySelectComponent implements OnInit {
    @Input() selectedQty: number;
    @Input() saObjectName: string;

    @Output()
    handleOptionChange: EventEmitter<number> = new EventEmitter();

    _selectedIndex: number;
    _rechargeQuantities: number[] = RECHARGE_OPTIONS;
    _rechargeOptions: RadioButton[];
    _disabledQuantities: number[] = [];

    constructor(
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        const subscriptionInfo = this.settingsService.getSettingsValue(
            "subscriptionInfo",
            null
        );

        if (subscriptionInfo) {
            this._disabledQuantities = this.utilService.getNestedValue(
                subscriptionInfo,
                "DISABLED_QUANTITIES",
                []
            );
        }

        this.setRechargeOptions();
        this.setSelectedIndex();
    }

    onSelectOption(selectedOption: number) {
        this._selectedIndex = selectedOption;
        this.handleOptionChange.emit(this._rechargeQuantities[selectedOption]);
    }

    private setRechargeOptions() {
        this._rechargeQuantities = RECHARGE_OPTIONS.filter(
            (op) => !this.isDisabledQty(op)
        );

        this._rechargeOptions = this._rechargeQuantities.map(
            (option: number, index) => ({
                index,
                text: `${option} deliveries`,
            })
        );
    }

    private isDisabledQty(qty: number): boolean {
        return this._disabledQuantities.indexOf(qty) > -1;
    }

    private setSelectedIndex() {
        this._selectedIndex = this._rechargeQuantities.findIndex(
            (option) => option === this.selectedQty
        );
    }
}
