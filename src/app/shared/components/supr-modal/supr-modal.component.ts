import {
    Component,
    ChangeDetectorRef,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
    OnDestroy,
} from "@angular/core";

import { ModalService } from "@services/layout/modal.service";

import {
    MODAL_TYPES,
    ANALYTICS_OBJECT_NAMES,
    ANIMATION_DELAY_IN_MS,
} from "@constants";

import { Segment } from "@types";

@Component({
    selector: "supr-modal",
    templateUrl: "./supr-modal.component.html",
    styleUrls: ["./supr-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent implements OnInit, OnDestroy {
    @Input() hideBackdrop = false;
    @Input() animate = true;
    @Input() backdropDismiss = true;
    @Input() changeStatusbarColor = true;
    @Input() closeDelayTime = ANIMATION_DELAY_IN_MS;
    @Input() modalType = MODAL_TYPES.BOTTOM_SHEET;
    @Input() fullScreen = false;
    @Input() stack = false;
    @Input() modalName: string;
    @Input() saObjectValue: any;
    @Input() saObjectName = ANALYTICS_OBJECT_NAMES.IMPRESSION.MODAL;
    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() stackingAllowed = false;

    @Output() handleBackdropClick: EventEmitter<void> = new EventEmitter();
    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    hide = false;
    modalTypes = MODAL_TYPES;

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private modalService: ModalService
    ) {}

    ngOnInit() {
        this.modalService.addModal(this);
    }

    ngOnDestroy() {
        this.modalService.removeModal();
    }

    closeModal(closeDelayTime?: number) {
        if (!this.handleClose) {
            return;
        }

        // hide the backdrop first
        this.hide = true;
        this._updateView();

        // Find delay and close modal post delay
        const closeDelay =
            closeDelayTime >= 0 ? closeDelayTime : this.closeDelayTime;

        setTimeout(() => {
            this.handleClose.emit();
        }, closeDelay);
    }

    canModalBeClosed(): boolean {
        return this.backdropDismiss;
    }

    _backdropClick() {
        // Emit backdrop click event
        if (this.handleBackdropClick) {
            this.handleBackdropClick.emit();
        }

        // Dismiss modal if its enabled
        if (this.backdropDismiss !== false) {
            this.closeModal();
        }
    }

    // private methods
    // ---------------
    private _updateView() {
        this.changeDetectorRef.detectChanges();
    }
}
