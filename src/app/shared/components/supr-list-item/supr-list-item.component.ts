import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-list-item",
    template: `
        <div class="suprListItem">
            <div class="suprRow header" (click)="toggleSelection()">
                <div class="suprColumn left icon">
                    <supr-icon
                        name="approve_filled"
                        [class.selected]="selected"
                        [attr.disabled]="disabled"
                        [class.disabled]="disabled"
                        *ngIf="selected"
                    ></supr-icon>
                    <supr-icon
                        name="approve"
                        *ngIf="!selected"
                        [attr.disabled]="disabled"
                        [class.disabled]="disabled"
                    ></supr-icon>
                </div>

                <div class="suprColumn block">
                    <ng-content select="div.listItem"></ng-content>
                </div>
            </div>

            <div class="suprRow bufferContent" (click)="toggleSelection()">
                <div class="suprColumn leftCol"></div>
                <div class="suprColumn left">
                    <ng-content select="div.listBuffer"></ng-content>
                </div>
            </div>

            <div class="suprRow content" *ngIf="isExpand && selected">
                <div class="suprColumn leftCol"></div>
                <div class="suprColumn left">
                    <ng-content select="div.listContent"></ng-content>
                </div>
            </div>

            <div class="saperator" *ngIf="!islast"></div>
        </div>
    `,
    styleUrls: ["./supr-list-item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprListItemComponent {
    @Input() islast: boolean = false;
    @Input() isExpand: boolean = false;
    @Input() selected: boolean = false;
    @Input() disabled: boolean = false;
    @Input() data: any;

    @Output() clickHandler: EventEmitter<any> = new EventEmitter();

    toggleSelection() {
        if (this.disabled) {
            return;
        }
        this.selected = !this.selected;
        this.clickHandler.emit({
            selectedItem: this.data,
            selected: this.selected,
        });
    }
}
