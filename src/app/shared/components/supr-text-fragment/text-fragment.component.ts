import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";
import { STYLE_LIST } from "./constants";

import { TextFragment } from "@shared/models";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-text-fragment",
    template: `
        <div #textFragmentWrapper class="wrapper">
            <supr-text-highlight
                [type]="type"
                [text]="textFragment?.text"
                [truncate]="truncate"
                [highlightedText]="textFragment?.highlightedText"
            ></supr-text-highlight>
        </div>
    `,
    styleUrls: ["text-fragment.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextFragmentComponent implements OnInit, OnChanges {
    @Input() type: string;
    @Input() textFragment: TextFragment;
    @Input() truncate: boolean;

    @ViewChild("textFragmentWrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["textFragment"];
        this.handleChange(change);
    }

    private setStyles() {
        if (!this.textFragment) {
            return;
        }

        this.utilService.setStyles(
            this.textFragment,
            STYLE_LIST,
            this.wrapperEl
        );
    }

    private handleChange(change: SimpleChange) {
        if (change && !change.firstChange) {
            this.setStyles();
        }
    }
}
