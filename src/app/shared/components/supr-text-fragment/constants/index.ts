export const STYLE_LIST = [
    {
        attributeName: "highlightedTextColor",
        attributeStyleVariableName:
            "--supr-text-fragment-highlighted-text-color",
    },
    {
        attributeName: "fontSize",
        attributeStyleVariableName: "--supr-text-fragment-font-size",
    },
    {
        attributeName: "textColor",
        attributeStyleVariableName: "--supr-text-fragment-font-color",
    },
];
