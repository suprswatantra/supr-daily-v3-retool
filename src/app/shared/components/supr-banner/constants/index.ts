export const BANNER_PARENT_STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-banner-bg",
    },
    {
        attributeName: "border.size",
        attributeStyleVariableName: "--supr-banner-border-size",
    },
    {
        attributeName: "border.radius",
        attributeStyleVariableName: "--supr-banner-border-radius",
    },
    {
        attributeName: "border.color",
        attributeStyleVariableName: "--supr-banner-border-color",
    },
    {
        attributeName: "border.type",
        attributeStyleVariableName: "--supr-banner-border-style",
    },
    {
        attributeName: "boxShadow",
        attributeStyleVariableName: "--supr-banner-box-shadow",
    },
];

export const BANNER_CONTENT_STYLE_LIST = [
    {
        attributeName: "highlightedTextColor",
        attributeStyleVariableName: "--supr-banner-highlighted-text-color",
    },
    {
        attributeName: "fontSize",
        attributeStyleVariableName: "--supr-banner-content-font-size",
    },
    {
        attributeName: "textColor",
        attributeStyleVariableName: "--supr-banner-content-font-color",
    },
    {
        attributeName: "link.textColor",
        attributeStyleVariableName: "--supr-banner-border-style",
    },
    {
        attributeName: "titleColor",
        attributeStyleVariableName: "--supr-banner-content-title-color",
    },
    {
        attributeName: "tag.bgColor",
        attributeStyleVariableName: "--supr-banner-tag-bg",
    },
    {
        attributeName: "titleWeight",
        attributeStyleVariableName: "--supr-banner-content-title-weight",
    },

    {
        attributeName: "textWeight",
        attributeStyleVariableName: "--supr-banner-content-text-weight",
    },
];
