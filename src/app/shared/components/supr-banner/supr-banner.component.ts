import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { Banner } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { BANNER_PARENT_STYLE_LIST } from "./constants";
import { SA_OBJECT_NAMES } from "./constants/analytics.constant";

@Component({
    selector: "supr-banner",
    template: `
        <div class="bannerContainer">
            <ng-container *ngIf="banner?.outerHeading?.text">
                <supr-text-fragment
                    [textFragment]="banner?.outerHeading"
                    class="boldText"
                ></supr-text-fragment>
                <div class="divider16"></div>
            </ng-container>
            <div
                class="bannerWrapper"
                [class.withTitle]="hasTitleInContent"
                [class.withCrossIcon]="showHideButton"
                #wrapper
            >
                <ng-container *ngIf="banner?.title">
                    <supr-text type="action14"> {{ banner?.title }} </supr-text>
                    <div class="divider12"></div>
                </ng-container>

                <ng-container *ngIf="showHideButton">
                    <div class="crossIconWrapper">
                        <supr-icon
                            name="cross"
                            (click)="handleHideButtonClick.emit()"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .HIDE_BTN_CLICK}"
                            [saContext]="saBannerContext"
                        ></supr-icon>
                    </div>
                </ng-container>

                <ng-container
                    *ngFor="
                        let bannerContent of banner.content;
                        last as isLast;
                        first as isFirst
                    "
                >
                    <supr-banner-content
                        [last]="isLast"
                        [first]="isFirst"
                        [content]="bannerContent"
                        [saBannerContext]="saBannerContext"
                    ></supr-banner-content>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["./styles/supr-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerComponent implements OnInit {
    @Input() banner: Banner;
    @Input() showHideButton: boolean;
    @Input() saBannerContext: string;

    @Output() handleHideButtonClick: EventEmitter<void> = new EventEmitter();

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    hasTitleInContent: boolean;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
        this.setHasTitleInContent();
    }

    private setHasTitleInContent() {
        const bannerContentArray = this.utilService.getNestedValue(
            this.banner,
            "content"
        );

        if (!bannerContentArray) {
            this.hasTitleInContent = false;
            return;
        }

        this.hasTitleInContent = bannerContentArray.find((bannerContent) => {
            return bannerContent && bannerContent.title;
        });
    }

    private setStyles() {
        if (!this.banner) {
            return;
        }

        this.utilService.setStyles(
            this.banner,
            BANNER_PARENT_STYLE_LIST,
            this.wrapperEl
        );
    }
}
