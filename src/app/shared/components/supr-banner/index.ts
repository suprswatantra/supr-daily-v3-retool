import { BannerComponent } from "./supr-banner.component";
import { BannerContentComponent } from "./components/banner-content.component";

export const bannerComponents = [BannerComponent, BannerContentComponent];
