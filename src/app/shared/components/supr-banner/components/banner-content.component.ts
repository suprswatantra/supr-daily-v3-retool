import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";
import { BANNER_CONTENT_STYLE_LIST } from "../constants";
import { BannerContent } from "@shared/models";

import { SA_OBJECT_NAMES } from "../constants/analytics.constant";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-banner-content",
    template: `
        <div
            class="bannerContentWrapper"
            [class.isLast]="last"
            [class.isFirst]="first"
            [class.withTitle]="content?.title"
            #wrapper
        >
            <div class="suprRow top">
                <ng-container *ngIf="content?.imgUrl">
                    <div
                        class="suprColumn bannerContentIcon left"
                        [class.bigIcon]="content?.title"
                    >
                        <div class="imgWrapper">
                            <supr-image [src]="content.imgUrl"></supr-image>
                        </div>
                    </div>
                </ng-container>

                <ng-container *ngIf="content?.text">
                    <div class="suprColumn left">
                        <ng-container *ngIf="content?.title">
                            <div class="suprRow titleBlock top">
                                <div class="suprColumn left">
                                    <supr-text class="title">
                                        {{ content?.title }}
                                    </supr-text>
                                    <div class="divider8"></div>
                                </div>
                                <div
                                    class="suprColumn right"
                                    *ngIf="content?.tag?.text"
                                >
                                    <div class="tag">
                                        <supr-text-fragment
                                            [textFragment]="content?.tag"
                                        ></supr-text-fragment>
                                    </div>
                                </div>
                            </div>
                        </ng-container>
                        <supr-text-highlight
                            [text]="content?.text"
                            [highlightedText]="content?.highlightedText"
                        ></supr-text-highlight>
                        <ng-container *ngIf="content?.link?.text">
                            <div class="divider8"></div>
                            <div class="suprRow">
                                <supr-text
                                    class="link"
                                    (click)="goToLink()"
                                    saClick
                                    saObjectName="${SA_OBJECT_NAMES.CLICK
                                        .LINK_CLICKED}"
                                    [saContext]="saBannerContext"
                                >
                                    {{ content?.link?.text }}
                                </supr-text>
                                <supr-icon
                                    name="chevron_right"
                                    class="link"
                                ></supr-icon>
                            </div>
                        </ng-container>
                        <ng-container *ngIf="content?.copyBlock">
                            <div class="divider8"></div>
                            <supr-copy-block
                                [copyText]="content?.copyBlock?.copyText"
                                [bgColor]="content?.copyBlock?.bgColor"
                                [iconColor]="content?.copyBlock?.iconColor"
                                [borderColor]="content?.copyBlock?.borderColor"
                                [showHelpText]="true"
                                [hideCopyText]="
                                    content?.copyBlock?.hideCopyText
                                "
                            ></supr-copy-block>
                        </ng-container>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerContentComponent implements OnInit {
    @Input() last: boolean;
    @Input() first: boolean;
    @Input() content: BannerContent;
    @Input() saBannerContext: string;

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.setStyles();
    }

    goToLink() {
        const link = this.utilService.getNestedValue(
            this.content,
            "link",
            null
        );

        if (!link) {
            return;
        }

        if (link.outsideUrl) {
            window.open(link.outsideUrl, "_system", "location=yes");
            return;
        }

        if (link.appUrl) {
            this.routerService.goToUrl(link.appUrl);
        }
    }

    private setStyles() {
        if (!this.content) {
            return;
        }

        this.utilService.setStyles(
            this.content,
            BANNER_CONTENT_STYLE_LIST,
            this.wrapperEl
        );
    }
}
