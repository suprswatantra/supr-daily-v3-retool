import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

@Component({
    selector: "supr-support-options",
    template: `
        <div class="help">
            <div class="suprRow">
                <supr-icon name="error"></supr-icon>
                <div class="spacer8"></div>
                <supr-text class="title"> {{ help }} </supr-text>
            </div>

            <div *ngIf="subTitle">
                <div class="divider16"></div>
                <supr-text class="subText">
                    {{ subTitle }}
                </supr-text>
            </div>

            <div class="divider24"></div>
            <div class="suprRow">
                <div class="suprColumn actionButton" *ngIf="isCall">
                    <supr-support-button
                        [show]="isCall"
                        class="inactive"
                        [displayText]="call"
                        (handleClick)="callActionButton.emit()"
                        saImpression
                        [saObjectName]="saObjectNameCall"
                        [saObjectValue]="saObjectValue"
                        [saContext]="saContext"
                    ></supr-support-button>
                </div>
                <div class="spacer24" *ngIf="isChat && isCall"></div>

                <div class="suprColumn actionButton" *ngIf="isChat">
                    <supr-support-button
                        [show]="isChat"
                        [displayText]="chat"
                        (handleClick)="chatActionButton.emit()"
                        saImpression
                        [saObjectName]="saObjectNameChat"
                        [saObjectValue]="saObjectValue"
                        [saContext]="saContext"
                    ></supr-support-button>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-support-options.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSupportOptionsComponent {
    @Input() chat: string;
    @Input() call: string;
    @Input() help: string;
    @Input() subTitle: string;
    @Input() isChat: boolean;
    @Input() isCall: boolean;
    @Input() saObjectName: string;
    @Input() saObjectValue: string;
    @Input() saContext: string;

    get saObjectNameCall() {
        return `${this.saObjectName}-${ANALYTICS_OBJECT_NAMES.CLICK.CALL_BUTTON}`;
    }

    get saObjectNameChat() {
        return `${this.saObjectName}-${ANALYTICS_OBJECT_NAMES.CLICK.CHAT_BUTTON}`;
    }

    @Output() chatActionButton: EventEmitter<void> = new EventEmitter();
    @Output() callActionButton: EventEmitter<void> = new EventEmitter();
}
