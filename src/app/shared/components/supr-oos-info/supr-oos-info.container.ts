import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";
import { Observable } from "rxjs";

import { SkuAdapter } from "@shared/adapters/sku.adapter";

import { Sku, OutOfStockInfoStateType } from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { Segment, OOSInfoComponentUpdateStateData } from "@types";

@Component({
    selector: "supr-oos-info",
    template: `
        <supr-oos-info-component
            [skuId]="sku?.id"
            [sku]="sku"
            [reverse]="reverse"
            [showOosBtn]="showOosBtn"
            [showHorizontalLayout]="showHorizontalLayout"
            [isSingleLine]="isSingleLine"
            [notified]="notified$ | async"
            [showNotifyBtn]="showNotifyBtn"
            [saContext]="saContext"
            [saPosition]="saPosition"
            [saBtnContextList]="saContextList"
            (handleUpdateSkuOosInfo)="updateSkuOosInfo($event)"
        >
        </supr-oos-info-component>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OOSInfoConatiner implements OnInit {
    @Input() sku: Sku;
    @Input() reverse = false;
    @Input() showOosBtn = true;
    @Input() isSingleLine = false;
    @Input() showNotifyBtn = true;
    @Input() showHorizontalLayout = false;

    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() saContextList: Segment.ContextListItem[];

    notified$: Observable<OutOfStockInfoStateType>;

    constructor(
        private skuAdapter: SkuAdapter,
        private settingsService: SettingsService
    ) {}

    ngOnInit(): void {
        this.notified$ = this.skuAdapter.isNotified(this.sku.id);
    }

    updateSkuOosInfo(data: OOSInfoComponentUpdateStateData) {
        const alternatesV2 = this.settingsService.getSettingsValue(
            "alternatesV2",
            true
        );

        if (alternatesV2) {
            this.updateSkuOOSInfo(data.state);
        } else {
            /* [TODO_RITESH] Deprecate this flow once alternates experiment is resolved */
            if (data.updateInLocal) {
                if (data.state !== "notifySl") {
                    this.updateOosInfoDep("notify", data.updateInLocal);
                } else {
                    this.updateOosInfoDep("oos", data.updateInLocal);
                }
            } else {
                if (data.state !== "notify") {
                    this.updateOosInfoDep("notify", data.updateInLocal);
                } else {
                    this.updateOosInfoDep("oos", data.updateInLocal);
                }
            }
        }
    }

    private updateSkuOOSInfo(state: OutOfStockInfoStateType) {
        if (this.sku) {
            this.skuAdapter.updateSkuOOSInfo(this.sku.id, state);
        }
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    private updateOosInfoDep(
        state: OutOfStockInfoStateType,
        updateInLocal: string
    ) {
        if (this.sku) {
            this.skuAdapter.updateSkuOosInfo_deprecate(
                this.sku.id,
                state,
                false,
                updateInLocal
            );
        }
    }
}
