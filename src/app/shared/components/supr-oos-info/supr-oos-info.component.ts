import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    SKU_OOS_TEXTS,
    OOS_INFO_STATES,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { Sku, OutOfStockInfoStateType } from "@models";

import { SettingsService } from "@services/shared/settings.service";

import { Segment, OOSInfoComponentUpdateStateData } from "@types";

import { TEXTS } from "./constants";

@Component({
    selector: "supr-oos-info-component",
    template: `
        <div
            class="stretch top oos"
            [ngClass]="getClass()"
            [class.suprRow]="showHorizontalLayout"
            [class.suprColumn]="!showHorizontalLayout"
            *ngIf="!isSingleLine; else singLine"
        >
            <ng-container *ngIf="sku?.notify_enabled">
                <supr-button
                    class="notify"
                    (click)="toggleSkuNotified()"
                    [class.notified]="notified === 'notify'"
                    [saObjectName]="
                        notified === 'notify'
                            ? 'cancel-notify'
                            : 'notify-button'
                    "
                    [saObjectValue]="sku?.id"
                    [saContext]="saContext"
                    [saPosition]="saPosition"
                    [saContextList]="saBtnContextList"
                    *ngIf="showNotifyBtn"
                >
                    <div class="suprRow center">
                        <supr-icon name="bell_2"> </supr-icon>
                        <div class="spacer4"></div>
                        <supr-text type="subtext10">
                            ${TEXTS.NOTIFY}
                        </supr-text>
                    </div>
                </supr-button>
                <div class="spacer8" *ngIf="showHorizontalLayout"></div>
            </ng-container>

            <div *ngIf="showOosBtn">
                <div
                    class="divider8"
                    *ngIf="
                        showOosBtn &&
                        showNotifyBtn &&
                        !reverse &&
                        !showHorizontalLayout
                    "
                ></div>
                <supr-button class="oos">
                    <div class="suprRow center">
                        <supr-text type="subtext11">
                            ${TEXTS.OUT_OF_STOCK}
                        </supr-text>
                    </div>
                </supr-button>

                <div class="divider4" *ngIf="reverse"></div>
            </div>
        </div>

        <ng-template #singLine>
            <div class="suprRow spaceBetween oos singleLine">
                <supr-text type="paragraph" class="text">
                    {{ _single_line_text }}
                </supr-text>
                <div class="spacer12"></div>
                <supr-button
                    class="notify"
                    (click)="toggleSkuNotified('singleLineUpdate')"
                    [class.notified]="notified === 'notifySl'"
                    [saObjectName]="
                        notified === 'notifySl'
                            ? 'cancel-notify'
                            : 'notify-button'
                    "
                    [saObjectValue]="sku?.id"
                    [saContext]="saContext"
                    [saPosition]="saPosition"
                    [saContextList]="saBtnContextList"
                >
                    <div class="suprRow center">
                        <supr-icon name="bell_2"> </supr-icon>
                        <div class="spacer4"></div>
                        <supr-text type="subtext10">
                            ${TEXTS.NOTIFY}
                        </supr-text>
                    </div>
                </supr-button>
            </div>
        </ng-template>
    `,
    styleUrls: ["./supr-oos-info.component.scss"],
    changeDetection: ChangeDetectionStrategy.Default,
})
export class OOSInfoComponent implements OnInit, OnChanges {
    @Input() skuId: number;
    @Input() sku: Sku;
    @Input() reverse = false;
    @Input() isSingleLine = false;
    @Input() showOosBtn: boolean;
    @Input() showNotifyBtn: boolean;
    @Input() notified: OutOfStockInfoStateType;
    @Input() showHorizontalLayout: boolean;

    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() saBtnContextList: Segment.ContextListItem[];

    @Output() handleUpdateSkuOosInfo: EventEmitter<
        OOSInfoComponentUpdateStateData
    > = new EventEmitter();

    constructor(private settingsService: SettingsService) {}

    _notifiedState = 0;
    isSaImpressionEnabled = false;
    _single_line_text = SKU_OOS_TEXTS.GET_AN_ALERT;

    ngOnInit() {
        this.setSingleLineText();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["notified"];
        if (this.canHandleChange(change)) {
            this.setSingleLineText();
        }
    }

    toggleSkuNotified(updateInLocal = "") {
        const newState =
            this.notified === OOS_INFO_STATES.NOTIFY
                ? OOS_INFO_STATES.OOS
                : OOS_INFO_STATES.NOTIFY;

        this.handleUpdateSkuOosInfo.emit({ updateInLocal, state: newState });
    }

    getClass() {
        if (this.reverse) {
            return {
                left: true,
                reverse: true,
            };
        }
    }

    setSingleLineText() {
        const oosNotifyTexts = this.settingsService.getSettingsValue(
            "oosNotifyTexts",
            SETTINGS_KEYS_DEFAULT_VALUE.oosNotifyTexts
        );

        const { willNotifyYou, getAnAlert } = oosNotifyTexts.alertSection;

        if (this.notified === "notifySl") {
            this._single_line_text = willNotifyYou;
        } else {
            this._single_line_text = getAnAlert;
        }
    }

    private canHandleChange(change: SimpleChange) {
        return change && !change.firstChange && change.currentValue;
    }
}
