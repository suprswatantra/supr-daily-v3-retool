import { Observable } from "rxjs";

export interface TouchEvents {
    [type: string]: Observable<SwipeCoordinate>;
}

export interface SwipeCoordinate {
    x: number;
    y: number;
}
