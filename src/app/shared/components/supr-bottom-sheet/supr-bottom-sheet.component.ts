import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    NgZone,
    OnDestroy,
    Output,
    EventEmitter,
} from "@angular/core";

import { fromEvent, Observable, Subject } from "rxjs";
import { tap, map, concatMap, takeUntil, first, filter } from "rxjs/operators";

import { TouchEvents, SwipeCoordinate } from "./types";

const SWIPE_DRAG_TOLERANCE = 75;
const WRAPPER_POS_CSS_VARIABLE = "--wrapper-position-y";
const WRAPPER_TRANSITION_CSS_VARIABLE = "--wrapper-transition-name";

@Component({
    selector: "supr-bottom-sheet",
    template: `
        <div #animateHolder>
            <supr-sticky-wrapper>
                <div class="wrapper suprVisibleDelay">
                    <supr-draggable-top></supr-draggable-top>
                    <div class="wrapperContent">
                        <ng-content></ng-content>
                    </div>
                </div>
            </supr-sticky-wrapper>
        </div>
    `,
    styleUrls: ["./supr-bottom-sheet.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BottomSheetComponent implements OnInit, OnDestroy {
    @Input() disableSwipe = false;
    @Output() handleSwipeDown: EventEmitter<void> = new EventEmitter();

    @ViewChild("animateHolder", { static: true })
    wrapperEl: ElementRef;

    private _onDestroy = new Subject();
    private touchEvents = <TouchEvents>{
        touchstart: null,
        touchmove: null,
        touchend: null,
    };
    private dragEvent$: Observable<number>;
    private dropEvent$: Observable<number>;

    constructor(private ngZone: NgZone) {}

    ngOnInit() {
        if (!this.disableSwipe) {
            this.ngZone.runOutsideAngular(() => {
                this.registerSwipeEvents();
            });
        }
    }

    ngOnDestroy() {
        // Unsubscribe from the subscribed events if any
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    private registerSwipeEvents() {
        // Prepare touch event Observables
        this.prepareTouchEvents();

        // Prepare the swipe down observable
        this.prepareDragEvent();

        // Prepare the last drop observable
        this.prepareDropEvent();

        // Subscribe for events
        this.subscribeForDrag();
        this.subscribeForDrop();
    }

    private prepareTouchEvents() {
        ["touchstart", "touchmove", "touchend"].forEach(
            this.prepareTouchEvent.bind(this)
        );
    }

    private prepareDragEvent() {
        const touchStart$ = this.touchEvents.touchstart;
        const touchMove$ = this.touchEvents.touchmove;
        const touchEnd$ = this.touchEvents.touchend;

        this.dragEvent$ = touchStart$.pipe(
            tap(() => {
                this.wrapperEl.nativeElement.style.setProperty(
                    WRAPPER_TRANSITION_CSS_VARIABLE,
                    "none"
                );
            }),
            concatMap((startPos: SwipeCoordinate) =>
                touchMove$.pipe(
                    takeUntil(touchEnd$),
                    map((dragPos: SwipeCoordinate) => dragPos.y - startPos.y),
                    filter(newY => newY > 0)
                )
            )
        );
    }

    private prepareDropEvent() {
        const touchStart$ = this.touchEvents.touchstart;
        const touchEnd$ = this.touchEvents.touchend;

        this.dropEvent$ = touchStart$.pipe(
            concatMap((startPos: SwipeCoordinate) =>
                touchEnd$.pipe(
                    first(),
                    map((endPos: SwipeCoordinate) => endPos.y - startPos.y)
                )
            )
        );
    }

    private prepareTouchEvent(eventType: string) {
        const domEl = this.wrapperEl.nativeElement;
        this.touchEvents[eventType] = fromEvent(domEl, eventType).pipe(
            map(this.touchEventToCoordinate.bind(this))
        );
    }

    private touchEventToCoordinate(touchEvent: TouchEvent): SwipeCoordinate {
        return {
            x: touchEvent.changedTouches[0].pageX,
            y: touchEvent.changedTouches[0].pageY,
        };
    }

    private subscribeForDrag() {
        this.dragEvent$
            .pipe(takeUntil(this._onDestroy))
            .subscribe((newY: number) => {
                this.wrapperEl.nativeElement.style.setProperty(
                    WRAPPER_POS_CSS_VARIABLE,
                    newY
                );
            });
    }

    private subscribeForDrop() {
        this.dropEvent$
            .pipe(takeUntil(this._onDestroy))
            .subscribe((diffY: number) => {
                if (diffY >= SWIPE_DRAG_TOLERANCE) {
                    if (this.handleSwipeDown) {
                        this.ngZone.run(() => {
                            this.handleSwipeDown.emit();
                        });
                    }
                } else {
                    this.wrapperEl.nativeElement.style.setProperty(
                        WRAPPER_TRANSITION_CSS_VARIABLE,
                        "transform"
                    );

                    this.wrapperEl.nativeElement.style.setProperty(
                        WRAPPER_POS_CSS_VARIABLE,
                        0
                    );
                }
            });
    }
}
