import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
} from "@angular/core";
import { SA_IMPRESSIONS_IS_ENABLED } from "@constants";
import { TileGridData } from "@types";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-tile-grid",
    template: `
        <div class="wrapper" #column>
            <div
                class="suprRow row top"
                *ngFor="let rowData of gridArr; trackBy: trackByFn"
            >
                <supr-tile
                    (click)="handleTileClick.emit(tileData)"
                    *ngFor="let tileData of rowData; trackBy: trackByFn"
                    [tileData]="tileData"
                    [colorBackground]="colorBackground"
                    saClick
                    saImpression
                    [saImpressionEnabled]="
                        ${SA_IMPRESSIONS_IS_ENABLED.SUPR_TILE}
                    "
                    [saObjectName]="saObjectName"
                    [saObjectValue]="tileData.id"
                >
                </supr-tile>
            </div>
        </div>
    `,
    styleUrls: ["./supr-tile-grid.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileGridComponent implements OnInit {
    @Input() columnSize = 3;
    @Input() colorBackground = false;
    @Input()
    set dataList(value: TileGridData[]) {
        this.prepareGridData(value);
    }
    @Input() saObjectName: string;

    @Output() handleTileClick: EventEmitter<any> = new EventEmitter();
    @ViewChild("column", { static: true }) columnEl: ElementRef;

    gridArr: TileGridData[][] = [];

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setColumnWidth(this.columnSize);
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private setColumnWidth(columnSize: number) {
        this.columnEl.nativeElement.style.setProperty(
            "--supr-grid-tile-width",
            `calc(100% / ${columnSize})`
        );
    }

    private prepareGridData(dataList: TileGridData[]) {
        this.gridArr = this.utilService.chunkArray(dataList, this.columnSize);
    }
}
