import { TileComponent } from "./components/supr-tile.component";
import {
    TileGridSkeletonComponent,
    TileSkeletonComponent,
} from "./components/supr-tile-grid-skeleton.component";
import { TileGridComponent } from "./supr-tile-grid.component";

export const tileGridComponents = [
    TileGridComponent,
    TileComponent,
    TileGridSkeletonComponent,
    TileSkeletonComponent,
];
