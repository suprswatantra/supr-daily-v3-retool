import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-tile-skeleton",
    template: `
        <div class="gridTile suprColumn" [ngClass]="edge">
            <div class="wrapper">
                <ion-skeleton-text animated></ion-skeleton-text>
            </div>
            <ion-skeleton-text animated class="title"></ion-skeleton-text>
            <ion-skeleton-text animated class="info"></ion-skeleton-text>
        </div>
    `,
    styleUrls: ["./supr-tile-grid-skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileSkeletonComponent {
    @Input() edge = "";
}

@Component({
    selector: "supr-tile-grid-skeleton",
    template: `
        <div class="suprRow gridRow">
            <supr-tile-skeleton edge="leftEdge"></supr-tile-skeleton>
            <supr-tile-skeleton></supr-tile-skeleton>
            <supr-tile-skeleton edge="rightEdge"></supr-tile-skeleton>
        </div>
        <div class="divider16"></div>
        <div class="suprRow gridRow">
            <supr-tile-skeleton edge="leftEdge"></supr-tile-skeleton>
            <supr-tile-skeleton></supr-tile-skeleton>
            <supr-tile-skeleton edge="rightEdge"></supr-tile-skeleton>
        </div>
    `,
    styleUrls: ["./supr-tile-grid-skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileGridSkeletonComponent {}
