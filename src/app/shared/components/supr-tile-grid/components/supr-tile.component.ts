import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ElementRef,
    ViewChild,
    OnInit,
} from "@angular/core";
import { Color } from "@types";
import { TileGridData } from "@types";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-tile",
    template: `
        <div class="suprColumn center wrapper" #tile>
            <div class="imgWrapper">
                <supr-image
                    [src]="tileData.imgUrl"
                    [image]="tileData.image"
                ></supr-image>
            </div>
            <supr-text type="regular14">{{ tileData.name }}</supr-text>
        </div>
    `,
    styleUrls: ["./supr-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent implements OnInit {
    @Input() tileData: TileGridData;
    @Input() colorBackground: boolean;
    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    hslData: Color.HSL;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        if (this.colorBackground) {
            this.convertColorToHSL();
            this.setBgColor();
        } else {
            this.setDefaultColor();
        }
    }

    private convertColorToHSL() {
        this.hslData = this.utilService.hexToHSL(this.tileData.bgColor);
    }

    private setBgColor() {
        const { h, s } = this.hslData;
        this.tileEl.nativeElement.style.setProperty(
            "--supr-tile-img-bg-color",
            `hsl(${h}, ${s}%, 95%)`
        );
    }

    private setDefaultColor() {
        this.tileEl.nativeElement.style.setProperty(
            "--supr-tile-img-bg-color",
            `#fff`
        );
    }
}
