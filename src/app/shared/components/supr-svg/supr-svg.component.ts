import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-svg",
    template: `
        <div class="svg"></div>
    `,
    styleUrls: ["./supr-svg.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SvgComponent {}
