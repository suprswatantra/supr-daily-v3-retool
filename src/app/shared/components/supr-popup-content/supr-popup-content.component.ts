import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { Popup } from "@shared/models";

@Component({
    selector: "supr-popup-content",
    template: `
        <div class="container">
            <div *ngIf="data?.title">
                <div class="suprRow">
                    <supr-text type="subtitle" class="title">
                        {{ data.title }}
                    </supr-text>
                </div>
                <div class="divider16"></div>
            </div>

            <div *ngIf="data?.img">
                <supr-image [src]="data.img" [imgWidth]="500"></supr-image>
                <div class="divider16"></div>
            </div>

            <div *ngIf="data?.desc">
                <supr-text type="body" class="subtitle">
                    {{ data.desc }}
                </supr-text>
                <div class="divider16"></div>
            </div>

            <div class="divider24"></div>
            <supr-button
                [saObjectName]="data.buttonText"
                (handleClick)="handleClick.emit()"
            >
                {{ data && data.buttonText }}
            </supr-button>
        </div>
    `,
    styleUrls: ["./supr-popup-content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopupContentComponent {
    @Input() data: Popup;
    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
