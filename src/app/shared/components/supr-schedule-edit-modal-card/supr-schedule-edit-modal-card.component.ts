import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { EDIT_DELIVERY_ACTIONS, DELIVERY_EDIT_MODAL_TEXTS } from "@constants";

import { Subscription, Sku, ScheduleItem } from "@models";

import { DateService } from "@services/date/date.service";
import { QuantityService } from "@services/util/quantity.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";
import { TEXTS } from "@shared/components/supr-schedule-edit-modal-card/constants/supr-schedule-edit-modal-card.constants";

@Component({
    selector: "supr-schedule-edit-modal-card",
    templateUrl: "./supr-schedule-edit-modal-card.component.html",
    styleUrls: ["./supr-schedule-edit-modal-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleEditModalCardComponent implements OnInit {
    @Input() sku: Sku;
    @Input() date: string;
    @Input() showCounter = true;
    @Input() plusDisabled = false;
    @Input() amountChange?: number;
    @Input() schedule: ScheduleItem;
    @Input() counterQuantity: number;
    @Input() adjustmentAction: string;
    @Input() subscription?: Subscription;
    @Input() showCancelConfirmation: boolean;
    @Input() showCancelAction?: boolean;
    @Input() showChangeAction?: boolean;
    @Input() confirmBtnText?: string;
    @Input() secondaryActionText?: string;
    @Input() showAdjustmentText?: boolean;
    @Input() headerText = DELIVERY_EDIT_MODAL_TEXTS.EDIT;

    @Output() handleButtonClick: EventEmitter<string> = new EventEmitter();
    @Output() handleCancelTextClick: EventEmitter<void> = new EventEmitter();
    @Output() handleQuantityUpdate: EventEmitter<number> = new EventEmitter();
    @Output()
    handleManageSubscription?: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;
    dateText: string;
    dateSuffix: string;
    showBtnLoader = false;
    remainingQtyStr: string;

    saObjectValueSubId: number;
    saObjectValueCancel: number;
    saObjectNamesClick = SA_OBJECT_NAMES.CLICK;

    readonly EDIT_DELIVERY_ACTIONS = EDIT_DELIVERY_ACTIONS;

    constructor(
        private dateService: DateService,
        private quantityService: QuantityService,
        private blockAccessService: BlockAccessService
    ) {}

    ngOnInit() {
        this.setQtyStr();
        this.setDateStr();
        this.setAnalyticsData();
    }

    _handleButtonClick(action: string) {
        this.showBtnLoader =
            action !== EDIT_DELIVERY_ACTIONS.DONE &&
            action !== EDIT_DELIVERY_ACTIONS.DELIVER_ONCE;

        if (
            action === EDIT_DELIVERY_ACTIONS.RECHARGE_SUBSCRIPTION &&
            this.blockAccessService.isSubscriptionAccessRestricted()
        ) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.handleButtonClick.emit(action);
    }

    _handleDeliveryDateClick(action: string) {
        if (this.showChangeAction && !this.showCancelAction) {
            this.showBtnLoader = true;
            this.handleButtonClick.emit(action);
        }
    }

    _handleCancelClick(event: MouseEvent | TouchEvent) {
        event.stopPropagation();

        if (this.showCancelAction && !this.showChangeAction) {
            this.handleCancelTextClick.emit();
        }
    }

    private setDateStr() {
        if (this.date) {
            this.dateSuffix = this.dateService.getDateSuffix(this.date);
            this.dateText = this.dateService.getDeliveryDateText(this.date);
        }
    }

    private setQtyStr() {
        if (this.sku && this.schedule) {
            this.remainingQtyStr = this.quantityService.toText(
                this.sku,
                this.schedule.remaining_quantity
            );
        }
    }

    private setAnalyticsData() {
        if (this.subscription) {
            this.saObjectValueSubId = this.subscription.id;

            this.saObjectValueCancel = this.subscription.id;
        }
    }
}
