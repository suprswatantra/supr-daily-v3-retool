export const TEXTS = {
    APPROX: "(Approx.)",
    DELIVERY_DATE: "Delivery date",
    DELIVERIES_LEFT: "Deliveries left",
    MANAGE_SUBSCRIPTION: "MANAGE SUBSCRIPTION",
    DATES_ADJUSTED_TEXT: "Order schedule will be adjusted as applicable.",
    RECHARGE_SUBSCRIPTION_TEXT: "Recharge subscription to continue",
};
