import { WrapperComponent } from "./components/wrapper.component";
import { PromptComponent } from "./components/prompt.component";
import { ThankyouComponent } from "./components/thankyou.component";

import { FeedbackComponent } from "./components/feedback/feedback.component";
import { FeedbackOptionsComponent } from "./components/feedback/options.componnent";

import { AppRatingComponent } from "./supr-app-rating.component";

export const appRatingComponents = [
    AppRatingComponent,

    WrapperComponent,
    PromptComponent,
    ThankyouComponent,

    FeedbackComponent,
    FeedbackOptionsComponent,
];
