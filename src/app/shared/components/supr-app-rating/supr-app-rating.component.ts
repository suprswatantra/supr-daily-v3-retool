import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

import { Observable } from "rxjs";

import { RatingOption } from "@models";
import { RatingAdapter as Adapter } from "@shared/adapters/rating.adapter";
import { SuprApi } from "@types";

@Component({
    selector: "supr-app-rating",
    template: `
        <supr-app-rating-wrapper
            [ratingOptions]="ratingOptions$ | async"
            [showModal]="showModal$ | async"
            [trigger]="trigger$ | async"
            [isLoggedIn]="isLoggedIn$ | async"
            (handleFetchOptions)="fetchRatingOptions()"
            (handleUpdateAction)="updateRatingAction($event)"
            (handleSaveFeedback)="saveFeedback($event)"
            (handleHideModal)="hideRatingModal()"
        ></supr-app-rating-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppRatingComponent implements OnInit {
    ratingOptions$: Observable<RatingOption[]>;
    isLoggedIn$: Observable<boolean>;
    showModal$: Observable<boolean>;
    trigger$: Observable<string>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.ratingOptions$ = this.adapter.ratingOptions$;
        this.isLoggedIn$ = this.adapter.isLoggedIn$;
        this.showModal$ = this.adapter.showRatingModal$;
        this.trigger$ = this.adapter.ratingTrigger$;
    }

    fetchRatingOptions() {
        this.adapter.fetchRatingOptions();
    }

    updateRatingAction(data: SuprApi.RatingActionBody) {
        this.adapter.updateRatingAction(data);
    }

    saveFeedback(data: SuprApi.RatingFeedbackBody) {
        this.adapter.saveFeedback(data);
    }

    hideRatingModal() {
        this.adapter.hideRatingModal();
    }
}
