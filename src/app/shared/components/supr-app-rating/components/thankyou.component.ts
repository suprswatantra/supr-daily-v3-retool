import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { PlatformService } from "@services/util/platform.service";
import { RatingSetting } from "@types";

import { TEXTS } from "../constants/supr-app-rating.constants";
import { SA_OBJECT_NAMES } from "../constants/analytics";

@Component({
    selector: "supr-app-rating-thankyou",
    template: `
        <div class="thankyou">
            <supr-text type="subtitle">
                {{ nudgeDetails?.title || TEXTS.THANKYOU.TITLE }}
            </supr-text>
            <div class="divider16"></div>

            <supr-text type="body" class="subtitle">
                {{ subTitle || TEXTS.THANKYOU.SUB_TITLE }}
            </supr-text>
            <div class="divider32"></div>

            <div class="suprRow">
                <div class="suprColumn">
                    <supr-button
                        (handleClick)="handleClick.emit(0)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.THANKYOU_NOTNOW}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.secondaryText || TEXTS.NOT_NOW }}
                        </supr-text>
                    </supr-button>
                </div>

                <div class="spacer16"></div>

                <div class="suprColumn">
                    <supr-button
                        class="primary"
                        (handleClick)="handleClick.emit(1)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.THANKYOU_YES}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.primaryText || TEXTS.SURE }}
                        </supr-text>
                    </supr-button>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-app-rating.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThankyouComponent implements OnInit {
    @Input() nudgeDetails: RatingSetting.Text;
    @Input() trigger: string;
    @Output() handleClick: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
    subTitle: string;

    constructor(private platformService: PlatformService) {}

    ngOnInit() {
        if (!this.nudgeDetails) {
            return;
        }

        if (this.platformService.isIOS()) {
            this.subTitle = this.nudgeDetails.subTitleIos;
        } else {
            this.subTitle = this.nudgeDetails.subTitle;
        }
    }
}
