import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import { MODAL_NAMES, NUDGE_TYPES, NUDGE_SETTINGS_KEYS } from "@constants";
import { RatingOption } from "@models";

import { ModalService } from "@services/layout/modal.service";
import { PlatformService } from "@services/util/platform.service";
import { SettingsService } from "@services/shared/settings.service";
import { ToastService } from "@services/layout/toast.service";
import { UtilService } from "@services/util/util.service";

import { RatingSetting, SuprApi } from "@types";

import {
    ACTIONS,
    SECTIONS,
    MODAL_TYPES,
    TEXTS,
} from "../constants/supr-app-rating.constants";

@Component({
    selector: "supr-app-rating-wrapper",
    template: `
        <ng-container *ngIf="showModal && ratingNudge?.enabled">
            <supr-modal
                [backdropDismiss]="false"
                (handleClose)="hideModal()"
                modalName="${MODAL_NAMES.APP_RATING}"
                [saContext]="trigger"
            >
                <div class="wrapper">
                    <ng-container [ngSwitch]="_modalType">
                        <supr-app-rating-prompt
                            *ngSwitchCase="MODAL_TYPES.PROMPT"
                            [nudgeDetails]="ratingNudge?.text?.prompt"
                            [trigger]="trigger"
                            (handleClick)="promptClick($event)"
                        ></supr-app-rating-prompt>
                        <supr-app-rating-thankyou
                            *ngSwitchCase="MODAL_TYPES.THANKYOU"
                            [nudgeDetails]="ratingNudge?.text?.thankyou"
                            [trigger]="trigger"
                            (handleClick)="thankyouClick($event)"
                        ></supr-app-rating-thankyou>
                        <ng-container
                            *ngIf="ratingOptions && ratingOptions.length"
                        >
                            <supr-app-rating-feedback
                                *ngSwitchCase="MODAL_TYPES.FEEDBACK"
                                [ratingOption]="ratingOptions[0]"
                                [trigger]="trigger"
                                [nudgeDetails]="ratingNudge?.text?.feedback"
                                (handleSaveFeedback)="saveFeedback($event)"
                            ></supr-app-rating-feedback>
                        </ng-container>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-app-rating.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() ratingOptions: RatingOption[];
    @Input() showModal: boolean;
    @Input() trigger: string;
    @Input() isLoggedIn: boolean;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();
    @Output() handleFetchOptions: EventEmitter<void> = new EventEmitter();
    @Output() handleUpdateAction: EventEmitter<
        SuprApi.RatingActionBody
    > = new EventEmitter();
    @Output() handleSaveFeedback: EventEmitter<
        SuprApi.RatingFeedbackBody
    > = new EventEmitter();

    MODAL_TYPES = MODAL_TYPES;
    _modalType = MODAL_TYPES.PROMPT;
    ratingNudge: RatingSetting.Nudge;

    constructor(
        private modalService: ModalService,
        private platformService: PlatformService,
        private settingsService: SettingsService,
        private toastService: ToastService,
        private utilService: UtilService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleIsLoggedInChange(changes["isLoggedIn"]);
        this.handleShowModalChange(changes["showModal"]);
    }

    hideModal() {
        this.handleHideModal.emit();
    }

    promptClick(btnIndex: number) {
        switch (btnIndex) {
            case 1: // primary
                this.showThankyouModal();
                break;
            case 0: // secondary
                this.showFeedbackModal();
                break;
            case -1: // never
                this.neverShow();
        }
    }

    thankyouClick(btnIndex: number) {
        switch (btnIndex) {
            case 1: // primary
                this.goToStoreReview();
                break;
            case 0: // secondary
                this.handleNotNow();
                break;
        }
    }

    saveFeedback(payload: SuprApi.RatingFeedbackBody) {
        // Close the modal
        this.closeModal();

        // Save the feedback
        this.handleSaveFeedback.emit(payload);

        // Show the toast for saving
        this.showFeedbackToast();
    }

    private handleIsLoggedInChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.currentValue && this.isOptionsEmpty()) {
            this.handleFetchOptions.emit();
        }
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        this._modalType = MODAL_TYPES.PROMPT;
        this.ratingNudge = this.settingsService.getSettingsValue(
            NUDGE_SETTINGS_KEYS[NUDGE_TYPES.RATING],
            {}
        );
    }

    private closeModal() {
        this.modalService.closeModal(-1, true);
    }

    private isOptionsEmpty(): boolean {
        return !this.ratingOptions;
    }

    private showThankyouModal() {
        this._modalType = MODAL_TYPES.THANKYOU;
    }

    private showFeedbackModal() {
        // Toggle the modal
        this._modalType = MODAL_TYPES.FEEDBACK;

        // Update rating action
        this.updateRatingAction(
            ACTIONS.NEVER_ASK_AGAIN,
            SECTIONS.RATING_OPTION_LIST
        );
    }

    private neverShow() {
        // Close the modal
        this.closeModal();

        // Update rating action
        this.updateRatingAction(
            ACTIONS.NEVER_ASK_AGAIN,
            SECTIONS.RATING_OPTION_LIST
        );
    }

    private goToStoreReview() {
        // Close the modal
        this.closeModal();

        // Route to store
        this.goToPlayStoreReview();

        // Update rating action
        this.updateRatingAction(ACTIONS.OKAY, SECTIONS.RATING_THANK_YOU);
    }

    private handleNotNow() {
        // Close the modal
        this.closeModal();

        // Update rating action
        this.updateRatingAction(
            ACTIONS.REMIND_ME_LATER,
            SECTIONS.RATING_THANK_YOU
        );
    }

    private showFeedbackToast() {
        const message = this.utilService.getNestedValue(
            this.ratingNudge,
            "text.toastText",
            TEXTS.FEEDBACK.THANK_YOU
        );
        this.toastService.present(message);
    }

    private updateRatingAction(actionType: string, section: string) {
        this.handleUpdateAction.emit({
            actionType,
            section,
        });
    }

    private goToPlayStoreReview() {
        this.platformService.redirectToPlayStoreForReview(
            () => {},
            () => {}
        );
    }
}
