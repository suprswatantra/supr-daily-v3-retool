import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { RatingSetting } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics";
import { TEXTS } from "../constants/supr-app-rating.constants";

@Component({
    selector: "supr-app-rating-prompt",
    template: `
        <div class="prompt">
            <supr-text type="subtitle">
                {{ nudgeDetails?.title || TEXTS.ARE_YOU_ENJOYING_HEADER }}
            </supr-text>
            <div class="divider32"></div>

            <div class="suprRow">
                <div class="suprColumn">
                    <supr-button
                        (handleClick)="handleClick.emit(0)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.PROMPT_NO}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.secondaryText || TEXTS.NO }}
                        </supr-text>
                    </supr-button>
                </div>

                <div class="spacer16"></div>

                <div class="suprColumn">
                    <supr-button
                        class="primary"
                        (handleClick)="handleClick.emit(1)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.PROMPT_YES}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.primaryText || TEXTS.YES }}
                        </supr-text>
                    </supr-button>
                </div>
            </div>
            <div class="divider16"></div>

            <div
                class="suprRow center"
                (click)="handleClick.emit(-1)"
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.PROMPT_NEVER}"
                [saContext]="trigger"
            >
                <supr-text class="never" type="subtext10">
                    {{ nudgeDetails?.neverAskText || TEXTS.NEVER_ASK_AGAIN }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../styles/supr-app-rating.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PromptComponent {
    @Input() nudgeDetails: RatingSetting.Text;
    @Input() trigger: string;
    @Output() handleClick: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
}
