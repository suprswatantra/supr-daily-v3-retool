import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnInit,
} from "@angular/core";

import { ContextOptions } from "@models";
import { SuprDictNumBool } from "@types";

import { FEEDBACK_OPTIONS_TYPES } from "../../constants/supr-app-rating.constants";

@Component({
    selector: "supr-app-rating-feedback-options",
    template: `
        <div class="feedbackOptions">
            <ng-container *ngFor="let item of options?.list">
                <div class="suprRow" (click)="selectItem(item.id)">
                    <supr-icon
                        [class.selected]="selectedList[item.id]"
                        [name]="
                            selectedList[item.id]
                                ? 'approve_filled'
                                : 'unchecked'
                        "
                    ></supr-icon>
                    <div class="spacer8"></div>
                    <supr-text type="regular14">
                        {{ item.displayText }}
                    </supr-text>
                </div>
                <div class="divider16"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/supr-app-rating.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackOptionsComponent implements OnInit {
    @Input() options: ContextOptions;
    @Output() handleSelectOption: EventEmitter<number[]> = new EventEmitter();

    selectedList: SuprDictNumBool = {};
    private isRadioBtn: boolean;

    ngOnInit() {
        this.setOptionsType();
    }

    selectItem(optionId: number) {
        if (this.isRadioBtn) {
            this.selectedList = {
                [optionId]: true,
            };
        } else {
            this.selectedList = {
                ...this.selectedList,
                [optionId]: !this.selectedList[optionId],
            };
        }

        const ids = Object.keys(this.selectedList)
            .filter(id => !!this.selectedList[id])
            .map(_id => +_id);
        this.handleSelectOption.emit(ids);
    }

    private setOptionsType() {
        this.isRadioBtn =
            this.options.type === FEEDBACK_OPTIONS_TYPES.RADIO_BUTTON;
    }
}
