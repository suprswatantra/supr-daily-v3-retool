import {
    Component,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { RatingOption } from "@models";
import { ModalService } from "@services/layout/modal.service";
import { RatingSetting, SuprApi } from "@types";

import { TEXTS } from "../../constants/supr-app-rating.constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics";

@Component({
    selector: "supr-app-rating-feedback",
    template: `
        <div class="feedback">
            <supr-text type="subtitle">
                {{ nudgeDetails?.title || TEXTS.FEEDBACK.TITLE }}
            </supr-text>
            <div class="divider16"></div>

            <supr-text type="body" class="subtitle">
                {{ nudgeDetails?.subTitle || TEXTS.FEEDBACK.SUB_TITLE }}
            </supr-text>
            <div class="divider24"></div>

            <supr-app-rating-feedback-options
                [options]="ratingOption?.optionContext?.options"
                (handleSelectOption)="onOptionSelect($event)"
            ></supr-app-rating-feedback-options>
            <div class="divider16"></div>

            <textarea
                #feedback_input
                rows="4"
                maxlength="500"
                [placeholder]="
                    nudgeDetails?.placeHolderText || TEXTS.FEEDBACK.PLACEHOLDER
                "
            ></textarea>
            <div class="divider32"></div>

            <div class="suprRow">
                <div class="suprColumn">
                    <supr-button
                        (handleClick)="closeModal()"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.FEEDBACK_NOTNOW}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.secondaryText || TEXTS.NOT_NOW }}
                        </supr-text>
                    </supr-button>
                </div>

                <div class="spacer16"></div>

                <div class="suprColumn">
                    <supr-button
                        class="primary"
                        [disabled]="disableBtn"
                        (handleClick)="saveFeedback()"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.FEEDBACK_SUBMIT}"
                        [saContext]="trigger"
                    >
                        <supr-text type="body">
                            {{ nudgeDetails?.primaryText || TEXTS.SUBMIT }}
                        </supr-text>
                    </supr-button>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/supr-app-rating.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackComponent {
    @Input() ratingOption: RatingOption;
    @Input() nudgeDetails: RatingSetting.Text;
    @Input() trigger: string;

    @Output() handleSaveFeedback: EventEmitter<
        SuprApi.RatingFeedbackBody
    > = new EventEmitter();

    @ViewChild("feedback_input", { static: true }) feedbackInputEl: ElementRef;

    TEXTS = TEXTS;
    disableBtn = true;
    private selectedOptions: number[] = [];

    constructor(private modalService: ModalService) {}

    onOptionSelect(optionIds: number[]) {
        this.selectedOptions = optionIds;
        this.disableBtn = !this.selectedOptions.length;
    }

    saveFeedback() {
        const body = {
            feedbackContext: {
                optionId: +this.ratingOption.optionId,
                reasons: this.selectedOptions,
                comments: this.feedbackInputEl.nativeElement.value,
            },
            meta: {},
        };
        this.handleSaveFeedback.emit(body);
    }

    closeModal() {
        this.modalService.closeModal(-1, true);
    }
}
