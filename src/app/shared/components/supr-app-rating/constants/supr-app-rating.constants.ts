export const TEXTS = {
    NEVER_ASK_AGAIN: "DON’T ASK ME AGAIN",
    SUBMIT: "Submit",
    NOT_NOW: "Not now",
    SURE: "Sure",
    YES: "Yes",
    NO: "No",
    ARE_YOU_ENJOYING_HEADER: "Are you enjoying your Supr Daily experience?",
    THANKYOU: {
        TITLE: "Glad you are enjoying Supr Daily!",
        SUB_TITLE: "Take a moment to rate us on Play Store.",
    },
    FEEDBACK: {
        TITLE: "We are on a mission to serve you better",
        SUB_TITLE: "How can we improve further?",
        PLACEHOLDER:
            "Anything specific in mind. Please let us know. We are listening!!",
        THANK_YOU: "Thank you for your valuable feedback!",
    },
};

export const FEEDBACK_OPTIONS_TYPES = {
    RADIO_BUTTON: "radio-button",
    CHECKBOX_BUTTON: "checkbox-button",
};

export const MODAL_TYPES = {
    PROMPT: "prompt",
    THANKYOU: "thankyou",
    FEEDBACK: "feedback",
};

export const ACTIONS = {
    REMIND_ME_LATER: "REMIND_ME_LATER",
    NEVER_ASK_AGAIN: "NEVER_ASK_AGAIN",
    OKAY: "OKAY",
};

export const SECTIONS = {
    RATING_OPTION_LIST: "rating_option_list_modal",
    RATING_THANK_YOU: "rating_thank_you_modal",
    FEEDBACK_PAGE: "feedback",
};
