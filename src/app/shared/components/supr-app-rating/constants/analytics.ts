export const SA_OBJECT_NAMES = {
    CLICK: {
        PROMPT_YES: "prompt-yes",
        PROMPT_NO: "prompt-no",
        PROMPT_NEVER: "prompt-never",
        FEEDBACK_SUBMIT: "feedback-submit",
        FEEDBACK_NOTNOW: "feedback-not-now",
        THANKYOU_NOTNOW: "thankyou-not-now",
        THANKYOU_YES: "thankyou-yes",
    },
};
