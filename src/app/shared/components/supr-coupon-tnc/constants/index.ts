export const TEXTS = {
    TNC_HEADING: "Terms & Conditions",
    COUPON_APPLIED_TEXT: "Coupon applied successfully!",
    MORE_TNC_DETAILS: "More Details",
};
