import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

import { TEXTS } from "../constants";
@Component({
    selector: "supr-coupon-tnc-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal (handleClose)="handleClose?.emit()">
                <div class="wrapper">
                    <supr-text type="subtitle">{{
                        texts.TNC_HEADING
                    }}</supr-text>
                    <div class="divider32"></div>
                    <div class="suprRow">
                        <div class="couponBlock">
                            <supr-text type="caption">
                                {{ couponCode }}
                            </supr-text>
                        </div>
                        <div class="spacer8"></div>
                        <supr-text type="caption" class="lightText">
                            {{ texts.COUPON_APPLIED_TEXT }}
                        </supr-text>
                    </div>
                    <div class="divider8"></div>
                    <div class="suprRow couponMessageBlock">
                        <supr-text type="caption">
                            {{ couponCodeMessage }}
                        </supr-text>
                    </div>
                    <div class="divider20"></div>
                    <ng-container *ngIf="tnc">
                        <div class="suprRow">
                            <supr-text type="action14">
                                {{ texts.MORE_TNC_DETAILS }}
                            </supr-text>
                        </div>
                        <div class="divider16"></div>
                        <ng-container *ngFor="let tncBlock of tncArray">
                            <supr-text type="caption">
                                {{ tncBlock }}.
                            </supr-text>
                            <div class="divider8"></div>
                        </ng-container>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-coupon-tnc.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit, OnChanges {
    @Input() showModal: boolean;
    @Input() couponCode: string;
    @Input() couponCodeMessage: string;
    @Input() tnc: string;
    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;
    tncArray: string[];

    ngOnInit() {
        this.prepareTnc();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["tnc"])) {
            this.prepareTnc();
        }
    }

    private prepareTnc() {
        if (this.tnc) {
            this.tncArray = this.tnc.split(".");
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
