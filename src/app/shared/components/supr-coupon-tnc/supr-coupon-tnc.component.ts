import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-coupon-tnc",
    template: `
        <supr-coupon-tnc-wrapper
            [showModal]="showModal"
            [couponCode]="couponCode"
            [couponCodeMessage]="couponCodeMessage"
            [tnc]="tnc"
            (handleClose)="handleClose?.emit()"
        ></supr-coupon-tnc-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponTncComponent {
    @Input() showModal: boolean;
    @Input() couponCode: string;
    @Input() couponCodeMessage: string;
    @Input() tnc: string;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
}
