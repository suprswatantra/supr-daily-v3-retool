import {
    Input,
    OnInit,
    Output,
    Component,
    OnChanges,
    SimpleChange,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { CartService } from "@services/shared/cart.service";
import { StoreService } from "@services/data/store.service";
import { RouterService } from "@services/util/router.service";
import { CalendarService } from "@services/date/calendar.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { Schedule } from "@shared/models";

import { SCHEDULE_DAYS } from "@pages/schedule/constants/schedule.constants";
import {
    TEXTS,
    ANALYTICS,
} from "@shared/components/supr-schedule-item/constants/supr-schedule-item.constants";

@Component({
    selector: "supr-schedule-item",
    templateUrl: "./supr-schedule-item.component.html",
    styleUrls: ["./supr-schedule-item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleItemComponent implements OnInit, OnChanges {
    day: string;
    previousDay: string;
    noSchedule: boolean;

    @Input() date: string;
    @Input() locked = false;
    @Input() paused = false;
    @Input() tomorrow = false;
    @Input() pausedMsg: string;
    @Input() schedule: Schedule;
    @Input() statusText: string;
    @Input() supportAction = true;
    @Input() pausedBySystem = false;
    @Input() subscriptionView = false;
    @Input() pausedBySystemMsg: string;
    @Input() noSchedulesMessage: string;
    @Input() pastOrderSkuList: Array<number>;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    TEXTS = TEXTS;
    tpEnabled = false;
    scheduleItemMessage: string;
    saObjectNamePausedBySystem = ANALYTICS.IMPRESSION.PAUSED_BY_SYSTEM;

    constructor(
        private cartService: CartService,
        private storeService: StoreService,
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private calendarService: CalendarService
    ) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleDateChange(changes["date"]);
        this.handleScheduleChange(changes["schedule"]);
    }

    getClasses() {
        if (this.tomorrow && this.tpEnabled) {
            return {
                disabled: true,
                bgAlert: true,
                systemVacation: !this.paused && this.pausedBySystem,
            };
        }
        return {
            disabled: this.paused,
            systemVacation: !this.paused && this.pausedBySystem,
        };
    }

    goToSearchPage(scheduleDate: string) {
        this.cartService.setDeliveryDate(scheduleDate);
        this.routerService.goToSearchPage();
    }

    openPauseModal() {
        this.storeService.togglePauseModal();
    }

    private init() {
        this.setTpStatus();
        this.setDateInfo();
        this.checkForNoSchedule();
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private handleDateChange(dateChanges: SimpleChange) {
        if (
            dateChanges &&
            !dateChanges.firstChange &&
            dateChanges.previousValue !== dateChanges.currentValue
        ) {
            this.setDateInfo();
        }
    }

    private handleScheduleChange(scheduleChanges: SimpleChange) {
        if (scheduleChanges && !scheduleChanges.firstChange) {
            this.checkForNoSchedule();
        }
    }

    private checkForNoSchedule() {
        this.noSchedule = !this.scheduleService.hasScheduledDeliveries(
            this.schedule
        );
    }

    private setDateInfo() {
        this.day = this.scheduleService.getScheduleDay(this.date);
        if (!this.schedule || !this.schedule.message) {
            let previousDay = this.scheduleService.getPreviousScheduleDay(
                this.date
            );

            previousDay =
                previousDay === SCHEDULE_DAYS.TODAY ||
                previousDay === SCHEDULE_DAYS.TOMORROW
                    ? previousDay.toLowerCase()
                    : previousDay;

            this.scheduleItemMessage = `${TEXTS.SCHEDULED_NOT_PAUSED_TEXT} ${previousDay}`;
        }
    }
}
