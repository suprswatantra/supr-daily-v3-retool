export const TEXTS = {
    ON_VACATION_TEXT: "Vacation active",
    SCHEDULED_NOT_PAUSED_TEXT: "Add or edit your orders before 11 pm",
    NOT_SCHEDULED_TEXT: "No orders for this day",
    PAUSED_TEXT: "Vacation active",
    SYSTEM_PAUSED_TEXT: "Deliveries paused",
};

export const ANALYTICS = {
    IMPRESSION: {
        PAUSED_BY_SYSTEM: "delivery-paused-by-system",
    },
};
