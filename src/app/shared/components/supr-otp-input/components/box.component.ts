import {
    Component,
    Input,
    Output,
    OnInit,
    ChangeDetectionStrategy,
    OnChanges,
    SimpleChanges,
    HostListener,
    ElementRef,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-otp-input-box",
    template: `
        <ion-row>
            <div
                class="box"
                *ngFor="let _ of boxArr; index as i"
                (click)="onBoxClick($event, i)"
            >
                <supr-text type="subtitle" *ngIf="otpArr[i] !== DASH_SYMBOL">
                    {{ otpArr[i] }}
                </supr-text>

                <supr-text
                    type="subtitle"
                    class="dash"
                    *ngIf="otpArr[i] === DASH_SYMBOL"
                >
                    {{ otpArr[i] }}
                </supr-text>
            </div>
        </ion-row>
    `,
    styleUrls: ["../supr-otp-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxComponent implements OnInit, OnChanges {
    @Input() boxCount: string;
    @Input() otpValue: string;
    @Output() boxTap: EventEmitter<number> = new EventEmitter();

    boxArr: number[] = [];
    otpArr: string[] = [];

    DASH_SYMBOL = "-";

    constructor(private elRef: ElementRef) {}

    ngOnInit() {
        const maxOtpLen = +this.boxCount;
        if (maxOtpLen > 0) {
            this.boxArr = Array.apply(null, Array(maxOtpLen)).map(
                (_, i: number) => i
            );
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["otpValue"];
        if (!change) {
            return;
        }

        const maxOtpLen = +this.boxCount;
        const otpVal =
            this.otpValue.length > maxOtpLen
                ? this.otpValue.substr(0, maxOtpLen)
                : this.asignValue(this.otpValue);
        this.otpArr = otpVal.split("");
    }

    onBoxClick(event: MouseEvent, boxIndex: number) {
        event.stopPropagation();
        this.boxTap.emit(boxIndex + 1);
    }

    @HostListener("document:click", ["$event"])
    onClick(event: MouseEvent) {
        // Clicked outside of host
        if (!this.elRef.nativeElement.contains(event.target)) {
            this.boxTap.emit(-1);
        }
    }

    private asignValue(value: string) {
        const count = +this.boxCount;
        if (this.otpValue.length > count) {
            return value;
        } else {
            return value + this.DASH_SYMBOL.repeat(count - value.length);
        }
    }
}
