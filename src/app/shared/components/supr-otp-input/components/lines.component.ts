import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-otp-input-lines",
    template: `
        <div class="suprRow spaceBetween">
            <div class="line" *ngFor="let _ of linesArr; index as i"></div>
        </div>
    `,
    styleUrls: ["../supr-otp-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LinesComponent implements OnInit {
    @Input() linesCount: string;

    linesArr: number[] = [];

    ngOnInit() {
        const maxOtpLen = +this.linesCount;
        if (maxOtpLen > 0) {
            this.linesArr = Array.apply(null, Array(maxOtpLen)).map(
                (_, i: number) => i
            );
        }
    }
}
