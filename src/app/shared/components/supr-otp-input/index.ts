import { OtpInputComponent } from "./supr-otp-input.component";
import { LinesComponent } from "./components/lines.component";
import { BoxComponent } from "./components/box.component";

export const otpInputComponents = [
    OtpInputComponent,
    LinesComponent,
    BoxComponent,
];
