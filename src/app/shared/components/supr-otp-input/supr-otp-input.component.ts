import {
    Component,
    OnInit,
    OnChanges,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
    ChangeDetectorRef,
    SimpleChanges,
} from "@angular/core";

import { InputComponent } from "@shared/components/supr-input/supr-input.component";
import { INPUT, OTP_INPUT } from "@constants";

const OTP_LEN_CSS_VARIABLE = "--otp-len";

@Component({
    selector: "supr-otp-input",
    template: `
        <div class="otp" [class.error]="error">
            <supr-input
                [type]="inputType"
                textType="subtitle"
                [debounceTime]="debounceTime"
                (handleInputChange)="changeOtp($event)"
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                [saPosition]="saPosition"
                [saContext]="saContext"
            ></supr-input>
            <supr-otp-input-lines
                [linesCount]="length"
                *ngIf="!otpValue?.length"
            >
            </supr-otp-input-lines>
        </div>
    `,
    styleUrls: ["./supr-otp-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpInputComponent implements OnInit, OnChanges {
    @Input() length: string;
    @Input() value: string;
    @Input() errMsg: string;
    @Input() error = false;

    @Input() saObjectName: string;
    @Input() saObjectValue: string;
    @Input() saContext: string;
    @Input() saPosition: number;

    @Output() updateOtp: EventEmitter<string> = new EventEmitter();

    @ViewChild(InputComponent, { static: true }) inputComponent: InputComponent;

    otpValue = "";
    debounceTime = OTP_INPUT.DEBOUNCE_TIME;
    inputType = INPUT.TYPES.NUMBER;

    private otpReg = new RegExp(/^\d{1,6}$/);
    private inputEl: ElementRef;

    constructor(private hostEl: ElementRef, private cdr: ChangeDetectorRef) {}

    ngOnInit() {
        this.otpValue = this.value || "";
        this.inputEl = this.inputComponent.getInputElement();
        this.hostEl.nativeElement.style.setProperty(
            OTP_LEN_CSS_VARIABLE,
            this.length
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["value"];
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.previousValue !== change.currentValue &&
            change.currentValue
        ) {
            this.updateInputValue(this.value);
            this.setOtpValue(this.value);
        }
    }

    changeOtp(otpText: string) {
        const otpLen = +this.length;
        const textLen = otpText.length;

        if (!textLen) {
            this.setOtpValue("");
            return;
        }

        if (!this.isValidOtp(otpText)) {
            this.updateInputValue(this.otpValue);
            return;
        }

        if (textLen === otpLen) {
            this.setBlur();
        }

        this.setOtpValue(otpText);
        this.updateOtp.emit(this.otpValue);
    }

    private setBlur() {
        this.inputEl.nativeElement.blur();
    }

    private updateInputValue(value: string) {
        this.inputEl.nativeElement.value = value;
    }

    private isValidOtp(otp: string): boolean {
        return this.otpReg.test(otp);
    }

    private setOtpValue(value: string) {
        this.otpValue = value;
        this.cdr.detectChanges();
    }
}
