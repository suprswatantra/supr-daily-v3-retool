export const ANALYTICS_OBJECT_NAME = {
    CLICK: {
        CTA: "supr-pass-card-cta",
        CARD: "supr-pass-card",
    },
    IMPRESSION: {
        CARD: "supr-pass-card",
    },
};
