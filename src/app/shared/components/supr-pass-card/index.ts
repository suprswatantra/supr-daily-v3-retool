import { SuprPassCardComponent } from "./supr-pass-card.component";
import { WrapperComponent } from "./components/wrapper.component";
import { SuprPassBigCardComponent } from "./components/big-card.component";
import { SuprPassSmallCardComponent } from "./components/small-card.component";

export const suprPassCardComponents = [
    SuprPassCardComponent,
    WrapperComponent,
    SuprPassBigCardComponent,
    SuprPassSmallCardComponent,
];
