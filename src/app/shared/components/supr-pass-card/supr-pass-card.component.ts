import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Input,
} from "@angular/core";

import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";
import { SuprPassCard } from "@shared/models";
import { Observable } from "rxjs";

@Component({
    selector: "supr-pass-card",
    template: `
        <supr-pass-card-wrapper
            [suprPassBigCardDetails]="suprPassBigCardDetails$ | async"
            [suprPassSmallCardDetails]="suprPassSmallCardDetails$ | async"
            [showSmallCard]="showSmallCard"
            [saCardContext]="saCardContext"
        ></supr-pass-card-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassCardComponent implements OnInit {
    @Input() showSmallCard: boolean;
    @Input() saCardContext: string;

    suprPassBigCardDetails$: Observable<SuprPassCard>;
    suprPassSmallCardDetails$: Observable<SuprPassCard>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.suprPassBigCardDetails$ = this.adapter.suprPassBigCardDetails$;
        this.suprPassSmallCardDetails$ = this.adapter.suprPassSmallCardDetails$;
    }
}
