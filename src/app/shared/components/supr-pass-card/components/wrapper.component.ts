import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SuprPassCard } from "@shared/models";

import { ANALYTICS_OBJECT_NAME } from "../constants/analytics.constants";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-pass-card-wrapper",
    template: `
        <ng-container *ngIf="showSmallCard; else bigCard">
            <supr-pass-small-card
                *ngIf="suprPassSmallCardDetails?.textBanners"
                [suprPassCard]="suprPassSmallCardDetails"
                [saCardContext]="saCardContext"
                (handleSuprPassCtaClick)="
                    suprPassCtaClick(
                        $event,
                        suprPassSmallCardDetails?.ctaInfo?.routeUrl
                    )
                "
                (handleGoToSuprPassPage)="goToSuprPassPage()"
                saImpression
                saObjectName="${ANALYTICS_OBJECT_NAME.IMPRESSION.CARD}"
                [saContext]="saCardContext"
            ></supr-pass-small-card>
        </ng-container>
        <ng-template #bigCard>
            <supr-pass-big-card
                *ngIf="suprPassBigCardDetails?.textBanners"
                [suprPassCard]="suprPassBigCardDetails"
                [saCardContext]="saCardContext"
                (handleSuprPassCtaClick)="
                    suprPassCtaClick(
                        $event,
                        suprPassSmallCardDetails?.ctaInfo?.routeUrl
                    )
                "
                (handleGoToSuprPassPage)="goToSuprPassPage()"
                saImpression
                saObjectName="${ANALYTICS_OBJECT_NAME.IMPRESSION.CARD}"
                [saContext]="saCardContext"
            ></supr-pass-big-card>
        </ng-template>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() suprPassBigCardDetails: SuprPassCard;
    @Input() suprPassSmallCardDetails: SuprPassCard;
    @Input() showSmallCard: boolean;
    @Input() saCardContext: string;

    constructor(private routerService: RouterService) {}

    goToSuprPassPage() {
        this.routerService.goToSuprPassPage();
    }

    suprPassCtaClick(event: TouchEvent, url) {
        event.stopPropagation();

        if (!url) {
            return;
        }

        this.routerService.goToUrl(url);
    }
}
