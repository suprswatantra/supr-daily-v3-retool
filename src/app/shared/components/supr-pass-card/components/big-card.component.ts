import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { SuprPassCard } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { ANALYTICS_OBJECT_NAME } from "../constants/analytics.constants";
import { CARD_STYLE_LIST } from "../constants";

@Component({
    selector: "supr-pass-big-card",
    template: `
        <div
            class="suprPassCard"
            (click)="handleGoToSuprPassPage.emit()"
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAME.CLICK.CARD}"
            [saContext]="saCardContext"
            #wrapper
        >
            <div class="translucentImage">
                <supr-svg></supr-svg>
            </div>
            <ng-container
                *ngIf="
                    suprPassCard?.textBanners && suprPassCard.textBanners.length
                "
            >
                <ng-container
                    *ngFor="let textBlock of suprPassCard.textBanners"
                >
                    <div class="textBlock">
                        <div class="suprRow">
                            <supr-text type="caption">
                                {{ textBlock?.upperText }}
                            </supr-text>
                        </div>
                        <div class="suprRow bottom">
                            <supr-text type="subtitle" class="bottomAlign">
                                {{ textBlock?.middleText }}
                            </supr-text>
                            <div class="subText">
                                <supr-text type="caption" class="lightText">
                                    {{ textBlock?.lowerText }}
                                </supr-text>
                            </div>
                        </div>
                    </div>
                    <div class="divider16"></div>
                </ng-container>
            </ng-container>
            <div class="actionBlock" *ngIf="suprPassCard?.ctaInfo?.ctaText">
                <div class="suprRow">
                    <div
                        class="suprRow cta"
                        (click)="handleSuprPassCtaClick.emit($event)"
                        saClick
                        saObjectName="${ANALYTICS_OBJECT_NAME.CLICK.CTA}"
                        [saContext]="saCardContext"
                    >
                        <supr-text type="action14">
                            {{ suprPassCard?.ctaInfo?.ctaText }}
                        </supr-text>
                        <supr-icon name="chevron_right"> </supr-icon>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../supr-pass-card.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassBigCardComponent implements OnInit {
    @Input() suprPassCard: SuprPassCard;
    @Input() saCardContext: string;

    @Output() handleSuprPassCtaClick: EventEmitter<
        TouchEvent
    > = new EventEmitter();
    @Output() handleGoToSuprPassPage: EventEmitter<void> = new EventEmitter();

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.suprPassCard) {
            return;
        }

        this.utilService.setStyles(
            this.suprPassCard,
            CARD_STYLE_LIST,
            this.wrapperEl
        );
    }
}
