import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { SuprPassCard } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { ANALYTICS_OBJECT_NAME } from "../constants/analytics.constants";
import { CARD_STYLE_LIST } from "../constants";

@Component({
    selector: "supr-pass-small-card",
    template: `
        <div
            class="suprPassCard smallCard"
            (click)="handleGoToSuprPassPage.emit()"
            saClick
            saObjectName="${ANALYTICS_OBJECT_NAME.CLICK.CARD}"
            [saContext]="saCardContext"
            #wrapper
        >
            <div class="translucentImage small">
                <supr-svg></supr-svg>
            </div>
            <div class="suprRow center top">
                <div class="suprRow leftSection">
                    <div
                        *ngIf="
                            suprPassCard?.textBanners &&
                            suprPassCard.textBanners.length
                        "
                    >
                        <ng-container
                            *ngFor="
                                let textBlock of suprPassCard.textBanners;
                                let last = last
                            "
                        >
                            <div class="textBlock">
                                <div class="suprRow">
                                    <supr-text type="caption">
                                        {{ textBlock?.upperText }}
                                    </supr-text>
                                </div>
                                <div class="divider4"></div>
                                <div class="suprRow">
                                    <supr-text type="subtitle">
                                        {{ textBlock?.middleText }}
                                    </supr-text>
                                </div>
                                <div class="divider4"></div>
                                <div class="suprRow">
                                    <supr-text type="caption">
                                        {{ textBlock?.lowerText }}
                                    </supr-text>
                                </div>
                            </div>
                            <div class="divider16" *ngIf="!last"></div>
                        </ng-container>
                    </div>
                </div>

                <div class="suprRow rightSection">
                    <div class="suprColumn right">
                        <div class="suprColumn left">
                            <ng-container
                                *ngIf="suprPassCard?.rightHeadingText"
                            >
                                <supr-text type="smallText">
                                    {{ suprPassCard?.rightHeadingText }}
                                </supr-text>
                                <div class="divider4"></div>
                            </ng-container>
                            <div
                                class="suprRow cta"
                                *ngIf="suprPassCard?.ctaInfo?.ctaText"
                                (click)="handleSuprPassCtaClick.emit($event)"
                                saClick
                                saObjectName="${ANALYTICS_OBJECT_NAME.CLICK
                                    .CTA}"
                                [saContext]="saCardContext"
                            >
                                <supr-button>
                                    <div class="suprRow">
                                        <supr-text type="subtext10">
                                            {{ suprPassCard?.ctaInfo?.ctaText }}
                                        </supr-text>
                                        <supr-icon name="chevron_right">
                                        </supr-icon>
                                    </div>
                                </supr-button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../supr-pass-card.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassSmallCardComponent implements OnInit {
    @Input() suprPassCard: SuprPassCard;
    @Input() saCardContext: string;

    @Output() handleSuprPassCtaClick: EventEmitter<
        TouchEvent
    > = new EventEmitter();
    @Output() handleGoToSuprPassPage: EventEmitter<void> = new EventEmitter();

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.suprPassCard) {
            return;
        }

        this.utilService.setStyles(
            this.suprPassCard,
            CARD_STYLE_LIST,
            this.wrapperEl
        );
    }
}
