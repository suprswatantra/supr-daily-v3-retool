import {
    Component,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
    SimpleChange,
    Output,
    EventEmitter,
} from "@angular/core";

import {
    OOS_TEXT,
    EDIT_DELIVERY_ACTIONS,
    DELIVERY_EDIT_MODAL_TEXTS,
} from "@constants";

import { Sku, Subscription, Vacation, ScheduleItem } from "@models";

import { ScheduleService } from "@services/shared/schedule.service";
import { SkuService } from "@services/shared/sku.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { DateService } from "@services/date/date.service";
import { CartService } from "@services/shared/cart.service";
import { CalendarService } from "@services/date/calendar.service";

import { Calendar, ScheduleDict } from "@types";

import { TEXTS } from "@pages/search/constants";
import { MODAL_NAMES } from "@pages/search/constants/analytics.constants";

@Component({
    selector: "supr-subscription-balance-modal",
    template: `
        <supr-modal
            *ngIf="showModal"
            (handleClose)="closeSubBalanceModal()"
            modalName="${MODAL_NAMES.ADD_FROM_SUBSCRIPTION}"
        >
            <supr-subscription-edit-card-container
                *ngIf="!showCalendar"
                [sku]="activeSku"
                [date]="deliveryDateText"
                [schedule]="activeScheduleItem"
                [showCancelAction]="false"
                [showChangeAction]="true"
                [showOrderUpdatedToast]="true"
                [subscription]="activeSubscription"
                [showAdjustmentText]="false"
                [instantRefreshSchedule]="instantRefreshSchedule"
                adjustmentAction="${EDIT_DELIVERY_ACTIONS.USE_BALANCE}"
                confirmBtnText="${TEXTS.SCHEDULE}"
                headerText="${DELIVERY_EDIT_MODAL_TEXTS.SCHEDULE}"
                [secondaryActionText]="secondaryActionText"
                (handleChangeDateClick)="toggleCalendarView()"
                (updateActionListener)="updateActionListener($event)"
            ></supr-subscription-edit-card-container>

            <div class="calendar" *ngIf="showCalendar">
                <div class="suprRow">
                    <supr-icon
                        name="chevron_left"
                        (click)="toggleCalendarView()"
                    ></supr-icon>
                    <supr-text type="subtitle">
                        ${TEXTS.CHOOSE_DELIVERY_DATE}
                    </supr-text>
                </div>
                <div class="divider24"></div>
                <supr-calendar
                    [selectedDate]="calendarSelectedDate"
                    [firstActiveDay]="firstActiveDate"
                    [disablePastDays]="true"
                    [vacation]="vacation"
                    [customDisabledDates]="oosDates"
                    [customDisabledDateText]="oosDateCalendarText"
                    (handleSelectedDate)="selectNewDate($event)"
                ></supr-calendar>
                <div class="divider24"></div>
                <supr-button (handleClick)="confirmDeliveryDate()">
                    <supr-text type="body">
                        ${TEXTS.CONFIRM_DATE} ({{
                            this.calendarSelectedDate.dateText
                                | date: "d LLL, yyy"
                        }})
                    </supr-text>
                </supr-button>
            </div>
        </supr-modal>
    `,
    styleUrls: ["./supr-subscription-balance-modal.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionBalanceModalComponent implements OnInit, OnChanges {
    @Input() vacation: Vacation;
    @Input() scheduleData: ScheduleDict;
    @Input() activeSubscription: Subscription;
    @Input() activeSku: Sku;
    @Input() secondaryActionText?: string;
    @Input() showSubBalanceModal: boolean;
    @Input() instantRefreshSchedule: boolean;

    @Output() handleShowModalChange: EventEmitter<boolean> = new EventEmitter();
    @Output() handleScheduleUpdate: EventEmitter<void> = new EventEmitter();

    showCategories = true;
    showCalendar = false;
    showModal = false;
    activeScheduleItem: ScheduleItem = null;
    deliveryDateText: string;
    calendarSelectedDate: Calendar.DayData;
    firstActiveDate: Calendar.DayData;
    subBalanceBgColor: string;

    oosDates = [];
    oosDateCalendarText = "";

    constructor(
        private suprDateService: SuprDateService,
        private scheduleService: ScheduleService,
        private dateService: DateService,
        private cartService: CartService,
        private calendarService: CalendarService,
        private skuService: SkuService
    ) {}

    ngOnInit() {
        this.setModalData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleChange(changes["activeSubscription"]);
        this.handleChange(changes["showSubBalanceModal"]);
        this.handleChange(changes["activeSku"]);
    }

    closeSubBalanceModal() {
        this.showCalendar = false;
        this.showModal = false;

        if (this.handleShowModalChange) {
            this.handleShowModalChange.emit(false);
        }
    }

    updateActionListener(date: string) {
        if (date && this.dateService.isValidDateString(date)) {
            const dateObject = this.dateService.dateFromText(date);
            const scheduleStartDateObject = this.dateService.startDateOfWeek(
                dateObject
            );
            const scheduleStartDate = this.dateService.textFromDate(
                scheduleStartDateObject
            );

            this.scheduleService.setCustomScheduleScrollDate(date);
            this.scheduleService.setCustomScheduleStartDate(scheduleStartDate);
        }
        this.handleScheduleUpdate.emit();
    }

    toggleCalendarView() {
        this.showCalendar = !this.showCalendar;
    }

    selectNewDate(date: Calendar.DayData) {
        this.setCalendarSelectedDate(date.dateText);
    }

    confirmDeliveryDate() {
        this.deliveryDateText = this.calendarSelectedDate.dateText;
        this.setActiveScheduleItem();
        this.toggleCalendarView();
    }

    private setSubscriptionDate() {
        const customStartDateText = this.cartService.getDeliveryDate();
        /* deliveryDate is the earliest available date based on sub start, vacation and sku oos */
        const deliveryDate = this.getAvailableDeliveryDate(
            this.activeSubscription
        );

        let deliveryDateText = this.dateService.textFromDate(deliveryDate);
        this.setFirstActiveDate(deliveryDateText);

        /* if there's a custom start date and it is after the deliveryDate, choose that date
        since the user is interested in that date */
        if (customStartDateText) {
            const customStartDate = this.dateService.dateFromText(
                customStartDateText
            );
            const daysBetween = this.dateService.daysBetweenTwoDates(
                deliveryDate,
                customStartDate
            );
            if (daysBetween > 0) {
                deliveryDateText = customStartDateText;
            }
        }

        this.deliveryDateText = deliveryDateText;
        this.setCalendarSelectedDate(deliveryDateText);
    }

    private setActiveScheduleItem() {
        this.activeScheduleItem = this.createScheduleItem(
            this.activeSubscription,
            this.scheduleData,
            this.deliveryDateText
        );
    }

    private setShowModal() {
        this.showModal =
            this.showSubBalanceModal &&
            !!this.activeSku &&
            !!this.activeSubscription;
    }

    private getAvailableDeliveryDate(activeSubscription: Subscription): Date {
        /*
            1. if subscription start date is in the past, allow the next available date for the user to schedule
            2. if subscription start date is in the future, it should be the initial date.
            3. This is because the API doesnt allow scheduling a delivery before the subscription start date.
        */

        const subStartDateText = activeSubscription.delivery_dates.start;
        const subStartDate = this.dateService.dateFromText(subStartDateText);
        const nextAvailableDate = this.calendarService.getNextAvailableDate(
            this.vacation,
            this.activeSku
        );

        const daysBetween = this.dateService.daysBetweenTwoDates(
            subStartDate,
            nextAvailableDate
        );

        /* if subStartDate is before nextAvailableDate, return nextAvailableDate
        else return subStartDate */
        return daysBetween >= 0 ? nextAvailableDate : subStartDate;
    }

    private createScheduleItem(
        subscription: Subscription,
        scheduleData: ScheduleDict,
        deliveryDateText: string
    ): ScheduleItem {
        /*
            1. we will be searching the chosen date in the schedule
            2. we will keep going back until we find a delivery
            3. when we find a delivery, we display the remaining quantity that day.
            4. worst case, we have to go back up to the subscription start date.
            5. That day, the remaining quantity would be equal to the subscription balance
        */
        const startDateTime = this.dateService
            .dateFromText(subscription.delivery_dates.start)
            .getTime();

        const scheduledQuantity = this.getSubscriptionQuantityForDate(
            subscription,
            scheduleData,
            deliveryDateText
        );

        const scheduleItem = {
            id: subscription.id,
            modified: false,
            sku_id: subscription.sku_id,
            quantity: scheduledQuantity,
            remaining_quantity: this.calendarService.getQuantityRemaining(
                subscription,
                deliveryDateText,
                scheduleData,
                startDateTime
            ),
        };

        return scheduleItem;
    }

    private getSubscriptionQuantityForDate(
        subscription: Subscription,
        scheduleData: ScheduleDict,
        deliveryDateText: string
    ) {
        const scheduleForDate = scheduleData[deliveryDateText];
        if (scheduleForDate) {
            const order = scheduleForDate.subscriptions.filter(
                (o) => o.id === subscription.id
            );
            if (order && order.length === 1) {
                return order[0].quantity;
            }
        }
        return 0;
    }

    private setCalendarSelectedDate(date: string) {
        this.calendarSelectedDate = this.suprDateService.suprDateFromText(date);
    }

    private setFirstActiveDate(firstDate: string) {
        this.firstActiveDate = this.suprDateService.suprDateFromText(firstDate);
    }

    private setModalData() {
        this.setShowModal();
        if (this.showModal) {
            this.setSubscriptionDate();
            this.setActiveScheduleItem();
            this.setOOSDates();
        }
    }

    private handleChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setModalData();
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private setOOSDates() {
        const oosDates = this.skuService.getOOSDates(this.activeSku);
        if (oosDates) {
            this.oosDates = oosDates;
            this.oosDateCalendarText = OOS_TEXT;
        }
    }
}
