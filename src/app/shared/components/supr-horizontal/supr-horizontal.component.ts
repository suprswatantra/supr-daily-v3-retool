import { Component } from "@angular/core";

@Component({
    selector: "supr-horizontal",
    template: `
        <div
            class="horizontalSlider"
            id="horizontalSliderContainer"
            #horizontalSliderContainer
        >
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-horizontal.component.scss"],
})
export class HorizontalComponent {}
