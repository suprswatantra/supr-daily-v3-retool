import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    OnInit,
} from "@angular/core";

import { DropdownOptions } from "@types";
import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { MODAL_NAMES, MODAL_TYPES } from "@constants";

/*
 <supr-dropdown
    [dropDownOptions]="dropdownList"
    classSelector="issueDropdown"
    title="Choose issue type"
    (select)="issueSelection($event)"
    [disabled]="disabled"
    *ngIf="dropdownList && dropdownList.length > 0"
    [error]="error"
    saImpression
    saObjectName="${SA_OBJECT_NAMES.IMPRESSION
        .SELF_SERVE_COMPLAINTS}"
    [saObjectValue]="id"
></supr-dropdown>
*/

@Component({
    selector: "supr-dropdown",
    template: `
        <div
            class="suprRow  spaceBetween dropdownInput"
            *ngIf="selectedOption && selectedOption.value"
            (click)="setModal()"
            saImpression
            [ngClass]="classSelector"
            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                .SUPR_DROPDOWN_OPTION}"
        >
            <supr-text>
                {{ selectedOption?.label }}
            </supr-text>

            <supr-icon
                class="secondary"
                name="chevron_down"
                *ngIf="!disabled"
            ></supr-icon>
        </div>
        <supr-modal
            *ngIf="isModal"
            modalName="${MODAL_NAMES.SUPR_DROPDOWN_MODAL}"
            modalType="${MODAL_TYPES.BOTTOM_SHEET}"
            [saObjectValue]="saObjectValue"
            [saObjectName]="saObjectName"
            [saContext]="saContext"
            (handleClose)="dropdownSelection()"
        >
            <div class="dropdownModal">
                <supr-text type="regular14" class="title">{{
                    title
                }}</supr-text>

                <div
                    class="option"
                    *ngIf="default"
                    saImpression
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .SUPR_DROPDOWN_OPTION}"
                    saObjectValue="default"
                    (click)="deselectDropdown()"
                >
                    <supr-text class="primary" type="regular14">
                        {{ default }}
                    </supr-text>
                </div>

                <div
                    *ngFor="let option of dropDownOptions; last as isLast"
                    class="option"
                    [class.disabled]="option.disabled"
                    [class.selected]="option.selected"
                    [class.last]="last"
                    saImpression
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .SUPR_DROPDOWN_OPTION}"
                    [saObjectValue]="option.value"
                    [saContext]="option.label"
                    (click)="dropdownSelection(option)"
                >
                    <supr-text
                        class="primary"
                        type="regular14"
                        [class.disabled]="option.disabled"
                    >
                        {{ option.label }}
                    </supr-text>
                </div>
            </div>
        </supr-modal>
        <supr-form-error [errMsg]="error"></supr-form-error>
    `,
    styleUrls: ["./supr-dropdown.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprDropdownComponent implements OnInit {
    @Input() dropDownOptions: DropdownOptions[];
    @Input() classSelector: string;
    @Input() error: string;
    @Input() title: string;
    @Input() isModal: boolean = false;
    @Input() disabled: boolean;
    @Input() default: string;

    selectedOption: DropdownOptions;

    @Output() select: EventEmitter<string> = new EventEmitter();
    @Output() deSelect: EventEmitter<string> = new EventEmitter();

    ngOnInit() {
        this.selectedOption = this.dropDownOptions.find(
            (issueType) => issueType.selected
        );
        if (this.selectedOption) {
            this.isModal = false;
        }
    }

    dropdownSelection(option?: any) {
        if (!option) {
            this.select.emit();
            this.isModal = false;
        } else if (option && !option.disabled) {
            this.select.emit(option.value);
            this.isModal = false;
        }
    }

    setModal() {
        if (!this.disabled) this.isModal = true;
    }

    deselectDropdown() {
        if (this.default && this.deSelect) {
            this.deSelect.emit();
        }
    }
}
