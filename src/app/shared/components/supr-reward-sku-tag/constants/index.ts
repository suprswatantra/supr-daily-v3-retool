export const TEXTS = {
    FREE: "Free",
};

export const STYLE_LIST = [
    {
        attributeName: "tagBgColor",
        attributeStyleVariableName: "--sku-reward-tag-bg-color",
    },
    {
        attributeName: "tagTextColor",
        attributeStyleVariableName: "--sku-reward-tag-text-color",
    },
    {
        attributeName: "rewardIconColor",
        attributeStyleVariableName: "--sku-reward-icon-color",
    },
    {
        attributeName: "infoIconColor",
        attributeStyleVariableName: "--sku-reward-info-icon-color",
    },
];
