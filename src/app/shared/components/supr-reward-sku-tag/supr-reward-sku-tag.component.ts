import {
    Component,
    ChangeDetectionStrategy,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
} from "@angular/core";

import { MODAL_NAMES, SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { SettingsService } from "@services/shared/settings.service";
import { UtilService } from "@services/util/util.service";
import { SkuRewardTagType, SkuRewardInfoInterface } from "@shared/models";

import { STYLE_LIST, TEXTS } from "./constants";

@Component({
    selector: "supr-reward-sku-tag",
    template: `
        <div class="suprRow" #skuRewardTag>
            <supr-tag class="freeItemTag" *ngIf="!noTag">
                <div class="suprRow">
                    <supr-icon
                        [name]="
                            isSuprPassActive ? 'logo_without_text' : 'trophy'
                        "
                        [class.suprPassIcon]="isSuprPassActive"
                    ></supr-icon>
                    <div class="spacer8"></div>
                    <supr-text type="subtext10">
                        {{ rewardText }}
                    </supr-text>
                </div>
            </supr-tag>
            <ng-container *ngIf="showInfoIcon && skuRewardTypeDescription">
                <div class="spacer4" *ngIf="!noTag"></div>
                <supr-icon
                    name="error"
                    (click)="toggleRewardDescriptionModal(true)"
                ></supr-icon>
            </ng-container>
        </div>
        <ng-container *ngIf="showRewardDescriptionModal">
            <supr-modal
                modalName="${MODAL_NAMES.REWARD_SKU_INFO_MODAL}"
                (handleClose)="toggleRewardDescriptionModal(false)"
            >
                <div class="wrapper">
                    <ng-container
                        *ngFor="
                            let lineItem of skuRewardTypeDescription;
                            trackBy: trackByFn
                        "
                    >
                        <supr-text type="body">- {{ lineItem }}</supr-text>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["./supr-reward-sku-tag.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardSkuTagComponent implements OnInit {
    @Input() skuRewardType: SkuRewardTagType;
    @Input() showInfoIcon: boolean;
    @Input() noTag: boolean;
    @Input() useCommonStyles = true;
    @Input() isSuprPassActive: boolean;

    @ViewChild("skuRewardTag", { static: true }) wrapperEl: ElementRef;

    rewardText: string;
    skuRewardTypeDescription: Array<string>;
    showRewardDescriptionModal: boolean;

    constructor(
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.skuRewardTypeDescription = this.getSkuRewardTypeDescriptionInfo();
        if (!this.noTag) {
            this.setRewardText();
        }
        if (this.useCommonStyles) {
            this.setStyles();
        }
    }

    toggleRewardDescriptionModal(value: boolean) {
        this.showRewardDescriptionModal = value;
    }

    trackByFn(_: any, index: number): number {
        return index;
    }

    private setRewardText() {
        if (!this.skuRewardType) {
            this.rewardText = TEXTS.FREE;
            return;
        }

        const currentRewardInfo = this.getRewardSkuInfo();

        if (this.isSuprPassActive) {
            this.rewardText =
                (currentRewardInfo && currentRewardInfo.suprPassTagName) ||
                TEXTS.FREE;
            return;
        }

        this.rewardText =
            (currentRewardInfo && currentRewardInfo.tagName) || TEXTS.FREE;
    }

    private getSkuRewardTypeDescriptionInfo() {
        if (!this.skuRewardType) {
            return null;
        }
        const currentRewardInfo = this.getRewardSkuInfo();
        return (currentRewardInfo && currentRewardInfo.offerDescription) || [];
    }

    private setStyles() {
        if (!this.skuRewardType) {
            return null;
        }
        const currentRewardInfo = this.getRewardSkuInfo();

        if (!currentRewardInfo || !currentRewardInfo.style) {
            return;
        }

        this.utilService.setStyles(
            currentRewardInfo.style,
            STYLE_LIST,
            this.wrapperEl
        );
    }

    private getRewardSkuInfo(): SkuRewardInfoInterface {
        const rewardSkuInfo = this.settingsService.getSettingsValue(
            SETTINGS.REWARD_SKU_INFO,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.REWARD_SKU_INFO]
        );

        return this.utilService.getNestedValue(
            rewardSkuInfo,
            this.skuRewardType,
            null
        );
    }
}
