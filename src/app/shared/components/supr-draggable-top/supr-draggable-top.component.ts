import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-draggable-top",
    template: `
        <div class="suprRow header center">
            <span class="headerPin"></span>
        </div>
    `,
    styleUrls: ["./supr-draggable-top.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraggableTopComponent {}
