import { FrequencySelectionComponent } from "./components/frequency.component";
import { ProductTileComponent } from "./components/product-tile.component";
import { EditSubscriptionComponent } from "./supr-edit-subscription.component";
import { EditSubscriptionWrapperComponent } from "./components/wrapper.component";

export const suprEditSubscriptionComponents = [
    FrequencySelectionComponent,
    ProductTileComponent,
    EditSubscriptionWrapperComponent,
    EditSubscriptionComponent,
];
