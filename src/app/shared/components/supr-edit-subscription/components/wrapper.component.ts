import {
    Component,
    OnInit,
    OnChanges,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { RATING_NUDGE_TRIGGERS, TOAST_MESSAGES } from "@constants";

import { Sku, Subscription } from "@models";

import { UtilService } from "@services/util/util.service";
import { ModalService } from "@services/layout/modal.service";
import { ToastService } from "@services/layout/toast.service";
import { MoengageService } from "@services/integration/moengage.service";
import { NudgeService } from "@services/layout/nudge.service";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { Frequency, SuprApi } from "@types";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-edit-subscription-wrapper",
    template: `
        <div class="wrapper">
            <ng-container *ngIf="_showFrequency; else review">
                <supr-edit-subscription-frequency
                    [frequency]="_frequency"
                    (handleConfirmFrequency)="updateFrequency($event)"
                    (handleClose)="toggleFrequency()"
                ></supr-edit-subscription-frequency>
            </ng-container>

            <ng-template #review>
                <supr-edit-subscription-product-tile
                    [quantity]="_quantity"
                    [sku]="sku"
                    (handleQtyUpdate)="updateQty($event)"
                ></supr-edit-subscription-product-tile>
                <div class="divider20"></div>
                <div class="frequencyWrapper">
                    <supr-text type="body">Delivery frequency</supr-text>
                    <div class="suprRow" (click)="toggleFrequency()">
                        <supr-week-days-small
                            [selectedDays]="_frequency"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .FREQUENCY_PAGE}"
                        ></supr-week-days-small>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
                <supr-button
                    [loading]="updating"
                    (handleClick)="updateSubscription()"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.DONE_BUTTON}"
                >
                    <supr-text type="body">Done</supr-text>
                </supr-button>
            </ng-template>
        </div>
    `,
    styleUrls: ["../supr-edit-subscription.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditSubscriptionWrapperComponent implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() subscription: Subscription;
    @Input() subscriptionState: SubscriptionCurrentState;
    @Input() subscriptionError: any;

    @Output() handleUpdateSubscription: EventEmitter<{
        subscriptionId: number;
        subscriptionData: SuprApi.SubscriptionBody;
    }> = new EventEmitter();

    _quantity: number;
    _frequency: Frequency;
    _showFrequency = false;
    updating = false;

    constructor(
        private utilService: UtilService,
        private modalService: ModalService,
        private toastService: ToastService,
        private moengageService: MoengageService,
        private nudgeService: NudgeService
    ) {}

    ngOnInit() {
        if (this.subscription) {
            this.setQtyAndFrequency();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSubscriptonDataChange(changes["subscription"]);
        this.handleSubscriptionStateChange(changes["subscriptionState"]);
    }

    updateQty(qty: number) {
        this._quantity = qty;
    }

    updateFrequency(frequency: Frequency) {
        this._frequency = frequency;

        this.toggleFrequency();
    }

    toggleFrequency() {
        this._showFrequency = !this._showFrequency;
    }

    updateSubscription() {
        this.updating = true;
        this.handleUpdateSubscription.emit({
            subscriptionId: this.subscription.id,
            subscriptionData: {
                quantity: this._quantity,
                frequency: this._frequency,
            },
        });
    }

    private showSuccessToast() {
        this.toastService.present(TOAST_MESSAGES.SUBSCRIPTIONS.UPDATED);
    }

    private closeModal() {
        this.modalService.closeModal();
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackSubscriptionUpdate(this.subscription);
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.UPDATE_SUBSCRIPTION
        );
    }

    private shouldIgnoreChange(change: SimpleChange) {
        return !change || change.firstChange;
    }

    private setQtyAndFrequency() {
        this._quantity = this.utilService.toValue(this.subscription.quantity);
        this._frequency = this.subscription.frequency;
    }

    private handleSubscriptionStateChange(change: SimpleChange) {
        if (this.shouldIgnoreChange(change)) {
            return;
        }

        if (
            change.previousValue ===
                SubscriptionCurrentState.UPDATING_SUBSCRIPTION &&
            change.currentValue === SubscriptionCurrentState.NO_ACTION
        ) {
            if (!this.subscriptionError) {
                this.showSuccessToast();
                this.closeModal();
                this.sendAnalyticsEvents();
                this.sendRatingNudge();
            }
        }
    }

    private handleSubscriptonDataChange(change: SimpleChange) {
        if (this.shouldIgnoreChange(change)) {
            return;
        }
        if (
            change.currentValue &&
            change.currentValue !== change.previousValue
        ) {
            this.setQtyAndFrequency();
        }
    }
}
