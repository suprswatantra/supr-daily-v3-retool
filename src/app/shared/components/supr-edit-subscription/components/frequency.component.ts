import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
} from "@angular/core";
import { Frequency } from "@types";
import {
    SA_OBJECT_NAMES,
    SA_CONTEXT,
} from "../constants/analytics.constants";

@Component({
    selector: "supr-edit-subscription-frequency",
    template: `
        <div
            class="frequency"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.EDIT_FREQUENCY}"
            saContext="${SA_CONTEXT.FREQUENCY_SCREEN}"
        >
            <div class="suprRow">
                <supr-icon
                    name="chevron_left"
                    (click)="handleClose.emit()"
                ></supr-icon>
                <div class="spacer8"></div>
                <supr-text type="subtitle">
                    Select delivery frequency
                </supr-text>
            </div>
            <div class="divider24"></div>

            <supr-frequency-select
                [frequency]="_frequency"
                (handleFrequencyChange)="changeFrequency($event)"
                saContext="${SA_CONTEXT.FREQUENCY_SCREEN}"
            ></supr-frequency-select>

            <div class="divider20"></div>
            <supr-button
                [disabled]="disableDone"
                (handleClick)="handleConfirmFrequency.emit(_frequency)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.DONE_BUTTON}"
                saContext="${SA_CONTEXT.FREQUENCY_SCREEN}"
            >
                <supr-text type="body">Done</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../supr-edit-subscription.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FrequencySelectionComponent {
    disableDone = false;

    @Input()
    set frequency(value: Frequency) {
        this._frequency = value;
    }

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output()
    handleConfirmFrequency: EventEmitter<Frequency> = new EventEmitter();

    _frequency: Frequency;

    changeFrequency(newFrequency: Frequency) {
        const days = [];
        for (const day in newFrequency) {
            if (newFrequency[day]) {
                days.push(day);
            }
        }

        this.disableDone = days.length ? false : true;

        this._frequency = { ...newFrequency };
    }
}
