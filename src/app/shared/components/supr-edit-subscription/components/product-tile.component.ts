import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    OnChanges,
    Input,
    Output,
    EventEmitter,
    SimpleChanges,
} from "@angular/core";

import { Sku } from "@models";
import { QuantityService } from "@services/util/quantity.service";

import { QTY_PER_DELIVERY } from "../constants";

@Component({
    selector: "supr-edit-subscription-product-tile",
    template: `
        <div class="suprRow tile">
            <div class="suprColumn stretch top tileImage">
                <supr-image
                    [src]="sku?.image?.fullUrl"
                    [image]="sku?.image"
                    [lazyLoad]="false"
                ></supr-image>
                <div class="spacer8"></div>
            </div>

            <div class="suprColumn stretch top left tileName">
                <supr-text class="title" type="subtitle">{{
                    sku?.sku_name
                }}</supr-text>
                <div class="divider8"></div>
                <div class="suprRow tileQty">
                    <supr-text type="paragraph">{{ quantityStr }}</supr-text>
                    <div class="suprColumn counter right">
                        <supr-number-counter
                            [quantity]="quantity"
                            [minusDisabled]="minusDisabled"
                            (handleClick)="qtyUpdate($event)"
                        ></supr-number-counter>
                        <div class="divider4"></div>
                        <supr-text type="caption">
                            {{ QTY_PER_DELIVERY }}
                        </supr-text>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../supr-edit-subscription.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTileComponent implements OnInit, OnChanges {
    @Input() quantity: number;
    @Input() sku: Sku;
    @Output() handleQtyUpdate: EventEmitter<number> = new EventEmitter();

    quantityStr: string;
    minusDisabled = false;

    QTY_PER_DELIVERY = QTY_PER_DELIVERY;

    constructor(private quantityService: QuantityService) {}

    ngOnInit() {
        this.setQty();
        this.minusDisabled = this.quantity === 1;
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["quantity"];
        if (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        ) {
            this.setQty();
        }
    }

    qtyUpdate(direction: number) {
        if (direction > 0) {
            this.minusDisabled = false;
            this.handleQtyUpdate.emit(this.quantity + 1);
        } else if (direction < 0 && this.quantity > 0) {
            this.minusDisabled = this.quantity === 2;
            this.handleQtyUpdate.emit(this.quantity - 1);
        }
    }

    private setQty() {
        this.quantityStr = this.quantityService.toText(this.sku, this.quantity);
    }
}
