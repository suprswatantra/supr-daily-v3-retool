import {
    Input,
    Component,
    OnInit,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SubscriptionAdapter as Adapter } from "@shared/adapters/subscription.adapter";

import { Sku, Subscription } from "@models";
import { SubscriptionCurrentState } from "@store/subscription/subscription.state";
import { SuprApi } from "@types";

@Component({
    selector: "supr-edit-subscription",
    template: `
        <supr-edit-subscription-wrapper
            [sku]="sku"
            [subscription]="subscription"
            [subscriptionState]="subscriptionState$ | async"
            [subscriptionError]="subscriptionError$ | async"
            (handleUpdateSubscription)="updateSubscription($event)"
        ></supr-edit-subscription-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditSubscriptionComponent implements OnInit {
    @Input() sku: Sku;
    @Input() subscription: Subscription;

    subscriptionState$: Observable<SubscriptionCurrentState>;
    subscriptionError$: Observable<any>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.subscriptionState$ = this.adapter.subscriptionState$;
        this.subscriptionError$ = this.adapter.subscriptionError$;
    }

    updateSubscription(data: {
        subscriptionId: number;
        subscriptionData: SuprApi.SubscriptionBody;
    }) {
        this.adapter.updateSubscription(
            data.subscriptionId,
            data.subscriptionData
        );
    }
}
