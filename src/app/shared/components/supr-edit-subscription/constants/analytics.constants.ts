export const SA_OBJECT_NAMES = {
    CLICK: {
        FREQUENCY_PAGE: "frequency-page",
        DONE_BUTTON: "done-button",
    },
    IMPRESSION: {
        EDIT_FREQUENCY: "edit-frequency",
    },
};

export const SA_CONTEXT = {
    FREQUENCY_SCREEN: "frequency-screen",
};
