import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";

@Component({
    selector: "supr-generic-nudge",
    template: `
        <supr-generic-nudge-wrapper
            [showModal]="showModal$ | async"
            (handleHideModal)="hideRetroModal()"
        ></supr-generic-nudge-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericNudgeComponent implements OnInit {
    showModal$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.showModal$ = this.adapter.showGenericNudge$;
    }

    hideRetroModal() {
        this.adapter.hideGenericNudge();
    }
}
