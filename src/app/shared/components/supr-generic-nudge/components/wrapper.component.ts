import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import { MODAL_NAMES, NUDGE_SETTINGS_KEYS, NUDGE_TYPES } from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

import { GenericNudgeSetting } from "@types";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-generic-nudge-wrapper",
    template: `
        <ng-container *ngIf="showModal && genericNudge">
            <supr-modal (handleClose)="hideModal()" [modalName]="modalName">
                <div class="wrapper">
                    <ng-container *ngIf="genericNudge?.title">
                        <supr-text type="subtitle">
                            {{ genericNudge.title }}
                        </supr-text>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.imgCarousal">
                        <div class="divider16"></div>
                        <supr-generic-nudge-image-carousal
                            [imageUrls]="genericNudge?.imgCarousal"
                        ></supr-generic-nudge-image-carousal>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.upperTextFragment">
                        <div class="divider20"></div>
                        <supr-text-fragment
                            [textFragment]="genericNudge?.upperTextFragment"
                        ></supr-text-fragment>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.banner">
                        <div class="divider16"></div>
                        <supr-banner
                            [banner]="genericNudge?.banner"
                        ></supr-banner>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.lowerTextFragment">
                        <div class="divider16"></div>
                        <supr-text-fragment
                            [textFragment]="genericNudge?.lowerTextFragment"
                        ></supr-text-fragment>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.linkCta">
                        <div class="divider16"></div>
                        <div class="linkCta">
                            <supr-text-fragment
                                (click)="goToLink()"
                                [textFragment]="genericNudge?.linkCta"
                            ></supr-text-fragment>
                        </div>
                    </ng-container>

                    <ng-container *ngIf="genericNudge?.actionButtons">
                        <div class="divider24"></div>
                        <div class="suprRow">
                            <ng-container
                                *ngFor="
                                    let actionButton of genericNudge?.actionButtons;
                                    last as isLast
                                "
                            >
                                <div
                                    class="buttonWrapper"
                                    [class.singleButton]="hasSingleActionButton"
                                >
                                    <supr-generic-nudge-action-button
                                        [actionButton]="actionButton"
                                        [nudgeType]="genericNudge?.type"
                                        (handleActionButtonClick)="
                                            btnClick($event)
                                        "
                                    ></supr-generic-nudge-action-button>
                                </div>
                                <div class="spacer8" *ngIf="!isLast"></div>
                            </ng-container>
                        </div>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-generic-nudge.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() showModal: boolean;
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    genericNudge: GenericNudgeSetting.Nudge;
    modalName: string;
    hasSingleActionButton: boolean;

    constructor(
        private modalService: ModalService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private utilService: UtilService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleShowModalChange(changes["showModal"]);
    }

    hideModal() {
        this.handleHideModal.emit();
    }

    btnClick(actionButton: GenericNudgeSetting.NudgeActionButton) {
        this.closeModal();

        if (actionButton.action === "close") {
            return;
        }

        if (actionButton.route) {
            this.delayOpen(() =>
                this.routerService.goToUrl(actionButton.route)
            );
            return;
        }
    }

    goToLink() {
        const link = this.utilService.getNestedValue(
            this.genericNudge,
            "linkCta",
            null
        );

        if (!link) {
            return;
        }

        this.closeModal();

        if (link.outsideUrl) {
            this.delayOpen(() =>
                window.open(link.outsideUrl, "_system", "location=yes")
            );
            return;
        }

        if (link.appUrl) {
            this.delayOpen(() => this.routerService.goToUrl(link.appUrl));
        }
    }

    private delayOpen(fn: () => void) {
        setTimeout(fn, 100);
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        this.getGenericNudgeData();
        this.setModalName();

        const buttons = this.utilService.getNestedValue(
            this.genericNudge,
            "actionButtons"
        );

        if (buttons && buttons.length > 1) {
            this.hasSingleActionButton = false;
            return;
        }

        this.hasSingleActionButton = true;
    }

    private getGenericNudgeData() {
        const key = NUDGE_SETTINGS_KEYS[NUDGE_TYPES.GENERIC];
        this.genericNudge = this.settingsService.getSettingsValue(key, {});
    }

    private setModalName() {
        this.modalName = `${MODAL_NAMES.GENERIC_NUDGE}-${this.genericNudge.type}`;
    }

    private closeModal() {
        this.modalService.closeModal();
    }
}
