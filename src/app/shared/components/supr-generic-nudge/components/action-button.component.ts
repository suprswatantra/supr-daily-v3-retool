import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
} from "@angular/core";
import { STYLE_LIST } from "../constants";

import { GenericNudgeSetting } from "@types";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-generic-nudge-action-button",
    template: `
        <div #buttonWrapper class="wrapper">
            <supr-button
                (handleClick)="handleActionButtonClick.emit(actionButton)"
                [saObjectName]="buttonSaObjectName"
                [saContext]="buttonSaContext"
            >
                <supr-text type="body">{{ actionButton.text }}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../styles/action-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericNudgeActionButtonComponent implements OnInit {
    @Input() actionButton: GenericNudgeSetting.NudgeActionButton;
    @Input()
    set nudgeType(value: string) {
        this.setButtonSaObjectName(value);
    }

    @Output() handleActionButtonClick: EventEmitter<
        GenericNudgeSetting.NudgeActionButton
    > = new EventEmitter();

    @ViewChild("buttonWrapper", { static: true }) wrapperEl: ElementRef;

    buttonSaObjectName: string;
    buttonSaContext: string;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setStyles();
    }

    private setStyles() {
        if (!this.actionButton) {
            return;
        }

        this.utilService.setStyles(
            this.actionButton,
            STYLE_LIST,
            this.wrapperEl
        );
    }

    private setButtonSaObjectName(nudgeType: string) {
        const action = this.utilService.getNestedValue(
            this.actionButton,
            "action",
            null
        );

        const route = this.utilService.getNestedValue(
            this.actionButton,
            "route",
            null
        );

        if (!action) {
            return;
        }

        this.buttonSaObjectName = `${nudgeType}-${action}`;

        if (route) {
            this.buttonSaContext = route;
            return;
        }
    }
}
