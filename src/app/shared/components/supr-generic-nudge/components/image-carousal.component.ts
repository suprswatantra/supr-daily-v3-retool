import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { CLODUINARY_IMAGE_SIZE } from "@constants";

@Component({
    selector: "supr-generic-nudge-image-carousal",
    template: `
        <div class="wrapper" *ngIf="imageUrls && imageUrls.length">
            <ion-slides [options]="slideOpts">
                <ng-container *ngFor="let imageUrl of imageUrls">
                    <ion-slide>
                        <supr-image
                            [src]="imageUrl"
                            [imgWidth]="
                                ${CLODUINARY_IMAGE_SIZE.NUDGE_FULL_WIDTH_IMAGE
                                    .WIDTH}
                            "
                        ></supr-image>
                    </ion-slide>
                </ng-container>
            </ion-slides>
        </div>
    `,
    styleUrls: ["../styles/image-carousal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericNudgeImageCarousalComponent {
    @Input() imageUrls: string[];

    slideOpts = {
        initialSlide: 0,
        speed: 600,
        autoplay: true,
    };
}
