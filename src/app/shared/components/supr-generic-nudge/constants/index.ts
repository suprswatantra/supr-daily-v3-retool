export const STYLE_LIST = [
    {
        attributeName: "bgColor",
        attributeStyleVariableName: "--supr-nudge-action-button-bg-color",
    },
    {
        attributeName: "borderColor",
        attributeStyleVariableName: "--supr-nudge-action-button-border-color",
    },
    {
        attributeName: "textColor",
        attributeStyleVariableName: "--supr-nudge-action-button-text-color",
    },
];
