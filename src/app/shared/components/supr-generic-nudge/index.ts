import { GenericNudgeComponent } from "./supr-generic-nudge.component";
import { WrapperComponent } from "./components/wrapper.component";
import { GenericNudgeActionButtonComponent } from "./components/action-button.component";
import { GenericNudgeImageCarousalComponent } from "./components/image-carousal.component";

export const genericNudgeComponents = [
    GenericNudgeComponent,
    WrapperComponent,
    GenericNudgeActionButtonComponent,
    GenericNudgeImageCarousalComponent,
];
