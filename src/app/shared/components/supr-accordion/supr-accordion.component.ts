import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

/*
    <supr-accordion
        title="Item Related Issues"
        subTitle="Select item with issue"
        icon="bag_cart"
    >
        <div>content</div>
    </supr-accordion>
*/

@Component({
    selector: "supr-accordion",
    template: `
        <div class="accordion">
            <div
                class="suprRow header"
                (click)="toggleAccordion()"
                [class.disabled]="disabled"
            >
                <div class="suprColumn left icon stretch">
                    <supr-icon [name]="icon" class="primary"></supr-icon>
                </div>
                <div class="suprColumn left block">
                    <supr-text type="subtitle">{{ title }}</supr-text>
                    <supr-text type="caption">{{ subTitle }}</supr-text>
                </div>

                <div *ngIf="accordionToggle" class="suprColumn icon">
                    <supr-icon
                        name="chevron_up"
                        class="primary"
                        *ngIf="!_isExpand"
                    ></supr-icon>
                    <supr-icon
                        name="chevron_down"
                        class="primary"
                        *ngIf="_isExpand"
                    ></supr-icon>
                </div>
            </div>

            <div class="content" *ngIf="_isExpand">
                <ng-content></ng-content>
            </div>
        </div>
    `,
    styleUrls: ["./supr-accordion.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprAccordion implements OnInit {
    @Input() accordionToggle: boolean = true;
    @Input() title: string;
    @Input() subTitle: string;
    @Input() icon: string;
    @Input() disabled: boolean = false;

    @Input()
    set expand(value: boolean) {
        this._isExpand = value;
    }

    _isExpand: boolean = true;

    ngOnInit() {
        this.toggleAccordion(true);
    }

    toggleAccordion(init?: boolean) {
        if (this.disabled) {
            return;
        }
        if (init) {
            if (!this.accordionToggle) this._isExpand = true;
            return;
        }

        if (this.accordionToggle) {
            this._isExpand = !this._isExpand;
        }
    }
}
