export const SA_OBJECT_NAMES = {
    CLICK: {
        WHATSAPP_SETTING_TOGGLE: "whatsapp-setting-toggle",
        WHATSAPP_SETTING_UPDATE: "whatsapp-setting-update",
    },
};
