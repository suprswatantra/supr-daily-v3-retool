export const TEXTS = {
    TITLE: "WhatsApp notifications",
    SUBTITLE: "Get OTP and order updates through WhatsApp?",
    UPDATE: "Update",
    YES: "YES",
    NO: "NO",
};
