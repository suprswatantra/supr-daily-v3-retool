import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

import { TEXTS } from "./constants";
import { SA_OBJECT_NAMES } from "./constants/analytics.component";

@Component({
    selector: "supr-whatsapp",
    template: `
        <div class="wrapper">
            <supr-text type="subtitle">${TEXTS.TITLE}</supr-text>
            <div class="divider16"></div>

            <div class="suprRow spaceBetween">
                <supr-text type="body" class="description">
                    ${TEXTS.SUBTITLE}
                </supr-text>

                <div
                    class="suprRow wrapperToggle"
                    (click)="toggleWhatsAppOptIn()"
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.CLICK
                        .WHATSAPP_SETTING_TOGGLE}"
                    [saObjectValue]="!isWhatsAppOptIn"
                >
                    <div
                        class="suprColumn center wrapperToggleBtn"
                        [class.active]="isWhatsAppOptIn"
                    >
                        <supr-text type="paragraph">${TEXTS.YES}</supr-text>
                    </div>

                    <div
                        class="suprColumn center wrapperToggleBtn wrapperToggleBtnRight"
                        [class.active]="!isWhatsAppOptIn"
                    >
                        <supr-text type="paragraph">${TEXTS.NO}</supr-text>
                    </div>
                </div>
            </div>
            <div class="divider24"></div>

            <supr-button
                [loading]="isLoading"
                [disabled]="whatsAppOptIn === isWhatsAppOptIn"
                (handleClick)="updateWhatsAppStatus()"
                saObjectName="${SA_OBJECT_NAMES.CLICK.WHATSAPP_SETTING_UPDATE}"
                [saObjectValue]="isWhatsAppOptIn"
            >
                <supr-text type="body">${TEXTS.UPDATE}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./whatsapp.component.scss"],
})
export class WhatsAppComponent implements OnInit {
    @Input() whatsAppOptIn: boolean;
    @Input() isLoading: boolean;

    @Output() handleUpdateWhatsApp: EventEmitter<boolean> = new EventEmitter();

    isWhatsAppOptIn = false;

    ngOnInit() {
        this.isWhatsAppOptIn = this.whatsAppOptIn;
    }

    toggleWhatsAppOptIn() {
        this.isWhatsAppOptIn = !this.isWhatsAppOptIn;
    }

    updateWhatsAppStatus() {
        this.showLoader();
        this.handleUpdateWhatsApp.emit(this.isWhatsAppOptIn);
    }

    private showLoader() {
        this.isLoading = true;
    }
}
