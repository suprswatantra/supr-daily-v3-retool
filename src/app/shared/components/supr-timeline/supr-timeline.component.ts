import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Segment, SuprTimelineProps } from "@types";

@Component({
    selector: "supr-timeline",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./supr-timeline.component.html",
    styleUrls: ["./supr-timeline.component.scss"],
})
export class SuprTimeline {
    @Input() collapsed: boolean;
    @Input() headerIcon: string;
    @Input() header: SuprTimelineProps.Header;
    @Input() timelineItems: SuprTimelineProps.LineItem[];

    @Input() isHeader: boolean = true;
    @Input() headerObjectName: string;
    @Input() headerObjectValue: any;
    @Input() headerContext: any;
    @Input() headerContextList: Segment.ContextListItem[] = [];

    @Input() imageContext: any;
    @Input() imageObjectName: string;
    @Input() imageObjectValue: any;

    @Output() handleImageClick: EventEmitter<void> = new EventEmitter();

    toggleTimeline() {
        this.collapsed = !this.collapsed;
    }
}
