import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";
import {
    IMAGE_UPLOAD,
    ANALYTICS_OBJECT_NAMES,
    CLODUINARY_IMAGE_SIZE,
} from "@constants";

/*
    <supr-image-upload
        [loading]="imageLoader"
        [imageArray]="uploadedImages"
        (fileChange)="fileUpload($event)"
        (deleteImage)="deleteImage($event)"
    ></supr-image-upload>
*/

@Component({
    selector: "supr-image-upload",
    template: `
        <div>
            <input
                style="display: none"
                type="file"
                (change)="onFileChanged($event)"
                [attr.disabled]="_disabled"
                #fileInput
            />
            <div class="suprRow wrap left">
                <div
                    class="uploadImage"
                    [class.loading]="_loading"
                    (click)="fileInput.click()"
                    *ngIf="!disabled"
                    saImpression
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .SUPR_IMAGE_UPLOAD}"
                >
                    <supr-image
                        [src]="defaultImgUrl"
                        [imgWidth]="${CLODUINARY_IMAGE_SIZE.ICON.WIDTH}"
                        [imgHeight]="${CLODUINARY_IMAGE_SIZE.ICON.HEIGHT}"
                        [lazyLoad]="false"
                        [withWrapper]="false"
                    ></supr-image>
                </div>

                <div class="uploadImage" *ngFor="let image of imageArray">
                    <img
                        [src]="image"
                        saImpression
                        saClick
                        saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                            .SUPR_IMAGE_UPLOADED}"
                        [saObjectValue]="image"
                    />
                    <supr-svg
                        (click)="deleteHandler(image)"
                        *ngIf="!disabled"
                    ></supr-svg>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-image-upload.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageUploadComponent {
    @Input() set loading(loading: boolean) {
        this._disabled = loading ? true : null;
        this._loading = loading;
    }

    @Input() imageArray: string[] = [];
    @Input() disabled: boolean = false;

    @Output() fileChange: EventEmitter<File> = new EventEmitter();
    @Output() deleteImage: EventEmitter<string> = new EventEmitter();

    defaultImgUrl = IMAGE_UPLOAD.imageUrl;

    _loading: boolean = false;
    _disabled = null;

    onFileChanged(event) {
        const selectedFile = event.target.files[0];
        if (selectedFile) {
            this.fileChange.emit(selectedFile);
        }
        event.target.value = "";
    }

    deleteHandler(image) {
        this.deleteImage.emit(image);
    }
}
