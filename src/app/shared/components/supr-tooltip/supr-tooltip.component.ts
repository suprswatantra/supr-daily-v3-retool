import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-tooltip",
    template: `
        <div class="wrapper">
            <div class="suprColumn center content">
                <ng-content></ng-content>
            </div>
            <div class="suprRow center tip"></div>
        </div>
    `,
    styleUrls: ["./supr-tooltip.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TooltipComponent {}
