import {
    AfterViewInit,
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { MODAL_NAMES, MODAL_TYPES, AUTODEBIT_TOKEN_STATUS } from "@constants";

import { SuprApi } from "@types";

import { TEXTS } from "./constants";

@Component({
    selector: "supr-pass-auto-debit-payment-success",
    template: `
        <supr-modal
            [backdropDismiss]="false"
            [fullScreen]="true"
            [modalType]="modalType"
            (handleClose)="handleClose.emit()"
            modalName="${MODAL_NAMES.AUTO_DEBIT_SUCCESS_MODAL}"
            [saObjectValue]="recurringPaymentRes?.auto_debit_status"
        >
            <div class="suprColumn center wrapper">
                <ng-container
                    *ngIf="
                        recurringPaymentRes?.auto_debit_status ===
                            autoDebitTokenStatus.CONFIRMED;
                        else retry
                    "
                >
                    <supr-text type="subtitle" class="recurringPaymentSuccess">
                        {{ texts.LIFETIME_MEMBER_SUCCESS_TEXT }}
                    </supr-text>
                    <div class="divider24"></div>
                    <supr-svg class="success"></supr-svg>
                </ng-container>
                <ng-template #retry>
                    <supr-text type="subtitle" class="recurringPaymentSuccess">
                        {{ texts.FAILURE_TEXT }}
                    </supr-text>
                    <div class="divider20"></div>
                    <supr-button
                        class="retryBtn"
                        (handleClick)="handleAutoDebitPaymentRetry.emit()"
                    >
                        {{ texts.RETRY }}
                    </supr-button>
                    <div class="divider20"></div>
                    <supr-text
                        type="paragraph"
                        class="skip"
                        (click)="handleSkip.emit()"
                    >
                        {{ texts.SKIP }}
                    </supr-text>
                </ng-template>
            </div>
        </supr-modal>
    `,
    styleUrls: ["./supr-pass-auto-debit-payment-success.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassAutoDebitPaymentSuccess implements AfterViewInit {
    @Input() showSuccessScreen: boolean;
    @Input() recurringPaymentRes: SuprApi.RecurringPaymentRes;
    @Input() autoDismissTimeout = 3000;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();
    @Output() handleSkip: EventEmitter<void> = new EventEmitter();
    @Output() handleAutoDebitPaymentRetry: EventEmitter<
        boolean
    > = new EventEmitter();
    @Output() autoDismissCallback: EventEmitter<void> = new EventEmitter();

    modalType = MODAL_TYPES.NO_WRAPPER;
    autoDebitTokenStatus = AUTODEBIT_TOKEN_STATUS;
    texts = TEXTS;

    ngAfterViewInit() {
        setTimeout(() => {
            if (
                this.recurringPaymentRes &&
                this.recurringPaymentRes.auto_debit_status ===
                    AUTODEBIT_TOKEN_STATUS.CONFIRMED
            ) {
                this.autoDismissCallback.emit();
            }
        }, this.autoDismissTimeout);
    }
}
