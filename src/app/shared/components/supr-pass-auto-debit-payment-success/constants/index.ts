export const TEXTS = {
    LIFETIME_MEMBER_SUCCESS_TEXT: "You’ve become a Lifetime Supr Access Member",
    FAILURE_TEXT: "Auto debit setup failed",
    RETRY: "Retry",
    SKIP: "Skip",
};
