import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { OUT_OF_STOCK_ENABLED, OUT_OF_STOCK_TEXT } from "@constants";

import { Segment } from "@types";
@Component({
    selector: "supr-oos-overlay",
    template: `
        <ng-container>
            <div
                class="suprOosOverlay"
                *ngIf="showOos"
                saImpression
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                [saContextList]="saContextList"
            >
                <supr-backdrop></supr-backdrop>
                <supr-text class="suprColumn text" type="paragraph">
                    ${OUT_OF_STOCK_TEXT}
                </supr-text>
            </div>
            <ng-content *ngIf="!showOos"></ng-content>
        </ng-container>
    `,
    styleUrls: ["./supr-oos-overlay.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOosOverlayComponent {
    @Input() outOfStock: boolean;
    @Input() saObjectName: string;
    @Input() saObjectValue: string;
    @Input() saContextList: Segment.ContextListItem[];

    get showOos() {
        return OUT_OF_STOCK_ENABLED && this.outOfStock;
    }
}
