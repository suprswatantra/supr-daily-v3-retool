import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-line-item-variant-A",
    templateUrl: "./supr-line-item-variant-A.component.html",
    styleUrls: ["./supr-line-item-variant-A.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprLineItemVariantAComponent {
    @Input() text: string;
    @Input() textType: string;
    @Input() imageLeft: string;
    @Input() imageRight: string;
    @Input() iconOnTheLeft: string;
    @Input() iconOnTheRight: string;
    @Input() imageLeftDirectUrl = false;
    @Input() imageRightDirectUrl = false;

    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
