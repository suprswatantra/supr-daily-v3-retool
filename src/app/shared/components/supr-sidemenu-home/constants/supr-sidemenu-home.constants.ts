export const TEXTS = {
    VACATION_TEXT: "Vacation",
    VACATION_ACTIVE_TEXT: "Active",
    ORDER_CALENDAR_TEXT: "Order calendar",
    MY_SUBSCRIPTIONS_TEXT: "My subscriptions",
    SUPR_WALLET_TEXT: "Supr Wallet",
    WALLET_TXN_TEXT: "Wallet Transactions",
    SUPPORT_TEXT: "Support & FAQs",
    SUPR_PASS_TEXT: "Supr Access",
    GET_SUPR_PASS: "Get now",
    REFERRAL: "Refer and save",
    REWARDS: "Rewards",
    ENTER_REFERRAL_CODE: "Enter code",
    LIFETIME_MEMBER: "Lifetime Member",
};
