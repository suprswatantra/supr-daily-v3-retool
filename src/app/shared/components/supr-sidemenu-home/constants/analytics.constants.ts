export const SA_OBJECT_NAMES = {
    CLICK: {
        PROFILE: "profile",
        SUBSCRIPTION: "subscriptions",
        CALENDAR: "calendar",
        VACATION: "vacation",
        WALLET: "wallet",
        WALLET_TXN: "wallet-transaction",
        SUPPORT: "support",
        SUPR_PASS: "get-supr-pass",
        REFERRAL: "referral",
        REWARDS: "rewards",
        CITY_SELECTION: "city-selection",
    },
};

export const SA_CONTEXT = {
    SIDEMENU_SCREEN: "side-menu-screen",
};
