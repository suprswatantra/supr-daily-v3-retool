import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { Subscription, ProfileLevelSuprPassDetails } from "@models";

import { SideMenuAdapter as Adpater } from "../../adapters/sidemenu.adapter";

@Component({
    selector: "supr-sidemenu-home",
    template: `
        <supr-sidemenu-home-content
            [name]="name$ | async"
            [walletBalance]="walletBalance$ | async"
            [isVacationActive]="isVacationActive$ | async"
            [subscriptionList]="subscriptionList$ | async"
            [suprCreditsEnabled]="suprCreditsEnabled$ | async"
            [isSuprPassEligible]="isSuprPassEligible$ | async"
            [activeSuprPassDetails]="activeSuprPassDetails$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
            [canUserApplyReferralCode]="canUserApplyReferralCode$ | async"
            [isReferralEligible]="isReferralEligible$ | async"
            [isExperimentsFetched]="isExperimentsFetched$ | async"
            [isRewardsEligible]="isRewardsEligible$ | async"
        ></supr-sidemenu-home-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidemenuHomeComponent implements OnInit {
    name$: Observable<string>;
    walletBalance$: Observable<number>;
    isVacationActive$: Observable<boolean>;
    subscriptionList$: Observable<Subscription[]>;
    suprCreditsEnabled$: Observable<boolean>;
    isSuprPassEligible$: Observable<boolean>;
    isReferralEligible$: Observable<boolean>;
    canUserApplyReferralCode$: Observable<boolean>;
    activeSuprPassDetails$: Observable<ProfileLevelSuprPassDetails>;
    isAnonymousUser$: Observable<boolean>;
    isExperimentsFetched$: Observable<boolean>;
    isRewardsEligible$: Observable<boolean>;

    appVersion: string;

    constructor(private adapter: Adpater) {}

    ngOnInit() {
        this.name$ = this.adapter.fullName$;
        this.walletBalance$ = this.adapter.walletBalance$;
        this.isVacationActive$ = this.adapter.isVacationActive$;
        this.subscriptionList$ = this.adapter.subscriptionList$;
        this.suprCreditsEnabled$ = this.adapter.suprCreditsEnabled$;
        this.isSuprPassEligible$ = this.adapter.isSuprPassEligible$;
        this.activeSuprPassDetails$ = this.adapter.activeSuprPassDetails$;
        this.isAnonymousUser$ = this.adapter.isAnonymousUser$;
        this.isReferralEligible$ = this.adapter.isReferralEligible$;
        this.canUserApplyReferralCode$ = this.adapter.canUserApplyReferralCode$;
        this.isExperimentsFetched$ = this.adapter.isExperimentsFetched$;
        this.isRewardsEligible$ = this.adapter.isRewardsEligible$;
    }
}
