import { ContentComponent } from "./components/content.component";
import { SidemenuHomeComponent } from "./supr-sidemenu-home.component";

export const sideMenuComponents = [ContentComponent, SidemenuHomeComponent];
