import {
    Component,
    ChangeDetectionStrategy,
    Input,
    SimpleChanges,
    OnChanges,
    SimpleChange,
    OnInit,
} from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { City, Subscription, ProfileLevelSuprPassDetails } from "@models";

import { RouterService } from "@services/util/router.service";
import { StoreService } from "@services/data/store.service";
import { PlatformService } from "@services/util/platform.service";
import { SubscriptionService } from "@services/shared/subscription.service";
import { UtilService } from "@services/util/util.service";
import { UserService } from "@services/shared/user.service";
import { AuthService } from "@services/data/auth.service";
import { SettingsService } from "@services/shared/settings.service";

import { TEXTS } from "@shared/components/supr-sidemenu-home/constants/supr-sidemenu-home.constants";
import { SA_OBJECT_NAMES, SA_CONTEXT } from "../constants/analytics.constants";

@Component({
    selector: "supr-sidemenu-home-content",
    templateUrl: "./content.component.html",
    styleUrls: ["../supr-sidemenu-home.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit, OnChanges {
    @Input() name: string;
    @Input() walletBalance: number;
    @Input() isVacationActive: boolean;
    @Input() subscriptionList: Subscription[] = [];
    @Input() suprCreditsEnabled: boolean;
    @Input() isSuprPassEligible: boolean;
    @Input() isRewardsEligible: boolean;
    @Input() activeSuprPassDetails: ProfileLevelSuprPassDetails;
    @Input() isAnonymousUser: boolean;
    @Input() isReferralEligible: boolean;
    @Input() canUserApplyReferralCode: boolean;
    @Input() isExperimentsFetched: boolean;

    appVersion: string;
    subscriptionCountStr: string;
    subscriptionCountClass: string;
    suprPassInfoText: string;
    referralInfoText: string;
    anonymousUserCityInfo: City;
    showSuprPassCard: boolean;

    vacationText = TEXTS.VACATION_TEXT;
    vacationActiveText = TEXTS.VACATION_ACTIVE_TEXT;
    orderCalendarText = TEXTS.ORDER_CALENDAR_TEXT;
    mySubscriptionsText = TEXTS.MY_SUBSCRIPTIONS_TEXT;
    suprWalletText = TEXTS.SUPR_WALLET_TEXT;
    supportText = TEXTS.SUPPORT_TEXT;
    walletTxnText = TEXTS.WALLET_TXN_TEXT;
    suprPassText = TEXTS.SUPR_PASS_TEXT;
    referralText = TEXTS.REFERRAL;
    rewardsText = TEXTS.REWARDS;
    SA_OBJECT_NAMES = SA_OBJECT_NAMES.CLICK;
    SA_CONTEXT = SA_CONTEXT;

    constructor(
        private routerService: RouterService,
        private storeService: StoreService,
        private platformService: PlatformService,
        private subscriptionService: SubscriptionService,
        private utilService: UtilService,
        private userService: UserService,
        private authService: AuthService,
        private settingsService: SettingsService
    ) {
        this.setAppVersion();
    }

    ngOnInit() {
        this.setShowSuprPassCard();
        this.handleSubscriptionCountInfo();
        this.handleSuprPassInfoText();
        this.handleReferralInfoText();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSubscriptionListChange(changes["subscriptionList"]);
        this.handleSuprPassInfoChange(changes["isSuprPassEligible"]);
        this.handleReferralInfoChange(changes["canUserApplyReferralCode"]);
        this.handleSuprPassActivePlanChange(changes["activeSuprPassDetails"]);
        this.handleAnonymousUserChange(changes["isAnonymousUser"]);
        this.handleIsExperimentsFetchedChange(changes["isExperimentsFetched"]);
    }

    goToProfilePage() {
        this.delayOpen(() => this.routerService.goToProfilePage());
    }

    goToMySubscriptionsPage() {
        this.delayOpen(() => {
            this.routerService.goToSubscriptionListPage();
        });
    }

    goToWalletPage() {
        this.delayOpen(() => this.routerService.goToWalletPage());
    }

    goToWalletTxnPage() {
        if (!this.suprCreditsEnabled) {
            this.delayOpen(() =>
                this.routerService.goToWalletTransactionsPage()
            );

            return;
        }

        this.delayOpen(() => this.routerService.goToWalletTransactionsV2Page());
    }

    goToPauseOrdersPage() {
        this.delayOpen(() => {
            this.storeService.togglePauseModal();
        });
    }

    goToSchedulePage() {
        this.delayOpen(() => this.routerService.goToSchedulePage());
    }

    goToSupportPage() {
        this.delayOpen(() => this.routerService.goToSupportPage());
    }

    goToSuprPassPage() {
        this.delayOpen(() => this.routerService.goToSuprPassPage());
    }

    goToReferralPage() {
        this.delayOpen(() => this.routerService.goToReferralPage());
    }

    goToRewardsPage() {
        this.delayOpen(() => this.routerService.goToRewardsPage());
    }

    handleProfileClick(event: TouchEvent) {
        event.stopPropagation();
        if (!this.isAnonymousUser) {
            this.goToProfilePage();
            return;
        } else {
            this.authService.logout();
        }
    }

    handleAnonymousUserCitySelection() {
        this.authService.logout(false);
        this.routerService.goToCitySelectionPage({ replaceUrl: true });
    }

    private setShowSuprPassCard() {
        const showSuprPassCard = SETTINGS.SHOW_SUPR_PASS_SIDEBAR_CARD;

        this.showSuprPassCard = this.settingsService.getSettingsValue(
            showSuprPassCard,
            SETTINGS_KEYS_DEFAULT_VALUE[showSuprPassCard]
        );
    }

    private setAnonymousUserCityInfo() {
        this.anonymousUserCityInfo = this.userService.getAnonymousUserCityInfo();
    }

    private handleAnonymousUserChange(change: SimpleChange) {
        if (
            change &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        ) {
            this.setAnonymousUserCityInfo();
        }
    }

    private handleIsExperimentsFetchedChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setShowSuprPassCard();
        }
    }

    private handleSuprPassInfoText() {
        if (this.activeSuprPassDetails) {
            if (this.activeSuprPassDetails.isAutoDebitActive) {
                this.suprPassInfoText = TEXTS.LIFETIME_MEMBER;
            } else {
                const { daysLeft } = this.activeSuprPassDetails;

                this.suprPassInfoText = `${daysLeft} days left `;
            }
        } else {
            this.suprPassInfoText = TEXTS.GET_SUPR_PASS;
        }
    }

    private handleReferralInfoText() {
        if (this.canUserApplyReferralCode) {
            this.referralInfoText = TEXTS.ENTER_REFERRAL_CODE;
            return;
        }

        this.referralInfoText = "";
    }

    private handleSubscriptionListChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleSubscriptionCountInfo();
        }
    }

    private handleSuprPassInfoChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleSuprPassInfoText();
        }
    }

    private handleReferralInfoChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleReferralInfoText();
        }
    }

    private handleSuprPassActivePlanChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleSuprPassInfoText();
        }
    }

    private delayOpen(fn: () => void) {
        setTimeout(fn, 100);
    }

    private setAppVersion() {
        this.appVersion = this.platformService.getAppVersion();
    }

    private handleSubscriptionCountInfo() {
        if (this.utilService.isLengthyArray(this.subscriptionList)) {
            this.setSubscriptionCountStr();
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setSubscriptionCountStr() {
        const split = this.subscriptionService.splitSubscriptionsBasedOnStatus(
            this.subscriptionList
        );

        const subscriptionChipInfo = this.subscriptionService.getPrioritySubcriptionInfoChip(
            split
        );

        if (subscriptionChipInfo) {
            const { type, count } = subscriptionChipInfo;
            this.subscriptionCountClass = type;
            this.subscriptionCountStr = `${count} ${this.getSubcriptionCountStatus(
                type
            )}`;
        } else {
            this.setEmptySubscriptionValues();
        }
    }

    private setEmptySubscriptionValues() {
        this.subscriptionCountStr = null;
        this.subscriptionCountClass = "";
    }

    private getSubcriptionCountStatus(type: string): string {
        const status = type === "pending" ? "ending" : type;
        return this.utilService.capitalizeOnlyFirstLetter(status);
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
