import { Component, ChangeDetectionStrategy } from "@angular/core";

const SCHEDULES_COUNT = 3;
const ITEMS_COUNT = 2;

@Component({
    selector: "supr-schedule-skeleton",
    templateUrl: "./supr-schedule-skeleton.component.html",
    styleUrls: ["./supr-schedule-skeleton.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleSkeletonComponent {
    items = [];
    schedules = [];

    constructor() {
        this.items = Array(ITEMS_COUNT).fill("");
        this.schedules = Array(SCHEDULES_COUNT).fill("");
    }

    trackByFn(_: any, index: number): number {
        return index;
    }
}
