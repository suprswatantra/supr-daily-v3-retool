import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { DeliveryTimelineSlot, DeliveryTimelineItem } from "@models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

import { Segment, SuprTimelineProps } from "@types";

import { ICON_NAMES } from "../supr-icon/supr-icon.constants";

@Component({
    selector: "supr-order-timeline",
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: "./supr-order-timeline.component.html",
})
export class SuprOrderTimeline {
    @Input() date: string;
    @Input() slot: string;
    @Input() collapsed = true;
    @Input() slotTime: string;
    @Input() multiplePOD: boolean;
    @Input() headerObjectName: string;
    @Input() deliveryProofObjectName: string;

    @Input() set slotTimeline(data: DeliveryTimelineSlot) {
        this._slotTimeline = data;

        this.setContextList();
        this.createTimelineConfig();
    }
    get slotTimeline(): DeliveryTimelineSlot {
        return this._slotTimeline;
    }

    headerIcon = "clock";
    globalStatus: string;
    header: SuprTimelineProps.Header;
    timelineItems: SuprTimelineProps.LineItem[];
    headerContextList: Segment.ContextListItem[] = [];

    private _slotTimeline: DeliveryTimelineSlot;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    goToDeliveryProofPage() {
        if (this.multiplePOD) {
            this.routerService.goToDeliveryProofV2Page({
                queryParams: {
                    date: this.date,
                    slot: this.slotTime,
                },
            });
        } else {
            this.routerService.goToDeliveryProofPage({
                queryParams: {
                    date: this.date,
                    slot: this.slot,
                },
            });
        }
    }

    private getSubtitleText(lineItem: DeliveryTimelineItem): string {
        if (!lineItem || !lineItem.subtitle) {
            return "";
        }

        const otpText = this.getOtpText(lineItem);

        return `${lineItem.subtitle} ${otpText}`;
    }

    private getOtpText(lineItem: DeliveryTimelineItem): string {
        if (!lineItem || !lineItem.otp) {
            return "";
        }

        return `OTP ${lineItem.otp}`;
    }

    private setContextList() {
        this.globalStatus = this.utilService.getNestedValue(
            this._slotTimeline,
            "global_status",
            ""
        );

        this.headerContextList = [
            {
                name: "status-header",
                value: this.globalStatus,
            },
        ];
    }

    private getIconName(lineItem: DeliveryTimelineItem): string {
        return lineItem.achieved
            ? ICON_NAMES.RADIO_CHECKED
            : ICON_NAMES.UNCHECKED;
    }

    private createTimelineConfig() {
        const timelineItems: DeliveryTimelineItem[] = this.utilService.getNestedValue(
            this._slotTimeline,
            "timeline"
        );

        this.header = {
            text: this.globalStatus,
        };

        if (this.utilService.isLengthyArray(timelineItems)) {
            this.timelineItems = timelineItems.map((lineItem) => {
                if (this.utilService.isEmpty(lineItem)) {
                    return;
                }

                return {
                    checked: lineItem.achieved,
                    icon: this.getIconName(lineItem),
                    title: {
                        text: lineItem.title,
                    },
                    subtitle: {
                        text: this.getSubtitleText(lineItem),
                        highlightText: this.getOtpText(lineItem),
                    },
                    image: {
                        text: "VIEW",
                        url: lineItem.delivery_proof,
                    },
                };
            });
        }
    }
}
