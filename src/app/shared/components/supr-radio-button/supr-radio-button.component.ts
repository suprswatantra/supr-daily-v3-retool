import {
    Component,
    EventEmitter,
    Input,
    Output,
    ChangeDetectionStrategy,
} from "@angular/core";
import { RadioButton } from "@types";

@Component({
    selector: "supr-radio-button",
    template: `
        <div
            class="wrapper suprRow"
            [class.selected]="selected"
            [class.disabled]="data?.disabled"
            (click)="selectOption()"
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="data?.text"
            [saContext]="saContext"
        >
            <supr-icon [name]="selected ? 'approve' : 'unchecked'"></supr-icon>
            <div class="spacer8"></div>
            <supr-text type="body">
                {{ data?.text }}
            </supr-text>
            <supr-icon
                [name]="data?.icon"
                class="right"
                *ngIf="data?.icon"
            ></supr-icon>
        </div>
    `,
    styleUrls: ["./supr-radio-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RadioButtonComponent {
    @Input() data: RadioButton;
    @Input() selected = false;
    @Input() saObjectName: string;
    @Input() saContext: string;
    @Output() handleClick: EventEmitter<TouchEvent> = new EventEmitter();

    selectOption() {
        if (this.data && !this.data.disabled) {
            this.handleClick.emit();
        }
    }
}
