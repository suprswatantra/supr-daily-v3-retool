import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { FeedbackService } from "@services/layout/feedback.service";

import { FEEDBACK_ANALYTICS_OBJECT_NAMES } from "@constants";
import { OrderFeedbackSetting } from "@types";
import { Feedback } from "@models";

@Component({
    selector: "supr-feedback-options",
    template: `
        <div class="primaryRes">
            <div class="suprRow">
                <supr-svg
                    class="thumbsUp"
                    [class.black]="primaryResponse === order_feedback.thumbsUp"
                    (click)="toggleThumbs(order_feedback.thumbsUp)"
                    saClick
                    saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .THUMBS_UP}"
                    [saObjectValue]="id"
                ></supr-svg>
                <supr-svg
                    class="thumbsDown"
                    [class.black]="
                        primaryResponse === order_feedback.thumbsDown
                    "
                    (click)="toggleThumbs(order_feedback.thumbsDown)"
                    saClick
                    saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .THUMBS_DOWN}"
                    [saObjectValue]="id"
                ></supr-svg>
                <div
                    class="suprColumn"
                    *ngIf="primaryResponse === order_feedback.thumbsDown"
                >
                    <supr-button
                        (click)="contactSupport.emit()"
                        saClick
                        saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES
                            .IMPRESSION.CONTACT_SUPPORT}"
                    >
                        <supr-text>{{
                            order_feedback.contactSupport
                        }}</supr-text>
                    </supr-button>
                </div>
            </div>
        </div>

        <div
            *ngIf="primaryResponse === order_feedback.thumbsDown"
            class="secResponse negative"
        >
            <div class="divider12"></div>

            <div
                class="textBlock"
                *ngIf="negativeParams.length <= 0; else defaultBlock"
            >
                <supr-text>{{ order_feedback.negativeTitle }}</supr-text>
                <div class="divider8"></div>
                <supr-text class="subtitle">{{
                    order_feedback.negativeSubtitle
                }}</supr-text>
            </div>

            <ng-template #defaultBlock>
                <supr-text>{{ header }}</supr-text>
                <div class="divider4"></div>
                <div class="buttonList">
                    <supr-button
                        *ngFor="let param of negativeParams"
                        [class.selected]="negativeParamsObject[param]"
                        (click)="handleSelectParams(param)"
                        saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES
                            .IMPRESSION.ATTRIBUTE}"
                        [saObjectValue]="param"
                        >{{ param }}</supr-button
                    >
                </div>
                <div class="divider12"></div>

                <div class="suprRow links">
                    <ion-row *ngIf="!comment" (click)="addComment()">
                        <supr-text class="commentText">{{
                            order_feedback.addComment
                        }}</supr-text>
                        <supr-icon name="chevron_right"></supr-icon>
                    </ion-row>

                    <supr-text *ngIf="comment" class="commentText">{{
                        comment
                    }}</supr-text>
                </div>
            </ng-template>
        </div>

        <div
            *ngIf="primaryResponse === order_feedback.thumbsUp"
            class="secResponse positive"
        >
            <div class="divider12"></div>

            <supr-text>{{ header }}</supr-text>
            <div class="divider4"></div>
            <div class="buttonList">
                <supr-button
                    *ngFor="let param of positiveParams"
                    [class.selected]="positiveParamsObject[param]"
                    (click)="handleSelectParams(param)"
                    saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .ATTRIBUTE}"
                    [saObjectValue]="param"
                    >{{ param }}</supr-button
                >
            </div>
            <div class="divider12"></div>

            <div class="suprRow links">
                <ion-row
                    *ngIf="!comment"
                    (click)="addComment()"
                    saClick
                    saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .ADD_COMMENT}"
                    [saObjectValue]="primaryResponse"
                >
                    <supr-text class="commentText">{{
                        order_feedback.addComment
                    }}</supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </ion-row>
            </div>

            <supr-text *ngIf="comment" class="commentText">{{
                comment
            }}</supr-text>
        </div>
    `,
    styleUrls: ["../styles/supr-feedback-options.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprFeedbackOptions implements OnInit {
    @Input() primaryResponse: string;
    @Input() positiveParams: Array<string>;
    @Input() negativeParams: Array<string>;
    @Input() positiveParamsObject: Feedback.ParamsObject = {};
    @Input() negativeParamsObject: Feedback.ParamsObject = {};
    @Input() header: string = "";

    @Input()
    set comment(comment: string) {
        this.setComment(comment);
    }
    get comment(): string {
        return this._comment;
    }

    @Input() id: string = "";

    @Output() handleCommentModal: EventEmitter<any> = new EventEmitter();
    @Output() feedbackChange: EventEmitter<any> = new EventEmitter();
    @Output() contactSupport: EventEmitter<any> = new EventEmitter();

    isThumbsUp: boolean = false;
    _comment: string = "";
    isThumbsDown: boolean = false;
    order_feedback: OrderFeedbackSetting.Nudge;

    constructor(private feedbackService: FeedbackService) {}

    ngOnInit() {
        this.order_feedback = this.feedbackService.getOrderFeedbackSettings();
    }

    setComment(comment) {
        let _comment = comment;
        if (comment && comment.length > 20) {
            _comment = `${comment.slice(0, 20)}...`;
        }
        this._comment = _comment;
    }

    toggleThumbs(primaryResponse: string) {
        this.handleChange({ primaryResponse });
    }

    handleSelectParams(param: string) {
        this.handleChange({ param });
    }

    handleChange(data) {
        this.feedbackChange.emit(data);
    }

    addComment() {
        this.handleCommentModal.emit(true);
    }
}
