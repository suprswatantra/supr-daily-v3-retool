import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { FeedbackService } from "@services/layout/feedback.service";

import { FEEDBACK_ANALYTICS_OBJECT_NAMES } from "@constants";
import { OrderFeedbackSetting } from "@types";

@Component({
    selector: "supr-feedback-comment",
    template: `
        <supr-page-header (handleBackButtonClick)="closeModal()">
            <div class="header">{{ order_feedback.addComment }}</div>
        </supr-page-header>

        <div class="suprScrollContent">
            <div class="divider16"></div>

            <supr-text type="action14">
                {{ order_feedback.commentLable }}
            </supr-text>

            <div class="divider12"></div>

            <textarea
                (keyup)="onChange($event)"
                [placeholder]="order_feedback.commentPlaceHolder"
                rows="5"
                type="text"
                saClick
                saObjectName="${FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION
                    .COMMENT}"
            ></textarea>
        </div>

        <supr-page-footer>
            <supr-button (handleClick)="handleSubmit()">
                {{ order_feedback.submit }}
            </supr-button>
        </supr-page-footer>
    `,
    styleUrls: ["../styles/feedback-comment.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprFeedbackComment implements OnInit {
    @Output() handleCommentModal: EventEmitter<any> = new EventEmitter();

    productText: string;
    order_feedback: OrderFeedbackSetting.Nudge;

    constructor(private feedbackService: FeedbackService) {}

    ngOnInit() {
        this.order_feedback = this.feedbackService.getOrderFeedbackSettings();
    }

    closeModal() {
        this.handleCommentModal.emit({ toggle: false });
    }

    onChange(event: any) {
        const { value = "" } = event && event.target;
        this.productText = value.trim();
    }

    handleSubmit() {
        if (this.productText) {
            this.handleCommentModal.emit({
                toggle: false,
                comment: this.productText,
            });
        }
    }
}
