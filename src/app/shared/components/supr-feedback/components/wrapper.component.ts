import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { Observable } from "rxjs";

import { FeedbackAdapter as Adapter } from "@shared/adapters/feedback.adapter";

import { Sku, Feedback } from "@models";

@Component({
    selector: "supr-feedback-wrapper",
    template: `
        <supr-order-feedback
            [skuList]="selectDeliverySkus$ | async"
            [showFeedbackSuccess]="showFeedbackSuccess"
            [showOverallOrderNudge]="showDeliveryFeedbackNudge$ | async"
            [overallOrderParams]="selectDeliveryFeedbackRequest$ | async"
            (hideDeliveryFeedbackNudge)="hideDeliveryFeedbackNudge($event)"
            (hideDeliverySuccessNudge)="hideDeliverySuccessNudge()"
        ></supr-order-feedback>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit {
    @Input() isLoggedIn: boolean;

    selectDeliverySkus$: Observable<Sku[]>;
    showDeliveryFeedbackNudge$: Observable<boolean>;
    selectDeliveryFeedbackRequest$: Observable<
        Feedback.deliveryFeedbackRequest
    >;
    showFeedbackSuccess: boolean = false;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.selectDeliverySkus$ = this.adapter.selectDeliverySkus$;
        this.showDeliveryFeedbackNudge$ = this.adapter.showDeliveryFeedbackNudge$;
        this.selectDeliveryFeedbackRequest$ = this.adapter.selectDeliveryFeedbackRequest$;
    }

    hideDeliveryFeedbackNudge(isSubmit: boolean) {
        this.adapter.hideDeliveryFeedbackNudge();
        if (isSubmit) {
            this.showFeedbackSuccess = true;
        }
    }

    hideDeliverySuccessNudge() {
        this.showFeedbackSuccess = false;
    }
}
