import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { FeedbackService } from "@services/layout/feedback.service";
import { UtilService } from "@services/util/util.service";

import { Feedback } from "@models";
import { OrderFeedbackSetting } from "@types";

@Component({
    selector: "supr-order-feedback-wrapper",
    template: `
        <supr-feedback-options
            [primaryResponse]="primaryResponse"
            [positiveParams]="positiveParams"
            [negativeParams]="negativeParams"
            [positiveParamsObject]="positiveParamsObject"
            [negativeParamsObject]="negativeParamsObject"
            [header]="getHeader"
            [comment]="comment"
            [id]="id"
            (feedbackChange)="feedbackChange($event)"
            (handleCommentModal)="addCommentModal($event)"
            (contactSupport)="contactSupport.emit()"
        ></supr-feedback-options>

        <div *ngIf="showButtons">
            <div class="divider24"></div>
            <supr-button
                *ngIf="primaryResponse"
                (click)="handleOnSubmit()"
                saClick
                [saObjectName]="actionButton"
                >{{ actionButton }}</supr-button
            >
        </div>
    `,
    styleUrls: ["../styles/supr-order-feedback-wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderFeedbackWrapper implements OnInit {
    @Input() showButtons: boolean = false;
    @Input() comment: string = "";
    @Input() id: string = "";

    @Input()
    set params(params: Feedback.PrimaryParam) {
        this.setParams(params);
    }

    @Output() onSubmit: EventEmitter<any> = new EventEmitter();
    @Output() handleCommentModal: EventEmitter<any> = new EventEmitter();
    @Output() contactSupport: EventEmitter<any> = new EventEmitter();

    primaryResponse: string = "";
    positiveParams: Array<string> = [];
    negativeParams: Array<string> = [];
    positiveParamsObject: Feedback.ParamsObject = {};
    negativeParamsObject: Feedback.ParamsObject = {};
    order_feedback: OrderFeedbackSetting.Nudge;

    constructor(
        private feedbackService: FeedbackService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.order_feedback = this.feedbackService.getOrderFeedbackSettings();
    }

    get getHeader() {
        if (this.primaryResponse) {
            if (this.showButtons) {
                return this.order_feedback[this.primaryResponse]
                    .defaultOptionsPosHeader;
            }
            return this.order_feedback[this.primaryResponse].optionsHeader;
        }
    }

    get actionButton() {
        return this.primaryResponse === this.order_feedback.thumbsUp
            ? this.order_feedback.submit
            : this.order_feedback.proceed;
    }

    handleOnSubmit(changedFeedback) {
        const {
            primaryResponse,
            positiveParamsObject,
            negativeParamsObject,
        } = this;

        const feedbackOrderResponse = {
            primaryResponse,
            positiveParamsObject,
            negativeParamsObject,
        };

        if (this.showButtons && !changedFeedback) {
            const goToSchedulePage =
                this.primaryResponse === this.order_feedback.thumbsDown;
            this.onSubmit.emit({ goToSchedulePage, feedbackOrderResponse });
        } else if (!this.showButtons && changedFeedback) {
            this.onSubmit.emit({
                goToSchedulePage: false,
                feedbackOrderResponse,
            });
        }
    }

    handleThumbsToggle(thumbs) {
        if (thumbs) {
            if (this.primaryResponse !== thumbs) {
                this.resetPositiveParams();
                this.resetNegParams();
                this.comment = "";
            }
            this.primaryResponse = thumbs;
        }
    }

    selectParams(param: string, type?: string) {
        const primaryResponse = type || this.primaryResponse;
        const paramsObject =
            primaryResponse === this.order_feedback.thumbsUp
                ? "positiveParamsObject"
                : "negativeParamsObject";
        if (param) {
            const value = this[paramsObject][param];
            this[paramsObject] = {
                ...this[paramsObject],
                [param]: !value,
            };
        }
    }

    feedbackChange(data: any) {
        const { primaryResponse = "", param = "" } = data;
        this.handleThumbsToggle(primaryResponse);
        this.selectParams(param);
        this.handleOnSubmit(true);
    }

    private resetPositiveParams(params: Array<string> = this.positiveParams) {
        this.positiveParamsObject = {
            ...params.reduce(
                (acc, item) => (acc = { ...acc, [item]: false }),
                {}
            ),
        };
    }

    private resetNegParams(params: Array<string> = this.negativeParams) {
        this.negativeParamsObject = {
            ...params.reduce(
                (acc, item) => (acc = { ...acc, [item]: false }),
                {}
            ),
        };
    }

    addCommentModal(value) {
        this.handleCommentModal.emit({ toggle: value });
    }

    setParams(params: any) {
        if (this.utilService.isLengthyArray(params)) {
            params.forEach(param => {
                const { value = null, statements = [] } = param;
                if (value === 1) {
                    this.positiveParams = statements || [];
                } else if (param.value === 0) {
                    this.negativeParams = statements || [];
                }
            });
        }
    }
}
