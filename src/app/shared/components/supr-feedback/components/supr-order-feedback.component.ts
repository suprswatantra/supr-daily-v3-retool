import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChange,
    EventEmitter,
    SimpleChanges,
    Output,
} from "@angular/core";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { SegmentService } from "@services/integration/segment.service";

import { Sku, Feedback } from "@models";
import {
    MODAL_NAMES,
    MODAL_TYPES,
    FEEDBACK_ANALYTICS_OBJECT_NAMES,
} from "@constants";
import { OrderFeedbackSetting } from "@types";

@Component({
    selector: "supr-order-feedback",
    template: `
        <ng-container *ngIf="showOverallOrderNudge">
            <supr-modal
                modalName="${MODAL_NAMES.ORDER_FEEDBACK}"
                [fullScreen]="fullscreen"
                [modalType]="modalType"
                (handleClose)="handleCloseFeedbackModal()"
            >
                <div
                    class="wrapper orderFeedbackNudge"
                    [style.display]="displayN"
                >
                    <supr-expand-banner
                        [isLeftPaddingZero]="true"
                        [title]="config.title"
                        [subtitle]="config.subtitle"
                        [isExpand]="false"
                    ></supr-expand-banner>
                    <div class="divider24"></div>

                    <div class="orderImage suprRow center">
                        <supr-text
                            *ngIf="
                                trimmedImagesLength && trimmedImagesLength > 0
                            "
                            >+{{ trimmedImagesLength }}</supr-text
                        >
                        <supr-product-tile-image
                            *ngFor="let item of images"
                            [skuImageUrl]="item"
                        ></supr-product-tile-image>
                    </div>
                    <div class="divider12"></div>

                    <supr-order-feedback-wrapper
                        [showButtons]="true"
                        [params]="params"
                        [comment]="comment"
                        (onSubmit)="onSubmit($event)"
                        [id]="overallOrderParams.feedback_date"
                        (contactSupport)="contactSupport()"
                        (handleCommentModal)="
                            handleCommentModal($event, $event)
                        "
                    ></supr-order-feedback-wrapper>
                </div>

                <supr-feedback-comment
                    [style.display]="displayF"
                    (handleCommentModal)="handleCommentModal($event)"
                ></supr-feedback-comment>
            </supr-modal>
        </ng-container>
        <supr-modal
            *ngIf="showFeedbackSuccess && !showOverallOrderNudge"
            modalName="${MODAL_NAMES.DELIVERY_FEEDBACK_SUCCESS}"
            modalType="${MODAL_TYPES.BOTTOM_SHEET}"
            (handleClose)="handleCloseDeliverySuccessModal()"
        >
            <div class="suprColumn feedbackSubmit">
                <supr-svg class="thumbsUp"></supr-svg>
                <div class="divider24"></div>
                <supr-text>
                    {{ config.feedbackSuccess }}
                </supr-text>
                <div class="divider24"></div>
            </div>
        </supr-modal>
    `,
    styleUrls: ["../styles/supr-order-feedback.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderFeedbackComponent implements OnInit, OnChanges {
    @Input() showOverallOrderNudge: boolean;
    @Input() showFeedbackSuccess = false;
    @Input() isLoggedIn: boolean;
    @Input() skuList: Sku[];
    @Input() overallOrderParams: Feedback.deliveryFeedbackRequest;

    @Output()
    hideDeliveryFeedbackNudge: EventEmitter<boolean> = new EventEmitter();

    @Output()
    hideDeliverySuccessNudge: EventEmitter<void> = new EventEmitter();

    images: Array<string> = [];
    params: Feedback.PrimaryParam[] = [];
    fullscreen = false;
    modalType: string;
    displayF: string;
    displayN: string;
    comment: string = "";
    trimmedImagesLength: number = 0;
    orderFeedbackSetting: OrderFeedbackSetting.Nudge;

    constructor(
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private utilService: UtilService,
        private feedbackService: FeedbackService,
        private segment: SegmentService
    ) {}

    ngOnInit() {
        this.setDisplayParams();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSkuListChange(changes["skuList"]);
        this.handleOverallOrderParamsChange(changes["overallOrderParams"]);
    }

    get config() {
        return this.feedbackService.getOrderFeedbackSettings();
    }

    private handleSkuListChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (this.utilService.isLengthyArray(change.currentValue)) {
            this.initializeImages();
        }
    }

    private handleOverallOrderParamsChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.currentValue &&
            !this.utilService.isEmpty(change.currentValue) &&
            this.utilService.isEmpty(change.previousValue)
        ) {
            this.initializeParams();
        }
    }

    private initializeImages() {
        if (this.skuList && this.skuList.length > 0) {
            let images = this.skuList.reduce((acc, sku) => {
                const { image } = sku;
                acc.push(image.fullUrl);
                return acc;
            }, []);
            const length = images.length;
            if (length > 3) {
                this.trimmedImagesLength = length - 3;
                images = images.splice(0, 3);
            }
            this.images = images || [];
        }
    }

    private initializeParams() {
        const { params = [] } = this.overallOrderParams;
        this.params = params;
    }

    onSubmit(data: Feedback.FeedbackWrapperResponse) {
        const { goToSchedulePage, feedbackOrderResponse } = data;

        const { feedback_request_id, feedback_date } = this.overallOrderParams;
        this.feedbackService.submitOrderFeedbackResponse({
            ...feedbackOrderResponse,
            feedback_request_id,
            comment: this.comment,
        });

        if (goToSchedulePage && feedback_date) {
            this.scheduleService.setOrderFeedback(true);
            this.scheduleService.setCustomScheduleStartDate(feedback_date);
            this.scheduleService.setCustomScheduleScrollDate(feedback_date);
            this.hideFeedbackModal(false);
        } else {
            setTimeout(() => {
                this.handleCloseDeliverySuccessModal();
            }, this.config.successTimer);
            this.hideFeedbackModal(true);
        }
    }

    hideFeedbackModal(isSubmit: boolean) {
        this.hideDeliveryFeedbackNudge.emit(isSubmit);
    }

    handleCloseFeedbackModal() {
        this.segment.trackImpression({
            objectName: "skip",
            objectValue: `order-rating ${this.overallOrderParams.feedback_date}`,
        });
        this.hideFeedbackModal(false);
    }

    setDisplayParams() {
        this.modalType = this.fullscreen ? "" : MODAL_TYPES.BOTTOM_SHEET;
        this.displayF = this.fullscreen ? "block" : "none";
        this.displayN = this.fullscreen ? "none" : "block";
    }

    handleCommentModal({ toggle, comment }) {
        this.fullscreen = toggle;
        this.setDisplayParams();
        this.comment = comment;

        this.segment.trackImpression({
            objectName:
                (toggle &&
                    FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION.VIEW_COMMENT) ||
                FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION.CLOSE_COMMENT,
        });
    }

    contactSupport() {
        this.hideFeedbackModal(false);
        this.routerService.goToSupportPage();
    }

    handleCloseDeliverySuccessModal() {
        this.hideDeliverySuccessNudge.emit();
    }
}
