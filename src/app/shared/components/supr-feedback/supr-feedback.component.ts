import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";

import { FeedbackAdapter as Adapter } from "@shared/adapters/feedback.adapter";

@Component({
    selector: "supr-feedback",
    template: `
        <supr-feedback-wrapper
            [isLoggedIn]="isLoggedIn$ | async"
        ></supr-feedback-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprFeedback implements OnInit {
    isLoggedIn$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isLoggedIn$ = this.adapter.isLoggedIn$;
    }
}
