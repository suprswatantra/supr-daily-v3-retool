import { SuprFeedback } from "./supr-feedback.component";
import { WrapperComponent } from "./components/wrapper.component";
import { SuprFeedbackComment } from "./components/feedback-comment.component";
import { SuprFeedbackOptions } from "./components/feedback-options.component";
import { OrderFeedbackComponent } from "./components/supr-order-feedback.component";
import { OrderFeedbackWrapper } from "./components/supr-order-feedback-wrapper.component";

export const FeedbackComponents = [
    SuprFeedback,
    WrapperComponent,
    SuprFeedbackComment,
    SuprFeedbackOptions,
    OrderFeedbackComponent,
    OrderFeedbackWrapper,
];
