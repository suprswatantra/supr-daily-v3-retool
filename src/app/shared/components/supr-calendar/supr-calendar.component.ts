import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
} from "@angular/core";

import { Vacation } from "@models";

import { SuprDateService } from "@services/date/supr-date.service";
import { CalendarService } from "@services/date/calendar.service";

import { Calendar } from "@types";

@Component({
    selector: "supr-calendar",
    template: `
        <div class="calendar">
            <supr-calendar-header
                *ngIf="!hideHeader"
                [currentDate]="currentDate"
                [dateFormat]="headerDateFormat"
                [monthChangeSaName]="monthChangeSaName"
                (handleChangeMonth)="_changeMonth($event)"
            ></supr-calendar-header>
            <supr-calendar-month-slider
                [startDate]="startDate"
                [endDate]="endDate"
                [selectedDate]="selectedDate"
                [currentDate]="currentDate"
                [firstActiveDay]="firstActiveDay"
                [disablePastDays]="disablePastDays"
                [slideMonthDirection]="slideMonthDirection"
                [vacation]="vacation"
                [enableToday]="enableToday"
                [customDisabledDates]="customDisabledDates"
                [customDisabledDateText]="customDisabledDateText"
                [saObjectNameDateClick]="saObjectNameDateClick"
                [monthSwipeSaName]="monthChangeSaName"
                (monthChanged)="_updateDateOnMonthChange($event)"
                (updateDate)="_updateSelectedDate($event)"
            ></supr-calendar-month-slider>
        </div>
    `,
    styleUrls: ["./supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarComponent implements OnInit {
    @Input() hideHeader = false;
    @Input() headerDateFormat = "LLL yyyy";
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() selectedDate: Calendar.DayData;
    @Input() disablePastDays = false;
    @Input() firstActiveDay: Calendar.DayData;
    @Input() vacation: Vacation;
    @Input() enableToday = false;
    @Input() customDisabledDates: Array<string>;
    @Input() customDisabledDateText;

    @Input() saObjectNameDateClick: string;
    @Input() monthChangeSaName: string;

    @Output()
    handleDateChange: EventEmitter<Calendar.DayData> = new EventEmitter();
    @Output()
    handleSelectedDate: EventEmitter<Calendar.DayData> = new EventEmitter();

    currentDate: Calendar.DayData;
    slideMonthDirection: number;

    constructor(
        private calendarService: CalendarService,
        private suprDateService: SuprDateService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        if (!this.firstActiveDay) {
            const nxtAvblDay = this.calendarService.getNextAvailableDate();
            this.firstActiveDay = this.suprDateService.suprDate(nxtAvblDay);
        }

        this.setCurrentDate();
        this.setSlideMonthDirection();
    }

    _updateDateOnMonthChange(newDate: Calendar.DayData) {
        this.currentDate = newDate;
        this.cdr.detectChanges();

        if (this.handleDateChange) {
            this.handleDateChange.emit(newDate);
        }
    }

    _updateSelectedDate(selectedDate?: Calendar.DayData) {
        if (this.handleSelectedDate) {
            this.handleSelectedDate.emit(selectedDate);
        }

        this.checkMonthSlideOnNewDate(selectedDate);
    }

    _changeMonth(direction: string) {
        if (direction === "left") {
            this.slideMonthDirection--;
        } else if (direction === "right") {
            this.slideMonthDirection++;
        }
    }

    private checkMonthSlideOnNewDate(newDate: Calendar.DayData) {
        if (this.currentDate.month === newDate.month) {
            return;
        }

        if (this.currentDate.time > newDate.time) {
            this.slideMonthDirection--;
        } else if (this.currentDate.time < newDate.time) {
            this.slideMonthDirection++;
        }

        this.cdr.detectChanges();
    }

    private setCurrentDate() {
        if (this.selectedDate) {
            this.currentDate = this.selectedDate;
        } else if (this.startDate || this.endDate) {
            this.currentDate = this.startDate;
        } else {
            this.currentDate = this.suprDateService.todaySuprDate();
        }
    }

    private setSlideMonthDirection() {
        this.slideMonthDirection = 0;
    }
}
