import { CalendarComponent } from "./supr-calendar.component";
import { CalendarHeaderComponent } from "./components/header.component";
import { CalendarDayBarComponent } from "./components/day-bar.component";
import { CalendarDayComponent } from "./components/day.component";
import { CalendarWeekComponent } from "./components/week.component";
import { CalendarMonthComponent } from "./components/month.component";
import { CalendarMonthSliderComponent } from "./components/month-slider.component";

export const calendarComponents = [
    CalendarComponent,
    CalendarHeaderComponent,
    CalendarDayBarComponent,
    CalendarDayComponent,
    CalendarWeekComponent,
    CalendarMonthComponent,
    CalendarMonthSliderComponent,
];
