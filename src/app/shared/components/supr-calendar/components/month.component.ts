import {
    Component,
    Input,
    EventEmitter,
    Output,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Vacation } from "@models";
import { Calendar } from "@types";

@Component({
    selector: "supr-calendar-month",
    template: `
        <div *ngFor="let week of monthData; index as weekIndex">
            <supr-calendar-week
                [weekData]="week"
                [monthData]="monthData"
                [disablePastDays]="disablePastDays"
                [startDate]="startDate"
                [endDate]="endDate"
                [selectedDate]="selectedDate"
                [firstActiveDay]="firstActiveDay"
                [enableToday]="enableToday"
                [vacation]="vacation"
                [customDisabledDates]="customDisabledDates"
                [customDisabledDateText]="customDisabledDateText"
                [saObjectNameDateClick]="saObjectNameDateClick"
                (updateDate)="updateDate.emit($event)"
            >
            </supr-calendar-week>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarMonthComponent {
    @Input() monthData: Calendar.MonthData;
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() selectedDate: Calendar.DayData;
    @Input() firstActiveDay: Calendar.DayData;
    @Input() vacation: Vacation;
    @Input() disablePastDays: boolean;

    @Input() saObjectNameDateClick: string;

    @Input() customDisabledDates: Array<string> = [];
    @Input() customDisabledDateText = "";

    @Input() enableToday: boolean;
    @Output() updateDate: EventEmitter<Calendar.DayData> = new EventEmitter();
}
