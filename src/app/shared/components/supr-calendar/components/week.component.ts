import {
    Component,
    Input,
    EventEmitter,
    Output,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Vacation } from "@models";
import { Calendar } from "@types";

@Component({
    selector: "supr-calendar-week",
    template: `
        <div class="suprRow">
            <ng-container *ngFor="let day of weekData; let dayIndex = index">
                <ng-container *ngIf="dayIndex >= 1 && dayIndex <= 7">
                    <supr-calendar-day
                        class="dayDate"
                        [dayData]="day"
                        [monthData]="monthData"
                        [selectedDate]="selectedDate"
                        [firstActiveDay]="firstActiveDay"
                        [startDate]="startDate"
                        [endDate]="endDate"
                        [vacation]="vacation"
                        [disablePastDays]="disablePastDays"
                        [enableToday]="enableToday"
                        [customDisabledDates]="customDisabledDates"
                        [customDisabledDateText]="customDisabledDateText"
                        [saObjectName]="saObjectNameDateClick"
                        (updateDate)="updateDate.emit($event)"
                    ></supr-calendar-day
                ></ng-container>
            </ng-container>
        </div>
    `,
    styleUrls: ["../supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarWeekComponent {
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() selectedDate: Calendar.DayData;
    @Input() firstActiveDay: Calendar.DayData;
    @Input() monthData: Calendar.MonthData;
    @Input() weekData: Calendar.WeekData;
    @Input() disablePastDays: boolean;
    @Input() vacation: Vacation;
    @Input() enableToday: boolean;

    @Input() saObjectNameDateClick: string;

    @Input() customDisabledDates: Array<string> = [];
    @Input() customDisabledDateText = "";

    @Output() updateDate: EventEmitter<Calendar.DayData> = new EventEmitter();
}
