import {
    Component,
    OnInit,
    OnChanges,
    Input,
    EventEmitter,
    Output,
    SimpleChanges,
    SimpleChange,
    ChangeDetectionStrategy,
} from "@angular/core";

import classNames from "classnames";

import { MONTH_NAMES } from "@constants";
import { Vacation } from "@models";

import { UtilService } from "@services/util/util.service";

import { DateService } from "@services/date/date.service";
import { SuprDateService } from "@services/date/supr-date.service";

import { Calendar } from "@types";

@Component({
    selector: "supr-calendar-day",
    template: `
        <div
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
            [ngClass]="containerClass"
            (click)="selectDate()"
        >
            <div [ngClass]="dateClass">
                {{ dayData.date }}
                <span class="month" *ngIf="monthText">{{ monthText }}</span>
            </div>
            <supr-text *ngIf="isPaused">Paused</supr-text>
            <supr-text
                *ngIf="isCustomDisabledDate && !isPaused && !this.isToday()"
                >{{ customDisabledDateText }}</supr-text
            >
        </div>
    `,
    styleUrls: ["../supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarDayComponent implements OnInit, OnChanges {
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() selectedDate: Calendar.DayData;
    @Input() dayData: Calendar.DayData;
    @Input() firstActiveDay: Calendar.DayData;
    @Input() monthData: Calendar.MonthData;
    @Input() vacation: Vacation;
    @Input() disablePastDays: boolean;
    @Input() enableToday: boolean;

    @Input() saObjectName: string;

    @Input() customDisabledDates: Array<string> = [];
    @Input() customDisabledDateText = "";

    @Output() updateDate: EventEmitter<Calendar.DayData> = new EventEmitter();

    dateClass: string;
    containerClass: string;
    disableDay: boolean;
    vacationDay: boolean;
    isPaused: boolean;
    monthText = "";
    saObjectValue: string;
    isCustomDisabledDate: boolean;

    constructor(
        private dateService: DateService,
        private suprDateService: SuprDateService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.setDisableDay();
        this.setVacationDay();
        this.setIsPausedDay();
        this.setDateClasses();
        this.setMonthText();
        this.setAnalyticsData();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleSelectedDateChange(changes["selectedDate"]);
        this.handleStartEndDateChange(changes["startDate"], changes["endDate"]);
    }

    trackByFn(index: number) {
        return index;
    }

    selectDate() {
        if (!this.disableDay && !this.isPaused) {
            this.updateDate.emit(this.dayData);
        }
    }

    isToday(): boolean {
        const todayDate = this.suprDateService.todaySuprDate();
        return this.dayData.dateText === todayDate.dateText;
    }

    private handleSelectedDateChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setDateClasses();
        }
    }

    private handleStartEndDateChange(
        startChange: SimpleChange,
        endChange: SimpleChange
    ) {
        if (
            this.canHandleChange(startChange) ||
            this.canHandleChange(endChange)
        ) {
            this.setVacationDay();
            this.setDateClasses();
        }
    }

    private setDisableDay() {
        if (this.isOneOfCustomDisabledDays()) {
            this.disableDay = true;
            this.isCustomDisabledDate = true;
            return;
        }
        if (this.enableToday && this.isToday()) {
            this.disableDay = false;
        } else {
            this.disableDay = this.disablePastDays && this.isPastDay();
        }
    }

    private setVacationDay() {
        this.vacationDay = this.isVacationDay();
    }

    private setIsPausedDay() {
        this.isPaused = this.isPausedDay();
    }

    private setDateClasses() {
        const isToday = this.isToday();
        const isSelected = this.isSelected();
        const isLastMonth = this.isLastMonth();
        const isStartDay = this.isStartDate();
        const isEndDay = this.isEndDate();
        const isStartSameAsEnd = this.isStartSameAsEnd();

        this.dateClass = classNames({
            dayDateValue: true,
            selected:
                isSelected &&
                (this.vacationDay || (!this.disableDay && !isLastMonth)),
            today: isToday,
            lastMonth: isLastMonth,
            disable: this.disableDay,
            vacation: this.vacationDay,
            pause: this.isPaused,
        });

        this.containerClass = classNames({
            dayDateContainer: true,
            suprColumn: true,
            vacation: this.vacationDay && !isStartSameAsEnd,
            start: isStartDay && !isStartSameAsEnd,
            end: isEndDay && !isStartSameAsEnd,
        });
    }

    private setMonthText() {
        if (this.dayData.date === 1) {
            this.monthText = MONTH_NAMES[this.dayData.month].toLowerCase();
        }
    }

    /*--------------------*
     * Helper methods     *
     *--------------------*/

    private isSelected(): boolean {
        const datesLookUp = [];
        if (this.selectedDate) {
            datesLookUp.push(this.selectedDate.dateText);
        }

        if (this.startDate && this.startDate.dateText) {
            datesLookUp.push(this.startDate.dateText);
        }

        if (this.endDate && this.endDate.dateText) {
            datesLookUp.push(this.endDate.dateText);
        }

        return datesLookUp.includes(this.dayData.dateText);
    }

    private isLastMonth(): boolean {
        return this.dayData.month !== this.monthData[1][1].month;
    }

    private isPastDay(): boolean {
        const dayDate = this.dateService.dateFromText(this.dayData.dateText);
        const firstDay = this.dateService.dateFromText(
            this.firstActiveDay.dateText
        );

        return this.dateService.daysBetweenTwoDates(firstDay, dayDate) < 0;
    }

    private isVacationDay(): boolean {
        if (!this.endDate || !this.startDate) {
            return false;
        }

        const startDayTime = this.startDate.time;
        const endDayTime = this.endDate.time;
        const currentDayTime = this.dayData.time;

        return currentDayTime >= startDayTime && currentDayTime <= endDayTime;
    }

    private isPausedDay(): boolean {
        if (!this.vacation) {
            return false;
        }

        const { start_date, end_date } = this.vacation;
        const startDayTime = this.suprDateService.suprDateFromText(start_date);
        const endDayTime = this.suprDateService.suprDateFromText(end_date);
        const currentDayTime = this.dayData.time;

        return (
            currentDayTime >= startDayTime.time &&
            currentDayTime <= endDayTime.time
        );
    }

    private isStartDate(): boolean {
        return (
            this.startDate && this.dayData.dateText === this.startDate.dateText
        );
    }

    private isEndDate(): boolean {
        return this.endDate && this.dayData.dateText === this.endDate.dateText;
    }

    private isStartSameAsEnd(): boolean {
        return (
            this.endDate && this.startDate.dateText === this.endDate.dateText
        );
    }

    private canHandleChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return false;
        }

        const prevValue = (change.previousValue || {}) as Calendar.DayData;
        const curValue = (change.currentValue || {}) as Calendar.DayData;

        return prevValue.dateText !== curValue.dateText;
    }

    private setAnalyticsData() {
        if (this.saObjectName && this.dayData) {
            this.saObjectValue = this.dayData.dateText;
        }
    }

    private isOneOfCustomDisabledDays() {
        if (!this.utilService.isLengthyArray(this.customDisabledDates)) {
            return false;
        }
        return this.customDisabledDates.indexOf(this.dayData.dateText) > -1;
    }
}
