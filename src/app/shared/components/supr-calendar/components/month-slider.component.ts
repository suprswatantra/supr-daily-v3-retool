import {
    Component,
    OnInit,
    Input,
    Output,
    OnChanges,
    SimpleChanges,
    EventEmitter,
    ViewChild,
    ChangeDetectionStrategy,
    NgZone,
    ChangeDetectorRef,
} from "@angular/core";

import { IonSlides } from "@ionic/angular";
import { SwiperOptions } from "swiper";

import {
    SLIDE_CHANGE_TIME_SLOW,
    CALENDAR_MONTH_CHANGE_DIRECTIONS,
} from "@constants";

import { Vacation } from "@models";

import { CalendarService } from "@services/date/calendar.service";
import { SuprDateService } from "@services/date/supr-date.service";
import { UtilService } from "@services/util/util.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { Calendar } from "@types";

@Component({
    selector: "supr-calendar-month-slider",
    template: `
        <ion-slides
            class="slider"
            [options]="sliderOptions"
            (ionSlideWillChange)="onSlideChangeStart()"
            (ionSlideDidChange)="onSlideChangeEnd()"
            (ionSlideTouchEnd)="onSlideTouchEnd()"
        >
            <ion-slide
                *ngFor="let month of threeMonthDisplayData; trackBy: trackByFn"
            >
                <div class="sliderMonth">
                    <supr-calendar-day-bar></supr-calendar-day-bar>
                    <supr-calendar-month
                        [monthData]="month"
                        [selectedDate]="selectedDate"
                        [firstActiveDay]="firstActiveDay"
                        [startDate]="startDate"
                        [endDate]="endDate"
                        [vacation]="vacation"
                        [disablePastDays]="disablePastDays"
                        [enableToday]="enableToday"
                        [customDisabledDates]="customDisabledDates"
                        [customDisabledDateText]="customDisabledDateText"
                        [saObjectNameDateClick]="saObjectNameDateClick"
                        (updateDate)="updateDate.emit($event)"
                    ></supr-calendar-month>
                </div>
            </ion-slide>
        </ion-slides>
    `,
    styleUrls: ["../supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarMonthSliderComponent implements OnInit, OnChanges {
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() selectedDate: Calendar.DayData;
    @Input() currentDate: Calendar.DayData;
    @Input() firstActiveDay: Calendar.DayData;
    @Input() vacation: Vacation;
    @Input() disablePastDays: boolean;
    @Input() slideMonthDirection: number;
    @Input() enableToday: boolean;

    @Input() saObjectNameDateClick: string;
    @Input() monthSwipeSaName: string;

    @Input() customDisabledDates: Array<string> = [];
    @Input() customDisabledDateText = "";

    @Output() monthChanged: EventEmitter<Calendar.DayData> = new EventEmitter();
    @Output() updateDate: EventEmitter<Calendar.DayData> = new EventEmitter();

    @ViewChild(IonSlides, { static: true }) private slides: IonSlides;

    private sliderInit = false;
    private threeMonthCurrentData: Calendar.MonthData[];
    private onSlideEndCb: Function;

    public threeMonthDisplayData: Calendar.MonthData[];
    public sliderOptions: SwiperOptions;

    constructor(
        private calendarService: CalendarService,
        private suprDateService: SuprDateService,
        private utilService: UtilService,
        private ngZone: NgZone,
        private cdr: ChangeDetectorRef,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit() {
        this.sliderOptions = this.utilService.getSwiperOptions("month");
        this.updateDisplayData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["slideMonthDirection"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue < change.currentValue) {
            this.goToNextSlide();
        } else if (change.previousValue > change.currentValue) {
            this.goToPrevSlide();
        }
    }

    trackByFn(_: number, month: Calendar.MonthData): number {
        return month[1][1].month;
    }

    async onSlideTouchEnd() {
        const activeIndex = await this.getActiveIndex();
        this.sendMonthSwipeAnalytics(activeIndex);
    }

    async onSlideChangeStart() {
        if (!this.sliderInit) {
            return;
        }

        // Disable the swiper first
        this.disableSwipe();

        const activeIndex = await this.getActiveIndex();

        if (activeIndex < 1) {
            this.loadPreviousMonth();
        } else {
            this.loadNextMonth();
        }
    }

    onSlideChangeEnd() {
        if (!this.sliderInit) {
            this.sliderInit = true;

            // Fill back initial setup pending display data [hack for stop inital flickering]
            this.threeMonthDisplayData = [...this.threeMonthCurrentData];
            return;
        }

        // Handle slide end callbacks if any
        if (this.onSlideEndCb) {
            this.onSlideEndCb();
            this.onSlideEndCb = null;
        }

        // Finally enable the swipe
        this.enableSwipe();
    }

    private updateDisplayData() {
        this.ngZone.runOutsideAngular(() => {
            this.threeMonthCurrentData = this.getThreeMonthData(
                this.currentDate
            );
        });

        const displayMonth = this.threeMonthCurrentData[1];

        // First fill the starting two months with center slide to stop flickering.
        this.threeMonthDisplayData = this.threeMonthCurrentData.map(
            (actualMonth: Calendar.MonthData, index: number) =>
                index < 2 ? displayMonth : actualMonth
        );
    }

    private loadPreviousMonth() {
        const previousMonthFirstDay = this.suprDateService.getPreviousMonthDate(
            this.currentDate
        );

        // Update the parent
        this.monthChanged.emit(previousMonthFirstDay);

        // Load the updated 3month data
        this.ngZone.runOutsideAngular(() => {
            this.threeMonthCurrentData = this.getThreeMonthData(
                previousMonthFirstDay
            );

            // Update dom once the slide is finished
            this.onSlideEndCb = () => this.handlePreviousMonthSliderChange();
        });
    }

    private loadNextMonth() {
        const nextMonthFirstDay = this.suprDateService.getNextMonthDate(
            this.currentDate
        );

        // Update the parent
        this.monthChanged.emit(nextMonthFirstDay);

        // Load the updated 3month data
        this.ngZone.runOutsideAngular(() => {
            this.threeMonthCurrentData = this.getThreeMonthData(
                nextMonthFirstDay
            );

            // Update dom once the slide is finished
            this.onSlideEndCb = () => this.handleNextMonthSliderChange();
        });
    }

    private handlePreviousMonthSliderChange() {
        this.threeMonthDisplayData = [
            this.threeMonthDisplayData[0],
            ...this.threeMonthCurrentData.slice(1),
        ];

        // Adjust to center
        this.resetSlide();

        // In next frame, change the left slide data with actual data
        requestAnimationFrame(() => {
            this.threeMonthDisplayData = [...this.threeMonthCurrentData];
            this.cdr.detectChanges();
        });
    }

    private handleNextMonthSliderChange() {
        // Change data for only hidden months
        this.threeMonthDisplayData = [
            ...this.threeMonthCurrentData.slice(0, 2),
            this.threeMonthDisplayData[2],
        ];

        // Adjust to center
        this.resetSlide();

        // In next frame, change the right slide data with actual data
        requestAnimationFrame(() => {
            this.threeMonthDisplayData = [...this.threeMonthCurrentData];
            this.cdr.detectChanges();
        });
    }

    /*--------------------*
     * Helper methods     *
     *--------------------*/
    private getThreeMonthData(date: Calendar.DayData): Calendar.MonthData[] {
        return this.calendarService.loadThreeMonthDataForDate(date);
    }

    private async getActiveIndex(): Promise<number> {
        return this.slides.getActiveIndex();
    }

    private enableSwipe() {
        this.slides.lockSwipes(false);
    }

    private disableSwipe() {
        this.slides.lockSwipes(true);
    }

    private resetSlide() {
        this.enableSwipe();
        this.slides.slideTo(1, 0, false);
    }

    private goToNextSlide() {
        this.slides.slideTo(2, SLIDE_CHANGE_TIME_SLOW, true);
    }

    private goToPrevSlide() {
        this.slides.slideTo(0, SLIDE_CHANGE_TIME_SLOW, true);
    }

    private sendMonthSwipeAnalytics(index: number) {
        /* If user touches the slider without changing the month, index remains 1 */

        if (this.monthSwipeSaName && index !== 1) {
            this.analyticsService.trackSwipe({
                objectName: this.monthSwipeSaName,
                objectValue:
                    index < 1
                        ? CALENDAR_MONTH_CHANGE_DIRECTIONS.LEFT
                        : CALENDAR_MONTH_CHANGE_DIRECTIONS.RIGHT,
            });
        }
    }
}
