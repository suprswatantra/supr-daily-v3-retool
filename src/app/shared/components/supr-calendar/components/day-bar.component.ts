import { Component, ChangeDetectionStrategy } from "@angular/core";
import { DAY_NAMES } from "@constants";

@Component({
    selector: "supr-calendar-day-bar",
    template: `
        <div class="dayBar suprRow">
            <div
                class="dayBarName"
                *ngFor="let dayName of dayNames; trackBy: trackByFn"
            >
                {{ dayName }}
            </div>
        </div>
    `,
    styleUrls: ["../supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarDayBarComponent {
    dayNames = DAY_NAMES;

    trackByFn(index: number): number {
        return index;
    }
}
