import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { CALENDAR_MONTH_CHANGE_DIRECTIONS } from "@constants";
import { Calendar } from "@types";

@Component({
    selector: "supr-calendar-header",
    template: `
        <div class="header suprRow center">
            <div
                class="icon"
                saClick
                [saObjectName]="monthChangeSaName"
                saObjectValue="${CALENDAR_MONTH_CHANGE_DIRECTIONS.LEFT}"
                (click)="handleChangeMonth.emit('left')"
            >
                <supr-icon name="chevron_left"></supr-icon>
            </div>
            <supr-text type="subheading">
                {{ currentDate?.dateText | date: dateFormat }}
            </supr-text>
            <div
                class="icon"
                saClick
                [saObjectName]="monthChangeSaName"
                saObjectValue="${CALENDAR_MONTH_CHANGE_DIRECTIONS.RIGHT}"
                (click)="handleChangeMonth.emit('right')"
            >
                <supr-icon name="chevron_right"></supr-icon>
            </div>
        </div>
    `,
    styleUrls: ["../supr-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarHeaderComponent {
    @Input() currentDate: Calendar.DayData;
    @Input() dateFormat: string;

    @Input() monthChangeSaName: string;

    @Output() handleChangeMonth: EventEmitter<string> = new EventEmitter();
}
