import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Sku, Order } from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { OrderService } from "@services/shared/order.service";
import { ModalService } from "@services/layout/modal.service";
import { ScheduleService } from "@services/shared/schedule.service";

import { OrderRefundModalAdjustmentInfo } from "@types";

@Component({
    selector: "supr-order-history-refund-modal",
    templateUrl: "./supr-order-history-refund-modal.component.html",
    styleUrls: ["./supr-order-history-refund-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderHistoryRefundModalComponent implements OnInit {
    @Input() sku: Sku;
    @Input() order: Order;
    @Input() deliveredQtyText: string;
    @Input() scheduledQtyText: string;

    footerInfo: OrderRefundModalAdjustmentInfo;
    refundedAmountInfo: OrderRefundModalAdjustmentInfo;
    modalContentInfo: OrderRefundModalAdjustmentInfo[];

    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private modalService: ModalService,
        private orderService: OrderService,
        private scheduleService: ScheduleService
    ) {}

    ngOnInit() {
        this.setTexts();
    }

    goToCalendar() {
        const rescheduleDate = this.utilService.getNestedValue(
            this.order,
            "rescheduled_date"
        );
        this.modalService.closeModal();

        if (!this.dateService.isValidDateString(rescheduleDate)) {
            return;
        }

        this.scheduleService.scrollScheduleIntoView(rescheduleDate);
    }

    private setTexts() {
        this.footerInfo = this.orderService.getOrderRefundModalFooterInfo(
            this.order
        );
        this.refundedAmountInfo = this.orderService.getOrderRefundModalRefundInfo(
            this.order
        );
        this.modalContentInfo = this.orderService.getOrderRefundModalAdjustmentInfo(
            this.sku,
            this.order
        );
    }
}
