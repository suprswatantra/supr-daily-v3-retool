import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { FaqItem } from "@shared/models";

import { ICONS } from "@pages/support/constants";

@Component({
    selector: "supr-faq-item",
    template: `
        <div (click)="toggleItem()" *ngIf="faq?.question">
            <div class="suprRow spaceBetween item top">
                <div class="heading">
                    <supr-text type="regular14"> {{ faq?.question }}</supr-text>
                </div>
                <supr-icon
                    [class.up]="!folded"
                    name="${ICONS.CHEVRON_DOWN}"
                ></supr-icon>
            </div>

            <div class="description" [class.suprHide]="folded">
                <div class="divider4"></div>
                <supr-text type="caption"> {{ faq?.answer }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["supr-faq-item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemComponent {
    @Input() faq: FaqItem;

    folded = true;

    toggleItem() {
        this.folded = !this.folded;
    }
}
