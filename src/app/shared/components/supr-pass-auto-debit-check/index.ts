import { SuprPassAutoDebitOffer } from "./supr-pass-auto-debit-offer.component";
import { SuprPassAutoDebitCheckbox } from "./supr-pass-auto-debit-checkbox.component";

export const suprPassAutoDebitCheckboxComponents = [
    SuprPassAutoDebitCheckbox,
    SuprPassAutoDebitOffer,
];
