import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { SuprPassPlan, AutoDebitOffer } from "@shared/models";

@Component({
    selector: "supr-pass-auto-debit-offer",
    template: `
        <ng-container *ngIf="autoDebitOffer">
            <div class="suprRow autoDebitOffer">
                <ng-container *ngIf="isSelected; else offerText">
                    <supr-chip>
                        <supr-text type="paragraph">{{
                            autoDebitOffer?.offerAppliedText
                        }}</supr-text>
                    </supr-chip>
                    <div
                        *ngIf="
                            selectedPlan?.unitPrice !== autoDebitOffer?.offerMrp
                        "
                        class="suprRow"
                    >
                        <div class="spacer12"></div>
                        <supr-text class="offerMrp" type="captionBold">{{
                            autoDebitOffer?.offerMrp | rupee
                        }}</supr-text>
                        <div class="spacer8"></div>
                        <supr-text class="unitPrice" type="captionSemiBold">{{
                            selectedPlan?.unitPrice | rupee
                        }}</supr-text>
                    </div>
                </ng-container>
                <ng-template #offerText>
                    <supr-text class="offerText">{{
                        autoDebitOffer?.offerText
                    }}</supr-text>
                </ng-template>
            </div>
        </ng-container>
    `,
    styleUrls: ["./supr-pass-auto-debit-checkbox.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassAutoDebitOffer {
    @Input() isSelected: boolean;
    @Input() selectedPlan: SuprPassPlan;
    @Input() autoDebitOffer: AutoDebitOffer;
}
