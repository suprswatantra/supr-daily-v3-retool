import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";

import { SuprPassPlan, AutoDebitData } from "@shared/models";

@Component({
    selector: "supr-pass-auto-debit-checkbox",
    template: `
        <div
            class="autoDebitCheckboxBlock"
            *ngIf="autoDebitInfo && autoDebitInfo.selectionDataBlock"
            (click)="handleCheckboxToggle(!isSelected)"
        >
            <div class="divider28"></div>
            <div class="suprRow top">
                <div class="suprRow">
                    <ion-checkbox
                        [checked]="isSelected"
                        saClick
                        [saObjectName]="autoDebitToggleSAObjectName"
                        [saObjectValue]="!isSelected"
                    ></ion-checkbox>
                </div>

                <div class="spacer8"></div>
                <div class="suprColumn left">
                    <supr-text type="action14" class="title">{{
                        autoDebitInfo?.selectionDataBlock?.title
                    }}</supr-text>
                    <div class="divider4"></div>
                    <supr-text type="paragraph" class="description">{{
                        autoDebitInfo?.selectionDataBlock?.description
                    }}</supr-text>
                    <ng-container *ngIf="canShowOffersBlock()">
                        <div class="divider8"></div>
                        <supr-pass-auto-debit-offer
                            [isSelected]="isSelected"
                            [selectedPlan]="selectedPlan"
                            [autoDebitOffer]="getSelectedPlanAutoDebitOffer()"
                        ></supr-pass-auto-debit-offer>
                    </ng-container>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-pass-auto-debit-checkbox.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPassAutoDebitCheckbox implements OnInit {
    @Input() selectedPlan: SuprPassPlan;
    @Input() autoDebitInfo: AutoDebitData;
    @Output() handleAutoDebitSelection: EventEmitter<
        boolean
    > = new EventEmitter();

    isSelected = false;
    autoDebitToggleSAObjectName = "auto-debit-toggle";

    // Select auto debit by default
    ngOnInit() {
        this.isSelected = true;
        this.handleAutoDebitSelection.emit(true);
    }

    handleCheckboxToggle(bool: boolean) {
        this.isSelected = bool;
        this.handleAutoDebitSelection.emit(bool);
    }

    canShowOffersBlock() {
        const offer = this.getSelectedPlanAutoDebitOffer();
        if (offer) {
            return true;
        }
        return false;
    }

    private getSelectedPlanAutoDebitOffer() {
        const { selectionDataBlock } = this.autoDebitInfo || {
            selectionDataBlock: null,
        };
        const { id: planId } = this.selectedPlan || { id: null };
        if (selectionDataBlock && selectionDataBlock.offers && planId) {
            return selectionDataBlock.offers.find(
                (item) => item.planId === planId
            );
        }
        return null;
    }
}
