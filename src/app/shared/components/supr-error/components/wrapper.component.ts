import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { ErrorCurrentState } from "@store/error/error.state";
import { GlobalError } from "@types";

@Component({
    selector: "supr-error-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                modalName="${MODAL_NAMES.OOPS_ERROR}"
                (handleClose)="closeModal()"
                [saObjectName]="error?.errorType"
                [saObjectValue]="error?.subTitle"
                [saContext]="error?.errorUrl"
            >
                <supr-error-content
                    [error]="error"
                    (handleClick)="retry()"
                ></supr-error-content>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../supr-error.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorWrapperComponent implements OnChanges {
    @Input() error: GlobalError;
    @Input() errorState: ErrorCurrentState;
    @Output() handleClearError: EventEmitter<void> = new EventEmitter();
    @Output() handleRetry: EventEmitter<void> = new EventEmitter();

    showModal: boolean;

    constructor() {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["errorState"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.handleError();
        }
    }

    retry() {
        this.showModal = false;
        this.handleRetry.emit();
    }

    closeModal() {
        this.showModal = false;
        this.handleClearError.emit();
    }

    private handleError() {
        if (this.errorState !== ErrorCurrentState.NO_ERROR) {
            this.showModal = true;
        }
    }
}
