import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { Observable } from "rxjs";

import { ErrorAdapter } from "@shared/adapters/error.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";

import { ErrorCurrentState } from "@store/error/error.state";
import { GlobalError } from "@types";

@Component({
    selector: "supr-error",
    template: `
        <supr-error-wrapper
            [error]="error$ | async"
            [errorState]="errorState$ | async"
            (handleClearError)="clearError()"
            (handleRetry)="handleRetry()"
        >
        </supr-error-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorComponent implements OnInit {
    @Input() appLaunchDone: boolean;

    error$: Observable<GlobalError>;
    errorState$: Observable<ErrorCurrentState>;

    constructor(
        private adapter: ErrorAdapter,
        private userAdapter: UserAdapter
    ) {}

    ngOnInit() {
        this.error$ = this.adapter.error$;
        this.errorState$ = this.adapter.errorState$;
    }

    clearError() {
        this.adapter.clearError();
    }

    handleRetry() {
        this.adapter.clearError();
        if (!this.appLaunchDone) {
            this.fetchProfile();
        }
    }

    fetchProfile() {
        this.userAdapter.fetchProfileWithAddress();
    }
}
