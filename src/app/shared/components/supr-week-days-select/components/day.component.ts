import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-week-days-select-day",
    template: `
        <div class="day suprColumn center" [class.selected]="selected">
            <supr-text type="body" class="weekDay">
                {{ day | slice: 0:1 | uppercase }}
            </supr-text>
        </div>
    `,
    styleUrls: ["./day.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekDayComponent {
    @Input() day: string;
    @Input() selected = false;
}
