import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { DAY_LETTERS } from "@constants";
import { Frequency } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "@shared/components/supr-create-subscription/constants/analytics.constants";

@Component({
    selector: "supr-week-days-select",
    template: `
        <div class="wrapper suprRow" [class.spaceBetween]="spaceBetween">
            <ng-container
                *ngFor="
                    let day of dayLetters;
                    trackBy: trackByFn;
                    last as _lastDay
                "
            >
                <supr-week-days-select-day
                    [selected]="selectedDays && selectedDays[day]"
                    [day]="day"
                    (click)="handleSelect?.emit(day)"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .CREATE_SUBSCRIPTION_WEEKDAYS}"
                    [saObjectValue]="day"
                    [saContext]="saContext"
                ></supr-week-days-select-day>
                <ng-container *ngIf="!_lastDay">
                    <div class="spacer12"></div>
                </ng-container>
            </ng-container>
        </div>
    `,
    styleUrls: ["./supr-week-days-select.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekDaysSelectComponent {
    @Input() saContext: string;
    @Input() spaceBetween = false;
    @Input() selectedDays: Frequency = {};

    @Output() handleSelect: EventEmitter<string> = new EventEmitter();

    dayLetters = DAY_LETTERS;

    trackByFn(_: any, index: number): number {
        return index;
    }
}
