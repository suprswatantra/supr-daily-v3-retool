import { WeekDaysSelectComponent } from "./supr-week-days-select.component";
import { WeekDayComponent } from "./components/day.component";

export const weekDaysSelectComponents = [
    WeekDaysSelectComponent,
    WeekDayComponent,
];
