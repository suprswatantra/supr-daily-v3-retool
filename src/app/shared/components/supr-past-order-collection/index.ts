import { PastOrderCollectionComponent } from "./supr-past-order-collection.component";
import { WrapperComponent } from "./components/wrapper.component";
import { SkuStackedComponent } from "./components/sku-stacked.component";
import { ContentComponent } from "./components/content.component";
import { SkuContainer } from "./containers/sku.container";

export const pastOrderCollections = [
    PastOrderCollectionComponent,
    ContentComponent,
    WrapperComponent,
    SkuStackedComponent,
    SkuContainer,
];
