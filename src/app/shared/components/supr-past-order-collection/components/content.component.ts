import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";
import { Sku, Vacation, CartItem } from "@shared/models";
import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-past-order-collection-content",
    template: `
        <supr-past-ordercollection-sku-stacked
            *ngIf="sku"
            [sku]="sku"
            [vacation]="vacation"
            [cartCurrentState]="cartCurrentState"
            (updateItemInCart)="updateItemInCart.emit($event)"
        ></supr-past-ordercollection-sku-stacked>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent {
    @Input() sku: Sku;
    @Input() vacation: Vacation;
    @Input() cartCurrentState: CartCurrentState;

    @Output() updateItemInCart: EventEmitter<CartItem> = new EventEmitter();
}
