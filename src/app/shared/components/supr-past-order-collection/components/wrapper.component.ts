import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

@Component({
    selector: "supr-past-order-collection-wrapper",
    template: `
        <div class="wrapper">
            <div *ngFor="let skuId of skuList; index as position">
                <div class="wrapperCard">
                    <supr-past-order-collection-sku-container
                        [skuId]="skuId"
                        (handleScrollPosition)="
                            handleScrollPosition.emit($event)
                        "
                    ></supr-past-order-collection-sku-container>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() skuList: Array<number>;

    @Output() handleScrollPosition: EventEmitter<void> = new EventEmitter();
}
