import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";

import { Sku, CartItem, Vacation } from "@models";

import { QuantityService } from "@services/util/quantity.service";
import { MoengageService } from "@services/integration/moengage.service";
import { CartService } from "@services/shared/cart.service";
import { CartCurrentState } from "@store/cart/cart.state";

import { ADDED_TO_CART } from "../contants";
import { SA_OBJECT_NAMES } from "../contants/analytics.constants";

@Component({
    selector: "supr-past-ordercollection-sku-stacked",
    template: `
        <div
            class="sku"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.COLLECTION_ITEM}"
            [saObjectValue]="sku?.id"
        >
            <div
                class="suprColumn spaceBetween"
                (click)="updateCartItem()"
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_PRODUCT}"
                [saObjectValue]="sku?.id"
            >
                <div class="skuSection suprColumn">
                    <supr-image
                        lazyLoad="false"
                        [src]="sku?.image?.fullUrl"
                        [image]="sku?.image"
                        [imgWidth]="${CLODUINARY_IMAGE_SIZE.PREVIEW.WIDTH}"
                        [imgHeight]="${CLODUINARY_IMAGE_SIZE.PREVIEW.HEIGHT}"
                    ></supr-image>
                    <div class="divider12"></div>
                    <supr-text type="body" class="name" [truncate]="true">
                        {{ sku?.sku_name }} ({{ displayQty }})
                    </supr-text>
                    <div class="divider4"></div>

                    <supr-product-tile-price
                        [unitPrice]="sku?.unit_price"
                        [unitMrp]="sku?.unit_mrp"
                    ></supr-product-tile-price>
                </div>
            </div>

            <div
                class="skuAdd"
                [class.skuAddedToCart]="isAddButtonClick"
                (click)="updateCartItem()"
                saClick
                saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_PRODUCT}"
                [saObjectValue]="sku?.id"
            >
                <ng-container *ngIf="!isAddButtonClick; else addedToCart">
                    <div class="skuAddIcon">
                        <supr-icon name="add"></supr-icon>
                    </div>
                </ng-container>
                <ng-template #addedToCart>
                    <div class="suprColumn center">
                        <supr-icon name="approve"></supr-icon>
                        <supr-text type="body">
                            ${ADDED_TO_CART}
                        </supr-text>
                    </div>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["../styles/sku.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkuStackedComponent implements OnInit {
    @Input() sku: Sku;
    @Input() vacation: Vacation;
    @Input() cartCurrentState: CartCurrentState;

    @Output() updateItemInCart: EventEmitter<CartItem> = new EventEmitter();

    displayQty: string;
    isAddButtonClick = false;

    constructor(
        private qtyService: QuantityService,
        private cartService: CartService,
        private moengageService: MoengageService
    ) {}

    ngOnInit() {
        this.setQty();
    }

    private setQty() {
        this.displayQty = this.qtyService.toText(this.sku);
    }

    updateCartItem() {
        this.isAddButtonClick = true;

        setTimeout(() => {
            let item: CartItem;
            item = this.cartService.getCartItemFromSku(
                this.sku,
                1,
                this.vacation
            );
            this.updateItemInCart.emit(item);
            if (this.sku) {
                this.moengageService.trackAddToCart({
                    sku_id: this.sku.id,
                    sku_price: this.sku.unit_price,
                    product_identifier: this.sku.product_identifier,
                });
            }
        }, 200);
    }
}
