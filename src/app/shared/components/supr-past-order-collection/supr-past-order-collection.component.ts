import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TEXTS } from "@pages/cart/constants";

@Component({
    selector: "supr-past-order-collection",
    template: `
        <div *ngIf="pastOrderSkuList.length > 0" id="pastCollectionCart">
            <supr-section-header
                icon="${TEXTS.PAST_ORDER_HEADER.icon}"
                title="${TEXTS.PAST_ORDER_HEADER.title}"
                subtitle="${TEXTS.PAST_ORDER_HEADER.subtitle}"
            >
            </supr-section-header>

            <div class="itemsWrapper">
                <supr-past-order-collection-wrapper
                    [skuList]="pastOrderSkuList"
                    (handleScrollPosition)="handleScroll($event)"
                ></supr-past-order-collection-wrapper>
            </div>
        </div>
    `,
    styleUrls: ["./styles/supr-past-order-collection.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PastOrderCollectionComponent {
    @Input() pastOrderSkuList: Array<number>;
    @Input() type = "cart";

    @Output() handleScrollPosition: EventEmitter<string> = new EventEmitter();

    handleScroll() {
        this.handleScrollPosition.emit("pastCollectionCart");
    }
}
