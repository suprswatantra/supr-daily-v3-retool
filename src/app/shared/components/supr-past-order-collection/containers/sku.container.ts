import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Observable } from "rxjs";

import { Sku, Vacation, CartItem } from "@models";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CartCurrentState } from "@store/cart/cart.state";

@Component({
    selector: "supr-past-order-collection-sku-container",
    template: `
        <supr-past-order-collection-content
            [sku]="sku$ | async"
            [vacation]="vacation$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            (updateItemInCart)="updateItemInCart($event)"
        ></supr-past-order-collection-content>
    `,
    styleUrls: ["../styles/wrapper.component.scss"],
})
export class SkuContainer implements OnInit {
    @Input() skuId: number;

    @Output() handleScrollPosition: EventEmitter<void> = new EventEmitter();

    sku$: Observable<Sku>;

    cartCurrentState$: Observable<CartCurrentState>;
    vacation$: Observable<Vacation>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.sku$ = this.adapter.getSkuById(this.skuId);
        this.vacation$ = this.adapter.vacation$;
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
    }

    updateItemInCart(cartItem: CartItem) {
        this.adapter.updateItemInCart(cartItem, true);
        this.handleScrollPosition.emit();
    }
}
