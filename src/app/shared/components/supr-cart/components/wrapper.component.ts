import {
    Component,
    Input,
    OnInit,
    OnChanges,
    ChangeDetectionStrategy,
    SimpleChanges,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { AppsFlyerService } from "@services/integration/appsflyer.service";
import { RouterService } from "@services/util/router.service";
import { ModalService } from "@services/layout/modal.service";
import { MoengageService } from "@services/integration/moengage.service";
import { WalletService } from "@services/shared/wallet.service";

import { CartMini } from "@types";

@Component({
    selector: "supr-cart-wrapper",
    template: `
        <div class="cart" [class.show]="showCartClass && showCart">
            <supr-sticky-wrapper>
                <div
                    class="cartWrapper suprRow ion-activatable"
                    (click)="goToCartPage()"
                    saClick
                    [saObjectValue]="cartId"
                    saImpression
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .FLOATING_CART}"
                >
                    <ion-ripple-effect></ion-ripple-effect>
                    <supr-icon class="bag" name="cart"></supr-icon>
                    <div class="spacer16"></div>
                    <div class="cartLine"></div>
                    <div class="spacer16"></div>
                    <div>
                        <div class="suprColumn">
                            <supr-text type="paragraph">
                                {{ cartItemCount }}
                            </supr-text>
                            <div class="divider4"></div>
                            <supr-text type="subheading">
                                {{ cartTotal | rupee: 2 }}
                            </supr-text>
                        </div>
                    </div>
                    <div class="cartProceed suprRow">
                        <supr-text type="body">Proceed to cart</supr-text>
                        <div class="spacer8"></div>
                        <div class="spacer4"></div>
                        <supr-icon name="chevron_right"></supr-icon>
                    </div>
                </div>
            </supr-sticky-wrapper>
        </div>
    `,
    styleUrls: ["./../supr-cart.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartWrapperComponent implements OnInit, OnChanges {
    @Input() cart: CartMini;
    @Input() cartId: string;
    @Input() showCart: string;
    @Input() isAnonymousUser: boolean;

    showCartClass = false;
    cartTotal: number;
    cartItemCount: string;

    constructor(
        private modalService: ModalService,
        private moengageService: MoengageService,
        private routerService: RouterService,
        private walletService: WalletService,
        private appsFlyerService: AppsFlyerService
    ) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["cart"];
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.initData();
        }
    }

    goToCartPage() {
        setTimeout(() => {
            this.sendAnalyticsEvents();
            this.routerService.goToCartPage();
            this.closeProductTileModal();
        }, 100);
    }

    private closeProductTileModal() {
        setTimeout(() => this.modalService.closeModal(), 300);
    }

    private initData() {
        if (!this.cart || this.isAnonymousUser) {
            return;
        }

        if (this.cart.itemCount <= 0) {
            this.showCartClass = false;
        } else if (this.cart.itemCount > 0) {
            this.showCartClass = true;
            this.assignCartValues();
        }
    }

    private assignCartValues() {
        this.cartTotal = this.cart.cartTotal;
        this.cartItemCount = `${this.cart.itemCount} item${
            this.cart.itemCount > 1 ? "s" : ""
        }`;
    }

    private async sendAnalyticsEvents() {
        this.appsFlyerService.trackInitiatedCheckout(this.cart.items);

        const walletBalance = this.walletService.getWalletBalance();
        const suprCreditsBalance = this.walletService.getSuprCreditsBalance();

        this.moengageService.trackInitiateCheckout(
            this.cart,
            walletBalance,
            suprCreditsBalance
        );
    }
}
