import { CartComponent } from "./supr-cart.component";
import { CartWrapperComponent } from "./components/wrapper.component";

export const cartComponents = [CartComponent, CartWrapperComponent];
