import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";

import { CartMini } from "@types";

@Component({
    selector: "supr-cart",
    template: `
        <supr-cart-wrapper
            [cart]="cart$ | async"
            [cartId]="cartId$ | async"
            [showCart]="showCart$ | async"
            [isAnonymousUser]="isAnonymousUser$ | async"
        ></supr-cart-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartComponent implements OnInit {
    cart$: Observable<CartMini>;
    cartId$: Observable<string>;
    showCart$: Observable<boolean>;
    isAnonymousUser$: Observable<boolean>;

    constructor(private adapter: Adapter, private userAdapter: UserAdapter) {}

    ngOnInit() {
        this.cart$ = this.adapter.miniCart$;
        this.cartId$ = this.adapter.cartId$;
        this.showCart$ = this.adapter.showCart$;
        this.isAnonymousUser$ = this.userAdapter.isAnonymousUser$;
    }
}
