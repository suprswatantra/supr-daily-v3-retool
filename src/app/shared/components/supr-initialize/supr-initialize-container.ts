import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { MiscAdapter as Adapter } from "@shared/adapters";

@Component({
    selector: "supr-initialize-container",
    template: `
        <supr-initialize
            [appInitDone]="appInitDone$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
        ></supr-initialize>
    `,
})
export class InitializeContainer implements OnInit {
    appInitDone$: Observable<boolean>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.appInitDone$ = this.adapter.appInitDone$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }
}
