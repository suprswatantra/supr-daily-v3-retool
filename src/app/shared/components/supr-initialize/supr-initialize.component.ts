import {
    Component,
    Input,
    OnChanges,
    SimpleChanges,
    ChangeDetectionStrategy,
    SimpleChange,
} from "@angular/core";

import { InitializeService } from "@services/util/initialize.service";

@Component({
    selector: "supr-initialize",
    template: `
        <supr-error [appLaunchDone]="appLaunchDone"></supr-error>
        <supr-internet-monitor></supr-internet-monitor>

        <div *ngIf="load">
            <supr-pn-container></supr-pn-container>
            <supr-app-update></supr-app-update>

            <supr-pause></supr-pause>
            <supr-address-retro></supr-address-retro>
            <supr-generic-nudge></supr-generic-nudge>
            <supr-app-rating></supr-app-rating>
            <supr-exit-cart></supr-exit-cart>
            <supr-campaign-popup></supr-campaign-popup>
            <supr-feedback></supr-feedback>
            <supr-checkout-disabled></supr-checkout-disabled>
            <supr-block-access-nudge></supr-block-access-nudge>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InitializeComponent implements OnChanges {
    @Input() appInitDone: boolean;
    @Input() appLaunchDone: boolean;

    constructor(private initializeService: InitializeService) {}

    load = false;

    ngOnChanges(changes: SimpleChanges) {
        this.handleAppInitDoneChange(changes["appInitDone"]);
    }

    private handleAppInitDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.appInitDone) {
            this.initializeService.delayedInitialize();
            this.load = true;
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue &&
            change.currentValue
        );
    }
}
