import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
    OnInit,
} from "@angular/core";
import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-page-header",
    template: `
        <supr-sticky-wrapper [class.noBg]="!withBackground">
            <div class="wrapper">
                <supr-back-button
                    (click)="onClick()"
                    [saObjectValue]="saObjectValueBack"
                ></supr-back-button>
                <div class="content">
                    <ng-content></ng-content>
                </div>
                <div class="wrapperBottomLine" *ngIf="withBackground"></div>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["./supr-page-header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageHeaderComponent implements OnInit {
    @Input() saObjectValueBack: any;
    @Input() withBackground = true;

    @Output() handleBackButtonClick: EventEmitter<void> = new EventEmitter();

    private routeId: string;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.routeId = this.utilService.uuid();
    }

    onClick() {
        if (
            this.handleBackButtonClick &&
            this.handleBackButtonClick.observers.length > 0
        ) {
            this.handleBackButtonClick.emit();
        } else {
            this.routerService.goBack(this.routeId);
        }
    }
}
