import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    FREQUENCY_OPTIONS,
    FREQUENCY_OPTIONS_MAP,
    ANALYTICS_OBJECT_NAMES,
    WEEK_DAYS_SELECTION_FORMAT,
} from "@constants";

import { UtilService } from "@services/util/util.service";

import { Frequency, Segment } from "@types";

@Component({
    selector: "supr-subscription-create-frequency-modal",
    template: `
        <div class="wrapper">
            <supr-text class="headerText" type="body">Choose Days</supr-text>
            <div class="divider12"></div>
            <supr-week-days-select
                [spaceBetween]="true"
                [selectedDays]="frequency"
                (handleSelect)="onFrequencyChange($event)"
            ></supr-week-days-select>
            <div class="divider24"></div>
            <div class="suprRow spaceBetween">
                <div
                    saClick
                    [saObjectValue]="frequencyOption.key"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .FREQUENCY_MODAL_REPEAT_DAYS}"
                    class="frequencyWrapper"
                    *ngFor="
                        let frequencyOption of frequencyOptions | keyvalue;
                        let _i = index;
                        trackBy: trackByFn
                    "
                    [class.selected]="selectedIndex === _i"
                    (click)="onFrequencyButtonClick(_i)"
                >
                    <supr-text type="body">{{
                        frequencyOption.value.TEXT
                    }}</supr-text>
                </div>
            </div>
            <div class="divider32"></div>
            <supr-button
                [loading]="loading"
                [disabled]="disableConfirm"
                [saContext]="skuId"
                [saObjectValue]="saObjectValue"
                [saContextList]="_saContextList"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .FREQUENCY_MODAL_CONFIRM_REPEAT}"
                (handleClick)="onFrequencyConfirmation()"
            >
                CONFIRM
            </supr-button>
        </div>
    `,
    styleUrls: ["./supr-subscription-frequency-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSubscriptionFrequencyModalComponent implements OnInit {
    @Input() skuId: number;
    @Input() loading = false;
    @Input() set frequency(selectedDays: Frequency) {
        this._frequency = selectedDays;
        this.setButtonState();
        this.setFrequencyText();
    }

    get frequency(): Frequency {
        return this._frequency;
    }

    @Input() saObjectValue: number;

    @Output() handleFrequencyConfirmation: EventEmitter<
        Frequency
    > = new EventEmitter();

    disableConfirm = true;
    selectedIndex: number;
    _saContextList: Segment.ContextListItem[] = [];

    readonly frequencyOptions = FREQUENCY_OPTIONS_MAP;

    private _frequency: Frequency;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setAnalyticsData();
    }

    onFrequencyChange(day: string) {
        if (this._frequency) {
            this._frequency = {
                ...this._frequency,
                [day]: !this._frequency[day],
            };
        }

        this.setButtonState();
        this.setFrequencyText();
        this.setAnalyticsData();
    }

    onFrequencyConfirmation() {
        if (!this.disableConfirm) {
            this.handleFrequencyConfirmation.emit(this._frequency);
        }
    }

    onFrequencyButtonClick(index: number) {
        this.selectedIndex = index;

        switch (index) {
            case 0:
                this._frequency = FREQUENCY_OPTIONS_MAP.daily.FREQUENCY;
                break;
            case 1:
                this._frequency = FREQUENCY_OPTIONS_MAP.weekdays.FREQUENCY;
                break;
            case 2:
                this._frequency = FREQUENCY_OPTIONS_MAP.weekends.FREQUENCY;
                break;
        }

        this.setAnalyticsData();
    }

    trackByFn(index: number) {
        return index;
    }

    getSaOContextList(): string {
        if (this.selectedIndex < 0) {
            return;
        }

        return FREQUENCY_OPTIONS[this.selectedIndex].type;
    }

    private setAnalyticsData() {
        if (this.selectedIndex < 0) {
            return;
        }

        this._saContextList = [
            {
                name: "repeat",
                value: FREQUENCY_OPTIONS[this.selectedIndex].type,
            },
        ];
    }

    private setButtonState() {
        const selectedDays = this.getSelectedDays();
        this.disableConfirm = selectedDays.length ? false : true;
    }

    private getSelectedDays(): string[] {
        const days = [];

        for (const day in this._frequency) {
            if (this._frequency[day]) {
                days.push(day);
            }
        }

        return days;
    }

    private setFrequencyText() {
        const frequencyFormat = this.utilService.getWeekDaysFormatFromDays(
            this._frequency
        );

        switch (frequencyFormat) {
            case WEEK_DAYS_SELECTION_FORMAT.DAILY:
                this.selectedIndex = 0;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.WEEK_DAYS:
                this.selectedIndex = 1;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.WEEK_ENDS:
                this.selectedIndex = 2;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.CUSTOM:
                this.selectedIndex = 3;
                break;
            default:
                this.selectedIndex = -1;
                break;
        }
    }
}
