import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { CartMeta } from "@models";
import { CheckoutInfo } from "@shared/models/user.model";

import { RouterService } from "@services/util/router.service";
import { UtilService } from "@services/util/util.service";
import { CartService } from "@services/shared/cart.service";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-cart-footer-dep",
    template: `
        <supr-page-footer>
            <supr-button
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                (handleClick)="_handleClick()"
            >
                <supr-text
                    type="body"
                    saImpression
                    [saObjectName]="saObjectName"
                    *ngIf="amountToAdd > 0; else payText"
                >
                    Add {{ amountToAdd | rupee }} to Supr Wallet
                </supr-text>

                <ng-template #payText>
                    <supr-text
                        type="body"
                        *ngIf="amountToPay > 0; else payFromSuprCredits"
                        saImpression
                        [saObjectName]="saObjectName"
                    >
                        Pay {{ amountToPay | rupee }} from Supr Wallet
                    </supr-text>
                </ng-template>

                <ng-template #payFromSuprCredits>
                    <supr-text
                        type="body"
                        saImpression
                        [saObjectName]="saObjectName"
                    >
                        Pay {{ suprCreditsUsed | rupee }} from Supr Credits
                    </supr-text>
                </ng-template>
            </supr-button>
        </supr-page-footer>
    `,
    styleUrls: ["./supr-cart-footer-dep.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CartFooterDepComponent implements OnChanges {
    @Input() amountToAdd: number;
    @Input() amountToPay: number;
    @Input() suprCreditsUsed: number;
    @Input() cartMeta: CartMeta;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() fromRechargeSubscriptionPage: boolean;
    @Input() fromSuprPassPage: boolean;
    @Input() isEligibleForSuprPass: boolean;

    @Output() handlePay: EventEmitter<void> = new EventEmitter();

    saObjectName: string;
    saObjectValue: number;

    constructor(
        private routerService: RouterService,
        private utilService: UtilService,
        private cartService: CartService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes["amountToAdd"]) {
            this.setAnalyticsData(changes["amountToAdd"]);
        }
    }

    _handleClick() {
        if (this.cartService.isCheckoutDisabled(this.userCheckoutInfo)) {
            return;
        }

        const queryParams = {
            amount: this.amountToAdd,
            auto: true,
            rechargeCoupon: "",
            cartPaymentCoupon: "",
        };

        if (this.amountToAdd > 0) {
            if (this.wasRechargeCouponUsed()) {
                queryParams.rechargeCoupon = this.utilService.getNestedValue(
                    this.cartMeta,
                    "discount_info.coupon_code",
                    ""
                );
            }

            if (this.wasPaymentCouponUsed()) {
                queryParams.cartPaymentCoupon = this.utilService.getNestedValue(
                    this.cartMeta,
                    "discount_info.coupon_code",
                    ""
                );
            }

            if (this.canShowSuprPassPrompt()) {
                this.routerService.goToSuprPassPage({ queryParams });
                return;
            }

            this.routerService.goToWalletPage({ queryParams });
        } else {
            if (this.canShowSuprPassPrompt()) {
                this.routerService.goToSuprPassPage({ queryParams });
                return;
            }

            this.handlePay.emit();
        }
    }

    private canShowSuprPassPrompt(): boolean {
        const showSuprPass = this.utilService.getNestedValue(
            this.cartMeta,
            "show_supr_pass_prompt",
            false
        );

        return (
            !this.fromSuprPassPage &&
            !this.fromRechargeSubscriptionPage &&
            this.isEligibleForSuprPass &&
            showSuprPass
        );
    }

    private wasRechargeCouponUsed(): boolean {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.is_recharge_coupon",
            false
        );
    }

    private wasPaymentCouponUsed(): boolean {
        return this.utilService.getNestedValue(
            this.cartMeta,
            "discount_info.is_payment_coupon",
            false
        );
    }

    private setAnalyticsData(change: SimpleChange) {
        const { currentValue } = change;
        if (!isNaN(currentValue)) {
            if (currentValue > 0) {
                this.saObjectName = SA_OBJECT_NAMES.CLICK.ADD_TO_WALLET;
                this.saObjectValue = currentValue;
            } else {
                this.saObjectName = SA_OBJECT_NAMES.CLICK.PAY_FROM_WALLET;
                this.saObjectValue = currentValue;
            }
        }
    }
}
