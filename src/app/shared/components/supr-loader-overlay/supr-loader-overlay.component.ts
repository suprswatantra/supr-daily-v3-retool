import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
} from "@angular/core";

import { MODAL_TYPES } from "@constants";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-loader-overlay",
    template: `
        <div class="loader">
            <supr-modal
                [modalType]="type"
                [backdropDismiss]="false"
                class="suprColumn center"
            >
                <div class="content suprRow">
                    <supr-loader></supr-loader>
                    <div class="spacer16"></div>
                    <ng-content></ng-content>
                </div>
            </supr-modal>
        </div>
    `,
    styleUrls: ["./supr-loader-overlay.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprLoaderOverlayComponent implements OnInit, OnDestroy {
    type = MODAL_TYPES.NO_WRAPPER;

    constructor(private routerService: RouterService) {}

    ngOnInit() {
        this.routerService.setIgnoreBackButton(true);
    }

    ngOnDestroy() {
        this.routerService.setIgnoreBackButton(false);
    }
}
