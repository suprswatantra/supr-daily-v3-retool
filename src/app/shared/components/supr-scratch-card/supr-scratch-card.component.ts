import {
    Component,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
    NgZone,
    OnDestroy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { fromEvent, Observable, Subscription } from "rxjs";

import { TextFragment } from "@shared/models";

import { UtilService } from "@services/util/util.service";

import { ConfigurableIcon } from "@types";

import {
    SCRATCH_CARD_VALUES,
    TOUCH_EVENTS,
    IMAGE_SMOOTHING_QUALITY,
} from "./constants";
import { ANALYTICS_OBJECT_NAMES } from "./constants/analytics.constants";

export interface Coordinates {
    x: number;
    y: number;
}

@Component({
    selector: "supr-scratch-card",
    template: `
        <div class="scratchCardContainer">
            <div class="canvasContainer">
                <canvas class="canvas" #canvasEl></canvas>
            </div>

            <div class="msgContainer" [class.withError]="!!icon">
                <div class="msgContent">
                    <ng-container
                        *ngIf="
                            textFragment?.text && !isFetchingScratchCardBenefit
                        "
                    >
                        <ng-container *ngIf="icon?.iconName">
                            <div
                                class="suprRow center"
                                saImpression
                                saObjectName="${ANALYTICS_OBJECT_NAMES
                                    .IMPRESSION.REVEALED_WITH_ERROR}"
                            >
                                <supr-configurable-icon
                                    [config]="icon"
                                ></supr-configurable-icon>
                            </div>
                            <div class="divider8"></div>
                        </ng-container>
                        <div
                            class="suprRow center"
                            saImpression
                            saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                                .REVEALED}"
                        >
                            <supr-text-fragment
                                [textFragment]="textFragment"
                            ></supr-text-fragment>
                        </div>
                    </ng-container>

                    <ng-container *ngIf="isFetchingScratchCardBenefit">
                        <div class="suprRow center">
                            <supr-loader></supr-loader>
                        </div>
                    </ng-container>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-scratch-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScratchCardComponent implements OnInit, OnDestroy {
    @Input() imageUrl: string;
    @Input() icon: ConfigurableIcon;
    @Input() textFragment: TextFragment;
    @Input() isFetchingScratchCardBenefit: boolean;

    @Output()
    handleFetchScratchCardBenefit: EventEmitter<void> = new EventEmitter();

    @ViewChild("canvasEl", { static: true }) canvasEl: ElementRef;

    private canvasWidth = SCRATCH_CARD_VALUES.CANVAS_WIDTH;
    private canvasHeight = SCRATCH_CARD_VALUES.CANVAS_HEIGHT;
    private ctx: CanvasRenderingContext2D;
    private image: HTMLImageElement;
    private brush: HTMLImageElement;
    private isDrawing: boolean;
    private lastPoint: Coordinates;

    private touchStart$: Observable<Event>;
    private touchMove$: Observable<Event>;
    private touchEnd$: Observable<Event>;

    private touchStartSubscription: Subscription;
    private touchMoveSubscription: Subscription;
    private touchEndSubscription: Subscription;

    constructor(private ngZone: NgZone, private utilService: UtilService) {}

    ngOnInit() {
        this.initScratchCard();
    }

    ngOnDestroy() {
        this.stopObservingTouchEvents();
    }

    /**
     * UnSubscribe from existing subscription if any subs running
     *
     * @private
     * @memberof ScratchCardComponent
     */
    private stopObservingTouchEvents() {
        if (this.touchStartSubscription) {
            this.touchStartSubscription.unsubscribe();
        }
        if (this.touchMoveSubscription) {
            this.touchMoveSubscription.unsubscribe();
        }
        if (this.touchEndSubscription) {
            this.touchEndSubscription.unsubscribe();
        }
    }

    /**
     * Initialize Canvas, acknowledge props like width, height
     * load images and brush, the current brush size is 20X20
     *
     * IMP: Running these things out of Angular for minimised change detection calls
     *
     * @private
     * @returns
     * @memberof ScratchCardComponent
     */
    private initScratchCard() {
        this.setCanvasProps();
        this.setImageProps();

        if (!this.getCanvas()) {
            return;
        }

        this.ngZone.runOutsideAngular(() => {
            this.startObservingTouchEvents();
        });
    }

    /**
     * Create canvas and set its context.
     * canvas props like width, height start reflicting from here on
     *
     * @private
     * @returns
     * @memberof ScratchCardComponent
     */
    private setCanvasProps() {
        const canvas: any = this.getCanvas();

        if (!canvas) {
            return;
        }

        this.ctx = canvas.getContext(SCRATCH_CARD_VALUES.CANVAS_DIMENSION);
        canvas.width = this.canvasWidth;
        canvas.height = this.canvasHeight;
    }

    /**
     * Sets image and brush props
     * Maps the image to canvas.
     * Note: Canvas and svg dont go well. Make sure img is only png/jpeg/jpg
     *
     * Improvement Scope[Nishant]: currently using img as brush
     * can improve logic to use markup as brush
     *
     * Better scaling function can be implemented to draw image in orginal size first
     * and then scale it to map to canvas.
     *
     * Currently both canvas and image are scaled as 1x image
     * For something like 4x, scaling function would help
     *
     * @private
     * @memberof ScratchCardComponent
     */
    private setImageProps() {
        this.brush = new Image();
        this.brush.src = SCRATCH_CARD_VALUES.DEFAULT_BRUSH_URL;

        this.image = new Image();
        this.image.crossOrigin = SCRATCH_CARD_VALUES.CORS;
        this.image.src = this.imageUrl;

        this.image.onload = (_event) => {
            this.ctx.imageSmoothingEnabled = true;
            this.ctx.imageSmoothingQuality = IMAGE_SMOOTHING_QUALITY.HIGH;
            this.ctx.drawImage(
                this.image,
                0,
                0,
                this.image.width,
                this.image.height,
                0,
                0,
                this.canvasWidth,
                this.canvasHeight
            );
        };
    }

    /**
     * Return Canvas DOM if found
     *
     * @private
     * @returns {HTMLCanvasElement}
     * @memberof ScratchCardComponent
     */
    private getCanvas(): HTMLCanvasElement {
        return this.utilService.getNestedValue(
            this.canvasEl,
            "nativeElement",
            null
        );
    }

    /**
     * Start watching native touch events
     *
     * Not added event listeners directly as they fail to bind to a handler sometimes
     * Converting touch events to streams(Observales). Event handler is attached to subscribe
     *
     * @private
     * @memberof ScratchCardComponent
     */
    private startObservingTouchEvents() {
        this.touchStart$ = fromEvent(
            this.getCanvas(),
            TOUCH_EVENTS.TOUCH_START
        );
        this.touchMove$ = fromEvent(this.getCanvas(), TOUCH_EVENTS.TOUCH_MOVE);
        this.touchEnd$ = fromEvent(this.getCanvas(), TOUCH_EVENTS.TOUCH_END);

        this.touchStartSubscription = this.touchStart$.subscribe(
            (e: TouchEvent) => {
                this.handleTouchStart(e);
            }
        );
        this.touchMoveSubscription = this.touchMove$.subscribe(
            (e: TouchEvent) => {
                this.handleTouchMove(e);
            }
        );
        this.touchEndSubscription = this.touchEnd$.subscribe(
            (e: TouchEvent) => {
                this.handleTouchEnd(e);
            }
        );
    }

    /**
     * Sets the lastpoint for further calculations on touch move
     *
     * @private
     * @param {TouchEvent} e
     * @returns
     * @memberof ScratchCardComponent
     */
    private handleTouchStart(e: TouchEvent) {
        if (!this.getCanvas()) {
            return;
        }

        this.isDrawing = true;
        this.lastPoint = this.getTouchCoordinates(e);
    }

    /**
     * Gets the current point the distance of the DRAG and the angle
     * to clear out that space from canvas using the image brush. The total diameter of the brush is 20px
     * for every touch move keep clearing the canvas using the brush
     *
     * Update the last point on clearing the canvas. starts again from lastpoint for futher touch moves
     *
     * Call a method to get the clear percent. Event emittion is based on an offset percent
     *
     * @private
     * @param {TouchEvent} e
     * @returns
     * @memberof ScratchCardComponent
     */
    private handleTouchMove(e: TouchEvent) {
        if (!this.getCanvas()) {
            return;
        }

        if (!this.isDrawing) {
            return;
        }

        e.preventDefault();

        const currentPoint = this.getTouchCoordinates(e);
        const dist = this.utilService.distanceBetweenCoordinates(
            this.lastPoint,
            currentPoint
        );
        const angle = this.utilService.angleBetweenCoordinates(
            this.lastPoint,
            currentPoint
        );
        let x: number;
        let y: number;

        for (let i = 0; i < dist; i++) {
            x =
                this.lastPoint.x +
                Math.sin(angle) * i -
                SCRATCH_CARD_VALUES.ANGLE_DELTA;
            y =
                this.lastPoint.y +
                Math.cos(angle) * i -
                SCRATCH_CARD_VALUES.ANGLE_DELTA;
            this.ctx.globalCompositeOperation =
                SCRATCH_CARD_VALUES.GLOBAL_COMPOSITE_OPERATOR;
            this.ctx.drawImage(this.brush, x, y);
        }

        this.lastPoint = currentPoint;

        this.handlePercentage(
            this.getFilledInPixels(SCRATCH_CARD_VALUES.DEFAULT_STRIDE)
        );
    }

    /**
     * Stop clearing the canvas on touchend for a session of touches[start/move]
     *
     * @private
     * @param {TouchEvent} _e
     * @returns
     * @memberof ScratchCardComponent
     */
    private handleTouchEnd(_e: TouchEvent) {
        if (!this.getCanvas()) {
            return;
        }

        this.isDrawing = false;
    }

    /**
     * Calculate the correct corrdinates of current touch
     *
     * @private
     * @param {TouchEvent} e
     * @returns
     * @memberof ScratchCardComponent
     */
    private getTouchCoordinates(e: TouchEvent): Coordinates {
        if (!e) {
            return null;
        }

        let offsetX = 0;
        let offsetY = 0;
        let mx;
        let my;

        let canvas: any = this.getCanvas();

        if (canvas.offsetParent !== undefined) {
            do {
                offsetX += canvas.offsetLeft;
                offsetY += canvas.offsetTop;
            } while ((canvas = canvas.offsetParent));
        }

        mx = e.touches[0].clientX - offsetX;
        my = e.touches[0].clientY - offsetY;

        return { x: mx, y: my };
    }

    /**
     * Calculates the total canvas area cleared keeping the initail canvas as base(which is equal to image dimesnsions)
     *
     * TODO: [Improvement, Nishant] Only test every `stride` pixel. `stride`x faster. This might lead to some inaccuracy
     *
     * @private
     * @param {number} stride
     * @returns {number}
     * @memberof ScratchCardComponent
     */
    private getFilledInPixels(stride: number): number {
        if (!stride || stride < 1) {
            stride = 1;
        }

        const pixels: ImageData = this.ctx.getImageData(
            0,
            0,
            this.canvasWidth,
            this.canvasHeight
        );
        const pdata = pixels.data;
        const l = pdata.length;
        const total = l / stride;
        let count = 0;

        // Iterate over all pixels
        for (let i = (count = 0); i < l; i += stride) {
            if (parseInt(String(pdata[i]), SCRATCH_CARD_VALUES.RADIX) === 0) {
                count++;
            }
        }

        return Math.round((count / total) * 100);
    }

    /**
     * Handles percentage of the canvas cleared
     * removes canvas and unsubscribe on offset breach
     *
     * Takes the context back to angular to emit fetch scratch card benetis
     *
     * @private
     * @param {number} filledInPixels
     * @memberof ScratchCardComponent
     */
    private handlePercentage(filledInPixels: number) {
        filledInPixels = filledInPixels || 0;

        if (filledInPixels >= SCRATCH_CARD_VALUES.OPEN_PERCENT) {
            if (this.canvasEl && this.canvasEl.nativeElement) {
                this.canvasEl.nativeElement.remove();
                this.stopObservingTouchEvents();
                this.ngZone.run(() => {
                    this.handleFetchScratchCardBenefit.emit();
                });
            }
        }
    }
}
