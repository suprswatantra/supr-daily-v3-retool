export const SCRATCH_CARD_VALUES = {
    DEFAULT_BRUSH_URL: "./assets/images/app/supr-brush.png",
    DEFAULT_SCRATCH_CARD: "./assets/images/app/supr-scratch-card.png",
    DEFAULT_EMPTY_STATE_IMAGE_URL: "./assets/images/app/supr-rewards.svg",
    CORS: "Anonymous",
    CANVAS_DIMENSION: "2d",
    GLOBAL_COMPOSITE_OPERATOR: "destination-out",
    ANGLE_DELTA: 25,
    DEFAULT_STRIDE: 32,
    OPEN_PERCENT: 30,
    RADIX: 10,
    CANVAS_WIDTH: 243,
    CANVAS_HEIGHT: 158,
};

export enum TOUCH_EVENTS {
    TOUCH_START = "touchstart",
    TOUCH_MOVE = "touchmove",
    TOUCH_END = "touchend",
}

export enum IMAGE_SMOOTHING_QUALITY {
    LOW = "low",
    MEDIUM = "medium",
    HIGH = "high",
}
