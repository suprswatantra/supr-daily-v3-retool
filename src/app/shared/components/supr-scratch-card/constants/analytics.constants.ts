export const ANALYTICS_OBJECT_NAMES = {
    IMPRESSION: {
        REVEALED: "scratch-card-revealed",
        REVEALED_WITH_ERROR: "scratch-card-revealed-with-error",
    },
};
