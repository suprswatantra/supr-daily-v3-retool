import { SuprOosNotifyComponent } from "./supr-oos-notify.component";
import { WrapperComponent } from "./components/wrapper.component";

export const SuprOOSNotifyComponents = [
    WrapperComponent,
    SuprOosNotifyComponent,
];
