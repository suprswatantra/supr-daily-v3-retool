import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { OOS_INFO_STATES } from "@constants";

import { OutOfStockInfoStateType } from "@models";

import { Segment } from "@types";

import {
    SA_OBJECT_NAMES,
    SA_CONTEXT_NAMES,
} from "../constants/analytics.constants";

@Component({
    selector: "supr-oos-notify-wrapper",
    template: `
        <div class="wrapper">
            <div class="suprRow" [class.header]="notifyEnabled">
                <supr-text class="addedFor" *ngIf="addedToCart" type="body">
                    Added for
                </supr-text>
                <supr-text class="addedFor" *ngIf="!addedToCart" type="body">
                    Earliest available date
                </supr-text>
                &nbsp;
                <supr-text type="body" class="dateText">
                    {{ date | date: "EEE, dd MMM" }}
                </supr-text>
            </div>
            <div class="suprRow content" *ngIf="notifyEnabled">
                <supr-text class="alertText" type="paragraph">
                    Get an alert if this item is available earlier
                </supr-text>
                <supr-button
                    [saObjectValue]="skuId"
                    [saObjectName]="saObjectName"
                    [saContextList]="saContextList"
                    saContext="${SA_CONTEXT_NAMES.NOTIFY_TPLUSX}"
                    (handleClick)="_updateOOSInfo()"
                    [class.selected]="state === '${OOS_INFO_STATES.NOTIFY}'"
                >
                    <div class="suprRow">
                        <supr-icon name="bell_2"> </supr-icon>
                        <div class="spacer4"></div>
                        <supr-text type="caption"> Notify </supr-text>
                    </div>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["../supr-oos-notify.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() date: string;
    @Input() skuId: number;
    @Input() addedToCart: boolean;
    @Input() notifyEnabled: boolean;
    @Input() state: OutOfStockInfoStateType;
    @Input() saContextList: Segment.ContextListItem[];

    @Output() updateOOSInfo: EventEmitter<
        OutOfStockInfoStateType
    > = new EventEmitter();

    _updateOOSInfo() {
        const newState =
            this.state === OOS_INFO_STATES.NOTIFY
                ? OOS_INFO_STATES.OOS
                : OOS_INFO_STATES.NOTIFY;

        this.updateOOSInfo.emit(newState);
    }

    get saObjectName(): string {
        return this.state === OOS_INFO_STATES.NOTIFY
            ? SA_OBJECT_NAMES.CLICK.CANCEL
            : SA_OBJECT_NAMES.CLICK.NOTIFY;
    }
}
