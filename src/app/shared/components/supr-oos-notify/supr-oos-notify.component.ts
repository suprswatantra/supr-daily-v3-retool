import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SkuAdapter } from "@shared/adapters/sku.adapter";

import { OutOfStockInfoStateType } from "@models";

import { Segment } from "@types";

import { SA_CONTEXT_NAMES } from "./constants/analytics.constants";
import { OOS_INFO_STATES } from "@constants";

@Component({
    selector: "supr-oos-notify",
    template: `<supr-oos-notify-wrapper
        [date]="date"
        [skuId]="skuId"
        [notifyEnabled]="notifyEnabled"
        [state]="state$ | async"
        [addedToCart]="addedToCart"
        [saContextList]="saContextList$ | async"
        (updateOOSInfo)="updateOOSInfo($event)"
    ></supr-oos-notify-wrapper>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOosNotifyComponent implements OnInit {
    @Input() date: string;
    @Input() skuId: number;
    @Input() notifyEnabled = true;
    @Input() addedToCart: boolean;

    state$: Observable<OutOfStockInfoStateType>;
    saContextList$: Observable<Segment.ContextListItem[]>;

    constructor(private skuAdapter: SkuAdapter) {}

    ngOnInit() {
        this.state$ = this.skuAdapter.getOOSInfoStateBySkuId(this.skuId);
        this.saContextList$ = this.skuAdapter
            .getOOSInfoStateBySkuId(this.skuId)
            .pipe(
                map((state: OutOfStockInfoStateType) => [
                    {
                        name: SA_CONTEXT_NAMES.NOTIFY,
                        value: state === OOS_INFO_STATES.OOS ? 0 : 1,
                    },
                    { name: SA_CONTEXT_NAMES.NOTIFY_TPLUSX, value: this.date },
                ])
            );
    }

    updateOOSInfo(state: OutOfStockInfoStateType) {
        this.skuAdapter.updateSkuOOSInfo(this.skuId, state);
    }
}
