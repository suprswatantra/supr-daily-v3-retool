export const SA_OBJECT_NAMES = {
    CLICK: {
        NOTIFY: "notify-button",
        CANCEL: "cancel-notify",
    },
};

export const SA_CONTEXT_NAMES = {
    NOTIFY: "notify",
    NOTIFY_TPLUSX: "notify-tplusx",
};
