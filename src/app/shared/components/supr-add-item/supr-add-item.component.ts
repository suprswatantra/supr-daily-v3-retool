import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Segment } from "@types";

import { SA_OBJECT_NAMES } from "./constants/analytics.constants";

@Component({
    selector: "supr-add-item",
    template: `
        <div
            saClick
            class="suprRow center left"
            (click)="handleClick.emit($event)"
            [saContext]="saContext"
            [saContextList]="saContextList"
            saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_PRODUCT}"
        >
            <supr-svg></supr-svg>
            <div class="spacer12"></div>
            <div class="suprColumn left">
                <supr-text type="body">ADD ITEM</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./supr-add-item.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddItemComponent {
    @Input() saContext: string;
    @Input() saContextList: Segment.ContextListItem[] = [];

    @Output() handleClick: EventEmitter<TouchEvent> = new EventEmitter();
}
