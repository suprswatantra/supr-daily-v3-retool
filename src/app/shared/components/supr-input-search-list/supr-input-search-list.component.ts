import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
    HostListener,
    ElementRef,
    Output,
    EventEmitter,
    SimpleChanges,
    OnChanges,
    AfterContentChecked,
} from "@angular/core";
import { INPUT } from "@constants";
/**
 *
 * @export
 * @class InputSearchListComponent
 * @implements {OnInit}
 */
@Component({
    selector: "supr-input-search-list",
    templateUrl: "./supr-input-search-list.component.html",
    styleUrls: ["./supr-input-search-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputSearchListComponent
    implements OnInit, OnChanges, AfterContentChecked {
    @Input() type: string = INPUT.TYPES.TEXT;
    @Input() textType: string;
    @Input() placeholder = "";
    @Input() disabled = false;
    @Input() debounceTime = INPUT.DEBOUNCE_TIME;
    @Input() label = "";
    @Input() optionsList = [];
    @Input() value = "";

    @Output() handleInputChange: EventEmitter<any> = new EventEmitter();

    constructor(private elRef: ElementRef) {}

    _showOptions = false;
    _searchText = "";
    _tempList = [];
    _selectedValue = "";
    _isSelected = false;
    _isDataChanged = false;

    ngOnInit() {
        this._selectedValue = this.value ? this.value : "";
        this._tempList = this.optionsList;
    }

    ngOnChanges(changes: SimpleChanges) {
        const changeOptionList = changes["optionsList"];
        if (changeOptionList) {
            if (
                !changeOptionList.firstChange &&
                changeOptionList.currentValue !== changeOptionList.previousValue
            ) {
                if (changeOptionList.currentValue) {
                    this._isDataChanged = true;
                    this._tempList = changeOptionList.currentValue;
                    if (!this.value) {
                        this._selectedValue = "";
                    }
                }
            }
        }
    }

    ngAfterContentChecked() {
        if (!this._isDataChanged) {
            this._selectedValue = this.value;
        }
    }

    showOptions(event: MouseEvent) {
        event.stopPropagation();
        this._showOptions = true;
    }

    hideOptions() {
        this._showOptions = false;
    }

    onInputChange(searchTerm: string) {
        // Input gets dirty onchange. Set isSelected as false
        if (this._isSelected) {
            this._isSelected = false;
        }
        this._searchText = searchTerm;
        this.filterOptions(searchTerm);
    }

    filterOptions(searchTerm: string) {
        const tempList = [
            ...this.optionsList.filter(
                (item: any) =>
                    item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) >
                    -1
            ),
        ];

        // is user typed string is not present then add it to options
        if (tempList.length < 1) {
            tempList.push({ id: -1, name: searchTerm });
        }

        this._tempList = [...tempList];
    }

    onSelect(selected: any, event?: MouseEvent) {
        if (event) {
            event.stopPropagation();
        }
        this._selectedValue = selected.name ? selected.name : "";
        this._isSelected = true;
        this.hideOptions();
        this.handleInputChange.emit(selected);
    }

    @HostListener("document:click", ["$event"])
    onClick(event: MouseEvent) {
        event.stopPropagation();
        // If clicked outside and some value was selected from drop down then just close the dropdown, else reset dropdown value
        if (!this.elRef.nativeElement.contains(event.target)) {
            this._isSelected ? this.hideOptions() : this.onSelect("");
        }
    }
}
