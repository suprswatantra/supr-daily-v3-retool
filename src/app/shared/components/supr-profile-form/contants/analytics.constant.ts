export const SA_OBJECT_NAMES = {
    CLICK: {
        FULL_NAME: "full-name",
        FIRST_NAME: "first-name",
        LAST_NAME: "last-name",
        EMAIL_ADDRESS: "email-address",
        REFERRAL_CODE: "referral-code",
    },
};
