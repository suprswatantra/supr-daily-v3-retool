import { ProfileFormComponent } from "./supr-profile-form.component";
import { ProfileFormErrorComponent } from "./components/error.component";

export const profileComponents = [
    ProfileFormErrorComponent,
    ProfileFormComponent,
];
