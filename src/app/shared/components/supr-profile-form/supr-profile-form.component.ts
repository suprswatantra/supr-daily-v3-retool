import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { User } from "@models";
import { ProfileFormError } from "@types";

import { SA_OBJECT_NAMES } from "./contants/analytics.constant";
import { REFERRAL_NOTE } from "./contants";

@Component({
    selector: "supr-profile-form",
    template: `
        <div class="form">
            <div class="suprRow">
                <div class="suprColumn left">
                    <supr-input-label
                        label="First name"
                        placeholder="Your first name"
                        [value]="user?.firstName || ''"
                        (handleInputChange)="updateFirstName($event)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.FIRST_NAME}"
                    ></supr-input-label>
                </div>

                <div class="spacer16"></div>

                <div class="suprColumn left">
                    <supr-input-label
                        label="Last name"
                        placeholder="Your last name"
                        [value]="user?.lastName || ''"
                        (handleInputChange)="updateLastName($event)"
                        saObjectName="${SA_OBJECT_NAMES.CLICK.LAST_NAME}"
                    ></supr-input-label>
                </div>
            </div>
            <supr-profile-form-error
                [errMsg]="formErrors?.name"
            ></supr-profile-form-error>
            <div class="divider16"></div>

            <supr-input-label
                label="Email address"
                placeholder="Your email address"
                [value]="user?.email || ''"
                (handleInputChange)="updateEmail($event)"
                saObjectName="${SA_OBJECT_NAMES.CLICK.EMAIL_ADDRESS}"
            ></supr-input-label>
            <supr-profile-form-error
                [errMsg]="formErrors?.email"
            ></supr-profile-form-error>

            <ng-container *ngIf="showReferralBlock">
                <div class="divider16"></div>

                <supr-input-label
                    label="Referral code (Optional)"
                    placeholder="Enter 10 digit referral code"
                    [value]="referralCode"
                    (handleInputChange)="updateReferralCode($event)"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.REFERRAL_CODE}"
                ></supr-input-label>

                <div class="divider12"></div>
                <supr-text type="regular14" class="referralNote">
                    ${REFERRAL_NOTE}
                </supr-text>

                <supr-profile-form-error
                    [errMsg]="formErrors?.referralCode"
                ></supr-profile-form-error>
            </ng-container>
        </div>
    `,
    styleUrls: ["./supr-profile-form.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileFormComponent implements OnInit, OnChanges {
    @Input() showReferralBlock: boolean;
    @Input() user: User;
    @Input() formErrors: ProfileFormError;
    @Input() prePopulatedReferralCode: string;

    @Output()
    handleUpdateFormData: EventEmitter<User> = new EventEmitter();
    @Output() handleResetPrePopulatedReferralCode: EventEmitter<
        void
    > = new EventEmitter();

    referralCode: string;

    ngOnInit() {
        this.setReferralCode();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["prePopulatedReferralCode"];

        if (this.canHandleChange(change)) {
            this.setReferralCode();
        }
    }

    updateFirstName(firstName = "") {
        this.handleUpdateFormData.emit({ firstName });
    }

    updateLastName(lastName = "") {
        this.handleUpdateFormData.emit({ lastName });
    }

    updateEmail(email = "") {
        this.handleUpdateFormData.emit({ email });
    }

    updateReferralCode(referralCode = "") {
        this.handleUpdateFormData.emit({ referralCode });
    }

    private setReferralCode() {
        if (this.prePopulatedReferralCode) {
            this.referralCode = this.prePopulatedReferralCode;

            this.updateReferralCode(this.prePopulatedReferralCode);
            this.handleResetPrePopulatedReferralCode.emit();

            return;
        }

        this.referralCode = (this.user && this.user.referralCode) || "";
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue &&
            change.currentValue !== change.previousValue
        );
    }
}
