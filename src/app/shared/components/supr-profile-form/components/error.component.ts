import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-profile-form-error",
    template: `
        <supr-text type="subtext10" class="error" *ngIf="errMsg?.length > 0">
            {{ errMsg }}
        </supr-text>
    `,
    styleUrls: ["../supr-profile-form.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileFormErrorComponent {
    @Input() errMsg: string;
}
