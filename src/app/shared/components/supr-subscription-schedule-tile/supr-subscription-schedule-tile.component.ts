import {
    Input,
    Output,
    OnInit,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    Schedule,
    Subscription,
    SubscriptionTransaction,
} from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { QuantityService } from "@services/util/quantity.service";

@Component({
    selector: "supr-subscription-schedule-tile",
    template: `
        <supr-card>
            <div class="tile" [class.history]="history">
                <ng-container *ngIf="history; else scheduleTile">
                    <div class="suprRow">
                        <supr-text
                            class="tileDate"
                            *ngIf="transaction?.created"
                            type="body"
                        >
                            {{ transaction.created | date: "fullDate" }}
                        </supr-text>

                        <supr-text
                            type="body"
                            class="tileAmount"
                            [ngClass]="_tileAmtClass"
                        >
                            {{ _consumptionPrefix }}
                            {{ transaction?.balance?.consumed | rupee: 2 }}
                        </supr-text>
                    </div>

                    <div
                        *ngIf="_qtyText || transaction?.id"
                        class="divider4"
                    ></div>

                    <div class="suprRow">
                        <supr-text
                            type="subtext10"
                            *ngIf="transaction?.type === 'ORDER'"
                        >
                            {{ _qtyText }}
                        </supr-text>
                        <supr-text
                            type="paragraph"
                            class="tileRechargeId"
                            *ngIf="transaction?.type === 'RECHARGE'"
                        >
                            Recharge id&nbsp;#{{ transaction?.id }}
                        </supr-text>
                    </div>
                    <div
                        *ngIf="_qtyText || transaction?.id"
                        class="divider12"
                    ></div>
                    <div class="tileStatus" [ngClass]="_statusClass">
                        <supr-text type="paragraph">
                            {{ transaction?.details }}
                        </supr-text>
                    </div>

                    <supr-text type="paragraph" class="tileLedger">
                        Updated balance&nbsp;
                        {{ transaction?.balance?.amount | rupee: 2 }}
                    </supr-text>
                </ng-container>

                <ng-template #scheduleTile>
                    <div (click)="handleEditClick.emit()">
                        <div class="suprRow">
                            <supr-text
                                class="tileDate"
                                *ngIf="schedule?.date"
                                type="body"
                            >
                                {{ schedule.date | date: "fullDate" }}
                            </supr-text>

                            <div
                                class="suprRow tileAction"
                                saClick
                                [saContext]="saContext"
                                [saObjectName]="saObjectName"
                                [saObjectValue]="saObjectValue"
                            >
                                <supr-text type="subtext10">
                                    EDIT
                                </supr-text>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                        </div>

                        <div class="divider4"></div>

                        <supr-text class="tileQuantity" type="subtext10">{{
                            _qtyText
                        }}</supr-text>
                    </div>
                </ng-template>
            </div>
        </supr-card>
    `,
    styleUrls: ["./supr-subscription-schedule-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionDetailsScheduleTile implements OnInit {
    @Input() history = false;
    @Input() schedule: Schedule;
    @Input() subscription: Subscription;
    @Input() set transaction(data: SubscriptionTransaction) {
        this._transaction = data;
        this.setConsumptionInfo();
    }
    get transaction(): SubscriptionTransaction {
        return this._transaction;
    }

    @Input() saContext: number;
    @Input() saObjectName: string;
    @Input() saObjectValue: number;

    @Output() handleEditClick: EventEmitter<void> = new EventEmitter();

    _qtyText: string;
    _statusClass: string;
    _tileAmtClass: string;
    _consumptionPrefix: string;
    _transaction: SubscriptionTransaction;

    constructor(
        private utilService: UtilService,
        private quantityService: QuantityService
    ) {}

    ngOnInit() {
        this.setQtyText();
    }

    private setConsumptionInfo() {
        const type = this.utilService.getNestedValue(this.transaction, "type");

        if (type !== "ORDER") {
            this._consumptionPrefix = "+";
            this._statusClass = "success";
            this._tileAmtClass = "success";
        } else {
            const consumption = this.utilService.getNestedValue(
                this.transaction,
                "balance.consumed"
            );

            this._consumptionPrefix = consumption ? "-" : "";
            this._tileAmtClass = consumption ? "caution" : "";
            this._statusClass = consumption ? "success" : "caution";
        }
    }

    private setQtyText() {
        const sku = this.utilService.getNestedValue(
            this.subscription,
            "sku_data"
        );
        const quantity = this.history
            ? this.utilService.getNestedValue(this.transaction, "qty")
            : this.utilService.getNestedValue(
                  this.schedule,
                  "subscriptions.0.quantity"
              );

        if (sku && quantity) {
            this._qtyText = this.quantityService.toText(sku, 1, quantity);
        }
    }
}
