import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { DAY_LETTERS } from "@constants";
import { Frequency } from "@types";

import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-week-day-custom",
    template: `
        <div class="week suprRow">
            <ng-container
                *ngFor="
                    let day of dayLetters;
                    trackBy: trackByFn;
                    last as _lastDay
                "
            >
                <supr-week-day-small
                    [selected]="selectedDays[day]"
                    [day]="day"
                    [textType]="textType"
                >
                </supr-week-day-small>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/custom.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekDayCustomComponent implements OnInit {
    @Input() selectedDays: Frequency;
    @Input() textType: string;

    dayLetters = DAY_LETTERS;

    constructor(private utilService: UtilService) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    ngOnInit() {
        if (this.utilService.isEmpty(this.selectedDays)) {
            this.selectedDays = {};
        }
    }
}
