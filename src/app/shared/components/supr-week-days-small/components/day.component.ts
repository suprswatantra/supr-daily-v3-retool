import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-week-day-small",
    template: `
        <supr-text
            [type]="textType"
            class="weekDay"
            [class.selected]="selected"
        >
            {{ day | slice: 0:1 | uppercase }}
        </supr-text>
    `,
    styleUrls: ["../styles/day.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekDayComponent {
    @Input() day: string;
    @Input() textType: string;
    @Input() selected = false;
}
