import { WeekDaysSmallComponent } from "./supr-week-days-small.component";
import { WeekDayComponent } from "./components/day.component";
import { WeekDayCustomComponent } from "./components/custom-selection.component";

export const weekDaysSmallComponents = [
    WeekDaysSmallComponent,
    WeekDayComponent,
    WeekDayCustomComponent,
];
