import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { WEEK_DAYS_SELECTION_FORMAT } from "@constants";
import { UtilService } from "@services/util/util.service";
import { Frequency } from "@types";

@Component({
    selector: "supr-week-days-small",
    template: `
        <ng-container *ngIf="isCustomSelection; else textSelection">
            <supr-week-day-custom
                [selectedDays]="selectedDays"
                [textType]="textType"
            ></supr-week-day-custom>
        </ng-container>
        <ng-template #textSelection>
            <supr-text [type]="textType">{{ daysFormat }}</supr-text>
        </ng-template>
    `,
    styleUrls: ["./styles/host.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekDaysSmallComponent implements OnInit, OnChanges {
    @Input() selectedDays: Frequency = {};
    @Input() textType = "caption";

    daysFormat: string;
    isCustomSelection = true;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["selectedDays"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.initData();
        }
    }

    private initData() {
        if (this.utilService.isEmpty(this.selectedDays)) {
            this.selectedDays = {};
        }

        this.daysFormat = this.utilService.getWeekDaysFormatFromDays(
            this.selectedDays
        );

        this.isCustomSelection =
            this.daysFormat === WEEK_DAYS_SELECTION_FORMAT.CUSTOM;
    }
}
