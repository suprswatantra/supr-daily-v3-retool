import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-ribbon",
    template: `
        <div class="wrapper">
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-ribbon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RibbonComponent {}
