import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Segment, ConfigurableIcon } from "@types";

@Component({
    selector: "supr-page-footer-v2",
    templateUrl: "./supr-page-footer-v2.component.html",
    styleUrls: ["./supr-page-footer-v2.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprPageFooterV2Component {
    @Input() disabled: boolean;
    @Input() buttonIcon: string = "chevron_right";
    @Input() buttonTitleIcon: ConfigurableIcon;
    @Input() buttonTitle: string;
    @Input() buttonSubtitle: string;
    @Input() buttonSubtitleIconLeft: string;
    @Input() footNoteText: string;
    @Input() footNoteLinkText: string;
    @Input() saObjectName: string;
    @Input() saObjectValue: any;
    @Input() saContext: string;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() footerNoteSaObjectName: string;

    @Output() handleButtonClick: EventEmitter<void> = new EventEmitter();
    @Output() handleFooterNoteClick: EventEmitter<void> = new EventEmitter();
}
