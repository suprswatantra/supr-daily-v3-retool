export const ICON_SIZE = "16px";

export const ICONS = {
    PLUS: "add",
    MINUS: "subtract",
};

export const TEXTS = {
    ADD: "ADD",
    SUBSCRIBE: "SUBSCRIBE",
};

export const SUPR_TEXT_TYPE = {
    PARAGRAPH: "paragraph",
    HEADING: "heading",
};
