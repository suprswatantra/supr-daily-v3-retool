import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { Segment } from "@types";

import { ICONS, ICON_SIZE } from "../supr-number-counter.constants";

@Component({
    selector: "supr-counter",
    template: `
        <div class="suprRow counter">
            <supr-icon
                [name]="minusIcon"
                [disabled]="minusDisabled"
                class="counterIcon counterIconLeft"
                (click)="minusClick()"
                saClick
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .PRODUCT_QUANTITY_MINUS}"
                [saObjectValue]="saObjectValue"
                [saContext]="saContext"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            >
            </supr-icon>
            <supr-text [type]="suprTextType" class="counterQuantity">
                {{ quantity }}
            </supr-text>
            <supr-icon
                [name]="plusIcon"
                [disabled]="plusDisabled"
                class="counterIcon counterIconRight"
                (click)="plusClick()"
                saClick
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .PRODUCT_QUANTITY_PLUS}"
                [saObjectValue]="saObjectValue"
                [saContext]="saContext"
                [saContextList]="saContextList"
                [saPosition]="saPosition"
            ></supr-icon>
        </div>
    `,
    styleUrls: ["../styles/counter.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CounterComponent {
    @Input() quantity: number;
    @Input() color = "primary";
    @Input() plusDisabled = false;
    @Input() minusDisabled = false;
    @Input() suprTextType: string;
    @Input() saObjectValue: number;
    @Input() saContext: any;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() saPosition: number;
    @Output() handleClick: EventEmitter<number> = new EventEmitter();

    plusIcon = ICONS.PLUS;
    minusIcon = ICONS.MINUS;
    iconSize = ICON_SIZE;

    plusClick() {
        if (!this.plusDisabled) {
            this.handleClick.emit(1);
        }
    }

    minusClick() {
        if (!this.minusDisabled) {
            this.handleClick.emit(-1);
        }
    }
}
