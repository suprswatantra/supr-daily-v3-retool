import { CounterComponent } from "./components/counter.component";
import { NumberCounterComponent } from "./supr-number-counter.component";

export const numberCounterComponents = [
    NumberCounterComponent,
    CounterComponent,
];
