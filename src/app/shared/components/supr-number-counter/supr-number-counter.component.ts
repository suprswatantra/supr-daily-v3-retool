import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    SimpleChanges,
    SimpleChange,
    OnChanges,
    OnInit,
} from "@angular/core";

import {
    NUMBER_COUNTER_TYPES,
    ANALYTICS_OBJECT_NAMES,
    SKU_PREFERRED_MODE,
} from "@constants";
import { Segment } from "@types";

import {
    ICONS,
    TEXTS,
    ICON_SIZE,
    SUPR_TEXT_TYPE,
} from "./supr-number-counter.constants";

@Component({
    selector: "supr-number-counter",
    template: `
        <div
            class="container"
            [ngClass]="type"
            [class.secondary]="
                mode === '${SKU_PREFERRED_MODE.SUBSCRIPTION}' &&
                (!quantity || quantity <= thresholdQty)
            "
        >
            <ng-container *ngIf="quantity > thresholdQty">
                <supr-counter
                    [quantity]="quantity"
                    [plusDisabled]="plusDisabled"
                    [minusDisabled]="minusDisabled"
                    (handleClick)="handleClick.emit($event)"
                    [suprTextType]="suprTextType"
                    [saObjectValue]="saObjectValue"
                    [saContext]="saContext"
                    [saContextList]="saContextList"
                    [saPosition]="saPosition"
                >
                </supr-counter>
            </ng-container>

            <ng-container
                *ngIf="
                    quantity <= thresholdQty &&
                    mode !== '${SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE}'
                "
            >
                <div
                    class="add suprRow center ion-activatable"
                    (click)="delayedClick()"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.ADD_PRODUCT}"
                    [saObjectValue]="saObjectValue"
                    [saContext]="saContext"
                    [saContextList]="saContextList"
                    [saPosition]="saPosition"
                >
                    <ion-ripple-effect></ion-ripple-effect>
                    <supr-icon class="addIcon" [name]="plusIcon"></supr-icon>
                    <supr-text
                        class="addLabel"
                        type="subtext10"
                        [class.addLabelSubscribe]="isDirectSubscribe"
                    >
                        ${TEXTS.ADD}
                    </supr-text>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["./styles/supr-number-counter.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberCounterComponent implements OnInit, OnChanges {
    @Input() mode: string;
    @Input() quantity = 0;
    @Input() thresholdQty = 0;
    @Input() showZero = false;
    @Input() plusDisabled = false;
    @Input() minusDisabled = false;
    @Input() autoIncrement = false;

    @Input() type = NUMBER_COUNTER_TYPES.DEFAULT;
    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() saObjectValue: number;
    @Input() set saContextList(list: Segment.ContextListItem[]) {
        this._saContextList = [
            ...(list || []),
            {
                name: "preferred",
                value: this.mode === SKU_PREFERRED_MODE.ADDON,
            },
        ];
    }
    get saContextList(): Segment.ContextListItem[] {
        return this._saContextList;
    }

    @Output() handleClick: EventEmitter<number> = new EventEmitter();

    plusIcon = ICONS.PLUS;
    iconSize = ICON_SIZE;
    suprTextType = SUPR_TEXT_TYPE.PARAGRAPH;

    private _saContextList: Segment.ContextListItem[];

    ngOnInit() {
        this.setButtonType();
    }

    ngOnChanges(changes: SimpleChanges) {
        const autoIncrementChange = changes["autoIncrement"];

        this.handleAutoIncrement(autoIncrementChange);
    }

    delayedClick() {
        setTimeout(() => this.handleClick.emit(1), 250);
    }

    private setButtonType() {
        if (this.type === NUMBER_COUNTER_TYPES.FLAT) {
            this.suprTextType = SUPR_TEXT_TYPE.HEADING;
        }
    }

    private handleAutoIncrement(change: SimpleChange) {
        if (change && change.currentValue) {
            this.delayedClick();
        }
    }
}
