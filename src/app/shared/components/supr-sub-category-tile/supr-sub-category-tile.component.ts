import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";
import { UtilService } from "@services/util/util.service";
import { Color } from "@types";

@Component({
    selector: "supr-sub-category-tile",
    template: `
        <div class="wrapper" #tile>
            <supr-image
                [src]="subCategory?.image?.fullUrl"
                [image]="subCategory?.image"
            ></supr-image>
            <div class="name">
                <supr-text class="heading" type="subheading">
                    {{ subCategory?.name }}
                </supr-text>
                <supr-text class="count" type="caption">
                    {{ noOfItemsText }}
                </supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./supr-sub-category-tile.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubCategoryTileComponent implements OnInit {
    @Input() subCategory: any;

    @ViewChild("tile", { static: true }) tileEl: ElementRef;

    noOfItemsText: string;
    hslData: Color.HSL;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.convertColorToHSL();

        this.setBgColor();

        this.setNoOfItemsText();
    }

    private setNoOfItemsText() {
        const itemCount = this.subCategory.skuIdList.length;
        this.noOfItemsText = `${itemCount} item${itemCount > 1 ? "s" : ""}`;
    }

    private convertColorToHSL() {
        this.hslData = this.utilService.hexToHSL(
            this.subCategory.image.bgColor
        );
    }

    private setBgColor() {
        const { h, s } = this.hslData;
        this.tileEl.nativeElement.style.setProperty(
            "--supr-sub-category-tile-img-bg-color",
            `hsl(${h}, ${s}%, 95%)`
        );
    }
}
