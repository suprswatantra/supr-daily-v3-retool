import { alternatesV5Components } from "./supr-oos-alternates-container/index";
import { SuprOfferListComponents } from "./supr-offers-list/index";
import { RewardSkuTagComponent } from "./supr-reward-sku-tag/supr-reward-sku-tag.component";
import { ButtonComponent } from "./supr-button/supr-button.component";
import { DraggableTopComponent } from "./supr-draggable-top/supr-draggable-top.component";
import { IconComponent } from "./supr-icon/supr-icon.component";
import { InputComponent } from "./supr-input/supr-input.component";
import { TagComponent } from "./supr-tag/supr-tag.component";
import { SearchInputComponent } from "./supr-search-input/supr-search-input.component";
import { ImageComponent } from "./supr-image/supr-image.component";
import { SideMenuOpenerComponent } from "./supr-sidemenu-opener/supr-sidemenu-opener.component";
import { ChipComponent } from "./supr-chip/supr-chip.component";
import { numberCounterComponents } from "./supr-number-counter";
import { otpInputComponents } from "./supr-otp-input";
import { productTileComponents } from "./supr-product-tile";
import { calendarComponents } from "./supr-calendar";
import { TextComponent } from "./supr-text/supr-text.component";
import { TextHighlightComponent } from "./supr-text/supr-text-highlight.component";
import { StickyWrapperComponent } from "./supr-sticky-wrapper/supr-sticky-wrapper.component";
import { BackButtonComponent } from "./supr-back-button/supr-back-button.component";
import { SwipeableWrapperComponent } from "./supr-swipeable-wrapper/supr-swipeable-wrapper.component";
import { SvgComponent } from "./supr-svg/supr-svg.component";
import { SubCategoryTileComponent } from "./supr-sub-category-tile/supr-sub-category-tile.component";
import { PageHeaderComponent } from "./supr-page-header/supr-page-header.component";
import { RibbonComponent } from "./supr-ribbon/supr-ribbon.component";
import { SuprBackdropComponent } from "./supr-backdrop/supr-backdrop.component";
import { SearchBoxComponent } from "./supr-search-box/supr-search-box.component";
import { BottomSheetComponent } from "./supr-bottom-sheet/supr-bottom-sheet.component";
import { RadioButtonComponent } from "./supr-radio-button/supr-radio-button.component";
import { cartComponents } from "./supr-cart";
import { weekDaysSmallComponents } from "./supr-week-days-small";
import { weekDaysSelectComponents } from "./supr-week-days-select";
import { AddItemComponent } from "./supr-add-item/supr-add-item.component";
import { CardComponent } from "./supr-card/supr-card.component";
import { OrderHistoryProductTileComponent } from "./supr-order-history-product-tile/supr-order-history-product-tile.component";
import { SuprBagRibbonComponent } from "./supr-bag-ribbon/supr-bag-ribbon.component";
import { ScheduleCardProductComponent } from "./supr-schedule-card-product/supr-schedule-card-product.component";
import { ScheduleEditModalCardComponent } from "./supr-schedule-edit-modal-card/supr-schedule-edit-modal-card.component";
import { SubscriptionProductTileComponent } from "./supr-subscription-product-tile/supr-subscription-product-tile.component";
import { ModalComponent } from "./supr-modal/supr-modal.component";
import { InputSearchListComponent } from "./supr-input-search-list/supr-input-search-list.component";
import { couponComponents } from "./supr-coupon";
import { RadioButtonGroupComponent } from "./supr-radio-button-group/supr-radio-button-group.component";
import { SuprScheduleWrapperDateComponent } from "./supr-schedules-wrapper-date/supr-schedules-wrapper-date.component";
import { SuprScheduleItemComponent } from "./supr-schedule-item/supr-schedule-item.component";
import { categoryGridComponents } from "./supr-category-grid";
import { collectionWidgetComponents } from "./supr-collection";
import { SuprScheduleSubscriptionContainer } from "./supr-schedule-subscription/supr-schedule-subscription.container";
import { SuprScheduleAddonContainer } from "./supr-schedule-addon/supr-schedule-addon.container";
import { SuprAddonEditCardComponent } from "./supr-addon-edit-card/supr-addon-edit-card.component";
import { SuprOrderHistoryComponent } from "./supr-order-history/supr-order-history.component";
import { ScheduleProductTileComponent } from "./supr-schedule-product-tile/supr-schedule-product-tile.component";
import { ScheduleAddonEditLayoutComponent } from "./supr-schedule-addon-edit-layout/supr-schedule-addon-edit.layout";
import { ScheduleSubscriptionEditLayoutComponent } from "./supr-schedule-subscription-edit-layout/supr-schedule-subscription-edit.layout";
import { SuprSubscriptionEditCardComponent } from "./supr-subscription-edit-card/supr-subscription-edit-card.component";
import { createSubscriptionComponents } from "./supr-create-subscription";
import { SuprLoaderComponent } from "./supr-loader/supr-loader.component";
import { FrequencySelectComponent } from "./supr-frequency-select/supr-frequency-select.component";
import { suprEditSubscriptionComponents } from "./supr-edit-subscription";
import { RechargeQtySelectComponent } from "./supr-recharge-qty-select/supr-recharge-qty-select.component";
import { CartFooterDepComponent } from "./supr-cart-footer-dep/supr-cart-footer-dep.component";
import { sideMenuComponents } from "./supr-sidemenu-home";
import { SuprLoaderOverlayComponent } from "./supr-loader-overlay/supr-loader-overlay.component";
import { pauseComponents } from "./supr-pause";
import { PageFooterComponent } from "./supr-page-footer/supr-page-footer.component";
import { TooltipComponent } from "./supr-tooltip/supr-tooltip.component";
import { profileComponents } from "./supr-profile-form";
import { ScheduleSkeletonComponent } from "./supr-schedule-skeleton/supr-schedule-skeleton.component";
import { errorComponents } from "./supr-error";
import { InternetMonitorComponent } from "./supr-internet-monitor/supr-internet-monitor.component";
import { appUpdateComponents } from "./supr-app-update";
import { NudgeMessageComponent } from "./supr-nudge-message/supr-nudge-message.component";
import { tileGridComponents } from "./supr-tile-grid";
import { addButtonComponents } from "./supr-add-button";
import { productPreviewComponents } from "./supr-product-preview";
import { mapComponents } from "./supr-map";
import { AddressSummaryComponent } from "./supr-address-summary/address-summary.component";
import { inputLabelComponents } from "./supr-input-label";
import { WhatsAppComponent } from "./supr-whatsapp/whatsapp.component";
import { TPlusOneBannerComponent } from "./supr-t-plus-one-banner/supr-t-plus-one-banner.component";
import { appRatingComponents } from "./supr-app-rating";
import { addressRetroComponents } from "./supr-address-retro";
import { SubscriptionBalanceModalComponent } from "./supr-subscription-balance-modal/supr-subscription-balance-modal.component";
import { exitCartComponents } from "./supr-exit-cart";
import { creditsInfoComponents } from "./supr-credits-info";
import { deliveryFeeInfoComponents } from "./supr-delivery-fee-info";
import { CheckboxComponent } from "./supr-checkbox/supr-checkbox.component";
import { SuprSubscriptionDeliveriesMessageComponent } from "./supr-subscription-deliveries-message/supr-subscription-deliveries-message.component";
import { subscribeButtonComponents } from "./supr-subscribe-button";
import { SuprOosOverlayComponent } from "./supr-oos-overlay/supr-oos-overlay.component";
import { SuprExpandBanner } from "./supr-expand-banner/supr-expand-banner.component";
import { cartCreditsComponents } from "./supr-cart-credits";
import { pastOrderCollections } from "./supr-past-order-collection";
import { ProductTilePrice } from "./supr-price-strike/supr-product-tile-price.component";
import { FeedbackComponents } from "./supr-feedback";
import { SchedulePastOrderCollection } from "./supr-schedule-past-order-collection/supr-schedule-past-order-collection.component";
import { camapignPopupComponents } from "./supr-campaign-popup";
import { discountBlockComponents } from "./supr-discount-block";
import { couponTncComponents } from "./supr-coupon-tnc";
import { pnComponents } from "./supr-pn";
import { SuprSubscriptionFrequencyModalComponent } from "./supr-frequency-modal/supr-subscription-frequency-modal.component";
import { SubscriptionDetailsScheduleTile } from "./supr-subscription-schedule-tile/supr-subscription-schedule-tile.component";
import { SuprSubDeliveryEditContainer } from "./supr-sub-delivery-schedule-container/supr-sub-delivery-schedule.container";
import { checkoutDisabledComponents } from "./supr-checkout-disabled";
import { v3Widgets } from "./supr-v3-widget";
import { v3Containers } from "./supr-v3-containers";
import { v3Elements } from "./supr-v3-elements";
import { HorizontalComponent } from "./supr-horizontal/supr-horizontal.component";
import { SuprScheduleDeliveryContainer } from "./supr-schedule-delivery/supr-schedule-delivery.container";
import { SuprScheduleDeliveryComponent } from "./supr-schedule-delivery/components/schedule-delivery/schedule-delivery.component";
import { SuprOrderDeliveryContainer } from "./supr-order-delivery/supr-order-delivery.container";
import { SuprOrderDeliveryComponent } from "./supr-order-delivery/components/schedule-delivery/order-delivery.component";
import { suprPassPlansComponents } from "./supr-pass-plans";
import { oosInfo } from "./supr-oos-info";
import { bannerComponents } from "./supr-banner";
import { OrderSuccessADModal } from "./supr-order-success-ad-modal/supr-order-success-ad-modal.component";
import { DeliverySlotChipComponent } from "./supr-delivery-slot-chip/supr-delivery-slot-chip.component";
import { SuprScheduleDayHeaderComponent } from "./supr-schedule-day-header/supr-schedule-day-header.component";
import { SuprSectionHeaderComponent } from "./supr-section-header/supr-section-header.component";
import { SuprTimeline } from "./supr-timeline/supr-timeline.component";
import { SuprOrderTimeline } from "./supr-order-timeline/supr-order-timeline.component";
import { SuprSupportButton } from "./supr-support-button/supr-support-button.component";
import { SuprComplaintsList } from "./supr-complaints-list/supr-complaints-list.component";
import { SuprComplaintView } from "./supr-complaint-view/supr-complaint-view.component";
import { SuprSupportOptionsComponent } from "./supr-support-options/supr-support-options.component";
import { genericNudgeComponents } from "./supr-generic-nudge";
import { TextFragmentComponent } from "./supr-text-fragment/text-fragment.component";
import { SuprOOSNotifyComponents } from "./supr-oos-notify";
import { suprBenefitsBannerComponents } from "./supr-benefits-banner";
import { ActivityBlockComponent } from "./supr-activity-block/activity-block.component";
import { CopyBlockComponent } from "./supr-copy-block/supr-copy-block.component";
import { referralSuccessComponents } from "./supr-referral-success";
import { suprPassAutoDebitCheckboxComponents } from "./supr-pass-auto-debit-check";
import { autoDebitComponents } from "./supr-auto-debit-payment";
import { SuprPassAutoDebitPaymentSuccess } from "./supr-pass-auto-debit-payment-success/supr-pass-auto-debit-payment-success.component";
import { FaqItemComponent } from "./supr-faq-item/supr-faq-item.component";
import { autoDebitPaymentFailureComponents } from "./supr-auto-debit-payment-failure";
import { SuprAccordion } from "./supr-accordion/supr-accordion.component";
import { SuprListItemComponent } from "./supr-list-item/supr-list-item.component";
import { SuprDropdownComponent } from "./supr-dropdown/supr-dropdown.component";
import { SuprPageFooterV2Component } from "./supr-page-footer-v2/supr-page-footer-v2.component";
import { CartFooterComponents } from "./supr-cart-footer/";
import { SuprConfigurableIconComponent } from "./supr-configurable-icon/supr-configurable-icon.component";
import { blockAccessNudgeComponents } from "./supr-block-access-nudge";
import { blockAccessBannerComponents } from "./supr-block-access-banner";
import { PopupComponent } from "./supr-popup/supr-popup-component";
import { PopupContentComponent } from "./supr-popup-content/supr-popup-content.component";
import { SuprLineItemVariantAComponent } from "./supr-line-item-variant-A/supr-line-item-variant-A.component";
import { SuprCheckboxLineItemComponent } from "./supr-checkbox-lineitem/supr-checkbox-lineitem.component";
import { referralBottomsheetComponents } from "./supr-referral-bottomsheet";
import { ImgBannerComponent } from "./supr-img-banner/img-banner.component";
import { featuredComponents } from "./supr-feature-card";
import { ImageUploadComponent } from "./supr-image-upload/supr-image-upload.component";
import { suprPassCardComponents } from "./supr-pass-card";
import { ProgressBarComponent } from "./supr-progress-bar/supr-progress-bar.compopnent";
import { ScratchCardComponent } from "./supr-scratch-card/supr-scratch-card.component";
import { OrderHistoryRefundModalComponent } from "./supr-order-history-refund-modal/supr-order-history-refund-modal.component";
import { OrderedListComponent } from "./supr-ordered-list/supr-ordered-list.component";
import { InitializeContainer } from "./supr-initialize/supr-initialize-container";
import { InitializeComponent } from "./supr-initialize/supr-initialize.component";
import { cartBannersComponents } from "./supr-cart-banner";

export const sharedComponents = [
    ButtonComponent,
    DraggableTopComponent,
    IconComponent,
    InputComponent,
    TagComponent,
    SearchInputComponent,
    ImageComponent,
    SideMenuOpenerComponent,
    ChipComponent,
    ...numberCounterComponents,
    ...otpInputComponents,
    ...calendarComponents,
    ...productTileComponents,
    BackButtonComponent,
    TextComponent,
    TextHighlightComponent,
    StickyWrapperComponent,
    SwipeableWrapperComponent,
    SvgComponent,
    SubCategoryTileComponent,
    PageHeaderComponent,
    PageFooterComponent,
    RibbonComponent,
    SuprBackdropComponent,
    SearchBoxComponent,
    BottomSheetComponent,
    RadioButtonComponent,
    ...cartComponents,
    ...weekDaysSmallComponents,
    ...weekDaysSelectComponents,
    ...cartBannersComponents,
    AddItemComponent,
    CardComponent,
    ScheduleProductTileComponent,
    SuprBagRibbonComponent,
    ScheduleCardProductComponent,
    ScheduleEditModalCardComponent,
    SubscriptionProductTileComponent,
    ModalComponent,
    InputSearchListComponent,
    ...couponComponents,
    RadioButtonGroupComponent,
    SuprScheduleItemComponent,
    SuprScheduleWrapperDateComponent,
    ...categoryGridComponents,
    ...collectionWidgetComponents,
    SuprScheduleSubscriptionContainer,
    SuprScheduleAddonContainer,
    SuprAddonEditCardComponent,
    SuprOrderHistoryComponent,
    OrderHistoryProductTileComponent,
    ScheduleAddonEditLayoutComponent,
    ScheduleSubscriptionEditLayoutComponent,
    SuprSubscriptionEditCardComponent,
    ...createSubscriptionComponents,
    SuprLoaderComponent,
    FrequencySelectComponent,
    ...suprEditSubscriptionComponents,
    RechargeQtySelectComponent,
    CartFooterDepComponent,
    ...sideMenuComponents,
    SuprLoaderOverlayComponent,
    ...pauseComponents,
    TooltipComponent,
    ...profileComponents,
    ScheduleSkeletonComponent,
    ...errorComponents,
    InternetMonitorComponent,
    ...appUpdateComponents,
    NudgeMessageComponent,
    ...tileGridComponents,
    ...addButtonComponents,
    ...productPreviewComponents,
    ...mapComponents,
    AddressSummaryComponent,
    ...inputLabelComponents,
    WhatsAppComponent,
    TPlusOneBannerComponent,
    ...appRatingComponents,
    ...addressRetroComponents,
    SubscriptionBalanceModalComponent,
    ...exitCartComponents,
    ...creditsInfoComponents,
    ...deliveryFeeInfoComponents,
    CheckboxComponent,
    SuprSubscriptionDeliveriesMessageComponent,
    subscribeButtonComponents,
    SuprOosOverlayComponent,
    SuprExpandBanner,
    ...cartCreditsComponents,
    ...pastOrderCollections,
    ProductTilePrice,
    ...FeedbackComponents,
    SchedulePastOrderCollection,
    ...camapignPopupComponents,
    ...discountBlockComponents,
    ...couponTncComponents,
    ...pnComponents,
    SuprSubscriptionFrequencyModalComponent,
    SubscriptionDetailsScheduleTile,
    SuprSubDeliveryEditContainer,
    ...checkoutDisabledComponents,
    ...v3Widgets,
    ...v3Containers,
    ...v3Elements,
    HorizontalComponent,
    SuprScheduleDeliveryComponent,
    SuprScheduleDeliveryContainer,
    SuprOrderDeliveryContainer,
    SuprOrderDeliveryComponent,
    ...suprPassPlansComponents,
    ...oosInfo,
    ...bannerComponents,
    OrderSuccessADModal,
    DeliverySlotChipComponent,
    SuprScheduleDayHeaderComponent,
    SuprSectionHeaderComponent,
    SuprTimeline,
    SuprOrderTimeline,
    SuprSupportButton,
    SuprComplaintsList,
    SuprComplaintView,
    SuprSupportOptionsComponent,
    ...genericNudgeComponents,
    TextFragmentComponent,
    ...SuprOOSNotifyComponents,
    ...alternatesV5Components,
    ...suprBenefitsBannerComponents,
    ActivityBlockComponent,
    CopyBlockComponent,
    ...referralSuccessComponents,
    ...suprPassAutoDebitCheckboxComponents,
    SuprPassAutoDebitPaymentSuccess,
    ...autoDebitComponents,
    FaqItemComponent,
    ...autoDebitPaymentFailureComponents,
    SuprAccordion,
    SuprListItemComponent,
    SuprDropdownComponent,
    SuprPageFooterV2Component,
    ...CartFooterComponents,
    SuprConfigurableIconComponent,
    ...blockAccessNudgeComponents,
    ...blockAccessBannerComponents,
    PopupComponent,
    PopupContentComponent,
    SuprLineItemVariantAComponent,
    SuprCheckboxLineItemComponent,
    ...referralBottomsheetComponents,
    ImgBannerComponent,
    ...featuredComponents,
    ImageUploadComponent,
    ...suprPassCardComponents,
    ProgressBarComponent,
    RewardSkuTagComponent,
    ScratchCardComponent,
    OrderHistoryRefundModalComponent,
    OrderedListComponent,
    ...SuprOfferListComponents,
    InitializeContainer,
    InitializeComponent,
];
