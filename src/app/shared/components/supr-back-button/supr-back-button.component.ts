import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

@Component({
    selector: "supr-back-button",
    template: `
        <div
            class="wrapper"
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
        >
            <supr-icon name="chevron_left"></supr-icon>
        </div>
    `,
    styleUrls: ["./supr-back-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BackButtonComponent {
    saObjectName = ANALYTICS_OBJECT_NAMES.CLICK.BACK_BUTTON;

    @Input() saObjectValue?: any;
}
