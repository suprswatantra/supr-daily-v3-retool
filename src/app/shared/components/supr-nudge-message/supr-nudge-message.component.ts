import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { NudgeMessage } from "@types";

@Component({
    selector: "supr-nudge-message",
    template: `
        <div class="message">
            <ng-container *ngIf="nudgeMessage?.img">
                <supr-image
                    [lazyLoad]="false"
                    [src]="nudgeMessage?.img"
                ></supr-image>
                <div class="divider20"></div>
            </ng-container>

            <supr-text type="subtitle">{{ nudgeMessage?.title }}</supr-text>
            <div class="divider8"></div>

            <supr-text type="body" class="subtitle">
                {{ nudgeMessage?.subTitle }}
            </supr-text>
            <div class="divider24"></div>

            <supr-button
                [saObjectName]="saObjectName"
                (handleClick)="handleClick.emit()"
            >
                <supr-text type="body">{{ nudgeMessage?.btnText }}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./supr-nudge-message.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NudgeMessageComponent {
    @Input() nudgeMessage: NudgeMessage;
    @Input() saObjectName: string;
    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
