import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { DeliveryFeeMessageInfo } from "@shared/models";

@Component({
    selector: "supr-delivery-fee-info",
    template: `
        <supr-delivery-fee-info-wrapper
            [showModal]="showModal"
            [delFeeInfo]="delFeeInfo"
            (handleClose)="handleClose?.emit()"
        ></supr-delivery-fee-info-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveryFeeInfoComponent {
    @Input() showModal: boolean;
    @Input() delFeeInfo: DeliveryFeeMessageInfo;
    @Output() handleClose: EventEmitter<void> = new EventEmitter();
}
