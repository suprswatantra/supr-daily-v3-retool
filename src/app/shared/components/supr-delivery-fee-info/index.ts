import { DeliveryFeeInfoComponent } from "./supr-credits-info.component";
import { WrapperComponent } from "./components/wrapper.component";

export const deliveryFeeInfoComponents = [
    DeliveryFeeInfoComponent,
    WrapperComponent,
];
