import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    SimpleChange,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { TEXTS } from "@shared/components/supr-delivery-fee-info/constants";
import { ANALYTICS_OBJECT_NAMES } from "@shared/components/supr-delivery-fee-info/constants/analytics";

import { DeliveryFeeMessageInfo, DeliveryFeeLinkInfo } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-delivery-fee-info-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleClose?.emit()"
                modalName="${MODAL_NAMES.DELIVERY_FEE_INFO_MODAL}"
            >
                <div class="wrapper">
                    <supr-text type="subtitle">{{
                        delFeeInfo?.title
                    }}</supr-text>
                    <div class="divider16"></div>
                    <ng-container *ngFor="let desc of delFeeArray">
                        <div class="suprRow">
                            <supr-text type="caption"> {{ desc }} </supr-text>
                        </div>
                        <div class="divider16"></div>
                    </ng-container>
                    <ng-container *ngIf="linkInfo">
                        <div class="suprRow">
                            <supr-text
                                type="action14"
                                class="linkText"
                                (click)="handleLinkClick()"
                                saClick
                                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                                    .LEARN_MORE_CLICK}"
                            >
                                {{ linkInfo?.linkText }}
                            </supr-text>
                            <supr-icon
                                name="chevron_right"
                                class="linkIcon"
                            ></supr-icon>
                        </div>
                        <div class="divider16"></div>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-delivery-fee-info.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit, OnChanges {
    @Input() showModal: boolean;
    @Input() delFeeInfo: DeliveryFeeMessageInfo;
    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;
    delFeeArray: string[] = [];
    linkInfo: DeliveryFeeLinkInfo;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["delFeeInfo"])) {
            this.initialize();
        }
    }

    handleLinkClick() {
        if (this.linkInfo) {
            const { url, outsideRedirection } = this.linkInfo;
            if (outsideRedirection) {
                window.open(url, "_system", "location=yes");
            } else {
                this.routerService.goToUrl(url);
            }
        }
    }

    private initialize() {
        const delFeeInfo = this.utilService.getNestedValue(
            this.delFeeInfo,
            "description",
            ""
        );

        const delFeeLinkInfo = this.utilService.getNestedValue(
            this.delFeeInfo,
            "link_info",
            null
        );

        if (delFeeLinkInfo) {
            this.linkInfo = delFeeLinkInfo;
        }

        if (!delFeeInfo) {
            return;
        }

        this.delFeeArray = delFeeInfo.split(/[\s, ]{3}/);
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
