import {
    Input,
    Component,
    OnChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { map } from "rxjs/operators";
import { Observable, combineLatest } from "rxjs";

import {
    Feedback,
    OrderDeliverySlotMap,
    ScheduleDeliverySlotMap,
} from "@models";
import { SupportCalendar } from "@types";

import { ScheduleAdapter as Adapter } from "@shared/adapters/schedule.adapter";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { ScheduleService } from "@services/shared/schedule.service";

@Component({
    selector: "supr-order-delivery-container",
    template: `
        <supr-order-delivery
            [date]="date"
            [multiplePOD]="multiplePOD$ | async"
            [islastAccordion]="islastAccordion"
            [supportCalendarConfig]="supportCalendarConfig"
            [noOrders]="noOrders$ | async"
            [orderFeedback]="orderFeedback"
            [orderHistory]="orderHistory$ | async"
            [scheduleData]="scheduleData$ | async"
            [noOrdersMessage]="noOrdersMessage$ | async"
        >
        </supr-order-delivery>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOrderDeliveryContainer implements OnChanges {
    @Input() date: string;
    @Input() islastAccordion: boolean;
    @Input() orderFeedback: Feedback.OrderFeedback[];
    @Input() supportCalendarConfig: SupportCalendar;

    noOrders$: Observable<boolean>;
    multiplePOD$: Observable<boolean>;
    noOrdersMessage$: Observable<string>;
    scheduleData$: Observable<OrderDeliverySlotMap>;
    orderHistory$: Observable<OrderDeliverySlotMap>;

    constructor(
        private adapter: Adapter,
        private dateService: DateService,
        private utilService: UtilService,
        private scheduleService: ScheduleService
    ) {}

    ngOnChanges() {
        this.setOrderData();
        this.setScheduleData();
        this.multiplePOD$ = this.adapter.multiplePOD$;
        this.noOrdersMessage$ = this.adapter.noOrdersMessage$;
    }

    private setOrderData() {
        if (this.date) {
            this.orderHistory$ = this.adapter.getOrderDeliveryByDate(this.date);

            /* Check if orders or schedules exist for a day, schedules need to be checked
            when orders are not yet processed post hub cutoff time */
            this.noOrders$ = combineLatest([
                this.adapter.getOrderByDate(this.date),
                this.adapter.getScheduleByDate(this.date),
            ]).pipe(
                map(([orderHistory, schedule]) => {
                    const hasOrders =
                        orderHistory &&
                        orderHistory.orders &&
                        orderHistory.orders.length;

                    const hasSchedules =
                        schedule &&
                        (this.utilService.isLengthyArray(schedule.addons) ||
                            this.utilService.isLengthyArray(
                                schedule.subscriptions
                            ));

                    return !hasOrders && !hasSchedules;
                })
            );
        }
    }

    private setScheduleData() {
        if (this.date) {
            const thisDateObject = this.dateService.dateFromText(this.date);
            const daysFromToday = this.dateService.daysFromToday(
                thisDateObject
            );

            if (daysFromToday === 1) {
                this.scheduleData$ = this.adapter
                    .getScheduleDeliveryByDate(this.date)
                    .pipe(
                        map((scheduleDelivery: ScheduleDeliverySlotMap) =>
                            this.scheduleService.getOrderDataFromScheduleDelivery(
                                { ...scheduleDelivery, date: this.date }
                            )
                        )
                    );
            }
        }
    }
}
