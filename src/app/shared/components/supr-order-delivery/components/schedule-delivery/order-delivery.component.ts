import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Complaintv2Store } from "@store/complaintv2";

import {
    ORDER_STATUS_ICONS,
    FEEDBACK_ANALYTICS_OBJECT_NAMES,
    SELF_SERVE,
} from "@constants";

import { Feedback, OrderDeliverySlotMap, DeliveryTimelines } from "@models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { RouterService } from "@services/util/router.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { FeedbackService } from "@services/layout/feedback.service";
import { SettingsService } from "@services/shared/settings.service";
import { FreshChatService } from "@services/integration/freshchat.service";

import { Segment, SupportCalendar, SuprTimelineProps } from "@types";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-order-delivery",
    templateUrl: "./order-delivery.component.html",
    styleUrls: ["./order-delivery.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOrderDeliveryComponent implements OnInit {
    @Input() noOrders: boolean;
    @Input() multiplePOD: boolean;
    @Input() islastAccordion = false;
    @Input() noOrdersMessage: string;
    @Input() supportCalendarConfig: SupportCalendar;

    @Input() set scheduleData(data: OrderDeliverySlotMap) {
        this._scheduleData = data;
        this.setOrderStatus();
        this.setSlotTimelineData();
    }
    get scheduleData(): OrderDeliverySlotMap {
        return this._scheduleData;
    }

    @Input()
    set date(date: string) {
        this._date = date;
        this.setDateInfo();
        this.setAnalyticsInfo();
    }
    get date(): string {
        return this._date;
    }

    @Input() set orderHistory(history: OrderDeliverySlotMap) {
        this._orderHistory = history;
        this.setOrderStatus();
        this.setSlotTimelineData();
    }
    get orderHistory(): OrderDeliverySlotMap {
        return this._orderHistory;
    }

    @Input() set orderFeedback(orderFeedback: Feedback.OrderFeedback[]) {
        this._orderFeedback = orderFeedback;
        this.setOrderFeedbackThanks(orderFeedback);
    }

    get orderFeedback() {
        return this._orderFeedback;
    }

    _day: string;
    _TEXTS = TEXTS;
    _isToday = false;
    _isTomorrow = false;
    _statusIcon: string;
    _orderStatus: string;
    _feedbackThanks: string;
    _slotTimelines: DeliveryTimelines;
    _showOrderFeedbackThanks: boolean;
    _DELIVERY_SLOTS: Array<string> = [];
    _expand: boolean = true;
    _isAccordion: boolean = false;
    complaintList: SuprTimelineProps.LineItem[] = [];
    complaintHeader: SuprTimelineProps.Header;

    saObjectNames = {
        ...SA_OBJECT_NAMES,
        ...FEEDBACK_ANALYTICS_OBJECT_NAMES.IMPRESSION,
    };
    saContextList: Segment.ContextListItem[] = [];

    private _date: string;
    private _scheduleData: OrderDeliverySlotMap;
    private _orderHistory: OrderDeliverySlotMap;
    private _orderFeedback: Feedback.OrderFeedback[];

    constructor(
        private utilService: UtilService,
        private dateService: DateService,
        private routerService: RouterService,
        private settingsService: SettingsService,
        private scheduleService: ScheduleService,
        private feedbackService: FeedbackService,
        private freshChatService: FreshChatService,
        private complaintv2Store: Complaintv2Store
    ) {}

    ngOnInit() {
        const order_feedback = this.feedbackService.getOrderFeedbackSettings();
        this._feedbackThanks = order_feedback.feedbackCalenderThanks;
        this.fetchSettings();
        this.toggleHeader(true);
    }

    getClasses() {
        return {
            today: this._isToday,
            noOrders: this.noOrders,
            tomorrow: this._isTomorrow && !this.noOrders,
        };
    }

    navigateSupport(slot: string) {
        const freshbotConfig = this.fetchSelfServeConfig();
        const date = this.dateService.dateFromText(this.date);
        const days = this.dateService.daysFromToday(date);
        if (
            freshbotConfig &&
            freshbotConfig.enabled &&
            days < 1 &&
            days > -freshbotConfig.orderCalendarToSelfServeDays
        ) {
            this.chatActionButton(slot);
            return;
        }
        this.routerService.goToSupportPage();
    }

    setOrderFeedbackThanks(orderFeedback: any) {
        this._showOrderFeedbackThanks = false;
        if (!this.utilService.isEmpty(orderFeedback)) {
            const isThanks = Object.values(orderFeedback).reduce(
                (acc, feedback: any) => {
                    const { isRequest } = feedback;
                    return acc && isRequest;
                },
                true
            );
            this._showOrderFeedbackThanks = !isThanks;
        }
    }

    getNestedData(slot: string, key: string) {
        if (this.orderHistory) {
            return this.utilService.getNestedValue(
                this.orderHistory,
                `slot_info.${slot}.${key}`,
                ""
            );
        }

        return this.utilService.getNestedValue(
            this.scheduleData,
            `slot_info.${slot}.${key}`,
            ""
        );
    }

    complaintTimeline() {
        const freshbotConfig = this.fetchSelfServeConfig();
        const { orderComplaintsList } = this.complaintv2Store;
        if (freshbotConfig && freshbotConfig.enabled) {
            const complaintList =
                orderComplaintsList && orderComplaintsList.get(this.date);
            if (this.utilService.isLengthyArray(complaintList)) {
                this.complaintHeader = {
                    text: `${complaintList.length} Support Tickets`,
                };
                this.complaintList = complaintList.map((complaint) => {
                    const {
                        complaint_status_timeline,
                        complaint_type_text,
                        complaint_id,
                    } = complaint;
                    const subtitle = `Issue #${complaint_id} | ${complaint_status_timeline.complaint_status_tag} Updated at ${complaint_status_timeline.updated_at}`;
                    return {
                        icon: "approve",
                        title: {
                            text: `${complaint_status_timeline.complaint_status}: ${complaint_type_text}`,
                        },
                        subtitle: {
                            text: subtitle,
                        },
                        checked: true,
                    };
                });
            }
        }
        return orderComplaintsList;
    }

    goToDeliveryProofPage(date: string, slot: string) {
        if (this.multiplePOD) {
            const timeslot = this.getNestedData(slot, "delivery_timeslots");

            this.routerService.goToDeliveryProofV2Page({
                queryParams: {
                    date,
                    slot: timeslot,
                },
            });
        } else {
            this.routerService.goToDeliveryProofPage({
                queryParams: {
                    date,
                    slot,
                },
            });
        }
    }

    private setDayIsToday(date: string) {
        const scheduleDate = this.dateService.dateFromText(date);
        const daysBetween = this.dateService.daysFromToday(scheduleDate);
        this._isToday = !daysBetween;
        this._isTomorrow = daysBetween === 1;
    }

    private fetchSettings() {
        this._DELIVERY_SLOTS = this.settingsService.getSettingsValue(
            "deliverySlotTypes"
        );
    }

    private setAnalyticsInfo() {
        this.saContextList = [
            {
                name: "order-date",
                value: this.date,
            },
        ];
    }

    private setDateInfo() {
        this._day = this.scheduleService.getScheduleDay(this._date);
        this.setDayIsToday(this._date);
    }

    private setOrderStatus() {
        this._orderStatus = this.utilService.getNestedValue(
            this.orderHistory || this.scheduleData,
            "delivery_summary.message"
        );

        const status = this.utilService.getNestedValue(
            this.orderHistory || this.scheduleData,
            "delivery_summary.status"
        );

        if (status) {
            this._statusIcon = ORDER_STATUS_ICONS[status];
        } else {
            this._statusIcon = null;
        }
    }

    toggleHeader(init?: boolean) {
        this._isAccordion =
            this.supportCalendarConfig &&
            this.supportCalendarConfig.isAccordion;
        if (this._isAccordion) {
            if (init && this.islastAccordion) {
                this._expand = true;
            } else {
                this._expand = !this._expand;
            }
        }
    }

    callActionButton() {
        this.freshChatService.callActionButton(
            this.supportCalendarConfig.number
        );
    }

    chatActionButton(slot: string) {
        this.freshChatService.chatActionButton(this._date, slot);
    }

    private setSlotTimelineData() {
        const deliveryTimelines = this.utilService.getNestedValue(
            this.orderHistory || this.scheduleData,
            "delivery_timeline"
        );

        this._slotTimelines = this.scheduleService.getSlotTimelineData(
            deliveryTimelines
        );
    }

    private fetchSelfServeConfig() {
        const selfserveConstants = this.settingsService.getSettingsValue(
            "selfServeConfig",
            SELF_SERVE
        );
        return selfserveConstants;
    }
}
