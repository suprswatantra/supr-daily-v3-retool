import {
    Input,
    Component,
    ChangeDetectionStrategy,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { Observable } from "rxjs";

import { AddonAdapter } from "@shared/adapters/addon.adapter";
import { WalletAdapter } from "@shared/adapters/wallet.adapter";

import { ScheduleItem, Sku } from "@shared/models";
import { AddonCurrentState } from "@store/addon/addon.state";

@Component({
    selector: "supr-addon-edit-card-container",
    template: `
        <supr-schedule-addon-edit-layout
            [sku]="sku"
            [date]="date"
            [schedule]="schedule"
            [walletBalance]="walletBalance$ | async"
            [addonCurrentState]="addonCurrentState$ | async"
            (cancelAddon)="cancelAddon($event)"
            (updateAddon)="updateAddon($event)"
            (updateActionListener)="updateActionListener.emit($event)"
        ></supr-schedule-addon-edit-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprAddonEditCardComponent implements OnInit {
    walletBalance$: Observable<number>;
    addonCurrentState$: Observable<AddonCurrentState>;

    @Input() sku: Sku;
    @Input() date: string;
    @Input() schedule: ScheduleItem;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    constructor(
        private walletAdapater: WalletAdapter,
        private addonAdapter: AddonAdapter
    ) {}

    ngOnInit() {
        this.walletBalance$ = this.walletAdapater.walletBalance$;
        this.addonCurrentState$ = this.addonAdapter.currentState$;
    }

    /* [TODO] Add Model */
    updateAddon(updateData: any) {
        const { addonId, quantity } = updateData;

        this.addonAdapter.updateAddOn(addonId, quantity);
    }

    cancelAddon(addonId: number) {
        this.addonAdapter.cancelAddOn(addonId);
    }
}
