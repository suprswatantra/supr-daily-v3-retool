export const MAP_VARIANTS = {
    DEFAULT: "default",
    READ_ONLY: "readOnly",
};

export const MAP_SETTINGS = {
    resizeTimeOut: 500,
    default: {
        zoom: 18,
        zoomControl: false,
        defaultInfo: false,
        doubleClickZoom: true,
        disableDefaultUI: true,
        streetViewControl: false,
    },
    static: {
        zoom: 18,
        usePanning: true,
        zoomControl: false,
        defaultInfo: false,
        disableDefaultUI: true,
        streetViewControl: false,
    },
};

const MAP_MARKER_ICON_URL =
    "https://suprdaily-address-directory.s3-ap-southeast-1.amazonaws.com/GeoLocatorEdit.svg";

export const MAP_MARKER_OPTION = {
    url: MAP_MARKER_ICON_URL,
    // We can set scale of image too
    // scaledSize: {
    //     width: 30,
    //     height: 40,
    // },
};
