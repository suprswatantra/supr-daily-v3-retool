import { MapComponent } from "./supr-map.component";
import { StaticMapComponent } from "./components/static-map/static-map.component";
import { DefaultMapComponent } from "./components/default-map/default-map.component";
import { LocationMarkerComponent } from "./components/location-marker/location-marker.component";

export const mapComponents = [
    MapComponent,
    StaticMapComponent,
    DefaultMapComponent,
    LocationMarkerComponent,
];
