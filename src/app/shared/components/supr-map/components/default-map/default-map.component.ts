import {
    Input,
    Output,
    Component,
    ViewChild,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
} from "@angular/core";

import { AgmMap } from "@agm/core";

import { LOCATION_AUTHORIZATION_STATUS } from "@constants";

import { LocationService } from "@services/util/location.service";
import { PlatformService } from "@services/util/platform.service";
import { AddressService } from "@services/shared/address.service";

import { SuprMaps } from "@types";

import { MAP_SETTINGS } from "../../constants/map.constants";
import { SA_OBJECT_NAME } from "../../constants/analytics.constants";

@Component({
    selector: "supr-map-default",
    templateUrl: "./default-map.component.html",
    styleUrls: ["../../styles/default-map.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefaultMapComponent implements OnChanges {
    @Input() cityId: number;
    @Input() markerText: string;
    @Input() unserviceable: boolean;
    @Input() location: SuprMaps.Location;
    @Input() goToCurrentLocation: boolean;
    @Input() checkingServiceability: boolean;

    @ViewChild(AgmMap, { static: true }) myMap: AgmMap;

    @Output() handleMapLocationChange: EventEmitter<
        SuprMaps.MapComponentLocationInfo
    > = new EventEmitter();

    tempLocation: SuprMaps.Location;
    mapSettings = MAP_SETTINGS.default;
    saObjectName = SA_OBJECT_NAME;

    // private googleMapInstance: any;

    constructor(
        private cdr: ChangeDetectorRef,
        private addressService: AddressService,
        private locationService: LocationService,
        private platformService: PlatformService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        // this.handleLocationChanges(changes["location"]);
        this.handleCurrentLocationFlagChanges(changes["goToCurrentLocation"]);
    }

    mapReady(map: any) {
        // this.googleMapInstance = map;
        this.myMap.zoom = this.mapSettings.zoom;
        map.addListener("dragend", () => {
            this.updateMapLocationInfo(this.tempLocation);
        });
    }

    centerChange(e: any) {
        this.tempLocation = { lat: e.lat, lng: e.lng };
    }

    async moveToCurrentLocation() {
        /* Checking for Location Service Enabled/Disabled on iOS */
        try {
            const locationStatus = await this.locationService.getLocationStatus();
            if (
                this.platformService.isIOS() &&
                (locationStatus ===
                    LOCATION_AUTHORIZATION_STATUS.ios.DISABLED ||
                    locationStatus ===
                        LOCATION_AUTHORIZATION_STATUS.ios.UNAUTHORIZED)
            ) {
                return this.handleMapLocationChange.emit({
                    authorization: locationStatus,
                });
            }

            const location = await this.fetchCurrentLocation();
            this.updateMapLocationInfo(location);
        } catch (e) {
            this.returnCityDefault();
        }
    }

    /* private handleLocationChanges(change: SimpleChange) {
        if (
            change &&
            !change.firstChange &&
            change.currentValue &&
            !this.locationService.compareLocations(
                change.currentValue,
                change.previousValue,
                5
            )
        ) {
            this.googleMapInstance.setZoom(this.mapSettings.zoom);
        }
    } */

    private returnCityDefault() {
        const cityId =
            this.cityId ||
            this.addressService.getAddressData("savedAddress.city.id");

        if (!cityId) {
            return this.handleMapLocationChange.emit({
                authorization: LOCATION_AUTHORIZATION_STATUS.android.DENIED,
            });
        }

        const cityInfo = this.locationService.getCityLocation(cityId);
        this.updateMapLocationInfo(cityInfo);
    }

    private handleCurrentLocationFlagChanges(change: SimpleChange) {
        if (change && change.currentValue) {
            this.moveToCurrentLocation();
        }
    }

    private updateMapLocationInfo(location: SuprMaps.Location) {
        this.location = location;
        this.cdr.detectChanges();

        return this.handleMapLocationChange.emit({
            location,
        });
    }

    private async fetchCurrentLocation(): Promise<SuprMaps.Location> {
        return this.locationService
            .getLocationAccess()
            .then(async (accessGranted: boolean) => {
                if (accessGranted) {
                    const location = await this.locationService.getCurrentLocation();
                    return location;
                } else {
                    return Promise.reject();
                }
            });
    }
}
