import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { SuprMaps } from "@types";

import { MAP_SETTINGS, MAP_MARKER_OPTION } from "../../constants/map.constants";
import { SA_OBJECT_NAME } from "../../constants/analytics.constants";

@Component({
    selector: "supr-map-static",
    templateUrl: "./static-map.component.html",
    styleUrls: ["../../styles/default-map.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StaticMapComponent {
    @Input() location: SuprMaps.Location;

    mapSettings = MAP_SETTINGS.static;

    mapMarkerOption = MAP_MARKER_OPTION;

    saObjectName = SA_OBJECT_NAME;

    private googleMapsInstance: any;

    @Output() handleMarkerClick: EventEmitter<void> = new EventEmitter();

    constructor() {}

    mapReady(map: any) {
        this.googleMapsInstance = map;
        this.googleMapsInstance.zoom = this.mapSettings.zoom;
    }

    async recenterMap() {
        this.googleMapsInstance.panTo(this.location);
        this.googleMapsInstance.setZoom(this.mapSettings.zoom);
    }
}
