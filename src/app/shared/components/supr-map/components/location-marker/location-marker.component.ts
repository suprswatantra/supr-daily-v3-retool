import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-map-location-marker",
    template: `
        <div class="wrapper suprColumn center">
            <supr-chip [class.error]="error">
                <supr-loader *ngIf="updating"></supr-loader>
                <supr-text *ngIf="!updating" type="paragraph">{{
                    text
                }}</supr-text>
            </supr-chip>
            <div class="pin" [class.error]="error"></div>
            <div class="base" [class.error]="error"></div>
        </div>
    `,
    styleUrls: ["../../styles/location-marker.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationMarkerComponent {
    @Input() text: string;
    @Input() error: boolean;
    @Input() updating: boolean;
}
