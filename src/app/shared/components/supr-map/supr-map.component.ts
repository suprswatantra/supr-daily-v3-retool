import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { AnalyticsService } from "@services/integration/analytics.service";
import { SuprMaps, Segment } from "@types";

import { MAP_VARIANTS } from "./constants/map.constants";
import { SA_OBJECT_NAME } from "./constants/analytics.constants";

@Component({
    selector: "supr-map",
    template: `
        <supr-map-static
            *ngIf="variant === VARIANTS.READ_ONLY"
            [location]="location"
            (handleMarkerClick)="handleStaticMarkerClick.emit($event)"
        >
        </supr-map-static>
        <supr-map-default
            *ngIf="!variant || variant === VARIANTS.DEFAULT"
            [cityId]="cityId"
            [location]="location"
            [markerText]="markerText"
            [unserviceable]="unserviceable"
            [goToCurrentLocation]="moveToCurrentLocation"
            [checkingServiceability]="checkingServiceability"
            (handleMapLocationChange)="locationChange($event)"
        >
        </supr-map-default>
    `,
    styleUrls: ["./styles/supr-map.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapComponent {
    @Input() cityId: number;
    @Input() markerText: string;
    @Input() unserviceable = false;
    @Input() location: SuprMaps.Location;
    @Input() checkingServiceability = true;
    @Input() moveToCurrentLocation: boolean;
    @Input() variant = MAP_VARIANTS.DEFAULT;

    @Output() handleMapLocationChange: EventEmitter<
        SuprMaps.MapComponentLocationInfo
    > = new EventEmitter();
    @Output() handleStaticMarkerClick: EventEmitter<void> = new EventEmitter();

    constructor(private analyticsService: AnalyticsService) {}

    VARIANTS = MAP_VARIANTS;

    locationChange(event) {
        const trait: Segment.Traits = {
            objectName: SA_OBJECT_NAME.CLICK.MOVE_DRAG_PIN,
        };

        this.analyticsService.trackClick(trait);

        this.handleMapLocationChange.emit(event);
    }
}
