import { CreateSubscriptionComponent } from "./supr-create-subscription.component";

import { HeaderComponent } from "./components/header/header.component";
import { ProductTileComponent } from "./components/header/product-tile.component";
import { StepsComponent } from "./components/header/steps.component";

import { ContentComponent } from "./components/content/content.component";
import { NextComponent } from "./components/content/next.component";
import { QuantityComponent } from "./components/quantity/quantity.component";
import { FrequencyComponent } from "./components/frequency/frequency.component";
import { DeliveriesComponent } from "./components/deliveries/deliveries.component";
import { StartDateComponent } from "./components/start-date/start-date.component";
import { CalendarModalComponent } from "./components/start-date/calendar-modal.component";
import { ReviewComponent } from "./components/review/review.component";

import { CalendarModalContainer } from "./containers/calendar-modal.container";

export const createSubscriptionComponents = [
    CreateSubscriptionComponent,

    HeaderComponent,
    ProductTileComponent,
    StepsComponent,

    ContentComponent,
    NextComponent,
    QuantityComponent,
    FrequencyComponent,
    DeliveriesComponent,
    StartDateComponent,
    CalendarModalComponent,
    ReviewComponent,

    CalendarModalContainer,
];
