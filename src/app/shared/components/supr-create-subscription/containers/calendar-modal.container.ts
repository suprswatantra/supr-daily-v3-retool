import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Observable } from "rxjs";

import { Vacation } from "@models";
import { PauseAdapter as Adapter } from "@shared/adapters/pause.adapter";

@Component({
    selector: "supr-create-subscription-calendar-modal-container",
    template: `
        <supr-create-subscription-calendar-modal
            [deliveryDate]="deliveryDate"
            [vacation]="vacation$ | async"
            (handleSelectDate)="handleSelectDate.emit($event)"
        ></supr-create-subscription-calendar-modal>
    `,
})
export class CalendarModalContainer implements OnInit {
    @Input() deliveryDate: string;
    @Output() handleSelectDate: EventEmitter<string> = new EventEmitter();

    vacation$: Observable<Vacation>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.vacation$ = this.adapter.vacation$;
    }
}
