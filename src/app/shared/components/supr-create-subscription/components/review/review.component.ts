import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { CartItem, Sku } from "@models";
import { QuantityService } from "@services/util/quantity.service";
import { UtilService } from "@services/util/util.service";

import { TEXTS } from "./../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-review",
    template: `
        <div class="review">
            <div class="suprRow headerEdit" (click)="handleEdit.emit(1)">
                <supr-icon name="chevron_left"></supr-icon>
                <supr-text type="subtext10">
                    {{ TEXTS.EDIT_SUBSCRIPTION }}
                </supr-text>
            </div>
            <div class="divider16"></div>

            <supr-text type="subheading">
                {{ TEXTS.REVIEW_SUBSCRIPTION_DETAILS }}
            </supr-text>
            <div class="divider16"></div>

            <div class="suprRow">
                <supr-text type="caption">
                    {{ TEXTS.PRICE_PER_DELIVERY }} ({{ _pricePerDeliveryStr }})
                </supr-text>
                <supr-text type="caption">
                    {{ _pricePerDeliveryVal | rupee }}
                </supr-text>
            </div>
            <div class="divider16"></div>

            <div class="suprRow">
                <supr-text type="caption">
                    {{ TEXTS.TOTAL_NUMBER_OF_DELIVERIES }}
                </supr-text>
                <supr-text type="caption">
                    {{ deliveries }}
                </supr-text>
            </div>
            <div class="divider16"></div>

            <div class="suprRow">
                <supr-text type="caption">
                    {{ TEXTS.FREQUENCY_OF_DELIVERY }}
                </supr-text>
                <supr-week-days-small
                    *ngIf="_frequency === 'custom'; else frequencyText"
                    [selectedDays]="item?.frequency"
                >
                </supr-week-days-small>

                <ng-template #frequencyText>
                    <supr-text type="caption">
                        {{ _frequency }}
                    </supr-text>
                </ng-template>
            </div>
            <div class="divider16"></div>

            <div class="suprRow">
                <supr-text type="caption">
                    {{ TEXTS.START_DATE }}
                </supr-text>
                <supr-text type="caption">
                    {{ _startDate | date: "d LLL, yyyy" }}
                </supr-text>
            </div>
            <div class="divider16"></div>

            <div class="suprRow">
                <supr-text type="body">
                    {{ TEXTS.TOTAL_AMOUNT }}
                </supr-text>
                <supr-text type="body">
                    {{ _totalAmount | rupee }}
                </supr-text>
            </div>
            <div class="divider80"></div>

            <supr-button
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .CONFIRM_ADD_TO_CART}"
                [saObjectValue]="sku?.id"
                [saContext]="categoryId"
                (handleClick)="handleConfirm.emit()"
            >
                <supr-text type="body">
                    {{ TEXTS.CONFIRM_N_ADD_TO_CART }}
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/review.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewComponent implements OnInit {
    @Input() sku: Sku;
    @Input() categoryId: number;
    @Input() item: CartItem;
    @Input() deliveries: number;
    @Output() handleEdit: EventEmitter<number> = new EventEmitter();
    @Output() handleConfirm: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
    _startDate: string;
    _frequency: string;
    _pricePerDeliveryStr: string;
    _pricePerDeliveryVal: number;
    _totalAmount: number;

    constructor(
        private utilService: UtilService,
        private quantityService: QuantityService
    ) {}

    ngOnInit() {
        this._startDate = this.item.start_date;
        this._setFrequency();
        this._setPricePerDelivery();
        this._setTotalAmount();
    }

    private _setFrequency() {
        this._frequency = this.utilService.getWeekDaysFormatFromDays(
            this.item.frequency
        );
    }

    private _setPricePerDelivery() {
        const qty = this.item.quantity;
        if (this.sku) {
            const price = this.utilService.getNestedValue(
                this.sku,
                "unit_price"
            );

            this._pricePerDeliveryStr = this.quantityService.toText(
                this.sku,
                qty
            );
            this._pricePerDeliveryVal = qty * price;
        }
    }

    private _setTotalAmount() {
        if (!this.sku) {
            return;
        }
        this._totalAmount = this.sku.unit_price * this.item.recharge_quantity;
    }
}
