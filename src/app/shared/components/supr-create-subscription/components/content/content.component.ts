import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    TemplateRef,
    OnInit,
    Output,
    EventEmitter,
} from "@angular/core";

import { CartItem, Sku, Vacation } from "@models";
import { Frequency } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-content",
    template: `
        <div class="content">
            <ng-container [ngTemplateOutlet]="_templateRef"></ng-container>

            <ng-template #screen1>
                <supr-create-subscription-quantity
                    [quantity]="item?.quantity"
                    [sku]="sku"
                    (handleBuyOnce)="handleBuyOnce.emit()"
                    (handleQtyChange)="handleQtyChange.emit($event)"
                    (handleNext)="_handleNext($event)"
                    saImpression
                    [saObjectName]="saObjectName"
                ></supr-create-subscription-quantity>
            </ng-template>

            <ng-template #screen2>
                <supr-create-subscription-deliveries
                    [deliveries]="deliveries"
                    (handleDeliveriesChange)="
                        handleDeliveriesChange.emit($event)
                    "
                    (handleNext)="_handleNext($event)"
                    saImpression
                    [saObjectName]="saObjectName"
                ></supr-create-subscription-deliveries>
            </ng-template>

            <ng-template #screen3>
                <supr-create-subscription-frequency
                    [frequency]="item?.frequency"
                    (handleFrequencyChange)="handleFrequencyChange.emit($event)"
                    (handleNext)="_handleNext($event)"
                    saImpression
                    [saObjectName]="saObjectName"
                ></supr-create-subscription-frequency>
            </ng-template>

            <ng-template #screen4>
                <supr-create-subscription-start-date
                    [vacation]="vacation"
                    [startDate]="item?.start_date"
                    (handleStartDateChange)="handleStartDateChange.emit($event)"
                    (handleNext)="_handleNext($event)"
                    saImpression
                    [saObjectName]="saObjectName"
                ></supr-create-subscription-start-date>
            </ng-template>

            <ng-template #screen5>
                <supr-create-subscription-review
                    [sku]="sku"
                    [categoryId]="categoryId"
                    [item]="item"
                    [deliveries]="deliveries"
                    (handleEdit)="handleNext.emit($event)"
                    (handleConfirm)="handleConfirm.emit()"
                    saImpression
                    [saObjectName]="saObjectName"
                ></supr-create-subscription-review>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit {
    @Input() sku: Sku;
    @Input() categoryId: number;
    @Input() item: CartItem;
    @Input() vacation: Vacation;
    @Input() deliveries: number;

    @Input()
    set selectedStep(step: number) {
        this._templateRef = this.getScreenTemplate(step);
        this.setSaObjectName(step);
    }

    @Output() updateStepState: EventEmitter<number> = new EventEmitter();
    @Output() handleBuyOnce: EventEmitter<void> = new EventEmitter();
    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();
    @Output() handleNext: EventEmitter<number> = new EventEmitter();
    @Output()
    handleFrequencyChange: EventEmitter<Frequency> = new EventEmitter();
    @Output()
    handleDeliveriesChange: EventEmitter<number> = new EventEmitter();
    @Output()
    handleStartDateChange: EventEmitter<string> = new EventEmitter();
    @Output() handleConfirm: EventEmitter<void> = new EventEmitter();

    @ViewChild("screen1", { static: true }) screen1Ref: TemplateRef<any>;
    @ViewChild("screen2", { static: true }) screen2Ref: TemplateRef<any>;
    @ViewChild("screen3", { static: true }) screen3Ref: TemplateRef<any>;
    @ViewChild("screen4", { static: true }) screen4Ref: TemplateRef<any>;
    @ViewChild("screen5", { static: true }) screen5Ref: TemplateRef<any>;

    _templateRef: TemplateRef<any>;
    saObjectName: string;

    ngOnInit() {
        this._templateRef = this.screen1Ref;
    }

    _handleNext(step: number) {
        this.handleNext.emit(step);
        this.updateStepState.emit(step - 1);
    }

    private setSaObjectName(step: number) {
        if (step < 5) {
            this.saObjectName = `${ANALYTICS_OBJECT_NAMES.IMPRESSION.CREATE_SUBSCRIPTION_PREFIX}${step}`;
        } else {
            this.saObjectName =
                ANALYTICS_OBJECT_NAMES.IMPRESSION.REVIEW_SUBSCRIPTION;
        }
    }

    private getScreenTemplate(step: number): TemplateRef<any> {
        switch (step) {
            case 1:
                return this.screen1Ref;
            case 2:
                return this.screen2Ref;
            case 3:
                return this.screen3Ref;
            case 4:
                return this.screen4Ref;
            case 5:
                return this.screen5Ref;
            default:
                return this.screen2Ref;
        }
    }
}
