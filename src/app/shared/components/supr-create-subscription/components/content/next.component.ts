import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-next",
    template: `
        <supr-button
            class="next"
            [disabled]="disabled"
            (handleClick)="handleClick.emit()"
            saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                .CREATE_SUBSCRIPTION_NEXT}"
            [saContext]="stepName"
        >
            <supr-text type="body">{{ TEXTS.NEXT }}</supr-text>
        </supr-button>
    `,
    styleUrls: ["../../styles/content.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NextComponent {
    TEXTS = TEXTS;

    @Input() disabled = false;
    @Input() stepName: string;

    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
