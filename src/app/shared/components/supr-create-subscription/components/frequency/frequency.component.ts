import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { Frequency } from "@types";

import { TEXTS } from "./../../constants";

@Component({
    selector: "supr-create-subscription-frequency",
    template: `
        <div class="frequency">
            <supr-text type="subheading">
                {{ TEXTS.WHEN_DO_YOU_WANT_DELIVERY }}
            </supr-text>

            <div class="divider16"></div>

            <supr-frequency-select
                [frequency]="frequency"
                (handleFrequencyChange)="_handleFrequencyChange($event)"
            ></supr-frequency-select>

            <supr-create-subscription-next
                [disabled]="disableNext"
                (handleClick)="handleNext.emit(4)"
                [stepName]="saObjectName"
            ></supr-create-subscription-next>
        </div>
    `,
    styleUrls: ["../../styles/frequency.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FrequencyComponent implements OnInit {
    @Input() frequency: Frequency = {};
    @Input() saObjectName: string;

    @Output()
    handleFrequencyChange: EventEmitter<Frequency> = new EventEmitter();
    @Output() handleNext: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
    disableNext: boolean;

    ngOnInit() {
        this.setNextButtonState();
    }

    _handleFrequencyChange(frequency: Frequency) {
        const days = this.getSelectedDays(frequency);

        this.disableNext = days.length ? false : true;
        this.handleFrequencyChange.emit(frequency);
    }

    private getSelectedDays(frequency: Frequency): string[] {
        const days = [];

        for (const day in frequency) {
            if (frequency[day]) {
                days.push(day);
            }
        }

        return days;
    }

    private setNextButtonState() {
        const days = this.getSelectedDays(this.frequency);
        this.disableNext = days.length ? false : true;
    }
}
