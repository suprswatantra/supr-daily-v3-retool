import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TEXTS } from "./../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-deliveries",
    template: `
        <div class="deliveries">
            <supr-text type="subheading">
                {{ TEXTS.HOW_MANY_DELIVERIES }}
            </supr-text>

            <div class="divider16"></div>

            <supr-recharge-qty-select
                [selectedQty]="deliveries"
                (handleOptionChange)="handleDeliveriesChange.emit($event)"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .CREATE_SUBSCRIPTION_NUM_DELIVERIES}"
            ></supr-recharge-qty-select>

            <supr-create-subscription-next
                (handleClick)="handleNext.emit(3)"
                [stepName]="saObjectName"
            ></supr-create-subscription-next>
        </div>
    `,
    styleUrls: ["../../styles/deliveries.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliveriesComponent {
    @Input() deliveries: number;
    @Input() saObjectName: string;

    @Output()
    handleDeliveriesChange: EventEmitter<number> = new EventEmitter();
    @Output() handleNext: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
}
