import {
    Component,
    Input,
    OnInit,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { Vacation } from "@models";
import { SuprDateService } from "@services/date/supr-date.service";
import { Calendar } from "@types";

import { TEXTS } from "./../../constants";

@Component({
    selector: "supr-create-subscription-calendar-modal",
    template: `
        <div class="calendar">
            <div class="divider24"></div>
            <supr-calendar
                [selectedDate]="_deliveryDate"
                [disablePastDays]="true"
                [vacation]="vacation"
                (handleSelectedDate)="selectNewDate($event)"
            ></supr-calendar>
            <div class="divider24"></div>
            <supr-button (handleClick)="updateDeliveryDate()">
                <supr-text type="body">
                    {{ TEXTS.SET_START_DATE }}
                    ({{ _deliveryDate.dateText | date: "d LLL, yyyy" }})
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../../styles/start-date.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarModalComponent implements OnInit {
    @Input() deliveryDate: string;
    @Input() vacation: Vacation;
    @Output() handleSelectDate: EventEmitter<string> = new EventEmitter();

    _deliveryDate: Calendar.DayData;
    TEXTS = TEXTS;

    constructor(private suprDateService: SuprDateService) {}

    ngOnInit() {
        this.setDeliveryDate();
    }

    selectNewDate(date: Calendar.DayData) {
        this._deliveryDate = date;
    }

    updateDeliveryDate() {
        this.handleSelectDate.emit(this._deliveryDate.dateText);
    }

    private setDeliveryDate() {
        this._deliveryDate = this.suprDateService.suprDateFromText(
            this.deliveryDate
        );
    }
}
