import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

import { T_PLUS_ONE } from "@constants";

import { Vacation } from "@models";

import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";
import { RadioButton } from "@types";

import { START_DATE_OPTIONS, TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-start-date",
    template: `
        <div class="startDate">
            <ng-container *ngIf="_showCalendar; else selectOptions">
                <supr-create-subscription-calendar-modal-container
                    [deliveryDate]="startDate"
                    (handleSelectDate)="handleCalendarSelectedDate($event)"
                ></supr-create-subscription-calendar-modal-container>
            </ng-container>

            <ng-template #selectOptions>
                <supr-text type="subheading">
                    {{ TEXTS.WHEN_DO_YOU_WANT_TO_START }}
                </supr-text>

                <div class="divider16"></div>

                <supr-radio-button-group
                    [options]="dateOptions"
                    colSize="1"
                    [selectedIndex]="selectedIndex"
                    (handleSelect)="onSelectOption($event)"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                        .CREATE_SUBSCRIPTION_CHOOSE_DATE}"
                ></supr-radio-button-group>

                <supr-create-subscription-next
                    (handleClick)="handleNext.emit(5)"
                    [stepName]="saObjectName"
                ></supr-create-subscription-next>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/start-date.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartDateComponent implements OnInit, OnChanges {
    @Input() startDate: string;
    @Input() vacation: Vacation;
    @Input() saObjectName: string;

    @Output()
    handleStartDateChange: EventEmitter<string> = new EventEmitter();
    @Output() handleNext: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;

    tomorrowAllowed = true;
    dateOptions: RadioButton[];
    selectedIndex: number;
    _showCalendar = false;
    tpEnabled = false;

    private _selectedDate: string;

    constructor(
        private dateService: DateService,
        private calendarService: CalendarService
    ) {}

    ngOnInit() {
        this.setTpStatus();
        this.setSelectedDate(this.startDate);
        this.setTomorrowAllowed();
        this.setSelectedIndex();
        this.setDateOptions();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["startDate"];
        if (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        ) {
            this.setDateOptions();
        }
    }

    onSelectOption(selectedOption: number) {
        this.selectedIndex = selectedOption;
        this.handleOptionSelect();
    }

    handleCalendarSelectedDate(date: string) {
        this.hideCalendar();
        this.setSelectedDate(date);
        this.updateSelectedDate();
        this.verifySelectedDate(date);
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private setSelectedDate(date: string) {
        this._selectedDate = date;
    }

    private setTomorrowAllowed() {
        this.tomorrowAllowed = this.calendarService.isTomorrowAvailable(
            this.vacation
        );
    }

    private setSelectedIndex() {
        if (this.tomorrowAllowed && this.isTomorrowDate(this.startDate)) {
            this.selectedIndex = 0;
        } else {
            this.selectedIndex = 1;
        }
    }

    private setDateOptions() {
        this.dateOptions = START_DATE_OPTIONS.map((option, index) => ({
            index,
            text: this.getDateOptionText(index, option.TEXT),
            icon: option.ICON,
            disabled: index === 0 && !this.tomorrowAllowed,
        }));
    }

    private getDateOptionText(index: number, defaultText: string): string {
        if (index === 0 || this.selectedIndex === 0) {
            return `${defaultText} ${
                this.tpEnabled ? T_PLUS_ONE.AVAILABLE_AFTER_FIRST_DELIVERY : ""
            }`;
        }

        const selectedDate = this.dateService.formatDate(
            this.startDate,
            "d LLL yyy"
        );
        return `${TEXTS.CUSTOM_DATE} (${selectedDate})`;
    }

    private showCalendar() {
        this._showCalendar = true;
    }

    private hideCalendar() {
        this._showCalendar = false;
    }

    private handleOptionSelect() {
        if (this.selectedIndex === 0) {
            this._selectedDate = this.tomorrowDateText();
            this.updateSelectedDate();
        } else {
            this.showCalendar();
        }
    }

    private verifySelectedDate(date: string) {
        this.selectedIndex = this.isTomorrowDate(date) ? 0 : 1;
    }

    private isTomorrowDate(date: string): boolean {
        const _date = this.dateService.dateFromText(date);
        const _tomorrow = this.dateService.getTomorrowDate();
        const daysDiff = this.dateService.daysBetweenTwoDates(_date, _tomorrow);
        return daysDiff === 0;
    }

    private tomorrowDateText(): string {
        return this.dateService.textFromDate(
            this.dateService.getTomorrowDate()
        );
    }

    private updateSelectedDate() {
        this.handleStartDateChange.emit(this._selectedDate);
    }
}
