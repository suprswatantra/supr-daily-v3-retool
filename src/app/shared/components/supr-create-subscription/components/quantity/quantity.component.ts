import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    OnInit,
    EventEmitter,
    SimpleChanges,
    OnChanges,
} from "@angular/core";

import { Sku } from "@models";
import { QuantityService } from "@services/util/quantity.service";

import { TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-quantity",
    template: `
        <div class="quantity">
            <supr-text class="top" type="subheading">
                {{ TEXTS.NO_OF_UNITS_PER_DELIVERY }}
            </supr-text>

            <div class="divider24"></div>

            <div class="suprRow">
                <div class="quantityUnit">
                    <supr-text type="body">{{ _qtyStr }}</supr-text>
                    <supr-text type="body">{{ TEXTS.PER_DELIVERY }}</supr-text>
                </div>
                <supr-number-counter
                    [quantity]="quantity"
                    [minusDisabled]="_minusDisabled"
                    (handleClick)="onQtyUpdate($event)"
                    type="flat"
                >
                </supr-number-counter>
            </div>

            <div class="footer">
                <supr-text class="noSubscribe" type="body">
                    {{ TEXTS.DONT_WANT_TO_SUBSCRBE }}
                </supr-text>

                <div class="suprRow">
                    <supr-text
                        class="buyTomorrow"
                        type="caption"
                        (click)="handleBuyOnce.emit()"
                        saClick
                        saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                            .CREATE_SUBSCRIPTION_DELIVER_ONCE}"
                    >
                        {{ TEXTS.DELIVER_ONCE | uppercase }}
                    </supr-text>

                    <supr-icon name="chevron_right"></supr-icon>
                </div>

                <div class="divider24"></div>
                <supr-create-subscription-next
                    (handleClick)="handleNext.emit(2)"
                    [stepName]="saObjectName"
                ></supr-create-subscription-next>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/quantity.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuantityComponent implements OnInit, OnChanges {
    @Input() quantity: number;
    @Input() sku: Sku;
    @Input() saObjectName: string;

    @Output() handleBuyOnce: EventEmitter<void> = new EventEmitter();
    @Output() handleQtyChange: EventEmitter<number> = new EventEmitter();
    @Output() handleNext: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
    _qtyStr: string;
    _minusDisabled = false;

    constructor(private quantityService: QuantityService) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["quantity"];
        if (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        ) {
            this.initData();
        }
    }

    onQtyUpdate(direction: number) {
        let newQty = this.quantity;
        if (direction > 0) {
            newQty++;
        } else if (newQty > 1 && direction < 0) {
            newQty--;
        }

        this.setMinusDisabled(newQty);
        this.handleQtyChange.emit(newQty);
    }

    private initData() {
        this._qtyStr = this.quantityService.toText(this.sku, this.quantity);
        this.setMinusDisabled(this.quantity);
    }

    private setMinusDisabled(qty: number) {
        this._minusDisabled = qty === 1;
    }
}
