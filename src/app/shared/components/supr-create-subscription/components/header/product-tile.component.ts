import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { Sku } from "@models";
import { QuantityService } from "@services/util/quantity.service";

@Component({
    selector: "supr-create-subscription-product-tile",
    template: `
        <div class="suprRow headerTile">
            <div class="suprColumn image stretch top">
                <supr-image
                    [src]="sku?.image?.fullUrl"
                    [lazyLoad]="false"
                    [image]="sku?.image"
                    [imgHeight]="${CLODUINARY_IMAGE_SIZE.DEFAULT.HEIGHT}"
                    [imgWidth]="${CLODUINARY_IMAGE_SIZE.DEFAULT.WIDTH}"
                ></supr-image>
            </div>

            <div class="spacer16"></div>

            <div class="suprColumn content stretch center left">
                <supr-text class="name" type="heading">
                    {{ sku?.sku_name }}
                </supr-text>

                <div class="divider14"></div>

                <div class="suprRow baseline">
                    <supr-text type="body">
                        {{ sku?.unit_price | rupee }}
                    </supr-text>
                    <div *ngIf="showPriceStrike" class="spacer8"></div>
                    <div class="suprRow baseline">
                        <supr-text
                            class="itemPriceStrike"
                            type="caption"
                            *ngIf="showPriceStrike"
                        >
                            {{ sku?.unit_mrp | rupee: 2 }}
                        </supr-text>
                        <div class="spacer4"></div>
                        <div class="suprRow center dotBetween"></div>
                        <div class="spacer4"></div>
                        <supr-text class="quantityText" type="subtext10">
                            {{ quantityText }}
                        </supr-text>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTileComponent {
    @Input() quantity: number;
    @Input() deliveries: number;
    @Input() set sku(data: Sku) {
        this._sku = data;
        this.initData();
    }
    get sku(): Sku {
        return this._sku;
    }

    quantityText: string;
    showPriceStrike = false;

    private _sku: Sku;

    constructor(private quantityService: QuantityService) {}

    private initData() {
        if (this._sku) {
            this.quantityText = this.quantityService.toText(this._sku);
            this.showPriceStrike = this._sku.unit_price < this._sku.unit_mrp;
        }
    }
}
