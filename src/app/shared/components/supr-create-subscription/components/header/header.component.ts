import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { Sku } from "@models";
import { SubscriptionStepsState } from "@types";

import { TEXTS } from "../../constants";
import { ANALYTICS_OBJECT_NAMES } from "../../constants/analytics.constants";

@Component({
    selector: "supr-create-subscription-header",
    template: `
        <div class="header">
            <supr-create-subscription-product-tile
                [sku]="sku"
                [quantity]="quantity"
                [deliveries]="deliveries"
            ></supr-create-subscription-product-tile>

            <div *ngIf="!_hideSteps; else divider">
                <div class="divider"></div>
                <div class="divider16"></div>
                <supr-create-subscription-header-steps
                    [stepsStates]="stepsStates"
                    [selectedStep]="_selectedStep"
                    (handleStepClick)="handleStepChange.emit($event)"
                    saClick
                    [saObjectName]="getSaObjectName()"
                ></supr-create-subscription-header-steps>
                <div class="divider24"></div>
            </div>

            <ng-template #divider>
                <div class="divider8"></div>
            </ng-template>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    @Input() sku: Sku;
    @Input() quantity: number;
    @Input() deliveries: number;
    @Input() stepsStates: SubscriptionStepsState;

    @Input()
    set selectedStep(step: number) {
        this._selectedStep = step || 1;
        this._hideSteps = step === 5;
    }

    @Output() handleStepChange: EventEmitter<number> = new EventEmitter();

    TEXTS = TEXTS;
    _hideSteps = false;
    _selectedStep: number;

    getSaObjectName() {
        return `${ANALYTICS_OBJECT_NAMES.CLICK.CREATE_SUBSCRIPTION_STEP_PREFIX}${this._selectedStep}`;
    }
}
