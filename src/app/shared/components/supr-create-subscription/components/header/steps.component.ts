import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { SubscriptionStepsState } from "@types";

import { STEPS } from "../../constants";

@Component({
    selector: "supr-create-subscription-header-steps",
    template: `
        <div class="headerSteps suprRow spaceBetween">
            <ng-container
                *ngFor="let step of steps; trackBy: trackByFn; index as _step"
            >
                <div
                    class="suprColumn center step enabled"
                    (click)="_handleStepClick(_step)"
                >
                    <supr-text type="subheading">{{ step.line1 }}</supr-text>
                    <supr-text type="caption">{{ step.line2 }}</supr-text>
                    <supr-text type="caption">{{ step.line3 }}</supr-text>
                </div>
            </ng-container>
        </div>
        <div class="suprRow stepMarkerWrapper spaceBetween">
            <ng-container
                *ngFor="let step of steps; trackBy: trackByFn; index as _step"
            >
                <div class="suprColumn center">
                    <div
                        class="stepMarker"
                        [class.active]="_step === _currentStep - 1"
                    ></div>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StepsComponent {
    @Input()
    set selectedStep(value: number) {
        this._currentStep = value || 1;
    }
    @Input() stepsStates: SubscriptionStepsState;

    @Output() handleStepClick: EventEmitter<number> = new EventEmitter();

    steps = STEPS;
    _currentStep: number;

    trackByFn(index: number) {
        return index;
    }

    _handleStepClick(stepIndex: number) {
        this.handleStepClick.emit(stepIndex + 1);
    }
}
