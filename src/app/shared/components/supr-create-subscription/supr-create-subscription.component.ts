import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    OnDestroy,
} from "@angular/core";

import { Platform } from "@ionic/angular";

import { Subscription } from "rxjs";

import {
    CART_ITEM_TYPES,
    FREQUENCY_OPTIONS,
    DEFAULT_RECHARGE_OPTION,
} from "@constants";

import { CartItem, Sku, Vacation } from "@models";

import { DateService } from "@services/date/date.service";
import { CartService } from "@services/shared/cart.service";
import { ModalService } from "@services/layout/modal.service";
import { CalendarService } from "@services/date/calendar.service";
import { RouterService } from "@services/util/router.service";

import { Frequency, SubscriptionStepsState } from "@types";

import {
    SUBSCRIPTION_STEPS_ENABLED,
    SUBSCRIPTION_STEPS_DIRTY_STATE,
} from "./constants";

@Component({
    selector: "supr-create-subscription",
    template: `
        <div class="subscription">
            <supr-create-subscription-header
                [sku]="sku"
                [selectedStep]="_selectedStep"
                [quantity]="_subscriptionItem?.quantity"
                [deliveries]="deliveries"
                [stepsStates]="stepsStates"
                (handleStepChange)="_changeStep($event)"
            ></supr-create-subscription-header>
            <supr-create-subscription-content
                [sku]="sku"
                [categoryId]="categoryId"
                [vacation]="vacation"
                [deliveries]="deliveries"
                [item]="_subscriptionItem"
                [selectedStep]="_selectedStep"
                (handleBuyOnce)="_handleBuyOnce()"
                (handleQtyChange)="_handleQtyChange($event)"
                (handleFrequencyChange)="_handleFrequencyChange($event)"
                (handleDeliveriesChange)="_handleDeliveriesChange($event)"
                (handleStartDateChange)="_handleStartDateChange($event)"
                (handleNext)="_changeStep($event)"
                (handleConfirm)="_handleConfirm()"
                (updateStepState)="_updateStepState($event)"
            ></supr-create-subscription-content>
        </div>
    `,
    styleUrls: ["./styles/supr-create-subscription.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateSubscriptionComponent implements OnInit, OnDestroy {
    @Input() sku: Sku;
    @Input() categoryId: number;
    @Input() item: CartItem;
    @Input() vacation: Vacation;
    @Input() fromCatalog: boolean;
    @Output() handleConfirm: EventEmitter<CartItem> = new EventEmitter();

    _selectedStep = 1;
    _subscriptionItem: CartItem;
    deliveries = DEFAULT_RECHARGE_OPTION;
    stepsStates: SubscriptionStepsState;

    private backBtnSubscription: Subscription;

    constructor(
        private cartService: CartService,
        private modalService: ModalService,
        private dateService: DateService,
        private calendarService: CalendarService,
        private routerService: RouterService,
        private platform: Platform,
        private changeDetectorRef: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this._initSubscriptionItem();
        this.registerBackButton();
    }

    ngOnDestroy() {
        this.deRegisterBackButton();
    }

    /* for all screens */
    _changeStep(step: number) {
        this._selectedStep = step;
    }

    /* Quantity Screen */
    _handleBuyOnce() {
        if (this.isSubscriptionItem()) {
            const item = this.cartService.getAddonItemFromSubscriptionItem(
                this.item
            );
            this._confirmItem(item);
        } else if (this.fromCatalog) {
            const qty = this._subscriptionItem.quantity;
            const item = this.cartService.getCartItemFromSku(
                this.sku,
                qty,
                this.vacation
            );
            this._confirmItem(item);
        } else {
            this.modalService.closeModal();
        }
    }

    _handleQtyChange(quantity: number) {
        this._subscriptionItem = {
            ...this._subscriptionItem,
            quantity,
            recharge_quantity: quantity * this.deliveries,
        };
    }

    /* Frequency Screen */
    _handleFrequencyChange(frequency: Frequency) {
        this._subscriptionItem = { ...this._subscriptionItem, frequency };
    }

    /* Deliveries Screen */
    _handleDeliveriesChange(deliveries: number) {
        this.deliveries = deliveries;

        this._subscriptionItem = {
            ...this._subscriptionItem,
            recharge_quantity: this._subscriptionItem.quantity * deliveries,
        };

        this.changeDetectorRef.detectChanges();
    }

    /* Start Date Screen */
    _handleStartDateChange(date: string) {
        this._subscriptionItem = {
            ...this._subscriptionItem,
            start_date: date,
        };
    }

    /* Review screen */
    _handleConfirm() {
        this._confirmItem(this._subscriptionItem);
    }

    /* Set step's dirty state on navigation by next */
    _updateStepState(step: number) {
        this.stepsStates[step] = true;
    }

    /** Private methods */
    private _confirmItem(item: CartItem) {
        this.handleConfirm.emit(item);
        this.modalService.closeModal();
    }

    private _initSubscriptionItem() {
        if (!this.fromCatalog && this.isSubscriptionItem()) {
            this.stepsStates = { ...SUBSCRIPTION_STEPS_ENABLED };
            this.deliveries = this.item.recharge_quantity / this.item.quantity;
            this._subscriptionItem = { ...this.item };
        } else {
            this.stepsStates = { ...SUBSCRIPTION_STEPS_DIRTY_STATE };
            this._subscriptionItem = {
                type: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
                quantity: 1,
                sku_id: this.sku.id,
                frequency: FREQUENCY_OPTIONS[0].FREQUENCY,
                recharge_quantity: DEFAULT_RECHARGE_OPTION,
                start_date: this.getStartDate(),
            };
        }
    }

    private registerBackButton() {
        this.disableBackButton();
        this.backBtnSubscription = this.platform.backButton.subscribeWithPriority(
            2000,
            () => this.handleBackButtonClick()
        );
    }

    private deRegisterBackButton() {
        if (this.backBtnSubscription && !this.backBtnSubscription.closed) {
            this.backBtnSubscription.unsubscribe();
        }

        this.enableBackButton();
    }

    private isSubscriptionItem(): boolean {
        return this.item && this.item.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW;
    }

    private getStartDate(): string {
        const date = this.calendarService.getNextAvailableDate(this.vacation);
        return this.dateService.formatDate(date, "yyyy-M-d");
    }

    private enableBackButton() {
        this.routerService.setIgnoreBackButton(false);
    }

    private disableBackButton() {
        this.routerService.setIgnoreBackButton(true);
    }

    private handleBackButtonClick() {
        if (this._selectedStep === 1) {
            this.modalService.closeModal();
        } else if (this._selectedStep === 5) {
            this._selectedStep = 1;
        } else {
            this._selectedStep -= 1;
        }

        this.changeDetectorRef.detectChanges();
    }
}
