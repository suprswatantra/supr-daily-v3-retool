export const TEXTS = {
    NO_OF_UNITS_PER_DELIVERY: "How many units do you want per delivery?",
    PER_DELIVERY: "per delivery",
    DONT_WANT_TO_SUBSCRBE: "Don’t want to subscribe?",
    DELIVER_ONCE: "DELIVER ONCE",
    NEXT: "Next",
    WHEN_DO_YOU_WANT_DELIVERY: "How often do you want your deliveries?",
    CHOOSE_DAYS: "Choose days",
    HOW_MANY_DELIVERIES: "How many deliveries do you want?",
    WHEN_DO_YOU_WANT_TO_START: "When do you want to start?",
    CHOOSE_START_DATE: "Choose start date",
    SET_START_DATE: "Set start date",
    EDIT_SUBSCRIPTION: "CHANGE SETTINGS",
    SUBSCRIPTION_DETAILS: "Subscription details",
    STARTS: "Starts",
    FREQUENCY_OF_DELIVERY: "Delivery frequency",
    TOTAL_NUMBER_OF_DELIVERIES: "Total number of deliveries",
    PRICE_PER_DELIVERY: "Price per delivery",
    TOTAL_AMOUNT: "Total amount",
    DISCOUNT_AMOUNT: "Discount",
    FINAL_AMOUNT: "Final amount",
    CONFIRM_N_ADD_TO_CART: "Confirm & Add to cart",
    CUSTOM_DATE: "Custom date",
    REVIEW_SUBSCRIPTION_DETAILS: "Review subscription details",
    START_DATE: "Start date",
};

export const STEPS = [
    {
        line1: "01",
        line2: "Quantity",
        line3: "per delivery",
    },
    {
        line1: "02",
        line2: "Total",
        line3: "deliveries",
    },
    {
        line1: "03",
        line2: "Delivery",
        line3: "frequency",
    },
    {
        line1: "04",
        line2: "Subscription",
        line3: "start date",
    },
];

export const START_DATE_OPTIONS = [
    {
        type: "tomorrow",
        TEXT: "Start tomorrow",
        ICON: "",
    },
    {
        type: "custom",
        TEXT: "Choose start date",
        ICON: "calendar",
    },
];

export const SUBSCRIPTION_STEPS_DIRTY_STATE = {
    0: true,
    1: false,
    2: false,
    3: false,
};

export const SUBSCRIPTION_STEPS_ENABLED = {
    0: true,
    1: true,
    2: true,
    3: true,
};
