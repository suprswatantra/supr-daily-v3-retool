export const ANALYTICS_OBJECT_NAMES = {
    CLICK: {
        CREATE_SUBSCRIPTION_DELIVER_ONCE: "buy-one-day",
        CREATE_SUBSCRIPTION_NEXT: "next-button",
        CREATE_SUBSCRIPTION_NUM_DELIVERIES: "num-deliveries",
        CREATE_SUBSCRIPTION_STEP_PREFIX: "button-step",
        CREATE_SUBSCRIPTION_DELIVERY_FREQ: "delivery-frequency",
        CREATE_SUBSCRIPTION_WEEKDAYS: "weekdays",
        CREATE_SUBSCRIPTION_CHOOSE_DATE: "choose-date",
        CONFIRM_ADD_TO_CART: "confirm-subscription",
    },
    IMPRESSION: {
        CREATE_SUBSCRIPTION_WEEKDAYS: "weekdays",
        CREATE_SUBSCRIPTION_PREFIX: "create-subscription-",
        REVIEW_SUBSCRIPTION: "review-subscription",
    },
};
