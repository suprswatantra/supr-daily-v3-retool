import {
    Component,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
} from "@angular/core";

import { ImageBanner, ImageBannerContent } from "@shared/models";

@Component({
    selector: "supr-img-banner",
    template: `
        <div class="imageBannerContainer" *ngIf="imgBanner?.content">
            <ng-container
                *ngFor="
                    let imgBannerContent of imgBanner.content;
                    last as isLast
                "
            >
                <div
                    class="imgWrapper"
                    *ngIf="imgBannerContent?.imgUrl"
                    (click)="handleImgBannerClick.emit(imgBannerContent)"
                >
                    <supr-image
                        [src]="imgBannerContent?.imgUrl"
                        [useDirectUrl]="true"
                    ></supr-image>
                </div>
                <div class="divider16" *ngIf="!isLast"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["img-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImgBannerComponent {
    @Input() imgBanner: ImageBanner;

    @Output() handleImgBannerClick: EventEmitter<
        ImageBannerContent
    > = new EventEmitter();
}
