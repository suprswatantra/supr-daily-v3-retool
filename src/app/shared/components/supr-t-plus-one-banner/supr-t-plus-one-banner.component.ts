import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { T_PLUS_ONE } from "@constants";

import { DateService } from "@services/date/date.service";
import { SettingsService } from "@services/shared/settings.service";

import { BannerSetting, CartBannerConfig } from "@types";

@Component({
    selector: "supr-t-plus-one-banner",
    template: `
        <ng-container *ngIf="type === 'CART'; else expandBanner">
            <supr-cart-banner [config]="bannerConfig"></supr-cart-banner>
        </ng-container>

        <ng-template #expandBanner>
            <div class="wrapper">
                <supr-expand-banner
                    [isLeftPaddingZero]="isLeftPaddingZero"
                    [title]="TPOne.TITLE"
                    [subtitle]="TPOne.SUBTITLE"
                    [isExpand]="false"
                ></supr-expand-banner>
            </div>
        </ng-template>
    `,
    styleUrls: ["./supr-t-plus-one-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TPlusOneBannerComponent implements OnInit {
    @Input() type = "CART";
    @Input() isLeftPaddingZero = false;

    TPOne: BannerSetting.TPOne;
    bannerConfig: CartBannerConfig = {};

    constructor(
        private dateService: DateService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.fetchConfig();

        if (this.type === "CART") {
            this.setTitleText();
        }
    }

    private setTitleText() {
        const dayAfterTomorrowDate = this.dateService.addNDaysToToday(2);
        const formatDate = this.dateService.formatDate(
            dayAfterTomorrowDate,
            "EEE, d LLL"
        );

        this.bannerConfig = {
            icon: this.TPOne.ICON,
            subtitle: this.TPOne.SUBTITLE,
            title: `${this.TPOne.TITLE} ${formatDate}.`,
        };
    }

    private fetchConfig() {
        const bannerConfig = this.settingsService.getSettingsValue(
            "banner",
            T_PLUS_ONE
        );

        this.TPOne = { ...bannerConfig[this.type] };
    }
}
