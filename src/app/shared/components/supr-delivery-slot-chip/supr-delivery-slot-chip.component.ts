import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-delivery-slot-chip",
    template: ` <div class="suprRow inline left bottom wrapper">
        <supr-svg [class]="slot"></supr-svg>
        <div class="spacer8"></div>
        <supr-text type="subtext10">
            {{ title | uppercase }}
        </supr-text>
    </div>`,
    styleUrls: ["./supr-delivery-slot-chip.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeliverySlotChipComponent {
    @Input() slot: string;
    @Input() title: string;
}
