import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { AddressAdapter as Adapter } from "@shared/adapters/address.adapter";

@Component({
    selector: "supr-address-retro",
    template: `
        <supr-address-retro-wrapper
            [showModal]="showModal$ | async"
            [isLoggedIn]="isLoggedIn$ | async"
            (handleHideModal)="hideRetroModal()"
        ></supr-address-retro-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddressRetroComponent implements OnInit {
    isLoggedIn$: Observable<boolean>;
    showModal$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.isLoggedIn$ = this.adapter.isLoggedIn$;
        this.showModal$ = this.adapter.showRetroModal$;
    }

    hideRetroModal() {
        this.adapter.hideRetroModal();
    }
}
