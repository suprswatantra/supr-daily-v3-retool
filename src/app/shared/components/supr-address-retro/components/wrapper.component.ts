import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import {
    ADDRESS_FROM_PARAM,
    MODAL_NAMES,
    NUDGE_SETTINGS_KEYS,
    NUDGE_TYPES,
} from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

import { AddressRetroSetting, NudgeMessage } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics";

@Component({
    selector: "supr-address-retro-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="hideModal()"
                modalName="${MODAL_NAMES.ADDRESS_RETRO}"
            >
                <div class="wrapper">
                    <supr-nudge-message
                        [nudgeMessage]="nudgeMessage"
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .UPDATE_ADDRESS_BUTTON}"
                        (handleClick)="primaryBtnClick()"
                    ></supr-nudge-message>

                    <ng-container *ngIf="retroNudge?.showSkip">
                        <div class="divider16"></div>
                        <div
                            class="suprRow center"
                            (click)="skipBtnClick()"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK.SKIP_BUTTON}"
                        >
                            <supr-text type="body" class="skip">SKIP</supr-text>
                            <supr-icon name="chevron_right"></supr-icon>
                        </div>
                    </ng-container>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-address-retro.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() showModal: boolean;
    @Input() isLoggedIn: boolean;
    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    retroNudge: AddressRetroSetting.Nudge;
    nudgeMessage: NudgeMessage;

    constructor(
        private modalService: ModalService,
        private routerService: RouterService,
        private settingsService: SettingsService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleShowModalChange(changes["showModal"]);
    }

    hideModal() {
        this.handleHideModal.emit();
    }

    primaryBtnClick() {
        this.closeModal();
        this.goToAddressMapPage();
    }

    skipBtnClick() {
        this.closeModal();
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        this.getRetroNudgeData();
        this.getNudgeMessageData();
    }

    private getRetroNudgeData() {
        const key = NUDGE_SETTINGS_KEYS[NUDGE_TYPES.ADDRESS];
        this.retroNudge = this.settingsService.getSettingsValue(key, {});
    }

    private getNudgeMessageData() {
        if (!this.retroNudge) {
            return;
        }

        this.nudgeMessage = {
            img: this.retroNudge.img,
            title: this.retroNudge.title,
            subTitle: this.retroNudge.subTitle,
            btnText: this.retroNudge.primaryButton.text,
        };
    }

    private closeModal() {
        this.modalService.closeModal();
    }

    private goToAddressMapPage() {
        this.routerService.goToAddressMapPage({
            queryParams: {
                from: ADDRESS_FROM_PARAM.NUDGE,
            },
        });
    }
}
