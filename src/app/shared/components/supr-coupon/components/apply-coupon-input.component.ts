import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

import { TEXTS } from "../constants";

@Component({
    selector: "supr-apply-coupon-input",
    template: `
        <div class="couponApplyModal">
            <ng-container *ngIf="!hideHeading">
                <supr-text type="subtitle"> ${TEXTS.APPLY_PROMO} </supr-text>
                <div class="divider16"></div>
            </ng-container>
            <div class="couponApplyModalInput">
                <div class="couponApplyModalInputBox">
                    <supr-input
                        saObjectName="${SA_OBJECT_NAMES.CLICK
                            .APPLY_COUPON_FORM}"
                        type="text"
                        textType="body"
                        [value]="_couponCode"
                        [placeholder]="placeholder"
                        (handleSubmit)="applyCoupon()"
                        (handleInputChange)="couponChange($event)"
                        (handleInputClick)="onInputClick()"
                    ></supr-input>
                </div>
            </div>
            <div
                *ngIf="
                    _isInputDirty &&
                    invalidCoupon &&
                    !isApplyingCouponCode &&
                    couponErrorMsg
                "
            >
                <div class="divider24"></div>
                <div
                    class="couponInvalid suprRow"
                    saImpression
                    saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                        .INVALID_COUPON}"
                    [saObjectValue]="_couponCode"
                    [saContext]="couponErrorMsg"
                >
                    <supr-icon name="error"></supr-icon>
                    <div class="spacer4"></div>
                    <supr-text type="paragraph">{{ couponErrorMsg }}</supr-text>
                </div>
                <div class="divider8"></div>
            </div>
            <div class="divider24"></div>
            <supr-button
                (handleClick)="applyCoupon()"
                [disabled]="!_couponCode"
                [loading]="isApplyingCouponCode"
                saObjectName="${SA_OBJECT_NAMES.CLICK.APPLY_COUPON_BTN}"
                [saObjectValue]="_couponCode"
            >
                <supr-text type="body"> ${TEXTS.APPLY} </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["../supr-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplyCouponInputComponent {
    @Input() hideHeading = false;
    @Input() placeholder = "";
    @Input() invalidCoupon: boolean;
    @Input() isApplyingCouponCode: boolean;
    @Input() couponErrorMsg: string;

    @Output() handleApply: EventEmitter<string> = new EventEmitter();
    @Output() handleCouponChange: EventEmitter<void> = new EventEmitter();
    @Output() handleInputClick: EventEmitter<void> = new EventEmitter();

    _couponCode = "";
    _showApplyCoupon = false;
    _isInputDirty = false;

    couponChange(couponCode: string) {
        this._isInputDirty = true;
        this._couponCode = couponCode;
        this.handleCouponChange.emit();
    }

    applyCoupon() {
        this.handleApply.emit(this._couponCode.toUpperCase());
    }

    onInputClick() {
        if (this.handleInputClick) {
            this.handleInputClick.emit();
        }
    }
}
