import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    OnInit,
    EventEmitter,
} from "@angular/core";

import {
    SETTINGS,
    SETTINGS_KEYS_DEFAULT_VALUE,
    TOAST_MESSAGES,
} from "@constants";

import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";
import { ToastService } from "@services/layout/toast.service";
import { CartCurrentState } from "@store/cart/cart.state";

import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";
import { TEXTS } from "@pages/cart/constants/";
@Component({
    selector: "supr-apply-coupon",
    template: `
        <div
            class="suprRow couponApply"
            [class.top]="canUseOffersPage && couponBlockSubTitle"
        >
            <supr-svg class="offer"></supr-svg>
            <div class="suprColumn left">
                <supr-text
                    type="action14"
                    saObjectName="${SA_OBJECT_NAMES.CLICK.APPLY_COUPON_FORM}"
                >
                    {{ couponBlockTitle }}
                </supr-text>
                <ng-container *ngIf="canUseOffersPage && couponBlockSubTitle">
                    <div class="divider4"></div>
                    <div class="suprRow" (click)="showCuponModal()">
                        <supr-text
                            type="paragraph"
                            class="couponBlockSubtitle"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .VIEW_ALL_OFFERS}"
                        >
                            {{ couponBlockSubTitle }}
                        </supr-text>
                        <ng-container *ngIf="showChevIconInCouponBlockSubtitle">
                            <supr-icon
                                name="chevron_right"
                                class="chevRightCouponBlock"
                            ></supr-icon>
                        </ng-container>
                    </div>
                </ng-container>
            </div>
            <ng-container *ngIf="suprCreditsApplicable; else showApplyButton">
                <div
                    class="checkboxCircle"
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.CLICK
                        .APPLY_COUPON_CHECKBOX}"
                    (click)="showCuponModal()"
                ></div>
            </ng-container>

            <ng-template #showApplyButton>
                <supr-text
                    type="caption"
                    class="applyRight"
                    (click)="showCuponModal()"
                    saClick
                    saObjectName="${SA_OBJECT_NAMES.CLICK
                        .APPLY_COUPON_CHECKBOX}"
                >
                    ${TEXTS.APPLY}
                </supr-text>
                <supr-icon
                    name="CHEVRON_RIGHT"
                    class="chevRight"
                    (click)="showCuponModal()"
                ></supr-icon>
            </ng-template>
        </div>
        <supr-apply-coupon-modal
            *ngIf="displayCouponModal"
            [placeholder]="placeholder"
            [invalidCoupon]="invalidCoupon"
            [couponErrorMsg]="couponErrorMsg"
            [isApplyingCouponCode]="isApplyingCouponCode"
            (handleApply)="handleApply.emit($event)"
            (handleCouponChange)="handleCouponChange.emit()"
            (hideCouponModal)="hideCouponModal()"
        ></supr-apply-coupon-modal>
    `,
    styleUrls: ["../supr-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplyCouponComponent implements OnInit {
    @Input() placeholder = "";
    @Input() invalidCoupon: boolean;
    @Input() isApplyingCouponCode: boolean;
    @Input() couponErrorMsg: string;
    @Input() suprCreditsApplicable: boolean;
    @Input() cartCurrentState: CartCurrentState;

    @Output() handleApply: EventEmitter<string> = new EventEmitter();
    @Output() handleCouponChange: EventEmitter<void> = new EventEmitter();

    displayCouponModal = false;

    couponBlockTitle = "";
    couponBlockSubTitle = "";
    showChevIconInCouponBlockSubtitle = false;
    canUseOffersPage = false;

    constructor(
        private routerService: RouterService,
        private settingsService: SettingsService,
        private toastService: ToastService
    ) {}

    ngOnInit() {
        this.setCouponBlockTexts();
        this.setUseOffersPage();
    }

    showCuponModal() {
        if (this.canUseOffersPage) {
            if (this.hasError()) {
                this.showFixErrorsToast();
                return;
            }
            this.routerService.goToCartOffersPage();
            return;
        }
        this.displayCouponModal = true;
    }

    hideCouponModal() {
        this.displayCouponModal = false;
    }

    private setCouponBlockTexts() {
        const couponBlockConfig = this.settingsService.getSettingsValue(
            SETTINGS.CART_COUPON_BLOCK_CONFIG,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.CART_COUPON_BLOCK_CONFIG]
        );
        this.couponBlockTitle = couponBlockConfig.title;
        this.couponBlockSubTitle = couponBlockConfig.subtitle;
        this.showChevIconInCouponBlockSubtitle = couponBlockConfig.showChevIcon;
    }

    private hasError() {
        return (
            this.cartCurrentState === CartCurrentState.VALIDATION_FAILED ||
            this.cartCurrentState === CartCurrentState.CHECKOUT_FAILED
        );
    }

    private showFixErrorsToast() {
        this.toastService.present(TOAST_MESSAGES.CART_ISSUE);
    }

    private setUseOffersPage() {
        this.canUseOffersPage = this.settingsService.getSettingsValue(
            SETTINGS.USE_OFFERS_PAGE_ON_CART,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.USE_OFFERS_PAGE_ON_CART]
        );
    }
}
