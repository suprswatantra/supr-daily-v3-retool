import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { PlatformService } from "@services/util/platform.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-apply-coupon-modal",
    template: `
        <supr-modal
            (handleClose)="closeModal()"
            modalName="${MODAL_NAMES.CART_COUPON_MODAL}"
        >
            <supr-apply-coupon-input
                [placeholder]="placeholder"
                [invalidCoupon]="invalidCoupon"
                [couponErrorMsg]="couponErrorMsg"
                [isApplyingCouponCode]="isApplyingCouponCode"
                (handleApply)="handleApply.emit($event)"
                (handleCouponChange)="handleCouponChange.emit()"
                (handleInputClick)="scrollModalIntoView()"
            ></supr-apply-coupon-input>
        </supr-modal>
    `,
    styleUrls: ["../supr-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplyCouponModalComponent {
    @Input() placeholder = "";
    @Input() invalidCoupon: boolean;
    @Input() isApplyingCouponCode: boolean;
    @Input() couponErrorMsg: string;

    @Output() handleApply: EventEmitter<string> = new EventEmitter();
    @Output() handleCouponChange: EventEmitter<void> = new EventEmitter();
    @Output() hideCouponModal: EventEmitter<void> = new EventEmitter();

    constructor(
        private platformService: PlatformService,
        private utilService: UtilService
    ) {}

    closeModal() {
        this.hideCouponModal.emit();
    }

    scrollModalIntoView() {
        if (this.platformService.isIOS()) {
            const elements = document.getElementsByClassName(
                "couponApplyModal"
            );
            const elem = this.utilService.isLengthyArray(elements)
                ? elements[0]
                : null;
            if (elem) {
                setTimeout(() => {
                    elem.scrollIntoView({
                        behavior: "smooth",
                        block: "center",
                    });
                }, 600);
            }
        }
    }
}
