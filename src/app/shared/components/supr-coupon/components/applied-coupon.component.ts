import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { RouterService } from "@services/util/router.service";

@Component({
    selector: "supr-applied-coupon",
    template: `
        <div class="suprRow couponApplied">
            <div class="suprColumn offerCol left">
                <supr-svg class="offer"></supr-svg>
            </div>
            <div class="suprColumn chipCol left">
                <supr-chip
                    class="code"
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.REMOVE_COUPON}"
                    [saObjectValue]="couponCode"
                    [saContext]="saContext"
                    (handleClick)="handleRemove.emit()"
                >
                    <supr-text type="action14">
                        {{ couponCode }} Applied!
                    </supr-text>
                    <div class="spacer8"></div>
                    <supr-icon class="closeIcon" name="cross"></supr-icon>
                </supr-chip>
                <ng-container *ngIf="couponCodeMessage">
                    <div class="divider4"></div>
                    <supr-text type="caption" class="couponMsg">
                        {{ couponCodeMessage }}
                    </supr-text>
                </ng-container>
                <ng-container *ngIf="showTncButton">
                    <div class="divider4"></div>
                    <div class="suprRow">
                        <supr-text
                            type="paragraph"
                            class="couponTnc"
                            (click)="handleTncClick()"
                        >
                            View TnC
                        </supr-text>
                        <supr-icon
                            name="chevron_right"
                            class="couponTncIcon"
                            (click)="handleTncClick()"
                        ></supr-icon>
                    </div>
                </ng-container>
            </div>
            <div
                class="suprColumn selectionCol right"
                *ngIf="suprCreditsApplicable"
            >
                <ion-checkbox
                    [checked]="couponCode ? true : false"
                    (ionChange)="handleRemove.emit()"
                ></ion-checkbox>
            </div>
        </div>

        <supr-coupon-tnc
            [showModal]="showTncModal"
            [couponCode]="couponCode"
            [couponCodeMessage]="couponCodeMessage"
            [tnc]="tnc"
            (handleClose)="closeTncModal()"
        ></supr-coupon-tnc>
    `,
    styleUrls: ["../supr-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppliedCouponComponent {
    @Input() couponCode: string;
    @Input() couponCodeMessage: string;
    @Input() tnc: string;
    @Output() handleRemove: EventEmitter<void> = new EventEmitter();
    @Input() suprCreditsApplicable: boolean;
    @Input() showTncButton = true;
    @Input() useBottomSheet = true;
    @Input() saContext = "cart";

    showTncModal = false;

    constructor(private routerService: RouterService) {}

    openTncModal() {
        this.showTncModal = true;
    }

    closeTncModal() {
        this.showTncModal = false;
    }

    handleTncClick() {
        if (this.useBottomSheet && this.tnc) {
            this.openTncModal();
            return;
        }
        this.routeToOfferDetails();
    }
    routeToOfferDetails() {
        this.routerService.goToOfferDetailsPage(this.couponCode);
    }
}
