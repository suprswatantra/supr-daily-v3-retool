import { CouponComponent } from "./supr-coupon.component";
import { ApplyCouponComponent } from "./components/apply-coupon.component";
import { AppliedCouponComponent } from "./components/applied-coupon.component";
import { ApplyCouponModalComponent } from "./components/apply-coupon-modal.component";
import { ApplyCouponInputComponent } from "./components/apply-coupon-input.component";

export const couponComponents = [
    CouponComponent,
    ApplyCouponComponent,
    AppliedCouponComponent,
    ApplyCouponModalComponent,
    ApplyCouponInputComponent,
];
