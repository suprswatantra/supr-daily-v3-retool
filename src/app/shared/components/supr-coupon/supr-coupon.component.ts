import { SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";
import { SETTINGS } from "@constants";
import {
    Component,
    Output,
    EventEmitter,
    Input,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    OnInit,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { ToastService } from "@services/layout/toast.service";
import { SettingsService } from "@services/shared/settings.service";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";

@Component({
    selector: "supr-coupon",
    template: `
        <div class="coupon">
            <ng-container
                *ngIf="
                    _couponCode?.length && !_invalidCoupon;
                    else _applyCoupon
                "
            >
                <supr-applied-coupon
                    [couponCode]="_couponCode"
                    [suprCreditsApplicable]="suprCreditsApplicable"
                    [couponCodeMessage]="couponCodeMessage"
                    [tnc]="tnc"
                    [useBottomSheet]="useBottomSheetForTnc"
                    (handleRemove)="removeCoupon.emit()"
                >
                </supr-applied-coupon>
            </ng-container>
            <ng-template #_applyCoupon>
                <supr-apply-coupon
                    [placeholder]="placeholder"
                    [invalidCoupon]="_invalidCoupon"
                    [couponErrorMsg]="_couponMsg"
                    [isApplyingCouponCode]="_applyingCoupon"
                    [suprCreditsApplicable]="suprCreditsApplicable"
                    [cartCurrentState]="cartCurrentState"
                    (handleApply)="handleCouponApply($event)"
                    (handleCouponChange)="couponChange()"
                ></supr-apply-coupon>
            </ng-template>
        </div>
    `,
    styleUrls: ["./supr-coupon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponComponent implements OnInit, OnChanges {
    @Input() couponCode: CouponCode;
    @Input() placeholder = "Apply coupon";
    @Input() cartCurrentState: CartCurrentState;
    @Input() suprCreditsApplicable: boolean;
    @Input() couponCodeMessage: string;
    @Input() tnc: string;

    @Output() applyCoupon: EventEmitter<string> = new EventEmitter();
    @Output() removeCoupon: EventEmitter<string> = new EventEmitter();

    _couponCode = "";
    _invalidCoupon = false;
    _applyingCoupon = false;
    _couponMsg = "";
    useBottomSheetForTnc = true;

    constructor(
        private cdr: ChangeDetectorRef,
        private toastService: ToastService,
        private settingsService: SettingsService
    ) {}

    ngOnInit() {
        this.handleCoupon();
        const couponDisplayConfig = this.settingsService.getSettingsValue(
            SETTINGS.COUPON_TNC_DISPLAY_CONFIG,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.COUPON_TNC_DISPLAY_CONFIG]
        );
        if (couponDisplayConfig && couponDisplayConfig.cart) {
            this.useBottomSheetForTnc = !!couponDisplayConfig.cart
                .useBottomSheet;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleCouponCodeChange(changes["couponCode"]);
        this.handleCartStateChange(changes["cartCurrentState"]);
    }

    couponChange() {
        if (this._invalidCoupon) {
            this.unsetCoupon();
            this.resetApplyingCouponFlag();
            this.cdr.detectChanges();
        }
    }

    handleCouponApply(couponCode: string) {
        this._applyingCoupon = true;
        this.applyCoupon.emit(couponCode);
    }

    private handleCoupon() {
        if (this.couponCode) {
            this.setCoupon();
        } else {
            this.unsetCoupon();
        }
    }

    private setCoupon() {
        const info = this.couponCode;

        this._couponCode = info.couponCode;
        this._invalidCoupon = !info.isApplied;
        this._couponMsg = info.message;

        if (this._couponCode && !this._invalidCoupon && info.couponApplying) {
            this.toastService.present(
                `${this._couponCode} applied successfully`
            );
        }
    }

    private unsetCoupon() {
        this._couponCode = "";
        this._invalidCoupon = false;
        this._couponMsg = "";
    }

    private resetApplyingCouponFlag() {
        this._applyingCoupon = false;
    }

    private setApplyingCouponFlag() {
        this._applyingCoupon = true;
    }

    private handleCouponCodeChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.handleCoupon();
        }
    }

    private handleCartStateChange(change: SimpleChange) {
        if (this.canHandleChange(change) && this.cartCurrentState) {
            if (this.isValidatingCart(change)) {
                this.setApplyingCouponFlag();
            } else {
                this.resetApplyingCouponFlag();
            }
        }
    }

    private isValidatingCart(change: SimpleChange): boolean {
        return change.currentValue === CartCurrentState.UPDATING_CART;
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
