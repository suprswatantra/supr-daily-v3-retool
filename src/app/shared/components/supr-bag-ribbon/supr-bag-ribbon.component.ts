import { Component, ChangeDetectionStrategy } from "@angular/core";
import { TEXTS } from "./constants/supr-bag-ribbon.constants";
@Component({
    selector: "supr-bag-ribbon",
    template: `
        <div class="suprRow left center ribbon">
            <div class="suprColumn center imageWrapper">
                <supr-svg></supr-svg>
            </div>
            <div class="spacer8"></div>
            <supr-text type="paragraph">
                ${TEXTS.PUT_SUPR_BAG}
            </supr-text>
        </div>
    `,
    styleUrls: ["./supr-bag-ribbon.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprBagRibbonComponent {}
