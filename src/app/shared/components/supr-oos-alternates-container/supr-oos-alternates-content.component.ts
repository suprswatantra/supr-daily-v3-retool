import {
    Input,
    Component,
    ChangeDetectionStrategy,
    SimpleChanges,
    OnChanges,
    ChangeDetectorRef,
    SimpleChange,
} from "@angular/core";

import { V3Layout } from "@models";

import { NotifiedDict } from "@types";

@Component({
    selector: "supr-oos-v5-alternates-content",
    template: `
        <supr-v3-widget-container
            [layout]="similarProducts"
            *ngIf="showAlternates"
        ></supr-v3-widget-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOOSV5AlternatesContentComponent implements OnChanges {
    @Input() similarProducts: V3Layout;
    @Input() notifiedSku: NotifiedDict;
    @Input() oosUpfront: boolean;
    @Input() skuId: number;

    skuNotifyState: string;
    showAlternates = false;

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["notifiedSku"];
        this.handleNotifiedSkuChanges(change);
    }

    private handleNotifiedSkuChanges(change: SimpleChange) {
        if (change && change.currentValue && change.previousValue) {
            const current = change.currentValue;
            const previous = change.previousValue;
            if (current[this.skuId] !== previous[this.skuId]) {
                this.showAlternates = true;
            }
        } else {
            this.checkOosUpFront();
        }
    }

    private checkOosUpFront() {
        if (this.oosUpfront) {
            this.showAlternates = true;
        }

        this.cdr.markForCheck();
    }
}
