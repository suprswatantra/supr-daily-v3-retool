import {
    Input,
    OnInit,
    Component,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { SkuAdapter } from "@shared/adapters/sku.adapter";

import { SkuAttributes, V3Layout } from "@models";

import { OOSInfoService } from "@services/shared/oos-info.service";
import { UtilService } from "@services/util/util.service";
import { NotifiedDict } from "@types";

@Component({
    selector: "supr-oos-v5-alternates-container",
    template: `
        <supr-oos-v5-alternates-content
            [similarProducts]="similarProducts$ | async"
            [notifiedSku]="notifiedSku$ | async"
            [oosUpfront]="oosUpfront"
            [skuId]="skuId"
        ></supr-oos-v5-alternates-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOOSV5AlternatesContainer implements OnInit {
    @Input() skuId: number;

    similarProducts$: Observable<V3Layout>;
    skuAlternateList$: Observable<Number[]>;
    notifiedSku$: Observable<NotifiedDict>;

    skuNotifyState: string;
    oosUpfront: boolean;

    constructor(
        private skuAdapter: SkuAdapter,
        private oosInfoService: OOSInfoService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.notifiedSku$ = this.skuAdapter.notifiedSku$;
        this.getSimilarProducts();
    }

    private getSimilarProducts() {
        if (this.skuId) {
            this.similarProducts$ = this.skuAdapter
                .getSkuAttributes(this.skuId)
                .pipe(
                    map((skuAttributes: SkuAttributes) => {
                        if (skuAttributes) {
                            this.oosUpfront = this.utilService.getNestedValue(
                                skuAttributes,
                                "oos_info.oos_upfront",
                                false
                            );
                            return this.oosInfoService.getWidgetDataFromAlternatives(
                                skuAttributes["alternate_sku_ids"],
                                this.skuId
                            );
                        } else {
                            this.getSimilarProductsDep();
                        }
                    })
                );
        }
    }

    private getSimilarProductsDep() {
        this.similarProducts$ = this.skuAdapter
            .getSimilarProductsBySkuId(this.skuId)
            .pipe(
                map((skuIds: number[]) => {
                    this.oosUpfront = false;
                    return this.oosInfoService.getWidgetDataFromAlternatives(
                        skuIds,
                        this.skuId
                    );
                })
            );
    }
}
