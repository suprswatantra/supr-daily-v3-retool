import { SuprOOSV5AlternatesContentComponent } from "./supr-oos-alternates-content.component";
import { SuprOOSV5AlternatesContainer } from "./supr-oos-alternates.container";

export const alternatesV5Components = [
    SuprOOSV5AlternatesContainer,
    SuprOOSV5AlternatesContentComponent,
];
