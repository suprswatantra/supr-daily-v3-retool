import { ReferralBottomsheetComponent } from "./supr-referral-bottomsheet.component";
import { WrapperComponent } from "./components/wrapper.component";

export const referralBottomsheetComponents = [
    ReferralBottomsheetComponent,
    WrapperComponent,
];
