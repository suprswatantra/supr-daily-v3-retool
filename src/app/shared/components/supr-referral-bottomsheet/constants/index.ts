import { Banner } from "@shared/models";

export const TEXTS = {
    INVITE_PHONE_CONTACTS: "Invite phone contacts",
    INVITE_PHONE_CONTACTS_DESCRIPTION:
        "See who’s not on Supr Daily and invite them",
    ALTERNATE: "Invite without syncing contacts",
    WHATSAPP: "WhatsApp",
    SMS: "SMS",
    MORE_OPTIONS: "More Options",
};

export const COPY_BANNER: Banner = {
    bgColor: "#ECFBFA",
    content: [
        {
            text: null,
            fontSize: "12px",
            textColor: "#3C3C35",
            copyBlock: {
                copyText: {
                    text: null,
                },
                iconColor: "#50C1BA",
                hideCopyText: true,
            },
        },
    ],
    border: {
        radius: "4px",
    },
};

export const SHARE_BLOCK_CONTENT = [
    [TEXTS.WHATSAPP, TEXTS.SMS, TEXTS.MORE_OPTIONS],
];
