import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { Observable } from "rxjs";

import { ReferralAdapter as Adapter } from "@shared/adapters/referral.adapter";

import { ReferralInfo } from "@shared/models";

@Component({
    selector: "supr-referral-bottomsheet",
    template: `
        <supr-referral-bottomsheet-wrapper
            [showModal]="showModal"
            [referralInfo]="referralInfo$ | async"
            (handleClose)="handleClose?.emit()"
        ></supr-referral-bottomsheet-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReferralBottomsheetComponent implements OnInit {
    @Input() showModal: boolean;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    referralInfo$: Observable<ReferralInfo>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.referralInfo$ = this.adapter.referralInfo$;
    }
}
