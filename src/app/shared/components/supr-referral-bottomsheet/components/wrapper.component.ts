import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";

import { ReferralInfo, Banner } from "@shared/models";

import { ReferralService } from "@services/shared/referral.service";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { ModalService } from "@services/layout/modal.service";

import { TEXTS, COPY_BANNER, SHARE_BLOCK_CONTENT } from "../constants";
import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-referral-bottomsheet-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleClose?.emit()"
                modalName="${MODAL_NAMES.REFERRAL_BOTTOMSHEET}"
            >
                <div class="wrapper">
                    <!-- header -->
                    <div class="suprRow bottom headerBlock">
                        <div
                            class="referralImage"
                            *ngIf="referralInfo?.header?.logo"
                        >
                            <img [src]="referralInfo?.header?.logo" />
                        </div>
                        <div class="spacer16"></div>
                        <div class="referralText">
                            <div
                                class="suprRow"
                                *ngIf="referralInfo?.header?.title?.text"
                            >
                                <supr-text-fragment
                                    [textFragment]="referralInfo?.header?.title"
                                    class="boldText"
                                ></supr-text-fragment>
                            </div>
                            <div
                                class="suprRow"
                                *ngIf="referralInfo?.header?.subTitle?.text"
                            >
                                <supr-text-fragment
                                    [textFragment]="
                                        referralInfo?.header?.subTitle
                                    "
                                    class="semiBoldText"
                                ></supr-text-fragment>
                            </div>
                        </div>
                    </div>

                    <!-- Banner -->
                    <ng-container *ngIf="referralInfo?.shareBlock?.message">
                        <div class="divider16"></div>
                        <supr-banner [banner]="copyBlockBanner"></supr-banner>
                    </ng-container>

                    <!-- Phone Contacts -->
                    <ng-container *ngIf="referralInfo?.imgBanner">
                        <div class="divider24"></div>
                        <supr-img-banner
                            [imgBanner]="referralInfo?.imgBanner"
                            (handleImgBannerClick)="goToReferralInvitePage()"
                            saClick
                            saObjectName="${SA_OBJECT_NAMES.CLICK
                                .REFERRAL_BOTTOMSHEET_INVITE_PHONE_CONTACTS}"
                        ></supr-img-banner>
                    </ng-container>

                    <div class="divider24"></div>

                    <!-- Third party invite block -->
                    <div class="thirdPartyInviteBlock">
                        <supr-text type="subheading" class="headingText">
                            ${TEXTS.ALTERNATE}
                        </supr-text>
                        <div class="divider20"></div>

                        <ng-container
                            *ngFor="let shareRow of shareblockContent"
                        >
                            <div class="suprRow ">
                                <ng-container
                                    *ngFor="
                                        let shareColumnText of shareRow;
                                        last as isLast
                                    "
                                >
                                    <div
                                        class="suprColumn individualShare center"
                                        [class.isLast]="isLast"
                                        (click)="share(shareColumnText)"
                                        saClick
                                        saObjectName="${SA_OBJECT_NAMES.CLICK
                                            .REFERRAL_BOTTOMSHEET_INVITE_PLATFORM}"
                                        [saObjectValue]="shareColumnText"
                                    >
                                        <div class="individualShareImage">
                                            <supr-svg
                                                [ngClass]="shareColumnText"
                                            ></supr-svg>
                                        </div>
                                        <div class="divider8"></div>
                                        <div class="individualShareText">
                                            <supr-text type="action14">
                                                {{ shareColumnText }}
                                            </supr-text>
                                        </div>
                                    </div>
                                </ng-container>
                            </div>
                            <div class="divider24"></div>
                        </ng-container>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-referral-bottomsheet.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit {
    @Input() showModal: boolean;
    @Input() referralInfo: ReferralInfo;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    copyBlockBanner: Banner;
    shareblockContent = SHARE_BLOCK_CONTENT;

    constructor(
        private referralService: ReferralService,
        private utilService: UtilService,
        private routerService: RouterService,
        private modalService: ModalService
    ) {}

    ngOnInit() {
        this.prepareCopyBlockBanner();
    }

    share(sharePlatform: string) {
        switch (sharePlatform) {
            case TEXTS.WHATSAPP:
                this.referralService.shareViaWhatsApp(this.referralInfo);
                break;

            case TEXTS.SMS:
                this.referralService.shareViaSms(this.referralInfo);
                break;

            default:
                this.referralService.genericShare(this.referralInfo);
                break;
        }
    }

    goToReferralInvitePage() {
        this.closeModal();
        this.routerService.goToReferralInvitePage();
    }

    private closeModal() {
        this.modalService.closeModal();
    }

    /**
     * Add message to copy block banner
     * the structure and styling is already stored in constants.
     *
     * @private
     * @memberof WrapperComponent
     */
    private prepareCopyBlockBanner() {
        this.copyBlockBanner = COPY_BANNER;

        const message = this.utilService.getNestedValue(
            this.referralInfo,
            "shareBlock.message",
            null
        );

        if (!message) {
            return;
        }

        const content = this.copyBlockBanner.content[0];
        content.text = message;
        content.copyBlock.copyText.text = message;
    }
}
