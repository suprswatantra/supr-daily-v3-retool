import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { PNData } from "@store/pn/pn.state";

/*
    <supr-popup
        [showModal]="isModal"
        [data]="data"
        modalName="${MODAL_NAMES.SELF_SERVE_DELIVERY}"
        modalType="${MODAL_TYPES.BOTTOM_SHEET}"
        (closeModal)="redirectSupportPage()"
        (buttonClick)="redirectSupportPage()"
    ></supr-popup>
*/

@Component({
    selector: "supr-popup",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                [modalName]="modalName"
                [modalType]="modalType"
                [saObjectValue]="saObjectValue"
                [saObjectName]="saObjectName"
                [saContext]="saContext"
                (handleClose)="closeModal.emit()"
            >
                <supr-popup-content
                    [data]="data"
                    (handleClick)="buttonClick.emit()"
                ></supr-popup-content>
            </supr-modal>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopupComponent {
    @Input() data: PNData;
    @Input() modalType: string;
    @Input() fullScreen: boolean = false;

    @Input() showModal: boolean = false;

    @Input() modalName: string;
    @Input() saObjectValue: any;
    @Input() saObjectName;
    @Input() saContext: any;

    @Output() closeModal: EventEmitter<void> = new EventEmitter();
    @Output() buttonClick: EventEmitter<void> = new EventEmitter();

    constructor() {}
}
