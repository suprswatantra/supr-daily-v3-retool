import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { OffersList } from "@models";

@Component({
    selector: "supr-offers-list",
    template: `
        <ng-container *ngIf="isFetching; else content">
            <div class="loaderContainer suprColumn center">
                <supr-loader></supr-loader>
            </div>
        </ng-container>
        <ng-template #content>
            <ng-container *ngIf="offersList && offersList.length; else noOffer">
                <ng-container
                    *ngFor="let item of offersList; last as _outerLast"
                >
                    <div>
                        <supr-section-header
                            [icon]="item?.section_info?.icon"
                            [title]="item?.section_info?.title"
                            [subtitle]="item?.section_info?.subtitle"
                        >
                        </supr-section-header>
                        <ng-container
                            *ngFor="
                                let offer of item?.offers;
                                last as _innerLast
                            "
                        >
                            <supr-offer-item
                                [offer]="offer"
                                (handleCouponTapApply)="
                                    handleCouponTapApply.emit($event)
                                "
                            ></supr-offer-item>
                            <div
                                class="divider24"
                                *ngIf="!_outerLast && _innerLast"
                            ></div>
                        </ng-container>
                    </div>
                </ng-container>
            </ng-container>
            <ng-template #noOffer>
                <supr-no-offers-found></supr-no-offers-found>
            </ng-template>
        </ng-template>
    `,
    styleUrls: ["./supr-offers-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOffersListComponent {
    @Input() isFetching: boolean;
    @Input() offersList: OffersList;
    @Output() handleCouponTapApply: EventEmitter<string> = new EventEmitter();
}
