import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { OfferListItem } from "@models";

import { RouterService } from "@services/util/router.service";

import { SA_OBJECT_NAMES, TEXTS } from "../constants";

@Component({
    selector: "supr-offer-item",
    template: `
        <ng-container *ngIf="offer">
            <div class="suprRow itemWrapper top">
                <supr-image [src]="offer?.preview_image_url"></supr-image>
                <div class="spacer12"></div>
                <div class="suprColumn left">
                    <ng-container *ngIf="offer?.title">
                        <supr-text class="title" type="body">{{
                            offer?.title
                        }}</supr-text>
                    </ng-container>
                    <ng-container *ngIf="offer?.description">
                        <supr-text
                            class="description"
                            type="paragraph"
                            [truncate]="true"
                            >{{ offer?.description }}</supr-text
                        >
                    </ng-container>
                    <div class="suprRow" (click)="goToCouponDetail()">
                        <supr-text class="learnMore" type="paragraph"
                            >${TEXTS.LEARN_MORE}</supr-text
                        >
                        <supr-icon
                            name="chevron_right"
                            class="chevRight"
                        ></supr-icon>
                    </div>
                    <div class="divider12"></div>

                    <ng-container *ngIf="!offer?.is_success">
                        <supr-text class="error" type="paragraph">{{
                            offer?.failure_reason
                        }}</supr-text>
                        <div class="divider12"></div>
                    </ng-container>

                    <ng-container
                        *ngIf="offer?.tap_to_apply_flag; else noCouponRequired"
                    >
                        <div class="suprRow">
                            <supr-button
                                class="couponCode"
                                [class.disabled]="!offer?.is_success"
                                [disabled]="!offer?.is_success"
                                saObjectName="${SA_OBJECT_NAMES.TAP_TO_APPLY}"
                                [saContext]="offer?.offer_name"
                                (handleClick)="
                                    handleCouponTapApply.emit(offer?.offer_name)
                                "
                                >{{ offer?.offer_name }}</supr-button
                            >
                            <div class="spacer8"></div>
                            <supr-text
                                class="tapApply"
                                [class.disabled]="!offer?.is_success"
                                type="paragraph"
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.TAP_TO_APPLY}"
                                [saContext]="offer?.offer_name"
                                (click)="onCouponTapApply(offer?.offer_name)"
                                >${TEXTS.TAP_TO_APPLY}</supr-text
                            >
                        </div>
                    </ng-container>
                    <ng-template #noCouponRequired>
                        <supr-text class="noCouponText" type="paragraph"
                            >${TEXTS.NO_COUPON_REQUIRED}</supr-text
                        >
                    </ng-template>
                </div>
            </div>
        </ng-container>
    `,
    styleUrls: ["../supr-offers-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprOfferItemComponent {
    @Input() offer: OfferListItem;
    @Output() handleCouponTapApply: EventEmitter<string> = new EventEmitter();

    constructor(private routerService: RouterService) {}

    goToCouponDetail() {
        if (this.offer && this.offer.offer_name) {
            this.routerService.goToOfferDetailsPage(this.offer.offer_name);
        }
    }

    onCouponTapApply(couponCode: string) {
        if (couponCode && this.offer && this.offer.is_success) {
            this.handleCouponTapApply.emit(couponCode);
        }
    }
}
