import { Component, ChangeDetectionStrategy } from "@angular/core";

import { TEXTS } from "../constants";

@Component({
    selector: "supr-no-offers-found",
    template: `
        <div class="noOffers suprColumn">
            <supr-svg></supr-svg>
            <div class="divider16"></div>
            <supr-text>${TEXTS.NO_OFFER_FOUND}</supr-text>
        </div>
    `,
    styleUrls: ["../supr-offers-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprNoOfferFoundComponent {}
