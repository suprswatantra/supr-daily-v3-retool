import { SuprOfferItemComponent } from "./components/offer-item.component";
import { SuprNoOfferFoundComponent } from "./components/no-offer.component";
import { SuprOffersListComponent } from "./supr-offers-list.component";

export const SuprOfferListComponents = [
    SuprOffersListComponent,
    SuprNoOfferFoundComponent,
    SuprOfferItemComponent,
];
