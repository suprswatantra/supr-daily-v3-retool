export const TEXTS = {
    NO_OFFER_FOUND:
        "Sorry we didn’t find any active offers for you. If you already have a coupon code, please apply above.",
    LEARN_MORE: "Learn more",
    TAP_TO_APPLY: "Tap to apply",
    NO_COUPON_REQUIRED: "No coupon required",
};

export const SA_OBJECT_NAMES = {
    TAP_TO_APPLY: "tap-to-apply",
};

export const DEFAULT_PREVIEW_IMG =
    "https://suprdaily.s3-ap-southeast-1.amazonaws.com/media/ui_images/120583145_348354483033896_681618425083697763_n.png";
