import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { OrderSuccessADModalConfig } from "@types";

import { TEXTS } from "./constants";
import { ANALYTICS } from "./constants/analytics";

@Component({
    selector: "supr-cart-order-success-ad-modal",
    template: `
        <supr-modal
            *ngIf="modalConfig"
            modalName="${ANALYTICS.MODAL_NAME}"
            (handleClose)="handleModalClose.emit()"
        >
            <div class="wrapper">
                <div class="suprRow">
                    <supr-svg></supr-svg>

                    <div class="spacer14"></div>

                    <supr-text class="title" type="subtitle">
                        {{ modalConfig.title }}
                        <supr-text
                            inline="true"
                            type="subtitle"
                            class="titleDate"
                        >
                            {{ date | date: "dd MMM" }}
                        </supr-text>
                    </supr-text>
                </div>

                <div class="divider12"></div>

                <supr-text type="subheading" class="subtitle">
                    {{ modalConfig.subtitle }}
                </supr-text>

                <div class="divider20"></div>

                <ng-container
                    *ngFor="let text of modalConfig.lineItems; index as i"
                >
                    <div class="suprRow top">
                        <div class="indexWrapper suprColumn stretch top">
                            <div class="index">
                                <supr-text type="paragraph">
                                    {{ i + 1 }}
                                </supr-text>
                            </div>
                        </div>

                        <div class="textWrapper suprColumn stretch top left">
                            <supr-text
                                class="info"
                                [type]="body"
                                [inline]="true"
                            >
                                {{ text }}
                            </supr-text>
                        </div>
                    </div>
                    <div class="divider8"></div>
                </ng-container>

                <div class="divider18"></div>

                <supr-button
                    (handleClick)="handleModalClose.emit()"
                    saObjectName="${ANALYTICS.OK_BTN}"
                >
                    <supr-text type="body">
                        ${TEXTS.ORDER_SUCCESS_AD_MODAL_BTN}
                    </supr-text>
                </supr-button>
            </div>
        </supr-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["./supr-order-success-ad-modal.component.scss"],
})
export class OrderSuccessADModal {
    @Input() date: string;
    @Input() modalConfig: OrderSuccessADModalConfig;

    @Output() handleModalClose: EventEmitter<void> = new EventEmitter();
}
