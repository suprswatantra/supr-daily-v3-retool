import { V3CarouselWidgetComponents } from "./supr-v3-carousel-widget";
import { v3GridWidgetComponents } from "./supr-v3-grid-widget";

export const v3Containers = [
    ...V3CarouselWidgetComponents,
    ...v3GridWidgetComponents,
];
