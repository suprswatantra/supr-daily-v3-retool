import { V3GridWidgetComponent } from "./supr-v3-grid-widget.component";

import { HeaderComponent } from "./components/header.components";
import { WrapperComponent } from "./components/wrapper.component";
import { ContentComponent } from "./components/content.component";

export const v3GridWidgetComponents = [
    V3GridWidgetComponent,
    HeaderComponent,
    WrapperComponent,
    ContentComponent,
];
