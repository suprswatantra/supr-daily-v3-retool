import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
} from "@angular/core";

import { V3LayoutItem } from "@models";
import { ComponentService } from "@services/layout/component.service";
import { AddComponentDirective } from "@shared/directives/supr-component-insertion/supr-component-insertion.directive";

@Component({
    selector: "supr-v3-layout-content-type-2",
    template: ` <ng-template saAddComponent></ng-template> `,
    styleUrls: ["../styles/wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentComponent implements OnInit {
    @Input() item: V3LayoutItem;

    @ViewChild(AddComponentDirective, { static: true })
    addComponent: AddComponentDirective;

    constructor(private componentService: ComponentService) {}

    ngOnInit() {
        this.render(this.item);
    }

    render(item: V3LayoutItem) {
        this.componentService.loadComponent(
            item.itemType,
            item.data,
            this.addComponent
        );
    }
}
