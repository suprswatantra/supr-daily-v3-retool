import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { V3LayoutItem } from "@models";

import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-layout-wrapper-type-2",
    template: `
        <div class="container">
            <div
                class="suprRow containerRowData"
                *ngFor="let rowData of groupItems; let row = index"
            >
                <div
                    *ngFor="let item of rowData; let col = index"
                    class="containerRowDataItem"
                    (click)="handleClick(item)"
                    [style.width.%]="rowStyle"
                >
                    <supr-v3-layout-content-type-2
                        [item]="item"
                        saImpression
                        [saImpressionEnabled]="
                            item?.data?.analytics?.saImpression
                        "
                        [saObjectName]="
                            item?.data?.analytics?.saImpression?.objectName
                        "
                        [saObjectValue]="
                            item?.data?.analytics?.saImpression?.objectValue
                        "
                        [saObjectTag]="
                            item?.data?.analytics?.saImpression?.objectTag
                        "
                        [saContext]="
                            item?.data?.analytics?.saImpression?.context
                        "
                        [saContextList]="
                            item?.data?.analytics?.saImpression?.contextList
                        "
                        [saPosition]="getPosition(row, col)"
                    ></supr-v3-layout-content-type-2>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["../styles/wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit {
    @Input() items: V3LayoutItem[];
    @Input() rowCount: number;

    groupItems: V3LayoutItem[][];

    rowStyle = "";

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.groupItems = this.groupArray(this.items, this.rowCount);
        this.rowStyle = `${100 / this.rowCount}`;
    }

    private groupArray<T>(data: Array<T>, n: number): Array<T[]> {
        return this.homeLayoutService.groupArray(data, n);
    }

    handleClick(item: V3LayoutItem) {
        this.homeLayoutService.handleEvents(item.data);
    }

    getPosition(row: number, col: number): number {
        return this.homeLayoutService.getPosition(row, col);
    }
}
