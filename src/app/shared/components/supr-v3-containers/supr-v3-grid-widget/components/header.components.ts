import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { V3LayoutText, V3LayoutCta, V3LayoutImage } from "@models";
import { Segment } from "@types";

@Component({
    selector: "supr-v3-container-type-2-header",
    template: `
        <div class="header" #header>
            <ng-container *ngIf="title || subtitle || cta">
                <div class="suprRow spaceBetween" (click)="goToPage()">
                    <div class="headerText" [class.wrap]="showCta">
                        <ng-container *ngIf="title">
                            <supr-text [type]="title?.type || 'subtitle'">
                                {{ title?.text }}
                            </supr-text>
                        </ng-container>

                        <ng-container *ngIf="subtitle">
                            <supr-text
                                [type]="title?.type || 'caption'"
                                class="subtitle"
                            >
                                {{ subtitle?.text }}
                            </supr-text>
                        </ng-container>
                    </div>
                    <ng-container *ngIf="showCta">
                        <ng-container [ngSwitch]="">
                            <!-- See All Card -->
                        </ng-container>
                    </ng-container>

                    <ng-container *ngIf="false">
                        <div class="headerImage">
                            <supr-image
                                [src]="image?.data?.fullUrl"
                                [image]="image?.data"
                            ></supr-image>
                        </div>
                    </ng-container>
                </div>
                <div class="divider16"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    @Input() title: V3LayoutText;
    @Input() subtitle: V3LayoutText;
    @Input() cta: V3LayoutCta;
    @Input() image: V3LayoutImage;

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    saContextList: Segment.ContextListItem[] = [];
    showCta = false;

    ngOnInit() {
        this.setStyles();
        this.setCtaFlag();
    }

    goToPage() {
        if (!this.cta) {
            return;
        }
    }

    private setStyles() {
        this.setTitleStyle();
        this.setSubtitleStyle();
    }

    private setTitleStyle() {
        if (!this.styleExists(this.title)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-v3-container-header-type-1-title-color",
            this.title.style.color
        );
    }

    private setSubtitleStyle() {
        if (!this.styleExists(this.subtitle)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-v3-container-header-type-1-subtitle-color",
            this.subtitle.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }

    private setCtaFlag() {
        this.showCta = !!this.cta;
    }
}
