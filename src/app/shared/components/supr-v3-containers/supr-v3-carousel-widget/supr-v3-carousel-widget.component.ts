import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

import { AUTO_SCROLL_INFO } from "@constants";
import { V3LayoutData } from "@shared/models";
import { UtilService } from "@services/util/util.service";
import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-container-carousel-widget",
    template: `
        <div
            class="container"
            [ngStyle]="data.style"
            [class.addPadding]="_addPadding"
            [class.suprDay]="checkToAddClass()"
            [id]="wrapperId"
        >
            <supr-v3-container-type-1-header
                [title]="data?.title"
                [subtitle]="data?.subtitle"
                [cta]="data?.cta"
                [image]="data?.image"
                [data]="data"
            ></supr-v3-container-type-1-header>

            <supr-v3-layout-wrapper
                [items]="data?.items"
                [rowCount]="data?.rowCount"
            ></supr-v3-layout-wrapper>

            <ng-container *ngIf="data?.tnc">
                <div class="divider16"></div>
                <div
                    class="suprRow containerTnc top"
                    [ngStyle]="data?.tnc?.style"
                    (click)="handleEvents()"
                    saClick
                    [saClickedEnabled]="data?.tnc?.cta?.analytics?.saClick"
                    [saObjectName]="
                        data?.tnc?.cta?.analytics?.saClick?.objectName
                    "
                    [saObjectValue]="
                        data?.tnc?.cta?.analytics?.saClick?.objectValue
                    "
                    [saObjectTag]="
                        data?.tnc?.cta?.analytics?.saClick?.objectTag
                    "
                    [saContext]="data?.tnc?.cta?.analytics?.saClick?.context"
                    [saContextList]="
                        data?.tnc?.cta?.analytics?.saClick?.contextList
                    "
                >
                    <supr-image
                        [src]="data?.tnc?.image?.data?.fullUrl"
                        [image]="data?.tnc?.image?.data"
                    ></supr-image>
                    <div class="spacer8"></div>
                    <div class="suprRow containerTncInfo top spaceBetween">
                        <supr-text
                            type="subtext10"
                            class="title"
                            [truncate]="true"
                        >
                            {{ data?.tnc?.title?.text }}
                        </supr-text>

                        <supr-text type="subtext8" class="subtitle">
                            {{ data?.tnc?.subtitle?.text }}
                        </supr-text>
                    </div>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ["./styles/supr-v3-carousel-widget.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class V3CarouselWidgetComponent implements OnInit {
    @Input() data: V3LayoutData;

    constructor(
        private utilService: UtilService,
        private homeLayoutService: HomeLayoutService
    ) {}

    _addPadding = false;
    wrapperId = "";

    ngOnInit() {
        this.checkToAddPadding();
        this.setWrapperID();
    }

    checkToAddClass() {
        if (this.data.items.length >= 0) {
            const item = this.data.items[0];
            if (!this.utilService.isEmpty(item)) {
                return (
                    item.itemType === "collection_square_with_supr_day_timer"
                );
            }

            return false;
        }
    }

    handleEvents() {
        if (this.data && this.data.tnc) {
            this.homeLayoutService.handleEvents(this.data.tnc);
        }
    }

    checkToAddPadding() {
        const style = this.utilService.getNestedValue(this.data, "style", null);

        if (style) {
            const bgColor = this.utilService.getNestedValue(
                style,
                "background-color",
                null
            );

            if (
                bgColor ||
                bgColor !== "var(--white-100)" ||
                bgColor !== "#fff"
            ) {
                this._addPadding = true;
            }
        }
    }

    private setWrapperID() {
        this.wrapperId = AUTO_SCROLL_INFO.prefix_collection + this.data.viewId;
    }
}
