import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
} from "@angular/core";

import { V3LayoutItem } from "@models";

import { HomeLayoutService } from "@services/layout/home-layout.service";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-v3-layout-wrapper",
    template: `
        <supr-horizontal>
            <div
                class="suprRow container top"
                [class.suprDayStyle]="checkToAddClass()"
            >
                <div
                    class="containerRowData"
                    *ngFor="let rowData of groupItems; let row = index"
                >
                    <div
                        *ngFor="let item of rowData; let col = index"
                        class="containerRowDataItem"
                        (click)="handleClick(item)"
                        [ngStyle]="item?.data?.style"
                    >
                        <supr-v3-layout-content
                            [item]="item"
                            saImpression
                            [saImpressionEnabled]="
                                item?.data?.analytics?.saImpression
                            "
                            [saObjectName]="
                                item?.data?.analytics?.saImpression?.objectName
                            "
                            [saObjectTag]="
                                item?.data?.analytics?.saImpression?.objectTag
                            "
                            [saObjectValue]="
                                item?.data?.analytics?.saImpression?.objectValue
                            "
                            [saContext]="
                                item?.data?.analytics?.saImpression?.context
                            "
                            [saContextList]="
                                item?.data?.analytics?.saImpression?.contextList
                            "
                            [saPosition]="getPosition(row, col)"
                        ></supr-v3-layout-content>
                    </div>
                </div>
            </div>
        </supr-horizontal>
    `,
    styleUrls: ["../styles/wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit {
    @Input() items: V3LayoutItem[];
    @Input() rowCount: number;

    groupItems: V3LayoutItem[][];

    constructor(
        private homeLayoutService: HomeLayoutService,
        private utilService: UtilService
    ) {}

    ngOnInit() {
        this.groupItems = this.groupArray(this.items, this.rowCount);
    }

    private groupArray<T>(data: Array<T>, n: number): Array<T[]> {
        return this.homeLayoutService.groupArray(data, n);
    }

    checkToAddClass() {
        if (this.items.length >= 0) {
            const item = this.items[0];
            if (!this.utilService.isEmpty(item)) {
                return (
                    item.itemType === "collection_square_with_supr_day_timer"
                );
            }

            return false;
        }
    }

    getPosition(row: number, col: number): number {
        return this.homeLayoutService.getPosition(row, col);
    }

    handleClick(item: V3LayoutItem) {
        this.homeLayoutService.handleEvents(item.data);
    }
}
