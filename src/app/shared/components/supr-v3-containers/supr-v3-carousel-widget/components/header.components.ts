import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    V3LayoutText,
    V3LayoutCta,
    V3LayoutImage,
    V3LayoutData,
} from "@models";
import { Segment } from "@types";
import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-v3-container-type-1-header",
    template: `
        <div class="header" #header>
            <ng-container *ngIf="title || subtitle || cta">
                <div class="suprRow spaceBetween top">
                    <div class="headerText" [class.wrap]="showCta">
                        <ng-container *ngIf="title">
                            <supr-text [type]="title?.textType || 'subtitle'">
                                {{ title?.text }}
                            </supr-text>
                        </ng-container>

                        <ng-container *ngIf="subtitle">
                            <div class="divider4"></div>
                            <supr-text
                                [type]="subtitle?.textType || 'caption'"
                                class="subtitle"
                            >
                                {{ subtitle?.text }}
                            </supr-text>
                        </ng-container>
                    </div>
                    <ng-container *ngIf="showCta">
                        <ng-container>
                            <!-- See All Card -->
                            <div
                                class="headerShowAll suprRow center"
                                [ngStyle]="cta?.style"
                                (click)="handleEvents()"
                                saClick
                                [saClickedEnabled]="cta?.analytics?.saClick"
                                [saObjectName]="
                                    cta?.analytics?.saClick?.objectName
                                "
                                [saObjectValue]="
                                    cta?.analytics?.saClick?.objectValue
                                "
                            >
                                <supr-text
                                    [type]="cta?.textType || 'subtext10'"
                                >
                                    {{ cta.text }}
                                </supr-text>

                                <supr-icon
                                    *ngIf="checkIsSeeAll()"
                                    name="chevron_right"
                                ></supr-icon>
                            </div>
                        </ng-container>
                    </ng-container>

                    <ng-container *ngIf="false">
                        <div class="headerImage">
                            <supr-image
                                [src]="image?.data?.fullUrl"
                                [image]="image?.data"
                            ></supr-image>
                        </div>
                    </ng-container>
                </div>
                <div class="divider16"></div>
            </ng-container>
        </div>
    `,
    styleUrls: ["../styles/header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    @Input() title: V3LayoutText;
    @Input() subtitle: V3LayoutText;
    @Input() cta: V3LayoutCta;
    @Input() image: V3LayoutImage;
    @Input() data: V3LayoutData;

    @ViewChild("header", { static: true }) headerEl: ElementRef;

    saContextList: Segment.ContextListItem[] = [];
    showCta = false;
    isSeeAllText = false;

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.setStyles();
        this.setCtaFlag();
    }

    handleEvents() {
        this.homeLayoutService.handleEvents(this.cta);
    }

    checkIsSeeAll() {
        if (!this.cta) {
            return;
        }

        return this.cta.text === "See All";
    }

    private setStyles() {
        this.setTitleStyle();
        this.setSubtitleStyle();
        this.setShowAllStyle();
    }

    private setTitleStyle() {
        if (!this.styleExists(this.title)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-v3-container-header-type-1-title-color",
            this.title.style.color
        );
    }

    private setSubtitleStyle() {
        if (!this.styleExists(this.subtitle)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-v3-container-header-type-1-subtitle-color",
            this.subtitle.style.color
        );
    }

    private setShowAllStyle() {
        if (!this.styleShowAllExists(this.cta)) {
            return;
        }

        this.headerEl.nativeElement.style.setProperty(
            "--supr-v3-container-header-show-all-color",
            this.cta.style.color
        );
    }

    private styleExists(text: V3LayoutText): boolean {
        return !!(text && text.style);
    }

    private styleShowAllExists(cta: V3LayoutCta): boolean {
        return !!(cta && cta.style);
    }

    private setCtaFlag() {
        this.showCta = !!this.cta;
    }
}
