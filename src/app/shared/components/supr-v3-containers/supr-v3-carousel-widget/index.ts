import { V3CarouselWidgetComponent } from "./supr-v3-carousel-widget.component";

import { HeaderComponent } from "./components/header.components";
import { WrapperComponent } from "./components/wrapper.component";
import { ContentComponent } from "./components/content.component";

export const V3CarouselWidgetComponents = [
    V3CarouselWidgetComponent,
    HeaderComponent,
    WrapperComponent,
    ContentComponent,
];
