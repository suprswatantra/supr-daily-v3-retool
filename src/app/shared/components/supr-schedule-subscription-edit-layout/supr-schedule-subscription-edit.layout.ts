import {
    Input,
    OnInit,
    Output,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    TOAST_MESSAGES,
    RATING_NUDGE_TRIGGERS,
    EDIT_DELIVERY_ACTIONS,
    DELIVERY_EDIT_MODAL_TEXTS,
} from "@constants";

import { Subscription, ScheduleItem, Sku, CartItem, Vacation } from "@models";

import { DateService } from "@services/date/date.service";
import { SkuService } from "@services/shared/sku.service";
import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { ToastService } from "@services/layout/toast.service";
import { NudgeService } from "@services/layout/nudge.service";
import { MoengageService } from "@services/integration/moengage.service";

import { SubscriptionCurrentState } from "@store/subscription/subscription.state";

import { UpdateSubInstructionActionPayload } from "@types";
import { CartService } from "@services/shared/cart.service";

@Component({
    selector: "supr-schedule-subscription-edit-layout",
    template: `
        <supr-schedule-edit-modal-card
            [sku]="sku"
            [date]="date"
            [schedule]="schedule"
            [plusDisabled]="plusDisabled"
            [subscription]="subscription"
            [adjustmentAction]="adjustmentAction"
            [amountChange]="amountChange"
            [counterQuantity]="counterQuantity"
            [showCancelConfirmation]="showCancelConfirmation"
            [showCancelAction]="showCancelAction"
            [showChangeAction]="showChangeAction"
            [confirmBtnText]="confirmBtnText"
            [showAdjustmentText]="showAdjustmentText"
            [headerText]="headerText"
            [secondaryActionText]="secondaryActionText"
            (handleButtonClick)="onButtonClick($event)"
            (handleQuantityUpdate)="onQuantityUpdate($event)"
            (handleCancelTextClick)="onCancelTextClick()"
            (handleManageSubscription)="onManageSubscriptionClick()"
        ></supr-schedule-edit-modal-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleSubscriptionEditLayoutComponent
    implements OnInit, OnChanges {
    @Input() sku: Sku;
    @Input() date: string;
    @Input() vacation: Vacation;
    @Input() amountChange?: number;
    @Input() schedule: ScheduleItem;
    @Input() adjustmentAction: string;
    @Input() subscription?: Subscription;
    @Input() showCancelConfirmation: boolean;
    @Input() subscriptionState: SubscriptionCurrentState;
    @Input() subscriptionError: any;
    @Input() showCancelAction?: boolean;
    @Input() showChangeAction?: boolean;
    @Input() showOrderUpdatedToast?: boolean;
    @Input() confirmBtnText?: string;
    @Input() secondaryActionText?: string;
    @Input() showAdjustmentText?: boolean;
    @Input() headerText = DELIVERY_EDIT_MODAL_TEXTS.EDIT;

    @Output() handleAddToCart: EventEmitter<CartItem> = new EventEmitter();
    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();
    @Output() updateSubscriptionInstruction: EventEmitter<
        UpdateSubInstructionActionPayload
    > = new EventEmitter();
    @Output() handleChangeDateClick: EventEmitter<void> = new EventEmitter();

    plusDisabled = false;
    counterQuantity: number;

    constructor(
        private skuService: SkuService,
        private cartService: CartService,
        private dateService: DateService,
        private modalService: ModalService,
        private toastService: ToastService,
        private nudgeService: NudgeService,
        private routerService: RouterService,
        private moengageService: MoengageService
    ) {}

    ngOnInit() {
        this.setCounterQty();
        this.setCounterState();
        this.checkForAdjustmentState();
    }

    ngOnChanges(changes: SimpleChanges) {
        const skuChange = changes["sku"];

        if (skuChange && !skuChange.firstChange) {
            this.setCounterQty();
        }

        this.handleSubscriptionStateChange(changes["subscriptionState"]);
    }

    private handleSubscriptionStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (
            change.previousValue ===
                SubscriptionCurrentState.UPDATING_SUBSCRIPTION_INSTRUCTION &&
            change.currentValue !== change.previousValue
        ) {
            if (!this.subscriptionError) {
                this.showSuccessToast();
                this.sendAnalyticsEvents();
                this.modalService.closeModal();
                this.sendRatingNudge();
            }
        }
    }

    onQuantityUpdate(direction: number) {
        if (direction > 0) {
            if (
                this.counterQuantity <=
                this.schedule.remaining_quantity + this.schedule.quantity
            ) {
                this.counterQuantity++;
            }
        } else if (direction < 0 && this.counterQuantity > 0) {
            this.counterQuantity--;
        }

        this.setCounterState();
        this.checkForCancelState();
        this.checkForAdjustmentState();
    }

    onCancelTextClick() {
        this.showCancelConfirmation = true;
        this.counterQuantity = 0;
        this.adjustmentAction = EDIT_DELIVERY_ACTIONS.ADJUSTMENT;
    }

    onButtonClick(action: string) {
        switch (action) {
            case EDIT_DELIVERY_ACTIONS.RECHARGE_SUBSCRIPTION:
                this.modalService.closeModal();
                this.routerService.goToSubscriptionRechargePage(
                    this.subscription.id
                );
                break;
            case EDIT_DELIVERY_ACTIONS.CANCEL:
            case EDIT_DELIVERY_ACTIONS.UPDATE:
                this.updateActionListener.emit(this.date);
                this.updateSubscriptionInstruction.emit({
                    subscriptionId: this.schedule.id,
                    quantity: this.counterQuantity,
                    date: this.date,
                });
                break;
            case EDIT_DELIVERY_ACTIONS.CHANGE_DATE:
                this.adjustmentAction = EDIT_DELIVERY_ACTIONS.CHANGE_DATE;
                this.handleChangeDateClick.emit();
                break;
            case EDIT_DELIVERY_ACTIONS.DELIVER_ONCE:
                const cartItem = this.cartService.getCartItemFromSku(
                    this.sku,
                    this.counterQuantity,
                    this.vacation
                );

                this.modalService.closeModal();
                this.handleAddToCart.emit(cartItem);
                break;
            default:
                this.modalService.closeModal();
        }
    }

    private showSuccessToast() {
        if (!this.showOrderUpdatedToast) {
            this.toastService.present(TOAST_MESSAGES.SUBSCRIPTIONS.UPDATED);
        } else {
            const { ORDER_UPDATED } = TOAST_MESSAGES.SUBSCRIPTIONS;

            const date = this.dateService.formatDate(this.date, "EEEE, dd MMM");
            const toastMessage = ORDER_UPDATED.replace("[DATE_STRING]", date);
            this.toastService.present(toastMessage, {
                actionText: "VIEW",
                actionHandler: () => {
                    this.routerService.goToSchedulePage({
                        replaceUrl: true,
                    });
                },
            });
        }
    }

    onManageSubscriptionClick() {
        this.modalService.closeModal();
        this.routerService.goToSubscriptionDetailsPage(this.subscription.id);
    }

    private setCounterQty() {
        if (this.schedule) {
            if (this.schedule.quantity > 0) {
                this.counterQuantity = this.schedule.quantity;
            } else {
                this.counterQuantity = this.subscription.quantity;
            }
        }
    }

    private checkForCancelState() {
        if (this.counterQuantity === 0) {
            this.showCancelConfirmation = true;
        } else {
            this.showCancelConfirmation = false;
        }
    }

    /* [TODO] Move this logic to adapter and adapter calls service */
    private checkForAdjustmentState() {
        if (this.counterQuantity < this.schedule.quantity) {
            this.adjustmentAction = EDIT_DELIVERY_ACTIONS.ADJUSTMENT;
        } else if (this.counterQuantity > this.schedule.quantity) {
            this.adjustmentAction =
                this.counterQuantity >
                this.schedule.remaining_quantity + this.schedule.quantity
                    ? EDIT_DELIVERY_ACTIONS.RECHARGE_SUBSCRIPTION
                    : EDIT_DELIVERY_ACTIONS.ADJUSTMENT;
        } else {
            this.adjustmentAction = null;
        }
    }

    private setCounterState() {
        if (this.isSkuOutOfStock()) {
            this.plusDisabled = true;
            return;
        }
        /* If sku is not available for given date then don't allow user to increase quantity */
        if (!this.skuService.isSkuAvailableOnGivenDate(this.sku, this.date)) {
            this.plusDisabled = true;
            return;
        }
        this.plusDisabled =
            this.counterQuantity >
            this.schedule.remaining_quantity + this.schedule.quantity;
    }

    private isSkuOutOfStock() {
        return (
            this.sku && this.sku.out_of_stock && !this.sku.next_available_date
        );
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackDeliveryCancellation({
            type: "subscription",
            sku_id: this.sku.id,
            delivery_date: this.date,
        });
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.UPDATE_SUBSCRIPTION_INSTRUCTION
        );
    }
}
