import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { ComplaintViewV2 } from "@models";
import { UtilService } from "@services/util/util.service";

@Component({
    selector: "supr-complaints-list",
    template: `
        <div>
            <div *ngFor="let complaint of ComplaintsListV2">
                <supr-complaint-view
                    *ngIf="isComplaint(complaint)"
                    [complaintV2]="complaint"
                    (handleClick)="handleClick.emit($event)"
                    (complaintClick)="complaintClick.emit($event)"
                    [saObjectName]="saObjectName"
                ></supr-complaint-view>
            </div>
        </div>
    `,
    styleUrls: ["./supr-complaints-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprComplaintsList {
    @Input() ComplaintsListV2: ComplaintViewV2[];
    @Input() saObjectName: string;

    @Output() handleClick: EventEmitter<ComplaintViewV2> = new EventEmitter();
    @Output() complaintClick: EventEmitter<
        ComplaintViewV2
    > = new EventEmitter();

    constructor(private utilService: UtilService) {}

    isComplaint(complaint: ComplaintViewV2) {
        return (
            !this.utilService.isEmpty(complaint) &&
            !this.utilService.isEmpty(complaint.complaint_status_timeline)
        );
    }
}
