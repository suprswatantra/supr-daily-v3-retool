import {
    Component,
    ChangeDetectionStrategy,
    Input,
    ViewChild,
    ElementRef,
    OnInit,
} from "@angular/core";

import { ActivityBlock } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { CurrencyService } from "@services/util/currency.service";

import { ACTIVITY_CONTENT_STYLE_LIST, TEXTS, BENEFIT_TYPES } from "./constants";
import { SA_OBJECT_NAMES } from "./constants/analytics-constants";

@Component({
    selector: "supr-activity-block",
    template: `
        <div class="activityItem" #activityItemWrapper>
            <div class="suprRow top">
                <ng-container *ngIf="activityItem?.tag">
                    <div class="suprColumn left leftColumn">
                        <div class="tag">
                            <supr-text-fragment
                                [textFragment]="activityItem?.tag"
                            ></supr-text-fragment>
                        </div>
                    </div>
                </ng-container>
                <ng-container *ngIf="benefitString">
                    <div class="suprColumn right rightColumn">
                        <div class="suprRow">
                            <supr-text type="action14" class="daysText">
                                {{ benefitString }}
                            </supr-text>
                            <ng-container *ngIf="activityItem?.benefitIcon">
                                <div class="spacer4"></div>
                                <supr-configurable-icon
                                    [config]="activityItem?.benefitIcon"
                                ></supr-configurable-icon>
                            </ng-container>
                        </div>
                    </div>
                </ng-container>
            </div>
            <div class="divider12"></div>
            <div class="suprRow top">
                <ng-container *ngIf="activityItem?.detailsText">
                    <div class="suprColumn left leftColumn">
                        <supr-text-fragment
                            [textFragment]="activityItem?.detailsText"
                        ></supr-text-fragment>
                    </div>
                </ng-container>
                <ng-container
                    *ngIf="
                        activityItem?.date && !activityItem?.footerText?.text
                    "
                >
                    <ng-template
                        [ngTemplateOutlet]="dateTemplate"
                    ></ng-template>
                </ng-container>
            </div>
            <ng-container *ngIf="activityItem?.footerText?.text">
                <div class="divider12"></div>
                <div class="suprRow bottom">
                    <div class="suprColumn left leftColumn">
                        <supr-text-fragment
                            [textFragment]="activityItem?.footerText"
                        ></supr-text-fragment>
                    </div>
                    <ng-container *ngIf="activityItem?.date">
                        <ng-template
                            [ngTemplateOutlet]="dateTemplate"
                        ></ng-template>
                    </ng-container>
                </div>
            </ng-container>
            <ng-container *ngIf="showAllActivityCta">
                <div class="divider12"></div>
                <div
                    class="suprRow viewAllBlock"
                    (click)="goToActivityPage()"
                    saClick
                    [saObjectName]="saClickViewAllObjectName"
                >
                    <supr-text type="paragraph">
                        {{ viewAllText }}
                    </supr-text>
                    <supr-icon name="chevron_right"></supr-icon>
                </div>
            </ng-container>
            <ng-template #dateTemplate>
                <div class="suprColumn right rightColumn">
                    <div class="suprRow">
                        <supr-text type="caption" class="dateText">
                            {{ activityItem?.date | date: "EEEE" }}
                        </supr-text>
                    </div>
                    <div class="divider4"></div>
                    <div class="suprRow">
                        <supr-text type="caption" class="dateText">
                            {{ activityItem?.date | date: "d LLL, yyy" }}
                        </supr-text>
                    </div>
                </div>
            </ng-template>
        </div>
    `,
    styleUrls: ["activity.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityBlockComponent implements OnInit {
    @Input() showAllActivityCta: boolean;
    @Input() viewAllText: string = TEXTS.VIEW_ALL_ACTIVITY;
    @Input() saClickViewAllObjectName: string =
        SA_OBJECT_NAMES.CLICK.VIEW_ALL_ACTIVITY;
    @Input() activityFullPageUrl: string;

    @Input() set activityItem(item: ActivityBlock) {
        this._activityItem = item;
        this.setDaysString();
    }
    get activityItem(): ActivityBlock {
        return this._activityItem;
    }

    @ViewChild("activityItemWrapper", { static: true }) wrapperEl: ElementRef;

    benefitString: string;

    private _activityItem: ActivityBlock;

    constructor(
        private utilService: UtilService,
        private routerService: RouterService,
        private currencyService: CurrencyService
    ) {}

    ngOnInit() {
        this.setStyles();
    }

    goToActivityPage() {
        if (!this.activityFullPageUrl) {
            return;
        }
        this.routerService.goToUrl(this.activityFullPageUrl);
    }

    private setStyles() {
        if (!this._activityItem) {
            return;
        }

        this.utilService.setStyles(
            this._activityItem,
            ACTIVITY_CONTENT_STYLE_LIST,
            this.wrapperEl
        );
    }

    private setDaysString() {
        const days = this.utilService.getNestedValue(
            this._activityItem,
            "totalDays",
            0
        );

        const totalBenefit = this.utilService.getNestedValue(
            this._activityItem,
            "totalBenefit",
            0
        );

        const benefitType = this.utilService.getNestedValue(
            this._activityItem,
            "benefitType",
            null
        );

        if (!(days || totalBenefit)) {
            this.benefitString = "";
            return;
        }

        if (days) {
            const daysStr = days > 0 ? "+" + days : days;

            this.benefitString =
                days === 1
                    ? daysStr + " " + TEXTS.DAY
                    : daysStr + " " + TEXTS.DAY + "s";
            return;
        }

        if (benefitType === BENEFIT_TYPES.RUPEE) {
            this.benefitString = this.currencyService.toText(totalBenefit);
            return;
        }

        this.benefitString =
            totalBenefit > 0 ? "+" + totalBenefit : "" + totalBenefit;
    }
}
