export const ACTIVITY_CONTENT_STYLE_LIST = [
    {
        attributeName: "tag.bgColor",
        attributeStyleVariableName: "--supr-activity-days-color",
    },
];

export const TEXTS = {
    VIEW_ALL_ACTIVITY: "VIEW ALL",
    DAY: "Day",
};

export enum BENEFIT_TYPES {
    SUPR_CREDITS = "suprCredits",
    RUPEE = "rupee",
}
