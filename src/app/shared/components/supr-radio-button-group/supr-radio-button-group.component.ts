import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
    ChangeDetectionStrategy,
    ElementRef,
    ViewChild,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import { UtilService } from "@services/util/util.service";
import { RadioButton } from "@types";

const WRAPPER_COL_SIZE_CSS_VARIABLE = "--supr-radio-button-group-col-size";

@Component({
    selector: "supr-radio-button-group",
    template: `
        <div class="wrapper" #wrapper>
            <div
                class="suprRow row"
                *ngFor="let rowData of gridArr; trackBy: trackByFn"
            >
                <ng-container *ngFor="let data of rowData; trackBy: trackByFn">
                    <supr-radio-button
                        [data]="data"
                        [selected]="selectedIndex === data.index"
                        (handleClick)="selectOption(data.index)"
                        [saObjectName]="saObjectName"
                        [saContext]="saContext"
                    ></supr-radio-button>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["./supr-radio-button-group.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RadioButtonGroupComponent implements OnInit, OnChanges {
    @Input() options: RadioButton[];
    @Input() colSize = 1;
    @Input() selectedIndex = -1;
    @Input() saObjectName: string;
    @Input() saContext: string;

    @Output() handleSelect: EventEmitter<number> = new EventEmitter();

    @ViewChild("wrapper", { static: true }) wrapperEl: ElementRef;

    gridArr: any = [];

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.setColSizeCssValue();
        this.splitOptionsIntoRows();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes["options"] || changes["selectedIndex"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.splitOptionsIntoRows();
        }
    }

    trackByFn(index: number): number {
        return index;
    }

    selectOption(optionId: number) {
        if (this.handleSelect) {
            this.handleSelect.emit(optionId);
        }
    }

    splitOptionsIntoRows() {
        this.gridArr = this.utilService.chunkArray(this.options, this.colSize);
    }

    private setColSizeCssValue() {
        this.wrapperEl.nativeElement.style.setProperty(
            WRAPPER_COL_SIZE_CSS_VARIABLE,
            this.colSize
        );
    }
}
