export const SUPR_CREDITS_DEFAULT_TEXTS = {
    HEADING: {
        SUB_TEXT_USE: "Use",
        SUB_TEXT_CREDITS: "Supr Credits!",
    },
    INFO: {
        SUB_TEXT_INFO:
            "Supr Credits are applicable on all products except milk.",
        SUB_TEXT_INFO_PUNE:
            "Supr Credits are only applicable on fruits and vegetables.",
    },
    LEARN: "Learn more",
    NOT_APPLICABLE_DISCLAIMER: "Add non-milk products to use",
    NOT_APPLICABLE_DISCLAIMER_PUNE: "Add fruits or vegetables products to use",
    NOT_APPLICABLE_HEADING: "Add items to use Supr Credits",
    DETAILS: "DETAILS",
    SC_USED_COPY_1: "Awesome! You will have ",
    SC_USED_COPY_2: "remaining after this transaction",
    SUPR_CREDITS: "Supr Credits",
    EXPIRY_SOON: "Supr Credits expiring ",
    EXPIRY_TEXT: "Supr Credits expiring on ",
};

export const SA_OBJECT_NAMES = {
    CLICK: {
        SUPR_CREDITS_CHECKBOX: "supr-credits-checkbox",
    },
    IMPRESSION: {
        SUPR_CREDITS_CONTAINER: "supr-credits-container",
    },
};
