import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { SUPR_CREDITS_DEFAULT_TEXTS, SA_OBJECT_NAMES } from "../constants";
import { City } from "@shared/models/address.model";
import { WalletExpiryDetails } from "@shared/models";

@Component({
    selector: "supr-cart-credits",
    template: `
        <div class="suprCredits">
            <div
                class="suprCreditsContainer"
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                    .SUPR_CREDITS_CONTAINER}"
            >
                <div class="suprRow">
                    <div class="suprColumn creditsImageContainer left">
                        <supr-svg class="creditsImage"></supr-svg>
                    </div>

                    <div class="suprColumn creditsContentContainer left">
                        <ng-container
                            *ngIf="
                                suprCreditsApplicable;
                                else nonUsableSuprCredits
                            "
                        >
                            <supr-text type="action14">
                                {{ texts.HEADING.SUB_TEXT_USE }}
                                {{ suprCreditsAmout }}
                                {{ texts.HEADING.SUB_TEXT_CREDITS }}
                            </supr-text>

                            <ng-container *ngIf="useSuprCredits">
                                <div class="divider4"></div>
                                <supr-text type="caption" class="subTitle">
                                    {{ texts.SC_USED_COPY_1 }}
                                    {{ suprCreditsRemaining }}
                                    {{ texts.SUPR_CREDITS }}
                                    {{ texts.SC_USED_COPY_2 }}
                                </supr-text>
                                <div class="divider4"></div>
                                <div class="suprRow info bottom">
                                    <supr-text
                                        type="paragraph"
                                        (click)="showSuprCreditsModal()"
                                    >
                                        {{ texts.LEARN }}
                                    </supr-text>
                                    <supr-icon name="chevron_right"></supr-icon>
                                </div>
                            </ng-container>
                        </ng-container>
                        <ng-template #nonUsableSuprCredits>
                            <supr-text type="action14">
                                {{ texts.NOT_APPLICABLE_HEADING }}
                            </supr-text>
                        </ng-template>

                        <ng-container
                            *ngIf="!useSuprCredits && expiry?.supr_credits"
                        >
                            <div class="divider4"></div>
                            <supr-text
                                type="caption"
                                class="subTitle expiry"
                                *ngIf="
                                    suprCreditsExpiryDateString;
                                    else dateBlock
                                "
                            >
                                {{ expiry?.supr_credits?.amount }}
                                {{ texts.EXPIRY_SOON }}
                                {{ suprCreditsExpiryDateString }}
                            </supr-text>
                            <ng-template #dateBlock>
                                <supr-text type="caption" class="subTitle">
                                    {{ expiry?.supr_credits?.amount }}
                                    {{ texts.EXPIRY_TEXT }}
                                    {{
                                        expiry.supr_credits.date
                                            | date: "MMM d, y"
                                    }}
                                </supr-text>
                            </ng-template>
                        </ng-container>
                    </div>

                    <div class="suprColumn creditsSelectionContainer right">
                        <ng-container
                            *ngIf="
                                suprCreditsApplicable;
                                else nonUsableSuprCreditsDetails
                            "
                        >
                            <ion-checkbox
                                [checked]="useSuprCredits"
                                (ionChange)="
                                    appySuprCreditsInCart($event.detail.checked)
                                "
                                saClick
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .SUPR_CREDITS_CHECKBOX}"
                                [saObjectValue]="!useSuprCredits"
                            ></ion-checkbox>
                        </ng-container>

                        <ng-template #nonUsableSuprCreditsDetails>
                            <div class="suprRow info bottom">
                                <supr-text
                                    type="caption"
                                    (click)="showSuprCreditsModal()"
                                >
                                    {{ texts.DETAILS }}
                                </supr-text>
                                <supr-icon name="chevron_right"></supr-icon>
                            </div>
                        </ng-template>
                    </div>
                </div>
            </div>
            <supr-credits-info
                [showModal]="showCreditsInfoModal"
                [suprCreditsBalance]="suprCreditsBalance"
                [city]="city"
                [expiry]="expiry"
                [suprCreditsExpiryDateString]="suprCreditsExpiryDateString"
                (handleClose)="closeCreditsInfoModal()"
            ></supr-credits-info>
        </div>
    `,
    styleUrls: ["../styles/supr-cart-credits.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreditsComponent {
    @Input() suprCreditsBalance: number;
    @Input() suprCreditsRemaining: number;
    @Input() useSuprCredits: boolean;
    @Input() city: City;
    @Input() suprCreditsApplicable: boolean;
    @Input() suprCreditsAmout: boolean;
    @Input() expiry: WalletExpiryDetails;
    @Input() suprCreditsExpiryDateString: string;

    @Output() applySuprCredits: EventEmitter<boolean> = new EventEmitter();

    texts = SUPR_CREDITS_DEFAULT_TEXTS;
    showCreditsInfoModal = false;

    appySuprCreditsInCart(useSuprCredits: any) {
        if (this.useSuprCredits === useSuprCredits) {
            return;
        }

        this.applySuprCredits.emit(useSuprCredits);
    }

    removeSuprCreditsInCart() {
        this.applySuprCredits.emit(false);
    }

    showSuprCreditsModal() {
        this.showCreditsInfoModal = true;
    }

    closeCreditsInfoModal() {
        this.showCreditsInfoModal = false;
    }
}
