import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-backdrop",
    template: `
        <div class="backdrop"></div>
        <div class="backdrop"></div>
        <div class="backdrop"></div>
    `,
    styleUrls: ["./supr-backdrop.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprBackdropComponent {}
