import {
    Component,
    AfterViewInit,
    ViewChild,
    ElementRef,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Input,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    Output,
    EventEmitter,
} from "@angular/core";

const INTERSECT_CONTENT_THRESHOLD = 0.9;

@Component({
    selector: "supr-swipeable-wrapper",
    template: `
        <div class="wrapper">
            <div class="pane paneTransparent"></div>
            <div class="pane paneHolder" #swipe_pan>
                <supr-draggable-top></supr-draggable-top>
                <div class="content" [class.contentSticky]="isCollapsed">
                    <ng-content></ng-content>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./supr-swipeable-wrapper.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SwipeableWrapperComponent
    implements AfterViewInit, OnChanges, OnDestroy {
    @Input() scrollDown: boolean;
    @Input() resetScroll = false;

    @Output() handleSwipeCollapse: EventEmitter<boolean> = new EventEmitter();

    @ViewChild("swipe_pan", { static: true }) swipeEl: ElementRef;

    isCollapsed = true;
    private _io: IntersectionObserver;
    private scrollOnTap = false;

    constructor(private cdr: ChangeDetectorRef) {}

    ngAfterViewInit(): void {
        this.registerObserver();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change: SimpleChange = changes["scrollDown"];

        if (!this.canHandleChange(change)) {
            return;
        }

        if (this.scrollDown && !this.isCollapsed) {
            this.handleProgrammaticSwipe();
        }
    }

    ngOnDestroy() {
        if (this._io && this._io.disconnect) {
            this._io.disconnect();
        }
    }

    private handleProgrammaticSwipe() {
        this.scrollOnTap = true;
        this.resetParentNodeScroll();
        this.scrollContentToTop();
    }

    private resetParentNodeScroll() {
        const elem = this.swipeEl.nativeElement;
        const parent = elem.parentNode;

        if (parent) {
            parent.scrollLeft = 0;
            parent.scrollTop = 0;
        }
    }

    private scrollContentToTop() {
        setTimeout(() => {
            const elem = this.swipeEl.nativeElement;
            const contentDiv = elem.querySelector(".content");
            if (contentDiv) {
                contentDiv.scrollTop = 0;
            }

            this.scrollOnTap = false;
        }, 300);
    }

    private registerObserver(): void {
        if (window && "IntersectionObserver" in window) {
            this._io = new IntersectionObserver(
                this.handleIntersect.bind(this),
                this.getIntersectOptions()
            );

            this._io.observe(this.swipeEl.nativeElement);
        } else {
            this.isCollapsed = false;
            this.handleSwipeCollapse.emit(this.isCollapsed);
            this.cdr.detectChanges();
        }
    }

    private handleIntersect(entries: IntersectionObserverEntry[]): void {
        entries.forEach(entry => {
            if (
                entry.isIntersecting ||
                entry.intersectionRatio >= INTERSECT_CONTENT_THRESHOLD
            ) {
                this.isCollapsed = false;
            } else {
                this.isCollapsed = true;
                if (this.resetScroll && !this.scrollOnTap) {
                    this.scrollContentToTop();
                }
            }

            this.handleSwipeCollapse.emit(this.isCollapsed);
            this.cdr.detectChanges();
        });
    }

    private getIntersectOptions(): IntersectionObserverInit {
        return {
            root: null,
            rootMargin: "0px",
            threshold: [INTERSECT_CONTENT_THRESHOLD],
        };
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        );
    }
}
