import { BlockAccessBannerComponent } from "./supr-block-access-banner.component";
import { WrapperComponent } from "./components/wrapper.component";

export const blockAccessBannerComponents = [
    BlockAccessBannerComponent,
    WrapperComponent,
];
