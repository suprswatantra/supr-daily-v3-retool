import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    ChangeDetectorRef,
} from "@angular/core";

import { SuprPassExperimentInfo } from "@shared/models";
import { CartMini } from "@types";

import { DateService } from "@services/date/date.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

export const AUTO_HIDE_BANNER_TIMEOUT = 3000;

@Component({
    selector: "supr-block-access-banner-wrapper",
    template: `
        <ng-container *ngIf="showFloatingBanner && expInfo?.banner">
            <div
                class="floatingBannerWrapper"
                [class.withMiniCart]="isMiniCartShown && showCart"
                [class.onSubsPage]="isFromSubscriptionsPage"
                [class.autoHide]="autoHide"
                [class.cartFooterV2Active]="isCartFooterV2Active"
                saImpression
                saObjectName="${SA_OBJECT_NAMES.IMPRESSION.BLOCK_ACCESS_BANNER}"
            >
                <supr-banner
                    [banner]="expInfo?.banner"
                    [showHideButton]="true"
                    (handleHideButtonClick)="hideBanner()"
                    saBannerContext="${SA_OBJECT_NAMES.CONTEXT}"
                ></supr-banner>
            </div>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-block-access-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnInit, OnChanges {
    @Input() showCart: boolean;
    @Input() showIfCartPopulated: boolean;
    @Input() isFromSubscriptionsPage: boolean;
    @Input() cart: CartMini;
    @Input() expInfo: SuprPassExperimentInfo;
    @Input() autoHide: boolean;
    @Input() isSuprPassAddedToCart: boolean;
    @Input() isCartFooterV2Active: boolean;

    @Output() handleHideBanner: EventEmitter<void> = new EventEmitter();

    isMiniCartShown = false;
    showFloatingBanner = false;

    constructor(
        private dateService: DateService,
        private blockAccessService: BlockAccessService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["cart"] || changes["isSuprPassAddedToCart"];

        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue !== change.currentValue) {
            this.initData();
        }
    }

    /**
     * Toggles show/hide based on various input params using the current time as base for computations
     * if showIfCartPopulated is set, this banner will show only when cart has items
     *
     * @private
     * @returns
     * @memberof WrapperComponent
     */
    private initData() {
        if (!this.blockAccessService.isTimeBasedExperimentActive()) {
            this.hideBanner();
            return;
        }

        if (!this.dateService.hasTodaysCutOffTimePassed()) {
            this.hideBanner();
            return;
        }

        if (this.autoHide) {
            this.autoHideBanner();
        }

        if (this.isSuprPassAddedToCart) {
            this.hideBanner();
            return;
        }

        this.setIsMiniCartShown();

        if (!this.showIfCartPopulated) {
            this.showBanner();
            return;
        }

        if (!this.cart) {
            this.hideBanner();
            return;
        }

        if (this.cart.itemCount > 0) {
            this.showBanner();
            return;
        }

        this.hideBanner();
    }

    hideBanner() {
        this.showFloatingBanner = false;
    }

    private showBanner() {
        this.showFloatingBanner = true;
    }

    private setIsMiniCartShown() {
        if (!this.cart) {
            this.isMiniCartShown = false;
            return;
        }

        if (this.cart.itemCount <= 0) {
            this.isMiniCartShown = false;
        } else if (this.cart.itemCount > 0) {
            this.isMiniCartShown = true;
        }
    }

    private autoHideBanner() {
        setTimeout(() => {
            if (!this.showFloatingBanner) {
                return;
            }

            this.hideBanner();
            if (!this.cdr["destroyed"]) {
                this.cdr.detectChanges();
            }
        }, AUTO_HIDE_BANNER_TIMEOUT);
    }
}
