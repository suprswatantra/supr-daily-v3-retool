import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";

import { SuprPassExperimentInfo } from "@shared/models";
import { CartMini } from "@types";

@Component({
    selector: "supr-block-access-banner",
    template: `
        <supr-block-access-banner-wrapper
            [expInfo]="expInfo$ | async"
            [showIfCartPopulated]="showIfCartPopulated"
            [autoHide]="autoHide"
            [isSuprPassAddedToCart]="isSuprPassAddedToCart"
            [isFromSubscriptionsPage]="isFromSubscriptionsPage"
            [isCartFooterV2Active]="isCartFooterV2Active"
            [showCart]="showCart$ | async"
            [cart]="cart$ | async"
        ></supr-block-access-banner-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlockAccessBannerComponent implements OnInit {
    @Input() showIfCartPopulated: boolean;
    @Input() isFromSubscriptionsPage: boolean;
    @Input() autoHide: boolean;
    @Input() isSuprPassAddedToCart: boolean;
    @Input() isCartFooterV2Active: boolean;

    expInfo$: Observable<SuprPassExperimentInfo>;
    showCart$: Observable<boolean>;
    cart$: Observable<CartMini>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.expInfo$ = this.adapter.suprPassExperimentInfo$;
        this.showCart$ = this.adapter.showCart$;
        this.cart$ = this.adapter.miniCart$;
    }
}
