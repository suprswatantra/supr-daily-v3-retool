import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { WEEK_DAYS_SELECTION_FORMAT, FREQUENCY_OPTIONS } from "@constants";
import { UtilService } from "@services/util/util.service";
import { Frequency, RadioButton } from "@types";

import { ANALYTICS_OBJECT_NAMES } from "@shared/components/supr-create-subscription/constants/analytics.constants";

@Component({
    selector: "supr-frequency-select",
    template: `
        <div class="frequency">
            <supr-radio-button-group
                [options]="_optionsStr"
                colSize="2"
                [selectedIndex]="_selectedIndex"
                (handleSelect)="onSelectOption($event)"
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .CREATE_SUBSCRIPTION_DELIVERY_FREQ}"
                [saContext]="saContext"
            ></supr-radio-button-group>

            <div
                class="suprSlideUp"
                saImpression
                saObjectName="${ANALYTICS_OBJECT_NAMES.IMPRESSION
                    .CREATE_SUBSCRIPTION_WEEKDAYS}"
                [saContext]="saContext"
            >
                <div class="divider20"></div>
                <supr-text class="chooseDays" type="body" *ngIf="_isCustom">
                    Choose days
                </supr-text>
                <div class="divider12"></div>
                <supr-week-days-select
                    [selectedDays]="frequency"
                    (handleSelect)="onCustomDaySelection($event)"
                    [saContext]="saContext"
                ></supr-week-days-select>
            </div>
        </div>
    `,
    styleUrls: ["./supr-frequency-select.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FrequencySelectComponent implements OnInit {
    @Input() frequency: Frequency = {};
    @Input() saContext: string;

    @Output()
    handleFrequencyChange: EventEmitter<Frequency> = new EventEmitter();

    _optionsStr: RadioButton[];
    _selectedIndex: number;
    _isCustom = false;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.initData();
    }

    onSelectOption(selectedIndex: number) {
        this._selectedIndex = selectedIndex;
        this.setCustomOption();
        this.handleFrequencyChange.emit(
            FREQUENCY_OPTIONS[selectedIndex].FREQUENCY
        );
    }

    onCustomDaySelection(day: string) {
        if (!this._isCustom) {
            return;
        }

        const daySelected = !this.frequency[day];
        this.handleFrequencyChange.emit({
            ...this.frequency,
            [day]: daySelected,
        });
    }

    private initData() {
        this.setOptionSelectedIndex();
        this.setFrequencyOptionsStr();
        this.setCustomOption();
    }

    private setFrequencyOptionsStr() {
        this._optionsStr = FREQUENCY_OPTIONS.map((option, index) => ({
            index,
            text: option.TEXT,
        }));
    }

    private setOptionSelectedIndex() {
        const frequencyFormat = this.utilService.getWeekDaysFormatFromDays(
            this.frequency
        );

        switch (frequencyFormat) {
            case WEEK_DAYS_SELECTION_FORMAT.DAILY:
                this._selectedIndex = 0;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.WEEK_DAYS:
                this._selectedIndex = 1;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.WEEK_ENDS:
                this._selectedIndex = 2;
                break;
            case WEEK_DAYS_SELECTION_FORMAT.CUSTOM:
                this._selectedIndex = 3;
                break;
        }
    }

    private setCustomOption() {
        this._isCustom = this._selectedIndex === 3;
    }
}
