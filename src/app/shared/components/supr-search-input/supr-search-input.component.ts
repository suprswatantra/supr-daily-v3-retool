import {
    Component,
    Input,
    Output,
    OnInit,
    EventEmitter,
    ChangeDetectionStrategy,
    ViewChild,
    ElementRef,
} from "@angular/core";

import {
    ANALYTICS_OBJECT_NAMES,
    INPUT,
    MAX_TEXT_INPUT_LIMIT,
} from "@constants";
import { InputComponent } from "@shared/components/supr-input/supr-input.component";

const DEBOUNCE_TIME = 250;

@Component({
    selector: "supr-search-input",
    template: `
        <div class="container suprRow">
            <supr-input
                textType="subheading"
                [type]="inputType"
                [value]="searchTerm"
                [placeholder]="placeholder"
                [debounceTime]="debounceTime"
                [maxLength]="maxLength"
                (handleInputChange)="textChange($event)"
            ></supr-input>
            <div class="icon" (click)="clearSearch()">
                <ng-container *ngIf="searchTerm.length; else searchIcon">
                    <supr-icon
                        name="cross"
                        saClick
                        [saObjectName]="saObjectName"
                    ></supr-icon>
                </ng-container>

                <ng-template #searchIcon>
                    <supr-icon name="search"></supr-icon>
                </ng-template>
            </div>
        </div>
    `,
    styleUrls: ["./supr-search-input.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchInputComponent implements OnInit {
    @Input() searchTerm = "";
    @Input() placeholder: string;
    @Input() debounceTime = DEBOUNCE_TIME;
    @Input() maxLength: number = MAX_TEXT_INPUT_LIMIT;
    @Output() handleChangeText: EventEmitter<string> = new EventEmitter();

    @ViewChild(InputComponent, { static: true }) inputComponent: InputComponent;

    inputType = INPUT.TYPES.TEXT;
    saObjectName = ANALYTICS_OBJECT_NAMES.CLICK.SEARCH_CLEAR;

    private inputEl: ElementRef;

    ngOnInit() {
        this.inputEl = this.inputComponent.getInputElement();
        // this.setFocus();
    }

    textChange(searchTerm: string) {
        this.searchTerm = searchTerm;
        this.handleChangeText.emit(searchTerm);
    }

    clearSearch() {
        this.textChange("");
        this.inputEl.nativeElement.value = "";
        this.setFocus();
    }

    private setFocus() {
        this.inputEl.nativeElement.focus();
    }
}
