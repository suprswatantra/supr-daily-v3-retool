import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-page-footer",
    template: `
        <supr-sticky-wrapper>
            <div class="wrapper">
                <ng-content></ng-content>
            </div>
        </supr-sticky-wrapper>
    `,
    styleUrls: ["./supr-page-footer.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageFooterComponent {}
