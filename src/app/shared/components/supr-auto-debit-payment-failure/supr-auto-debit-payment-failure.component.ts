import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from "rxjs";
import { MiscAdapter as Adapter } from "@shared/adapters/misc.adapter";

@Component({
    selector: "supr-auto-debit-payment-failure-nudge",
    template: `
        <supr-auto-debit-payment-failure-wrapper
            [showModal]="showModal$ | async"
            [suprPassSubscriptionId]="suprPassSubscriptionId$ | async"
            (handleHideModal)="hideExitCartModal()"
        ></supr-auto-debit-payment-failure-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutoDebitPaymentFailureComponent implements OnInit {
    showModal$: Observable<boolean>;
    suprPassSubscriptionId$: Observable<number>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.showModal$ = this.adapter.showAutoDebitPaymentFailureNudge$;
        this.suprPassSubscriptionId$ = this.adapter.suprPassSubscriptionId$;
    }

    hideExitCartModal() {
        this.adapter.hideAutoDebitPaymentFailureNudge();
    }
}
