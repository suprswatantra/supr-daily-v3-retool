export const SA_OBJECT_NAMES = {
    CLICK: {
        RECHARGE_NOW:
            "supr-access-auto-debit-payment-failure-nudge-recharge-now",
        RECHARGE_LATER:
            "supr-access-auto-debit-payment-failure-nudge-recharge-later",
    },
};
