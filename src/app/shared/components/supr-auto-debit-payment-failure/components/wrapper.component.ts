import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    SimpleChanges,
    OnChanges,
    EventEmitter,
    SimpleChange,
} from "@angular/core";

import {
    MODAL_NAMES,
    NUDGE_SETTINGS_KEYS,
    NUDGE_TYPES,
    SETTINGS_KEYS_DEFAULT_VALUE,
} from "@constants";

import { ModalService } from "@services/layout/modal.service";
import { RouterService } from "@services/util/router.service";
import { SettingsService } from "@services/shared/settings.service";

import { AutoDebitPaymentFailureSetting } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics";

@Component({
    selector: "supr-auto-debit-payment-failure-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal
                (handleClose)="handleHideModal.emit()"
                modalName="${MODAL_NAMES.AUTO_DEBIT_PAYMENT_FAILURE}"
            >
                <div class="wrapper">
                    <div class="suprRow">
                        <supr-text type="subtitle">
                            {{ nudge?.title }}
                        </supr-text>
                    </div>
                    <div class="divider12"></div>

                    <supr-text type="body" class="subtitle">
                        {{ nudge?.subTitle }}
                    </supr-text>
                    <supr-text type="body" class="subtitle">
                        {{ nudge?.noteText }}
                    </supr-text>
                    <div class="divider24"></div>

                    <div class="suprRow">
                        <div class="buttonWrapper">
                            <supr-button
                                class="leave"
                                (handleClick)="closeModal()"
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .RECHARGE_LATER}"
                            >
                                <supr-text type="body">
                                    {{ nudge?.secondaryButton?.text }}
                                </supr-text>
                            </supr-button>
                        </div>

                        <div class="buttonWrapper right">
                            <supr-button
                                saObjectName="${SA_OBJECT_NAMES.CLICK
                                    .RECHARGE_NOW}"
                                (handleClick)="primaryBtnClick()"
                            >
                                <supr-text type="body">
                                    {{ nudge?.primaryButton?.text }}
                                </supr-text>
                            </supr-button>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-auto-debit-payment-failure.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent implements OnChanges {
    @Input() showModal: boolean;
    @Input() suprPassSubscriptionId: number;

    @Output() handleHideModal: EventEmitter<void> = new EventEmitter();

    nudge: AutoDebitPaymentFailureSetting.Nudge;

    constructor(
        private modalService: ModalService,
        private routerService: RouterService,
        private settingsService: SettingsService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleShowModalChange(changes["showModal"]);
    }

    closeModal() {
        this.modalService.closeModal();
    }

    primaryBtnClick() {
        this.closeModal();
        this.handleRechargeBtnClick();
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (!change.previousValue && change.currentValue) {
            this.initialize();
        }
    }

    private initialize() {
        this.getAutoDebitPaymentFailureNudgeData();
    }

    private getAutoDebitPaymentFailureNudgeData() {
        this.nudge = this.settingsService.getSettingsValue(
            NUDGE_SETTINGS_KEYS[NUDGE_TYPES.AUTO_DEBIT_PAYMENT_FAILURE],
            SETTINGS_KEYS_DEFAULT_VALUE.autoDebitPaymentFailureNudge
        );
    }

    private handleRechargeBtnClick() {
        if (!this.suprPassSubscriptionId) {
            return;
        }

        this.routerService.goToSubscriptionRechargePage(
            this.suprPassSubscriptionId
        );
    }
}
