import { AutoDebitPaymentFailureComponent } from "./supr-auto-debit-payment-failure.component";
import { WrapperComponent } from "./components/wrapper.component";

export const autoDebitPaymentFailureComponents = [
    AutoDebitPaymentFailureComponent,
    WrapperComponent,
];
