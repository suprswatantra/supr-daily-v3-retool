import { FeaturedCardComponent } from "./feature.card.component";

import { FeaturedComponent } from "./components/featured.component";
import { FeaturedCardTileComponent } from "./components/featured-title.component";
import { FeaturedSkeletonComponent } from "./components/featured-skeleton.component";

export const featuredComponents = [
    FeaturedComponent,
    FeaturedCardTileComponent,
    FeaturedSkeletonComponent,
    FeaturedCardComponent,
];
