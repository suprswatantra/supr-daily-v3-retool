import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-featured-skeleton",
    template: `
        <div class="featured">
            <div class="featuredContainer">
                <ion-skeleton-text animated class="tile"></ion-skeleton-text>
                <ion-skeleton-text animated class="tile"></ion-skeleton-text>
            </div>
        </div>
    `,
    styleUrls: ["../styles/featured.loader.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedSkeletonComponent {}
