import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
    OnChanges,
    SimpleChange,
    SimpleChanges,
} from "@angular/core";

import { Featured } from "@models";

import { CLODUINARY_IMAGE_SIZE } from "@constants";
import { HomeLayoutService } from "@services/layout/home-layout.service";

@Component({
    selector: "supr-featured-tile",
    template: `
        <div
            class="featuredTile"
            [ngClass]="featuredTileClassName"
            (click)="handleItemClick()"
        >
            <supr-image
                [src]="item?.image?.fullUrl"
                [image]="item?.image"
                [withWrapper]="false"
                [imgWidth]="imgWidth"
                [imgHeight]="imgHeight"
                [lazyLoad]="false"
            ></supr-image>
        </div>
    `,
    styleUrls: ["../styles/featured.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedCardTileComponent implements OnInit, OnChanges {
    @Input() item: Featured.Item;
    @Input() version: number;

    imgWidth: number;
    imgHeight: number;
    featuredTileClassName: string;

    constructor(private homeLayoutService: HomeLayoutService) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.canHandleChange(changes["item"])) {
            this.initialize();
        }
    }

    handleItemClick() {
        this.homeLayoutService.handleFeaturedItemClick(this.item);
    }

    private initialize() {
        if (this.version === CLODUINARY_IMAGE_SIZE.FEATURED_V2.V2) {
            this.imgWidth = CLODUINARY_IMAGE_SIZE.FEATURED_V2.IMG_WIDTH;
            this.imgHeight = CLODUINARY_IMAGE_SIZE.FEATURED_V2.IMG_HEIGHT;
            this.featuredTileClassName =
                CLODUINARY_IMAGE_SIZE.FEATURED_V2.V2_CLASSNAME;
            return;
        }

        this.imgWidth = CLODUINARY_IMAGE_SIZE.FEATURED.WIDTH;
        this.imgHeight = CLODUINARY_IMAGE_SIZE.FEATURED.HEIGHT;
        this.featuredTileClassName = "";
        return;
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }
}
