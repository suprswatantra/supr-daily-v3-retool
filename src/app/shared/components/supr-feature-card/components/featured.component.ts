import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    Output,
    EventEmitter,
} from "@angular/core";

import { SA_IMPRESSIONS_IS_ENABLED, FEATURED_CARD_CONSTANTS } from "@constants";

import { Featured } from "@models";
import { FeaturedCurrentState } from "@store/featured/featured.state";

import { ANALYTICS_OBJECT_NAMES } from "@pages/home/constants/analytics.constants";

@Component({
    selector: "supr-feature-card-component",
    template: `
        <ng-container *ngIf="loading; else content">
            <div class="discoverySpacing"></div>
            <supr-featured-skeleton></supr-featured-skeleton>
            <div class="discoverySpacing" *ngIf="home === componentName"></div>
        </ng-container>

        <ng-template #content>
            <div
                class="featured"
                *ngIf="featured?.itemList?.length"
                id="home-feature_section"
            >
                <supr-text type="subtitle">{{
                    featured?.displayName
                }}</supr-text>
                <div class="discoveryHalfSpacing"></div>
                <div class="featuredSlider">
                    <ng-container
                        *ngFor="
                            let item of featured?.itemList;
                            trackBy: trackByFn;
                            let i = index
                        "
                    >
                        <supr-featured-tile
                            [item]="item"
                            [version]="featured.version"
                            saClick
                            saImpression
                            [saImpressionEnabled]="
                                ${SA_IMPRESSIONS_IS_ENABLED.HOME_FEATURED}
                            "
                            [saObjectName]="getSaEventName(i)"
                            [saObjectValue]="item.name"
                            [saContext]="componentId"
                        >
                        </supr-featured-tile>
                    </ng-container>
                </div>
                <div
                    class="discoverySpacing"
                    *ngIf="home === componentName"
                ></div>
            </div>
        </ng-template>
    `,
    styleUrls: ["../styles/featured.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedComponent implements OnChanges {
    @Input() featured: Featured.List;
    @Input() featuredState: FeaturedCurrentState;
    @Input() appLaunchDone: boolean;
    @Input() componentName: string;
    @Input() componentId = "";

    @Output() handleFetchFeatured: EventEmitter<void> = new EventEmitter();

    loading = true;
    home = FEATURED_CARD_CONSTANTS.HOME;

    ngOnChanges(changes: SimpleChanges) {
        this.handleFeaturedStateChange(changes["featured"]);
        this.handleAppLaunchDoneChange(changes["appLaunchDone"]);
    }

    trackByFn(item: Featured.Item): number {
        return item.id;
    }

    getSaEventName(index: number): string {
        return `${ANALYTICS_OBJECT_NAMES.CLICK.HOME_FEATURED_CARD_PREFIX}${
            index + 1
        }`;
    }

    private handleFeaturedStateChange(change: SimpleChange) {
        if (!change) {
            return;
        }

        const { currentValue } = change;
        if (!currentValue && this.appLaunchDone) {
            this.handleFetchFeatured.emit();
        } else if (this.appLaunchDone && currentValue) {
            this.loading = false;
        }
    }

    private handleAppLaunchDoneChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            if (this.appLaunchDone && this.fetchFeaturedListDone()) {
                this.loading = false;
            } else if (this.appLaunchDone && !this.fetchFeaturedListDone()) {
                this.handleFetchFeatured.emit();
            }
        }
    }

    private canHandleChange(change: SimpleChange) {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private fetchFeaturedListDone(): boolean {
        return !!this.featured;
    }
}
