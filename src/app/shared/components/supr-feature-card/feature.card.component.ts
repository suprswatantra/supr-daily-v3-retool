import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { FEATURED_CARD_CONSTANTS } from "@constants";

import { Featured } from "@models";
import { FeaturedCurrentState } from "@store/featured/featured.state";

import { MiscAdapter as Adapter } from "../../adapters/misc.adapter";

@Component({
    selector: "supr-feature-card",
    template: `
        <supr-feature-card-component
            [featured]="featuredList$ | async"
            [featuredState]="featuredState$ | async"
            [appLaunchDone]="appLaunchDone$ | async"
            [componentName]="componentName"
            [componentId]="componentId"
            (handleFetchFeatured)="fetchFeaturedList()"
        ></supr-feature-card-component>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedCardComponent implements OnInit {
    @Input() componentName: string;
    @Input() componentId = "";
    @Input() silent = false;

    featuredList$: Observable<Featured.List>;
    featuredState$: Observable<FeaturedCurrentState>;
    appLaunchDone$: Observable<boolean>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        const COMPONENT_NAME = this.componentId
            ? String(this.componentId)
            : FEATURED_CARD_CONSTANTS.HOME;

        this.featuredList$ = this.adapter.fetchFeaturedListByComponent(
            COMPONENT_NAME
        );
        this.featuredState$ = this.adapter.featuredCurrentState$;
        this.appLaunchDone$ = this.adapter.appLaunchDone$;
    }

    fetchFeaturedList() {
        this.adapter.fetchFeaturedList({
            component: this.componentName,
            component_id: this.componentId,
            silent: this.silent,
        });
    }
}
