import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-form-error",
    template: `
        <supr-text type="subtext10" class="error" *ngIf="errMsg?.length > 0">
            {{ errMsg }}
        </supr-text>
    `,
    styleUrls: ["../supr-input-label.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormErrorComponent {
    @Input() errMsg: string;
}
