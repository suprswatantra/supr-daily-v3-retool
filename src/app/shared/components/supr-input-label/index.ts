import { InputLabelComponent } from "./supr-input-label.component";
import { FormErrorComponent } from "./components/error.component";

export const inputLabelComponents = [InputLabelComponent, FormErrorComponent];
