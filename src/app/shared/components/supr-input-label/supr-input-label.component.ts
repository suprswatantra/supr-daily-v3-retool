import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";
import { INPUT, MAX_TEXT_INPUT_LIMIT } from "@constants";

@Component({
    selector: "supr-input-label",
    template: `
        <div class="wrapper">
            <supr-text type="body">{{ label }}</supr-text>
            <div class="divider4"></div>
            <supr-input
                *ngIf="!isLabelIcon"
                [type]="type"
                [textType]="textType"
                [placeholder]="placeholder"
                [value]="value"
                [disabled]="disabled"
                [debounceTime]="debounceTime"
                [class.disabled]="disabled"
                [maxLength]="maxLength"
                (handleInputChange)="handleInputChange.emit($event)"
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                [saPosition]="saPosition"
                [saContext]="saContext"
            >
            </supr-input>
            <div
                *ngIf="isLabelIcon"
                [class.disabled]="disabled"
                class="suprRow wrapperIconContent"
            >
                <supr-text type="subheading" *ngIf="value">
                    {{ value }}
                </supr-text>

                <supr-text
                    type="subheading"
                    class="placeholderText"
                    *ngIf="!value"
                >
                    {{ placeholder }}
                </supr-text>

                <supr-icon name="chevron_right"></supr-icon>
            </div>
            <supr-form-error [errMsg]="error"></supr-form-error>
        </div>
    `,
    styleUrls: ["./supr-input-label.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputLabelComponent {
    @Input() type: string = INPUT.TYPES.TEXT;
    @Input() textType: string;
    @Input() placeholder = "";
    @Input() disabled = false;
    @Input() value = "";
    @Input() debounceTime = INPUT.DEBOUNCE_TIME;
    @Input() label = "";
    @Input() maxLength = MAX_TEXT_INPUT_LIMIT;
    @Input() isLabelIcon = false;

    @Input() saObjectName: string;
    @Input() saObjectValue: string;
    @Input() saContext: string;
    @Input() saPosition: number;

    @Input() error: string;

    @Output() handleInputChange: EventEmitter<string> = new EventEmitter();
}
