import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { ScheduleAdapter } from "@shared/adapters/schedule.adapter";

import { Subscription, Vacation } from "@shared/models";

import { ScheduleDict } from "@types";

@Component({
    selector: "supr-subscription-delivery-schedule-container",
    template: `
        <supr-subscription-balance-modal
            *ngIf="showModal"
            [vacation]="vacation$ | async"
            [showSubBalanceModal]="showModal"
            [activeSubscription]="subscription"
            [activeSku]="subscription?.sku_data"
            [scheduleData]="scheduleDict$ | async"
            [secondaryActionText]="secondaryActionText"
            [instantRefreshSchedule]="instantRefreshSchedule"
            (handleShowModalChange)="handleModalClose.emit()"
        ></supr-subscription-balance-modal>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSubDeliveryEditContainer implements OnInit {
    @Input() showModal = false;
    @Input() subscription: Subscription;
    @Input() secondaryActionText?: string;
    @Input() instantRefreshSchedule: boolean;

    @Output() handleModalClose: EventEmitter<boolean> = new EventEmitter();

    vacation$: Observable<Vacation>;
    scheduleDict$: Observable<ScheduleDict>;

    constructor(private scheduleAdapter: ScheduleAdapter) {}

    ngOnInit() {
        this.vacation$ = this.scheduleAdapter.vacation$;
        this.scheduleDict$ = this.scheduleAdapter.scheduleDict$;
    }
}
