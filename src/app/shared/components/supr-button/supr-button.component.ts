import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { Segment } from "@types";

@Component({
    selector: "supr-button",
    template: `
        <button
            #btn
            type="button"
            class="button ion-activatable"
            [class.disabled]="disabled"
            [disabled]="disabled"
            (click)="onClick($event)"
            saClick
            [saObjectName]="saObjectName"
            [saObjectValue]="saObjectValue"
            [saPosition]="saPosition"
            [saContext]="saContext"
            [saContextList]="saContextList"
        >
            <ng-container *ngIf="loading; else content">
                <supr-loader></supr-loader>
            </ng-container>

            <ng-template #content>
                <ion-ripple-effect></ion-ripple-effect>
                <ng-content></ng-content>
            </ng-template>
        </button>
    `,
    styleUrls: ["./supr-button-component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
    @Input() disabled = false;
    @Input() loading = false;
    @Input() saObjectName: string;
    @Input() saObjectValue: any;
    @Input() saContext: string;
    @Input() saContextList: Segment.ContextListItem[] = [];
    @Input() saPosition: number;
    @Output() handleClick: EventEmitter<TouchEvent> = new EventEmitter();

    onClick(event: TouchEvent) {
        if (!this.loading && !this.disabled && this.handleClick) {
            setTimeout(() => this.handleClick.emit(event), 100);
        }
    }
}
