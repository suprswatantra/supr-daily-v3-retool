import {
    Input,
    Output,
    Component,
    OnChanges,
    EventEmitter,
    SimpleChange,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { TOAST_MESSAGES, RATING_NUDGE_TRIGGERS } from "@constants";

import { ScheduleItem, Sku } from "@shared/models";

import { ModalService } from "@services/layout/modal.service";
import { ToastService } from "@services/layout/toast.service";
import { NudgeService } from "@services/layout/nudge.service";
import { MoengageService } from "@services/integration/moengage.service";

import { AddonCurrentState } from "@store/addon/addon.state";

@Component({
    selector: "supr-schedule-addon-edit-layout",
    template: `
        <supr-schedule-edit-modal-card
            [sku]="sku"
            [date]="date"
            [showCounter]="false"
            [schedule]="schedule"
            [adjustmentAction]="adjustmentAction"
            [amountChange]="amountChange"
            [showCancelAction]="true"
            [showCancelConfirmation]="showCancelConfirmation"
            (handleButtonClick)="onButtonClick($event)"
            (handleCancelTextClick)="onCancelTextClick()"
        ></supr-schedule-edit-modal-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScheduleAddonEditLayoutComponent implements OnChanges {
    amountChange: number;
    counterQuantity: number;
    adjustmentAction: string;
    showCancelConfirmation: boolean;

    @Input() sku: Sku;
    @Input() date: string;
    @Input() walletBalance: number;
    @Input() schedule: ScheduleItem;
    @Input() addonCurrentState: AddonCurrentState;

    /* [TODO] Add Model - code disabled at the moment */
    @Output() updateAddon: EventEmitter<any> = new EventEmitter();
    @Output() cancelAddon: EventEmitter<number> = new EventEmitter();
    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    constructor(
        private modalService: ModalService,
        private toastService: ToastService,
        private moengageService: MoengageService,
        private nudgeService: NudgeService
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        this.handleAddonStateChange(changes["addonCurrentState"]);
    }

    onCancelTextClick() {
        if (!this.sku) {
            return;
        }

        this.showCancelConfirmation = true;
        this.amountChange = this.schedule.quantity * this.sku.unit_price;
        /* [TODO] - Move to constants */
        this.adjustmentAction = "refund";
    }

    onButtonClick(action: string) {
        /* [TODO] Move actions to constants */
        switch (action) {
            case "cancel":
                this.updateActionListener.emit(this.date);
                this.cancelAddon.emit(this.schedule.id);

                break;
            default:
                this.modalService.closeModal();
        }
    }

    private handleAddonStateChange(addonStateChange: SimpleChange) {
        if (
            addonStateChange &&
            !addonStateChange.firstChange &&
            addonStateChange.currentValue !== addonStateChange.previousValue
        ) {
            switch (addonStateChange.currentValue) {
                case AddonCurrentState.CANCELLING_ADDON_DONE: {
                    this.toastService.present(TOAST_MESSAGES.ADDONS.CANCELLED);
                    this.modalService.closeModal();
                    this.sendAnalyticsEvents();
                    this.sendRatingNudge();
                    break;
                }
            }
        }
    }

    private sendAnalyticsEvents() {
        this.moengageService.trackDeliveryCancellation({
            type: "addon",
            sku_id: this.sku.id,
            delivery_date: this.date,
        });
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(RATING_NUDGE_TRIGGERS.CANCEL_ADDON);
    }
}
