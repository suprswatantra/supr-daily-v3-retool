import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

import { SETTINGS, SETTINGS_KEYS_DEFAULT_VALUE } from "@constants";

import { SettingsService } from "@services/shared/settings.service";

@Component({
    selector: "supr-search-box",
    template: `
        <div class="searchBox suprRow">
            <supr-icon name="search"></supr-icon>
            <supr-text type="body">{{ placeholderText }}</supr-text>
        </div>
    `,
    styleUrls: ["./supr-search-box.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchBoxComponent {
    @Input() placeholderText = "";

    constructor(private settingsService: SettingsService) {
        const searchBoxText = this.settingsService.getSettingsValue(
            SETTINGS.SEARCHBOX_PLACEHOLDER,
            SETTINGS_KEYS_DEFAULT_VALUE[SETTINGS.SEARCHBOX_PLACEHOLDER]
        );
        this.placeholderText = searchBoxText.default;
    }
}
