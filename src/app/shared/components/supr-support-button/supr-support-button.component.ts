import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-support-button",
    template: `
        <div class="actionButton" *ngIf="show">
            <supr-button
                *ngIf="show"
                (handleClick)="handleClick.emit()"
                saClick
                saImpression
                [saObjectName]="saObjectName"
                [saObjectValue]="saObjectValue"
                [saContext]="saContext"
            >
                {{ displayText }}
            </supr-button>
        </div>
    `,
    styleUrls: ["./supr-support-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSupportButton {
    @Input() show: boolean = false;
    @Input() displayText: String = "Contact Us";
    @Input() saObjectName: string;
    @Input() saObjectValue: string;
    @Input() saContext: string;
    @Output() handleClick: EventEmitter<void> = new EventEmitter();
}
