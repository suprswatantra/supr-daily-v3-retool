import { Component, Input } from "@angular/core";

@Component({
    selector: "supr-subscription-deliveries-message",
    template: `
        <supr-ribbon>
            <div class="suprRow top left">
                <div>
                    <supr-text type="caption">
                        {{ title }}
                    </supr-text>
                </div>
            </div>
        </supr-ribbon>
    `,
    styleUrls: ["./supr-subscription-deliveries-message.component.scss"],
})
export class SuprSubscriptionDeliveriesMessageComponent {
    @Input() title: string;
}
