import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-schedule-wrapper-date",
    template: `
        <div class="scheduleWrapperDate">
            <div class="suprRow center">
                <supr-text class="dateText" type="heading">{{
                    date | date: "d"
                }}</supr-text>
            </div>
            <div class="suprRow center">
                <supr-text class="dayText" type="caption">{{
                    date | date: "LLL" | uppercase
                }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./supr-schedules-wrapper-date.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleWrapperDateComponent {
    @Input() date: string;
}
