import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
    SimpleChanges,
    SimpleChange,
    OnChanges,
} from "@angular/core";

@Component({
    selector: "supr-progress-bar",
    template: `
        <div class="wrapper suprRow" #progressWrapper>
            <div class="wrapperFill"></div>
        </div>
    `,
    styleUrls: ["./supr-progress-bar.compopnent.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressBarComponent implements OnInit, OnChanges {
    @Input() percentage: string;

    @ViewChild("progressWrapper", { static: true }) wrapperEl: ElementRef;

    ngOnInit() {
        this.setStyles();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["percentage"];
        this.handleChange(change);
    }

    private setStyles() {
        if (!this.percentage) {
            return;
        }

        this.wrapperEl.nativeElement.style.setProperty(
            "--progress-width-percentage",
            this.percentage + "%"
        );
    }

    private handleChange(change: SimpleChange) {
        if (change && !change.firstChange) {
            this.setStyles();
        }
    }
}
