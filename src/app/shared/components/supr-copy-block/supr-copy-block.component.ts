import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
} from "@angular/core";

import { Clipboard } from "@ionic-enterprise/clipboard/ngx";

import { TOAST_MESSAGES } from "@constants";

import { TextFragment } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { ToastService } from "@services/layout/toast.service";
import { PlatformService } from "@services/util/platform.service";

import { HELP_TEXT } from "./constants";
import { SA_OBJECT_NAMES } from "./constants/analytics.constants";

@Component({
    selector: "supr-copy-block",
    template: `
        <div class="suprRow copyBlockWrapper" #copyBlockWrapper>
            <div class="suprRow" (click)="handleCopyBlockClick()">
                <div
                    class="copyBlockTextContainer suprRow"
                    [class.borderHidden]="hideBorder"
                    saClick
                    [saObjectName]="copyCodeClickSaObjectName"
                >
                    <ng-container *ngIf="!hideCopyText">
                        <supr-text-fragment
                            [textFragment]="copyText"
                        ></supr-text-fragment>
                        <div class="spacer4"></div>
                    </ng-container>

                    <supr-icon name="copy_code"></supr-icon>
                </div>
                <div class="spacer8" *ngIf="!hideCopyText"></div>
                <ng-container *ngIf="showHelpText">
                    <div class="helpText">
                        <supr-text type="caption">${HELP_TEXT}</supr-text>
                    </div>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["./supr-copy-block.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyBlockComponent implements OnInit {
    @Input() copyText: TextFragment;
    @Input() bgColor: string;
    @Input() iconColor: string;
    @Input() borderColor: string;
    @Input() showHelpText: boolean;
    @Input() hideBorder: boolean;
    @Input() hideCopyText: boolean;
    @Input() copyCodeClickSaObjectName: string =
        SA_OBJECT_NAMES.CLICK.COPY_CODE;

    @Output() handleClick: EventEmitter<void> = new EventEmitter();

    @ViewChild("copyBlockWrapper", { static: true }) wrapperEl: ElementRef;

    constructor(
        private utilService: UtilService,
        private clipboard: Clipboard,
        private toastService: ToastService,
        private platformService: PlatformService
    ) {}

    ngOnInit() {
        this.setStyles();
    }

    handleCopyBlockClick() {
        const copyText = this.utilService.getNestedValue(this.copyText, "text");

        if (!copyText || !this.platformService.isCordova()) {
            return;
        }

        this.clipboard.copy(copyText);

        this.toastService.present(TOAST_MESSAGES.COPY_BLOCK);
    }

    private setStyles() {
        this.utilService.setStyleAttribute(
            this.wrapperEl,
            "--supr-copy-block-bg-color",
            this.bgColor
        );

        this.utilService.setStyleAttribute(
            this.wrapperEl,
            "--supr-copy-block-icon-color",
            this.iconColor
        );

        this.utilService.setStyleAttribute(
            this.wrapperEl,
            "--supr-copy-block-border-color",
            this.borderColor
        );

        const fontSize = this.utilService.getNestedValue(
            this.copyText,
            "fontSize"
        );

        if (fontSize) {
            this.utilService.setStyleAttribute(
                this.wrapperEl,
                "--supr-copy-block-icon-font-size",
                fontSize
            );
        }
    }
}
