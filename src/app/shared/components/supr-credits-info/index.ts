import { CreditsInfoComponent } from "./supr-credits-info.component";
import { WrapperComponent } from "./components/wrapper.component";
import { FaqItemComponent } from "./components/faq/item.component";
import { FaqItemListComponent } from "./components/faq/item-list.component";

export const creditsInfoComponents = [
    CreditsInfoComponent,
    WrapperComponent,
    FaqItemComponent,
    FaqItemListComponent,
];
