import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { SettingsService } from "@services/shared/settings.service";
import { City } from "@shared/models/address.model";
import { DEFAULT_FAQ_LIST, TEXTS } from "./constants";
import { WalletExpiryDetails } from "@shared/models";

@Component({
    selector: "supr-credits-info",
    template: `
        <supr-credits-info-wrapper
            [showModal]="showModal"
            [content]="info_content"
            [suprCreditsBalance]="suprCreditsBalance"
            [expiry]="expiry"
            [suprCreditsExpiryDateString]="suprCreditsExpiryDateString"
            (handleClose)="handleClose?.emit()"
        ></supr-credits-info-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreditsInfoComponent implements OnInit {
    @Input() showModal: boolean;
    @Input() suprCreditsBalance: number;
    @Input() city: City;
    @Input() expiry: WalletExpiryDetails;
    @Input() suprCreditsExpiryDateString: string;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    info_content = {};

    constructor(private settingsService: SettingsService) {}

    ngOnInit() {
        const cityId = this.city
            ? this.city.id.toString()
            : TEXTS.DEFAULT_CITY_ID;

        const faqKey = TEXTS.FAQ_PREFIX + cityId;
        const faqData = this.settingsService.getSettingsValue(faqKey);

        if (faqData) {
            this.info_content = faqData;
        } else {
            this.info_content = DEFAULT_FAQ_LIST;
        }
    }
}
