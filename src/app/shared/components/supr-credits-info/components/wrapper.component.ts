import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
} from "@angular/core";

import { TEXTS } from "@shared/components/supr-credits-info/constants";
import { SA_OBJECT_NAMES } from "@shared/components/supr-credits-info/constants/analytics";
import { WalletExpiryDetails } from "@shared/models";
@Component({
    selector: "supr-credits-info-wrapper",
    template: `
        <ng-container *ngIf="showModal">
            <supr-modal (handleClose)="handleClose?.emit()">
                <div class="wrapper">
                    <supr-text type="subtitle">{{ texts.HEADING }}</supr-text>
                    <div class="divider16"></div>
                    <div class="suprRow info bottom">
                        <div class="suprColumn textContainer left">
                            <supr-text type="regular14" class="subHeading">
                                {{ content.desc }}
                            </supr-text>
                            <div class="divider12"></div>
                            <supr-text type="regular14" class="infoText">
                                {{ texts.INFO_TEXT }}
                            </supr-text>
                        </div>
                    </div>
                    <div class="divider16"></div>
                    <div class="suprRow balance">
                        <div class="suprColumn left center">
                            <supr-text type="paragraph" class="balanceText">
                                {{ texts.CURRENT_BALANCE }}
                            </supr-text>
                        </div>
                        <div class="suprColumn right center">
                            <div class="suprRow">
                                <supr-text
                                    type="subtitle"
                                    class="balanceAmount"
                                >
                                    {{ suprCreditsBalance || 0 }}
                                </supr-text>
                                <div class="spacer4"></div>
                                <supr-svg class="creditsImage"></supr-svg>
                            </div>
                        </div>
                    </div>

                    <ng-container *ngIf="expiry?.supr_credits?.date">
                        <div class="divider8"></div>
                        <supr-text
                            type="caption"
                            class="subTitle expiry"
                            *ngIf="suprCreditsExpiryDateString; else dateBlock"
                        >
                            {{ texts.NOTE }}
                            {{ expiry?.supr_credits?.amount }}
                            {{ texts.EXPIRY_SOON }}
                            {{ suprCreditsExpiryDateString }}
                        </supr-text>
                        <ng-template #dateBlock>
                            <supr-text type="caption" class="subTitle">
                                {{ texts.NOTE }}
                                {{ expiry?.supr_credits?.amount }}
                                {{ texts.EXPIRY_TEXT }}
                                {{
                                    expiry.supr_credits.date | date: "MMM d, y"
                                }}
                            </supr-text>
                        </ng-template>
                    </ng-container>
                    <div class="divider24"></div>
                    <div class="wrapperFaq">
                        <supr-text type="subheading"
                            >Frequently asked questions</supr-text
                        >
                        <div class="divider8"></div>

                        <div
                            class="wrapperFaqList"
                            saImpression
                            saObjectName="${SA_OBJECT_NAMES.IMPRESSION
                                .FAQS_VIEWED}"
                        >
                            <supr-sc-faq-item-list
                                [faqList]="content?.faqs"
                            ></supr-sc-faq-item-list>
                        </div>
                    </div>
                </div>
            </supr-modal>
        </ng-container>
    `,
    styleUrls: ["../styles/supr-credits-info.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() showModal: boolean;
    @Input() suprCreditsBalance: number;
    @Input() content: any;
    @Input() expiry: WalletExpiryDetails;
    @Input() suprCreditsExpiryDateString: string;

    @Output() handleClose: EventEmitter<void> = new EventEmitter();

    texts = TEXTS;
}
