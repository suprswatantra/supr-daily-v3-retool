import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-sc-faq-item-list",
    template: `
        <div *ngFor="let faq of faqList; trackBy: trackByFn; last as isLast">
            <supr-sc-faq-item [faq]="faq"></supr-sc-faq-item>
            <div class="divider16"></div>
            <div class="separator" *ngIf="!isLast"></div>
        </div>
    `,
    styleUrls: ["../../styles/faq.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemListComponent {
    @Input() faqList: any[];

    trackByFn(index: number): number {
        return index;
    }
}
