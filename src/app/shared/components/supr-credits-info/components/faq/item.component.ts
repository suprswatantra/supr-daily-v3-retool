import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { ICONS } from "@pages/support/constants";
import { UtilService } from "@services/util/util.service";
import { SA_OBJECT_NAMES } from "../../constants/analytics";

@Component({
    selector: "supr-sc-faq-item",
    template: `
        <div
            (click)="toggleItem()"
            saClick
            [saObjectName]="getSaObjectName()"
            [saObjectValue]="faq.title"
        >
            <div class="suprRow spaceBetween item">
                <div class="heading">
                    <supr-text type="body"> {{ faq.ques }}</supr-text>
                </div>
                <supr-icon
                    [class.up]="!folded"
                    name="${ICONS.CHEVRON_DOWN}"
                ></supr-icon>
            </div>

            <div class="description" [class.suprHide]="folded">
                <supr-text type="regular14"> {{ faq.ans }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["../../styles/faq.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqItemComponent {
    @Input() faq: any;

    folded = true;
    saObjectNames = SA_OBJECT_NAMES;

    constructor(private utilService: UtilService) {}

    toggleItem() {
        this.folded = !this.folded;
    }

    getSaObjectName(): string {
        if (this.utilService.isEmpty(this.saObjectNames)) {
            return;
        }

        return this.folded
            ? this.saObjectNames["CLICK"].QUESTION_CLOSE
            : this.saObjectNames["CLICK"].QUESTION_OPEN;
    }
}
