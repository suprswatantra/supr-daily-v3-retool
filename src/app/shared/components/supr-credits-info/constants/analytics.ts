export const SA_OBJECT_NAMES = {
    CLICK: {
        QUESTION_OPEN: "SuprCredits question-open",
        QUESTION_CLOSE: "SuprCredits question-close",
    },
    IMPRESSION: {
        FAQS_VIEWED: "suprcredits-faqs-viewed",
    },
};
