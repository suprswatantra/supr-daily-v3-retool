export const TEXTS = {
    HEADING: "What are Supr Credits?",
    SUB_HEADING:
        "Supr Credits is a currency you can use to buy all groceries except milk.",
    INFO_TEXT: "1 Supr Credit = 1 Rupee",
    CURRENT_BALANCE: "CURRENT BALANCE",
    DEFAULT_CITY_ID: "4",
    FAQ_PREFIX: "scFaq_",
    EXPIRY_SOON: "Supr Credits expiring ",
    EXPIRY_TEXT: "Supr Credits expiring on ",
    NOTE: "Note: ",
};

export const DEFAULT_FAQ_LIST = {
    desc:
        "Supr Credits are a type of rewards that can be used to purchase anything on Supr, except milk.",
    faqs: [
        {
            ques: "How can I use my available Supr Credits?",
            ans:
                "If you have Supr Credits balance, you can use it to purchase any grocery item on Supr other than milk during checkout. It will be applied automatically on the sum of grocery items in the cart.",
        },
        {
            ques:
                "Is there a minimum order value in order to use the Supr Credits?",
            ans:
                "There is no minimum order value and you may utilize your Supr Credits on purchase of any number of grocery items other than milk.",
        },
        {
            ques:
                "Can I decide how much Supr Credits balance to use for a particular order?",
            ans:
                "No. You can choose to either use or not use the Supr Credits. If applied during checkout, the balance deducted will be either your total Supr Credits balance or the cart value, whichever is lower.",
        },
        {
            ques: "Is there an expiry date for my Supr Credits?",
            ans:
                "Yes. Supr Credits expires 30 days from the date of credit. So, go ahead and use it for your grocery purchases! (all non-milk items)",
        },
        {
            ques:
                "Can I transfer my Supr Credits balance to my wallet or withdraw them to my bank account?",
            ans:
                "No, you can neither transfer Supr Credits to wallet balance, nor transfer the Supr Credits balance to your bank account. Supr Credits can only be used to purchase all grocery items (except milk) from Supr Daily during checkout.",
        },
        {
            ques:
                "What if I cancel an order where I used or earned Supr Credits?",
            ans:
                "If you cancel a transaction or receive a refund for a transaction for any reason, the used Supr Credits is refunded back to your Supr Credits balance. Also, the Supr Credits given as an offer on the transaction is deducted from the Supr Credits balance.",
        },
    ],
};
