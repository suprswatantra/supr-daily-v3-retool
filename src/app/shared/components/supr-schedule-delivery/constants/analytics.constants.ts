export const SA_OBJECT_NAMES = {
    CLICK: {
        NEED_HELP: "need-help",
        VIEW_PHOTO: "view-photo",
        DELIVERY_STATUS: "delivery-status",
    },
    IMPRESSION: {
        PAUSED_BY_SYSTEM: "delivery-paused-by-system",
    },
};
