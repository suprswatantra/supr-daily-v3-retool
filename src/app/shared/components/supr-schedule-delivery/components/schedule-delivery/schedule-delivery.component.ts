import {
    Input,
    OnInit,
    Output,
    Component,
    OnChanges,
    SimpleChange,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { DateService } from "@services/date/date.service";
import { UtilService } from "@services/util/util.service";
import { CartService } from "@services/shared/cart.service";
import { StoreService } from "@services/data/store.service";
import { RouterService } from "@services/util/router.service";
import { CalendarService } from "@services/date/calendar.service";
import { ScheduleService } from "@services/shared/schedule.service";
import { SettingsService } from "@services/shared/settings.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import {
    DeliveryTimelines,
    ScheduleDeliverySlotMap,
    ScheduleDeliverySlotItem,
} from "@models";

import { Segment } from "@types";

import { TEXTS } from "../../constants";
import { SA_OBJECT_NAMES } from "../../constants/analytics.constants";
import { SCHEDULE_DAYS } from "@pages/schedule/constants/schedule.constants";

@Component({
    selector: "supr-schedule-delivery",
    templateUrl: "./schedule-delivery.component.html",
    styleUrls: ["./schedule-delivery.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleDeliveryComponent implements OnInit, OnChanges {
    @Input() date: string;
    @Input() locked = false;
    @Input() paused = false;
    @Input() pausedMsg: string;
    @Input() statusText: string;
    @Input() noSchedule: boolean;
    @Input() supportAction = true;
    @Input() multiplePOD: boolean;
    @Input() pausedBySystem = false;
    @Input() subscriptionView = false;
    @Input() pausedBySystemMsg: string;
    @Input() noSchedulesMessage: string;
    @Input() pastOrderSkuList: Array<number>;
    @Input() schedule: ScheduleDeliverySlotMap;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    TEXTS = TEXTS;
    tpEnabled = false;
    previousDay: string;
    isTomorrow = false;
    scheduleItemMessage: string;
    slotTimelines: DeliveryTimelines;
    DELIVERY_SLOTS: Array<string> = [];

    saContextAddItem: string;
    saObjectNames = SA_OBJECT_NAMES;
    saContextList: Segment.ContextListItem[] = [];

    constructor(
        private dateService: DateService,
        private cartService: CartService,
        private utilService: UtilService,
        private storeService: StoreService,
        private routerService: RouterService,
        private scheduleService: ScheduleService,
        private settingsService: SettingsService,
        private calendarService: CalendarService,
        private blockAccessService: BlockAccessService
    ) {}

    ngOnInit() {
        this.init();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleDateChange(changes["date"]);
    }

    getClasses() {
        return {
            tomorrow: this.isTomorrow,
            noSchedules: this.noSchedule,
            systemVacation: !this.paused && this.pausedBySystem,
        };
    }

    goToSearchPage(scheduleDate: string) {
        /* Block schedule date in case of access restrictions */
        const isAccessRestricted = this.blockAccessService.isFutureDeliveryAccessRestricted();

        if (isAccessRestricted && !this.isTomorrow) {
            this.blockAccessService.showBlockAcessNudge();
            return;
        }

        this.cartService.setDeliveryDate(scheduleDate);
        this.routerService.goToSearchPage();
    }

    goToSupportPage() {
        this.routerService.goToSupportPage();
    }

    onScheduleDayHeaderActionClick() {
        if (!this.pausedBySystem && this.paused) {
            this.openPauseModal();
        }
    }

    getNestedData(slot: string, key: string) {
        return this.utilService.getNestedValue(
            this.schedule,
            `slot_info.${slot}.${key}`,
            ""
        );
    }

    getDayHeaderSubtitle(): string {
        if (this.pausedBySystem) {
            return this.pausedBySystemMsg;
        } else {
            if (this.paused) {
                return this.pausedMsg || TEXTS.PAUSED_TEXT;
            } else {
                return this.noSchedule
                    ? this.noSchedulesMessage || TEXTS.NOT_SCHEDULED_TEXT
                    : this.utilService.getNestedValue(
                          this.schedule,
                          "message",
                          this.scheduleItemMessage
                      );
            }
        }
    }

    getDayHeaderActionText(): string {
        if (this.pausedBySystem) {
            return TEXTS.SYSTEM_PAUSED_TEXT;
        }

        return this.paused ? TEXTS.ON_VACATION_TEXT : null;
    }

    getDayHeaderActionIcon(): string {
        if (this.pausedBySystem) {
            return "lock";
        }

        return this.paused ? "pause" : null;
    }

    getDayHeaderActionMeta(): { name: string; value: string } {
        if (this.pausedBySystem) {
            return {
                name: this.date,
                value: SA_OBJECT_NAMES.IMPRESSION.PAUSED_BY_SYSTEM,
            };
        }
    }

    private init() {
        this.setTpStatus();
        this.setDateInfo();
        this.fetchSettings();
        this.setAnalyticsInfo();
        this.setSlotTimelineData();
    }

    private openPauseModal() {
        this.storeService.togglePauseModal();
    }

    private setTpStatus() {
        this.tpEnabled = this.calendarService.isTpEnabled();
    }

    private handleDateChange(dateChanges: SimpleChange) {
        if (
            dateChanges &&
            !dateChanges.firstChange &&
            dateChanges.previousValue !== dateChanges.currentValue
        ) {
            this.setDateInfo();
        }
    }

    private fetchSettings() {
        this.DELIVERY_SLOTS = this.settingsService.getSettingsValue(
            "deliverySlotTypes"
        );
    }

    private setAnalyticsInfo() {
        this.saContextList = [
            {
                name: "schedule-date",
                value: this.date,
            },
        ];

        if (!this.DELIVERY_SLOTS) {
            return;
        }

        let allSlotsAvailable = true;
        this.DELIVERY_SLOTS.forEach((slot: string) => {
            if (allSlotsAvailable && this.schedule && this.schedule[slot]) {
                const slotData = this.schedule[
                    slot
                ] as ScheduleDeliverySlotItem;
                const hasAddons = !!(slotData.addons && slotData.addons.length);
                const hasSubs = !!(
                    slotData.subscriptions && slotData.subscriptions.length
                );
                const slotHasSchedule = hasAddons || hasSubs;

                allSlotsAvailable = allSlotsAvailable && (hasAddons || hasSubs);
                this.saContextAddItem = slotHasSchedule
                    ? slot
                    : this.saContextAddItem;
            } else {
                allSlotsAvailable = false;
            }
        });

        this.saContextAddItem = allSlotsAvailable
            ? "BOTH"
            : this.saContextAddItem;
    }

    private setDateInfo() {
        if (!this.schedule || !this.schedule.message) {
            let previousDay = this.scheduleService.getPreviousScheduleDay(
                this.date
            );

            previousDay =
                previousDay === SCHEDULE_DAYS.TODAY ||
                previousDay === SCHEDULE_DAYS.TOMORROW
                    ? previousDay.toLowerCase()
                    : previousDay;

            this.scheduleItemMessage = `${TEXTS.SCHEDULED_NOT_PAUSED_TEXT} ${previousDay}`;
        }

        this.setDayIsTomorrow();
    }

    private setDayIsTomorrow() {
        this.isTomorrow = this.dateService.isNthDayFromToday(this.date, 1);
    }

    private setSlotTimelineData() {
        const deliveryTimelines = this.utilService.getNestedValue(
            this.schedule,
            "delivery_timeline"
        );

        this.slotTimelines = this.scheduleService.getSlotTimelineData(
            deliveryTimelines
        );
    }
}
