import {
    Input,
    OnInit,
    Output,
    OnChanges,
    Component,
    EventEmitter,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { ScheduleAdapter as Adapter } from "@shared/adapters/schedule.adapter";

import { ScheduleDeliverySlotMap, Schedule } from "@models";

import { ScheduleService } from "@services/shared/schedule.service";

@Component({
    selector: "supr-schedule-delivery-container",
    template: `
        <supr-schedule-delivery
            [date]="date"
            [paused]="paused"
            [schedule]="schedule$ | async"
            [pausedMsg]="pausedMsg$ | async"
            [pausedBySystem]="pausedBySystem"
            [noSchedule]="noSchedule$ | async"
            [multiplePOD]="multiplePOD$ | async"
            [pastOrderSkuList]="pastOrderSkuList"
            [pausedBySystemMsg]="pausedBySystemMsg$ | async"
            [noSchedulesMessage]="noSchedulesMessage$ | async"
            (updateActionListener)="updateActionListener.emit($event)"
        ></supr-schedule-delivery>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprScheduleDeliveryContainer implements OnInit, OnChanges {
    @Input() date: string;
    @Input() paused: boolean;
    @Input() pausedBySystem: boolean;
    @Input() pastOrderSkuList: Array<number>;

    @Output() updateActionListener: EventEmitter<string> = new EventEmitter();

    pausedMsg$: Observable<string>;
    noSchedule$: Observable<boolean>;
    multiplePOD$: Observable<boolean>;
    pausedBySystemMsg$: Observable<string>;
    noSchedulesMessage$: Observable<string>;
    schedule$: Observable<ScheduleDeliverySlotMap>;

    constructor(
        private adapter: Adapter,
        private scheduleService: ScheduleService
    ) {}

    ngOnInit() {
        this.setScheduleData();
        this.setSystemPauseMessgae();
        this.multiplePOD$ = this.adapter.multiplePOD$;
        this.noSchedulesMessage$ = this.adapter.noSchedulesMessage$;
    }

    ngOnChanges(changes: SimpleChanges) {
        const dateChange = changes["date"];
        if (
            dateChange &&
            !dateChange.firstChange &&
            dateChange.currentValue !== dateChange.previousValue
        ) {
            this.setScheduleData();
        }
    }

    private setScheduleData() {
        if (this.date) {
            this.noSchedule$ = this.adapter
                .getScheduleByDate(this.date)
                .pipe(
                    map(
                        (schedule: Schedule) =>
                            !this.scheduleService.hasScheduledDeliveries(
                                schedule,
                                true
                            )
                    )
                );
            this.schedule$ = this.adapter.getScheduleDeliveryByDate(this.date);
        }
    }

    private setSystemPauseMessgae() {
        this.pausedMsg$ = this.adapter.pausedMsg$;
        this.pausedBySystemMsg$ = this.adapter.pausedBySystemMsg$;
    }
}
