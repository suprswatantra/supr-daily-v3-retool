import { Input, Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-section-header",
    template: `
        <div class="suprRow wrapper">
            <div class="suprColumn top stretch iconWrapper">
                <supr-icon [name]="icon"> </supr-icon>
            </div>
            <div class="suprColumn top stretch left textWrapper">
                <div class="suprRow titleWrapper">
                    <supr-text [type]="titleType">
                        {{ title }}
                    </supr-text>
                    <ng-container *ngIf="chevronDown || chevronUp">
                        <supr-icon
                            name="chevron_down"
                            class="chevronRight"
                            [class.up]="chevronUp"
                        >
                        </supr-icon>
                    </ng-container>
                    <ng-container *ngIf="chevronRight">
                        <supr-icon name="chevron_right" class="chevronRight">
                        </supr-icon>
                    </ng-container>
                </div>
                <ng-container *ngIf="subtitle">
                    <div class="divider2"></div>
                    <supr-text
                        [type]="subTitleType"
                        *ngIf="subtitle"
                        class="subtitle"
                    >
                        {{ subtitle }}
                    </supr-text>
                </ng-container>
                <ng-container *ngIf="footerText">
                    <div class="divider4"></div>
                    <supr-text
                        [type]="subTitleType"
                        *ngIf="subtitle"
                        class="subtitle"
                    >
                        {{ footerText }}
                    </supr-text>
                </ng-container>
            </div>
        </div>
    `,
    styleUrls: ["./supr-section-header-component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprSectionHeaderComponent {
    @Input() icon: string;
    @Input() title: string;
    @Input() subtitle: string;
    @Input() footerText: string;
    @Input() chevronUp = false;
    @Input() chevronDown = false;
    @Input() titleType = "subtitle";
    @Input() subTitleType = "caption";
    @Input() chevronRight = false;
}
