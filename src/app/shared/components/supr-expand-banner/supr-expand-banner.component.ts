import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
    selector: "supr-expand-banner",
    template: `
        <supr-ribbon [class.noLeftPadding]="isLeftPaddingZero">
            <div class="suprRow spaceBetween" (click)="toggle()">
                <supr-text>{{ title }}</supr-text>
                <div class="spacer20"></div>

                <supr-icon
                    name="chevron_up"
                    *ngIf="_showDetails && _isExpand"
                ></supr-icon>
                <supr-icon
                    name="chevron_down"
                    *ngIf="!_showDetails && _isExpand"
                ></supr-icon>
            </div>
            <div class="divider4" *ngIf="_showDetails"></div>
            <div class="suprRow spaceBetween" *ngIf="_showDetails">
                <supr-text class="subtitle">{{ subtitle }}</supr-text>
                <div class="spacer24"></div>
                <div class="spacer24"></div>
            </div>
        </supr-ribbon>
    `,
    styleUrls: ["./supr-expand-banner.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprExpandBanner {
    @Input() title: string;
    @Input() subtitle: string;
    @Input() isLeftPaddingZero: boolean = false;
    @Input()
    set isExpand(value: boolean) {
        this._showDetails = !value;
        this._isExpand = value;
    }

    get isExpand() {
        return this._showDetails;
    }

    _isExpand: boolean = true;
    _showDetails: boolean = true;

    constructor() {}

    toggle() {
        if (this.subtitle && this._isExpand) {
            this._showDetails = !this._showDetails;
        }
    }
}
