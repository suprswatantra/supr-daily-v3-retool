import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-checkbox-lineitem",
    template: `<supr-line-item-variant-A
        [text]="text"
        [textType]="textType"
        [imageLeft]="imageLeft"
        [imageRight]="imageRight"
        [iconOnTheLeft]="iconLeft"
        [iconOnTheRight]="_iconRight"
        (handleClick)="_handleClick()"
    ></supr-line-item-variant-A>`,
    styleUrls: ["./supr-checkbox-lineitem.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuprCheckboxLineItemComponent {
    @Input() text: string;
    @Input() checked = false;
    @Input() textType: string;
    @Input() iconLeft: string;
    @Input() imageLeft: string;
    @Input() imageRight: string;
    @Input() checkedIcon: string;
    @Input() set uncheckedIcon(icon: string) {
        this._iconRight = icon;
        this._uncheckedIcon = icon;
    }

    get uncheckedIcon(): string {
        return this._uncheckedIcon;
    }

    @Output() handleClick: EventEmitter<boolean> = new EventEmitter();

    _iconRight: string;

    private _uncheckedIcon: string;

    _handleClick() {
        this.checked = !this.checked;
        this._iconRight = this.checked ? this.checkedIcon : this.uncheckedIcon;

        this.handleClick.emit(this.checked);
    }
}
