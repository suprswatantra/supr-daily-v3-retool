import { UtilService } from "@services/util/util.service";
import {
    Input,
    OnInit,
    OnChanges,
    Component,
    SimpleChanges,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Collection } from "@models";

import { PAST_ORDER_COLLECTION_PROPS } from "./constants";

@Component({
    selector: "schedule-past-order-collection",
    template: `
        <div class="pastOrderCollectionWrapper" *ngIf="pastOrderCollection">
            <supr-collection-widget
                [collection]="pastOrderCollection"
            ></supr-collection-widget>
        </div>
    `,
    styleUrls: ["./schedule-past-order-collection.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SchedulePastOrderCollection implements OnInit, OnChanges {
    @Input() bgColor: string;
    @Input() showHeader = true;
    @Input() pastOrderSkuList: Array<number>;

    pastOrderCollection: Collection;

    constructor(private utilService: UtilService) {}

    ngOnInit() {
        this.generatePastOrderCollection();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["pastOrderSkuList"];
        if (
            change &&
            !change.firstChange &&
            change.currentValue !== change.previousValue
        ) {
            this.generatePastOrderCollection();
        }
    }

    private generatePastOrderCollection() {
        if (this.utilService.isLengthyArray(this.pastOrderSkuList)) {
            this.pastOrderCollection = {
                viewId: null,
                title: this.showHeader
                    ? {
                          text: PAST_ORDER_COLLECTION_PROPS.TITLE.TEXT,
                          color: PAST_ORDER_COLLECTION_PROPS.TITLE.COLOR,
                      }
                    : null,
                subtitle: null,
                background: {
                    image: null,
                    color: this.bgColor || PAST_ORDER_COLLECTION_PROPS.BG.COLOR,
                },
                type: PAST_ORDER_COLLECTION_PROPS.TYPE,
                items: this.getCollectionItems(),
                displayType: PAST_ORDER_COLLECTION_PROPS.DISPLAY_TYPE,
                pastOrder: true,
            };
        }
    }

    private getCollectionItems() {
        return this.pastOrderSkuList.map((skuId) => {
            return {
                image: null,
                entityId: skuId,
                borderColor: PAST_ORDER_COLLECTION_PROPS.ITEM.BORDER_COLOR,
                preferredMode: PAST_ORDER_COLLECTION_PROPS.ITEM.PREFERRED_MODE,
                tags: null,
            };
        });
    }
}
