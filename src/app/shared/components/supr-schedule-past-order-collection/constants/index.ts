export const PAST_ORDER_COLLECTION_PROPS = {
    TITLE: {
        TEXT: "Quick add for tomorrow",
        COLOR: "#999999",
    },
    BG: {
        COLOR: "#f9f9f9",
    },
    TYPE: "sku",
    DISPLAY_TYPE: "horizontal_list",
    ITEM: {
        BORDER_COLOR: "#F2F2F2",
        PREFERRED_MODE: "direct_add",
    },
};
