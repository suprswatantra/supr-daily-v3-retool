import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Category } from "@models";
import { UtilService } from "@services/util/util.service";
import { RouterService } from "@services/util/router.service";
import { TileGridData } from "@types";

const CATEGORY_GRID_COLUMNS = 3;

@Component({
    selector: "supr-category-grid-wrapper",
    template: `
        <supr-tile-grid
            [dataList]="gridData"
            [columnSize]="${CATEGORY_GRID_COLUMNS}"
            [colorBackground]="true"
            (handleTileClick)="goToCategoryPage($event)"
            [saObjectName]="saObjectName"
        >
        </supr-tile-grid>
    `,
    styleUrls: ["./../supr-category-grid.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridWrapperComponent {
    @Input()
    set categories(value: Category[]) {
        if (this.utilService.isEmpty(value)) {
            this.handleFetchCategoryList.emit();
        } else {
            this.setTileGridData(value);
        }
    }
    @Input() saObjectName: string;

    @Output() handleFetchCategoryList: EventEmitter<void> = new EventEmitter();

    gridData: TileGridData[] = [];

    constructor(
        private utilService: UtilService,
        private routerService: RouterService
    ) {}

    trackByFn(_: any, index: number): number {
        return index;
    }

    goToCategoryPage(category: Category) {
        this.routerService.goToCategoryPage(category.id);
    }

    private setTileGridData(categories: Category[]) {
        this.gridData = categories.map((category) => ({
            id: category.id,
            imgUrl: category.image.fullUrl,
            image: category.image,
            name: category.name,
            bgColor: category.image.bgColor,
        }));
    }
}
