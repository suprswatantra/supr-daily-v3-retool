import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";
import { Observable } from "rxjs";

import { Category } from "@models";
import { CategoryAdapter as Adapter } from "../../adapters/category.adapter";

@Component({
    selector: "supr-category-grid",
    template: `
        <supr-category-grid-wrapper
            [categories]="categoryList$ | async"
            (handleFetchCategoryList)="fetchCategoryList()"
            [saObjectName]="saObjectName"
        ></supr-category-grid-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryGridComponent implements OnInit {
    @Input() saObjectName: string;

    categoryList$: Observable<Category[]>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.categoryList$ = this.adapter.categoryList$;
    }

    fetchCategoryList() {
        this.adapter.fetchCategoryList();
    }
}
