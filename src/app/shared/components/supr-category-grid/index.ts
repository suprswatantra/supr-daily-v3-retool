import { GridWrapperComponent } from "./components/grid-wrapper.component";
import { CategoryGridComponent } from "./supr-category-grid.component";

export const categoryGridComponents = [
    CategoryGridComponent,
    GridWrapperComponent,
];
