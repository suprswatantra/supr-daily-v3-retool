import { DiscountBlockContainer } from "./supr-discount-block.container";
import { DiscountBlockLayout } from "./supr-discount-block.layout";
import { CouponAddAddressComponent } from "./supr-coupon-add-address-modal.component";

export const discountBlockComponents = [
    DiscountBlockContainer,
    DiscountBlockLayout,
    CouponAddAddressComponent,
];
