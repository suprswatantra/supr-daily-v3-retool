import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
    SimpleChanges,
    SimpleChange,
    OnInit,
    OnChanges,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";
import { CartItem, CartMeta, City } from "@shared/models";

import { UtilService } from "@services/util/util.service";
import { DateService } from "@services/date/date.service";
import { CartService } from "@services/shared/cart.service";

import { TEXTS } from "./constants";
import { TEXTS as CART_TEXTS } from "@pages/cart/constants";

@Component({
    selector: "supr-discount-block-layout",
    template: `
        <supr-section-header
            *ngIf="!fromRechargeSubscriptionPage"
            icon="${CART_TEXTS.PAYMENT_HEADER.icon}"
            title="${CART_TEXTS.PAYMENT_HEADER.title}"
            subtitle="${CART_TEXTS.PAYMENT_HEADER.subtitle}"
        >
        </supr-section-header>
        <div [class.discountWrapper]="!fromRechargeSubscriptionPage">
            <div class="discountContent">
                <ng-container *ngIf="showSuprCreditsBlock">
                    <supr-cart-credits
                        [city]="city"
                        [useSuprCredits]="useSuprCredits"
                        [suprCreditsBalance]="suprCreditsBalance"
                        [suprCreditsRemaining]="
                            cartMeta?.wallet_info?.updated_supr_credits_balance
                        "
                        [suprCreditsApplicable]="suprCreditsApplicable"
                        [suprCreditsAmout]="suprCreditsAmout"
                        [suprCreditsExpiryDateString]="
                            suprCreditsExpiryDateString
                        "
                        [expiry]="cartMeta?.wallet_info?.expiry"
                        (applySuprCredits)="onApplySuprCredits($event)"
                    ></supr-cart-credits>
                </ng-container>

                <supr-coupon
                    [couponCode]="couponCode"
                    [cartCurrentState]="cartCurrentState"
                    [suprCreditsApplicable]="suprCreditsApplicable"
                    [couponCodeMessage]="
                        cartMeta?.discount_info?.success_message
                    "
                    [tnc]="cartMeta?.discount_info?.tnc"
                    (applyCoupon)="onApplyCoupon($event)"
                    (removeCoupon)="onRemoveCoupon()"
                ></supr-coupon>
            </div>

            <div class="divider4"></div>

            <supr-text
                type="caption"
                *ngIf="showSuprCreditsBlock && suprCreditsApplicable"
                class="noteText"
            >
                ${TEXTS.NOTE}
            </supr-text>
        </div>

        <supr-modal
            *ngIf="showModal"
            modalName="${MODAL_NAMES.COUPON_ADD_ADDRESS}"
            (handleClose)="closeModal()"
        >
            <supr-coupon-add-address
                (handleAddAddress)="closeModal()"
            ></supr-coupon-add-address>
        </supr-modal>
    `,
    styleUrls: ["./styles/supr-discount-block.layout.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiscountBlockLayout implements OnInit, OnChanges {
    // For Coupon
    @Input() couponCode: CouponCode;
    @Input() cartCurrentState: CartCurrentState;
    @Input() hasAddress: boolean;
    @Input() fromRechargeSubscriptionPage: boolean;
    // For Supr Credits
    @Input() cartItems: CartItem[];
    @Input() suprCreditsBalance: number;
    @Input() cartModified: boolean;
    @Input() cartMeta: CartMeta;
    @Input() useSuprCredits: boolean;
    @Input() city: City;

    @Output() applySuprCredits: EventEmitter<boolean> = new EventEmitter();
    @Output() updateCoupon: EventEmitter<string> = new EventEmitter();

    showModal: boolean;
    suprCreditsAmout = 0;
    showSuprCreditsBlock: boolean;
    suprCreditsApplicable: boolean;
    suprCreditsExpiryDateString: string;

    constructor(
        private cdr: ChangeDetectorRef,
        private utilService: UtilService,
        private dateService: DateService,
        private cartService: CartService
    ) {}

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["cartMeta"];

        if (this.canHandleChange(change)) {
            this.initialize();
        }
    }

    onApplyCoupon(couponCode: string) {
        if (this.hasAddress) {
            this.updateCoupon.emit(couponCode);
        } else {
            this.openModal();
        }
    }

    async onRemoveCoupon() {
        const wasCouponAutoApplied = this.utilService.getNestedValue(
            this.cartMeta,
            "auto_applied_referral_coupon",
            null
        );

        if (!wasCouponAutoApplied) {
            this.updateCoupon.emit();
            return;
        }

        await this.cartService.removeReferralCouponFromCart();
        this.updateCoupon.emit();
    }

    async onApplySuprCredits(applySuprCredits: boolean) {
        const wasCouponAutoApplied = this.utilService.getNestedValue(
            this.cartMeta,
            "auto_applied_referral_coupon",
            null
        );

        if (!wasCouponAutoApplied) {
            this.applySuprCredits.emit(applySuprCredits);
            return;
        }

        await this.cartService.removeReferralCouponFromCart();
        this.applySuprCredits.emit(applySuprCredits);
    }

    closeModal() {
        this.showModal = false;
        this.cdr.detectChanges();
    }

    private openModal() {
        this.showModal = true;
        this.cdr.detectChanges();
    }

    private canHandleChange(change: SimpleChange): boolean {
        return (
            change &&
            !change.firstChange &&
            change.previousValue !== change.currentValue
        );
    }

    private initialize() {
        this.setSuprCreditsExpiryDateString();
        this.suprCreditsAmout = 0;

        if (this.isCartMetaDataAbsent()) {
            this.hideSuprCreditsBlock();
            return;
        }

        if (
            this.fromRechargeSubscriptionPage &&
            !this.isSuprCreditsAvailable()
        ) {
            this.hideSuprCreditsBlock();
            return;
        }

        const suprCreditsBalance = this.utilService.getNestedValue(
            this.cartMeta,
            "wallet_info.supr_credits_balance"
        );

        if (suprCreditsBalance) {
            this.showSuprCreditsBlock = true;
        } else {
            this.hideSuprCreditsBlock();
            return;
        }

        if (this.isSuprCreditsAvailable()) {
            this.suprCreditsApplicable = true;

            const suprCreditsUsed = this.utilService.getNestedValue(
                this.cartMeta,
                "payment_data.supr_credits_used"
            );
            const suprCreditsUsable = this.utilService.getNestedValue(
                this.cartMeta,
                "wallet_info.supr_credits_usable"
            );

            this.suprCreditsAmout = suprCreditsUsed || suprCreditsUsable;
            return;
        }

        this.suprCreditsApplicable = false;
    }

    private hideSuprCreditsBlock() {
        this.showSuprCreditsBlock = false;
        this.suprCreditsApplicable = false;
    }

    private setSuprCreditsExpiryDateString() {
        const expiry = this.utilService.getNestedValue(
            this.cartMeta,
            "wallet_info.expiry.supr_credits.date"
        );

        if (!expiry) {
            this.suprCreditsExpiryDateString = "";
            return;
        }

        const date = this.dateService.dateFromText(expiry);
        const daysDiff = this.dateService.daysFromToday(date);

        if (daysDiff === 1) {
            this.suprCreditsExpiryDateString = TEXTS.TOMORROW;
            return;
        } else if (daysDiff === 0) {
            this.suprCreditsExpiryDateString = TEXTS.TODAY;
            return;
        }

        this.suprCreditsExpiryDateString = "";
    }

    private isCartMetaDataAbsent(): boolean {
        return (
            !this.cartMeta ||
            !this.cartMeta.payment_data ||
            !this.cartMeta.wallet_info
        );
    }

    private isSuprCreditsAvailable(): boolean {
        const suprCreditsUsed = this.utilService.getNestedValue(
            this.cartMeta,
            "payment_data.supr_credits_used"
        );
        const suprCreditsUsable = this.utilService.getNestedValue(
            this.cartMeta,
            "wallet_info.supr_credits_usable"
        );

        if (suprCreditsUsed || suprCreditsUsable) {
            return true;
        }

        return false;
    }
}
