import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { ADDRESS_FROM_PARAM } from "@constants";

import { RouterService } from "@services/util/router.service";

import { TEXTS } from "@pages/cart/constants";
import { SA_OBJECT_NAMES } from "@pages/cart/constants/analytics.constants";

@Component({
    selector: "supr-coupon-add-address",
    template: `
        <div class="addAddress">
            <supr-text type="subtitle">
                ${TEXTS.ADD_ADDRESS_FOR_APPLY_COUPON}
            </supr-text>
            <div class="divider24"></div>
            <supr-text type="body" class="body">
                ${TEXTS.ADD_ADDRESS_TO_APPLY_COUPON_MESSAGE}
            </supr-text>
            <div class="divider24"></div>
            <supr-button
                saObjectName="${SA_OBJECT_NAMES.CLICK.ADD_ADDRESS}"
                (handleClick)="goToAddressPage()"
            >
                <supr-text type="body">${TEXTS.ADD_ADDRESS}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./styles/supr-coupon-add-address-modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponAddAddressComponent {
    @Output() handleAddAddress: EventEmitter<void> = new EventEmitter();

    constructor(private routerService: RouterService) {}

    goToAddressPage() {
        this.handleAddAddress.emit();

        this.routerService.goToAddressMapPage({
            queryParams: {
                from: ADDRESS_FROM_PARAM.CART,
            },
        });
    }
}
