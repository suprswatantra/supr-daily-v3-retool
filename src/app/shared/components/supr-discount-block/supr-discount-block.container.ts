import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
} from "@angular/core";

import { Observable } from "rxjs";

import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";
import { CartCurrentState } from "@store/cart/cart.state";
import { CouponCode } from "@types";
import { CartItem, CartMeta } from "@models";
import { City } from "@shared/models/address.model";

@Component({
    selector: "supr-discount-block-container",
    template: `
        <supr-discount-block-layout
            [couponCode]="couponCode$ | async"
            [cartCurrentState]="cartCurrentState$ | async"
            [hasAddress]="hasAddress$ | async"
            [cartItems]="cartItems$ | async"
            [cartMeta]="cartMeta$ | async"
            [city]="city$ | async"
            [useSuprCredits]="useSuprCredits$ | async"
            [suprCreditsBalance]="suprCreditsBalance$ | async"
            [fromRechargeSubscriptionPage]="fromRechargeSubscriptionPage"
            (applySuprCredits)="applyCredits($event)"
            (updateCoupon)="updateCoupon($event)"
        ></supr-discount-block-layout>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiscountBlockContainer implements OnInit {
    @Input() fromRechargeSubscriptionPage: boolean;

    couponCode$: Observable<CouponCode>;
    cartCurrentState$: Observable<CartCurrentState>;
    hasAddress$: Observable<boolean>;
    cartItems$: Observable<CartItem[]>;
    cartMeta$: Observable<CartMeta>;
    suprCreditsBalance$: Observable<number>;
    useSuprCredits$: Observable<boolean>;
    city$: Observable<City>;

    constructor(private adapter: Adapter) {}

    ngOnInit() {
        this.couponCode$ = this.adapter.cartCouponCode$;
        this.cartCurrentState$ = this.adapter.cartCurrentState$;
        this.hasAddress$ = this.adapter.hasAddress$;
        this.cartItems$ = this.adapter.cartItems$;
        this.cartMeta$ = this.adapter.cartMeta$;
        this.suprCreditsBalance$ = this.adapter.suprCreditsBalance$;
        this.useSuprCredits$ = this.adapter.useSuprCredits$;
        this.city$ = this.adapter.city$;
    }

    updateCoupon(couponCode?: string) {
        this.adapter.updateCouponInCart(couponCode);
    }

    applyCredits(useSuprCredits: boolean) {
        this.adapter.useSuprCreditsInCart(useSuprCredits);
    }
}
