import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-checkbox",
    template: `
        <div class="checkboxWrapper">
            <div class="round">
                <input
                    type="checkbox"
                    id="checkbox"
                    [name]="name"
                    [checked]="value"
                    [disabled]="disabled"
                    (change)="handleInputClick.emit($event.target.checked)"
                />
                <label for="checkbox"></label>
            </div>
        </div>
    `,
    styleUrls: ["./supr-checkbox.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxComponent {
    @Input() disabled = false;
    @Input() value: boolean;
    @Input() name: string;

    @Output() handleInputClick: EventEmitter<TouchEvent> = new EventEmitter();
}
