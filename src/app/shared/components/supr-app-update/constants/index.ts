export const TEXTS = {
    NEW_UPDATE_AVAILBLE: "New update is available!",
    UPDATE_NOW: "Update now",
    CRITICAL: {
        UPGRADE_TO_ENJOY:
            "Upgrade to the latest version to continue enjoying our app.",
    },
    OPTIONAL: {
        REMIND_ME_LATER: "Remind me later",
        DO_NOT_WANT_TO_UPDATE: "Don’t want to upgrade now?",
        UPGRADE_TO_LATEST_EXPERIENCE:
            "Upgrade to the latest version of our app for a better experience - bug fixes, improved performance & new features!",
    },
};

export const APP_UPDATE_TYPE = {
    OPTIONAL: "optional",
    CRITICAL: "critical",
};

export const OPTIONAL_UPDATE_DISPLAY_COOL_OFF_DAYS = 7;
