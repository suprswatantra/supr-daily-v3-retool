import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

import { STORAGE_DB_DATA_KEYS } from "@constants";

import { environment } from "@environments/environment";

import { ApiService } from "@services/data/api.service";
import { DateService } from "@services/date/date.service";
import { DbService } from "@services/data/db.service";
import { PlatformService } from "@services/util/platform.service";

import { SuprApi } from "@types";

import {
    APP_UPDATE_TYPE,
    OPTIONAL_UPDATE_DISPLAY_COOL_OFF_DAYS,
} from "./constants";
import { AppUpdateDismissInfo } from "./types";

@Component({
    selector: "supr-app-update",
    template: `
        <supr-app-update-modal
            *ngIf="showModal"
            [appUpdateType]="appUpdateType"
            (handleUpdateAppClick)="handleUpdateAppClick()"
            (handleModalDismiss)="handleModalDismiss()"
        >
        </supr-app-update-modal>
    `,
})
export class AppUpdateComponent implements OnInit, OnDestroy {
    private versionCheckSubscription: Subscription;

    showModal = false;
    appUpdateType = "";

    constructor(
        private apiService: ApiService,
        private dateService: DateService,
        private dbService: DbService,
        private platformService: PlatformService
    ) {}

    ngOnInit() {
        if (this.canCheck()) {
            this.checkVersion();
        }
    }

    ngOnDestroy() {
        this.unsubscribeVersionCheck();
    }

    handleModalDismiss() {
        if (this.isAppUpdateCritical()) {
            this.exitApp();
        } else {
            if (this.isAppUpdateOptional()) {
                this.handleOptionalAppUpdateDismiss();
            }
            this.closeModal();
        }
    }

    handleUpdateAppClick() {
        this.updateApp();

        this.exitApp();
    }

    private canCheck(): boolean {
        return true;
    }

    private checkVersion() {
        const versionCheckObj = this.getVersionApiBody();
        this.versionCheckSubscription = this.apiService
            .checkVersion(versionCheckObj, true)
            .subscribe((status: string) => this.handleAppUpdateStatus(status));
    }

    private unsubscribeVersionCheck() {
        if (
            this.versionCheckSubscription &&
            !this.versionCheckSubscription.closed
        ) {
            this.versionCheckSubscription.unsubscribe();
        }
    }

    private getVersionApiBody(): SuprApi.VersionCheckBody {
        if (this.platformService.isAndroid()) {
            return this.getAndroidVersionCheckBody();
        } else {
            return this.getIosVersionCheckBody();
        }
    }

    private getAndroidVersionCheckBody(): SuprApi.VersionCheckBody {
        const { android } = environment;

        return {
            platform: "android",
            version: android.check_version,
        };
    }

    private getIosVersionCheckBody(): SuprApi.VersionCheckBody {
        const { ios } = environment;

        return {
            platform: "ios",
            version: ios.check_version,
        };
    }

    private handleAppUpdateStatus(status: string) {
        switch (status) {
            case APP_UPDATE_TYPE.OPTIONAL:
                this.handleOptionalUpdate();
                return;
            case APP_UPDATE_TYPE.CRITICAL:
                this.handleCriticalUpdate();
                return;
        }
    }

    private handleOptionalUpdate() {
        this.showOptionalUpdate().then((show: boolean) => {
            if (show) {
                this.setAppUpdateType(APP_UPDATE_TYPE.OPTIONAL);
                this.openModal();
            }
        });
    }

    private handleCriticalUpdate() {
        this.setAppUpdateType(APP_UPDATE_TYPE.CRITICAL);
        this.openModal();
    }

    private setAppUpdateType(type: string) {
        this.appUpdateType = type;
    }

    private updateApp() {
        this.platformService.redirectToPlayStore();
    }

    private exitApp() {
        this.platformService.exitApp();
    }

    private openModal() {
        this.showModal = true;
    }

    private closeModal() {
        this.showModal = false;
    }

    private handleOptionalAppUpdateDismiss() {
        const { version } = environment;

        const infoToSave: AppUpdateDismissInfo = {
            optionalAppVersion: version,
            updatedAt: new Date(),
        };

        this.saveOptionalUpdateDismissData(infoToSave);
    }

    private saveOptionalUpdateDismissData(infoToSave: AppUpdateDismissInfo) {
        this.dbService.setData(
            STORAGE_DB_DATA_KEYS.APP_UPDATE_INFO,
            infoToSave
        );
    }

    private async getOptionalUpdateDismissData(): Promise<
        AppUpdateDismissInfo
    > {
        return this.dbService.getData(STORAGE_DB_DATA_KEYS.APP_UPDATE_INFO);
    }

    private async showOptionalUpdate(): Promise<boolean> {
        try {
            const updateDismissInfo = await this.getOptionalUpdateDismissData();

            return this.canOptionalUpdateModalBeShown(updateDismissInfo);
        } catch (error) {
            return false;
        }
    }

    private canOptionalUpdateModalBeShown(
        updateDismissInfo: AppUpdateDismissInfo = {}
    ): boolean {
        if (!updateDismissInfo) {
            return true;
        }

        const { optionalAppVersion, updatedAt } = updateDismissInfo;
        const { version } = environment;

        if (optionalAppVersion === version && updatedAt) {
            return !this.wasOptionalUpdateShownRecently(updatedAt);
        } else {
            return true;
        }
    }

    private wasOptionalUpdateShownRecently(updatedAt: string | Date): boolean {
        const daysDifferenceBetweenNowlAndLastDispalyed = this.dateService.daysFromToday(
            new Date(updatedAt)
        );
        return (
            daysDifferenceBetweenNowlAndLastDispalyed <
            OPTIONAL_UPDATE_DISPLAY_COOL_OFF_DAYS
        );
    }

    private isAppUpdateCritical(): boolean {
        return this.appUpdateType === APP_UPDATE_TYPE.CRITICAL;
    }

    private isAppUpdateOptional(): boolean {
        return this.appUpdateType === APP_UPDATE_TYPE.OPTIONAL;
    }
}
