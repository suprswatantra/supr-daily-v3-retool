import { AppUpdateComponent } from "./supr-app-update.component";
import { AppUpdateModalComponent } from "./components/update-modal.component";
import { OptionalComponent } from "./components/optional.component";
import { CriticalComponent } from "./components/critical.component";

export const appUpdateComponents = [
    AppUpdateComponent,
    AppUpdateModalComponent,
    OptionalComponent,
    CriticalComponent,
];
