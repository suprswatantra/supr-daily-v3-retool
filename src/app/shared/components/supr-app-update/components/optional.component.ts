import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { TEXTS } from "../constants";

@Component({
    selector: "supr-app-update-optional",
    template: `
        <div class="appUpdate">
            <div class="appUpdateHeader">
                <div class="appUpdateHeaderBg"></div>
                <div class="suprRow left appUpdateHeaderImg ">
                    <supr-svg></supr-svg>
                </div>
            </div>
            <div class="divider36"></div>

            <div class="appUpdateBody">
                <supr-text type="subtitle" class="subtitle">
                    {{ TEXTS.NEW_UPDATE_AVAILBLE }}
                </supr-text>
                <div class="divider16"></div>

                <supr-text type="body">
                    {{ TEXTS.OPTIONAL.UPGRADE_TO_LATEST_EXPERIENCE }}
                </supr-text>
                <div class="divider24"></div>

                <supr-text type="body">
                    {{ TEXTS.OPTIONAL.DO_NOT_WANT_TO_UPDATE }}
                </supr-text>

                <div
                    class="appUpdateBodyRemind suprRow"
                    (click)="handleModalDismiss.emit()"
                    saClick
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.REMIND_LATER}"
                >
                    <supr-text type="subtext">
                        {{ TEXTS.OPTIONAL.REMIND_ME_LATER }}
                    </supr-text>

                    <supr-icon name="chevron_right"></supr-icon>
                </div>

                <div class="divider24"></div>

                <supr-button
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.UPDATE_NOW}"
                    saObjectValue="optional"
                    (handleClick)="handleUpdateAppClick.emit()"
                >
                    <supr-text type="body">
                        {{ TEXTS.UPDATE_NOW }}
                    </supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["./../styles/supr-app-update.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionalComponent {
    @Output() handleUpdateAppClick: EventEmitter<void> = new EventEmitter();
    @Output() handleModalDismiss: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
}
