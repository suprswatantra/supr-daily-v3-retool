import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { TEXTS } from "../constants";

@Component({
    selector: "supr-app-update-critical",
    template: `
        <div class="appUpdate">
            <div class="appUpdateHeader">
                <div class="appUpdateHeaderBg critical"></div>
                <div class="suprRow center appUpdateHeaderImg">
                    <supr-svg></supr-svg>
                </div>
            </div>

            <div class="divider36"></div>

            <div class="appUpdateBody critical">
                <supr-text type="subtitle" class="subtitle">
                    {{ TEXTS.NEW_UPDATE_AVAILBLE }}
                </supr-text>

                <div class="divider16"></div>

                <supr-text type="body">
                    {{ TEXTS.CRITICAL.UPGRADE_TO_ENJOY }}
                </supr-text>

                <div class="divider24"></div>

                <supr-button
                    saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.UPDATE_NOW}"
                    saObjectValue="critical"
                    (handleClick)="handleUpdateAppClick.emit()"
                >
                    <supr-text type="body">
                        {{ TEXTS.UPDATE_NOW }}
                    </supr-text>
                </supr-button>
            </div>
        </div>
    `,
    styleUrls: ["./../styles/supr-app-update.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CriticalComponent {
    @Output() handleUpdateAppClick: EventEmitter<void> = new EventEmitter();

    TEXTS = TEXTS;
}
