import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from "@angular/core";

import { MODAL_NAMES } from "@constants";
import { APP_UPDATE_TYPE } from "../constants";

@Component({
    selector: "supr-app-update-modal",
    template: `
        <supr-modal
            (handleClose)="handleModalDismiss.emit()"
            [class.critical]="isCritical"
            modalName="${MODAL_NAMES.APP_UPDATE}"
            saObjectName="status"
            [saObjectValue]="saModalValue"
        >
            <ng-container *ngIf="isCritical; else optional">
                <supr-app-update-critical
                    (handleUpdateAppClick)="handleUpdateAppClick.emit()"
                ></supr-app-update-critical>
            </ng-container>

            <ng-template #optional>
                <supr-app-update-optional
                    (handleUpdateAppClick)="handleUpdateAppClick.emit()"
                    (handleModalDismiss)="handleModalDismiss.emit()"
                ></supr-app-update-optional>
            </ng-template>
        </supr-modal>
    `,
    styleUrls: ["./../styles/supr-app-update.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppUpdateModalComponent {
    @Input()
    set appUpdateType(appUpdateType: string) {
        this.isCritical = appUpdateType === APP_UPDATE_TYPE.CRITICAL;
        this.setModalValue();
    }
    @Output() handleUpdateAppClick: EventEmitter<void> = new EventEmitter();
    @Output() handleModalDismiss: EventEmitter<void> = new EventEmitter();

    isCritical = false;
    saModalValue = APP_UPDATE_TYPE.OPTIONAL;

    private setModalValue() {
        this.saModalValue = this.isCritical
            ? APP_UPDATE_TYPE.CRITICAL
            : APP_UPDATE_TYPE.OPTIONAL;
    }
}
