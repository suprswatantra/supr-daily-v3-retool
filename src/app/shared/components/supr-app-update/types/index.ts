export interface AppUpdateDismissInfo {
    optionalAppVersion?: string;
    updatedAt?: Date | string;
}
