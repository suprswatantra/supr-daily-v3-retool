import {
    Input,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import {
    CART_ITEM_TYPES,
    EVENT_TYPES,
    OOS_INFO_STATES,
    SKU_PREFERRED_MODE,
} from "@constants";

import { Sku, CartItem, CheckoutInfo, OutOfStockInfoStateType } from "@models";

import { UtilService } from "@services/util/util.service";
import { AuthService } from "@services/data/auth.service";
import { SkuService } from "@services/shared/sku.service";
import { CartService } from "@services/shared/cart.service";
import { RouterService } from "@services/util/router.service";
import { ModalService } from "@services/layout/modal.service";
import { QuantityService } from "@services/util/quantity.service";
import { OOSInfoService } from "@services/shared/oos-info.service";
import { SettingsService } from "@services/shared/settings.service";
import { MoengageService } from "@services/integration/moengage.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import { Segment, FetchSimilarProductsActionData } from "@types";

import { SA_OBJECT_NAMES } from "../constants/analytics.constants";

@Component({
    selector: "supr-subscribe-button-wrapper",
    template: `
        <supr-button
            class="primary"
            *ngIf="_showSubscriptionQty"
            saObjectName="${SA_OBJECT_NAMES.CLICK.EDIT_SUB}"
            [saContext]="saContext"
            [saObjectValue]="sku?.id"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
            (handleClick)="goToSubscriptionPage()"
        >
            <div class="suprRow center">
                <supr-text type="paragraph">{{ _qtyText }}</supr-text>
                <div class="spacer4"></div>
                <supr-icon name="edit"></supr-icon>
            </div>
        </supr-button>
        <supr-button
            *ngIf="_showSubscriptionBtn"
            saObjectName="${SA_OBJECT_NAMES.CLICK.CREATE_SUB}"
            [saContext]="saContext"
            [saObjectValue]="sku?.id"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
            (handleClick)="goToSubscriptionPage()"
            [class.primary]="isSubscriptionPreferred()"
        >
            <div class="suprRow center">
                <supr-icon name="repeat"></supr-icon>
                <div class="spacer4"></div>
                <supr-text class="label" type="subtext10">
                    SUBSCRIBE
                </supr-text>
            </div>
        </supr-button>
    `,
    styleUrls: ["../supr-subscribe-button.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WrapperComponent {
    @Input() sku: Sku;
    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() isAnonymousUser: boolean;
    @Input() userCheckoutInfo: CheckoutInfo;
    @Input() oosInfoState: OutOfStockInfoStateType;
    @Input() saContextList: Segment.ContextListItem[];
    @Input() set mode(type: string) {
        this._mode = type;
        this.setDisplayFlags();
    }
    get mode(): string {
        return this._mode;
    }
    @Input() set cartItem(item: CartItem) {
        this._cartItem = item;

        this.setDisplayFlags();
        this.setQuantityText();
    }
    get cartItem(): CartItem {
        return this._cartItem;
    }

    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();
    @Output() handleOosInfoUpdate: EventEmitter<void> = new EventEmitter();
    @Output() handleOosInfoUpdateDep: EventEmitter<void> = new EventEmitter();
    @Output() fetchSimilarProducts: EventEmitter<
        FetchSimilarProductsActionData
    > = new EventEmitter();

    _mode: string;
    _qtyText: string;
    _quantity: number;
    _showSubscriptionBtn: boolean;
    _showSubscriptionQty: boolean;

    private _cartItem: CartItem;

    constructor(
        private skuService: SkuService,
        private utilService: UtilService,
        private cartService: CartService,
        private authService: AuthService,
        private modalService: ModalService,
        private routerService: RouterService,
        private oosInfoService: OOSInfoService,
        private settingsService: SettingsService,
        private quantityService: QuantityService,
        private moengageService: MoengageService,
        private blockAccessService: BlockAccessService
    ) {}

    goToSubscriptionPage() {
        if (this.isAnonymousUser) {
            this.authService.logout();
            return;
        }
        if (this.cartService.isCheckoutDisabled(this.userCheckoutInfo)) {
            return;
        }

        if (this.sku) {
            const alternatesV2 = this.settingsService.getSettingsValue(
                "alternatesV2",
                true
            );

            if (alternatesV2) {
                return this.handleAlternatesV2Flow();
            }

            this.handleAlternatesV1Flow();
        }
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    private handleAlternatesV1Flow() {
        if (this.sku.oos_info && !this.sku.oos_info.navigate) {
            this.trackAnalytics();
            this.handleOosInfoUpdateDep.emit();
        } else {
            /* Re-route to alternate products view if product is oos */
            if (
                this.cartService.showAlternatesFlow(this.sku, "subscription") &&
                !this.routerService.isAlternatesScreen() &&
                this.utilService.isEmpty(this._cartItem)
            ) {
                return this.routerService.goToOOSAlternatesPage(this.sku.id, {
                    queryParams: {
                        intent: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
                    },
                });
            }

            if (this.sku && this._showSubscriptionBtn) {
                this.moengageService.trackInitiateSubscription({
                    sku_id: this.sku.id,
                    sku_price: this.sku.unit_price,
                    product_identifier: this.sku.product_identifier,
                });
            }

            /* Block subscription creation in case of access restrictions */
            if (
                this.sku.out_of_stock &&
                this.sku.next_available_date &&
                this.blockAccessService.isFutureDeliveryAccessRestricted()
            ) {
                this.blockAccessService.showBlockAcessNudge();
            } else {
                this.handlePostSubscribe.emit();
                this.routerService.goToSubscriptionCreatePage({
                    queryParams: {
                        skuId: this.sku.id,
                        from: "catalog",
                    },
                });
            }
        }
    }

    private handleAlternatesV2Flow() {
        const showAlternatesFlow = this.cartService.showAlternatesFlow(
            this.sku,
            "subscription"
        );
        const deliveryDate = this.cartService.getDeliveryDate();

        if (
            this.sku.out_of_stock &&
            /* Check if user is trying to add for a future date when the sku is available */
            !this.skuService.isSkuAvailableOnGivenDate(
                this.sku,
                deliveryDate
            ) &&
            (!this.oosInfoState || this.oosInfoState === OOS_INFO_STATES.ADD)
        ) {
            this.trackAnalytics();
            this.handleOosInfoUpdate.emit();
        }

        if (
            this.sku.oos_info &&
            showAlternatesFlow &&
            this.sku.oos_info.navigate &&
            this.utilService.isEmpty(this._cartItem) &&
            !this.routerService.isHomeScreen() /* don't fetch alternates for collections */
        ) {
            /* Close preview modal if open */
            if (this.modalService.isModalOpened()) {
                this.modalService.closeModal();
            }

            return this.fetchSimilarProducts.emit({
                skuId: this.sku.id,
                intent: CART_ITEM_TYPES.SUBSCRIPTION_NEW,
            });
        }

        if (this._showSubscriptionBtn) {
            this.moengageService.trackInitiateSubscription({
                sku_id: this.sku.id,
                sku_price: this.sku.unit_price,
                product_identifier: this.sku.product_identifier,
            });
        }

        /* Block subscription creation in case of access restrictions */
        if (
            this.sku.out_of_stock &&
            this.sku.next_available_date &&
            this.blockAccessService.isFutureDeliveryAccessRestricted()
        ) {
            this.blockAccessService.showBlockAcessNudge();
        } else {
            this.handlePostSubscribe.emit();
            this.routerService.goToSubscriptionCreatePage({
                queryParams: {
                    skuId: this.sku.id,
                    from: "catalog",
                },
            });
        }
    }

    private setDisplayFlags() {
        this._showSubscriptionQty = this.isSubscriptionItem();
        this._showSubscriptionBtn =
            (this.isSubscriptionPreferred() ||
                !this.isDirectAddOnPreferred()) &&
            !this._cartItem;
    }

    private isSubscriptionItem(): boolean {
        if (this.utilService.isEmpty(this.cartItem)) {
            return false;
        }

        return (
            this.cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_NEW ||
            this.cartItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
        );
    }

    private isSubscriptionPreferred(): boolean {
        return (
            this._mode === SKU_PREFERRED_MODE.SUBSCRIPTION ||
            this._mode === SKU_PREFERRED_MODE.DIRECT_SUBSCRIBE
        );
    }

    private isDirectAddOnPreferred(): boolean {
        return this._mode === SKU_PREFERRED_MODE.DIRECT_ADD;
    }

    private setQuantityText() {
        this._quantity = this.getCurrentSkuQty();

        if (this.sku) {
            this._qtyText = this.quantityService.toText(
                this.sku,
                this._quantity
            );
        }
    }

    private getCurrentSkuQty(): number {
        return (this._cartItem && this._cartItem.quantity) || 0;
    }

    private trackAnalytics() {
        this.oosInfoService.trackAnalytics(
            this.sku,
            this.cartItem,
            1,
            EVENT_TYPES.CLICK_EVENT
        );
    }
}
