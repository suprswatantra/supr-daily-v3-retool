import {
    Input,
    OnInit,
    Output,
    Component,
    EventEmitter,
    ChangeDetectionStrategy,
} from "@angular/core";

import { Observable } from "rxjs";

import { SKU_PREFERRED_MODE } from "@constants";

import { Sku, CartItem, CheckoutInfo, OutOfStockInfoStateType } from "@models";

import { SkuAdapter } from "@shared/adapters/sku.adapter";
import { UserAdapter } from "@shared/adapters/user.adapter";
import { CartAdapter as Adapter } from "@shared/adapters/cart.adapter";

import { Segment, FetchSimilarProductsActionData } from "@types";

@Component({
    selector: "supr-subscribe-button",
    template: `
        <supr-subscribe-button-wrapper
            [sku]="sku"
            [mode]="mode"
            [cartItem]="cartItem$ | async"
            [oosInfoState]="oosInfoState$ | async"
            [userCheckoutInfo]="userCheckoutInfo$ | async"
            [saContext]="saContext"
            [saPosition]="saPosition"
            [saContextList]="saContextList"
            [isAnonymousUser]="isAnonymousUser$ | async"
            (handleOosInfoUpdate)="oosInfoUpdate()"
            (handleOosInfoUpdateDep)="oosInfoUpdateDep()"
            (handlePostSubscribe)="handlePostSubscribe.emit()"
            (fetchSimilarProducts)="fetchSimilarProducts($event)"
        >
        </supr-subscribe-button-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscribeButtonComponent implements OnInit {
    @Input() sku: Sku;
    @Input() mode: string;
    @Input() isOosUpfront: boolean;

    @Input() saContext: any;
    @Input() saPosition: number;
    @Input() set saContextList(list: Segment.ContextListItem[]) {
        this._saContextList = [
            ...(list || []),
            {
                name: "preferred",
                value: this.mode === SKU_PREFERRED_MODE.SUBSCRIPTION,
            },
        ];
    }
    get saContextList(): Segment.ContextListItem[] {
        return this._saContextList;
    }

    @Output() handlePostSubscribe: EventEmitter<void> = new EventEmitter();

    cartItem$: Observable<CartItem>;
    isAnonymousUser$: Observable<boolean>;
    userCheckoutInfo$: Observable<CheckoutInfo>;
    oosInfoState$: Observable<OutOfStockInfoStateType>;

    private _saContextList: Segment.ContextListItem[];

    constructor(
        private adapter: Adapter,
        private userAdapter: UserAdapter,
        private skuAdapter: SkuAdapter
    ) {}

    ngOnInit() {
        if (!this.sku) {
            return;
        }

        this.isAnonymousUser$ = this.userAdapter.isAnonymousUser$;
        this.cartItem$ = this.adapter.getCartItemById(this.sku.id);
        this.userCheckoutInfo$ = this.userAdapter.userCheckoutInfo$;
        this.oosInfoState$ = this.skuAdapter.isNotified(this.sku.id);
    }

    oosInfoUpdate() {
        this.skuAdapter.updateSkuOOSInfo(this.sku.id, "oos");
    }

    /* [TODO_RITESH] Deprecate this method once alternates experiment is resolved */
    oosInfoUpdateDep() {
        this.skuAdapter.updateSkuOosInfo_deprecate(this.sku.id, "oos", true);
    }

    fetchSimilarProducts(data: FetchSimilarProductsActionData) {
        this.skuAdapter.fetchSimilarProducts(data);
    }
}
