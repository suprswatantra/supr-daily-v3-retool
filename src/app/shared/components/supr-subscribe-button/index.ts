import { WrapperComponent } from "./components/wrapper.component";
import { SubscribeButtonComponent } from "./supr-subscribe-button.component";

export const subscribeButtonComponents = [
    WrapperComponent,
    SubscribeButtonComponent,
];
