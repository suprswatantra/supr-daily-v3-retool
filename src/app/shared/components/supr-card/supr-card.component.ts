import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: "supr-card",
    template: `
        <div class="card">
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-card.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {}
