import {
    Component,
    ChangeDetectionStrategy,
    Input,
    OnInit,
} from "@angular/core";

@Component({
    selector: "supr-product-tile-price",
    template: `
        <div class="suprRow">
            <supr-text type="body" class="price">
                {{ unitPrice | rupee: 2 }}
            </supr-text>

            <ng-container *ngIf="showPriceStrike">
                <div class="spacer8"></div>
                <supr-text class="priceStrike" type="caption">
                    {{ unitMrp | rupee: 2 }}
                </supr-text>
            </ng-container>
        </div>
    `,
    styleUrls: ["./supr-product-tile-price.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTilePrice implements OnInit {
    @Input() unitPrice: number;
    @Input() unitMrp: number;

    showPriceStrike = false;

    ngOnInit() {
        this.setPrice();
    }

    private setPrice() {
        this.showPriceStrike = this.unitPrice < this.unitMrp;
    }
}
