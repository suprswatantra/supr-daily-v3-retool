import {
    Component,
    Input,
    OnInit,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
} from "@angular/core";

@Component({
    selector: "supr-tag",
    template: `
        <div class="tag" #tag>
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ["./supr-tag.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagComponent implements OnInit {
    @Input() bgColor: string;

    @ViewChild("tag", { static: true }) tagEl: ElementRef;

    ngOnInit() {
        this.setBgColor();
    }

    private setBgColor() {
        if (!this.bgColor) {
            return;
        }

        this.tagEl.nativeElement.style.setProperty(
            "--supr-tag-bg-color",
            this.bgColor
        );
    }
}
