import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    ChangeDetectorRef,
} from "@angular/core";

import { DateService } from "@services/date/date.service";
import { ToastService } from "@services/layout/toast.service";
import { ModalService } from "@services/layout/modal.service";
import { MoengageService } from "@services/integration/moengage.service";
import { NudgeService } from "@services/layout/nudge.service";
import { RouterService } from "@services/util/router.service";

import { VacationCurrentState } from "@store/vacation/vacation.state";

import { RATING_NUDGE_TRIGGERS, TOAST_MESSAGES, MODAL_NAMES } from "@constants";
import { Vacation } from "@models";

@Component({
    selector: "supr-pause-wrapper",
    template: `
        <div class="pause">
            <supr-modal
                *ngIf="_showModal"
                modalName="${MODAL_NAMES.PAUSE_ORDER}"
                (handleClose)="closeModal()"
            >
                <supr-edit-pause
                    *ngIf="activeView === 'edit-pause'"
                    [vacation]="_vacation"
                    (handleEditVacation)="showEditDatesView()"
                    (handleEndVacation)="showResumeConfirm()"
                >
                </supr-edit-pause>

                <supr-confirm-resume
                    (handleResumePause)="resumePause()"
                    (handleBack)="hideResumeConfirm()"
                    *ngIf="activeView === 'confirm-resume-deliveries'"
                ></supr-confirm-resume>
            </supr-modal>
        </div>
    `,
    styleUrls: ["./../styles/supr-pause.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PauseWrapperComponent implements OnInit, OnChanges {
    @Input() vacation: Vacation;
    @Input() vacationState: VacationCurrentState;
    @Input() vacationError: any;
    @Input() showModal: boolean;
    @Output() handleCloseModal: EventEmitter<void> = new EventEmitter();
    @Output() handleResumePause: EventEmitter<void> = new EventEmitter();

    activeView = "select-dates";
    _vacation: Vacation;
    _showModal = false;

    constructor(
        private toastService: ToastService,
        private modalService: ModalService,
        private cdr: ChangeDetectorRef,
        private dateService: DateService,
        private moengageService: MoengageService,
        private nudgeService: NudgeService,
        private routerService: RouterService
    ) {}

    ngOnInit() {
        this._vacation = this.vacation;
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleVacationStateChange(changes["vacationState"]);
        this.handleShowModalChange(changes["showModal"]);
    }

    closeModal() {
        this.resetPause();
        this.handleCloseModal.emit();
    }

    resumePause() {
        this.handleResumePause.emit();
    }

    hideResumeConfirm() {
        this.showEditPauseView();
    }

    setActiveView() {
        if (this._vacation) {
            this._showModal = true;
            this.showEditPauseView();
        } else {
            this.showEditDatesView();
        }
    }

    showResumeConfirm() {
        this.activeView = "confirm-resume-deliveries";
    }

    showEditDatesView() {
        this._showModal = false;
        this.handleCloseModal.emit();
        this.routerService.goToVacationPage();
    }

    showEditPauseView() {
        this.activeView = "edit-pause";
    }

    private handleVacationStateChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }
        const wasCancelling =
            change.previousValue === VacationCurrentState.CANCELLING_VACATION;

        if (
            wasCancelling &&
            change.currentValue === VacationCurrentState.NO_ACTION
        ) {
            this.modalService.closeModal();

            if (!this.vacationError) {
                this.showCancelSuccessMessage();
            }
        }
    }

    private handleShowModalChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return;
        }

        if (change.previousValue === false && change.currentValue === true) {
            this._vacation = this.vacation;
            this.setActiveView();
        }
    }

    private showCancelSuccessMessage() {
        const startDate = this.getFormatDate(this._vacation.start_date);
        const endDate = this.getFormatDate(this._vacation.end_date);

        const message = this.getToastMessage(
            TOAST_MESSAGES.VACATION.CANCEL,
            startDate,
            endDate
        );

        this.sendCancelSuccessAnalyticsEvents();
        this.toastService.present(message);

        this.sendRatingNudge();
    }

    private sendRatingNudge() {
        this.nudgeService.sendRatingNudge(
            RATING_NUDGE_TRIGGERS.CANCEL_VACATION
        );
    }

    private sendCancelSuccessAnalyticsEvents() {
        this.moengageService.trackVacationUpdate({
            start_date: this._vacation.start_date,
            end_date: this._vacation.end_date,
            status: "inactive",
        });
    }

    private getToastMessage(messagePrefix, startDate, endDate) {
        const isVacationOneDay = startDate === endDate;

        return `${messagePrefix} ${
            !isVacationOneDay ? "from" : "for"
        } ${startDate} ${!isVacationOneDay ? "to " + endDate : ""}`;
    }

    private resetPause() {
        this._vacation = null;
        this._showModal = false;
        this.cdr.detectChanges();
    }

    private getFormatDate(date: string): string {
        return this.dateService.formatDate(date, "d LLL yyy");
    }
}
