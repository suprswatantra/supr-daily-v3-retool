import {
    Component,
    Input,
    ChangeDetectionStrategy,
    OnInit,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from "@angular/core";

import { DateService } from "@services/date/date.service";
import { Calendar } from "@types";

import { TEXTS } from "../../constants/supr-pause.constants";

@Component({
    selector: "supr-pause-calendar-header",
    template: `
        <div class="header suprRow spaceBetween">
            <div class="suprColumn left">
                <supr-text type="subheading">{{ startDateDay }}</supr-text>
                <supr-text type="subheading">{{ startDateStr }}</supr-text>
            </div>
            <div class="suprColumn center">
                <div class="centerWrapper">
                    <supr-chip>
                        <supr-text type="subtext10">
                            {{ noOfDaysStr }}
                        </supr-text>
                    </supr-chip>
                </div>
                <div class="centerLine"></div>
            </div>
            <div class="suprColumn right">
                <supr-text type="subheading">{{ endDateDay }}</supr-text>
                <supr-text type="subheading">{{ endDateStr }}</supr-text>
            </div>
        </div>
    `,
    styleUrls: ["./../../styles/pause-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PauseCalendarHeaderComponent implements OnInit, OnChanges {
    @Input() startDate: Calendar.DayData;
    @Input() endDate: Calendar.DayData;
    @Input() noOfDaysStr: string;

    readonly TEXTS = TEXTS;

    startDateStr: string;
    startDateDay: string;
    endDateStr: string;
    endDateDay: string;

    constructor(private dateService: DateService) {}

    ngOnInit() {
        this.setDates();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleStartDateChange(changes["startDate"]);
        this.handleEndDateChange(changes["endDate"]);
    }

    private setDates() {
        this.setStartDate();
        this.setEndDate();
    }

    private setStartDate() {
        if (!this.startDate) {
            this.startDateStr = "--";
            this.startDateDay = "--";
        } else {
            const date = this.startDate.dateText;
            this.startDateStr = this.dateService.formatDate(date, "dd LLL");
            this.startDateDay = this.dateService.formatDate(date, "EEEE");
        }
    }

    private setEndDate() {
        const endDate = this.endDate || this.startDate;
        if (!endDate) {
            this.endDateStr = "--";
            this.endDateDay = "--";
        } else {
            const date = endDate.dateText;
            this.endDateStr = this.dateService.formatDate(date, "dd LLL");
            this.endDateDay = this.dateService.formatDate(date, "EEEE");
        }
    }

    private handleStartDateChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setStartDate();
        }
    }

    private handleEndDateChange(change: SimpleChange) {
        if (this.canHandleChange(change)) {
            this.setEndDate();
        }
    }

    private canHandleChange(change: SimpleChange) {
        if (!change || change.firstChange) {
            return false;
        }

        const prevValue = (change.previousValue || {}) as Calendar.DayData;
        const curValue = (change.currentValue || {}) as Calendar.DayData;

        return prevValue.time !== curValue.time;
    }
}
