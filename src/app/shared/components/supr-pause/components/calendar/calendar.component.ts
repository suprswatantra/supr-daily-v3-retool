import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
    OnInit,
    ChangeDetectorRef,
} from "@angular/core";

import { Vacation } from "@models";

import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";
import { SuprDateService } from "@services/date/supr-date.service";

import { ANALYTICS_OBJECT_NAMES } from "@constants";
import { TEXTS } from "../../constants/supr-pause.constants";

import { Calendar } from "@types";

@Component({
    selector: "supr-pause-calendar",
    template: `
        <div class="calendar">
            <supr-pause-calendar-header
                [startDate]="startDate"
                [endDate]="endDate"
                [noOfDaysStr]="noOfDaysStr"
            ></supr-pause-calendar-header>
            <div class="divider16"></div>
            <supr-calendar
                [startDate]="startDate"
                [endDate]="endDate"
                [disablePastDays]="true"
                [saObjectNameDateClick]="saObjectNameDateClick"
                monthChangeSaName="${ANALYTICS_OBJECT_NAMES.CLICK
                    .PAUSE_MONTH_CHANGE}"
                (handleSelectedDate)="updateDate($event)"
            ></supr-calendar>
            <div class="divider24"></div>
            <supr-button
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.PAUSE_BUTTON}"
                (handleClick)="updateVacation()"
            >
                <supr-text type="body">
                    {{
                        hasVacation
                            ? TEXTS.UPDATE_VACATION
                            : TEXTS.PAUSE_ORDERS_FOR + noOfDaysStr
                    }}
                </supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./../../styles/pause-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PauseCalendarComponent implements OnInit {
    @Input() vacation: Vacation;
    @Input() hasVacation: boolean;
    @Output() handleConfirm: EventEmitter<Vacation> = new EventEmitter();

    readonly TEXTS = TEXTS;

    startDate: Calendar.DayData;
    endDate: Calendar.DayData;
    noOfDaysStr: string;
    saObjectNameDateClick = ANALYTICS_OBJECT_NAMES.CLICK.PAUSE_START_DATE;

    private isFirstTap = true;

    constructor(
        private dateService: DateService,
        private calendarService: CalendarService,
        private suprDateService: SuprDateService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.initData();
    }

    updateDate(selectedDate: Calendar.DayData) {
        // Set the start or end dates
        if (selectedDate.time < this.startDate.time || this.isFirstTap) {
            this.startDate = selectedDate;
            this.endDate = selectedDate;
            this.isFirstTap = false;
            this.saObjectNameDateClick =
                ANALYTICS_OBJECT_NAMES.CLICK.PAUSE_END_DATE;
        } else {
            this.endDate = selectedDate;
            this.isFirstTap = true;
            this.saObjectNameDateClick =
                ANALYTICS_OBJECT_NAMES.CLICK.PAUSE_START_DATE;
        }

        this.setNoOfDays();
        this.cdr.detectChanges();
    }

    updateVacation() {
        const vacation = {
            start_date: this.startDate.dateText,
            end_date: this.endDate.dateText,
        };

        this.handleConfirm.emit(vacation);
    }

    private initData() {
        if (this.vacation) {
            const { start_date, end_date } = this.vacation;

            this.startDate = this.getSuprDate(start_date);
            this.endDate = this.getSuprDate(end_date) || this.startDate;
        } else {
            const nextAvblDate = this.calendarService.getNextAvailableDate();
            this.startDate = this.suprDateService.suprDate(nextAvblDate);
            this.endDate = this.startDate;
        }

        // Set initial no of days string
        this.setNoOfDays();
    }

    private getSuprDate(dateText: string): Calendar.DayData {
        return this.suprDateService.suprDateFromText(dateText);
    }

    private setNoOfDays() {
        const startDate = this.getDateFromText(this.startDate);
        const endDate = this.getDateFromText(this.endDate);

        const dayCount =
            this.dateService.daysBetweenTwoDates(startDate, endDate) + 1;

        this.noOfDaysStr = `${dayCount} day${dayCount > 1 ? "s" : ""}`;
    }

    private getDateFromText(date: Calendar.DayData): Date {
        return this.dateService.dateFromText(date.dateText);
    }
}
