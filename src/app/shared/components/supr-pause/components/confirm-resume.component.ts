import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { ANALYTICS_OBJECT_NAMES } from "@constants";

import { TEXTS } from "../constants/supr-pause.constants";
import { SA_OBJECT_NAMES } from "@pages/schedule/constants/analytics.constants";

@Component({
    selector: "supr-confirm-resume",
    template: `
        <div
            class="confirm resume container"
            saImpression
            saObjectName="${SA_OBJECT_NAMES.IMPRESSION.CONFIRM_PAUSE}"
        >
            <div class="suprRow">
                <supr-icon
                    name="chevron_left"
                    (click)="handleBack.emit()"
                ></supr-icon>
                <div class="spacer4"></div>
                <supr-text type="subtitle">
                    {{ TEXTS.CONFIRM_RESUME_DELIVERIES }}
                </supr-text>
            </div>
            <div class="divider4"></div>

            <supr-text class="subtitle" type="body">
                {{ TEXTS.RESUME_DELIVERIES_SUBTEXT }}
            </supr-text>

            <div class="divider36"></div>
            <supr-button
                saObjectName="${ANALYTICS_OBJECT_NAMES.CLICK.UNPAUSE_CONFIRM}"
                [loading]="loading"
                (handleClick)="resumePause()"
            >
                <supr-text type="body">{{ TEXTS.CONFIRM_RESUME }}</supr-text>
            </supr-button>
        </div>
    `,
    styleUrls: ["./../styles/supr-pause.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmResumeComponent {
    @Output() handleResumePause: EventEmitter<void> = new EventEmitter();
    @Output() handleBack: EventEmitter<void> = new EventEmitter();

    readonly TEXTS = TEXTS;

    loading = false;

    resumePause() {
        this.loading = true;
        this.handleResumePause.emit();
    }
}
