import {
    Component,
    Input,
    ChangeDetectionStrategy,
    Output,
    EventEmitter,
} from "@angular/core";

import { TEXTS } from "../constants/supr-pause.constants";

import { Vacation } from "@models";

@Component({
    selector: "supr-edit-pause",
    template: `
        <div class="edit container">
            <supr-text type="subtitle" class="title">
                ${TEXTS.VACATION_ACTIVE_TITLE}
            </supr-text>
            <div class="divider8"></div>
            <supr-text type="body" class="subtitle">
                ${TEXTS.VACATION_ACTIVE_SUBTITLE}
            </supr-text>
            <div class="divider24"></div>
            <div class="divider12"></div>
            <div class="suprRow">
                <div
                    class="vacationStart suprColumn left"
                    *ngIf="vacation?.start_date"
                >
                    <supr-text class="date" type="subtitle">{{
                        vacation.start_date | date: "d LLL, yyy"
                    }}</supr-text>
                    <supr-text class="type" type="body">Start date</supr-text>
                </div>
                <div
                    class="vacationEnd suprColumn left"
                    *ngIf="vacation?.end_date"
                >
                    <supr-text class="date" type="subtitle">{{
                        vacation.end_date | date: "d LLL, yyy"
                    }}</supr-text>
                    <supr-text class="type" type="body">End date</supr-text>
                </div>
            </div>
            <div class="divider8"></div>
            <div
                class="suprRow editVacation"
                (click)="handleEditVacation.emit()"
            >
                <supr-text type="subtext10">${TEXTS.EDIT_VACATION}</supr-text>
                <supr-icon name="chevron_right"></supr-icon>
            </div>
            <div class="divider24"></div>
            <supr-button
                class="endVacation"
                (handleClick)="handleEndVacation.emit()"
            >
                ${TEXTS.END_VACATION}
            </supr-button>
        </div>
    `,
    styleUrls: ["./../styles/supr-edit-pause.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditPauseComponent {
    @Input() vacation: Vacation;

    @Output() handleEndVacation: EventEmitter<Vacation> = new EventEmitter();
    @Output() handleEditVacation: EventEmitter<Vacation> = new EventEmitter();
}
