import { PauseComponent } from "./supr-pause.component";
import { PauseWrapperComponent } from "./components/wrapper.component";

import { EditPauseComponent } from "./components/edit-pause.component";
import { ConfirmResumeComponent } from "./components/confirm-resume.component";

import { PauseCalendarComponent } from "./components/calendar/calendar.component";
import { PauseCalendarHeaderComponent } from "./components/calendar/header.component";

export const pauseComponents = [
    PauseComponent,
    PauseWrapperComponent,

    EditPauseComponent,
    ConfirmResumeComponent,

    PauseCalendarComponent,
    PauseCalendarHeaderComponent,
];
