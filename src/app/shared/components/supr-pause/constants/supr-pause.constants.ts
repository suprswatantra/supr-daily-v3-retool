export const TEXTS = {
    CREATE_TITLE: "Set vacation dates",
    CREATE_SUBTITLE:
        "Select start and end date to suspend all deliveries during your vacation.",
    EDIT_TITLE: "Edit vacation dates",
    EDIT_SUBTITLE:
        "Select start and end date to suspend all deliveries during your vacation",
    UPDATE_VACATION: "Update vacation",
    VACATION_ACTIVE_TITLE: "Vacation active",
    VACATION_ACTIVE_SUBTITLE:
        "Your deliveries have been paused for below date range",
    EDIT_VACATION: "EDIT VACATION DATES",
    END_VACATION: "End vacation",
    BEFORE_CONFIRM_NOTE: "Vacation date: ",
    PAUSE_ORDERS_FOR: "Set vacation for ",
    CONFIRM_PAUSE_DELIVERIES: "Confirm to pause deliveries",
    CONFIRM_PAUSE_TITLE: "Confirm vacation",
    CONFIRM_PAUSE: "Confirm vacation",
    CONFIRM_RESUME_DELIVERIES: "Confirm to end vacation",
    RESUME_DELIVERIES_SUBTEXT: "Your subscription orders will be resumed.",
    CONFIRM_RESUME: "End vacation",
    TNC: {
        LINE1: "All subscription orders will be rescheduled to later dates.",
        LINE2:
            "All deliver once orders will be cancelled and refunded to Supr wallet.",
    },
    PAUSE_SET_SUCCESS: "Vacation dates set successfully.",
    PAUSE_UNSET_SUCCESS: "Resumed your deliveries successfully",
};
