import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";

import { Observable } from "rxjs";

import { Vacation } from "@models";
import { StoreService } from "@services/data/store.service";
import { VacationCurrentState } from "@store/vacation/vacation.state";

import { PauseAdapter as Adapter } from "@shared/adapters/pause.adapter";

@Component({
    selector: "supr-pause",
    template: `
        <supr-pause-wrapper
            [vacation]="vacation$ | async"
            [vacationState]="vacationState$ | async"
            [vacationError]="vacationError$ | async"
            [showModal]="modalOpen$ | async"
            (handleCloseModal)="toggleModal()"
            (handleResumePause)="resumePause()"
        ></supr-pause-wrapper>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PauseComponent implements OnInit {
    vacation$: Observable<Vacation>;
    vacationState$: Observable<VacationCurrentState>;
    vacationError$: Observable<any>;
    modalOpen$: Observable<boolean>;

    constructor(private adapter: Adapter, private storeService: StoreService) {}

    ngOnInit() {
        this.vacation$ = this.adapter.vacation$;
        this.vacationState$ = this.adapter.vacationState$;
        this.vacationError$ = this.adapter.vacationError$;
        this.modalOpen$ = this.adapter.vacationModalOpen$;
    }

    toggleModal() {
        this.storeService.togglePauseModal();
    }

    resumePause() {
        this.adapter.cancelVacation();
    }
}
