import { Action } from "@ngrx/store";

export enum ActionTypes {
    TOGGLE_APP_LAUNCH_DONE = "[Misc] Toggling the app launch done",
    SET_APP_INIT_DONE = "[Misc] Set app init done",
    SET_SETTINGS_FETCHED = "[Misc] Setting the settings fetched",
    SET_EXPERIMENTS_FETCHED = "[MISC] Set experiments fetched",
    SHOW_CAMPAIGN_POPUP_NUDGE = "[MISC] Show the nudge campaign popup",
    HIDE_CAMPAIGN_POPUP_NUDGE = "[MISC] Hide the nudge campaign popup",
    SHOW_GENERIC_NUDGE = "[MISC] Show the nudge generic",
    HIDE_GENERIC_NUDGE = "[MISC] Hide the nudge generic",
    SHOW_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE = "[MISC] Show the nudge auto debit payment failure",
    HIDE_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE = "[MISC] Hide the nudge auto debit payment failure",
    SHOW_BLOCK_ACCESS_NUDGE = "[MISC] Show the nudge block access",
    HIDE_BLOCK_ACCESS_NUDGE = "[MISC] Hide the nudge block access",
}

export class ToggleAppLaunchDone implements Action {
    readonly type = ActionTypes.TOGGLE_APP_LAUNCH_DONE;
}

export class SetAppInitDone implements Action {
    readonly type = ActionTypes.SET_APP_INIT_DONE;
}

export class ShowCampaignPopupNudge implements Action {
    readonly type = ActionTypes.SHOW_CAMPAIGN_POPUP_NUDGE;
}

export class HideCampaignPopupNudge implements Action {
    readonly type = ActionTypes.HIDE_CAMPAIGN_POPUP_NUDGE;
}

export class ShowGenericNudge implements Action {
    readonly type = ActionTypes.SHOW_GENERIC_NUDGE;
}

export class HideGenericNudge implements Action {
    readonly type = ActionTypes.HIDE_GENERIC_NUDGE;
}

export class ShowAutoDebitPaymentFailureNudge implements Action {
    readonly type = ActionTypes.SHOW_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE;
}

export class HideAutoDebitPaymentFailureNudge implements Action {
    readonly type = ActionTypes.HIDE_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE;
}

export class ShowBlockAccessNudge implements Action {
    readonly type = ActionTypes.SHOW_BLOCK_ACCESS_NUDGE;
    constructor(public payload?: { showSkipBtn: boolean }) {}
}

export class HideBlockAccessNudge implements Action {
    readonly type = ActionTypes.HIDE_BLOCK_ACCESS_NUDGE;
}

export class SetSettingsFetched implements Action {
    readonly type = ActionTypes.SET_SETTINGS_FETCHED;
    constructor(public payload: boolean) {}
}

export class SetExperimentsFetched implements Action {
    readonly type = ActionTypes.SET_EXPERIMENTS_FETCHED;
    constructor(public payload: boolean) {}
}

export type Actions =
    | ToggleAppLaunchDone
    | SetAppInitDone
    | SetSettingsFetched
    | SetExperimentsFetched
    | ShowCampaignPopupNudge
    | HideCampaignPopupNudge
    | ShowGenericNudge
    | HideGenericNudge
    | ShowAutoDebitPaymentFailureNudge
    | HideAutoDebitPaymentFailureNudge
    | ShowBlockAccessNudge
    | HideBlockAccessNudge;
