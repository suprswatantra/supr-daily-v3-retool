export interface MiscState {
    appLaunchDone?: boolean;
    appInitDone?: boolean;
    settingsFetched?: boolean;
    experimentsFetched?: boolean;
    showCampaignPopupNudge?: boolean;
    showGenericNudge?: boolean;
    showShowAutoDebitPaymentFailureNudge?: boolean;
    showBlockAccessNudge?: boolean;
    blockAccessNudgeShowSkipBtn: boolean;
}

export const initialState: MiscState = {
    appLaunchDone: false,
    appInitDone: false,
    settingsFetched: false,
    experimentsFetched: false,
    showCampaignPopupNudge: false,
    showGenericNudge: false,
    showShowAutoDebitPaymentFailureNudge: false,
    showBlockAccessNudge: false,
    blockAccessNudgeShowSkipBtn: false,
};
