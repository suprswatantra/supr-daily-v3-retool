import { createFeatureSelector, createSelector } from "@ngrx/store";

import { MiscState } from "./misc.state";

import { selectCategoryState } from "../category/category.selectors";
import { CategoryCurrentState } from "./../category/category.state";

export const selectMiscFeature = createFeatureSelector<MiscState>("misc");

export const selectAppLaunchDone = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.appLaunchDone
);

export const selectAppInitDone = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.appInitDone
);

export const selectNudgeCampaignPopup = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.showCampaignPopupNudge
);

export const selectNudgeGeneric = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.showGenericNudge
);

export const selectNudgeAutoDebitPaymentFailure = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.showShowAutoDebitPaymentFailureNudge
);

export const selectBlockAccessNudge = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.showBlockAccessNudge
);

export const selectBlockAccessNudgeShowSkipBtn = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.blockAccessNudgeShowSkipBtn
);

export const selectSettingsFetched = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.settingsFetched
);

export const selectExperimentsFetched = createSelector(
    selectMiscFeature,
    (state: MiscState) => state.experimentsFetched
);

export const selectDiscoveryDataLoading = createSelector(
    selectCategoryState,
    (categoryState: CategoryCurrentState) =>
        categoryState === CategoryCurrentState.FETCHING_CATEGORIES
);
