import { Actions, ActionTypes } from "./misc.actions";
import { initialState, MiscState } from "./misc.state";

export function miscReducer(state = initialState, action: Actions): MiscState {
    switch (action.type) {
        case ActionTypes.TOGGLE_APP_LAUNCH_DONE: {
            return { ...state, appLaunchDone: !state.appLaunchDone };
        }

        case ActionTypes.SET_APP_INIT_DONE: {
            return { ...state, appInitDone: true };
        }

        case ActionTypes.SHOW_CAMPAIGN_POPUP_NUDGE: {
            return { ...state, showCampaignPopupNudge: true };
        }

        case ActionTypes.HIDE_CAMPAIGN_POPUP_NUDGE: {
            return { ...state, showCampaignPopupNudge: false };
        }

        case ActionTypes.SHOW_GENERIC_NUDGE: {
            return { ...state, showGenericNudge: true };
        }

        case ActionTypes.HIDE_GENERIC_NUDGE: {
            return { ...state, showGenericNudge: false };
        }

        case ActionTypes.SHOW_BLOCK_ACCESS_NUDGE: {
            return {
                ...state,
                showBlockAccessNudge: true,
                blockAccessNudgeShowSkipBtn: action.payload
                    ? !!action.payload.showSkipBtn
                    : false,
            };
        }

        case ActionTypes.HIDE_BLOCK_ACCESS_NUDGE: {
            return {
                ...state,
                showBlockAccessNudge: false,
                blockAccessNudgeShowSkipBtn: false,
            };
        }

        case ActionTypes.SHOW_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE: {
            return { ...state, showShowAutoDebitPaymentFailureNudge: true };
        }

        case ActionTypes.HIDE_AUTO_DEBIT_PAYMENT_FAILURE_NUDGE: {
            return { ...state, showShowAutoDebitPaymentFailureNudge: false };
        }

        case ActionTypes.SET_SETTINGS_FETCHED: {
            return { ...state, settingsFetched: action.payload };
        }

        case ActionTypes.SET_EXPERIMENTS_FETCHED: {
            return { ...state, experimentsFetched: action.payload };
        }

        default: {
            return state;
        }
    }
}
