import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";

import { miscReducer } from "./misc.reducer";

@NgModule({
    imports: [StoreModule.forFeature("misc", miscReducer)],
    providers: [],
})
export class MiscStoreModule {}
