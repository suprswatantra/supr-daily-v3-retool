import { Action } from "@ngrx/store";

import { SuprApi } from "@types";

export enum ActionTypes {
    GET_ORDERS_DATA = "[Order] Get orders data",
    GET_ORDERS_DATA_SUCCESS = "[Order] Get orders success",
    GET_ORDERS_DATA_FAILURE = "[Order] Get orders failure",
}

export class GetOrdersDataAction implements Action {
    readonly type = ActionTypes.GET_ORDERS_DATA;
    constructor(
        public payload: { days: number; toDate: string; firstLoad?: boolean }
    ) {}
}

export class GetOrdersDataSuccessAction implements Action {
    readonly type = ActionTypes.GET_ORDERS_DATA_SUCCESS;
    constructor(
        public payload: {
            response: SuprApi.OrderHistoryData;
            firstLoad?: boolean;
        }
    ) {}
}

export class GetOrdersDataFailureAction implements Action {
    readonly type = ActionTypes.GET_ORDERS_DATA_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | GetOrdersDataAction
    | GetOrdersDataSuccessAction
    | GetOrdersDataFailureAction;
