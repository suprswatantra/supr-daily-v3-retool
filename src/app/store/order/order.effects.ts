import { Injectable } from "@angular/core";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";

import { ApiService } from "app/services/data/api.service";

import * as OrderActions from "./order.actions";
import { ScheduleStoreActions } from "@store/schedule";

import { SuprApi } from "@types";

@Injectable()
export class OrderStoreEffects {
    constructor(private actions$: Actions, private apiService: ApiService) {}

    @Effect()
    fetchOrderHistory$: Observable<Action> = this.actions$.pipe(
        ofType<OrderActions.GetOrdersDataAction>(
            OrderActions.ActionTypes.GET_ORDERS_DATA
        ),
        switchMap((action) =>
            this.apiService
                .fetchOrderHistory(action.payload.toDate, action.payload.days)
                .pipe(
                    switchMap((res: SuprApi.OrderHistoryData) => [
                        new OrderActions.GetOrdersDataSuccessAction({
                            response: res,
                            firstLoad: action.payload.firstLoad,
                        }),
                        new ScheduleStoreActions.UpdateScheduleCalendarAction({
                            startDate: action.payload.toDate,
                            numberOfDays: action.payload.days,
                            history: true,
                        }),
                    ]),

                    catchError((error) =>
                        observableOf(
                            new OrderActions.GetOrdersDataFailureAction(error)
                        )
                    )
                )
        )
    );
}
