import { OrderDict, SkuDict } from "@types";
import { OrderHistory, Sku, OrderDeliverySlotDict } from "@models";

export enum OrderCurrentState {
    NO_ACTION = "0",
    FETCHING_ORDERS = "1",
    FETCHING_ORDERS_DONE = "2",
    FETCHING_MORE_ORDERS = "3",
}

export interface OrderState {
    error?: any;
    skuList: Sku[];
    message: string;
    skuDict: SkuDict;
    multiplePOD: boolean;
    orderDict: OrderDict;
    orderList: OrderHistory[];
    currentState: OrderCurrentState;
    orderDeliverySlotDict: OrderDeliverySlotDict;
}

export const initialState: OrderState = {
    skuList: [],
    skuDict: {},
    error: null,
    message: "",
    orderList: [],
    orderDict: {},
    multiplePOD: false,
    orderDeliverySlotDict: {},
    currentState: OrderCurrentState.NO_ACTION,
};
