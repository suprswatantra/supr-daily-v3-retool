import {
    Order,
    OrderHistory,
    OrderDeliverySlotMap,
    OrderDeliverySlotDict,
} from "@models";

import { Actions, ActionTypes } from "./order.actions";
import { initialState, OrderState, OrderCurrentState } from "./order.state";

export function orderReducer(
    state = initialState,
    action: Actions
): OrderState {
    switch (action.type) {
        case ActionTypes.GET_ORDERS_DATA: {
            return {
                ...state,
                error: null,
                currentState: action.payload.firstLoad
                    ? OrderCurrentState.FETCHING_ORDERS
                    : OrderCurrentState.FETCHING_MORE_ORDERS,
            };
        }
        case ActionTypes.GET_ORDERS_DATA_SUCCESS: {
            const { firstLoad, response } = action.payload;

            /* [TODO] Better ways ? */
            const mergedSkuList = firstLoad
                ? response.sku_details
                : [...state.skuList, ...response.sku_details];
            const reversedOrderList = [...response.order_history].reverse();
            const mergedOrderList = firstLoad
                ? reversedOrderList
                : [...reversedOrderList, ...state.orderList];

            return {
                ...state,
                error: null,
                skuList: mergedSkuList,
                orderList: mergedOrderList,
                message: response.message,
                skuDict: convertListToObj(mergedSkuList, "id"),
                orderDict: convertListToObj(mergedOrderList, "date"),
                currentState: OrderCurrentState.FETCHING_ORDERS_DONE,
                multiplePOD: response.is_multiple_delivery_proof_enabled,
                orderDeliverySlotDict: generateDeliverySlotMap(mergedOrderList),
            };
        }
        case ActionTypes.GET_ORDERS_DATA_FAILURE: {
            return {
                ...state,
                skuList: [],
                skuDict: {},
                orderList: [],
                orderDict: {},
                error: action.payload,
                currentState: OrderCurrentState.FETCHING_ORDERS_DONE,
            };
        }
        default: {
            return state;
        }
    }
}

/* [TODO] Move this util function to a central space */
function convertListToObj(entityList: any[], key: string): any {
    return entityList.reduce((acc, entity) => {
        acc[entity[key]] = entity;
        return acc;
    }, {});
}

function generateDeliverySlotMap(
    orderHistory: OrderHistory[]
): OrderDeliverySlotDict {
    const orderDeliverySlotDict = {};

    orderHistory.forEach((history: OrderHistory) => {
        let orderDeliverySlotMap: OrderDeliverySlotMap;

        if (history.orders && history.orders.length) {
            orderDeliverySlotMap = history.orders.reduce(
                (deliverySlotMap, order: Order) => {
                    if (!deliverySlotMap[order.delivery_type]) {
                        deliverySlotMap[order.delivery_type] = [];
                    }

                    deliverySlotMap[order.delivery_type].push(order);

                    return deliverySlotMap;
                },
                {
                    date: history.date,
                    delivery_summary: history.delivery_summary,
                    delivery_timeline: history.delivery_timeline,
                    slot_info:
                        history.delivery_summary &&
                        history.delivery_summary.slot_info,
                }
            );
        }

        orderDeliverySlotDict[history.date] = orderDeliverySlotMap;
    });

    return orderDeliverySlotDict;
}
