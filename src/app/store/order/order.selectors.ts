import { createFeatureSelector, createSelector } from "@ngrx/store";

import { OrderStoreState } from ".";
import { SkuDict, OrderDict } from "@types";
import { selectQueryParams } from "@store/router/router.selectors";
import { SlotInfoMeta } from "@shared/models/schedule.model";

export const selectFeature = createFeatureSelector<OrderStoreState>("order");

export const selectOrderList = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.orderList
);

export const selectOrderDict = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.orderDict
);

export const selectOrderByDate = createSelector(
    selectOrderDict,
    (orderDict: OrderDict, props: { date: string }) => orderDict[props.date]
);

export const selectSkuList = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.skuList
);

export const selectSkuDict = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.skuDict
);

export const selectSkuById = createSelector(
    selectSkuDict,
    (skuDict: SkuDict, props: { skuId: number }) => skuDict[props.skuId]
);

export const selectOrdersCurrentState = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.currentState
);

export const selectNoOrdersMessage = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.message
);

export const selectOrderDeliveryByDate = createSelector(
    selectFeature,
    (state: OrderStoreState, props: { date: string }) =>
        state.orderDeliverySlotDict && state.orderDeliverySlotDict[props.date]
);

export const selectMultiplePODFlag = createSelector(
    selectFeature,
    (state: OrderStoreState) => state.multiplePOD
);

export const selectDeliveryImageByDateAndSlot = createSelector(
    selectOrderDict,
    selectQueryParams,
    (orderDict: OrderDict, queryParams) => {
        if (
            !orderDict ||
            !queryParams ||
            !queryParams["date"] ||
            !queryParams["slot"] ||
            !orderDict[queryParams["date"]] ||
            !orderDict[queryParams["date"]].delivery_summary ||
            !orderDict[queryParams["date"]].delivery_summary.slot_info ||
            !orderDict[queryParams["date"]].delivery_summary.slot_info[
                queryParams["slot"]
            ]
        ) {
            return;
        }

        const slotInfo = orderDict[queryParams["date"]].delivery_summary
            .slot_info[queryParams["slot"]] as SlotInfoMeta;

        return slotInfo.delivery_proof;
    }
);
