import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { orderReducer } from "./order.reducer";
import { OrderStoreEffects } from "./order.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("order", orderReducer),
        EffectsModule.forFeature([OrderStoreEffects]),
    ],
    providers: [],
})
export class OrderStoreModule {}
