import { HomeLayoutListWithPosition } from "@models";

export enum HomeLayoutCurrentState {
    NO_ACTION = "0",
    FETCHING_HOME_LAYOUT = "1",
    FETCHING_HOME_LAYOUT_DONE = "2",
}

export interface HomeLayoutState {
    currentState?: HomeLayoutCurrentState;
    error?: any;
    homeLayoutList: HomeLayoutListWithPosition;
}

export const initialState: HomeLayoutState = {
    currentState: HomeLayoutCurrentState.NO_ACTION,
    error: null,
    homeLayoutList: <HomeLayoutListWithPosition>{},
};
