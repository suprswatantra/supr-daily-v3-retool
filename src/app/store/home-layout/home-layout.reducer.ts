import { Actions, ActionTypes } from "./home-layout.actions";
import {
    initialState,
    HomeLayoutState,
    HomeLayoutCurrentState,
} from "./home-layout.state";

export function homeLayoutReducer(
    state = initialState,
    action: Actions
): HomeLayoutState {
    switch (action.type) {
        case ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: HomeLayoutCurrentState.FETCHING_HOME_LAYOUT,
            };
        }

        case ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: HomeLayoutCurrentState.FETCHING_HOME_LAYOUT_DONE,
                homeLayoutList: action.payload,
            };
        }

        case ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: HomeLayoutCurrentState.FETCHING_HOME_LAYOUT,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}
