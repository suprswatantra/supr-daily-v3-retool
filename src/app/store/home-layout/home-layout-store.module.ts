import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { homeLayoutReducer } from "./home-layout.reducer";
import { HomeLayoutStoreEffects } from "./home-layout.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("homeLayout", homeLayoutReducer),
        EffectsModule.forFeature([HomeLayoutStoreEffects]),
    ],
    providers: [],
})
export class HomeLayoutStoreModule {}
