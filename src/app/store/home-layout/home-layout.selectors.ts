import { createSelector, createFeatureSelector } from "@ngrx/store";

import { HomeLayoutState } from "./home-layout.state";
import { HomeLayoutListWithPosition } from "@shared/models";

export const selectFeature = createFeatureSelector<HomeLayoutState>(
    "homeLayout"
);

export const selectHomeLayoutList = createSelector(
    selectFeature,
    (state: HomeLayoutState) => state.homeLayoutList
);

export const selectHomeLayoutListByPosition = createSelector(
    selectHomeLayoutList,
    (collectionList: HomeLayoutListWithPosition, props: { position: string }) =>
        collectionList[props.position]
);

export const selectHomeLayoutCurrentState = createSelector(
    selectFeature,
    (state: HomeLayoutState) => state.currentState
);

export const selectFeaturedError = createSelector(
    selectFeature,
    (state: HomeLayoutState) => state.error
);
