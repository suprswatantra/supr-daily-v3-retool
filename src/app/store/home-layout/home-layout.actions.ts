import { Action } from "@ngrx/store";
import { HomeLayoutListWithPosition } from "@models";

export enum ActionTypes {
    FETCH_HOME_LAYOUT_LIST_REQUEST_ACTION = "[HOME_LAYOUT] Fetch home layout list request",
    FETCH_HOME_LAYOUT_LIST_REQUEST_SUCCESS_ACTION = "[HOME_LAYOUT] Fetch home layout list request success",
    FETCH_HOME_LAYOUT_LIST_REQUEST_FAILURE_ACTION = "[HOME_LAYOUT] Fetch home layout list request failure",
}

export class FetchHomeLayoutListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_ACTION;
    constructor(public payload?: boolean) {}
}

export class FetchHomeLayoutListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: HomeLayoutListWithPosition) {}
}

export class FetchHomeLayoutListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchHomeLayoutListRequestAction
    | FetchHomeLayoutListRequestSuccessAction
    | FetchHomeLayoutListRequestFailureAction;
