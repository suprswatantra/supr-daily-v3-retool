import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf, forkJoin } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";

import { HomeLayoutListWithPosition, HomeLayoutPositions } from "@models";

import { ApiService } from "@services/data/api.service";

import * as homeLayoutActions from "./home-layout.actions";

@Injectable()
export class HomeLayoutStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchHomeLayoutList$: Observable<Action> = this.actions$.pipe(
        ofType<homeLayoutActions.FetchHomeLayoutListRequestAction>(
            homeLayoutActions.ActionTypes.FETCH_HOME_LAYOUT_LIST_REQUEST_ACTION
        ),
        switchMap((action) => {
            const homeLayoutPositions = Object.values(HomeLayoutPositions);
            const apiCalls = homeLayoutPositions.map((position) =>
                this.apiService.fetchHomeLayoutList(position, action.payload)
            );
            return forkJoin(apiCalls).pipe(
                switchMap((results) => {
                    const homeLayoutsWithPosition = <
                        HomeLayoutListWithPosition
                    >{};
                    homeLayoutPositions.forEach((position, index) => {
                        homeLayoutsWithPosition[position] = results[index];
                    });

                    return [
                        new homeLayoutActions.FetchHomeLayoutListRequestSuccessAction(
                            homeLayoutsWithPosition
                        ),
                    ];
                }),
                catchError((error) =>
                    observableOf(
                        new homeLayoutActions.FetchHomeLayoutListRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );
}
