import { Action } from "@ngrx/store";
import { Sku, Feedback } from "@shared/models";

export enum ActionTypes {
    SET_ORDER_FEEDBACK_FETCH_FLAG = "[feedback] setting order feedback fetch flag",
    SET_ORDER_FEEDBACK_COMPLETED = "[feedback] setting  order feedback flag as completed",
    SET_ORDER_FEEDBACK = "[feedback] setting  order feedback request",

    SET_DELIVERY_FEEDBACK_FLAG = "[feedback] setting delivery feedback flag",
    SET_DELIVERY_FEEDBACK_COMPLETED = "[feedback] setting delivery feedback flag as completed",
    SET_DELIVERY_FEEDBACK_REQUEST = "[feedback] setting delivery feedback request",

    SET_DELIVERY_FEEDBACK_NUDGE = "[feedback] setting delivery feedback nudge",
    HIDE_DELIVERY_FEEDBACK_NUDGE = "[feedback] hide delivery feedback nudge",

    SET_DELIVERY_SKUS_FLAG = "[feedback] fetch delivery skus flag",
    SET_ORDER_SKU_LIST = "[feedback] set sku list",
}

export class setOrderFeedbackFetch implements Action {
    readonly type = ActionTypes.SET_ORDER_FEEDBACK_FETCH_FLAG;
}

export class setOrderFeedbackCompleted implements Action {
    readonly type = ActionTypes.SET_ORDER_FEEDBACK_COMPLETED;
}

export class setOrderFeedback implements Action {
    readonly type = ActionTypes.SET_ORDER_FEEDBACK;
    constructor(public payload: Feedback.OrderFeedback[]) {}
}

export class setDeliveryFeedbackFlag implements Action {
    readonly type = ActionTypes.SET_DELIVERY_FEEDBACK_FLAG;
}

export class setDeliveryFeedbackFlagCompleted implements Action {
    readonly type = ActionTypes.SET_DELIVERY_FEEDBACK_COMPLETED;
}

export class setDeliveryFeedbackRequest implements Action {
    readonly type = ActionTypes.SET_DELIVERY_FEEDBACK_REQUEST;
    constructor(public payload: Feedback.deliveryFeedbackRequest) {}
}

export class setDeliveryFeedbackNudge implements Action {
    readonly type = ActionTypes.SET_DELIVERY_FEEDBACK_NUDGE;
    constructor(public payload: boolean) {}
}

export class hideDeliveryFeedbackNudge implements Action {
    readonly type = ActionTypes.HIDE_DELIVERY_FEEDBACK_NUDGE;
}

export class setDeliverySkuFetchFlag implements Action {
    readonly type = ActionTypes.SET_DELIVERY_SKUS_FLAG;
    constructor(public payload: string) {}
}

export class setSkuList implements Action {
    readonly type = ActionTypes.SET_ORDER_SKU_LIST;
    constructor(public payload: Sku[]) {}
}

export type Actions =
    | setOrderFeedbackFetch
    | setOrderFeedbackCompleted
    | setOrderFeedback
    | setDeliveryFeedbackFlag
    | setDeliveryFeedbackFlagCompleted
    | setDeliveryFeedbackRequest
    | setDeliveryFeedbackNudge
    | hideDeliveryFeedbackNudge
    | setDeliverySkuFetchFlag
    | setSkuList;
