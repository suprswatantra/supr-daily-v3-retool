import { createFeatureSelector, createSelector } from "@ngrx/store";

import { FeedbackState } from "./feedback.state";

export const selectOrderFeedbackFeature = createFeatureSelector<FeedbackState>(
    "feedback"
);

export const selectDeliveryFeedbackRequest = createSelector(
    selectOrderFeedbackFeature,
    (state: FeedbackState) => state.deliveryFeedbackRequest
);

export const showDeliveryFeedbackNudge = createSelector(
    selectOrderFeedbackFeature,
    (state: FeedbackState) => state.deliveryFeedbackNudge
);

export const selectOrderFeedback = createSelector(
    selectOrderFeedbackFeature,
    (state: FeedbackState) => state.orderFeedback
);

export const selectDeliverySkus = createSelector(
    selectOrderFeedbackFeature,
    (state: FeedbackState) => state.skuList
);
