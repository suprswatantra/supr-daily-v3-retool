import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { CommonModule } from "@angular/common";
import { EffectsModule } from "@ngrx/effects";

import { feedbackReducer } from "./feedback.reducer";
import { FeedbackStoreEffects } from "./feedback.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("feedback", feedbackReducer),
        EffectsModule.forFeature([FeedbackStoreEffects]),
    ],

    providers: [],
})
export class FeedbackStoreModule {}
