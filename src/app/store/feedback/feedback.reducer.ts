import { Actions, ActionTypes } from "./feedback.actions";
import { initialState, FeedbackState } from "./feedback.state";

export function feedbackReducer(
    state = initialState,
    action: Actions
): FeedbackState {
    switch (action.type) {
        case ActionTypes.SET_ORDER_FEEDBACK_FETCH_FLAG: {
            return { ...state, orderFeedbackFetchFlag: true };
        }

        case ActionTypes.SET_ORDER_FEEDBACK_COMPLETED: {
            return { ...state, orderFeedbackFetchFlag: false };
        }

        case ActionTypes.SET_ORDER_FEEDBACK: {
            return { ...state, orderFeedback: action.payload };
        }

        case ActionTypes.SET_DELIVERY_FEEDBACK_FLAG: {
            return { ...state, deliveryFeedbackFetchFlag: true };
        }

        case ActionTypes.SET_DELIVERY_FEEDBACK_COMPLETED: {
            return { ...state, deliveryFeedbackFetchFlag: false };
        }

        case ActionTypes.SET_DELIVERY_FEEDBACK_REQUEST: {
            return { ...state, deliveryFeedbackRequest: action.payload };
        }

        case ActionTypes.SET_DELIVERY_FEEDBACK_NUDGE: {
            return { ...state, deliveryFeedbackNudge: action.payload };
        }

        case ActionTypes.HIDE_DELIVERY_FEEDBACK_NUDGE: {
            return { ...state, deliveryFeedbackNudge: false };
        }

        case ActionTypes.SET_DELIVERY_SKUS_FLAG: {
            return { ...state, date: action.payload };
        }

        case ActionTypes.SET_ORDER_SKU_LIST: {
            return { ...state, skuList: action.payload };
        }

        default: {
            return state;
        }
    }
}
