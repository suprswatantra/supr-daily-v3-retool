import { Sku, Feedback } from "@shared/models";

export interface FeedbackState {
    orderFeedbackFetchFlag?: boolean;
    orderFeedback: Feedback.OrderFeedback[];

    deliveryFeedbackFetchFlag?: boolean;
    deliveryFeedbackRequest?: Feedback.deliveryFeedbackRequest;

    deliveryFeedbackNudge?: boolean;

    date: string;
    skuList: Sku[];
}

export const initialState: FeedbackState = {
    orderFeedbackFetchFlag: false,
    orderFeedback: null,

    deliveryFeedbackFetchFlag: false,
    deliveryFeedbackRequest: null,

    deliveryFeedbackNudge: false,

    date: "",
    skuList: [],
};
