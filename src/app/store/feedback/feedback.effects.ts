import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, select, Store } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap, withLatestFrom } from "rxjs/operators";

import { StoreState } from "@store/store.state";

import { ApiService } from "@services/data/api.service";
import { FeedbackService } from "@services/layout/feedback.service";

import { UserStoreSelectors, FeedbackStoreActions } from "@supr/store";

import * as FeedbackActions from "./feedback.actions";

@Injectable()
export class FeedbackStoreEffects {
    constructor(
        private apiService: ApiService,
        private actions$: Actions,
        private store$: Store<StoreState>,
        private feedbackService: FeedbackService
    ) {}

    /*
        Fetch Feedback Requests for Order Calender
    */

    @Effect()
    fetchOrderFeedbackRequests$: Observable<Action> = this.actions$.pipe(
        ofType<FeedbackActions.setOrderFeedbackFetch>(
            FeedbackActions.ActionTypes.SET_ORDER_FEEDBACK_FETCH_FLAG
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectUserId))
        ),
        switchMap(([_, userId]) => {
            return this.apiService.fetchOrderFeedbackRequest(userId).pipe(
                switchMap(orderFeedback => [
                    new FeedbackStoreActions.setOrderFeedbackCompleted(),
                    new FeedbackStoreActions.setOrderFeedback(orderFeedback),
                ]),
                catchError(() =>
                    observableOf(
                        new FeedbackActions.setOrderFeedbackCompleted()
                    )
                )
            );
        })
    );

    /*
        Fetch Feedback Requests for delivery Nudge
    */

    @Effect()
    fetchDeliveryFeedbackRequest$: Observable<Action> = this.actions$.pipe(
        ofType<FeedbackActions.setDeliveryFeedbackFlag>(
            FeedbackActions.ActionTypes.SET_DELIVERY_FEEDBACK_FLAG
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectUserId))
        ),
        switchMap(([_, userId]) => {
            this.feedbackService.fetchOrderFeedbackSettings();

            return this.apiService
                .fetchOverallOrderFeedbackRequest(userId)
                .pipe(
                    switchMap(([overallFeedback]) => {
                        return [
                            new FeedbackActions.setDeliverySkuFetchFlag(
                                overallFeedback.feedback_date
                            ),
                            new FeedbackActions.setDeliveryFeedbackRequest(
                                overallFeedback
                            ),
                            new FeedbackActions.setDeliveryFeedbackFlagCompleted(),
                        ];
                    }),
                    catchError(() =>
                        observableOf(
                            new FeedbackActions.setDeliveryFeedbackFlagCompleted()
                        )
                    )
                );
        })
    );

    /*
        Fetch skuList for Delivery Nudge
    */

    @Effect()
    fetchDeliverySkuList$: Observable<Action> = this.actions$.pipe(
        ofType<FeedbackActions.setDeliverySkuFetchFlag>(
            FeedbackActions.ActionTypes.SET_DELIVERY_SKUS_FLAG
        ),
        switchMap(action => {
            return this.apiService.fetchOrderHistory(action.payload, 0).pipe(
                switchMap(orderHistory => {
                    const orderFeedbackConfig = this.feedbackService.getOrderFeedbackSettings();
                    const { order_history, sku_details } = orderHistory;
                    if (
                        orderFeedbackConfig.enabled &&
                        order_history[0] &&
                        order_history[0].date &&
                        order_history[0].date === action.payload &&
                        sku_details &&
                        sku_details.length > 0
                    ) {
                        return [
                            new FeedbackStoreActions.setSkuList(sku_details),
                            new FeedbackActions.setDeliveryFeedbackNudge(true),
                        ];
                    }
                    return [];
                }),
                catchError(() =>
                    observableOf(
                        new FeedbackActions.hideDeliveryFeedbackNudge(),
                        new FeedbackActions.setSkuList([])
                    )
                )
            );
        })
    );
}
