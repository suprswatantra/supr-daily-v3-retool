import { StoreService } from "@services/data/store.service";
import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, select, Store } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import {
    mergeMap,
    switchMap,
    catchError,
    withLatestFrom,
    map,
} from "rxjs/operators";

import {
    SETTINGS_KEYS_DEFAULT_VALUE,
    OOS_INFO_TOAST_MESSAGE_DELAY,
    OOS_INFO_STATES,
} from "@constants";

import { Sku, SkuAttributes } from "@models";

import { ToastService } from "@services/layout/toast.service";
import { ApiService } from "@services/data/api.service";
import { SkuService } from "@services/shared/sku.service";
import { SettingsService } from "@services/shared/settings.service";

import { StoreState } from "../store.state";

import {
    CollectionStoreActions,
    CollectionStoreSelectors,
} from "@store/collection";

import * as CartStoreSelectors from "../cart/cart.selectors";

import * as skuActions from "./sku.actions";
import * as SkuStoreSelectors from "./sku.selectors";

@Injectable()
export class SkuStoreEffects {
    constructor(
        private apiService: ApiService,
        private actions$: Actions,
        private store$: Store<StoreState>,
        private skuService: SkuService,
        private storeService: StoreService,
        private toastService: ToastService,
        private settingsService: SettingsService
    ) {}

    @Effect()
    fetchSkuList$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.FetchSkuListRequestAction>(
            skuActions.ActionTypes.FETCH_SKU_LIST_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(select(SkuStoreSelectors.selectSkuList)),
            this.store$.pipe(select(CartStoreSelectors.selectCartItems)),
            this.store$.pipe(
                select(CollectionStoreSelectors.selectPastOrderSkuListOriginal)
            )
        ),
        mergeMap(([action, skuList, cartItems, pastOrderSkuList]) => {
            const { refresh, silent } = action.payload;

            if (!refresh && skuList && skuList.length) {
                return observableOf({ type: "IGNORE" });
            } else {
                return this.apiService.fetchSkuList(silent).pipe(
                    switchMap((_skuList: Sku[]) => {
                        const skuDict = this.skuService.convertSkuListToDict(
                            _skuList
                        );
                        /* Filtering out oos items from the above filtered list */
                        let filteredPastOrderSkuList = this.skuService.filterOOSSkuFromList(
                            pastOrderSkuList,
                            skuDict
                        );

                        if (cartItems && cartItems.length) {
                            /* Filtering out cart items from past order cart collection */
                            filteredPastOrderSkuList = this.skuService.filterCartItemsFromPastOrderList(
                                filteredPastOrderSkuList,
                                cartItems
                            );
                        }

                        return [
                            new skuActions.FetchSkuListRequestSuccessAction(
                                _skuList
                            ),
                            new CollectionStoreActions.FetchCollectionListRequestAction(
                                true
                            ),
                            /* Update the cart past order collection store with filtered sku list
                            whenever sku data is updated */
                            new CollectionStoreActions.UpdatePastOrderSkuListAction(
                                filteredPastOrderSkuList
                            ),
                        ];
                    }),
                    catchError(() => observableOf({ type: "IGNORE" }))
                );
            }
        })
    );

    @Effect()
    fetchSkuListSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.FetchSkuListRequestSuccessAction>(
            skuActions.ActionTypes.FETCH_SKU_LIST_REQUEST_SUCCESS_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(
                select(CollectionStoreSelectors.selectPastOrderSkuListOriginal)
            ),
            this.store$.pipe(select(CartStoreSelectors.selectCartItems))
        ),
        map(([action, pastOrderSkuList, cartItems]) => {
            const skuDict =
                action.payload && action.payload.length
                    ? this.skuService.convertSkuListToDict(action.payload)
                    : {};
            /* Filtering out oos items from the past order sku list */
            let filteredSkuList = this.skuService.filterOOSSkuFromList(
                pastOrderSkuList,
                skuDict
            );

            if (cartItems && cartItems.length) {
                /* Filtering out cart items from past order cart collection */
                filteredSkuList = this.skuService.filterCartItemsFromPastOrderList(
                    filteredSkuList,
                    cartItems
                );
            }

            this.storeService.checkCartDates();

            return new CollectionStoreActions.UpdatePastOrderSkuListAction(
                filteredSkuList
            );
        })
    );

    @Effect()
    fetchSimilarProducts$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.FetchSimilarProductsRequestAction>(
            skuActions.ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(select(SkuStoreSelectors.selectSkuDict))
        ),
        mergeMap(([action, skuDict]) =>
            this.apiService.fetchSimilarProducts(action.payload.skuId).pipe(
                map((products: number[]) => {
                    /* Remove OOS Products from similar products */

                    const filteredList = this.skuService.filterOOSSkuFromList(
                        products,
                        skuDict
                    );

                    return new skuActions.FetchSimilarProductsRequestSuccessAction(
                        {
                            skuId: action.payload.skuId,
                            intent: action.payload.intent,
                            skuList: filteredList.slice(0, 10),
                        }
                    );
                }),

                catchError((error) =>
                    observableOf(
                        new skuActions.FetchSimilarProductsRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchSkuOtherInfo$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.FetchSkuAttributesRequestAction>(
            skuActions.ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_ACTION
        ),
        mergeMap(() =>
            this.apiService.fetchSkuAttributes().pipe(
                map((skuOtherInfo: SkuAttributes) => {
                    return new skuActions.FetchSkuAttributesRequestSuccessAction(
                        skuOtherInfo
                    );
                }),

                catchError((error) =>
                    observableOf(
                        new skuActions.FetchSkuAttributesRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    updateSkuOosInfo$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.UpdateSkuOosInfo>(
            skuActions.ActionTypes.UPDATE_SKU_OOS_INFO
        ),
        withLatestFrom(
            this.store$.pipe(select(SkuStoreSelectors.selectSkuDict))
        ),
        mergeMap(([action, skuDict]) => {
            const { skuId, state } = action.payload;

            const requestPayload = {
                state,
                sku_id: skuId,
            };

            return this.apiService.updateSkuOOSInfo(requestPayload).pipe(
                map(() => {
                    if (skuDict[skuId].notify_enabled) {
                        const TEXTS = this.settingsService.getSettingsValue(
                            "oosNotifyTexts",
                            SETTINGS_KEYS_DEFAULT_VALUE.oosNotifyTexts
                        );

                        if (state === OOS_INFO_STATES.OOS) {
                            this.toastService.present(TEXTS.notifySection.oos, {
                                delay: OOS_INFO_TOAST_MESSAGE_DELAY,
                            });
                        } else if (state === OOS_INFO_STATES.NOTIFY) {
                            this.toastService.present(
                                TEXTS.notifySection.notify_v2,
                                {
                                    delay: OOS_INFO_TOAST_MESSAGE_DELAY,
                                }
                            );
                        }
                    }

                    return new skuActions.UpdateSkuOOSInfoSuccessAction({
                        skuId,
                        state,
                    });
                }),
                catchError(() =>
                    observableOf(new skuActions.UpdateSkuOOSInfoFailureAction())
                )
            );
        })
    );

    /* [TODO_RITESH] Deprecate this effect once alternates experiment is resolved */
    @Effect()
    updateSkuOosInfoDeprecate$: Observable<Action> = this.actions$.pipe(
        ofType<skuActions.UpdateSkuOosInfoDeprecate>(
            skuActions.ActionTypes.UPDATE_SKU_OOS_INFO_DEPRECATE
        ),
        mergeMap((action) => {
            const { skuId, state, showToastMessage } = action.payload;

            const body = {
                sku_id: skuId,
                state,
            };

            return this.apiService.updateSkuOOSInfo(body).pipe(
                switchMap(() => {
                    const TEXTS = this.settingsService.getSettingsValue(
                        "oosNotifyTexts",
                        SETTINGS_KEYS_DEFAULT_VALUE.oosNotifyTexts
                    );

                    if (showToastMessage) {
                        this.toastService.present(TEXTS.notifySection.notify, {
                            delay: OOS_INFO_TOAST_MESSAGE_DELAY,
                        });
                    }

                    return [];
                }),
                catchError(() =>
                    observableOf(
                        new skuActions.UpdateSkuNotifyRequestFailureAction({
                            state,
                            skuId,
                        })
                    )
                )
            );
        })
    );
}
