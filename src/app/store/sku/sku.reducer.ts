import { Actions, ActionTypes } from "./sku.actions";
import { initialState, SkuState } from "./sku.state";
import { Sku, OutOfStockInfoStateType } from "@models";
import { SkuDict, NotifiedDict } from "@types";

const convertSkuListToDict = (skuList: Sku[]): SkuDict =>
    skuList.reduce((dict, sku) => {
        dict[sku.id] = { ...sku };
        return dict;
    }, {});

const getSkuOosState = (
    skuId: number,
    notifiedDic: NotifiedDict
): OutOfStockInfoStateType => {
    const dt = notifiedDic[skuId];
    return dt ? (dt === "notify" ? "oos" : "notify") : "oos";
};

const filterNotifiedSkus = (skuList: Sku[]): NotifiedDict =>
    skuList.reduce((dict, sku) => {
        if (sku.oos_info && sku.oos_info.state !== "add") {
            dict[sku.id] = sku.oos_info.state;
        }
        return dict;
    }, {});

const updateNotifiedSku = (
    skuId: number,
    notifiedDic: NotifiedDict,
    state: OutOfStockInfoStateType = null,
    singleLineUpdate = ""
): NotifiedDict => {
    if (state) {
        return {
            ...notifiedDic,
            [skuId]: state,
        };
    } else if (singleLineUpdate) {
        const dt = notifiedDic[skuId];
        return {
            ...notifiedDic,
            [skuId]: dt
                ? dt === "unNotify"
                    ? "notifySl"
                    : "unNotify"
                : "notifySl",
        };
    } else {
        const dt = getSkuOosState(skuId, notifiedDic);
        return {
            ...notifiedDic,
            [skuId]: dt,
        };
    }
};

export function skuReducer(state = initialState, action: Actions): SkuState {
    switch (action.type) {
        case ActionTypes.FETCH_SKU_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                loading: true,
            };
        }

        case ActionTypes.FETCH_SKU_LIST_REQUEST_SUCCESS_ACTION: {
            const skuData = action.payload;
            return {
                ...state,
                error: null,
                skuList: skuData,
                notifiedSkuDict: filterNotifiedSkus(skuData),
                skuDict: convertSkuListToDict(skuData),
                loading: false,
            };
        }

        case ActionTypes.FETCH_SKU_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                loading: false,
            };
        }

        case ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                loading: true,
            };
        }

        case ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                skuAttributes: action.payload,
                loading: false,
            };
        }

        case ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                loading: false,
            };
        }

        case ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_ACTION: {
            return {
                ...state,
                loading: true,
                fetchingSimilar: true,
                similarProducts: null,
            };
        }

        case ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                loading: false,
                fetchingSimilar: false,
                similarProductsDict: {
                    ...state.similarProductsDict,
                    [action.payload.skuId]: action.payload.skuList,
                },
                /* [TODO_RITESH] Deprecate this state once alternates experiment is resolved */
                similarProducts: action.payload.skuList,
            };
        }

        case ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                loading: false,
                fetchingSimilar: false,
                similarProducts: null,
            };
        }

        case ActionTypes.RESET_SIMILAR_PRODUCTS_LIST_ACTION: {
            return {
                ...state,
                fetchingSimilar: false,
                similarProducts: null,
            };
        }

        case ActionTypes.UPDATE_SKU_NOTIFY: {
            return {
                ...state,
                notifiedSkuDict: updateNotifiedSku(
                    action.payload.skuId,
                    state.notifiedSkuDict,
                    null,
                    action.payload.singleLineUpdate
                ),
            };
        }

        /* [TODO_RITESH] Deprecate this case once alternates experiment is resolved */
        case ActionTypes.UPDATE_SKU_NOTIFY_FAILURE: {
            return {
                ...state,
                notifiedSkuDict: updateNotifiedSku(
                    action.payload.skuId,
                    state.notifiedSkuDict,
                    action.payload.state
                ),
            };
        }

        case ActionTypes.UPDATE_SKU_OOS_INFO: {
            return {
                ...state,
                loading: true,
            };
        }

        case ActionTypes.UPDATE_SKU_OOS_INFO_SUCCESS: {
            return {
                ...state,
                loading: false,
                notifiedSkuDict: {
                    ...state.notifiedSkuDict,
                    [action.payload.skuId]: action.payload.state,
                },
            };
        }

        case ActionTypes.UPDATE_SKU_OOS_INFO_FAILURE: {
            return {
                ...state,
                loading: false,
            };
        }

        default: {
            return state;
        }
    }
}
