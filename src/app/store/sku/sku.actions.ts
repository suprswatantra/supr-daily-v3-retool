import { Action } from "@ngrx/store";
import { Sku, OutOfStockInfoStateType, SkuAttributes } from "@models";
import { FetchSimilarProductsActionData } from "@types";

export enum ActionTypes {
    FETCH_SKU_LIST_REQUEST_ACTION = "[SKU] Fetch sku list request",
    FETCH_SKU_LIST_REQUEST_SUCCESS_ACTION = "[SKU] Fetch sku list request success",
    FETCH_SKU_LIST_REQUEST_FAILURE_ACTION = "[SKU] Fetch sku list request failure",

    FETCH_SKU_ATTRIBUTES_REQUEST_ACTION = "[SKU] Fetch sku other attributes request",
    FETCH_SKU_ATTRIBUTES_REQUEST_SUCCESS_ACTION = "[SKU] Fetch sku other attributes request success",
    FETCH_SKU_ATTRIBUTES_REQUEST_FAILURE_ACTION = "[SKU] Fetch sku other attributes request failure",

    FETCH_SIMILAR_PRODUCTS_REQUEST_ACTION = "[SKU] Fetch similar products request",
    FETCH_SIMILAR_PRODUCTS_REQUEST_SUCCESS_ACTION = "[SKU] Fetch similar products request success",
    FETCH_SIMILAR_PRODUCTS_REQUEST_FAILURE_ACTION = "[SKU] Fetch similar products request failure",

    RESET_SIMILAR_PRODUCTS_LIST_ACTION = "[SKU] RESET SIMILAR PRODUCTS LIST",

    UPDATE_SKU_OOS_INFO = "[SKU] Update Sku Oos Info",
    UPDATE_SKU_OOS_INFO_SUCCESS = "[SKU] Update Sku Oos Info Success",
    UPDATE_SKU_OOS_INFO_FAILURE = "[SKU] Update Sku Oos Info Failure",
    UPDATE_SKU_OOS_INFO_DEPRECATE = "[SKU] Update Sku Oos Info Deprecate",

    UPDATE_SKU_NOTIFY = "[SKU] Update sku Notify",

    UPDATE_SKU_NOTIFY_FAILURE = "[SKU] Update sku Notify Failure",
}

export class FetchSkuListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_LIST_REQUEST_ACTION;
    constructor(public payload: { refresh: boolean; silent?: boolean }) {}
}

export class FetchSkuListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: Sku[]) {}
}

export class FetchSkuListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchSkuAttributesRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_ACTION;
    constructor(public payload: { refresh: boolean; silent?: boolean }) {}
}

export class FetchSkuAttributesRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_SUCCESS_ACTION;
    constructor(public payload: SkuAttributes) {}
}

export class FetchSkuAttributesRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SKU_ATTRIBUTES_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchSimilarProductsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_ACTION;
    constructor(public payload: FetchSimilarProductsActionData) {}
}

export class FetchSimilarProductsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_SUCCESS_ACTION;
    constructor(
        public payload: { skuId: number; skuList: number[]; intent?: string }
    ) {}
}

export class FetchSimilarProductsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SIMILAR_PRODUCTS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

/* [TODO_RITESH] Deprecate this action once alternates experiment is resolved */
export class UpdateSkuOosInfoDeprecate implements Action {
    readonly type = ActionTypes.UPDATE_SKU_OOS_INFO_DEPRECATE;
    constructor(
        public payload: {
            skuId: number;
            state?: OutOfStockInfoStateType;
            showToastMessage?: boolean;
        }
    ) {}
}

export class UpdateSkuOosInfo implements Action {
    readonly type = ActionTypes.UPDATE_SKU_OOS_INFO;
    constructor(
        public payload: {
            skuId: number;
            state?: OutOfStockInfoStateType;
        }
    ) {}
}

export class UpdateSkuNotify implements Action {
    readonly type = ActionTypes.UPDATE_SKU_NOTIFY;
    constructor(
        public payload: {
            skuId: number;
            state?: OutOfStockInfoStateType;
            singleLineUpdate?: string;
        }
    ) {}
}

/* [TODO_RITESH] Deprecate this action once alternates experiment is resolved */
export class UpdateSkuNotifyRequestFailureAction implements Action {
    readonly type = ActionTypes.UPDATE_SKU_NOTIFY_FAILURE;
    constructor(
        public payload: {
            skuId: number;
            state?: OutOfStockInfoStateType;
        }
    ) {}
}

export class UpdateSkuOOSInfoSuccessAction implements Action {
    readonly type = ActionTypes.UPDATE_SKU_OOS_INFO_SUCCESS;
    constructor(
        public payload: {
            skuId: number;
            state: OutOfStockInfoStateType;
        }
    ) {}
}

export class UpdateSkuOOSInfoFailureAction implements Action {
    readonly type = ActionTypes.UPDATE_SKU_OOS_INFO_FAILURE;
    constructor() {}
}

export class ResetSimilarProductsListAction implements Action {
    readonly type = ActionTypes.RESET_SIMILAR_PRODUCTS_LIST_ACTION;
}

export type Actions =
    | FetchSkuListRequestAction
    | FetchSkuListRequestSuccessAction
    | FetchSkuListRequestFailureAction
    | FetchSimilarProductsRequestAction
    | FetchSimilarProductsRequestSuccessAction
    | FetchSimilarProductsRequestFailureAction
    | ResetSimilarProductsListAction
    | UpdateSkuOosInfoDeprecate
    | UpdateSkuOosInfo
    | UpdateSkuOOSInfoSuccessAction
    | UpdateSkuOOSInfoFailureAction
    | UpdateSkuNotify
    | UpdateSkuNotifyRequestFailureAction
    | FetchSkuAttributesRequestAction
    | FetchSkuAttributesRequestSuccessAction
    | FetchSkuAttributesRequestFailureAction;
