import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { skuReducer } from "./sku.reducer";
import { EffectsModule } from "@ngrx/effects";
import { SkuStoreEffects } from "./sku.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("sku", skuReducer),
        EffectsModule.forFeature([SkuStoreEffects]),
    ],
    providers: [],
})
export class SkuStoreModule {}
