import { Sku, SkuAttributes } from "@models";
import { SkuDict, NotifiedDict } from "@types";

export interface SkuState {
    loading?: boolean;
    error?: any;
    skuList: Sku[];
    skuDict: SkuDict;
    fetchingSimilar?: boolean;
    similarProducts: number[];
    similarProductsDict: {
        [skuId: number]: number[];
    };
    notifiedSkuDict: NotifiedDict;
    skuAttributes?: SkuAttributes;
}

export const initialState: SkuState = {
    loading: false,
    error: null,
    skuList: [],
    skuDict: {},
    notifiedSkuDict: {},
    similarProducts: null,
    fetchingSimilar: false,
    similarProductsDict: null,
    skuAttributes: null,
};
