import { createSelector, createFeatureSelector } from "@ngrx/store";
import { SkuAttributes } from "@shared/models";
import { SkuDict, NotifiedDict } from "@types";
import { SkuState } from "./sku.state";

export const selectFeature = createFeatureSelector<SkuState>("sku");

export const selectSkuList = createSelector(
    selectFeature,
    (state: SkuState) => state.skuList
);

export const selectSkuAttributes = createSelector(
    selectFeature,
    (state: SkuState) => state.skuAttributes
);

export const selectSkuDict = createSelector(
    selectFeature,
    (state: SkuState) => state.skuDict
);

export const selectNotifiedSkuDict = createSelector(
    selectFeature,
    (state: SkuState) => state.notifiedSkuDict
);

export const selectSkuById = createSelector(
    selectSkuDict,
    (skuDict: SkuDict, props: { skuId: number }) => skuDict[props.skuId]
);

export const selectSkuAlternatesById = createSelector(
    selectSkuAttributes,
    (skuAttributes: SkuAttributes, props: { skuId: number }) => {
        if (skuAttributes && skuAttributes[props.skuId]) {
            return skuAttributes[props.skuId];
        } else {
            return null;
        }
    }
);

export const selectSkuLoading = createSelector(
    selectFeature,
    (state: SkuState) => state.loading
);

export const selectSimilarProductsFetchStatus = createSelector(
    selectFeature,
    (state: SkuState) => state.fetchingSimilar
);

export const selectSimilarProducts = createSelector(
    selectFeature,
    (state: SkuState) => state.similarProducts
);

export const selectSimilarProductsBySkuId = createSelector(
    selectFeature,
    (state: SkuState, props: { skuId: number }) =>
        state.similarProductsDict && state.similarProductsDict[props.skuId]
);

export const selectSkuNotified = createSelector(
    selectNotifiedSkuDict,
    (notifiedSkuDict: NotifiedDict, props: { skuId: number }) =>
        notifiedSkuDict[props.skuId]
);
