import { SELFSERVE } from "@shared/models";
import { SuprApi } from "@types";

export function sum(a: number, b: number): number {
    return a + b;
}
export function complaintCreationRequest({
    orderData,
    serviceData,
    notesData,
    notes,
    orderRelatedIssues,
    selectedServiceIssues,
    isOndSelected,
    date,
    slot,
    interaction_id,
    uploadedImages,
}): {
    requestBody: SuprApi.ComplaintCreationRequest;
    agentMessage: string;
} {
    const { ond_template } = orderData;
    const orders = [];
    orderRelatedIssues.forEach((value, key) => {
        if (value.selected && !value.disabled) {
            const complaint =
                value.dropdownList &&
                value.dropdownList.length > 0 &&
                value.dropdownList.find((complaint) => complaint.selected);
            const complaint_id = isOndSelected
                ? ond_template.id
                : complaint && complaint.id;

            const maxQuantity =
                value.maxQuantity ||
                (value.orderData && value.orderData.delivered_quantity);

            const feedback_params = [];
            for (const [key, val] of Object.entries(
                value.feedbackParamsObject
            )) {
                if (val) {
                    feedback_params.push(key);
                }
            }

            orders.push({
                order_id: key,
                id: value.id,
                complaint_id,
                complaint_name: isOndSelected
                    ? ond_template.description
                    : complaint.value,
                quantity: (isOndSelected && maxQuantity) || value.quantity,
                sku_name: value.sku_name,
                feedback_request_id: value.feedback_request_id,
                feedback_params,
            });
        }
    });

    let requestBody = {
        order_date: date,
        time_slot: slot,
        node_id: 1,
        interaction_id,
        complaint_images: uploadedImages || [],
    };
    let agentMessage = `Order Date : ${date},
        Delivery slot type: ${slot},`;

    if (orderData && orderData.id && orders && orders.length > 0) {
        requestBody[orderData.id] = orders;
        agentMessage = orders.reduce((acc, order) => {
            return (
                acc +
                `
                Complaint type : ${order.complaint_name}, Order ID: ${order.order_id}, Sku name: ${order.sku_name}, Quantity with issues: ${order.quantity}`
            );
        }, agentMessage);
    }
    if (
        serviceData &&
        serviceData.id &&
        selectedServiceIssues &&
        selectedServiceIssues.length > 0
    ) {
        requestBody[serviceData.id] = selectedServiceIssues;
        agentMessage = selectedServiceIssues.reduce(
            (acc, issue) => {
                return acc + `"${issue.name}", `;
            },
            `${agentMessage}
        Service related issues: `
        );
    }
    if (
        notesData &&
        notesData.id &&
        notesData.expand_template[0] &&
        notesData.expand_template[0].id &&
        notes
    ) {
        requestBody[notesData.id] = [
            {
                complaint_id: notesData.expand_template[0].id,
                notes,
            },
        ];
        agentMessage =
            agentMessage +
            `
        Additional notes: "${notes}"`;
    }

    if (uploadedImages && uploadedImages.length > 0) {
        agentMessage =
            agentMessage +
            `
        Uploaded Images: "${uploadedImages}"`;
    }

    return { requestBody, agentMessage };
}

export function isAllOrdersSelected(
    orderRelatedIssues: SELFSERVE.OrderSelfServe[]
) {
    let bool = true;
    let isSize = false;
    orderRelatedIssues.forEach((value) => {
        if (value && !value.disabled) {
            const complaint =
                value.dropdownList &&
                value.dropdownList.length > 0 &&
                value.dropdownList.find((complaint) => complaint.selected);
            bool =
                bool &&
                value.selected &&
                value.quantity === value.maxQuantity &&
                complaint &&
                complaint.id &&
                complaint.id === 12;
            isSize = true;
        }
    });
    return bool && isSize;
}
