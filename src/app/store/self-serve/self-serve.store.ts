import { Injectable } from "@angular/core";
import { observable, computed } from "mobx";
import SelfServeActions from "./self-serve.actions";
import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { SELFSERVE } from "@shared/models";

@Injectable({
    providedIn: "root",
})
export class SelfServeStore extends SelfServeActions {
    constructor(
        public apiService: ApiService,
        private utilService: UtilService,
        public analyticsService: AnalyticsService
    ) {
        super();
    }

    interaction_id: string;

    @observable
    orderRelatedIssues = new Map();

    @observable
    isOndSelected: boolean = false;

    @observable
    selectedServiceIssues: SELFSERVE.Node_TYPE[] = [];

    @observable
    notes: string = "";

    @observable
    date: string = "";

    @observable
    slot: string = "";

    @observable
    serviceData: SELFSERVE.Node_TYPE;

    @observable
    orderData: SELFSERVE.Node_TYPE;

    @observable
    notesData: SELFSERVE.Node_TYPE;

    @observable
    agentMessage: string = ``;

    @observable
    quantityData: SELFSERVE.Node_TYPE;

    @observable
    isAgentTransfer: boolean = false;

    @observable
    errorResponse: any = null;

    @observable
    uploadedImages: string[] = [];

    @observable
    ImageUploadOrderIds: number[] = [];

    /*
        Derived State
     */

    @computed
    get notesComputedData() {
        const { notesData } = this;
        if (
            !this.utilService.isEmpty(notesData) &&
            this.utilService.isLengthyArray(notesData.expand_template)
        ) {
            const { name, description, expand_template } = notesData;
            return {
                name,
                description,
                childName: expand_template[0] && expand_template[0].name,
            };
        }
        return null;
    }

    @computed
    get serviceComputedData() {
        const { name, description, expand_template } = this.serviceData;

        return {
            name,
            description,
            expand_template,
            selectedServiceIssues: this.selectedServiceIssues,
        };
    }

    @computed
    get selectedOrdersWithIssue() {
        const selectedOrders = [];
        this.orderRelatedIssues.forEach((value) => {
            if (value.selected && !value.disabled) selectedOrders.push(value);
        });
        return selectedOrders;
    }

    @computed
    get selectedOrdersForInstumentation() {
        const selectedOrders = [];
        this.orderRelatedIssues.forEach((value) => {
            if (value.selected && !value.disabled) {
                const {
                    order_id,
                    sku_name,
                    feedbackParamsObject,
                    quantity,
                    dropdownList,
                } = value;
                const complaint =
                    dropdownList &&
                    dropdownList.find((complaint) => complaint.selected);
                selectedOrders.push({
                    order_id,
                    quantity,
                    sku_name,
                    feedbackParamsObject,
                    complaint: (complaint && complaint.value) || "",
                });
            }
        });
        return selectedOrders;
    }

    @computed
    get imageUploadOrders() {
        const orders =
            this.utilService.isLengthyArray(this.ImageUploadOrderIds) &&
            this.ImageUploadOrderIds.map((id) => {
                return this.getMap(id);
            });
        return orders;
    }

    @computed
    get isComplaint() {
        const { selectedOrdersWithIssue, selectedServiceIssues } = this;
        return (
            this.utilService.isLengthyArray(selectedOrdersWithIssue) ||
            this.utilService.isLengthyArray(selectedServiceIssues)
        );
    }

    @observable
    faqList: SELFSERVE.STATIC_NODE[] = [];

    @observable
    staticData: SELFSERVE.STATIC_NODE;
}
