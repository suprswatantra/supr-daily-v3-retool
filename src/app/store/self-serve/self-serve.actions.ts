import { action } from "mobx";

import { Injectable } from "@angular/core";

import { finalize, take } from "rxjs/operators";

import { SuprApi } from "@types";
import * as SelfServeUtils from "./self-serve.utils";
import { SELFSERVE, Order, Feedback } from "@shared/models";
import { Subscription } from "rxjs";

import { SA_OBJECT_NAMES } from "@pages/support/constants/analytics.constants";

@Injectable()
export default class SelfServeActions {
    [x: string]: any;
    constructor() {}

    subscription$: Subscription;

    @action
    toggleFeedbackParams(param: string, id: number) {
        const order = this.getMap(id);
        if (order) {
            const { feedbackParamsObject } = order;
            const value = feedbackParamsObject[param];
            const feedbackObject = {
                ...feedbackParamsObject,
                [param]: !value,
            };
            this.setAttr(id, {
                feedbackParamsObject: feedbackObject,
                error: "",
            });
        }
    }

    @action
    setDateSlot(date: string, slot: string) {
        const interaction_id = this.utilService.randomAlphaNumeric();
        this.analyticsService.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_ENTRY,
            objectValue: JSON.stringify({ date, slot, interaction_id }),
        });
        Object.assign(this, {
            date,
            slot,
            selectedServiceIssues: [],
            notes: "",
            serviceData: {},
            notesData: {},
            orderData: {},
            isAgentTransfer: false,
            errorResponse: null,
            interaction_id,
            uploadedImages: [],
            isOndSelected: false,
        });
        this.clearMap();
    }

    @action
    setQuantity(direction: number, id: number) {
        const order = this.getMap(id);
        if (order) {
            const { quantity = 1, maxQuantity } = order;

            let qty: number = quantity;

            if (direction > 0 && quantity < maxQuantity) {
                qty = quantity + 1;
            } else if (direction < 0 && quantity >= 1) {
                qty = quantity - 1;
            }
            if (quantity !== qty) {
                let update = {};
                if (qty === 0) {
                    Object.assign(update, {
                        selected: false,
                        isOrderDetails: false,
                    });
                } else {
                    Object.assign(update, {
                        quantity: qty,
                    });
                }

                this.setAttr(id, update);
            }
        }
    }

    @action
    setIssue(issue: string, id: number) {
        const order = this.getMap(id);
        if (order) {
            const { dropdownList } = order;
            const list =
                this.utilService.isLengthyArray(dropdownList) &&
                dropdownList.map((complaint) => {
                    if (complaint.value === issue) {
                        complaint.selected = true;
                        this.getOrderAttributes({
                            node_id: complaint.id,
                            order_date: this.date,
                            order_id: id,
                            interaction_id: this.interaction_id,
                        });
                    } else {
                        complaint.selected = false;
                    }
                    return complaint;
                });
            this.setAttr(id, {
                dropdownList: list,
                feedbackParams: [],
                feedback_request_id: null,
            });
        }
    }

    @action
    setNotes(notes: string) {
        Object.assign(this, { notes });
    }

    @action
    setSelectedOrders(order: SELFSERVE.OrderSelfServe) {
        const { selected, selectedItem } = order;
        const { id: node_id, orderData } = selectedItem;
        if (selected) {
            this.getOrderAttributes({
                node_id,
                order_date: this.date,
                order_id: orderData.id,
                interaction_id: this.interaction_id,
            });
        } else {
            this.deselectOnd();
        }
        this.setAttr(orderData.id, {
            orderData,
            selected,
            isOrderDetails: false,
        });
    }

    deselectIssue(id: number) {
        this.setAttr(id, {
            selected: false,
            isOrderDetails: false,
        });
    }

    @action
    deselectOnd() {
        const { isOndSelected } = this;
        if (isOndSelected) {
            this.setOrderRelatedIssues(this.orderData);
            Object.assign(this, { isOndSelected: false });
        }
    }

    @action
    deselectSelectedOrders(id: number) {
        const data = this.orderRelatedIssues.get(id);
        if (
            !this.utilService.isEmpty(data) &&
            this.utilService.isLengthyArray(data.dropdownList)
        ) {
            const selected = data.dropdownList.find(
                (complaint) => complaint.selected
            );
            if (!selected) {
                Object.assign(this, {
                    isOndSelected: false,
                });
                this.setAttr(id, {
                    selected: false,
                    isOrderDetails: false,
                });
            }
        }
    }

    @action
    setServiceIssue(issue) {
        const { selectedServiceIssues } = this;
        const { selected, selectedItem } = issue;
        let selectedIssues = [];
        if (selected) {
            selectedIssues = [
                ...selectedServiceIssues,
                { ...selectedItem, complaint_id: selectedItem.id },
            ];
        } else {
            selectedIssues =
                selectedServiceIssues &&
                selectedServiceIssues.filter(
                    (issue) => issue.id !== selectedItem.id
                );
        }
        Object.assign(this, {
            selectedServiceIssues: selectedIssues,
        });
    }

    @action
    setAttr(id: number, param: any) {
        const data = this.orderRelatedIssues.get(id);
        this.orderRelatedIssues.set(id, {
            ...data,
            ...param,
        });
    }

    @action
    setMapAttr(param: any = {}) {
        this.selectedOrdersWithIssue.forEach((value) => {
            this.setAttr(value.order_id, param);
        });
    }

    @action
    getMap(id: number) {
        return this.orderRelatedIssues.get(id);
    }

    @action
    clearMap() {
        this.orderRelatedIssues.clear();
    }

    @action
    setSelfServeFlowData(data: any, request?: SuprApi.SelfServeRequest) {
        data &&
            data.templates &&
            data.templates.forEach((component) => {
                switch (component && component.id) {
                    case 2:
                        Object.assign(this, { orderData: component });
                        this.setOrderRelatedIssues(component);
                        break;
                    case 3:
                        Object.assign(this, { serviceData: component });
                        break;
                    case 4:
                        Object.assign(this, {
                            notesData: component,
                        });
                        break;
                    case 11:
                        this.setComplaintTypes(component, request);
                        break;
                    case 17:
                        this.setMaxQuantity(component, request);
                        break;
                    case 19:
                        this.setFeedbackParams(component, request);
                        break;
                    default:
                        break;
                }
            });
    }

    @action
    setComplaintTypes(
        component: SELFSERVE.Node_TYPE,
        request: SuprApi.SelfServeRequest
    ) {
        const dropdownList =
            !this.utilService.isEmpty(component) &&
            this.utilService.isLengthyArray(component.expand_template) &&
            component.expand_template.map((issue) => {
                const { name = "", disabled = false, complaint_id, id } = issue;
                const label = disabled
                    ? `${name} (Issue id #${complaint_id} already exists)`
                    : name;
                return {
                    label,
                    value: name,
                    selected: false,
                    disabled: disabled,
                    id,
                };
            });
        this.setAttr(request.order_id, { dropdownList });
    }

    @action
    setMaxQuantity(
        component: SELFSERVE.Node_TYPE,
        request: SuprApi.SelfServeRequest
    ) {
        const maxQuantity = component && component.quantity;
        Object.assign(this, { quantityData: component });
        this.setAttr(request.order_id, { maxQuantity });
    }

    @action
    setFeedbackParams(
        component: SELFSERVE.Node_TYPE,
        request: SuprApi.SelfServeRequest
    ) {
        const { feedback_params, feedback_request_id } = component;
        this.setAttr(request.order_id, {
            feedbackParams: feedback_params,
            feedback_request_id,
        });
    }

    @action
    setOnd(ond: SELFSERVE.OrderSelfServe, orderHistory) {
        const { selected } = ond;
        this.clearMap();
        this.setOndOrderRelatedIssues(this.orderData, orderHistory, selected);
        Object.assign(this, { isOndSelected: selected });
    }

    @action
    setOrderRelatedIssues(component: SELFSERVE.Node_TYPE) {
        const { expand_template } = component;
        expand_template.forEach((order) => {
            this.orderRelatedIssues.set(order.order_id, {
                ...order,
                selected: false,
                isOrderDetails: false,
                feedbackParamsObject: {},
                quantity: 1,
                issue: "",
                error: "",
            });
        });
    }

    @action
    setOndOrderRelatedIssues(
        component: SELFSERVE.Node_TYPE,
        orderHistory: Order[],
        selected: boolean
    ) {
        const { expand_template } = component;
        expand_template.forEach((order) => {
            const orderData = orderHistory.find(
                (historyOrder) => historyOrder.id === order.order_id
            );
            this.orderRelatedIssues.set(order.order_id, {
                ...order,
                selected: order.disabled ? false : selected,
                isOrderDetails: false,
                feedbackParamsObject: {},
                quantity: 1,
                issue: "",
                orderData,
            });
        });
    }

    @action
    validateOrders() {
        const { selectedOrdersWithIssue, isOndSelected } = this;
        if (isOndSelected) {
            return false;
        }
        const erroredOrders =
            this.utilService.isLengthyArray(selectedOrdersWithIssue) &&
            selectedOrdersWithIssue.filter((order) => {
                const selectedIssueType =
                    this.utilService.isLengthyArray(order.dropdownList) &&
                    order.dropdownList.find((issueType) => issueType.selected);

                const isParams = this.validateParams(
                    order.feedbackParamsObject,
                    order.feedbackParams,
                    order.isOrderDetails
                );
                if (selectedIssueType && !isParams) {
                    return false;
                }
                return true;
            });
        this.setErrorMessage(erroredOrders);
        this.checkForOnd();
        return erroredOrders.length > 0;
    }

    @action
    validateParams(
        feedbackParamsObject: Feedback.ParamsObject,
        feedbackParams: string[],
        isOrderDetails: boolean
    ) {
        if (isOrderDetails && feedbackParams && feedbackParams.length > 0) {
            if (this.utilService.isEmpty(feedbackParamsObject)) {
                return true;
            }
            const feedback_params = [];
            for (const [key, val] of Object.entries(feedbackParamsObject)) {
                if (val) {
                    feedback_params.push(key);
                }
            }

            return !this.utilService.isLengthyArray(feedback_params);
        }
        return false;
    }

    @action
    setErrorMessage(orders: SELFSERVE.OrderSelfServe[]) {
        this.utilService.isLengthyArray(orders) &&
            orders.forEach((order) => {
                this.setAttr(order.order_id, {
                    error: "This field is a mandatory field",
                });
            });
    }

    @action
    selfServeGet() {
        const selfServeFetchRequest = {
            node_id: 1,
            order_date: this.date,
            time_slot: this.slot,
            interaction_id: this.interaction_id,
        };
        this.analyticsService.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_TRIGGER,
            objectValue: JSON.stringify(selfServeFetchRequest),
        });
        this.subscription$ = this.apiService
            .selfServeGet(selfServeFetchRequest)
            .pipe(take(1))
            .pipe(
                finalize(() => {
                    this.unsubscribeApi();
                })
            )
            .subscribe(
                (data: any) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_RESPONSE,
                        objectValue: JSON.stringify(data),
                    });
                    this.setSelfServeFlowData(data, selfServeFetchRequest);
                },
                (error) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_ERROR,
                        objectValue: JSON.stringify(error),
                    });
                    this.handleError(error);
                }
            );
    }

    @action
    getOrderAttributes(selfServeRequest: SuprApi.SelfServeRequest) {
        this.analyticsService.trackImpression({
            objectName: SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_TRIGGER,
            objectValue: JSON.stringify(selfServeRequest),
        });
        this.subscription$ = this.apiService
            .selfServeGet(selfServeRequest)
            .pipe(take(1))
            .pipe(
                finalize(() => {
                    this.unsubscribeApi();
                })
            )
            .subscribe(
                (data: any) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_RESPONSE,
                        objectValue: JSON.stringify(data),
                    });
                    this.setSelfServeFlowData(data, selfServeRequest);
                    this.setAttr(selfServeRequest.order_id, {
                        isOrderDetails: true,
                    });
                },
                (error) => {
                    this.analyticsService.trackImpression({
                        objectName:
                            SA_OBJECT_NAMES.IMPRESSION.SELF_SERVE_API_ERROR,
                        objectValue: JSON.stringify(error),
                    });
                    this.handleError(error);
                }
            );
    }

    @action
    complaintCreation() {
        const complaintCreationRequest = this.selfServeRequestData();
        Object.assign(this, {
            agentMessage: complaintCreationRequest.agentMessage,
        });
        return this.apiService.complaintCreation(
            complaintCreationRequest.requestBody
        );
    }

    @action
    handleError(error) {
        Object.assign(this, { isAgentTransfer: true, errorResponse: error });
    }

    @action
    checkForOnd() {
        const isOnd = SelfServeUtils.isAllOrdersSelected(
            this.orderRelatedIssues
        );
        const { orderData } = this;

        if (
            isOnd &&
            orderData &&
            orderData.ond_template &&
            orderData.ond_template.is_ond
        ) {
            Object.assign(this, { isOndSelected: true });
            this.setMapAttr({
                isOrderDetails: false,
                selected: true,
            });
        }
    }

    @action
    setUploadedImages(image: string) {
        const { uploadedImages } = this;
        uploadedImages.push(image);
        Object.assign(this, { uploadedImages });
    }

    @action
    deleteUploadedImages(image: string) {
        const { uploadedImages } = this;
        const updatedImages = uploadedImages.filter((img) => image !== img);
        Object.assign(this, { uploadedImages: updatedImages });
    }

    @action
    imageUpload(file: File) {
        return this.apiService.imageUpload(file, this.interaction_id);
    }

    @action
    isImageUpload() {
        const complaintCreationRequest = this.selfServeRequestData();
        return this.apiService.isImageUpload(
            complaintCreationRequest.requestBody
        );
    }

    @action
    selfServeRequestData() {
        const {
            orderData,
            notesData,
            serviceData,
            orderRelatedIssues,
            isOndSelected,
            selectedServiceIssues,
            notes,
            date,
            slot,
            interaction_id,
            uploadedImages,
        } = this;
        return SelfServeUtils.complaintCreationRequest({
            orderData,
            notesData,
            serviceData,
            orderRelatedIssues,
            isOndSelected,
            selectedServiceIssues,
            notes,
            date,
            slot,
            interaction_id,
            uploadedImages,
        });
    }

    @action
    setImageUploadIds(ImageUploadOrderIds: number[]) {
        Object.assign(this, { ImageUploadOrderIds });
    }

    getOrders(toDate: string, days: number) {
        return this.apiService.fetchOrderHistory(toDate, days);
    }

    // static flows

    getChatRoutingData() {
        return this.apiService.getChatRoutingData();
    }

    @action
    getSelfServeStaticFlows() {
        return this.apiService.fetchSelfServeStaticFlows();
    }

    @action
    setStaticData(data: SELFSERVE.STATIC_NODE) {
        Object.assign(this, { staticData: data });
    }

    @action
    setFaqById(id: number) {
        const { staticData } = this;
        const nav =
            staticData &&
            staticData.expand_template &&
            staticData.expand_template.find((faq) => faq.id === id);
        if (nav) {
            Object.assign(this, { faqList: nav.expand_template });
        }
    }

    private unsubscribeApi() {
        if (this.subscription$) {
            this.subscription$.unsubscribe();
            this.subscription$ = null;
        }
    }
}
