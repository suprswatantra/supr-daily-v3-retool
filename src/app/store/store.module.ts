import { SuprPassStoreModule } from "./supr-pass/supr-pass-store.module";
import { ReferralStoreModule } from "./referral/referral-store.module";
import { RewardsStoreModule } from "./rewards/rewards-store.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { WalletStoreModule } from "./wallet";
import { AuthStoreModule } from "./auth";
import { UserStoreModule } from "./user";
import { AddressStoreModule } from "./address";
import { SkuStoreModule } from "./sku";
import { ScheduleStoreModule } from "./schedule";
import { SubscriptionStoreModule } from "./subscription";
import { CategoryStoreModule } from "./category/category-store.module";
import { EssentialsStoreModule } from "./essentials/essentials-store.module";
import { FeaturedStoreModule } from "./featured/featured-store.module";
import { CollectionStoreModule } from "./collection/collection-store.module";
import { VacationStoreModule } from "./vacation/vacation-store.module";
import { CartStoreModule } from "./cart/cart-store.module";
import { MiscStoreModule } from "./misc/misc-store.module";
import { PnStoreModule } from "./pn/pn-store.module";
import { FeedbackStoreModule } from "./feedback/feedback-store.module";
import { RouterStoreModule } from "./router/router-store.module";
import { OrderStoreModule } from "./order/order-store.module";
import { AddonStoreModule } from "./addon";
import { DeliveryStatusStoreModule } from "./delivery-status/delivery-status-store.module";
import { ErrorStoreModule } from "./error/error-store.module";
import { RatingStoreModule } from "./rating/rating-store.module";
import { HomeLayoutStoreModule } from "./home-layout";
import { SearchStoreModule } from "./search";
import { MileStoneStoreModule } from "./milestone";
import { OffersStoreModule } from "./offers";
import { DeliveryProofStoreModule } from "./delivery-proof/delivery-proof-store.module";

@NgModule({
    imports: [
        CommonModule,
        WalletStoreModule,
        UserStoreModule,
        AuthStoreModule,
        AddressStoreModule,
        SkuStoreModule,
        ScheduleStoreModule,
        SubscriptionStoreModule,
        CategoryStoreModule,
        EssentialsStoreModule,
        FeaturedStoreModule,
        CollectionStoreModule,
        HomeLayoutStoreModule,
        VacationStoreModule,
        CartStoreModule,
        MiscStoreModule,
        PnStoreModule,
        FeedbackStoreModule,
        OrderStoreModule,
        RouterStoreModule,
        AddonStoreModule,
        DeliveryStatusStoreModule,
        ErrorStoreModule,
        RatingStoreModule,
        SuprPassStoreModule,
        ReferralStoreModule,
        RewardsStoreModule,
        SearchStoreModule,
        MileStoneStoreModule,
        OffersStoreModule,
        DeliveryProofStoreModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
        }),
    ],
    declarations: [],
    providers: [],
})
export class RootStoreModule {}
