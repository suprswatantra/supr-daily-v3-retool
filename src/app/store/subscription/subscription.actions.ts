import { Action } from "@ngrx/store";
import { Subscription, SubscriptionTransaction } from "app/shared/models";
import { SuprApi } from "app/types";

export enum ActionTypes {
    FETCH_SUBSCRIPTION_LIST_REQUEST_ACTION = "[Subscription] Fetch subscription request",
    FETCH_SUBSCRIPTION_LIST_REQUEST_SUCCESS_ACTION = "[Subscription] Fetch subscription request success",
    FETCH_SUBSCRIPTION_LIST_REQUEST_FAILURE_ACTION = "[Subscription] Fetch subscription request failure",

    UPDATE_SUBSCRIPTION_REQUEST_ACTION = "[Subscription] Update subscription request",
    UPDATE_SUBSCRIPTION_REQUEST_SUCCESS_ACTION = "[Subscription] Update subscription request success",
    UPDATE_SUBSCRIPTION_REQUEST_FAILURE_ACTION = "[Subscription] Update subscription request failure",

    CANCEL_SUBSCRIPTION_REQUEST_ACTION = "[Subscription] Cancel subscription request",
    CANCEL_SUBSCRIPTION_REQUEST_SUCCESS_ACTION = "[Subscription] Cancel subscription request success",
    CANCEL_SUBSCRIPTION_REQUEST_FAILURE_ACTION = "[Subscription] Cancel subscription request failure",

    UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_ACTION = "[Subscription] Update subscription instruction request",
    UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_SUCCESS_ACTION = "[Subscription] Update subscription instruction request success",
    UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_FAILURE_ACTION = "[Subscription] Update subscription instruction request failure",

    FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_ACTION = "[Subscription] Fetch subscription transactions request",
    FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_COMPLETE_ACTION = "[Subscription] Fetch subscription transactions request completion",
}

export class FetchSubscriptionListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_ACTION;
}

export class FetchSubscriptionListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload?: Subscription[]) {}
}

export class FetchSubscriptionListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class UpdateSubscriptionRequestAction implements Action {
    readonly type = ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_ACTION;
    constructor(
        public payload: {
            subscriptionId: number;
            body: SuprApi.SubscriptionBody;
            instantRefreshSchedule?: boolean;
        }
    ) {}
}

export class UpdateSubscriptionRequestSuccessAction implements Action {
    readonly type = ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_SUCCESS_ACTION;
    constructor(public payload: { subscription: Subscription }) {}
}

export class UpdateSubscriptionRequestFailureAction implements Action {
    readonly type = ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class CancelSubscriptionRequestAction implements Action {
    readonly type = ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_ACTION;
    constructor(public payload: { subscriptionId: number }) {}
}

export class CancelSubscriptionRequestSuccessAction implements Action {
    readonly type = ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_SUCCESS_ACTION;
    constructor(public payload: { subscriptionId: number }) {}
}

export class CancelSubscriptionRequestFailureAction implements Action {
    readonly type = ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class UpdateSubscriptionInstructionRequestAction implements Action {
    readonly type = ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_ACTION;
    constructor(
        public payload: {
            subscriptionId: number;
            body: SuprApi.SubscriptionInstructionBody;
            instantRefreshSchedule?: boolean;
        }
    ) {}
}

export class UpdateSubscriptionInstructionRequestSuccessAction
    implements Action {
    readonly type =
        ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_SUCCESS_ACTION;
}

export class UpdateSubscriptionInstructionRequestFailureAction
    implements Action {
    readonly type =
        ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchSubTransactionRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_ACTION;
    constructor(public payload: number) {}
}

export class FetchSubTransactionRequestCompleteAction implements Action {
    readonly type =
        ActionTypes.FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_COMPLETE_ACTION;
    constructor(public payload: SubscriptionTransaction[]) {}
}

export type Actions =
    | FetchSubscriptionListRequestAction
    | FetchSubscriptionListRequestSuccessAction
    | FetchSubscriptionListRequestFailureAction
    | UpdateSubscriptionRequestAction
    | UpdateSubscriptionRequestSuccessAction
    | UpdateSubscriptionRequestFailureAction
    | CancelSubscriptionRequestAction
    | CancelSubscriptionRequestSuccessAction
    | CancelSubscriptionRequestFailureAction
    | UpdateSubscriptionInstructionRequestAction
    | UpdateSubscriptionInstructionRequestSuccessAction
    | UpdateSubscriptionInstructionRequestFailureAction
    | FetchSubTransactionRequestAction
    | FetchSubTransactionRequestCompleteAction;
