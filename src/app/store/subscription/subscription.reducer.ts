import { Actions, ActionTypes } from "./subscription.actions";
import {
    initialState,
    SubscriptionState,
    SubscriptionCurrentState,
} from "./subscription.state";

import { Subscription } from "@models";
import { SubscriptionDict, SkuDict } from "@types";

export function subscriptionReducer(
    state = initialState,
    action: Actions
): SubscriptionState {
    switch (action.type) {
        case ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.FETCHING_SUBSCRIPTIONS,
                error: null,
            };
        }

        case ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: SubscriptionCurrentState.NO_ACTION,
                subscriptionList: action.payload || [],
                subscriptionDict: convertSubscriptionsListToObj(action.payload),
                skuDict: getSkuDictionaryFromSubscriptions(action.payload),
            };
        }

        case ActionTypes.FETCH_SUBSCRIPTION_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: SubscriptionCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.UPDATING_SUBSCRIPTION,
                error: null,
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
                ...updateSubscription(state, action.payload.subscription),
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_ACTION: {
            return {
                ...state,
                currentState:
                    SubscriptionCurrentState.UPDATING_SUBSCRIPTION_INSTRUCTION,
                error: null,
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.CANCELLING_SUBSCRIPTION,
                error: null,
            };
        }

        case ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
                ...deleteSubscription(state, action.payload.subscriptionId),
            };
        }

        case ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: SubscriptionCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_ACTION: {
            return {
                ...state,
                transactions: null,
                currentState:
                    SubscriptionCurrentState.FETCHING_SUB_TRANSACTIONS,
            };
        }

        case ActionTypes.FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_COMPLETE_ACTION: {
            return {
                ...state,
                transactions: action.payload,
                currentState:
                    SubscriptionCurrentState.FETCHING_SUB_TRANSACTIONS_DONE,
            };
        }

        default: {
            return state;
        }
    }
}

/*******************************************
 *         Helper methods                  *
 *******************************************/

function convertSubscriptionsListToObj(
    subscriptions: Subscription[]
): SubscriptionDict {
    if (subscriptions && subscriptions.length) {
        return subscriptions.reduce((acc, subscription) => {
            acc[subscription.id] = subscription;
            return acc;
        }, {});
    }

    return {};
}

function deleteSubscription(
    state: SubscriptionState,
    subId: number
): SubscriptionState {
    const currentList = state.subscriptionList;
    const subscriptionList = currentList.filter(sub => sub.id !== subId);

    const subscriptionDict = { ...state.subscriptionDict };
    delete subscriptionDict[subId];

    return { subscriptionList, subscriptionDict };
}

function updateSubscription(
    state: SubscriptionState,
    subscription: Subscription
): SubscriptionState {
    const currentList = state.subscriptionList;
    const subscriptionList = currentList.map(sub => {
        if (sub.id === subscription.id) {
            return subscription;
        } else {
            return sub;
        }
    });

    const subscriptionDict = { ...state.subscriptionDict };
    subscriptionDict[subscription.id] = subscription;

    return { subscriptionList, subscriptionDict };
}

function getSkuDictionaryFromSubscriptions(
    subscriptions: Subscription[]
): SkuDict {
    if (!Array.isArray(subscriptions)) {
        return {};
    }

    const skuDict = {};

    subscriptions.forEach((subscription: Subscription) => {
        if (subscription.sku_data) {
            skuDict[subscription.sku_data.id] = subscription.sku_data;
        }
    });

    return skuDict;
}
