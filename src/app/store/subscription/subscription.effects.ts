import { Injectable } from "@angular/core";

import { Action, Store, select } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, of as observableOf } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    withLatestFrom,
    filter,
} from "rxjs/operators";

import { Subscription, SubscriptionTransaction } from "@models";

import { ApiService } from "@services/data/api.service";

import { StoreState } from "@store/store.state";
import { SearchStoreActions } from "@store/search";
import { ScheduleStoreActions } from "@store/schedule";
import { CollectionStoreActions } from "@store/collection";
import { WalletStoreActions as walletActions } from "@store/wallet";
import { DeliveryStatusStoreActions } from "@store/delivery-status";
import * as UserStoreSelectors from "@store/user/user.selectors";

import * as subscriptionActions from "./subscription.actions";
import * as SubscriptionSelectors from "./subscription.selectors";

@Injectable()
export class SubscriptionStoreEffects {
    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private store$: Store<StoreState>
    ) {}

    @Effect()
    fetchSubscriptions$: Observable<Action> = this.actions$.pipe(
        ofType<subscriptionActions.FetchSubscriptionListRequestAction>(
            subscriptionActions.ActionTypes
                .FETCH_SUBSCRIPTION_LIST_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.isAnonymousUser))
        ),
        filter(([_action, isAnonymousUser]) => isAnonymousUser === false),
        switchMap(() =>
            this.apiService.fetchSubscriptions().pipe(
                map(
                    (subscriptionsList: Subscription[]) =>
                        new subscriptionActions.FetchSubscriptionListRequestSuccessAction(
                            subscriptionsList
                        )
                ),

                catchError((error) =>
                    observableOf(
                        new subscriptionActions.FetchSubscriptionListRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchSubscriptionTransactions$: Observable<Action> = this.actions$.pipe(
        ofType<subscriptionActions.FetchSubTransactionRequestAction>(
            subscriptionActions.ActionTypes
                .FETCH_SUBSCRIPTION_TRANSACTION_REQUEST_ACTION
        ),
        switchMap((action) =>
            this.apiService.fetchSubscriptionTransactions(action.payload).pipe(
                map((transactions: SubscriptionTransaction[]) => {
                    const reversed = Array.isArray(transactions)
                        ? transactions.reverse()
                        : null;
                    return new subscriptionActions.FetchSubTransactionRequestCompleteAction(
                        reversed
                    );
                }),

                catchError((_) =>
                    observableOf(
                        new subscriptionActions.FetchSubTransactionRequestCompleteAction(
                            null
                        )
                    )
                )
            )
        )
    );

    @Effect()
    updateSubscription$: Observable<Action> = this.actions$.pipe(
        ofType<subscriptionActions.UpdateSubscriptionRequestAction>(
            subscriptionActions.ActionTypes.UPDATE_SUBSCRIPTION_REQUEST_ACTION
        ),
        switchMap((action) => {
            const {
                subscriptionId,
                body,
                instantRefreshSchedule,
            } = action.payload;
            return this.apiService
                .updateSubscription(subscriptionId, body)
                .pipe(
                    switchMap((subscription: Subscription) => {
                        const actions = [
                            new subscriptionActions.UpdateSubscriptionRequestSuccessAction(
                                { subscription }
                            ),
                            new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                        ];

                        if (instantRefreshSchedule) {
                            return [
                                ...actions,
                                new ScheduleStoreActions.GetScheduleDataAction({
                                    subscriptionId,
                                }),
                            ];
                        }
                        return [
                            ...actions,
                            new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                        ];
                    }),

                    catchError((error) =>
                        observableOf(
                            new subscriptionActions.UpdateSubscriptionRequestFailureAction(
                                error
                            )
                        )
                    )
                );
        })
    );

    @Effect()
    updateSubscriptionInstruction$: Observable<Action> = this.actions$.pipe(
        ofType<subscriptionActions.UpdateSubscriptionInstructionRequestAction>(
            subscriptionActions.ActionTypes
                .UPDATE_SUBSCRIPTION_INSTRUCTION_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(
                select(SubscriptionSelectors.selectSubscriptionDict)
            )
        ),
        switchMap(([action, subscriptionDict]) => {
            const {
                subscriptionId,
                body,
                instantRefreshSchedule,
            } = action.payload;
            const subscription = subscriptionDict[subscriptionId];
            const skuId = subscription && subscription.sku_id;

            return this.apiService
                .updateSubscriptionInstruction(subscriptionId, body)
                .pipe(
                    switchMap(() => {
                        const actions = [
                            new subscriptionActions.UpdateSubscriptionInstructionRequestSuccessAction(),
                            new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                            new subscriptionActions.FetchSubscriptionListRequestAction(),
                            new SearchStoreActions.CheckForSubInstrumentationContextAction(
                                skuId
                            ),
                        ];

                        if (instantRefreshSchedule) {
                            return [
                                ...actions,
                                new ScheduleStoreActions.GetScheduleDataAction({
                                    subscriptionId,
                                }),
                            ];
                        }
                        return [
                            ...actions,
                            new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                        ];
                    }),

                    catchError((error) =>
                        observableOf(
                            new subscriptionActions.UpdateSubscriptionInstructionRequestFailureAction(
                                error
                            )
                        )
                    )
                );
        })
    );

    @Effect()
    cancelSubscription$: Observable<Action> = this.actions$.pipe(
        ofType<subscriptionActions.CancelSubscriptionRequestAction>(
            subscriptionActions.ActionTypes.CANCEL_SUBSCRIPTION_REQUEST_ACTION
        ),
        switchMap((action) => {
            const { subscriptionId } = action.payload;
            return this.apiService.cancelSubscription(subscriptionId).pipe(
                switchMap(() => [
                    new subscriptionActions.CancelSubscriptionRequestSuccessAction(
                        { subscriptionId }
                    ),
                    new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                    new walletActions.LoadBalanceRequestAction(),
                    new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                    new CollectionStoreActions.FetchPastOrderSkuListRequestAction(),
                ]),
                catchError((error) =>
                    observableOf(
                        new subscriptionActions.CancelSubscriptionRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );
}
