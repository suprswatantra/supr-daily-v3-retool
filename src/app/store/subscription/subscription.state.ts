import { SubscriptionDict, SkuDict } from "@types";
import { Subscription, SubscriptionTransaction } from "@models";

export enum SubscriptionCurrentState {
    NO_ACTION = "0",
    FETCHING_SUBSCRIPTIONS = "1",
    CREATING_SUBSCRIPTION = "2",
    UPDATING_SUBSCRIPTION = "3",
    CANCELLING_SUBSCRIPTION = "4",
    UPDATING_SUBSCRIPTION_INSTRUCTION = "5",
    CANCELLING_ADDON = "6",
    FETCHING_SUB_TRANSACTIONS = "7",
    FETCHING_SUB_TRANSACTIONS_DONE = "8",
}

export interface SubscriptionState {
    error?: any;
    subscriptionList?: Subscription[];
    subscriptionDict?: SubscriptionDict;
    currentState?: SubscriptionCurrentState;
    transactions?: SubscriptionTransaction[];
    skuDict?: SkuDict;
}

export const initialState: SubscriptionState = {
    error: null,
    subscriptionList: null,
    subscriptionDict: null,
    currentState: SubscriptionCurrentState.NO_ACTION,
    transactions: [],
};
