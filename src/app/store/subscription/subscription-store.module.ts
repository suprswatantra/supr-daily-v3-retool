import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { subscriptionReducer } from "./subscription.reducer";
import { SubscriptionStoreEffects } from "./subscription.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("subscription", subscriptionReducer),
        EffectsModule.forFeature([SubscriptionStoreEffects]),
    ],
    providers: [],
})
export class SubscriptionStoreModule {}
