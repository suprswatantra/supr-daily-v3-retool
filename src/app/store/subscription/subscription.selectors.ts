import { createFeatureSelector, createSelector } from "@ngrx/store";

import { Subscription } from "@models";
import { selectUrlParams } from "@store/router/router.selectors";
import { SubscriptionDict, SkuDict } from "@types";

import { SubscriptionState } from "./subscription.state";

export const selectFeature = createFeatureSelector<SubscriptionState>(
    "subscription"
);

export const selectSubscriptionList = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.subscriptionList
);

export const selectSubscriptionDict = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.subscriptionDict
);

export const selectSubscriptionById = createSelector(
    selectSubscriptionDict,
    (subscriptionDict: SubscriptionDict, props: { subscriptionId: number }) =>
        subscriptionDict && props && props.subscriptionId
            ? subscriptionDict[props.subscriptionId]
            : null
);

export const selectSubscriptionSkuDataById = createSelector(
    selectSubscriptionDict,
    (subscriptionDict: SubscriptionDict, props: { subscriptionId: number }) =>
        subscriptionDict &&
        subscriptionDict[props.subscriptionId] &&
        subscriptionDict[props.subscriptionId].sku_data
);

export const selectSubscriptionByUrlParams = createSelector(
    selectSubscriptionDict,
    selectUrlParams,
    (subscriptionDict, urlParams) =>
        subscriptionDict && subscriptionDict[urlParams["subscriptionId"]]
);

export const selectSubscriptionState = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.currentState
);

export const selectSubscriptionError = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.error
);

export const selectActiveSubscriptionCount = createSelector(
    selectFeature,
    (state: SubscriptionState) =>
        getActiveSubscriptionCount(state.subscriptionList)
);

export const selectSkuDictionary = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.skuDict
);

export const selectSkuDetailsById = createSelector(
    selectSkuDictionary,
    (skuDict: SkuDict, props: { skuId: number }) =>
        skuDict && skuDict[props && props.skuId]
);

export const selectSubscriptionTransactions = createSelector(
    selectFeature,
    (state: SubscriptionState) => state.transactions
);

export const selectActiveSubscriptionBySkuId = createSelector(
    selectSkuDictionary,
    selectSubscriptionList,
    (
        skuDict: SkuDict,
        subscriptions: Subscription[],
        props: { skuId: number }
    ) => {
        if (
            skuDict &&
            skuDict[props.skuId] &&
            subscriptions &&
            subscriptions.length
        ) {
            return subscriptions.find(
                (subscription: Subscription) =>
                    subscription.sku_id === props.skuId &&
                    subscription.balance > 0
            );
        }

        return null;
    }
);

function getActiveSubscriptionCount(list: Subscription[]): number {
    return list ? list.length : 0;
}
