import { Action } from "@ngrx/store";

import { ScheduleApiData } from "@models";

export enum ActionTypes {
    GET_SCHEDULE_DATA = "[Schedule] Get schedule data",
    GET_SCHEDULE_DATA_SUCCESS = "[Schedule] Get schedule success",
    GET_SCHEDULE_DATA_FAILURE = "[Schedule] Get schedule failure",

    SET_REFRESH_SCHEDULE_FLAG = "[Schedule] Set schedule refresh flag",

    UPDATE_SCHEDULE_CALENDAR = "[Schedule] Update schedule calendar",
    UPDATE_SCHEDULE_CALENDAR_SUCCESS = "[Schedule] Update schedule calendar success",
}

export class GetScheduleDataAction implements Action {
    readonly type = ActionTypes.GET_SCHEDULE_DATA;
    constructor(
        public payload: {
            startDate?: string;
            thankYouFlag?: boolean;
            subscriptionId?: number;
            numberOfDays?: number;
        }
    ) {}
}

export class SetRefreshScheduleFlagAction implements Action {
    readonly type = ActionTypes.SET_REFRESH_SCHEDULE_FLAG;
}

export class GetScheduleDataSuccessAction implements Action {
    readonly type = ActionTypes.GET_SCHEDULE_DATA_SUCCESS;
    constructor(public payload: ScheduleApiData) {}
}

export class GetScheduleDataFailureAction implements Action {
    readonly type = ActionTypes.GET_SCHEDULE_DATA_FAILURE;
    constructor(public payload: any) {}
}

export class UpdateScheduleCalendarAction implements Action {
    readonly type = ActionTypes.UPDATE_SCHEDULE_CALENDAR;
    constructor(
        public payload: {
            reset?: boolean;
            history?: boolean;
            startDate?: string;
            numberOfDays: number;
            thankYouFlag?: boolean;
        }
    ) {}
}

export class UpdateScheduleCalendarSuccessAction implements Action {
    readonly type = ActionTypes.UPDATE_SCHEDULE_CALENDAR_SUCCESS;
    constructor(public payload: { dates: string[]; history?: boolean }) {}
}

export type Actions =
    | GetScheduleDataAction
    | GetScheduleDataSuccessAction
    | GetScheduleDataFailureAction
    | SetRefreshScheduleFlagAction
    | UpdateScheduleCalendarAction
    | UpdateScheduleCalendarSuccessAction;
