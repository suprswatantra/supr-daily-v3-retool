import { Schedule, Sku, ScheduleDeliverySlotDict } from "@models";

import { ScheduleDict, ScheduleCalendarDictionary } from "@types";

export enum ScheduleCurrentState {
    NO_ACTION = "0",
    FETCHING_SCHEDULE = "1",
    FETCHING_SCHEDULE_DONE = "2",
    UPDATING_CALENDAR = "3",
    UPDATING_CALENDAR_DONE = "4",
}

export interface ScheduleState {
    error?: any;
    skuData: Sku[];
    message: string;
    calendar: string[];
    refreshSchedule: boolean;
    scheduleList: Schedule[];
    scheduleDict: ScheduleDict;
    currentState?: ScheduleCurrentState;
    calendarDict: ScheduleCalendarDictionary;
    scheduleDeliverySlotDict: ScheduleDeliverySlotDict;
}

export const initialState: ScheduleState = {
    error: null,
    message: "",
    skuData: [],
    calendar: [],
    scheduleList: [],
    scheduleDict: {},
    calendarDict: {},
    refreshSchedule: true,
    scheduleDeliverySlotDict: {},
    currentState: ScheduleCurrentState.NO_ACTION,
};
