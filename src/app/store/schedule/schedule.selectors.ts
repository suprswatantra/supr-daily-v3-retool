import { createFeatureSelector, createSelector } from "@ngrx/store";

import { ScheduleStoreState } from ".";
import { Schedule, Sku } from "@shared/models";
import { selectUrlParams } from "@store/router/router.selectors";

import { ScheduleDict } from "@types";

export const selectFeature = createFeatureSelector<ScheduleStoreState>(
    "schedule"
);

export const selectScheduleList = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.scheduleList
);

export const selectScheduleSkuList = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.skuData
);

export const selectScheduleSku = createSelector(
    selectScheduleSkuList,
    (skuList: Sku[], props: { skuId: number }) =>
        getScheduleSkuById(skuList, props.skuId)
);

export const selectScheduleRefreshState = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.refreshSchedule
);

export const selectScheduleDict = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.scheduleDict
);

export const selectNoSchedulesMessage = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.message
);

export const selectScheduleByDate = createSelector(
    selectScheduleDict,
    (scheduleDict: ScheduleDict, props: { date: string }) =>
        scheduleDict[props.date]
);

export const selectSubscriptionScheduleByUrlParams = createSelector(
    selectScheduleList,
    selectUrlParams,
    (scheduleList: Schedule[], urlParams) =>
        getSubscriptionSchedulesById(
            scheduleList,
            Number(urlParams["subscriptionId"])
        )
);

export const selectScheduleCalendar = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.calendar
);

export const selectScheduleCalendarDictionary = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.calendarDict
);

export const selectScheduleCurrentState = createSelector(
    selectFeature,
    (state: ScheduleStoreState) => state.currentState
);

export const selectScheduleDeliveryByDate = createSelector(
    selectFeature,
    (state: ScheduleStoreState, props: { date: string }) =>
        state.scheduleDeliverySlotDict &&
        state.scheduleDeliverySlotDict[props.date]
);

/* [TODO] Move to a local util */
function getSubscriptionSchedulesById(scheduleList: Schedule[], id: number) {
    const schedules: Schedule[] = [];

    scheduleList.forEach((schedule) => {
        if (schedule.subscriptions) {
            const scheduleSubscription = schedule.subscriptions.find(
                (subscription) => subscription.id === id
            );

            if (scheduleSubscription) {
                schedules.push({
                    date: schedule.date,
                    subscriptions: [scheduleSubscription],
                });
            }
        }
    });

    return schedules;
}

function getScheduleSkuById(skuList: Sku[], skuId: number): Sku {
    return skuList.find((sku) => sku.id === skuId);
}
