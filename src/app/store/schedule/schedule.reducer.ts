import {
    Schedule,
    ScheduleItem,
    ScheduleDeliverySlotItem,
    ScheduleDeliverySlotDict,
} from "@models";

import { ScheduleDict, ScheduleCalendarDictionary } from "@types";

import { Actions, ActionTypes } from "./schedule.actions";

import {
    initialState,
    ScheduleState,
    ScheduleCurrentState,
} from "./schedule.state";

export function scheduleReducer(
    state = initialState,
    action: Actions
): ScheduleState {
    switch (action.type) {
        case ActionTypes.GET_SCHEDULE_DATA: {
            return {
                ...state,
                error: null,
                calendar: [],
                calendarDict: {},
                refreshSchedule: false,
                currentState: ScheduleCurrentState.FETCHING_SCHEDULE,
            };
        }
        case ActionTypes.SET_REFRESH_SCHEDULE_FLAG: {
            return {
                ...state,
                refreshSchedule: true,
            };
        }
        case ActionTypes.GET_SCHEDULE_DATA_SUCCESS: {
            return {
                ...state,
                error: null,
                skuData: action.payload.skuData,
                message: action.payload.message,
                scheduleList: action.payload.schedule,
                scheduleDict: convertListToObj(action.payload.schedule),
                currentState: ScheduleCurrentState.FETCHING_SCHEDULE_DONE,
                scheduleDeliverySlotDict: generateDeliverySlotMap(
                    action.payload.schedule
                ),
            };
        }
        case ActionTypes.GET_SCHEDULE_DATA_FAILURE: {
            return {
                ...state,
                scheduleList: [],
                scheduleDict: {},
                error: action.payload,
                currentState: ScheduleCurrentState.FETCHING_SCHEDULE_DONE,
            };
        }
        case ActionTypes.UPDATE_SCHEDULE_CALENDAR: {
            if (action.payload.reset) {
                return {
                    ...state,
                    calendar: [],
                    calendarDict: {},
                    currentState: ScheduleCurrentState.UPDATING_CALENDAR,
                };
            }

            return {
                ...state,
                currentState: ScheduleCurrentState.UPDATING_CALENDAR,
            };
        }
        case ActionTypes.UPDATE_SCHEDULE_CALENDAR_SUCCESS: {
            const calendar = action.payload.history
                ? [...action.payload.dates, ...state.calendar]
                : [...state.calendar, ...action.payload.dates];

            return {
                ...state,
                calendar,
                calendarDict: getCalendarDictionary(calendar),
                currentState: ScheduleCurrentState.UPDATING_CALENDAR_DONE,
            };
        }
        default: {
            return state;
        }
    }
}

/* [TODO] Move this util function to a central space */
function convertListToObj(scheduleList: any[]): ScheduleDict {
    return scheduleList.reduce((acc, schedule) => {
        acc[schedule.date] = schedule;
        return acc;
    }, {});
}

function getCalendarDictionary(dates: string[]): ScheduleCalendarDictionary {
    return dates.reduce((acc, date) => {
        acc[date] = true;
        return acc;
    }, {});
}

function generateDeliverySlotMap(
    schedules: Schedule[]
): ScheduleDeliverySlotDict {
    const scheduleDeliverySlotDict = {};

    schedules.forEach((schedule: Schedule) => {
        let scheduleDeliverySlotMap: {
            [slot: string]: ScheduleDeliverySlotItem;
        } = {};

        if (schedule.subscriptions && schedule.subscriptions.length) {
            scheduleDeliverySlotMap = schedule.subscriptions.reduce(
                (deliverySlotMap, subscription: ScheduleItem) => {
                    if (
                        !deliverySlotMap[subscription.delivery_type] ||
                        !deliverySlotMap[subscription.delivery_type]
                            .subscriptions
                    ) {
                        deliverySlotMap[subscription.delivery_type] = {
                            subscriptions: [],
                        };
                    }

                    deliverySlotMap[
                        subscription.delivery_type
                    ].subscriptions.push(subscription);

                    return deliverySlotMap;
                },
                {}
            );
        }

        if (schedule.addons && schedule.addons.length) {
            scheduleDeliverySlotMap = schedule.addons.reduce(
                (deliverySlotMap, addon: ScheduleItem) => {
                    if (
                        !deliverySlotMap[addon.delivery_type] ||
                        !deliverySlotMap[addon.delivery_type].addons
                    ) {
                        deliverySlotMap[addon.delivery_type] = {
                            ...deliverySlotMap[addon.delivery_type],
                            addons: [],
                        };
                    }

                    deliverySlotMap[addon.delivery_type].addons.push(addon);

                    return deliverySlotMap;
                },
                scheduleDeliverySlotMap
            );
        }

        scheduleDeliverySlotDict[schedule.date] = {
            date: schedule.date,
            message: schedule.message,
            slot_info: schedule.slot_info,
            delivery_timeline: schedule.delivery_timeline,
            ...scheduleDeliverySlotMap,
        };
    });

    return scheduleDeliverySlotDict;
}
