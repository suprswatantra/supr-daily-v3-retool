import { Injectable } from "@angular/core";

import { Action, select, Store } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, of as observableOf } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    withLatestFrom,
    filter,
} from "rxjs/operators";

import { ApiService } from "@services/data/api.service";
import { DateService } from "@services/date/date.service";
import { CalendarService } from "@services/date/calendar.service";

import { StoreState } from "@store/store.state";
import * as scheduleActions from "./schedule.actions";
import * as CartStoreSelectors from "../cart/cart.selectors";
import * as ScheduleStoreSelectors from "./schedule.selectors";
import * as UserStoreSelectors from "@store/user/user.selectors";

import { Schedule, CartOrder, ScheduleApiData } from "@models";

import { CALENDAR_BASE_SIZE } from "@pages/schedule/constants/schedule.constants";

@Injectable()
export class ScheduleStoreEffects {
    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private dateService: DateService,
        private store$: Store<StoreState>,
        private calendarService: CalendarService
    ) {}

    @Effect()
    fetchSchedule$: Observable<Action> = this.actions$.pipe(
        ofType<scheduleActions.GetScheduleDataAction>(
            scheduleActions.ActionTypes.GET_SCHEDULE_DATA
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.isAnonymousUser))
        ),
        filter(([_action, isAnonymousUser]) => isAnonymousUser === false),
        switchMap(([action]) =>
            this.apiService.fetchSchedule(action.payload.subscriptionId).pipe(
                switchMap((scheduleData: ScheduleApiData) => [
                    new scheduleActions.GetScheduleDataSuccessAction(
                        scheduleData
                    ),
                    new scheduleActions.UpdateScheduleCalendarAction({
                        numberOfDays:
                            action.payload.numberOfDays || CALENDAR_BASE_SIZE,
                        startDate: action.payload.startDate,
                        thankYouFlag: action.payload.thankYouFlag,
                    }),
                ]),
                catchError((error) =>
                    observableOf(
                        new scheduleActions.GetScheduleDataFailureAction(error)
                    )
                )
            )
        )
    );

    @Effect()
    updateScheduleCalendar$: Observable<Action> = this.actions$.pipe(
        ofType<scheduleActions.UpdateScheduleCalendarAction>(
            scheduleActions.ActionTypes.UPDATE_SCHEDULE_CALENDAR
        ),
        withLatestFrom(
            this.store$.pipe(
                select(ScheduleStoreSelectors.selectScheduleCalendar)
            ),
            this.store$.pipe(select(CartStoreSelectors.selectCartOrder)),
            this.store$.pipe(select(ScheduleStoreSelectors.selectScheduleList))
        ),
        map(([action, calendar, cartOrder, scheduleList]) => {
            if (action.payload.history) {
                return this.renderPastDays(
                    action.payload.startDate,
                    action.payload.numberOfDays,
                    calendar
                );
            } else {
                let startDate = action.payload.startDate;

                if (action.payload.thankYouFlag && cartOrder) {
                    startDate = this.getScrollToDateForThankYouFlow(
                        scheduleList,
                        cartOrder
                    );
                }

                return this.renderFutureDays(
                    startDate,
                    action.payload.numberOfDays,
                    calendar
                );
            }
        })
    );

    private getScrollToDateForThankYouFlow(
        scheduleList: Schedule[],
        cartOrder: CartOrder
    ): string {
        for (let i = 0; i < scheduleList.length; i++) {
            if (cartOrder.addons && scheduleList[i].addons) {
                const matchedAddon = scheduleList[i].addons.find(
                    (addon) => cartOrder.addons.indexOf(addon.id) > -1
                );

                if (matchedAddon) {
                    return scheduleList[i].date;
                }
            }

            if (cartOrder.subscriptions && scheduleList[i].subscriptions) {
                const matchedSub = scheduleList[i].subscriptions.find(
                    (sub) => cartOrder.subscriptions.indexOf(sub.id) > -1
                );

                if (matchedSub) {
                    return scheduleList[i].date;
                }
            }
        }

        return null;
    }

    private renderPastDays(
        startDate: string,
        numberOfDays: number,
        calendar: string[]
    ) {
        let fromDate: Date, dates: string[], fromDateText: string;

        if (startDate) {
            fromDateText = startDate;
        } else if (calendar.length > 0) {
            /* Show calendar from one day before last rendered date */
            fromDateText = calendar[0];
            fromDate = this.dateService.dateFromText(fromDateText);
            fromDate = this.dateService.addDays(fromDate, -1);
            fromDateText = this.dateService.textFromDate(fromDate);
        } else {
            /* On first render, show calendar from previous calendar date */
            fromDate = this.calendarService.getLatestOrderDate();
            fromDateText = this.dateService.textFromDate(fromDate);
        }

        dates = this.calendarService.getPastDaysFromDate(
            fromDateText,
            numberOfDays
        );

        return new scheduleActions.UpdateScheduleCalendarSuccessAction({
            history: true,
            dates: dates.reverse(),
        });
    }

    private renderFutureDays(
        startDate: string,
        numberOfDays: number,
        calendar: string[]
    ): Action {
        let dates: string[], fromDateText: string, fromDate: Date;

        if (startDate) {
            fromDateText = startDate;
        } else if (calendar.length > 0) {
            /* Show calendar from one day after last rendered date */
            fromDateText = calendar[calendar.length - 1];
            fromDate = this.dateService.dateFromText(fromDateText);
            fromDate = this.dateService.addDays(fromDate, 1);
            fromDateText = this.dateService.textFromDate(fromDate);
        } else {
            /* On first render, show calendar from next available date */
            fromDate = this.calendarService.getNextAvailableDate();
            fromDateText = this.dateService.textFromDate(fromDate);
        }

        dates = this.calendarService.getFutureDaysFromDate(
            fromDateText,
            numberOfDays
        );

        return new scheduleActions.UpdateScheduleCalendarSuccessAction({
            dates,
        });
    }
}
