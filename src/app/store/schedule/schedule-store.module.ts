import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { scheduleReducer } from "./schedule.reducer";
import { ScheduleStoreEffects } from "./schedule.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("schedule", scheduleReducer),
        EffectsModule.forFeature([ScheduleStoreEffects]),
    ],
    providers: [],
})
export class ScheduleStoreModule {}
