import { Injectable } from "@angular/core";

import { Action } from "@ngrx/store";
import { Observable, of as observableOf } from "rxjs";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, debounceTime, map, switchMap } from "rxjs/operators";

import { DeliveryProof } from "@models";

import { ApiService } from "@services/data/api.service";

import { SuprApi } from "@types";

import * as DeliveryProofActions from "./delivery-proof.actions";

@Injectable()
export class DeliveryProofEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchDeliveryProofsEffect$: Observable<Action> = this.actions$.pipe(
        ofType<DeliveryProofActions.FetchDeliveryProofsAction>(
            DeliveryProofActions.ActionTypes.FETCH_DELIVERY_PROOFS
        ),

        switchMap((action) =>
            this.apiService.fetchDeliveryProof(action.payload).pipe(
                map(
                    (data: DeliveryProof[]) =>
                        new DeliveryProofActions.FetchDeliveryProofsSuccessAction(
                            data
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new DeliveryProofActions.FetchDeliveryProofsFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
    @Effect()
    postDeliveryProofFeedbackEffect$: Observable<Action> = this.actions$.pipe(
        ofType<DeliveryProofActions.PostDeliveryProofFeedbackAction>(
            DeliveryProofActions.ActionTypes.POST_DELIVERY_PROOF_FEEDBACK
        ),
        debounceTime(1000),
        switchMap((action) =>
            this.apiService.sendDeliveryProofFeedback(action.payload).pipe(
                map(
                    (_: SuprApi.ApiRes) =>
                        new DeliveryProofActions.PostDeliveryProofFeedbackSuccessAction()
                ),
                catchError((error) =>
                    observableOf(
                        new DeliveryProofActions.PostDeliveryProofFeedbackFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
    @Effect()
    reportDeliveryProofImagesEffect$: Observable<Action> = this.actions$.pipe(
        ofType<DeliveryProofActions.ReportDeliveryProofImagesAction>(
            DeliveryProofActions.ActionTypes.REPORT_DELIVERY_PROOF_IMAGES
        ),
        switchMap((action) =>
            this.apiService.reportDeliveryProofImage(action.payload).pipe(
                map(
                    (_: SuprApi.ApiRes) =>
                        new DeliveryProofActions.ReportDeliveryProofImagesSuccessAction()
                ),
                catchError((error) =>
                    observableOf(
                        new DeliveryProofActions.ReportDeliveryProofImagesFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
}
