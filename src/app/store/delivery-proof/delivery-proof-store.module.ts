import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { deliveryProofReducer } from "./delivery-proof.reducer";
import { DeliveryProofEffects } from "./delivery-proof.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("deliveryProof", deliveryProofReducer),
        EffectsModule.forFeature([DeliveryProofEffects]),
    ],
    providers: [],
})
export class DeliveryProofStoreModule {}
