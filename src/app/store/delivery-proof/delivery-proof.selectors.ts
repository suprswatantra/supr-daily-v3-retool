import { createSelector, createFeatureSelector } from "@ngrx/store";
import { DeliveryProofState } from "./delivery-proof.state";

export const selectFeature = createFeatureSelector<DeliveryProofState>(
    "deliveryProof"
);

export const selectDeliveryProof = createSelector(
    selectFeature,
    (state: DeliveryProofState) => state.deliveryProofs
);

export const selectStoreCurrentState = createSelector(
    selectFeature,
    (state: DeliveryProofState) => state.currentState
);

export const selectStoreErrorState = createSelector(
    selectFeature,
    (state: DeliveryProofState, props: { type: string }) => {
        switch (props.type) {
            case "fetchDeliveryProof":
                return state.fetchingDeliveryProofsError;
            case "reportDeliveryProof":
                return state.reportingDeliveryProofError;
            case "postDeliveryProofFeedback":
                return state.postingDeliveryFeedbackError;
            default:
                return null;
        }
    }
);
