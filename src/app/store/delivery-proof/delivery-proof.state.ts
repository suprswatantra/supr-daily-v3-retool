import { DeliveryProof } from "@models";

export enum DeliveryProofCurrentState {
    IDLE = "0",
    FETCHING_POD = "1",
    REPORTING_POD_IMAGES = "2",
}

export interface DeliveryProofState {
    fetchingDeliveryProofsError?: any;
    reportingDeliveryProofError?: any;
    postingDeliveryFeedbackError?: any;
    deliveryProofs: Array<DeliveryProof>;
    currentState: DeliveryProofCurrentState;
}

export const initialState: DeliveryProofState = {
    deliveryProofs: [],
    currentState: DeliveryProofCurrentState.IDLE,
};
