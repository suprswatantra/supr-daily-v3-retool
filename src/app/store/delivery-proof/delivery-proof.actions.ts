import { Action } from "@ngrx/store";

import { DeliveryProof } from "@models";

import { SuprApi } from "@types";

export enum ActionTypes {
    FETCH_DELIVERY_PROOFS = "[Delivery Proof] Fetch Delivery Proofs",
    POST_DELIVERY_PROOF_FEEDBACK = "[Delivery Proof] Post Delivery Proof Feedback",
    REPORT_DELIVERY_PROOF_IMAGES = "[Delivery Proof] Report Delivery Proof Images",
    FETCH_DELIVERY_PROOFS_SUCCESS = "[Delivery Proof] Fetch Delivery Proofs Success",
    FETCH_DELIVERY_PROOFS_FAILURE = "[Delivery Proof] Fetch Delivery Proofs Failure",
    POST_DELIVERY_PROOF_FEEDBACK_SUCCESS = "[Delivery Proof] Post Delivery Proof Success",
    POST_DELIVERY_PROOF_FEEDBACK_FAILURE = "[Delivery Proof] Post Delivery Proof Failure",
    REPORT_DELIVERY_PROOF_IMAGES_SUCCESS = "[Delivery Proof] Report Delivery Proof Images Success",
    REPORT_DELIVERY_PROOF_IMAGES_FAILURE = "[Delivery Proof] Report Delivery Proof Images Failure",
}

export class FetchDeliveryProofsAction implements Action {
    readonly type = ActionTypes.FETCH_DELIVERY_PROOFS;
    constructor(public payload: SuprApi.DeliveryProofQueryParams) {}
}

export class FetchDeliveryProofsSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_DELIVERY_PROOFS_SUCCESS;
    constructor(public payload: DeliveryProof[]) {}
}

export class FetchDeliveryProofsFailureAction implements Action {
    readonly type = ActionTypes.FETCH_DELIVERY_PROOFS_FAILURE;
    constructor(public payload: any) {}
}

export class PostDeliveryProofFeedbackAction implements Action {
    readonly type = ActionTypes.POST_DELIVERY_PROOF_FEEDBACK;
    constructor(public payload: SuprApi.DeliveryProofFeedbackPayload) {}
}

export class PostDeliveryProofFeedbackSuccessAction implements Action {
    readonly type = ActionTypes.POST_DELIVERY_PROOF_FEEDBACK_SUCCESS;
}

export class PostDeliveryProofFeedbackFailureAction implements Action {
    readonly type = ActionTypes.POST_DELIVERY_PROOF_FEEDBACK_FAILURE;
    constructor(public payload: any) {}
}

export class ReportDeliveryProofImagesAction implements Action {
    readonly type = ActionTypes.REPORT_DELIVERY_PROOF_IMAGES;
    constructor(public payload: SuprApi.DeliveryProofReportPayload) {}
}
export class ReportDeliveryProofImagesSuccessAction implements Action {
    readonly type = ActionTypes.REPORT_DELIVERY_PROOF_IMAGES_SUCCESS;
}
export class ReportDeliveryProofImagesFailureAction implements Action {
    readonly type = ActionTypes.REPORT_DELIVERY_PROOF_IMAGES_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchDeliveryProofsAction
    | PostDeliveryProofFeedbackAction
    | ReportDeliveryProofImagesAction
    | FetchDeliveryProofsSuccessAction
    | FetchDeliveryProofsFailureAction
    | PostDeliveryProofFeedbackSuccessAction
    | PostDeliveryProofFeedbackFailureAction
    | ReportDeliveryProofImagesSuccessAction
    | ReportDeliveryProofImagesFailureAction;
