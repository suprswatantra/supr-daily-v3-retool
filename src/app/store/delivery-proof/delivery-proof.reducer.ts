import { Actions, ActionTypes } from "./delivery-proof.actions";

import {
    initialState,
    DeliveryProofState,
    DeliveryProofCurrentState,
} from "./delivery-proof.state";

export function deliveryProofReducer(
    state = initialState,
    action: Actions
): DeliveryProofState {
    switch (action.type) {
        case ActionTypes.FETCH_DELIVERY_PROOFS:
            return {
                ...state,
                deliveryProofs: [],
                fetchingDeliveryProofsError: null,
                currentState: DeliveryProofCurrentState.FETCHING_POD,
            };

        case ActionTypes.FETCH_DELIVERY_PROOFS_SUCCESS:
            return {
                ...state,
                deliveryProofs: action.payload,
                fetchingDeliveryProofsError: null,
                currentState: DeliveryProofCurrentState.IDLE,
            };

        case ActionTypes.FETCH_DELIVERY_PROOFS_FAILURE:
            return {
                ...state,
                fetchingDeliveryProofsError: action.payload,
                currentState: DeliveryProofCurrentState.IDLE,
            };

        case ActionTypes.POST_DELIVERY_PROOF_FEEDBACK_FAILURE:
            return {
                ...state,
                postingDeliveryFeedbackError: action.payload,
            };

        case ActionTypes.REPORT_DELIVERY_PROOF_IMAGES:
            return {
                ...state,
                reportingDeliveryProofError: null,
                currentState: DeliveryProofCurrentState.REPORTING_POD_IMAGES,
            };

        case ActionTypes.REPORT_DELIVERY_PROOF_IMAGES_SUCCESS:
            return {
                ...state,
                reportingDeliveryProofError: null,
                currentState: DeliveryProofCurrentState.IDLE,
            };

        case ActionTypes.REPORT_DELIVERY_PROOF_IMAGES_FAILURE:
            return {
                ...state,
                reportingDeliveryProofError: action.payload,
                currentState: DeliveryProofCurrentState.IDLE,
            };

        default:
            return state;
    }
}
