import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, Store, select } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap, map, withLatestFrom } from "rxjs/operators";

import { SuprApi } from "@types";
import { StoreState } from "../store.state";

import { ReferralInfo } from "@shared/models";

import { ApiService } from "@services/data/api.service";

import * as ReferralActions from "./referral.actions";
import { UserStoreSelectors } from "@store/user";

@Injectable()
export class ReferralEffects {
    constructor(
        private apiService: ApiService,
        private actions$: Actions,
        private store$: Store<StoreState>
    ) {}

    @Effect()
    fetchReferralInfoEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ReferralActions.FetchReferralRequestAction>(
            ReferralActions.ActionTypes.FETCH_REFERRAL_REQUEST
        ),
        switchMap((action) =>
            this.apiService.fetchReferralInfo(action.payload.silent).pipe(
                map(
                    (data: ReferralInfo) =>
                        new ReferralActions.FetchReferralRequestSuccessAction(
                            data
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new ReferralActions.FetchReferralRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    applyReferralCodeEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ReferralActions.ApplyReferralCodeRequestAction>(
            ReferralActions.ActionTypes.APPLY_REFERRAL_CODE_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectUserId))
        ),
        switchMap(([action, custId]) =>
            this.apiService
                .applyReferralcode(custId, action.payload.referralCode)
                .pipe(
                    map(
                        (data: SuprApi.ApplyReferralCodeRes) =>
                            new ReferralActions.ApplyReferralCodeRequestSuccessAction(
                                data
                            )
                    ),
                    catchError((error) =>
                        observableOf(
                            new ReferralActions.FetchReferralRequestFailureAction(
                                error
                            )
                        )
                    )
                )
        )
    );

    @Effect()
    referralFilterContactsEffect$: Observable<Action> = this.actions$.pipe(
        ofType<ReferralActions.ReferralFilterContactsRequestAction>(
            ReferralActions.ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST
        ),
        switchMap((action) =>
            this.apiService
                .filterReferralContacts(action.payload.contacts)
                .pipe(
                    map(
                        (data: SuprApi.FilterReferralContactsRes) =>
                            new ReferralActions.ReferralFilterContactsRequestSuccessAction(
                                data
                            )
                    ),
                    catchError((error) =>
                        observableOf(
                            new ReferralActions.ReferralFilterContactsRequestFailureAction(
                                error
                            )
                        )
                    )
                )
        )
    );
}
