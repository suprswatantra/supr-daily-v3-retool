import { Actions, ActionTypes } from "./referral.actions";
import {
    initialState,
    ReferralCurrentState,
    ReferralState,
} from "./referral.state";

export function referralReducer(
    state = initialState,
    action: Actions
): ReferralState {
    switch (action.type) {
        case ActionTypes.FETCH_REFERRAL_REQUEST: {
            return {
                ...state,
                currentState: ReferralCurrentState.FETCHING_REFERRAL,
                error: null,
            };
        }
        case ActionTypes.FETCH_REFERRAL_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                referralInfo: action.payload,
                error: null,
            };
        }
        case ActionTypes.FETCH_REFERRAL_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                error: action.payload,
            };
        }

        case ActionTypes.APPLY_REFERRAL_CODE_REQUEST: {
            return {
                ...state,
                currentState: ReferralCurrentState.APPLYING_REFERRAL_CODE,
                error: null,
            };
        }
        case ActionTypes.APPLY_REFERRAL_CODE_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                referralInfo: {
                    ...state.referralInfo,
                    referralCouponBlock: {
                        ...state.referralInfo.referralCouponBlock,
                        ...action.payload,
                        showCouponBlock:
                            action.payload && action.payload.applied
                                ? false
                                : true,
                    },
                },
                error: null,
            };
        }
        case ActionTypes.APPLY_REFERRAL_CODE_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                error: action.payload,
            };
        }
        case ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST: {
            return {
                ...state,
                currentState: ReferralCurrentState.SYNCING_CONTACTS,
                error: null,
            };
        }
        case ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                filteredContacts:
                    (action.payload && action.payload.contacts) || [],
                error: null,
            };
        }
        case ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: ReferralCurrentState.IDLE,
                error: action.payload,
            };
        }
        case ActionTypes.SET_REFERRAL_CODE: {
            return {
                ...state,
                prePopulatedReferralCode:
                    action.payload && action.payload.referralCode,
            };
        }
        case ActionTypes.RESET_REFERRAL_CODE: {
            return {
                ...state,
                prePopulatedReferralCode: null,
            };
        }
        default: {
            return state;
        }
    }
}
