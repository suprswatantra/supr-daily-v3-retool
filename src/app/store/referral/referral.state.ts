import { ReferralInfo } from "@models";

export enum ReferralCurrentState {
    IDLE = "idle",
    FETCHING_REFERRAL = "fetching_referral",
    APPLYING_REFERRAL_CODE = "applying_referral_code",
    SYNCING_CONTACTS = "syncing_contacts",
}

export interface ReferralState {
    currentState: ReferralCurrentState;
    error?: any;
    referralInfo: ReferralInfo | null;
    filteredContacts: string[];
    prePopulatedReferralCode: string;
}

export const initialState: ReferralState = {
    currentState: ReferralCurrentState.IDLE,
    error: null,
    referralInfo: null,
    filteredContacts: [],
    prePopulatedReferralCode: null,
};
