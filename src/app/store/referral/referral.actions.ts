import { Action } from "@ngrx/store";

import { ReferralInfo } from "@shared/models";
import { SuprApi } from "@types";

export enum ActionTypes {
    FETCH_REFERRAL_REQUEST = "[Referral] Fetch Request",
    FETCH_REFERRAL_REQUEST_SUCCESS = "[Referral] Fetch Request Success",
    FETCH_REFERRAL_REQUEST_FAILURE = "[Referral] Fetch Request Failure",

    APPLY_REFERRAL_CODE_REQUEST = "[Referral] Apply Referral Code Request",
    APPLY_REFERRAL_CODE_REQUEST_SUCCESS = "[Referral] Apply Referral Code Request Success",
    APPLY_REFERRAL_CODE_REQUEST_FAILURE = "[Referral] Apply Referral Code Request Failure",

    REFERRAL_FILTER_CONTACTS_REQUEST = "[Referral] Filter Contacts Fetch Request",
    REFERRAL_FILTER_CONTACTS_REQUEST_SUCCESS = "[Referral] Filter Contacts Fetch Request Success",
    REFERRAL_FILTER_CONTACTS_REQUEST_FAILURE = "[Referral] Filter Contacts Fetch Request Failure",

    SET_REFERRAL_CODE = "[Referral] Set Referral Code",
    RESET_REFERRAL_CODE = "[Referral] Reset Referral Code",
}

export class FetchReferralRequestAction implements Action {
    readonly type = ActionTypes.FETCH_REFERRAL_REQUEST;
    constructor(public payload: { silent: boolean }) {}
}

export class FetchReferralRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_REFERRAL_REQUEST_SUCCESS;
    constructor(public payload: ReferralInfo) {}
}

export class FetchReferralRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_REFERRAL_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class ApplyReferralCodeRequestAction implements Action {
    readonly type = ActionTypes.APPLY_REFERRAL_CODE_REQUEST;
    constructor(public payload: { referralCode: string }) {}
}

export class ApplyReferralCodeRequestSuccessAction implements Action {
    readonly type = ActionTypes.APPLY_REFERRAL_CODE_REQUEST_SUCCESS;
    constructor(public payload: SuprApi.ApplyReferralCodeRes) {}
}

export class ApplyReferralCodeRequestFailureAction implements Action {
    readonly type = ActionTypes.APPLY_REFERRAL_CODE_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class ReferralFilterContactsRequestAction implements Action {
    readonly type = ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST;
    constructor(public payload: { contacts: string[] }) {}
}

export class ReferralFilterContactsRequestSuccessAction implements Action {
    readonly type = ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST_SUCCESS;
    constructor(public payload: SuprApi.FilterReferralContactsRes) {}
}

export class ReferralFilterContactsRequestFailureAction implements Action {
    readonly type = ActionTypes.REFERRAL_FILTER_CONTACTS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class SetReferralCode implements Action {
    readonly type = ActionTypes.SET_REFERRAL_CODE;
    constructor(public payload: { referralCode: string }) {}
}

export class ResetReferralCode implements Action {
    readonly type = ActionTypes.RESET_REFERRAL_CODE;
}

export type Actions =
    | FetchReferralRequestAction
    | FetchReferralRequestSuccessAction
    | FetchReferralRequestFailureAction
    | ApplyReferralCodeRequestAction
    | ApplyReferralCodeRequestSuccessAction
    | ApplyReferralCodeRequestFailureAction
    | ReferralFilterContactsRequestAction
    | ReferralFilterContactsRequestSuccessAction
    | ReferralFilterContactsRequestFailureAction
    | SetReferralCode
    | ResetReferralCode;
