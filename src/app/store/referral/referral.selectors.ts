import { createSelector, createFeatureSelector } from "@ngrx/store";
import { ReferralState, ReferralCurrentState } from "./referral.state";

export const selectFeature = createFeatureSelector<ReferralState>("referral");

export const selectReferralStoreCurrentState = createSelector(
    selectFeature,
    (state: ReferralState) => state.currentState
);

export const selectIsFetchingReferralInfo = createSelector(
    selectFeature,
    (state: ReferralState) =>
        state.currentState === ReferralCurrentState.FETCHING_REFERRAL
);

export const selectIsApplyingReferralCode = createSelector(
    selectFeature,
    (state: ReferralState) =>
        state.currentState === ReferralCurrentState.APPLYING_REFERRAL_CODE
);

export const selectReferralError = createSelector(
    selectFeature,
    (state: ReferralState) => state.error
);

export const selectReferralInfo = createSelector(
    selectFeature,
    (state: ReferralState) => state.referralInfo
);

export const selectReferralFaqs = createSelector(
    selectFeature,
    (state: ReferralState) =>
        (state.referralInfo && state.referralInfo.faqs) || []
);

export const selectPrePopulatedReferralCode = createSelector(
    selectFeature,
    (state: ReferralState) => state.prePopulatedReferralCode
);

export const selectReferralRewards = createSelector(
    selectFeature,
    (state: ReferralState) =>
        (state.referralInfo && state.referralInfo.rewards) || {}
);

export const selectFilteredContacts = createSelector(
    selectFeature,
    (state: ReferralState) => state.filteredContacts || []
);

export const selectIsSyncingContacts = createSelector(
    selectFeature,
    (state: ReferralState) =>
        state.currentState === ReferralCurrentState.SYNCING_CONTACTS
);
