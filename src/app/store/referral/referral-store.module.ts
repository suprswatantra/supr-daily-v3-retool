import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { referralReducer } from "./referral.reducer";
import { ReferralEffects } from "./referral.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("referral", referralReducer),
        EffectsModule.forFeature([ReferralEffects]),
    ],
    providers: [],
})
export class ReferralStoreModule {}
