import { createFeatureSelector, createSelector } from "@ngrx/store";

import { AddonStoreState } from ".";

export const selectFeature = createFeatureSelector<AddonStoreState>("addon");

export const selectAddonCurrentState = createSelector(
    selectFeature,
    (state: AddonStoreState) => {
        return state.currentState;
    }
);

export const selectAddonErrorState = createSelector(
    selectFeature,
    (state: AddonStoreState) => {
        return state.error;
    }
);
