import { Action } from "@ngrx/store";

export enum ActionTypes {
    UPDATE_ADDON_REQUEST_ACTION = "[Addon] Update addon data",
    UPDATE_ADDON_REQUEST_SUCCESS_ACTION = "[Addon] Update addon data success",
    UPDATE_ADDON_REQUEST_FAILURE_ACTION = "[Addon] Update addon data failure",
    CANCEL_ADDON_REQUEST_ACTION = "[Schedule] Cancel addon data",
    CANCEL_ADDON_REQUEST_SUCCESS_ACTION = "[Schedule] Cancel addon data success",
    CANCEL_ADDON_REQUEST_FAILURE_ACTION = "[Schedule] Cancel addon data failure",
}

export class UpdateAddonRequestAction implements Action {
    readonly type = ActionTypes.UPDATE_ADDON_REQUEST_ACTION;
    constructor(public payload: { addonId: number; quantity: number }) {}
}

export class UpdateAddonRequestActionSuccess implements Action {
    readonly type = ActionTypes.UPDATE_ADDON_REQUEST_SUCCESS_ACTION;
    constructor() {}
}

export class UpdateAddonRequestActionFailure implements Action {
    readonly type = ActionTypes.UPDATE_ADDON_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class CancelAddonRequestAction implements Action {
    readonly type = ActionTypes.CANCEL_ADDON_REQUEST_ACTION;
    constructor(public payload: { addonId: number }) {}
}

export class CancelAddonRequestActionSuccess implements Action {
    readonly type = ActionTypes.CANCEL_ADDON_REQUEST_SUCCESS_ACTION;
    constructor() {}
}

export class CancelAddonRequestActionFailure implements Action {
    readonly type = ActionTypes.CANCEL_ADDON_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | UpdateAddonRequestAction
    | UpdateAddonRequestActionSuccess
    | UpdateAddonRequestActionFailure
    | CancelAddonRequestAction
    | CancelAddonRequestActionSuccess
    | CancelAddonRequestActionFailure;
