import { Injectable } from "@angular/core";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";

import { ApiService } from "app/services/data/api.service";

import { WalletStoreActions } from "@store/wallet";
import { CollectionStoreActions } from "@store/collection";
import { DeliveryStatusStoreActions } from "@store/delivery-status";

import * as AddonStoreActions from "./addon.actions";
import { ScheduleStoreActions } from "@store/schedule";

@Injectable()
export class AddonStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    cancelAddon$: Observable<Action> = this.actions$.pipe(
        ofType<AddonStoreActions.CancelAddonRequestAction>(
            AddonStoreActions.ActionTypes.CANCEL_ADDON_REQUEST_ACTION
        ),
        switchMap(action => {
            const { addonId } = action.payload;
            return this.apiService.cancelAddon(addonId).pipe(
                switchMap(() => [
                    new AddonStoreActions.CancelAddonRequestActionSuccess(),
                    new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                    new WalletStoreActions.LoadBalanceRequestAction(),
                    new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                    new CollectionStoreActions.FetchPastOrderSkuListRequestAction(),
                ]),

                catchError(error =>
                    observableOf(
                        new AddonStoreActions.CancelAddonRequestActionFailure(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    updateAddon$: Observable<Action> = this.actions$.pipe(
        ofType<AddonStoreActions.UpdateAddonRequestAction>(
            AddonStoreActions.ActionTypes.UPDATE_ADDON_REQUEST_ACTION
        ),
        switchMap(action => {
            const { addonId, quantity } = action.payload;
            return this.apiService.updateAddon(addonId, quantity).pipe(
                switchMap(() => {
                    return [
                        new AddonStoreActions.UpdateAddonRequestActionSuccess(),
                        new ScheduleStoreActions.GetScheduleDataAction({}),
                        new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                    ];
                }),

                catchError(error =>
                    observableOf(
                        new AddonStoreActions.UpdateAddonRequestActionFailure(
                            error
                        )
                    )
                )
            );
        })
    );
}
