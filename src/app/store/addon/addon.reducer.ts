import { Actions, ActionTypes } from "./addon.actions";
import { initialState, AddonState, AddonCurrentState } from "./addon.state";

export function addonReducer(
    state = initialState,
    action: Actions
): AddonState {
    switch (action.type) {
        case ActionTypes.CANCEL_ADDON_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: AddonCurrentState.CANCELLING_ADDON,
            };
        }
        case ActionTypes.UPDATE_ADDON_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: AddonCurrentState.UPDATING_ADDON,
            };
        }
        case ActionTypes.CANCEL_ADDON_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: AddonCurrentState.CANCELLING_ADDON_DONE,
            };
        }
        case ActionTypes.UPDATE_ADDON_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: AddonCurrentState.UPDATING_ADDON_DONE,
            };
        }
        case ActionTypes.CANCEL_ADDON_REQUEST_FAILURE_ACTION:
        case ActionTypes.UPDATE_ADDON_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: AddonCurrentState.NO_ACTION,
            };
        }
        default: {
            return state;
        }
    }
}
