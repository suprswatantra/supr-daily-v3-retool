export enum AddonCurrentState {
    NO_ACTION = "0",
    CANCELLING_ADDON = "1",
    CANCELLING_ADDON_DONE = "2",
    UPDATING_ADDON = "3",
    UPDATING_ADDON_DONE = "4",
}

export interface AddonState {
    error?: any;
    currentState?: AddonCurrentState;
}

export const initialState: AddonState = {
    error: null,
    currentState: AddonCurrentState.NO_ACTION,
};
