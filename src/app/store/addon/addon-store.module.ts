import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { addonReducer } from "./addon.reducer";
import { AddonStoreEffects } from "./addon.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("addon", addonReducer),
        EffectsModule.forFeature([AddonStoreEffects]),
    ],
    providers: [],
})
export class AddonStoreModule {}
