import { PNData } from "./pn.state";
import { Action } from "@ngrx/store";

export enum ActionTypes {
    SET_PN_DATA = "[PN] Set PN Data",
    RESET_PN_DATA = "[PN] Reset PN Data",
}

export class SetPNData implements Action {
    readonly type = ActionTypes.SET_PN_DATA;
    constructor(public payload: PNData) {}
}

export class ResetPNData implements Action {
    readonly type = ActionTypes.RESET_PN_DATA;
}

export type Actions = SetPNData | ResetPNData;
