export interface PNData {
    pnType: PNType;
    url?: string;
    title?: string;
    desc?: string;
    img?: string;
    buttonUrl?: string;
    buttonText?: string;
    outsideRedirection?: boolean;
    skipLoginCheck?: boolean;
    source?: "moengage" | "onelink" | "native";
    utm_source?: string;
    utm_medium?: string;
    utm_campaign?: string;
    utm_term?: string;
    utm_content?: string;
}

export type PNType = "popup" | "deeplink" | "richLanding";

export interface PnState {
    pnData?: PNData | null;
}

export const initialState: PnState = {
    pnData: null,
};
