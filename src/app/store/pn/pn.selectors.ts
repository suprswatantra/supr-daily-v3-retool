import { createFeatureSelector, createSelector } from "@ngrx/store";

import { PnState } from "./pn.state";

export const selectPnFeature = createFeatureSelector<PnState>("pn");

export const selectPNType = createSelector(selectPnFeature, (state: PnState) =>
    state.pnData ? state.pnData.pnType : null
);

export const selectPNData = createSelector(
    selectPnFeature,
    (state: PnState) => state.pnData
);
