import { Actions, ActionTypes } from "./pn.actions";
import { initialState, PnState } from "./pn.state";

export function pnReducer(state = initialState, action: Actions): PnState {
    switch (action.type) {
        case ActionTypes.SET_PN_DATA: {
            return { ...state, pnData: action.payload };
        }

        case ActionTypes.RESET_PN_DATA: {
            return { ...state, pnData: null };
        }

        default: {
            return state;
        }
    }
}
