import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";

import { pnReducer } from "./pn.reducer";

@NgModule({
    imports: [StoreModule.forFeature("pn", pnReducer)],
    providers: [],
})
export class PnStoreModule {}
