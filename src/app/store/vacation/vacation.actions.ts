import { Action } from "@ngrx/store";
import { Vacation, VacationApiRes } from "@models";

export enum ActionTypes {
    FETCH_VACATION_REQUEST = "[Vacation] Fetch vacation request",
    FETCH_VACATION_REQUEST_SUCCESS = "[Vacation] Fetch vacation request success",
    FETCH_VACATION_REQUEST_FAILURE = "[Vacation] Fetch vacation request failure",

    CREATE_VACATION_REQUEST = "[Vacation] Create vacation request",
    CREATE_VACATION_REQUEST_SUCCESS = "[Vacation] Create vacation request success",
    CREATE_VACATION_REQUEST_FAILURE = "[Vacation] Create vacation request failure",

    CANCEL_VACATION_REQUEST = "[Vacation] Cancel vacation request",
    CANCEL_VACATION_REQUEST_SUCCESS = "[Vacation] Cancel vacation request success",
    CANCEL_VACATION_REQUEST_FAILURE = "[Vacation] Cancel vacation request failure",

    TOGGLE_VACATION_MODEL = "[Vacation] Toggle vacation model",
}

export class FetchVacationRequestAction implements Action {
    readonly type = ActionTypes.FETCH_VACATION_REQUEST;
}

export class FetchVacationRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_VACATION_REQUEST_SUCCESS;
    constructor(public payload: VacationApiRes) {}
}

export class FetchVacationRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_VACATION_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class CreateVacationRequestAction implements Action {
    readonly type = ActionTypes.CREATE_VACATION_REQUEST;
    constructor(public payload: Vacation) {}
}

export class CreateVacationRequestSuccessAction implements Action {
    readonly type = ActionTypes.CREATE_VACATION_REQUEST_SUCCESS;
    constructor(public payload: Vacation) {}
}

export class CreateVacationRequestFailureAction implements Action {
    readonly type = ActionTypes.CREATE_VACATION_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class CancelVacationRequestAction implements Action {
    readonly type = ActionTypes.CANCEL_VACATION_REQUEST;
}

export class CancelVacationRequestSuccessAction implements Action {
    readonly type = ActionTypes.CANCEL_VACATION_REQUEST_SUCCESS;
}

export class CancelVacationRequestFailureAction implements Action {
    readonly type = ActionTypes.CANCEL_VACATION_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class ToggleVacationModal implements Action {
    readonly type = ActionTypes.TOGGLE_VACATION_MODEL;
}

export type Actions =
    | FetchVacationRequestAction
    | FetchVacationRequestSuccessAction
    | FetchVacationRequestFailureAction
    | CreateVacationRequestAction
    | CreateVacationRequestSuccessAction
    | CreateVacationRequestFailureAction
    | CancelVacationRequestAction
    | CancelVacationRequestSuccessAction
    | CancelVacationRequestFailureAction
    | ToggleVacationModal;
