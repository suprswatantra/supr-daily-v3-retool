import { createFeatureSelector, createSelector } from "@ngrx/store";
import { VacationState } from "./vacation.state";

export const selectVacationFeature = createFeatureSelector<VacationState>(
    "vacation"
);

export const selectVacation = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.vacation
);

export const selectVacationCurrentState = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.currentState
);

export const selectVacationError = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.error
);

export const selectIsVacationActive = createSelector(
    selectVacationFeature,
    (state: VacationState) => !!state.vacation
);

export const selectIsSystemVacationActive = createSelector(
    selectVacationFeature,
    (state: VacationState) => !!state.systemVacation
);

export const selectIsVacationModalOpen = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.isModalOpen
);

export const selectSystemVacation = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.systemVacation
);

export const selectVacationMsg = createSelector(
    selectVacationFeature,
    (state: VacationState) => state.vacation && state.vacation.message
);

export const selectSystemVacationMsg = createSelector(
    selectVacationFeature,
    (state: VacationState) =>
        state.systemVacation && state.systemVacation.message
);
