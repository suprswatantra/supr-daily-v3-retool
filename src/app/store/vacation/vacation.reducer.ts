import { Actions, ActionTypes } from "./vacation.actions";
import {
    initialState,
    VacationState,
    VacationCurrentState,
} from "./vacation.state";

export function vacationReducer(
    state = initialState,
    action: Actions
): VacationState {
    switch (action.type) {
        case ActionTypes.FETCH_VACATION_REQUEST: {
            return {
                ...state,
                currentState: VacationCurrentState.FETCHING_VACATION,
                error: null,
            };
        }

        case ActionTypes.FETCH_VACATION_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                vacation: action.payload.vacation,
                systemVacation: action.payload.system_vacation,
                systemVacationMsg: action.payload.system_vacation_message,
            };
        }

        case ActionTypes.FETCH_VACATION_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.CREATE_VACATION_REQUEST: {
            return {
                ...state,
                currentState: VacationCurrentState.CREATING_VACATION,
                error: null,
            };
        }

        case ActionTypes.CREATE_VACATION_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                vacation: action.payload,
            };
        }

        case ActionTypes.CREATE_VACATION_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.CANCEL_VACATION_REQUEST: {
            return {
                ...state,
                currentState: VacationCurrentState.CANCELLING_VACATION,
                error: null,
            };
        }

        case ActionTypes.CANCEL_VACATION_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                vacation: null,
            };
        }

        case ActionTypes.CANCEL_VACATION_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: VacationCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.TOGGLE_VACATION_MODEL: {
            return {
                ...state,
                isModalOpen: !state.isModalOpen,
            };
        }

        default: {
            return state;
        }
    }
}
