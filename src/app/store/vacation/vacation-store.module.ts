import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { vacationReducer } from "./vacation.reducer";
import { VacationStoreEffects } from "./vacation.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("vacation", vacationReducer),
        EffectsModule.forFeature([VacationStoreEffects]),
    ],
    providers: [],
})
export class VacationStoreModule {}
