import { Vacation, SystemVacation, SystemVacationMessage } from "@models";

export enum VacationCurrentState {
    NO_ACTION = "0",
    FETCHING_VACATION = "1",
    CREATING_VACATION = "2",
    CANCELLING_VACATION = "3",
}

export interface VacationState {
    currentState: VacationCurrentState;
    vacation: Vacation;
    systemVacation?: SystemVacation;
    systemVacationMsg?: SystemVacationMessage;
    error?: any;
    isModalOpen: boolean;
}

export const initialState: VacationState = {
    currentState: VacationCurrentState.NO_ACTION,
    vacation: null,
    systemVacation: null,
    systemVacationMsg: null,
    error: null,
    isModalOpen: false,
};
