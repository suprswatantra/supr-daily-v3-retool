import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import { VacationApiRes, Vacation } from "@models";

import { ApiService } from "@services/data/api.service";

import * as VacationActions from "./vacation.actions";
import { WalletStoreActions } from "@store/wallet";
import { ScheduleStoreActions } from "@store/schedule";
import { DeliveryStatusStoreActions } from "@store/delivery-status";
import { SubscriptionStoreActions } from "@store/subscription";
import { CartStoreActions } from "@store/cart";

@Injectable()
export class VacationStoreEffects {
    constructor(private actions$: Actions, private apiService: ApiService) {}

    @Effect()
    fetchVacation$: Observable<Action> = this.actions$.pipe(
        ofType<VacationActions.FetchVacationRequestAction>(
            VacationActions.ActionTypes.FETCH_VACATION_REQUEST
        ),
        switchMap(() =>
            this.apiService.fetchVacation().pipe(
                map(
                    (vacation: VacationApiRes) =>
                        new VacationActions.FetchVacationRequestSuccessAction(
                            vacation
                        )
                ),
                catchError(error =>
                    observableOf(
                        new VacationActions.FetchVacationRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    createVacation$: Observable<Action> = this.actions$.pipe(
        ofType<VacationActions.CreateVacationRequestAction>(
            VacationActions.ActionTypes.CREATE_VACATION_REQUEST
        ),
        switchMap(action =>
            this.apiService.createVacation(action.payload).pipe(
                switchMap((vacation: Vacation) => [
                    new VacationActions.CreateVacationRequestSuccessAction(
                        vacation
                    ),
                    new WalletStoreActions.LoadBalanceRequestAction(),
                    new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                    new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                    new SubscriptionStoreActions.FetchSubscriptionListRequestAction(),
                    new CartStoreActions.EnableRevalidateAction(),
                ]),
                catchError(error =>
                    observableOf(
                        new VacationActions.CreateVacationRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    cancelVacation$: Observable<Action> = this.actions$.pipe(
        ofType<VacationActions.CancelVacationRequestAction>(
            VacationActions.ActionTypes.CANCEL_VACATION_REQUEST
        ),
        switchMap(() =>
            this.apiService.cancelVaction().pipe(
                switchMap(() => [
                    new WalletStoreActions.LoadBalanceRequestAction(),
                    new VacationActions.CancelVacationRequestSuccessAction(),
                    new ScheduleStoreActions.SetRefreshScheduleFlagAction(),
                    new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                    new SubscriptionStoreActions.FetchSubscriptionListRequestAction(),
                    new CartStoreActions.EnableRevalidateAction(),
                ]),
                catchError(error =>
                    observableOf(
                        new VacationActions.CancelVacationRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
}
