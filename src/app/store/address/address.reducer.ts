import { Actions, ActionTypes } from "./address.actions";
import {
    initialState,
    AddressState,
    AddressCurrentState,
} from "./address.state";

export function addressReducer(
    state = initialState,
    action: Actions
): AddressState {
    switch (action.type) {
        case ActionTypes.FETCH_ADDRESS_REQUEST_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.FETCHING_ADDRESS,
                error: null,
            };
        }

        case ActionTypes.FETCH_ADDRESS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.NO_ACTION,
                selectedAddress: action.payload,
            };
        }

        case ActionTypes.FETCH_ADDRESS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.CREATE_ADDRESS_REQUEST_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.CREATING_ADDRESS,
                error: null,
            };
        }

        case ActionTypes.CREATE_ADDRESS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: action.payload.state,
                error: null,
                selectedAddress: action.payload.address,
            };
        }

        case ActionTypes.CREATE_ADDRESS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.SEARCH_PREMISE_REQUEST_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.SEARCHING_SOCIETIES,
                error: null,
            };
        }

        case ActionTypes.SEARCH_PREMISE_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                premise: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SEARCH_PREMISE_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.SEARCHING_POPULAR_SOCIETIES,
                error: null,
            };
        }

        case ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                popularPremise: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.SEARCHING_SOCIETY_DETAILS,
                error: null,
            };
        }

        case ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                premiseAddress: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.REFRESH_SKU_AFTER_ADDRESS_CREATE_ACTION: {
            return {
                ...state,
                currentState: AddressCurrentState.REFRESHING_ON_HUB_CHANGE,
            };
        }

        case ActionTypes.REFRESH_DATA_AFTER_ADDRESS_CREATE_ACTION_DONE: {
            return {
                ...state,
                currentState: AddressCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SHOW_ADDRESS_RETRO_MODAL: {
            return {
                ...state,
                showAddressRetroModal: true,
            };
        }

        case ActionTypes.HIDE_ADDRESS_RETRO_MODAL: {
            return {
                ...state,
                showAddressRetroModal: false,
            };
        }

        default: {
            return state;
        }
    }
}
