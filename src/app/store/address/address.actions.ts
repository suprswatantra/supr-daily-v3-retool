import { Action } from "@ngrx/store";

import { Address } from "app/shared/models";
import { SuprApi } from "app/types";

import { AddressCurrentState } from "./address.state";

export enum ActionTypes {
    FETCH_ADDRESS_REQUEST_ACTION = "[Address] Fetch address request",
    FETCH_ADDRESS_REQUEST_SUCCESS_ACTION = "[Address] Fetch address request success",
    FETCH_ADDRESS_REQUEST_FAILURE_ACTION = "[Address] Fetch address request failure",

    CREATE_ADDRESS_REQUEST_ACTION = "[Address] Create address request",
    CREATE_ADDRESS_REQUEST_SUCCESS_ACTION = "[Address] Create address request success",
    CREATE_ADDRESS_REQUEST_FAILURE_ACTION = "[Address] Create address request failure",

    SEARCH_PREMISE_REQUEST_ACTION = "[Search Premise] Search Premise request",
    SEARCH_PREMISE_REQUEST_SUCCESS_ACTION = "[Search Premise] Search Premise request success",
    SEARCH_PREMISE_REQUEST_FAILURE_ACTION = "[Search Premise] Search Premise request failure",

    SEARCH_PREMISE_ADDRESS_REQUEST_ACTION = "[Search Premise Address] Search Premise Address request",
    SEARCH_PREMISE_ADDRESS_REQUEST_SUCCESS_ACTION = "[Search Premise Address] Search Premise Address request success",
    SEARCH_PREMISE_ADDRESS_REQUEST_FAILURE_ACTION = "[Search Premise Address] Search Premise Address request failure",

    REFRESH_SKU_AFTER_ADDRESS_CREATE_ACTION = "[Address] Refreshing the sku data",
    REFRESH_CATALOG_AFTER_ADDRESS_CREATE_ACTION = "[Address] Refreshing the catalog",
    REFRESH_DATA_AFTER_ADDRESS_CREATE_ACTION_DONE = "[Address] Refreshing the data success",

    SEARCH_POPULAR_PREMISE_REQUEST_ACTION = "[Search Popular Premise] Search Popular Premise request",
    SEARCH_POPULAR_PREMISE_REQUEST_SUCCESS_ACTION = "[Search Popular Premise] Search Popular Premise request success",
    SEARCH_POPULAR_PREMISE_REQUEST_FAILURE_ACTION = "[Search Popular Premise] Search Popular Premise request failure",

    SHOW_ADDRESS_RETRO_MODAL = "[Address] Show Retro Modal",
    HIDE_ADDRESS_RETRO_MODAL = "[Address] Hide Retro Modal",
}

export class FetchAddressRequestAction implements Action {
    readonly type = ActionTypes.FETCH_ADDRESS_REQUEST_ACTION;
}

export class FetchAddressRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_ADDRESS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: Address) {}
}

export class FetchAddressRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_ADDRESS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class CreateAddressRequestAction implements Action {
    readonly type = ActionTypes.CREATE_ADDRESS_REQUEST_ACTION;
    constructor(
        public payload: { data: SuprApi.AddressBody; meta?: SuprApi.Meta }
    ) {}
}

export class CreateAddressRequestSuccessAction implements Action {
    readonly type = ActionTypes.CREATE_ADDRESS_REQUEST_SUCCESS_ACTION;
    constructor(
        public payload: { address: Address; state: AddressCurrentState }
    ) {}
}

export class CreateAddressRequestFailureAction implements Action {
    readonly type = ActionTypes.CREATE_ADDRESS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class SearchPremiseRequestAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_REQUEST_ACTION;
    constructor(public payload: { data: SuprApi.PremiseSearchReq }) {}
}

export class SearchPremiseRequestSuccessAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_REQUEST_SUCCESS_ACTION;
    constructor(public payload: SuprApi.PremiseSearchRes[]) {}
}

export class SearchPremiseRequestFailureAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class SearchPremiseAddressRequestAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_ACTION;
    constructor(public payload: { data: number }) {}
}

export class SearchPremiseAddressRequestSuccessAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: SuprApi.PremiseAddressRes) {}
}

export class SearchPremiseAddressRequestFailureAction implements Action {
    readonly type = ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class RefreshSkuAfterCreateAddress implements Action {
    readonly type = ActionTypes.REFRESH_SKU_AFTER_ADDRESS_CREATE_ACTION;
}

export class RefreshDataAfterCreateAddressDone implements Action {
    readonly type = ActionTypes.REFRESH_DATA_AFTER_ADDRESS_CREATE_ACTION_DONE;
}

export class RefreshCatalogAfterCreateAddress implements Action {
    readonly type = ActionTypes.REFRESH_CATALOG_AFTER_ADDRESS_CREATE_ACTION;
}

export class SearchPopularPremiseRequestAction implements Action {
    readonly type = ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_ACTION;
    constructor(public payload: { data: SuprApi.PremiseSearchReq }) {}
}

export class SearchPopularPremiseRequestSuccessAction implements Action {
    readonly type = ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_SUCCESS_ACTION;
    constructor(public payload: SuprApi.PremiseSearchRes[]) {}
}

export class SearchPopularPremiseRequestFailureAction implements Action {
    readonly type = ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class ShowAddressRetroModal implements Action {
    readonly type = ActionTypes.SHOW_ADDRESS_RETRO_MODAL;
}

export class HideAddressRetroModal implements Action {
    readonly type = ActionTypes.HIDE_ADDRESS_RETRO_MODAL;
}

export type Actions =
    | FetchAddressRequestAction
    | FetchAddressRequestSuccessAction
    | FetchAddressRequestFailureAction
    | CreateAddressRequestAction
    | CreateAddressRequestSuccessAction
    | CreateAddressRequestFailureAction
    | RefreshSkuAfterCreateAddress
    | RefreshCatalogAfterCreateAddress
    | RefreshDataAfterCreateAddressDone
    | SearchPremiseRequestAction
    | SearchPremiseRequestSuccessAction
    | SearchPremiseRequestFailureAction
    | SearchPremiseAddressRequestAction
    | SearchPremiseAddressRequestSuccessAction
    | SearchPremiseAddressRequestFailureAction
    | SearchPopularPremiseRequestAction
    | SearchPopularPremiseRequestSuccessAction
    | SearchPopularPremiseRequestFailureAction
    | ShowAddressRetroModal
    | HideAddressRetroModal;
