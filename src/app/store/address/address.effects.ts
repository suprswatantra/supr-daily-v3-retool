import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, select, Store } from "@ngrx/store";

import { Observable, of as observableOf, forkJoin } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    withLatestFrom,
    tap,
} from "rxjs/operators";

import { Address } from "@models";

import { ApiService } from "@services/data/api.service";
import { MoengageService } from "@services/integration/moengage.service";
import { CartService } from "../../services/shared/cart.service";

import { StoreState } from "@store/store.state";
import { SuprApi } from "@types";

import * as featuredStoreActions from "../featured/featured.actions";
import * as categoryStoreActions from "../category/category.actions";
import * as essentialStoreActions from "../essentials/essentials.actions";
import * as skuStoreActions from "../sku/sku.actions";
import * as cartStoreActions from "../cart/cart.actions";

import * as collectionStoreActions from "../collection/collection.actions";
import * as collectionV3StoreActions from "../home-layout/home-layout.actions";

import * as addressActions from "./address.actions";
import * as userActions from "../user/user.actions";
import * as AddressStoreSelectors from "./address.selectors";
import { AddressCurrentState } from "./address.state";

@Injectable()
export class AddressStoreEffects {
    constructor(
        private apiService: ApiService,
        private cartService: CartService,
        private moengageService: MoengageService,

        private actions$: Actions,
        private store$: Store<StoreState>
    ) {}

    @Effect()
    fetchAddress$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.FetchAddressRequestAction>(
            addressActions.ActionTypes.FETCH_ADDRESS_REQUEST_ACTION
        ),
        switchMap(() =>
            this.apiService.fetchAddress().pipe(
                map(
                    (selectedAddress: Address) =>
                        new addressActions.FetchAddressRequestSuccessAction(
                            selectedAddress
                        )
                ),

                catchError((error) =>
                    observableOf(
                        new addressActions.FetchAddressRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    createAddress$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.CreateAddressRequestAction>(
            addressActions.ActionTypes.CREATE_ADDRESS_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(select(AddressStoreSelectors.selectAddress))
        ),
        switchMap(([action, oldAddress]) =>
            this.apiService
                .createAddress(action.payload.data, action.payload.meta)
                .pipe(
                    tap((newAddress: Address) => {
                        this.sendAnalyticsEvents(newAddress);
                    }),
                    switchMap((newAddress: Address) => {
                        let addressSuccessActions: Action[] = [
                            new userActions.LoadProfileWithAddressRequestAction(
                                {
                                    address: true,
                                    wallet: true,
                                    subscriptions: true,
                                    t_plus_one: true,
                                }
                            ),
                        ];

                        if (this.hasAddressHubChanged(newAddress, oldAddress)) {
                            return [
                                new addressActions.CreateAddressRequestSuccessAction(
                                    {
                                        address: newAddress,
                                        state:
                                            AddressCurrentState.REFRESHING_ON_HUB_CHANGE,
                                    }
                                ),
                                new addressActions.RefreshSkuAfterCreateAddress(),
                                new collectionStoreActions.FetchCollectionListRequestAction(),
                                new collectionV3StoreActions.FetchHomeLayoutListRequestAction(),
                                new collectionStoreActions.FetchPastOrderSkuListRequestAction(),
                            ];
                        } else {
                            addressSuccessActions = addressSuccessActions.concat(
                                [
                                    new addressActions.CreateAddressRequestSuccessAction(
                                        {
                                            address: newAddress,
                                            state:
                                                AddressCurrentState.NO_ACTION,
                                        }
                                    ),
                                ]
                            );
                        }

                        return addressSuccessActions;
                    }),
                    catchError((error) =>
                        observableOf(
                            new addressActions.CreateAddressRequestFailureAction(
                                error
                            )
                        )
                    )
                )
        )
    );

    @Effect()
    searchPremise$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.SearchPremiseRequestAction>(
            addressActions.ActionTypes.SEARCH_PREMISE_REQUEST_ACTION
        ),
        switchMap((action) =>
            this.apiService.searchPremise(action.payload.data).pipe(
                map((selectedPremise: SuprApi.PremiseSearchRes[]) => {
                    return new addressActions.SearchPremiseRequestSuccessAction(
                        selectedPremise
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new addressActions.SearchPremiseRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    searchPopularPremise$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.SearchPopularPremiseRequestAction>(
            addressActions.ActionTypes.SEARCH_POPULAR_PREMISE_REQUEST_ACTION
        ),
        switchMap((action) =>
            this.apiService.searchPopularPremise(action.payload.data).pipe(
                map((selectedPopularPremise: SuprApi.PremiseSearchRes[]) => {
                    return new addressActions.SearchPopularPremiseRequestSuccessAction(
                        selectedPopularPremise
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new addressActions.SearchPopularPremiseRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    searchPremiseAddress$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.SearchPremiseAddressRequestAction>(
            addressActions.ActionTypes.SEARCH_PREMISE_ADDRESS_REQUEST_ACTION
        ),
        switchMap((action) =>
            this.apiService.searchPremiseAddress(action.payload.data).pipe(
                map(
                    (selectedPremiseAddress: SuprApi.PremiseAddressRes) =>
                        new addressActions.SearchPremiseAddressRequestSuccessAction(
                            selectedPremiseAddress
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new addressActions.SearchPremiseAddressRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    refreshCatalog$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.RefreshCatalogAfterCreateAddress>(
            addressActions.ActionTypes
                .REFRESH_CATALOG_AFTER_ADDRESS_CREATE_ACTION
        ),
        switchMap(() => {
            return forkJoin([
                new featuredStoreActions.FetchFeaturedRequestAction(),
                new categoryStoreActions.FetchCategoryListRequestAction(),
                new collectionStoreActions.FetchCollectionListRequestAction(),
                new collectionStoreActions.FetchPastOrderSkuListRequestAction(),
                new essentialStoreActions.FetchEssentialListRequestAction(),
                new cartStoreActions.ClearCartAction(),
            ]).pipe(
                map(() => {
                    return new addressActions.RefreshDataAfterCreateAddressDone();
                }),
                catchError(() =>
                    observableOf(
                        new addressActions.RefreshDataAfterCreateAddressDone()
                    )
                )
            );
        })
    );

    @Effect()
    refreshSkus$: Observable<Action> = this.actions$.pipe(
        ofType<addressActions.RefreshSkuAfterCreateAddress>(
            addressActions.ActionTypes.REFRESH_SKU_AFTER_ADDRESS_CREATE_ACTION
        ),
        switchMap(() => {
            // clear cart from DB
            this.cartService.clearCartFromDB();

            return this.apiService.fetchSkuList().pipe(
                switchMap((skuList) => [
                    new skuStoreActions.FetchSkuListRequestSuccessAction(
                        skuList
                    ),
                    new addressActions.RefreshCatalogAfterCreateAddress(),
                    new skuStoreActions.FetchSkuAttributesRequestAction({
                        refresh: true,
                    }),
                ]),
                catchError(() =>
                    observableOf(
                        new addressActions.RefreshDataAfterCreateAddressDone()
                    )
                )
            );
        })
    );

    private sendAnalyticsEvents(address: Address) {
        try {
            const hub = address.hub ? address.hub.id : address.hub;
            const city = address.city ? address.city.id : address.city;

            this.moengageService.trackAddressChange({
                address_type: address.type,
                society_id: address.societyId,
                hub,
                city,
            });
        } catch (_error) {}
    }

    private hasAddressHubChanged(
        newAddress: Address,
        oldAddress: Address
    ): boolean {
        return (
            oldAddress && newAddress && newAddress.hub.id !== oldAddress.hub.id
        );
    }
}
