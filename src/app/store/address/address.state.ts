import { Address } from "app/shared/models";
import { SuprApi } from "@types";

export enum AddressCurrentState {
    NO_ACTION = "0",
    FETCHING_ADDRESS = "1",
    CREATING_ADDRESS = "2",
    SEARCHING_SOCIETIES = "3",
    SEARCHING_SOCIETY_DETAILS = "4",
    REFRESHING_ON_HUB_CHANGE = "5",
    SEARCHING_POPULAR_SOCIETIES = "6",
}

export interface AddressState {
    currentState: AddressCurrentState;
    error?: any;
    selectedAddress: Address;
    premise?: SuprApi.PremiseSearchRes[];
    popularPremise?: SuprApi.PremiseSearchRes[];
    premiseAddress?: SuprApi.PremiseAddressRes;
    showAddressRetroModal: boolean;
}

export const initialState: AddressState = {
    currentState: AddressCurrentState.NO_ACTION,
    error: null,
    selectedAddress: null,
    premise: [],
    popularPremise: [],
    premiseAddress: null,
    showAddressRetroModal: false,
};
