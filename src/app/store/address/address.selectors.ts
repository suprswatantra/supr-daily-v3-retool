import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Address, Building } from "@models";
import { AddressCurrentState, AddressState } from "./address.state";
import { SuprApi } from "@types";

export const selectFeature = createFeatureSelector<AddressState>("address");

export const selectAddress = createSelector(
    selectFeature,
    (state: AddressState) => state.selectedAddress
);

export const selectError = createSelector(
    selectFeature,
    (state: AddressState) => state.error
);

export const selectPremise = createSelector(
    selectFeature,
    (state: AddressState) => state.premise
);

export const selectPremiseAddress = createSelector(
    selectFeature,
    (state: AddressState) => state.premiseAddress
);

export const selectPopularPremiseAddress = createSelector(
    selectFeature,
    (state: AddressState) => state.popularPremise
);

export const selectBuildings = createSelector(
    selectFeature,
    (state: AddressState) =>
        getBuildingsFromPremiseDetails(state.premiseAddress)
);

export const selectAddressCurrentState = createSelector(
    selectFeature,
    (state: AddressState) => state.currentState
);

export const selectAddressError = createSelector(
    selectFeature,
    (state: AddressState) => state.error
);

export const selectCity = createSelector(selectFeature, (state: AddressState) =>
    state.selectedAddress ? state.selectedAddress.city : null
);

export const selectHasAddress = createSelector(
    selectFeature,
    (state: AddressState) => isAddressPresent(state.selectedAddress)
);

export const selectIsFetchingSocieties = createSelector(
    selectFeature,
    (state: AddressState) => isFetchingPremiseList(state.currentState)
);

export const selectAddressRetroShowModal = createSelector(
    selectFeature,
    (state: AddressState) => state.showAddressRetroModal
);

/*************************************
 *        Helper methods             *
 *************************************/

function isAddressPresent(address: Address): boolean {
    if (address && address.id && address.type !== "skip") {
        return true;
    }

    return false;
}

function isFetchingPremiseList(addressState: AddressCurrentState): boolean {
    return addressState === AddressCurrentState.SEARCHING_SOCIETIES;
}

function getBuildingsFromPremiseDetails(
    premise: SuprApi.PremiseAddressRes
): Building[] {
    if (!premise) {
        return;
    }

    return premise.buildings;
}
