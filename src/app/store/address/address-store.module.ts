import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { addressReducer } from "./address.reducer";
import { AddressStoreEffects } from "./address.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("address", addressReducer),
        EffectsModule.forFeature([AddressStoreEffects]),
    ],
    providers: [],
})
export class AddressStoreModule {}
