import { createSelector, createFeatureSelector } from "@ngrx/store";

import { selectUrlParams } from "@store/router/router.selectors";

import { EssentialsState } from "./essentials.state";

export const selectFeature = createFeatureSelector<EssentialsState>(
    "essentials"
);

export const selectEssentialList = createSelector(
    selectFeature,
    (state: EssentialsState) => state.essentialList
);

export const selectEssentialDetailsList = createSelector(
    selectFeature,
    (state: EssentialsState) => state.essentialDetailsList
);

export const selectEssentialDetailsById = createSelector(
    selectEssentialDetailsList,
    selectUrlParams,
    (essentialDict, urlParams) => essentialDict[urlParams["categoryId"]]
);

export const selectEssentialsCurrentState = createSelector(
    selectFeature,
    (state: EssentialsState) => state.currentState
);

export const selectEssentialsError = createSelector(
    selectFeature,
    (state: EssentialsState) => state.error
);
