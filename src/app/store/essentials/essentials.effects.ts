import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import { ApiService } from "@services/data/api.service";

import * as essentialsActions from "./essentials.actions";

@Injectable()
export class EssentialsStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchEssentialList$: Observable<Action> = this.actions$.pipe(
        ofType<essentialsActions.FetchEssentialListRequestAction>(
            essentialsActions.ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_ACTION
        ),
        switchMap(action =>
            this.apiService.fetchEssentialList(action.payload).pipe(
                map(
                    essentialList =>
                        new essentialsActions.FetchEssentialListRequestSuccessAction(
                            essentialList
                        )
                ),
                catchError(error =>
                    observableOf(
                        new essentialsActions.FetchEssentialListRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
}
