import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { essentialsReducer } from "./essentials.reducer";
import { EssentialsStoreEffects } from "./essentials.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("essentials", essentialsReducer),
        EffectsModule.forFeature([EssentialsStoreEffects]),
    ],
    providers: [],
})
export class EssentialsStoreModule {}
