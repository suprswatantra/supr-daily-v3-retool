import { Category } from "@models";
import { CategoryDict } from "@types";

export enum EssentialsCurrentState {
    NO_ACTION = "0",
    FETCHING_ESSENTIALS = "1",
    FETCHING_ESSENTIALS_DONE = "2",
}

export interface EssentialsState {
    currentState: EssentialsCurrentState;
    error?: any;
    essentialList: Category[];
    essentialDetailsList: CategoryDict;
}

export const initialState: EssentialsState = {
    currentState: EssentialsCurrentState.NO_ACTION,
    error: null,
    essentialList: [],
    essentialDetailsList: {},
};
