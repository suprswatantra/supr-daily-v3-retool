import { Action } from "@ngrx/store";
import { Category } from "@models";

export enum ActionTypes {
    FETCH_ESSENTIAL_LIST_REQUEST_ACTION = "[Essentials] Fetch essential list request",
    FETCH_ESSENTIAL_LIST_REQUEST_SUCCESS_ACTION = "[Essentials] Fetch essential list request success",
    FETCH_ESSENTIAL_LIST_REQUEST_FAILURE_ACTION = "[Essentials] Fetch essential list request failure",
}

export class FetchEssentialListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_ACTION;
    constructor(public payload?: boolean) {}
}

export class FetchEssentialListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: Category[]) {}
}

export class FetchEssentialListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchEssentialListRequestAction
    | FetchEssentialListRequestSuccessAction
    | FetchEssentialListRequestFailureAction;
