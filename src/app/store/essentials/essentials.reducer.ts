import { Actions, ActionTypes } from "./essentials.actions";
import {
    initialState,
    EssentialsState,
    EssentialsCurrentState,
} from "./essentials.state";

import { Category } from "@models";
import { CategoryDict } from "@types";

export function essentialsReducer(
    state = initialState,
    action: Actions
): EssentialsState {
    switch (action.type) {
        case ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: EssentialsCurrentState.FETCHING_ESSENTIALS,
            };
        }

        case ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: EssentialsCurrentState.FETCHING_ESSENTIALS_DONE,
                essentialList: action.payload,
                essentialDetailsList: convertEssentialsListToDict(
                    action.payload
                ),
            };
        }

        case ActionTypes.FETCH_ESSENTIAL_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: EssentialsCurrentState.FETCHING_ESSENTIALS_DONE,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}

// Helper methods
function convertEssentialsListToDict(essentialList: Category[]): CategoryDict {
    return essentialList.reduce((dict, category) => {
        dict[category.id] = { ...category };
        return dict;
    }, {});
}
