import { Injectable } from "@angular/core";

import { Effect, ofType, Actions } from "@ngrx/effects";

import { Observable, of as observableOf } from "rxjs";
import { switchMap } from "rxjs/operators";

import { SuprDateService } from "app/services/date/supr-date.service";
import * as calendarActions from "./calendar.actions";

@Injectable()
export class CalendarStoreEffects {
    constructor(
        private suprDateService: SuprDateService,
        private actions$: Actions
    ) {}

    @Effect()
    updateVacation$: Observable<any> = this.actions$.pipe(
        ofType<calendarActions.UpdateVacationAction>(
            calendarActions.ActionTypes.UPDATE_VACATION_DATA
        ),
        switchMap(action => {
            const { start_date, end_date } = action.payload.data;
            const startDate = this.suprDateService.suprDateFromText(start_date);
            const endDate = this.suprDateService.suprDateFromText(end_date);

            return observableOf(
                new calendarActions.UpdateCalendarAction({
                    data: {
                        vacationStartSuprDate: startDate,
                        vacationEndSuprDate: endDate,
                    },
                })
            );
        })
    );
}
