import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CalendarState } from "./calendar.state";

export const selectFeature = createFeatureSelector<CalendarState>("calendar");

export const selectCurrentDate = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar.currentSuprDate
);

export const selectSelectedDate = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar.selectedSuprDate
);

export const selectNextAvblDate = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar.nextAvailableSuprDate
);

export const selectTomorrowDate = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar.tomorrowSuprDate
);

export const selectYearData = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar.yearData
);

export const selectCalendarData = createSelector(
    selectFeature,
    (state: CalendarState) => state.calendar
);
