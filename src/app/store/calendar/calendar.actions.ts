import { Action } from "@ngrx/store";
import { Vacation } from "@models";

export enum ActionTypes {
    UPDATE_CALENDAR = "[Calendar] Updating the data",
    UPDATE_VACATION_DATA = "[Calendar] Updating the vacation data",
}

export class UpdateCalendarAction implements Action {
    readonly type = ActionTypes.UPDATE_CALENDAR;
    constructor(public payload: { data: any }) {}
}

export class UpdateVacationAction implements Action {
    readonly type = ActionTypes.UPDATE_VACATION_DATA;
    constructor(public payload: { data: Vacation }) {}
}
export type Actions = UpdateCalendarAction | UpdateVacationAction;
