import { Actions, ActionTypes } from "./calendar.actions";
import { initialState, CalendarState } from "./calendar.state";

export function calendarReducer(
    state = initialState,
    action: Actions
): CalendarState {
    switch (action.type) {
        case ActionTypes.UPDATE_CALENDAR:
            return {
                calendar: {
                    ...state.calendar,
                    ...action.payload.data,
                },
            };

        default:
            return state;
    }
}
