import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { calendarReducer } from "./calendar.reducer";
import { CalendarStoreEffects } from "./calendar.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("calendar", calendarReducer),
        EffectsModule.forFeature([CalendarStoreEffects]),
    ],
    providers: [],
})
export class CalendarStoreModule {}
