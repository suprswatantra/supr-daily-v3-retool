export interface CalendarState {
    calendar: any;
}

export const initialState: CalendarState = {
    calendar: null,
};
