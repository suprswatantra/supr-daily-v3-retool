import { createSelector, createFeatureSelector } from "@ngrx/store";
import { selectUrlParams } from "@store/router/router.selectors";
import { RewardsState, RewardsCurrentState } from "./rewards.state";
import {
    ScratchCard,
    ScratchedCardDict,
    ScratchedCardDetails,
} from "@shared/models";

export const selectFeature = createFeatureSelector<RewardsState>("rewards");

export const selectRewardsStoreCurrentState = createSelector(
    selectFeature,
    (state: RewardsState) => state.currentState
);

export const selectIsFetchingRewardsInfo = createSelector(
    selectFeature,
    (state: RewardsState) =>
        state.currentState === RewardsCurrentState.FETCHING_REWARDS
);

export const selectIsFetchingScratchCardBenefit = createSelector(
    selectFeature,
    (state: RewardsState) =>
        state.currentState === RewardsCurrentState.FETCHING_SCRATCH_CARD_BENEFIT
);

export const selectRewardsError = createSelector(
    selectFeature,
    (state: RewardsState) => state.error
);

export const selectRewardsInfo = createSelector(
    selectFeature,
    (state: RewardsState) => state.rewardsInfo
);

export const selectRewardsScratchCards = createSelector(
    selectFeature,
    (state: RewardsState) =>
        (state.rewardsInfo && state.rewardsInfo.scratchCards) || null
);

export const selectRewardsScratchedCardDetailsById = createSelector(
    selectFeature,
    (state: RewardsState, props: { scratchCardId: number }) =>
        getScratchedCardDetailsById(
            state.scratchedCardDict,
            props && props.scratchCardId
        )
);

export const selectScratchCardByUrlParams = createSelector(
    selectFeature,
    selectUrlParams,
    (state: RewardsState, urlParams) =>
        getScratchCardDetailsById(
            (state.rewardsInfo && state.rewardsInfo.scratchCards) || null,
            urlParams && urlParams["scratchCardId"]
        )
);

export const selectRewardsFaqs = createSelector(
    selectFeature,
    (state: RewardsState) => (state.rewardsInfo && state.rewardsInfo.faqs) || []
);

export const selectRewardsActivity = createSelector(
    selectFeature,
    (state: RewardsState) =>
        (state.rewardsInfo && state.rewardsInfo.pastRewards) || []
);

function getScratchCardDetailsById(
    scratchCardList: ScratchCard[],
    id: string
): ScratchCard {
    if (!isLengthyArray(scratchCardList) || !id) {
        return null;
    }

    return scratchCardList.find(
        (scratchCard) => scratchCard.id.toString() === id
    );
}

function getScratchedCardDetailsById(
    scratchedCardDict: ScratchedCardDict,
    id: number
): ScratchedCardDetails {
    if (!scratchedCardDict || !id) {
        return null;
    }
    return scratchedCardDict[id];
}

function isLengthyArray(item): boolean {
    return Array.isArray(item) && item.length > 0;
}
