import { Action } from "@ngrx/store";

import { RewardsInfo, ScratchedCardDetails } from "@shared/models";

export enum ActionTypes {
    FETCH_REWARDS_REQUEST = "[Rewards] Fetch Request",
    FETCH_REWARDS_REQUEST_SUCCESS = "[Rewards] Fetch Request Success",
    FETCH_REWARDS_REQUEST_FAILURE = "[Rewards] Fetch Request Failure",

    FETCH_SCRATCH_CARD_BENEFIT_REQUEST = "[Scratch card benefit] Fetch Request",
    FETCH_SCRATCH_CARD_BENEFIT_REQUEST_SUCCESS = "[Scratch card benefit] Fetch Request Success",
    FETCH_SCRATCH_CARD_BENEFIT_REQUEST_FAILURE = "[Scratch card benefit] Fetch Request Failure",
}

export class FetchRewardsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_REWARDS_REQUEST;
    constructor() {}
}

export class FetchRewardsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_REWARDS_REQUEST_SUCCESS;
    constructor(public payload: RewardsInfo) {}
}

export class FetchRewardsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_REWARDS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class FetchScratchCardBenefitRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST;
    constructor(public payload: { id: number }) {}
}

export class FetchScratchCardBenefitRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST_SUCCESS;
    constructor(public payload: ScratchedCardDetails) {}
}

export class FetchScratchCardBenefitRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchRewardsRequestAction
    | FetchRewardsRequestSuccessAction
    | FetchRewardsRequestFailureAction
    | FetchScratchCardBenefitRequestAction
    | FetchScratchCardBenefitRequestSuccessAction
    | FetchScratchCardBenefitRequestFailureAction;
