import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { rewardsReducer } from "./rewards.reducer";
import { RewardsEffects } from "./rewards.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("rewards", rewardsReducer),
        EffectsModule.forFeature([RewardsEffects]),
    ],
    providers: [],
})
export class RewardsStoreModule {}
