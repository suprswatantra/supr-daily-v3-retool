import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap, map } from "rxjs/operators";

import { RewardsInfo, ScratchedCardDetails } from "@shared/models";

import { ApiService } from "@services/data/api.service";

import * as RewardsActions from "./rewards.actions";

@Injectable()
export class RewardsEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchRewardsInfoEffect$: Observable<Action> = this.actions$.pipe(
        ofType<RewardsActions.FetchRewardsRequestAction>(
            RewardsActions.ActionTypes.FETCH_REWARDS_REQUEST
        ),
        switchMap((_action) =>
            this.apiService.fetchRewardsInfo().pipe(
                map(
                    (data: RewardsInfo) =>
                        new RewardsActions.FetchRewardsRequestSuccessAction(
                            data
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new RewardsActions.FetchRewardsRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchScratchCardBenefitEffect$: Observable<Action> = this.actions$.pipe(
        ofType<RewardsActions.FetchScratchCardBenefitRequestAction>(
            RewardsActions.ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST
        ),
        switchMap((action) => {
            return this.apiService
                .fetchScratchCardBenefit(action.payload.id)
                .pipe(
                    switchMap((data: ScratchedCardDetails) => {
                        let actions: Action[] = [
                            new RewardsActions.FetchScratchCardBenefitRequestSuccessAction(
                                data
                            ),
                        ];

                        if (data) {
                            actions = actions.concat([
                                new RewardsActions.FetchRewardsRequestAction(),
                            ]);
                        }

                        return actions;
                    }),
                    catchError((error) =>
                        observableOf(
                            new RewardsActions.FetchScratchCardBenefitRequestFailureAction(
                                error
                            )
                        )
                    )
                );
        })
    );
}
