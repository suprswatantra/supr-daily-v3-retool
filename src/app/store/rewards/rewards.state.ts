import { RewardsInfo, ScratchedCardDict } from "@models";

export enum RewardsCurrentState {
    IDLE = "idle",
    FETCHING_REWARDS = "fetching_rewards",
    FETCHING_SCRATCH_CARD_BENEFIT = "fetching_scratch_card_benefit",
}

export interface RewardsState {
    currentState: RewardsCurrentState;
    error?: any;
    rewardsInfo: RewardsInfo | null;
    scratchedCardDict: ScratchedCardDict;
}

export const initialState: RewardsState = {
    currentState: RewardsCurrentState.IDLE,
    error: null,
    rewardsInfo: null,
    scratchedCardDict: null,
};
