import { Actions, ActionTypes } from "./rewards.actions";
import {
    initialState,
    RewardsCurrentState,
    RewardsState,
} from "./rewards.state";
import { ScratchedCardDict, ScratchedCardDetails } from "@shared/models";

export function rewardsReducer(
    state = initialState,
    action: Actions
): RewardsState {
    switch (action.type) {
        case ActionTypes.FETCH_REWARDS_REQUEST: {
            return {
                ...state,
                currentState: RewardsCurrentState.FETCHING_REWARDS,
                error: null,
            };
        }
        case ActionTypes.FETCH_REWARDS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: RewardsCurrentState.IDLE,
                rewardsInfo: action.payload,
                error: null,
            };
        }
        case ActionTypes.FETCH_REWARDS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: RewardsCurrentState.IDLE,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST: {
            return {
                ...state,
                currentState: RewardsCurrentState.FETCHING_SCRATCH_CARD_BENEFIT,
                error: null,
            };
        }
        case ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: RewardsCurrentState.IDLE,
                scratchedCardDict: updateScratchCardDict(
                    state.scratchedCardDict,
                    action.payload
                ),
                error: null,
            };
        }
        case ActionTypes.FETCH_SCRATCH_CARD_BENEFIT_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: RewardsCurrentState.IDLE,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}

/*******************************************
 *         Helper methods                  *
 *******************************************/

function updateScratchCardDict(
    scratchedCardDict: ScratchedCardDict,
    scratchedCard: ScratchedCardDetails
): ScratchedCardDict {
    const cardDict = { ...scratchedCardDict };
    cardDict[scratchedCard.id] = scratchedCard;

    return cardDict;
}
