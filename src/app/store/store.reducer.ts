import { routerReducer } from "@ngrx/router-store";
import {
    ActionReducerMap,
    MetaReducer,
    ActionReducer,
    Action,
} from "@ngrx/store";
import { storeFreeze } from "ngrx-store-freeze";

import { environment } from "@environments/environment";
import { StoreState } from "./store.state";

import { AuthStoreActions } from "app/store/auth";

import { walletReducer } from "./wallet/wallet.reducer";
import { userReducer } from "./user/user.reducer";
import { authReducer } from "./auth/auth.reducer";
import { addressReducer } from "./address/address.reducer";
import { feedbackReducer } from "./feedback/feedback.reducer";
import { skuReducer } from "./sku/sku.reducer";
import { categoryReducer } from "./category/category.reducer";
import { essentialsReducer } from "./essentials/essentials.reducer";
import { featuredReducer } from "./featured/featured.reducer";
import { collectionReducer } from "./collection/collection.reducer";
import { cartReducer } from "./cart/cart.reducer";
import { miscReducer } from "./misc/misc.reducer";
import { pnReducer } from "./pn/pn.reducer";
import { deliveryStatusReducer } from "./delivery-status/delivery-status.reducer";
import { scheduleReducer } from "./schedule/schedule.reducer";
import { subscriptionReducer } from "./subscription/subscription.reducer";
import { vacationReducer } from "./vacation/vacation.reducer";
import { errorReducer } from "./error/error.reducer";
import { ratingReducer } from "./rating/rating.reducer";
import { suprPassReducer } from "./supr-pass/supr-pass.reducer";
import { referralReducer } from "./referral/referral.reducer";
import { rewardsReducer } from "./rewards/rewards.reducer";
import { homeLayoutReducer } from "./home-layout/home-layout.reducer";
import { searchReducer } from "./search/search.reducer";
import { mileStoneReducer } from "./milestone/milestone.reducer";
import { offersReducer } from "./offers/offers.reducer";

export const rootReducer: ActionReducerMap<StoreState> = {
    wallet: walletReducer,
    user: userReducer,
    auth: authReducer,
    address: addressReducer,
    feedback: feedbackReducer,
    sku: skuReducer,
    category: categoryReducer,
    essentials: essentialsReducer,
    featured: featuredReducer,
    collection: collectionReducer,
    cart: cartReducer,
    misc: miscReducer,
    pn: pnReducer,
    router: routerReducer,
    deliveryStatus: deliveryStatusReducer,
    schedule: scheduleReducer,
    subscription: subscriptionReducer,
    vacation: vacationReducer,
    error: errorReducer,
    rating: ratingReducer,
    suprPass: suprPassReducer,
    referral: referralReducer,
    rewards: rewardsReducer,
    homeLayout: homeLayoutReducer,
    search: searchReducer,
    milestone: mileStoneReducer,
    offers: offersReducer,
};

export function clearStore(
    reducer: ActionReducer<StoreState>
): ActionReducer<StoreState> {
    return function (state: StoreState, action: Action): StoreState {
        if (action.type === AuthStoreActions.ActionTypes.LOGOUT) {
            state = undefined;
        }
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<StoreState>[] = !environment.production
    ? [storeFreeze, clearStore]
    : [clearStore];
