import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CityState } from "./city.state";

export const selectCityFeature = createFeatureSelector<CityState>("city");

export const selectCityList = createSelector(
    selectCityFeature,
    (state: CityState) => state.cityList
);
