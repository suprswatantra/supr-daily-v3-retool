import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { cityReducer } from "./city.reducer";
import { CityStoreEffects } from "./city.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("city", cityReducer),
        EffectsModule.forFeature([CityStoreEffects]),
    ],
    providers: [],
})
export class CityStoreModule {}
