import { City } from "app/shared/models";

export interface CityState {
    isLoading?: boolean;
    error?: any;
    cityList: City[];
}

export const initialState: CityState = {
    isLoading: false,
    error: null,
    cityList: [],
};
