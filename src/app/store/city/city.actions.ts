import { Action } from "@ngrx/store";
import { City } from "../../shared/models/city.model";

export enum ActionTypes {
    LOAD_CITY_LIST_REQUEST = "[City] Load city list request",
    LOAD_CITY_LIST_REQUEST_SUCCESS = "[City] Load city list request success",
    LOAD_CITY_LIST_REQUEST_FAILURE = "[City] Load city list request failure",
}

export class LoadCityListRequestAction implements Action {
    readonly type = ActionTypes.LOAD_CITY_LIST_REQUEST;
}

export class LoadCityListRequestSuccessAction implements Action {
    readonly type = ActionTypes.LOAD_CITY_LIST_REQUEST_SUCCESS;
    constructor(public payload: City[]) {}
}

export class LoadCityListRequestFailureAction implements Action {
    readonly type = ActionTypes.LOAD_CITY_LIST_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | LoadCityListRequestAction
    | LoadCityListRequestSuccessAction
    | LoadCityListRequestFailureAction;
