import { Actions, ActionTypes } from "./city.actions";
import { initialState, CityState } from "./city.state";

export function cityReducer(state = initialState, action: Actions): CityState {
    switch (action.type) {
        case ActionTypes.LOAD_CITY_LIST_REQUEST: {
            return {
                ...state,
                isLoading: true,
                error: null,
            };
        }
        case ActionTypes.LOAD_CITY_LIST_REQUEST_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                error: null,
                cityList: action.payload,
            };
        }
        case ActionTypes.LOAD_CITY_LIST_REQUEST_FAILURE: {
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}
