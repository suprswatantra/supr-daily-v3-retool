import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import { ApiService } from "@services/data/api.service";

import * as categoryActions from "./category.actions";

@Injectable()
export class CategoryStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchCategoryList$: Observable<Action> = this.actions$.pipe(
        ofType<categoryActions.FetchCategoryListRequestAction>(
            categoryActions.ActionTypes.FETCH_CATEGORY_LIST_REQUEST_ACTION
        ),
        switchMap(action =>
            this.apiService.fetchCategoryList(action.payload).pipe(
                map(
                    categories =>
                        new categoryActions.FetchCategoryListRequestSuccessAction(
                            categories
                        )
                ),
                catchError(error =>
                    observableOf(
                        new categoryActions.FetchCategoryListRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchCategoryDetails$: Observable<Action> = this.actions$.pipe(
        ofType<categoryActions.FetchCategoryDetailsRequestAction>(
            categoryActions.ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_ACTION
        ),
        switchMap(action => {
            const { categoryId } = action.payload;
            return this.apiService.fetchCategoryDetails(categoryId).pipe(
                map(
                    category =>
                        new categoryActions.FetchCategoryDetailsRequestSuccessAction(
                            { category }
                        )
                ),
                catchError(error =>
                    observableOf(
                        new categoryActions.FetchCategoryDetailsRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );
}
