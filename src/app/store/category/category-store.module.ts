import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { categoryReducer } from "./category.reducer";
import { CategoryStoreEffects } from "./category.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("category", categoryReducer),
        EffectsModule.forFeature([CategoryStoreEffects]),
    ],
    providers: [],
})
export class CategoryStoreModule {}
