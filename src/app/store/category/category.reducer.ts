import { Actions, ActionTypes } from "./category.actions";
import {
    initialState,
    CategoryState,
    CategoryCurrentState,
} from "./category.state";

import { CategoryMeta } from "@models";

import { CategoryMetaDict } from "@types";

const convertListToDict = (categories: CategoryMeta[]): CategoryMetaDict =>
    categories.reduce((dict, category) => {
        if (category && category.id) {
            dict[category.id] = { ...category };
        }

        return dict;
    }, {});

export function categoryReducer(
    state = initialState,
    action: Actions
): CategoryState {
    switch (action.type) {
        case ActionTypes.FETCH_CATEGORY_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: CategoryCurrentState.FETCHING_CATEGORIES,
            };
        }

        case ActionTypes.FETCH_CATEGORY_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                categoryDetailsList: {},
                categoryList: action.payload,
                currentState: CategoryCurrentState.NO_ACTION,
                categoryMetaDict: convertListToDict(action.payload),
            };
        }

        case ActionTypes.FETCH_CATEGORY_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_ACTION: {
            return {
                ...state,
                currentState: CategoryCurrentState.FETCHING_CATEGORY_DETAILS,
                error: null,
            };
        }

        case ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                currentState: CategoryCurrentState.NO_ACTION,
                error: null,
                categoryDetailsList: {
                    ...state.categoryDetailsList,
                    [action.payload.category.id]: action.payload.category,
                },
            };
        }

        case ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: CategoryCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}
