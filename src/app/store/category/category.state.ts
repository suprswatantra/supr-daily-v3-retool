import { Category } from "@models";
import { CategoryDict, CategoryMetaDict } from "@types";

export enum CategoryCurrentState {
    NO_ACTION = "0",
    FETCHING_CATEGORIES = "1",
    FETCHING_CATEGORY_DETAILS = "2",
}

export interface CategoryState {
    error?: any;
    categoryList: Category[];
    categoryDetailsList: CategoryDict;
    categoryMetaDict: CategoryMetaDict;
    currentState?: CategoryCurrentState;
}

export const initialState: CategoryState = {
    currentState: CategoryCurrentState.NO_ACTION,
    error: null,
    categoryList: [],
    categoryMetaDict: {},
    categoryDetailsList: {},
};
