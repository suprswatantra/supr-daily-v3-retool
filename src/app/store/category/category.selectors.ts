import { createSelector, createFeatureSelector } from "@ngrx/store";

import { selectUrlParams } from "@store/router/router.selectors";

import { CategoryState, CategoryCurrentState } from "./category.state";
import { CategoryDict, CategoryMetaDict } from "@types";

export const selectFeature = createFeatureSelector<CategoryState>("category");

export const selectCategoryList = createSelector(
    selectFeature,
    (state: CategoryState) => state.categoryList
);

export const selectCategoryDetailsDict = createSelector(
    selectFeature,
    (state: CategoryState) => state.categoryDetailsList
);

export const selectCategoryMetaDict = createSelector(
    selectFeature,
    (state: CategoryState) => state.categoryMetaDict
);

export const selectCategoryDetailsFromUrlParams = createSelector(
    selectCategoryDetailsDict,
    selectUrlParams,
    (categoryDict, urlParams) => categoryDict[urlParams["categoryId"]]
);

export const selectCategoryDetailsById = createSelector(
    selectCategoryDetailsDict,
    (categoryDict: CategoryDict, props: { categoryId: number }) =>
        categoryDict[props.categoryId]
);

export const selectCategoryMetaById = createSelector(
    selectCategoryMetaDict,
    (categoryMetaDict: CategoryMetaDict, props: { categoryId: number }) =>
        categoryMetaDict[props.categoryId]
);

export const selectCategoryState = createSelector(
    selectFeature,
    (state: CategoryState) => state.currentState
);

export const selectIsFetchingCategories = createSelector(
    selectFeature,
    (state: CategoryState) =>
        state.currentState === CategoryCurrentState.FETCHING_CATEGORIES
);
