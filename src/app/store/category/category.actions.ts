import { Action } from "@ngrx/store";
import { Category, CategoryMeta } from "app/shared/models/category.model";

export enum ActionTypes {
    FETCH_CATEGORY_LIST_REQUEST_ACTION = "[CATEGORY] Fetch category list request",
    FETCH_CATEGORY_LIST_REQUEST_SUCCESS_ACTION = "[CATEGORY] Fetch category list request success",
    FETCH_CATEGORY_LIST_REQUEST_FAILURE_ACTION = "[CATEGORY] Fetch category list request failure",

    FETCH_CATEGORY_DETAILS_REQUEST_ACTION = "[CATEGORY] Fetch category details request",
    FETCH_CATEGORY_DETAILS_REQUEST_SUCCESS_ACTION = "[CATEGORY] Fetch category details request success",
    FETCH_CATEGORY_DETAILS_REQUEST_FAILURE_ACTION = "[CATEGORY] Fetch category details request failure",

    FETCH_ESSENTIAL_LIST_REQUEST_ACTION = "[CATEGORY] Fetch essential list request",
    FETCH_ESSENTIAL_LIST_REQUEST_SUCCESS_ACTION = "[CATEGORY] Fetch essential list request success",
    FETCH_ESSENTIAL_LIST_REQUEST_FAILURE_ACTION = "[CATEGORY] Fetch essential list request failure",
}

export class FetchCategoryListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_LIST_REQUEST_ACTION;
    constructor(public payload?: boolean) {}
}

export class FetchCategoryListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: CategoryMeta[]) {}
}

export class FetchCategoryListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchCategoryDetailsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_ACTION;
    constructor(public payload: { categoryId: number }) {}
}

export class FetchCategoryDetailsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: { category: Category }) {}
}

export class FetchCategoryDetailsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_CATEGORY_DETAILS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchCategoryListRequestAction
    | FetchCategoryListRequestSuccessAction
    | FetchCategoryListRequestFailureAction
    | FetchCategoryDetailsRequestAction
    | FetchCategoryDetailsRequestSuccessAction
    | FetchCategoryDetailsRequestFailureAction;
