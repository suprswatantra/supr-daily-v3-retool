import { Injectable } from "@angular/core";

import { Action, Store, select } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, from, of } from "rxjs";
import {
    map,
    mergeMap,
    switchMap,
    catchError,
    withLatestFrom,
} from "rxjs/operators";

import {
    LOCAL_DB_DATA_KEYS,
    SEARCH_INSTRUMENTATION_EVENT_TYPE,
    SEARCH_INSTRUMENTATION_EVENT_NAME,
} from "@constants";
import { SearchInstrumentation, CartItem } from "@models";

import { DbService } from "@services/data/db.service";
import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { SettingsService } from "@services/shared/settings.service";

import { StoreState } from "@store/store.state";
import { UserStoreSelectors } from "@store/user";

import * as SearchActions from "./search.actions";
import * as SearchSelectors from "./search.selectors";

@Injectable()
export class SearchStoreEffects {
    constructor(
        private actions$: Actions,
        private dbService: DbService,
        private apiService: ApiService,
        private utilService: UtilService,
        private store$: Store<StoreState>,
        private settingsService: SettingsService
    ) {}

    @Effect()
    sendInstrumentationInfo$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.SendSearchInstrumentationAction>(
            SearchActions.ActionTypes.SEND_SEARCH_INSTRUMENTATION
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectUserId))
        ),
        mergeMap(([action, userId]) => {
            if (
                this.isSearchInstrumentationEnabled() &&
                action.payload &&
                action.payload.data
            ) {
                if (action.payload.skuId) {
                    this.saveContextInLocalDb(
                        action.payload.skuId,
                        action.payload.data
                    );
                }

                return this.apiService
                    .sendSearchInstrumentation({
                        ...action.payload.data,
                        userToken: userId && userId.toString(),
                    })
                    .pipe(
                        map(() => {
                            return new SearchActions.SendSearchInstrumentationComplete();
                        }),
                        catchError((_: any) => {
                            /* [TODO_RITESH] check what needs to be done for error handling */
                            return of(
                                new SearchActions.SendSearchInstrumentationComplete()
                            );
                        })
                    );
            } else {
                return of(
                    new SearchActions.SendSearchInstrumentationComplete()
                );
            }
        })
    );

    @Effect()
    checkSubInstrumentationContext$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.CheckForSubInstrumentationContextAction>(
            SearchActions.ActionTypes.CHECK_SUBSCRIPTION_INSTRUMENTATION_CONTEXT
        ),
        withLatestFrom(
            this.store$.pipe(select(SearchSelectors.selectInstrumentationInfo))
        ),
        map(([action, instrumentationDict]) => {
            if (
                this.isSearchInstrumentationEnabled() &&
                instrumentationDict &&
                instrumentationDict[action.payload]
            ) {
                return new SearchActions.SendSearchInstrumentationAction({
                    skuId: action.payload,
                    data: instrumentationDict[action.payload],
                });
            } else {
                return new SearchActions.SendSearchInstrumentationComplete();
            }
        })
    );

    @Effect()
    sendDeliverSubBalanceInstrumentation$: Observable<
        Action
    > = this.actions$.pipe(
        ofType<SearchActions.SendDeliverSubBalInstrumentation>(
            SearchActions.ActionTypes.SEND_DELIVER_SUB_BAL_INSTRUMENTATION
        ),
        withLatestFrom(
            this.store$.pipe(select(SearchSelectors.selectInstrumentationInfo))
        ),
        map(([action, instrumentationDict]) => {
            if (
                this.isSearchInstrumentationEnabled() &&
                instrumentationDict &&
                instrumentationDict[action.payload]
            ) {
                return new SearchActions.SendSearchInstrumentationAction({
                    skuId: action.payload,
                    data: {
                        ...instrumentationDict[action.payload],
                        eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CLICK,
                        search_eventName:
                            SEARCH_INSTRUMENTATION_EVENT_NAME.CLICK,
                    },
                });
            } else {
                return new SearchActions.SendSearchInstrumentationComplete();
            }
        })
    );

    @Effect()
    updateInstrumentationContext$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.UpdateSearchInstrumentationDb>(
            SearchActions.ActionTypes.UPDATE_INSTRUMENTATION_CONTEXT_DB
        ),
        mergeMap((action) => {
            if (
                this.isSearchInstrumentationEnabled() &&
                this.utilService.isLengthyArray(action.payload)
            ) {
                const skuIds = action.payload.map(
                    (cartItem: CartItem) => cartItem.sku_id
                );

                return from(this.updateContextInLocalDb(skuIds)).pipe(
                    map(() => {
                        return new SearchActions.UpdateInstrumentationDbComplete();
                    }),
                    catchError((_: any) => {
                        /* [TODO_RITESH] check what needs to be done for error handling */
                        return of(
                            new SearchActions.UpdateInstrumentationDbComplete()
                        );
                    })
                );
            } else {
                return of(new SearchActions.UpdateInstrumentationDbComplete());
            }
        })
    );

    @Effect()
    clearInstrumentationContext$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.ClearSearchInstrumentationDb>(
            SearchActions.ActionTypes.CLEAR_INSTRUMENTATION_CONTEXT_DB
        ),
        switchMap((_) => {
            if (this.isSearchInstrumentationEnabled()) {
                return from(this.updateContextInLocalDb()).pipe(
                    map(() => {
                        return new SearchActions.UpdateInstrumentationDbComplete();
                    }),
                    catchError((_: any) => {
                        /* [TODO_RITESH] check what needs to be done for error handling */
                        return of(
                            new SearchActions.UpdateInstrumentationDbComplete()
                        );
                    })
                );
            } else {
                return of(new SearchActions.UpdateInstrumentationDbComplete());
            }
        })
    );

    @Effect()
    sendCheckoutInstrumentation$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.SendCheckoutInstrumentation>(
            SearchActions.ActionTypes.SEND_CHECKOUT_INSTRUMENTATION
        ),
        switchMap((action) => {
            if (
                this.isSearchInstrumentationEnabled() &&
                this.utilService.isLengthyArray(action.payload)
            ) {
                return from(this.getContextFromLocalDb()).pipe(
                    switchMap((context: SearchInstrumentation) => {
                        const queryMap = this.getQueryMapForCheckout(
                            context,
                            action.payload
                        );

                        const actions: Action[] = Object.keys(queryMap)
                            .filter((queryID) =>
                                this.isValidQueryData(queryMap, queryID)
                            )
                            .map(
                                (queryId) =>
                                    new SearchActions.SendSearchInstrumentationAction(
                                        {
                                            data: queryMap[queryId],
                                        }
                                    )
                            );

                        actions.push(
                            new SearchActions.ClearSearchInstrumentationDb()
                        );

                        return actions;
                    }),
                    catchError((_: any) => {
                        /* [TODO_RITESH] check what needs to be done for error handling */
                        return of(
                            new SearchActions.UpdateInstrumentationDbComplete()
                        );
                    })
                );
            } else {
                return of(new SearchActions.UpdateInstrumentationDbComplete());
            }
        })
    );

    @Effect()
    fetchPopularSearches$: Observable<Action> = this.actions$.pipe(
        ofType<SearchActions.FetchPopularSearches>(
            SearchActions.ActionTypes.FETCH_POPULAR_SEARCHES
        ),
        switchMap((_) => {
            return this.apiService.fetchPopularSkuSearches().pipe(
                map(
                    (searches) =>
                        new SearchActions.FetchPopularSearchesComplete(searches)
                ),
                catchError(() =>
                    of(new SearchActions.FetchPopularSearchesComplete())
                )
            );
        })
    );

    private isSearchInstrumentationEnabled(): boolean {
        const backendSearch = this.settingsService.getSettingsValue(
            "backendSearch"
        );
        const instrumentationEnabled = this.settingsService.getSettingsValue(
            "searchInstrumentationEnabled"
        );

        return backendSearch && instrumentationEnabled;
    }

    private async saveContextInLocalDb(
        skuId: number,
        data: SearchInstrumentation
    ) {
        if (skuId === null || !data) {
            return;
        }

        const searchInstrumentationContext = await this.getContextFromLocalDb();
        const updatedContext = {
            ...(searchInstrumentationContext || {}),
            [skuId]: data,
        };

        this.setContextInLocalDb(
            updatedContext as Promise<SearchInstrumentation>
        );
    }

    private async updateContextInLocalDb(skuIds?: number[]): Promise<any> {
        if (!this.utilService.isLengthyArray(skuIds)) {
            return await this.dbService.clearData(
                LOCAL_DB_DATA_KEYS.SEARCH_INSTRUMENTATION_CONTEXT
            );
        }

        const searchInstrumentationContext = this.getContextFromLocalDb();

        if (this.utilService.isEmpty(searchInstrumentationContext)) {
            return Promise.resolve();
        }

        for (const skuId in searchInstrumentationContext) {
            if (skuIds.indexOf(Number(skuId)) < 0) {
                delete searchInstrumentationContext[skuId];
            }
        }

        return this.setContextInLocalDb(searchInstrumentationContext);
    }

    private async getContextFromLocalDb(): Promise<SearchInstrumentation> {
        return await this.dbService.getData(
            LOCAL_DB_DATA_KEYS.SEARCH_INSTRUMENTATION_CONTEXT
        );
    }

    private async setContextInLocalDb(
        searchInstrumentationContext: Promise<SearchInstrumentation>
    ): Promise<any> {
        const context = await searchInstrumentationContext;

        return this.dbService.setData(
            LOCAL_DB_DATA_KEYS.SEARCH_INSTRUMENTATION_CONTEXT,
            context
        );
    }

    private getQueryMapForCheckout(
        context: SearchInstrumentation,
        skuIds: Number[]
    ): { [queryId: string]: SearchInstrumentation } {
        const queryMap = {};
        /* creating payload by grouping based on query id */
        for (const skuId in context) {
            if (skuIds.indexOf(Number(skuId)) > -1 && context[skuId]) {
                const queryID = this.utilService.getNestedValue(
                    context,
                    `${skuId}.queryID`
                );

                if (!queryMap[queryID]) {
                    queryMap[queryID] = {
                        queryID,
                        search_objectIDs: [],
                        search_positions: [],
                        eventType: SEARCH_INSTRUMENTATION_EVENT_TYPE.CONVERSION,
                        search_eventName:
                            SEARCH_INSTRUMENTATION_EVENT_NAME.CONVERSION,
                    };
                }

                const objectIds = this.utilService.isLengthyArray(
                    context[skuId].search_objectIDs
                )
                    ? context[skuId].search_objectIDs
                    : [];
                const positions = this.utilService.isLengthyArray(
                    context[skuId].search_positions
                )
                    ? context[skuId].search_positions
                    : [];

                queryMap[queryID].search_objectIDs = queryMap[
                    queryID
                ].search_objectIDs.concat(objectIds);
                queryMap[queryID].search_positions = queryMap[
                    queryID
                ].search_positions.concat(positions);
            }
        }

        return queryMap;
    }

    private isValidQueryData(
        queryMap: { [queryId: string]: SearchInstrumentation },
        queryID: string
    ): boolean {
        const objectIds = this.utilService.getNestedValue(
            queryMap,
            `${queryID}.search_objectIDs`,
            []
        );

        const filteredObjectIds = objectIds.filter(
            (objectId: string) => objectId
        );

        return this.utilService.isLengthyArray(filteredObjectIds);
    }
}
