import { Actions, ActionTypes } from "./search.actions";
import { SearchState, initialState, SearchCurrentState } from "./search.state";

export function searchReducer(
    state = initialState,
    action: Actions
): SearchState {
    switch (action.type) {
        case ActionTypes.SAVE_SEARCH_INSTRUMENTATION: {
            return {
                ...state,
                instrumentationDict: {
                    ...state.instrumentationDict,
                    [action.payload.skuId]: action.payload.data,
                },
            };
        }
        case ActionTypes.CLEAR_SEARCH_INSTRUMENTATION: {
            const instrumentationDict = { ...state.instrumentationDict };
            delete instrumentationDict[action.payload];

            return {
                ...state,
                instrumentationDict: instrumentationDict,
            };
        }
        case ActionTypes.FETCH_POPULAR_SEARCHES: {
            return {
                ...state,
                currentState: SearchCurrentState.LOADING_POPULAR,
            };
        }
        case ActionTypes.FETCH_POPULAR_SEARCHES_COMPLETE: {
            return {
                ...state,
                currentState: SearchCurrentState.NO_ACTION,
                popularSearches: action.payload || state.popularSearches,
            };
        }
        default: {
            return state;
        }
    }
}
