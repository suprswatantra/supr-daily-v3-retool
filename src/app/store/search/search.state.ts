import { PopularSkuSearch } from "@models";
import { SearchInstrumentationDict } from "@types";

export enum SearchCurrentState {
    NO_ACTION = "0",
    LOADING_POPULAR = "1",
}

export interface SearchState {
    currentState?: SearchCurrentState;
    popularSearches?: PopularSkuSearch[];
    instrumentationDict?: SearchInstrumentationDict;
}

export const initialState: SearchState = {
    popularSearches: [],
    instrumentationDict: {},
    currentState: SearchCurrentState.NO_ACTION,
};
