import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { searchReducer } from "./search.reducer";
import { SearchStoreEffects } from "./search.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("search", searchReducer),
        EffectsModule.forFeature([SearchStoreEffects]),
    ],
    providers: [],
})
export class SearchStoreModule {}
