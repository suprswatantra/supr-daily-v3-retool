import { createFeatureSelector, createSelector } from "@ngrx/store";

import { SearchState } from "./search.state";

export const selectFeature = createFeatureSelector<SearchState>("search");

export const selectInstrumentationInfo = createSelector(
    selectFeature,
    (state: SearchState) => state.instrumentationDict
);

export const selectPopularSearches = createSelector(
    selectFeature,
    (state: SearchState) => state.popularSearches || []
);

export const selectSearchCurrentState = createSelector(
    selectFeature,
    (state: SearchState) => state.currentState
);
