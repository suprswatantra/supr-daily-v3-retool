import { Action } from "@ngrx/store";

import { CartItem, PopularSkuSearch, SearchInstrumentation } from "@models";

export enum ActionTypes {
    FETCH_POPULAR_SEARCHES = "[Search] Fetch Popular Searches",
    SEND_SEARCH_INSTRUMENTATION = "[Search] Send Search Instrumentation",
    SAVE_SEARCH_INSTRUMENTATION = "[Search] Save Search Instrumentation",
    CLEAR_SEARCH_INSTRUMENTATION = "[Search] Clear Search Instrumentation",
    SEND_CHECKOUT_INSTRUMENTATION = "[Search] Send Checkout Instrumentation",
    FETCH_POPULAR_SEARCHES_COMPLETE = "[Search] Fetch Popular Searches Complete",
    SEND_SEARCH_INSTRUMENTATION_COMPLETE = "[Search] Send Search Instrumentation Complete",
    UPDATE_INSTRUMENTATION_DB_COMPLETE = "[Search] Update Search Instrumentation DB Complete",
    SEND_DELIVER_SUB_BAL_INSTRUMENTATION = "[Search] Send Deliver Sub Balance Instrumentation",
    CLEAR_INSTRUMENTATION_CONTEXT_DB = "[Search] Clear Search Instrumentation In DB On Cart Clear",
    UPDATE_INSTRUMENTATION_CONTEXT_DB = "[Search] Update Search Instrumentation In DB On Cart Update",
    CHECK_SUBSCRIPTION_INSTRUMENTATION_CONTEXT = "[Search] Check For Saved Search Instrumentation Context",
}

export class SendSearchInstrumentationAction implements Action {
    readonly type = ActionTypes.SEND_SEARCH_INSTRUMENTATION;
    constructor(
        public payload: { data: SearchInstrumentation; skuId?: number }
    ) {}
}

export class SaveSearchInstrumentationAction implements Action {
    readonly type = ActionTypes.SAVE_SEARCH_INSTRUMENTATION;
    constructor(
        public payload: { data: SearchInstrumentation; skuId: number }
    ) {}
}

export class ClearSearchInstrumentationAction implements Action {
    readonly type = ActionTypes.CLEAR_SEARCH_INSTRUMENTATION;
    constructor(public payload: number) {}
}

export class UpdateSearchInstrumentationDb implements Action {
    readonly type = ActionTypes.UPDATE_INSTRUMENTATION_CONTEXT_DB;
    constructor(public payload: CartItem[]) {}
}

export class ClearSearchInstrumentationDb implements Action {
    readonly type = ActionTypes.CLEAR_INSTRUMENTATION_CONTEXT_DB;
}

export class CheckForSubInstrumentationContextAction implements Action {
    readonly type = ActionTypes.CHECK_SUBSCRIPTION_INSTRUMENTATION_CONTEXT;
    constructor(public payload: number) {}
}

export class SendDeliverSubBalInstrumentation implements Action {
    readonly type = ActionTypes.SEND_DELIVER_SUB_BAL_INSTRUMENTATION;
    constructor(public payload: number) {}
}

export class SendCheckoutInstrumentation implements Action {
    readonly type = ActionTypes.SEND_CHECKOUT_INSTRUMENTATION;
    constructor(public payload: Number[]) {}
}

export class SendSearchInstrumentationComplete implements Action {
    readonly type = ActionTypes.SEND_SEARCH_INSTRUMENTATION_COMPLETE;
}

export class UpdateInstrumentationDbComplete implements Action {
    readonly type = ActionTypes.UPDATE_INSTRUMENTATION_DB_COMPLETE;
}

export class FetchPopularSearches implements Action {
    readonly type = ActionTypes.FETCH_POPULAR_SEARCHES;
}

export class FetchPopularSearchesComplete implements Action {
    readonly type = ActionTypes.FETCH_POPULAR_SEARCHES_COMPLETE;

    constructor(public payload?: PopularSkuSearch[]) {}
}

export type Actions =
    | FetchPopularSearches
    | SendCheckoutInstrumentation
    | FetchPopularSearchesComplete
    | ClearSearchInstrumentationDb
    | UpdateSearchInstrumentationDb
    | UpdateInstrumentationDbComplete
    | SendSearchInstrumentationAction
    | SaveSearchInstrumentationAction
    | ClearSearchInstrumentationAction
    | SendDeliverSubBalInstrumentation
    | SendSearchInstrumentationComplete
    | CheckForSubInstrumentationContextAction;
