import { Action } from "@ngrx/store";

import { Auth, User } from "@models";
import { SuprApi } from "@types";

export enum ActionTypes {
    LOAD_PROFILE_REQUEST = "[User] Profile Request",
    LOAD_PROFILE_REQUEST_SUCCESS = "[User] Profile Request Success",
    LOAD_PROFILE_REQUEST_FAILURE = "[User] Profile Request Failure",

    LOAD_PROFILE_WITH_ADDRESS_REQUEST = "[User] Profile with address request",

    UPDATE_PROFILE_REQUEST = "[User] Update Profile Request",
    UPDATE_PROFILE_REQUEST_SUCCESS = "[User] Update Profile Request Success",
    UPDATE_PROFILE_REQUEST_FAILURE = "[User] Update Profile Request Failure",

    SET_USER_TYPE = "[User] Set User type",

    SET_WHATS_APP_OPT_IN_ENABLED = "[User] Set Whats App Opt In Enabled",

    SHOW_CHECKOUT_DISABLED_NUDGE = "[User] Show Checkout disabled Nudge",
    HIDE_CHECKOUT_DISABLED_NUDGE = "[User] Hide Checkout disabled Nudge",

    FETCH_ANONYMOUS_USER_TOKEN = "[User] Fetch Anonymous user token",
    FETCH_ANONYMOUS_USER_TOKEN_SUCCESS = "[User] Fetch Anonymous user token Success",
    FETCH_ANONYMOUS_USER_TOKEN_FAILURE = "[User] Fetch Anonymous user token Failure",
}

export class LoadProfileRequestAction implements Action {
    readonly type = ActionTypes.LOAD_PROFILE_REQUEST;
}

export class LoadProfileWithAddressRequestAction implements Action {
    readonly type = ActionTypes.LOAD_PROFILE_WITH_ADDRESS_REQUEST;
    constructor(public payload?: SuprApi.ProfileQueryParams) {}
}

export class LoadProfileRequestSuccessAction implements Action {
    readonly type = ActionTypes.LOAD_PROFILE_REQUEST_SUCCESS;
    constructor(public payload: User) {}
}

export class LoadProfileRequestFailureAction implements Action {
    readonly type = ActionTypes.LOAD_PROFILE_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class UpdateProfileRequestAction implements Action {
    readonly type = ActionTypes.UPDATE_PROFILE_REQUEST;
    constructor(public payload: { user: User }) {}
}

export class UpdateProfileRequestSuccessAction implements Action {
    readonly type = ActionTypes.UPDATE_PROFILE_REQUEST_SUCCESS;
    constructor(public payload: User) {}
}

export class UpdateProfileRequestFailureAction implements Action {
    readonly type = ActionTypes.UPDATE_PROFILE_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class SetUserTypeAction implements Action {
    readonly type = ActionTypes.SET_USER_TYPE;
    constructor(public payload: { userType: string }) {}
}

export class SetWhatsAppOptInEnabled implements Action {
    readonly type = ActionTypes.SET_WHATS_APP_OPT_IN_ENABLED;
}

export class ShowCheckoutDisabledNudge implements Action {
    readonly type = ActionTypes.SHOW_CHECKOUT_DISABLED_NUDGE;
}

export class HideCheckoutDisabledNudge implements Action {
    readonly type = ActionTypes.HIDE_CHECKOUT_DISABLED_NUDGE;
}

export class FetchAnonymousUserTokenRequest implements Action {
    readonly type = ActionTypes.FETCH_ANONYMOUS_USER_TOKEN;
    constructor(public payload: SuprApi.AnonymousTokenReqParams) {}
}

export class FetchAnonymousUserTokenRequestSuccess implements Action {
    readonly type = ActionTypes.FETCH_ANONYMOUS_USER_TOKEN_SUCCESS;
    constructor(public payload: Auth) {}
}

export class FetchAnonymousUserTokenRequestfailure implements Action {
    readonly type = ActionTypes.FETCH_ANONYMOUS_USER_TOKEN_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | LoadProfileRequestAction
    | LoadProfileWithAddressRequestAction
    | LoadProfileRequestSuccessAction
    | LoadProfileRequestFailureAction
    | UpdateProfileRequestAction
    | UpdateProfileRequestSuccessAction
    | UpdateProfileRequestFailureAction
    | SetUserTypeAction
    | SetWhatsAppOptInEnabled
    | ShowCheckoutDisabledNudge
    | HideCheckoutDisabledNudge
    | FetchAnonymousUserTokenRequest
    | FetchAnonymousUserTokenRequestSuccess
    | FetchAnonymousUserTokenRequestfailure;
