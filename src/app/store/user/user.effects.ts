import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, Store, select } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    tap,
    withLatestFrom,
} from "rxjs/operators";

import { Auth, User } from "@models";
import { ApiService } from "@services/data/api.service";
import { SegmentService } from "@services/integration/segment.service";
import { CalendarService } from "@services/date/calendar.service";

import { AuthService } from "@services/data/auth.service";
import { UserService } from "@services/shared/user.service";

import { ExperimentsService } from "@services/shared/experiments.service";
import { SuprPassService } from "@services/shared/supr-pass.service";
import { SuprApi } from "@types";

import { StoreState } from "../store.state";
import * as userActions from "./user.actions";
import * as UserStoreSelectors from "./user.selectors";
import * as suprPassStoreSelectors from "../supr-pass/supr-pass.selectors";
import * as suprPassStoreActions from "../supr-pass/supr-pass.actions";
import * as addressActions from "../address/address.actions";
import * as walletActions from "../wallet/wallet.actions";
import * as subscriptionActions from "../subscription/subscription.actions";

@Injectable()
export class UserStoreEffects {
    constructor(
        private authService: AuthService,
        private userService: UserService,
        private apiService: ApiService,
        private segmentService: SegmentService,
        private experimentService: ExperimentsService,
        private actions$: Actions,
        private calenderService: CalendarService,
        private suprPassService: SuprPassService,
        private store$: Store<StoreState>
    ) {}

    // NOTE: This method is no longere in use
    @Effect()
    fetchUserEffect$: Observable<Action> = this.actions$.pipe(
        ofType<userActions.LoadProfileRequestAction>(
            userActions.ActionTypes.LOAD_PROFILE_REQUEST
        ),
        switchMap(() =>
            this.apiService.fetchProfile().pipe(
                map((response: SuprApi.ProfileResData) => {
                    const { customer } = response;
                    const { address, ...user } = customer;

                    return new userActions.LoadProfileRequestSuccessAction(
                        <User>user
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new userActions.LoadProfileRequestFailureAction(error)
                    )
                )
            )
        )
    );

    @Effect()
    fetchUserWithAddressEffect$: Observable<Action> = this.actions$.pipe(
        ofType<userActions.LoadProfileWithAddressRequestAction>(
            userActions.ActionTypes.LOAD_PROFILE_WITH_ADDRESS_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(suprPassStoreSelectors.selectSuprPassInfo))
        ),
        switchMap(([action, suprPassInfo]) =>
            this.apiService.fetchProfile(action.payload).pipe(
                tap((response: SuprApi.ProfileResData) => {
                    const { customer, suprCreditsEnabled } = response;

                    this.segmentService.setSuprCreditsFlag(
                        !!suprCreditsEnabled
                    );

                    this.experimentService.fetchExperiments(customer.id);
                    this.userService.setIsAnonymousUser(false);
                }),
                switchMap((response: SuprApi.ProfileResData) => {
                    const {
                        customer,
                        walletBalance,
                        subscriptions,
                        tPlusOneEnabled,
                        walletVersion,
                        suprCreditsEnabled,
                        suprCreditsBalance,
                        suprPassEligible = false,
                        suprPassDetails = null,
                        referralEligible = false,
                        referralDetails = null,
                        rewardsEligible = false,
                        rewardsDetails = null,
                        alternateDeliveryEligible = false,
                    } = response;
                    const { address, ...user } = customer;

                    user.tPlusOneEnabled = tPlusOneEnabled;
                    user.walletVersion = walletVersion;
                    user.suprCreditsEnabled = suprCreditsEnabled;
                    user.suprPassEligible = suprPassEligible;
                    user.suprPassDetails = suprPassDetails;
                    user.referralEligible = referralEligible;
                    user.referralDetails = referralDetails;
                    user.rewardsEligible = rewardsEligible;
                    user.rewardsDetails = rewardsDetails;
                    user.alternateDeliveryEligible = alternateDeliveryEligible;
                    this.calenderService.setTpEnabled(tPlusOneEnabled);

                    if (suprPassDetails && suprPassDetails.daysLeft) {
                        this.suprPassService.setIsSuprAccessMember(true);
                        this.suprPassService.setSuprAccessValidity(
                            suprPassDetails.daysLeft
                        );
                    } else {
                        this.suprPassService.setIsSuprAccessMember(false);
                        this.suprPassService.setSuprAccessValidity(0);
                    }

                    const actions: any = [
                        new userActions.LoadProfileRequestSuccessAction(
                            <User>user
                        ),
                        new walletActions.LoadSuprCreditsBalanceAction(
                            suprCreditsBalance
                        ),
                    ];

                    if (action.payload.address) {
                        actions.push(
                            new addressActions.FetchAddressRequestSuccessAction(
                                address
                            )
                        );
                    }

                    if (action.payload.wallet) {
                        actions.push(
                            new walletActions.LoadBalanceRequestSuccessAction(
                                walletBalance
                            )
                        );
                    }

                    if (action.payload.subscriptions) {
                        actions.push(
                            new subscriptionActions.FetchSubscriptionListRequestSuccessAction(
                                subscriptions
                            )
                        );
                    }

                    if (suprPassEligible && !suprPassInfo) {
                        return [
                            ...actions,
                            new suprPassStoreActions.FetchSuprPassRequestAction(
                                { silent: true }
                            ),
                        ];
                    }

                    return [...actions];
                }),
                catchError((error) =>
                    observableOf(
                        new userActions.LoadProfileRequestFailureAction(error)
                    )
                )
            )
        )
    );

    @Effect()
    updateUserEffect$: Observable<Action> = this.actions$.pipe(
        ofType<userActions.UpdateProfileRequestAction>(
            userActions.ActionTypes.UPDATE_PROFILE_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectUserMobileNumber))
        ),
        switchMap(([action, mobileNumber]) =>
            this.apiService
                .updateProfile(action.payload.user, mobileNumber)
                .pipe(
                    map((response: SuprApi.ProfileResData) => {
                        const { customer } = response;
                        const { ...user } = customer;

                        return new userActions.UpdateProfileRequestSuccessAction(
                            <User>user
                        );
                    }),
                    catchError((error) =>
                        observableOf(
                            new userActions.UpdateProfileRequestFailureAction(
                                error
                            )
                        )
                    )
                )
        )
    );

    @Effect()
    getAnonymousUserToken$ = this.actions$.pipe(
        ofType<userActions.FetchAnonymousUserTokenRequest>(
            userActions.ActionTypes.FETCH_ANONYMOUS_USER_TOKEN
        ),
        switchMap((action) =>
            this.apiService.fetchAnonymousToken(action.payload).pipe(
                map((data: Auth) => {
                    this.authService.setAuthToken(data.key, false);
                    this.userService.setAnonymousUserCityInfo(
                        action.payload.city_id
                    );
                    this.userService.setIsAnonymousUser(true);
                    return new userActions.FetchAnonymousUserTokenRequestSuccess(
                        data
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new userActions.FetchAnonymousUserTokenRequestfailure(
                            error
                        )
                    )
                )
            )
        )
    );
}
