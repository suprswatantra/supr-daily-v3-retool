import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { userReducer } from "./user.reducer";
import { UserStoreEffects } from "./user.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("user", userReducer),
        EffectsModule.forFeature([UserStoreEffects]),
    ],
    providers: [],
})
export class UserStoreModule {}
