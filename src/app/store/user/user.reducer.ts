import { Actions, ActionTypes } from "./user.actions";
import { initialState, UserState, UserCurrentState } from "./user.state";

export function userReducer(state = initialState, action: Actions): UserState {
    switch (action.type) {
        case ActionTypes.LOAD_PROFILE_REQUEST:
        case ActionTypes.LOAD_PROFILE_WITH_ADDRESS_REQUEST: {
            return {
                ...state,
                currentState: UserCurrentState.FETCHING_USER,
                error: null,
            };
        }
        case ActionTypes.LOAD_PROFILE_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: null,
                user: action.payload,
                isAnonymousUser: false,
            };
        }
        case ActionTypes.LOAD_PROFILE_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.UPDATE_PROFILE_REQUEST: {
            return {
                ...state,
                currentState: UserCurrentState.UPDATING_USER,
                error: null,
            };
        }
        case ActionTypes.UPDATE_PROFILE_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: null,
                user: {
                    ...state.user,
                    ...action.payload,
                },
            };
        }
        case ActionTypes.UPDATE_PROFILE_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.SET_USER_TYPE: {
            return {
                ...state,
                userType: action.payload.userType,
            };
        }

        case ActionTypes.SET_WHATS_APP_OPT_IN_ENABLED: {
            return {
                ...state,
                user: {
                    ...state.user,
                    isWhatsAppOptIn: true,
                },
            };
        }

        case ActionTypes.SHOW_CHECKOUT_DISABLED_NUDGE: {
            return {
                ...state,
                showCheckoutDisabledNudge: true,
            };
        }

        case ActionTypes.HIDE_CHECKOUT_DISABLED_NUDGE: {
            return {
                ...state,
                showCheckoutDisabledNudge: false,
            };
        }

        case ActionTypes.FETCH_ANONYMOUS_USER_TOKEN: {
            return {
                ...state,
                currentState: UserCurrentState.FETCHING_ANONYMOUS_TOKEN,
            };
        }

        case ActionTypes.FETCH_ANONYMOUS_USER_TOKEN_SUCCESS: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: null,
            };
        }
        case ActionTypes.FETCH_ANONYMOUS_USER_TOKEN_FAILURE: {
            return {
                ...state,
                currentState: UserCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}
