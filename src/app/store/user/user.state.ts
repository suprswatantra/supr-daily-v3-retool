import { User } from "@models";

export enum UserCurrentState {
    NO_ACTION = "0",
    FETCHING_USER = "1",
    UPDATING_USER = "2",
    FETCHING_ANONYMOUS_TOKEN = "3",
}

export interface UserState {
    currentState: UserCurrentState;
    error?: any;
    user?: User;
    userType?: string;
    showCheckoutDisabledNudge?: boolean;
    isAnonymousUser: boolean;
}

export const initialState: UserState = {
    currentState: UserCurrentState.NO_ACTION,
    error: null,
    user: null,
    userType: "",
    showCheckoutDisabledNudge: false,
    isAnonymousUser: true,
};
