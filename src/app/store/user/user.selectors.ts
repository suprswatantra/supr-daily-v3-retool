import { createSelector, createFeatureSelector } from "@ngrx/store";
import { User } from "@models";
import { UserState } from "./user.state";

export const selectFeature = createFeatureSelector<UserState>("user");
export const WALLET_VERSION_V2 = "V2";

export const selectUser = createSelector(selectFeature, (state: UserState) => {
    if (state.user) {
        return state.user;
    } else {
        return null;
    }
});

export const selectUserFullName = createSelector(
    selectFeature,
    (state: UserState) => {
        if (state.user) {
            return `${state.user.firstName} ${state.user.lastName}`;
        } else {
            return "";
        }
    }
);

export const selectIsNewUser = createSelector(
    selectFeature,
    (state: UserState) => state.userType === "new"
);

export const selectUserCheckoutInfo = createSelector(
    selectFeature,
    (state: UserState) => (state.user && state.user.checkoutInfo) || null
);

export const selectShowCheckoutDisabledNudge = createSelector(
    selectFeature,
    (state: UserState) => state.showCheckoutDisabledNudge
);

export const selectUserCurrentState = createSelector(
    selectFeature,
    (state: UserState) => state.currentState
);

export const selectUserMobileNumber = createSelector(selectUser, (user: User) =>
    user ? user.mobileNumber : null
);

export const selectUserError = createSelector(
    selectFeature,
    (state: UserState) => state.error
);

export const selectDoorBell = createSelector(selectUser, (user: User) =>
    user ? user.bellRing : false
);

export const selectWhatsAppOptIn = createSelector(selectUser, (user: User) =>
    user ? user.isWhatsAppOptIn : false
);

export const selectIsLoggedIn = createSelector(
    selectUser,
    (user: User) => !!(user && user.id)
);

export const selectUserId = createSelector(
    selectUser,
    (user: User) => user && user.id
);

export const selectIsSuprCreditsEnabled = createSelector(
    selectUser,
    (user: User) => (user ? user.suprCreditsEnabled : false)
);

export const selectWalletVersion = createSelector(selectUser, (user: User) =>
    user ? user.walletVersion : null
);

export const selectIsWalletVersionV2 = createSelector(
    selectUser,
    (user: User) =>
        user && user.walletVersion === WALLET_VERSION_V2 ? true : false
);

export const selectIsUserEligibleForSuprPass = createSelector(
    selectUser,
    (user: User) => (user ? user.suprPassEligible : false)
);

export const selectIsUserEligibleForReferral = createSelector(
    selectUser,
    (user: User) => (user ? user.referralEligible : false)
);

export const selectIsUserEligibleForRewards = createSelector(
    selectUser,
    (user: User) => (user ? user.rewardsEligible : false)
);

export const selectRewardsDetails = createSelector(selectUser, (user: User) =>
    user ? user.rewardsDetails : null
);

export const selectCanUserApplyReferralCode = createSelector(
    selectUser,
    (user: User) =>
        user
            ? user.referralDetails && user.referralDetails.showReferralCodeBlock
            : false
);

export const selectIsUserSuprPassMember = createSelector(
    selectUser,
    (user: User) =>
        user
            ? user.suprPassDetails && user.suprPassDetails.daysLeft >= 0
            : false
);

export const selectActiveSuprPassDetails = createSelector(
    selectUser,
    (user: User) => (user ? user.suprPassDetails : null)
);

export const selectIsUserEligibleForAlternateDelivery = createSelector(
    selectUser,
    (user: User) => (user ? user.alternateDeliveryEligible : false)
);

export const isAnonymousUser = createSelector(
    selectFeature,
    (user: UserState) => (user ? user.isAnonymousUser : false)
);
