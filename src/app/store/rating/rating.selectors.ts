import { createSelector, createFeatureSelector } from "@ngrx/store";
import { RatingState } from "./rating.state";

export const selectFeature = createFeatureSelector<RatingState>("rating");

export const selectRatingOptions = createSelector(
    selectFeature,
    (state: RatingState) => state.options
);

export const selectRatingShowModal = createSelector(
    selectFeature,
    (state: RatingState) => state.showModal
);

export const selectRatingTrigger = createSelector(
    selectFeature,
    (state: RatingState) => state.trigger
);
