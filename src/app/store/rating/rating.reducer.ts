import { Actions, ActionTypes } from "./rating.actions";
import { initialState, RatingState, RatingCurrentState } from "./rating.state";

export function ratingReducer(
    state = initialState,
    action: Actions
): RatingState {
    switch (action.type) {
        case ActionTypes.FETCH_RATING_OPTIONS_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: RatingCurrentState.FETCHING_OPTIONS,
            };
        }

        case ActionTypes.FETCH_RATING_OPTIONS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                options: action.payload,
                currentState: RatingCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.FETCH_RATING_OPTIONS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentState: RatingCurrentState.NO_ACTION,
            };
        }

        case ActionTypes.SHOW_RATING_MODAL: {
            return {
                ...state,
                showModal: true,
                trigger: action.payload.trigger,
            };
        }

        case ActionTypes.HIDE_RATING_MODAL: {
            return {
                ...state,
                showModal: false,
            };
        }

        default: {
            return state;
        }
    }
}
