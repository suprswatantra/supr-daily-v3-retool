import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import { ApiService } from "@services/data/api.service";

import * as ratingActions from "./rating.actions";

@Injectable()
export class RatingStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchRatingOptions$: Observable<Action> = this.actions$.pipe(
        ofType<ratingActions.FetchRatingOptionsRequestAction>(
            ratingActions.ActionTypes.FETCH_RATING_OPTIONS_REQUEST_ACTION
        ),
        switchMap(() =>
            this.apiService.fetchRatingOptions().pipe(
                map(
                    options =>
                        new ratingActions.FetchRatingOptionsRequestSuccessAction(
                            options
                        )
                ),
                catchError(error =>
                    observableOf(
                        new ratingActions.FetchRatingOptionsRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect({ dispatch: false })
    updateRatingAction$: Observable<any> = this.actions$.pipe(
        ofType<ratingActions.UpdateRatingActionRequestAction>(
            ratingActions.ActionTypes.UPDATE_RATING_ACTION_REQUEST_ACTION
        ),
        switchMap(action =>
            this.apiService
                .updateRatingAction(action.payload)
                .pipe(catchError(() => observableOf()))
        )
    );

    @Effect({ dispatch: false })
    saveFeedback$: Observable<any> = this.actions$.pipe(
        ofType<ratingActions.SaveFeedbackRequestAction>(
            ratingActions.ActionTypes.SAVE_FEEDBACK_REQUEST_ACTION
        ),
        switchMap(action =>
            this.apiService
                .saveRatingFeedback(action.payload)
                .pipe(catchError(() => observableOf()))
        )
    );
}
