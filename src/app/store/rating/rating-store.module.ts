import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { ratingReducer } from "./rating.reducer";
import { RatingStoreEffects } from "./rating.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("rating", ratingReducer),
        EffectsModule.forFeature([RatingStoreEffects]),
    ],
    providers: [],
})
export class RatingStoreModule {}
