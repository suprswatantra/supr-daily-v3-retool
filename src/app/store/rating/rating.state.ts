import { RatingOption } from "@models";

export enum RatingCurrentState {
    NO_ACTION = "0",
    FETCHING_OPTIONS = "1",
    SAVING_FEEDBACK = "2",
    UPDATING_ACTION = "3",
}

export interface RatingState {
    currentState?: RatingCurrentState;
    error?: any;
    showModal: boolean;
    options?: RatingOption[];
    trigger?: string;
}

export const initialState: RatingState = {
    currentState: RatingCurrentState.NO_ACTION,
    error: null,
    showModal: false,
    options: null,
    trigger: null,
};
