import { Action } from "@ngrx/store";

import { RatingOption } from "@models";
import { SuprApi } from "@types";

export enum ActionTypes {
    FETCH_RATING_OPTIONS_REQUEST_ACTION = "[Rating] Fetch rating options request",
    FETCH_RATING_OPTIONS_REQUEST_SUCCESS_ACTION = "[Rating] Fetch rating options request success",
    FETCH_RATING_OPTIONS_REQUEST_FAILURE_ACTION = "[Rating] Fetch rating options request failure",

    UPDATE_RATING_ACTION_REQUEST_ACTION = "[Rating] Update rating action request",

    SAVE_FEEDBACK_REQUEST_ACTION = "[Rating] Save feedback request",

    SHOW_RATING_MODAL = "[Rating] Show Modal",
    HIDE_RATING_MODAL = "[Rating] Hide Modal",
}

export class FetchRatingOptionsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_RATING_OPTIONS_REQUEST_ACTION;
}

export class FetchRatingOptionsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_RATING_OPTIONS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: RatingOption[]) {}
}

export class FetchRatingOptionsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_RATING_OPTIONS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class UpdateRatingActionRequestAction implements Action {
    readonly type = ActionTypes.UPDATE_RATING_ACTION_REQUEST_ACTION;
    constructor(public payload: SuprApi.RatingActionBody) {}
}

export class SaveFeedbackRequestAction implements Action {
    readonly type = ActionTypes.SAVE_FEEDBACK_REQUEST_ACTION;
    constructor(public payload: SuprApi.RatingFeedbackBody) {}
}

export class ShowRatingModal implements Action {
    readonly type = ActionTypes.SHOW_RATING_MODAL;
    constructor(public payload: { trigger: string }) {}
}

export class HideRatingModal implements Action {
    readonly type = ActionTypes.HIDE_RATING_MODAL;
}

export type Actions =
    | FetchRatingOptionsRequestAction
    | FetchRatingOptionsRequestSuccessAction
    | FetchRatingOptionsRequestFailureAction
    | UpdateRatingActionRequestAction
    | SaveFeedbackRequestAction
    | ShowRatingModal
    | HideRatingModal;
