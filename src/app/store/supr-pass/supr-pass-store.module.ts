import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { suprPassReducer } from "./supr-pass.reducer";
import { SuprPassEffects } from "./supr-pass.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("suprPass", suprPassReducer),
        EffectsModule.forFeature([SuprPassEffects]),
    ],
    providers: [],
})
export class SuprPassStoreModule {}
