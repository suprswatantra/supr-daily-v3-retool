import { Actions, ActionTypes } from "./supr-pass.actions";
import {
    initialState,
    SuprPassCurrentState,
    SuprPassState,
} from "./supr-pass.state";

export function suprPassReducer(
    state = initialState,
    action: Actions
): SuprPassState {
    switch (action.type) {
        case ActionTypes.FETCH_SUPR_PASS_REQUEST: {
            return {
                ...state,
                currentState: SuprPassCurrentState.FETCHING_SUPR_PASS,
                error: null,
            };
        }
        case ActionTypes.FETCH_SUPR_PASS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                suprPassInfo: action.payload,
                error: null,
            };
        }
        case ActionTypes.FETCH_SUPR_PASS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                error: action.payload,
            };
        }
        case ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST: {
            return {
                ...state,
                currentState:
                    SuprPassCurrentState.CANCELING_SUPR_PASS_AUTO_DEBIT,
                error: null,
            };
        }
        case ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                error: null,
            };
        }
        case ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                error: action.payload,
            };
        }
        case ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST: {
            return {
                ...state,
                currentState:
                    SuprPassCurrentState.FETCHING_SUPR_PASS_SAVINGS_DETAILS,
                error: null,
            };
        }
        case ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                suprPassSavingsDetails: action.payload,
                error: null,
            };
        }
        case ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: SuprPassCurrentState.IDLE,
                error: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}
