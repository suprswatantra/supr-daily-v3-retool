import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap, map } from "rxjs/operators";

import { SuprPassInfo, SuprPassSavingsInfo } from "@shared/models";

import { ApiService } from "@services/data/api.service";
import { BlockAccessService } from "@services/shared/block-access.service";

import * as SuprPassActions from "./supr-pass.actions";

@Injectable()
export class SuprPassEffects {
    constructor(
        private apiService: ApiService,
        private blockAccessService: BlockAccessService,
        private actions$: Actions
    ) {}

    @Effect()
    fetchSuprPassInfoEffect$: Observable<Action> = this.actions$.pipe(
        ofType<SuprPassActions.FetchSuprPassRequestAction>(
            SuprPassActions.ActionTypes.FETCH_SUPR_PASS_REQUEST
        ),
        switchMap((action) =>
            this.apiService.fetchSuprPassInfo(action.payload.silent).pipe(
                map((data: SuprPassInfo) => {
                    /* Set block access instances */
                    const { experimentInfo, isActive } = data;

                    this.blockAccessService.setBlockAccessExpData(
                        experimentInfo
                    );

                    this.blockAccessService.setIsSuprPassActive(isActive);

                    return new SuprPassActions.FetchSuprPassRequestSuccessAction(
                        data
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new SuprPassActions.FetchSuprPassRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    cancelSuprPassAutoDebitEffect$: Observable<Action> = this.actions$.pipe(
        ofType<SuprPassActions.CancelSuprPassAutoDebitRequestAction>(
            SuprPassActions.ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST
        ),
        switchMap(() =>
            this.apiService.cancelSuprPassAutoDebit().pipe(
                /** Don't change the sequence of actions here else
                 * RequestSuccesAction will set the current state to idle
                 * even though supr pass info fetch is also happening */
                switchMap(() => [
                    new SuprPassActions.CancelSuprPassAutoDebitRequestSuccessAction(),
                    new SuprPassActions.FetchSuprPassRequestAction({
                        silent: false,
                    }),
                ]),
                catchError((error) =>
                    observableOf(
                        new SuprPassActions.CancelSuprPassAutoDebitRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchSuprPassSavingsDetailsEffect$: Observable<Action> = this.actions$.pipe(
        ofType<SuprPassActions.FetchSuprPassSavingsRequestAction>(
            SuprPassActions.ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST
        ),
        switchMap((action) =>
            this.apiService
                .fetchSuprPassSavingsDetails(action.payload.silent)
                .pipe(
                    map((data: SuprPassSavingsInfo) => {
                        return new SuprPassActions.FetchSuprPassSavingsRequestSuccessAction(
                            data
                        );
                    }),
                    catchError((error) =>
                        observableOf(
                            new SuprPassActions.FetchSuprPassSavingsRequestFailureAction(
                                error
                            )
                        )
                    )
                )
        )
    );
}
