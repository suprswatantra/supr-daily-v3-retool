import { SuprPassInfo, SuprPassSavingsInfo } from "@models";

export enum SuprPassCurrentState {
    IDLE = "idle",
    FETCHING_SUPR_PASS = "fetching_supr_pass",
    CANCELING_SUPR_PASS_AUTO_DEBIT = "canceling_supr_pass_auto_debit",
    FETCHING_SUPR_PASS_SAVINGS_DETAILS = "fetching_supr_pass_savings_details",
}

export interface SuprPassState {
    currentState: SuprPassCurrentState;
    error?: any;
    suprPassInfo: SuprPassInfo | null;
    suprPassSavingsDetails: SuprPassSavingsInfo | null;
}

export const initialState: SuprPassState = {
    currentState: SuprPassCurrentState.IDLE,
    error: null,
    suprPassInfo: null,
    suprPassSavingsDetails: null,
};
