import { createSelector, createFeatureSelector } from "@ngrx/store";
import { SuprPassState, SuprPassCurrentState } from "./supr-pass.state";
import { SuprPassInfo } from "@shared/models";

export const selectFeature = createFeatureSelector<SuprPassState>("suprPass");

export const selectSuprPassStoreCurrentState = createSelector(
    selectFeature,
    (state: SuprPassState) => state.currentState
);

export const selectIsFetchingSuprPassInfo = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        state.currentState === SuprPassCurrentState.FETCHING_SUPR_PASS
);

export const selectIsCancelingSuprPassAutoDebit = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        state.currentState ===
        SuprPassCurrentState.CANCELING_SUPR_PASS_AUTO_DEBIT
);

export const selectIsFetchingSuprPassSavingsInfo = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        state.currentState ===
        SuprPassCurrentState.FETCHING_SUPR_PASS_SAVINGS_DETAILS
);

export const selectSuprPassError = createSelector(
    selectFeature,
    (state: SuprPassState) => state.error
);

export const selectSuprPassInfo = createSelector(
    selectFeature,
    (state: SuprPassState) => state.suprPassInfo
);

export const selectSuprPassSavingsDetails = createSelector(
    selectFeature,
    (state: SuprPassState) => state.suprPassSavingsDetails
);

export const selectSuprPassFaqs = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        (state.suprPassInfo && state.suprPassInfo.faqs) || []
);

export const selectSuprPassExperimentInfo = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        (state.suprPassInfo && state.suprPassInfo.experimentInfo) || null
);

export const selectSuprPassActivity = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        (state.suprPassInfo && state.suprPassInfo.activity) || []
);

export const selectIsSuprPassMember = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        state.suprPassInfo ? state.suprPassInfo.isActive : false
);

export const selectSuprPassSubscriptionId = createSelector(
    selectFeature,
    (state: SuprPassState) => getSuprPassSubscriptionId(state.suprPassInfo)
);

export const selectSuprPassBigCardDetails = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        (state.suprPassInfo &&
            state.suprPassInfo.savingsCardInfo &&
            state.suprPassInfo.savingsCardInfo.sidebar) ||
        null
);

export const selectSuprPassSmallCardDetails = createSelector(
    selectFeature,
    (state: SuprPassState) =>
        (state.suprPassInfo &&
            state.suprPassInfo.savingsCardInfo &&
            state.suprPassInfo.savingsCardInfo.homepage) ||
        null
);

function getSuprPassSubscriptionId(suprPassInfo: SuprPassInfo): number {
    if (!suprPassInfo) {
        return null;
    }

    const isSuprPassActive = suprPassInfo.isActive;
    const expiredSuprPassPurchaseId =
        suprPassInfo.expiredSuprPassData &&
        suprPassInfo.expiredSuprPassData.purchaseId;

    if (isSuprPassActive) {
        return (
            suprPassInfo.activePlanDetails &&
            suprPassInfo.activePlanDetails.purchaseId
        );
    } else {
        return expiredSuprPassPurchaseId;
    }
}
