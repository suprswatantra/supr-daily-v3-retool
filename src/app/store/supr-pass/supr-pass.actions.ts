import { Action } from "@ngrx/store";

import { SuprPassInfo, SuprPassSavingsInfo } from "@shared/models";

export enum ActionTypes {
    FETCH_SUPR_PASS_REQUEST = "[Supr Pass] Fetch Request",
    FETCH_SUPR_PASS_REQUEST_SUCCESS = "[Supr Pass] Fetch Request Success",
    FETCH_SUPR_PASS_REQUEST_FAILURE = "[Supr Pass] Fetch Request Failure",

    CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST = "[Supr Pass] Cancel Auto Debit",
    CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_SUCCESS = "[Supr Pass] Cancel Auto Debit Success",
    CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_FAILURE = "[Supr Pass] Cancel Auto Debit Failure",

    FETCH_SUPR_PASS_SAVINGS_REQUEST = "[Supr Pass] Fetch Savings Request",
    FETCH_SUPR_PASS_SAVINGS_REQUEST_SUCCESS = "[Supr Pass] Fetch Savings Request Success",
    FETCH_SUPR_PASS_SAVINGS_REQUEST_FAILURE = "[Supr Pass] Fetch Savings Reuqest Failure",
}

export class FetchSuprPassRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_REQUEST;
    constructor(public payload: { silent: boolean }) {}
}

export class FetchSuprPassRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_REQUEST_SUCCESS;
    constructor(public payload: SuprPassInfo) {}
}

export class FetchSuprPassRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class CancelSuprPassAutoDebitRequestAction implements Action {
    readonly type = ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST;
    constructor() {}
}

export class CancelSuprPassAutoDebitRequestSuccessAction implements Action {
    readonly type = ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_SUCCESS;
    constructor() {}
}

export class CancelSuprPassAutoDebitRequestFailureAction implements Action {
    readonly type = ActionTypes.CANCEL_SUPR_PASS_AUTO_DEBIT_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class FetchSuprPassSavingsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST;
    constructor(public payload: { silent: boolean }) {}
}

export class FetchSuprPassSavingsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST_SUCCESS;
    constructor(public payload: SuprPassSavingsInfo) {}
}

export class FetchSuprPassSavingsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_SUPR_PASS_SAVINGS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchSuprPassRequestAction
    | FetchSuprPassRequestSuccessAction
    | FetchSuprPassRequestFailureAction
    | CancelSuprPassAutoDebitRequestAction
    | CancelSuprPassAutoDebitRequestSuccessAction
    | CancelSuprPassAutoDebitRequestFailureAction
    | FetchSuprPassSavingsRequestAction
    | FetchSuprPassSavingsRequestSuccessAction
    | FetchSuprPassSavingsRequestFailureAction;
