import { DeliveryStatus } from "@models";

export enum DeliveryStatusCurrentState {
    IDLE = "0",
    FETCHING = "1",
}

export interface DeliveryStatusState {
    currentState: DeliveryStatusCurrentState;
    error: any;
    status: DeliveryStatus;
    refresh: boolean;
}

export const initialState: DeliveryStatusState = {
    currentState: DeliveryStatusCurrentState.FETCHING,
    error: null,
    status: {},
    refresh: false,
};
