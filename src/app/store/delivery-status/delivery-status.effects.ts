import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";

import { DeliveryStatus } from "@models";

import { ApiService } from "@services/data/api.service";

import * as DeliveryStatusActions from "./delivery-status.actions";

@Injectable()
export class DeliveryStatusEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchDeliveryStatusEffect$: Observable<Action> = this.actions$.pipe(
        ofType<DeliveryStatusActions.FetchDeliveryStatusAction>(
            DeliveryStatusActions.ActionTypes.FETCH_ORDER_STATUS
        ),
        switchMap(action =>
            this.apiService.fetchDeliveryStatus(action.payload.silent).pipe(
                switchMap((data: DeliveryStatus) => [
                    new DeliveryStatusActions.FetchDeliveryStatusSuccessAction({
                        data,
                    }),
                ]),
                catchError(_error =>
                    observableOf(
                        new DeliveryStatusActions.FetchDeliveryStatusFailureAction(
                            "Something went wrong"
                        )
                    )
                )
            )
        )
    );
}
