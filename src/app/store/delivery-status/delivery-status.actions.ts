import { Action } from "@ngrx/store";

import { DeliveryStatus } from "@models";

export enum ActionTypes {
    FETCH_ORDER_STATUS = "[Delivery Status] Fetch Order Status",
    FETCH_ORDER_STATUS_SUCCESS = "[Delivery Status] Fetch Order Status Success",
    FETCH_ORDER_STATUS_FAILURE = "[Delivery Status] Fetch Order Status Failure",
    ENABLE_REFRESH_DELIVERY_STATUS = "[Delivery Status] Enable Refresh Delivery Status",
    DISABLE_REFRESH_DELIVERY_STATUS = "[Delivery Status] Disable Refresh Delivery Status",
}

export class FetchDeliveryStatusAction implements Action {
    readonly type = ActionTypes.FETCH_ORDER_STATUS;
    constructor(public payload?: { silent?: boolean }) {}
}

export class FetchDeliveryStatusSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_ORDER_STATUS_SUCCESS;
    constructor(public payload: { data: DeliveryStatus }) {}
}

export class FetchDeliveryStatusFailureAction implements Action {
    readonly type = ActionTypes.FETCH_ORDER_STATUS_FAILURE;
    constructor(public payload: any) {}
}

export class EnableRefreshDeliveryStatus implements Action {
    readonly type = ActionTypes.ENABLE_REFRESH_DELIVERY_STATUS;
}

export class DisableRefreshDeliveryStatus implements Action {
    readonly type = ActionTypes.DISABLE_REFRESH_DELIVERY_STATUS;
}

export type Actions =
    | FetchDeliveryStatusAction
    | FetchDeliveryStatusSuccessAction
    | FetchDeliveryStatusFailureAction
    | EnableRefreshDeliveryStatus
    | DisableRefreshDeliveryStatus;
