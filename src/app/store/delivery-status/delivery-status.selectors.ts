import { createSelector, createFeatureSelector } from "@ngrx/store";
import {
    DeliveryStatusState,
    DeliveryStatusCurrentState,
} from "./delivery-status.state";

export const selectFeature = createFeatureSelector<DeliveryStatusState>(
    "deliveryStatus"
);

export const selectDeliveryStatus = createSelector(
    selectFeature,
    (state: DeliveryStatusState) => state.status
);

export const selectCurrentDeliveryStatusStoreState = createSelector(
    selectFeature,
    (state: DeliveryStatusState) => state.currentState
);

export const selectDeliveryStatusError = createSelector(
    selectFeature,
    (state: DeliveryStatusState) => state.error
);

export const selectIsFetchingDeliveryStatus = createSelector(
    selectFeature,
    (state: DeliveryStatusState) =>
        state.currentState === DeliveryStatusCurrentState.FETCHING
);

export const selectShouldRefreshDeliveryStatus = createSelector(
    selectFeature,
    (state: DeliveryStatusState) => state.refresh
);
