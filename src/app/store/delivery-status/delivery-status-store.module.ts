import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { deliveryStatusReducer } from "./delivery-status.reducer";
import { DeliveryStatusEffects } from "./delivery-status.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("deliveryStatus", deliveryStatusReducer),
        EffectsModule.forFeature([DeliveryStatusEffects]),
    ],
    providers: [],
})
export class DeliveryStatusStoreModule {}
