import { Actions, ActionTypes } from "./delivery-status.actions";
import {
    initialState,
    DeliveryStatusState,
    DeliveryStatusCurrentState,
} from "./delivery-status.state";

export function deliveryStatusReducer(
    state = initialState,
    action: Actions
): DeliveryStatusState {
    switch (action.type) {
        case ActionTypes.FETCH_ORDER_STATUS: {
            return {
                ...state,
                currentState:
                    action.payload && action.payload.silent
                        ? DeliveryStatusCurrentState.IDLE
                        : DeliveryStatusCurrentState.FETCHING,
                error: null,
            };
        }
        case ActionTypes.FETCH_ORDER_STATUS_SUCCESS: {
            return {
                ...state,
                currentState: DeliveryStatusCurrentState.IDLE,
                error: null,
                status: action.payload.data,
            };
        }
        case ActionTypes.FETCH_ORDER_STATUS_FAILURE: {
            return {
                ...state,
                currentState: DeliveryStatusCurrentState.IDLE,
                error: action.payload,
                refresh: false,
            };
        }
        case ActionTypes.ENABLE_REFRESH_DELIVERY_STATUS: {
            return {
                ...state,
                refresh: true,
            };
        }
        case ActionTypes.DISABLE_REFRESH_DELIVERY_STATUS: {
            return {
                ...state,
                refresh: false,
            };
        }
        default: {
            return state;
        }
    }
}
