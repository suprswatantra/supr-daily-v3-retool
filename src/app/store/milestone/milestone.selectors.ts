import { createSelector, createFeatureSelector } from "@ngrx/store";

import { MileStoneState } from "./milestone.state";

export const selectFeature = createFeatureSelector<MileStoneState>("milestone");

export const selectMileStone = createSelector(
    selectFeature,
    (state: MileStoneState) => state.mileStone
);

export const selectMileStoneDetails = createSelector(
    selectFeature,
    (state: MileStoneState) => state.mileStoneDetails
);

export const selectMilestoneState = createSelector(
    selectFeature,
    (state: MileStoneState) => state.currentState
);

export const selectStartMilestoneState = createSelector(
    selectFeature,
    (state: MileStoneState) => state.currentStartMilestoneState
);

export const selectStartMilestoneRewards = createSelector(
    selectFeature,
    (state: MileStoneState) => state.mileStoneRewards
);
