import { Action } from "@ngrx/store";
import { MileStoneHome, MilestonePastRewards, MileStoneRewards } from "@models";
import { SuprApi } from "app/types";

export enum ActionTypes {
    FETCH_MILESTONE_REQUEST_ACTION = "[MILESTONE] Fetch milestone list request",
    FETCH_MILESTONE_REQUEST_SUCCESS_ACTION = "[MILESTONE] Fetch milestone list request success",
    FETCH_MILESTONE_REQUEST_FAILURE_ACTION = "[MILESTONE] Fetch milestone list request failure",

    FETCH_MILESTONE_REWARDS_REQUEST_ACTION = "[MILESTONE] Fetch milestone rewards list request",
    FETCH_MILESTONE_REWARDS_REQUEST_SUCCESS_ACTION = "[MILESTONE] Fetch milestone rewards list request success",
    FETCH_MILESTONE_REWARDS_REQUEST_FAILURE_ACTION = "[MILESTONE] Fetch milestone rewards list request failure",

    FETCH_MILESTONE_DETAILS_REQUEST_ACTION = "[MILESTONE] Fetch milestone details request",
    FETCH_MILESTONE_DETAILS_REQUEST_SUCCESS_ACTION = "[MILESTONE] Fetch milestone details request success",
    FETCH_MILESTONE_DETAILS_REQUEST_FAILURE_ACTION = "[MILESTONE] Fetch milestone details request failure",

    POST_MILESTONE_START_REQUEST_ACTION = "[MILESTONE] POST milestone start request",
    POST_MILESTONE_START_REQUEST_SUCCESS_ACTION = "[MILESTONE] POST milestone start request success",
    POST_MILESTONE_START_REQUEST_FAILURE_ACTION = "[MILESTONE] POST milestone start request failure",
}

export class FetchMileStoneRequestAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REQUEST_ACTION;
    constructor() {}
}

export class FetchMileStoneRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REQUEST_SUCCESS_ACTION;
    constructor(public payload: MileStoneHome) {}
}

export class FetchMileStoneRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchMileStoneRewardsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_ACTION;
    constructor() {}
}

export class FetchMileStoneRewardsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: MilestonePastRewards) {}
}

export class FetchMileStoneRewardsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchMileStoneDetailRequestAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_ACTION;
    constructor() {}
}

export class FetchMileStoneDetailRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: MileStoneRewards) {}
}

export class FetchMileStoneDetailRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class PostMileStoneStartRequestAction implements Action {
    readonly type = ActionTypes.POST_MILESTONE_START_REQUEST_ACTION;
    constructor(public payload: SuprApi.MilestoneStartActionBody) {}
}

export class PostMileStoneStartRequestSuccessAction implements Action {
    readonly type = ActionTypes.POST_MILESTONE_START_REQUEST_SUCCESS_ACTION;
    constructor() {}
}

export class PostMileStoneStartRequestFailureAction implements Action {
    readonly type = ActionTypes.POST_MILESTONE_START_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchMileStoneRequestAction
    | FetchMileStoneRequestSuccessAction
    | FetchMileStoneRequestFailureAction
    | FetchMileStoneDetailRequestAction
    | FetchMileStoneDetailRequestSuccessAction
    | FetchMileStoneDetailRequestFailureAction
    | PostMileStoneStartRequestAction
    | PostMileStoneStartRequestSuccessAction
    | PostMileStoneStartRequestFailureAction
    | FetchMileStoneRewardsRequestAction
    | FetchMileStoneRewardsRequestSuccessAction
    | FetchMileStoneRewardsRequestFailureAction;
