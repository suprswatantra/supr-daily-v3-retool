import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { mileStoneReducer } from "./milestone.reducer";
import { MileStoneStoreEffects } from "./milestone.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("milestone", mileStoneReducer),
        EffectsModule.forFeature([MileStoneStoreEffects]),
    ],
    providers: [],
})
export class MileStoneStoreModule {}
