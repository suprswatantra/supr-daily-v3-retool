import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import { ApiService } from "@services/data/api.service";

import * as mileStoneActions from "./milestone.actions";

@Injectable()
export class MileStoneStoreEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchMilestone$: Observable<Action> = this.actions$.pipe(
        ofType<mileStoneActions.FetchMileStoneRequestAction>(
            mileStoneActions.ActionTypes.FETCH_MILESTONE_REQUEST_ACTION
        ),
        switchMap(() => {
            return this.apiService.fetchMileStone().pipe(
                map(
                    (milestone) =>
                        new mileStoneActions.FetchMileStoneRequestSuccessAction(
                            milestone
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new mileStoneActions.FetchMileStoneRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchMilestoneRewards$: Observable<Action> = this.actions$.pipe(
        ofType<mileStoneActions.FetchMileStoneRewardsRequestAction>(
            mileStoneActions.ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_ACTION
        ),
        switchMap(() => {
            return this.apiService.fetchMileStoneRewardsHistory().pipe(
                map(
                    (milestoneRewards) =>
                        new mileStoneActions.FetchMileStoneRewardsRequestSuccessAction(
                            milestoneRewards
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new mileStoneActions.FetchMileStoneRewardsRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchMilestoneDetail$: Observable<Action> = this.actions$.pipe(
        ofType<mileStoneActions.FetchMileStoneDetailRequestAction>(
            mileStoneActions.ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_ACTION
        ),
        switchMap(() => {
            return this.apiService.fetchMileStoneDetail().pipe(
                map(
                    (milestoneDetail) =>
                        new mileStoneActions.FetchMileStoneDetailRequestSuccessAction(
                            milestoneDetail
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new mileStoneActions.FetchMileStoneDetailRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    postStartMilestone$: Observable<any> = this.actions$.pipe(
        ofType<mileStoneActions.PostMileStoneStartRequestAction>(
            mileStoneActions.ActionTypes.POST_MILESTONE_START_REQUEST_ACTION
        ),
        switchMap((action) => {
            return this.apiService.postStartMilestone(action.payload).pipe(
                switchMap(() => {
                    return [
                        new mileStoneActions.FetchMileStoneDetailRequestAction(),
                        new mileStoneActions.FetchMileStoneRequestAction(),
                        new mileStoneActions.PostMileStoneStartRequestSuccessAction(),
                    ];
                }),
                catchError((error) =>
                    observableOf(
                        new mileStoneActions.PostMileStoneStartRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );
}
