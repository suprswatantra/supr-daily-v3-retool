import { Actions, ActionTypes } from "./milestone.actions";
import {
    initialState,
    MileStoneState,
    MileStoneCurrentState,
    MileStoneStartCurrentState,
} from "./milestone.state";

export function mileStoneReducer(
    state = initialState,
    action: Actions
): MileStoneState {
    switch (action.type) {
        case ActionTypes.FETCH_MILESTONE_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE,
            };
        }

        case ActionTypes.FETCH_MILESTONE_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE_DONE,
                mileStone: action.payload,
            };
        }

        case ActionTypes.FETCH_MILESTONE_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE_DONE,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE,
            };
        }

        case ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE_DONE,
                mileStoneRewards: action.payload,
            };
        }

        case ActionTypes.FETCH_MILESTONE_REWARDS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE_DONE,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: MileStoneCurrentState.FETCHING_MILESTONE_DETAILS,
            };
        }

        case ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState:
                    MileStoneCurrentState.FETCHING_MILESTONE_DETAILS_DONE,
                mileStoneDetails: action.payload,
            };
        }

        case ActionTypes.FETCH_MILESTONE_DETAILS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState:
                    MileStoneCurrentState.FETCHING_MILESTONE_DETAILS_DONE,
                error: action.payload,
            };
        }

        case ActionTypes.POST_MILESTONE_START_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentStartMilestoneState:
                    MileStoneStartCurrentState.POSTING_MILESTONE_START_STATE,
            };
        }

        case ActionTypes.POST_MILESTONE_START_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentStartMilestoneState:
                    MileStoneStartCurrentState.POSTING_MILESTONE_START_DONE,
            };
        }

        case ActionTypes.POST_MILESTONE_START_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                error: action.payload,
                currentStartMilestoneState:
                    MileStoneStartCurrentState.POSTING_MILESTONE_START_STATE,
            };
        }

        default: {
            return state;
        }
    }
}
