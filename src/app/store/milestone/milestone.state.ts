import { MileStoneHome, MilestonePastRewards, MileStoneRewards } from "@models";

export enum MileStoneCurrentState {
    NO_ACTION = "0",
    FETCHING_MILESTONE = "1",
    FETCHING_MILESTONE_DONE = "2",
    FETCHING_MILESTONE_DETAILS = "3",
    FETCHING_MILESTONE_DETAILS_DONE = "4",
}

export enum MileStoneStartCurrentState {
    POSTING_MILESTONE_START_STATE = "0",
    POSTING_MILESTONE_START_DONE = "1",
}

export interface MileStoneState {
    currentState?: MileStoneCurrentState;
    error?: any;
    mileStone: MileStoneHome;
    mileStoneDetails: MileStoneRewards;
    currentStartMilestoneState?: MileStoneStartCurrentState;
    mileStoneRewards?: MilestonePastRewards;
}

export const initialState: MileStoneState = {
    currentState: MileStoneCurrentState.NO_ACTION,
    currentStartMilestoneState:
        MileStoneStartCurrentState.POSTING_MILESTONE_START_STATE,
    error: null,
    mileStone: <MileStoneHome>{},
    mileStoneDetails: <MileStoneRewards>{},
    mileStoneRewards: <MilestonePastRewards>{},
};
