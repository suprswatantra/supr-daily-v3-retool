import * as FeaturedStoreActions from "./featured.actions";
import * as FeaturedStoreSelectors from "./featured.selectors";
import * as FeaturedStoreState from "./featured.state";

export { FeaturedStoreModule } from "./featured-store.module";
export { FeaturedStoreState, FeaturedStoreActions, FeaturedStoreSelectors };
