import { createSelector, createFeatureSelector } from "@ngrx/store";

import { FeaturedState } from "./featured.state";

export const selectFeature = createFeatureSelector<FeaturedState>("featured");

export const selectFeaturedData = createSelector(
    selectFeature,
    (state: FeaturedState) => state.featured
);

export const selectFeaturedDataByComponent = createSelector(
    selectFeature,
    (featuredState: FeaturedState, props: { componentName: string }) =>
        featuredState.featured
            ? featuredState.featured[props.componentName]
                ? featuredState.featured[props.componentName]
                : null
            : null
);

export const selectFeaturedCurrentState = createSelector(
    selectFeature,
    (state: FeaturedState) => state.currentState
);

export const selectFeaturedError = createSelector(
    selectFeature,
    (state: FeaturedState) => state.error
);
