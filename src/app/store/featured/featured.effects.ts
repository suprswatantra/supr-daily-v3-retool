import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, Store, select } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, map, switchMap, withLatestFrom } from "rxjs/operators";

import { FEATURED_CARD_CONSTANTS } from "@constants";

import { ApiService } from "@services/data/api.service";

import * as featuredActions from "./featured.actions";
import { StoreState } from "@store/store.state";
import { selectFeaturedData } from "./featured.selectors";

@Injectable()
export class FeaturedStoreEffects {
    constructor(
        private apiService: ApiService,
        private actions$: Actions,
        private store$: Store<StoreState>
    ) {}

    @Effect()
    fetchFeaturedList$: Observable<Action> = this.actions$.pipe(
        ofType<featuredActions.FetchFeaturedRequestAction>(
            featuredActions.ActionTypes.FETCH_FEATURED_REQUEST_ACTION
        ),
        withLatestFrom(this.store$.pipe(select(selectFeaturedData))),
        switchMap(([action, featureLists]) => {
            return this.apiService.fetchFeaturedListItems(action.payload).pipe(
                map((featuredList) => {
                    const { component_id } = action.payload;
                    const key = component_id
                        ? String(component_id)
                        : FEATURED_CARD_CONSTANTS.HOME;

                    featureLists = {
                        ...featureLists,
                        [key]: featuredList,
                    };

                    return new featuredActions.FetchFeaturedRequestSuccessAction(
                        featureLists
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new featuredActions.FetchFeaturedRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );
}
