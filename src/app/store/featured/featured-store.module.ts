import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { featuredReducer } from "./featured.reducer";
import { FeaturedStoreEffects } from "./featured.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("featured", featuredReducer),
        EffectsModule.forFeature([FeaturedStoreEffects]),
    ],
    providers: [],
})
export class FeaturedStoreModule {}
