import { Actions, ActionTypes } from "./featured.actions";
import {
    initialState,
    FeaturedState,
    FeaturedCurrentState,
} from "./featured.state";

export function featuredReducer(
    state = initialState,
    action: Actions
): FeaturedState {
    switch (action.type) {
        case ActionTypes.FETCH_FEATURED_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: FeaturedCurrentState.FETCHING_FEATURED,
            };
        }

        case ActionTypes.FETCH_FEATURED_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: FeaturedCurrentState.FETCHING_FEATURED_DONE,
                featured: action.payload,
            };
        }

        case ActionTypes.FETCH_FEATURED_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: FeaturedCurrentState.FETCHING_FEATURED_DONE,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}
