import { Action } from "@ngrx/store";
import { Featured } from "@models";

export enum ActionTypes {
    FETCH_FEATURED_REQUEST_ACTION = "[Feauted] Fetch feauted request",
    FETCH_FEATURED_REQUEST_SUCCESS_ACTION = "[Feauted] Fetch feauted request success",
    FETCH_FEATURED_REQUEST_FAILURE_ACTION = "[Feauted] Fetch feauted request failure",
}

export class FetchFeaturedRequestAction implements Action {
    readonly type = ActionTypes.FETCH_FEATURED_REQUEST_ACTION;
    constructor(public payload?: Featured.Payload) {}
}

export class FetchFeaturedRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_FEATURED_REQUEST_SUCCESS_ACTION;
    constructor(public payload: Featured.Dict) {}
}

export class FetchFeaturedRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_FEATURED_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchFeaturedRequestAction
    | FetchFeaturedRequestSuccessAction
    | FetchFeaturedRequestFailureAction;
