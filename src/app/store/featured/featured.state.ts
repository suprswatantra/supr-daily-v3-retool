import { Featured } from "@models";

export enum FeaturedCurrentState {
    NO_ACTION = "0",
    FETCHING_FEATURED = "1",
    FETCHING_FEATURED_DONE = "2",
}

export interface FeaturedState {
    currentState: FeaturedCurrentState;
    error?: any;
    featured: Featured.Dict;
}

export const initialState: FeaturedState = {
    currentState: FeaturedCurrentState.NO_ACTION,
    error: null,
    featured: null,
};
