import { Injectable } from "@angular/core";
import { observable, computed } from "mobx";
import Complaintv2Actions from "./complaintv2.actions";
import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { SupportOptions, ComplaintViewV2 } from "@models";

@Injectable({
    providedIn: "root",
})
export class Complaintv2Store extends Complaintv2Actions {
    constructor(
        private apiService: ApiService,
        private utilService: UtilService
    ) {
        super();
        console.log(this.apiService);
    }

    @observable
    complaintsListV2: ComplaintViewV2[];

    @observable
    supportOptions: SupportOptions = {
        chat_access: true,
        call_access: true,
        ivr_popup: false,
    };

    @observable
    complaint: ComplaintViewV2[];

    @observable
    complaintDetails: any;

    @computed
    get orderComplaintsList() {
        const { complaintsListV2 } = this;
        let orderWiseComplaints = new Map();
        if (this.utilService.isLengthyArray(complaintsListV2)) {
            complaintsListV2.forEach((complaint) => {
                let complaints = [];
                if (orderWiseComplaints.has(complaint.order_date)) {
                    const complaintValue = orderWiseComplaints.get(
                        complaint.order_date
                    );
                    complaints = [...complaintValue, complaint];
                } else {
                    complaints.push(complaint);
                }
                orderWiseComplaints.set(complaint.order_date, complaints);
            });
        }
        return orderWiseComplaints;
    }
}
