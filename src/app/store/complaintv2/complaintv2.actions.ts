import { action } from "mobx";

import { Injectable } from "@angular/core";
import { Subscription } from "rxjs";
import { finalize, take } from "rxjs/operators";

import { SupportOptions, ComplaintViewV2 } from "@models";

import { COMPLAINT_DETAILS_DATA } from "@stubs/complaint.stub";

@Injectable()
export default class Complaintv2Actions {
    [x: string]: any;
    constructor() {}

    subscription$: Subscription;

    @action
    fetchComplaintsListV2(date: string, days) {
        this.subscription$ = this.apiService
            .fetchComplaintsDataV2(date, days, "complaint_date")
            .pipe(take(1))
            .pipe(
                finalize(() => {
                    this.unsubscribeApi();
                })
            )
            .subscribe(
                (complaintsListV2: ComplaintViewV2[]) => {
                    Object.assign(this, { complaintsListV2 });
                },
                (err) => {
                    console.log(err);
                }
            );
    }

    @action
    getComplaintsByOrders(
        date: string,
        days: number = 7,
        append: boolean = false
    ) {
        this.subscription$ = this.apiService
            .fetchComplaintsDataV2(date, days, "order_date")
            .pipe(take(1))
            .pipe(
                finalize(() => {
                    this.unsubscribeApi();
                })
            )
            .subscribe(
                (complaintsList: ComplaintViewV2[]) => {
                    Object.assign(this, { complaintsList });
                    this.setComplaintsList(complaintsList, append);
                },
                (err) => {
                    console.log(err);
                }
            );
    }

    @action
    setComplaintsList(complaints: ComplaintViewV2[], append: boolean) {
        if (append) {
            const { complaintsListV2 } = this;
            Object.assign(this, {
                complaintsListV2: [...complaintsListV2, ...complaints],
            });
            return;
        }
        Object.assign(this, { complaintsListV2: complaints });
    }

    @action
    fetchSupportOptions() {
        return this.apiService.fetchContactSupport();
    }

    @action
    setSupportOptions(supportOptions: SupportOptions) {
        Object.assign(this, { supportOptions });
    }

    @action
    setComplaint(complaint: ComplaintViewV2) {
        Object.assign(this, {
            complaint,
            complaintDetails: {},
        });
    }

    @action
    fetchComplaintDetails(complaintId: number) {
        console.log(complaintId);
        setTimeout(() => {
            Object.assign(this, {
                complaintDetails: COMPLAINT_DETAILS_DATA,
            });
        }, 100);
    }

    private unsubscribeApi() {
        if (this.subscription$) {
            this.subscription$.unsubscribe();
            this.subscription$ = null;
        }
    }
}
