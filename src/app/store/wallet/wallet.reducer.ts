import { PAYMENT_METHODS_V2 } from "@constants";

import { RechargeSuccessInfo, PaymentMethodsV2, PaymentMethods } from "@models";

import {
    SuprApi,
    PaymentMethodComponent,
    PaymentMethodV2Component,
} from "@types";

import { Actions, ActionTypes } from "./wallet.actions";
import { initialState, WalletState, WalletCurrentState } from "./wallet.state";

export function walletReducer(
    state = initialState,
    action: Actions
): WalletState {
    switch (action.type) {
        case ActionTypes.FETCH_INITIAL_DATA: {
            return {
                ...state,
                error: null,
                currentState: WalletCurrentState.LOADING_INITIAL_DATA,
            };
        }
        case ActionTypes.FETCH_INITIAL_DATA_SUCCESS: {
            const { wallet, paymentMethodsRes } = action.payload;
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    suprCreditsBalance: wallet && wallet.suprCredits,
                    balance: wallet && wallet.online,
                    expiry: wallet && wallet.expiry,
                },
                paymentMethods: getPaymentMethodsDict(paymentMethodsRes),
                paymentMethodsList: getPaymentMethodsList(paymentMethodsRes),
                paymentMethodsVersion: getPaymentMethodsUiVersion(
                    paymentMethodsRes
                ),
            };
        }
        case ActionTypes.FETCH_INITIAL_DATA_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.LOAD_BALANCE_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.FETCHING_BALANCE,
                error: null,
            };
        }
        case ActionTypes.LOAD_BALANCE_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    balance: action.payload,
                },
            };
        }
        case ActionTypes.LOAD_BALANCE_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.LOAD_BALANCE_V2_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.FETCHING_BALANCE,
                error: null,
            };
        }
        case ActionTypes.LOAD_BALANCE_V2_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    balance: action.payload.online,
                    suprCreditsBalance: action.payload.suprCredits,
                    expiry: action.payload.expiry,
                },
            };
        }
        case ActionTypes.LOAD_BALANCE_V2_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.LOAD_SUPR_CREDITS_BALANCE: {
            return {
                ...state,
                wallet: {
                    ...state.wallet,
                    suprCreditsBalance: action.payload,
                },
            };
        }
        case ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.FETCHING_TRANSACTION_DETAILS,
                error: null,
            };
        }
        case ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                transactionDetails: action.payload,
            };
        }
        case ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.FETCH_WALLET_OFFERS_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.FETCHING_WALLET_OFFERS,
                error: null,
            };
        }
        case ActionTypes.FETCH_WALLET_OFFERS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                walletOffers: action.payload,
            };
        }
        case ActionTypes.FETCH_WALLET_OFFERS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                walletOffers: [],
            };
        }
        case ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST:
        case ActionTypes.CREATE_PAYMENT_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.CREATING_PAYMENT,
                error: null,
            };
        }
        case ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST_SUCCESS:
        case ActionTypes.CREATE_PAYMENT_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    balance: action.payload.wallet_balance,
                    suprCreditsBalance: action.payload.supr_credits_balance,
                    meta: {
                        ...state.wallet.meta,
                        rechargeSuccessInfo: _getRechargeSuccessInfo(
                            action.payload
                        ),
                    },
                },
            };
        }
        case ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST_FAILURE:
        case ActionTypes.CREATE_PAYMENT_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }
        case ActionTypes.APPLY_RECHARGE_COUPON_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.APPLYING_RECHARGE_COUPON,
                error: null,
            };
        }
        case ActionTypes.APPLY_RECHARGE_COUPON_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    meta: {
                        ...state.wallet.meta,
                        rechargeCouponInfo: action.payload,
                    },
                },
            };
        }
        case ActionTypes.APPLY_RECHARGE_COUPON_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST: {
            return {
                ...state,
                currentState:
                    WalletCurrentState.FETCHING_RECHARGE_COUPON_OFFERS,
                error: null,
            };
        }
        case ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                wallet: {
                    ...state.wallet,
                    meta: {
                        ...state.wallet.meta,
                        rechargeCouponOffers: action.payload,
                    },
                },
            };
        }
        case ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.CLEAR_RECHARGE_COUPON: {
            return {
                ...state,
                wallet: {
                    ...state.wallet,
                    meta: {
                        ...state.wallet.meta,
                        rechargeCouponInfo: null,
                    },
                },
            };
        }

        case ActionTypes.CLEAR_WALLET_META: {
            return {
                ...state,
                wallet: {
                    ...state.wallet,
                    meta: null,
                },
            };
        }

        case ActionTypes.FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_REQUEST: {
            return {
                ...state,
                currentState: WalletCurrentState.FETCHING_AUTO_DEBIT_MODES,
            };
        }

        case ActionTypes.FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_SUCCESS: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                autoDebitModes: action.payload,
            };
        }

        case ActionTypes.APPLY_RECHARGE_COUPON_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: WalletCurrentState.NO_ACTION,
                error: action.payload,
            };
        }

        case ActionTypes.TOGGLE_FETCHED_FILTERED_METHODS_STATUS: {
            return {
                ...state,
                hasFetchedFilteredMethods: action.payload.status,
            };
        }

        default: {
            return state;
        }
    }
}

function _getRechargeSuccessInfo(
    payment: SuprApi.PaymentRes
): RechargeSuccessInfo {
    const rechargeSuccessInfo: RechargeSuccessInfo = {};

    if (payment.recharge_amount && !payment.buy_supr_credits) {
        rechargeSuccessInfo.amountAddedToWallet = payment.recharge_amount;
    }

    if (
        payment.benefits &&
        payment.benefits.supr_credits_benefits &&
        payment.benefits.supr_credits_benefits.length
    ) {
        rechargeSuccessInfo.amountAddedToSuprCredits =
            payment.benefits.supr_credits_benefits[0].benefit_value;
    }

    return rechargeSuccessInfo;
}

function getPaymentMethodsDict(
    response: SuprApi.PaymentMethodsResData
): PaymentMethods | PaymentMethodsV2 {
    return response && response["payment-methods"];
}

function getPaymentMethodsList(
    response: SuprApi.PaymentMethodsResData
): Array<PaymentMethodComponent | PaymentMethodV2Component> {
    const uiVersion = getPaymentMethodsUiVersion(response);

    if (uiVersion === 1) {
        return getPaymentMethodsV1List(response);
    }

    return getPaymentMethodsV2List(response);
}

function getPaymentMethodsV1List(
    response: SuprApi.PaymentMethodsResData
): PaymentMethodComponent[] {
    const paymentMethods = getPaymentMethodsDict(response);

    return Object.keys(paymentMethods).map((group: string) => ({
        ...paymentMethods[group],
        group,
    })) as PaymentMethodComponent[];
}

function getPaymentMethodsV2List(
    response: SuprApi.PaymentMethodsResData
): PaymentMethodV2Component[] {
    const paymentMethods = getPaymentMethodsDict(response) as PaymentMethodsV2;
    const emptyMethodsToBeFiltered: Array<string> = [
        PAYMENT_METHODS_V2.WALLET,
        PAYMENT_METHODS_V2.PREFERRED,
        PAYMENT_METHODS_V2.NETBANKING,
    ];

    return (
        Object.keys(paymentMethods)
            /* filtering out specific methods that have no options */
            .filter((group: string) =>
                emptyMethodsToBeFiltered.indexOf(group) > -1
                    ? /* check for options array length if the method needs to be filtered out when empty; else return true */
                      paymentMethods[group] &&
                      isLengthyArray(paymentMethods[group].options)
                    : true
            )
            /* converting map to list */
            .map((group: string) => ({
                ...paymentMethods[group],
                group,
            }))
    );
}

function getPaymentMethodsUiVersion(response: SuprApi.PaymentMethodsResData) {
    return response && response.ui_version === 2 ? 2 : 1;
}

function isLengthyArray(items: Array<any>): boolean {
    return Array.isArray(items) && items.length > 0;
}
