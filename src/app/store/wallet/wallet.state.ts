import {
    Wallet,
    PaymentMethods,
    PaymentMethodsV2,
    TransactionDetails,
    AutoDebitPaymentModes,
    OffersList,
} from "@models";

import { PaymentMethodComponent, PaymentMethodV2Component } from "@types";

export enum WalletCurrentState {
    NO_ACTION = "0",
    FETCHING_BALANCE = "1",
    CREATING_PAYMENT = "2",
    APPLYING_RECHARGE_COUPON = "3",
    FETCHING_RECHARGE_COUPON_OFFERS = "4",
    FETCHING_TRANSACTION_DETAILS = "5",
    FETCHING_WALLET_OFFERS = "6",
    LOADING_INITIAL_DATA = "7",
    FETCHING_AUTO_DEBIT_MODES = "8",
}

export interface WalletState {
    error: any;
    wallet: Wallet;
    walletOffers: OffersList;
    paymentMethodsVersion: number;
    currentState: WalletCurrentState;
    autoDebitModes: AutoDebitPaymentModes;
    transactionDetails: TransactionDetails;
    paymentMethods: PaymentMethods | PaymentMethodsV2;
    paymentMethodsList: Array<
        PaymentMethodComponent | PaymentMethodV2Component
    >;
    hasFetchedFilteredMethods: boolean;
}

export const initialState: WalletState = {
    error: null,
    wallet: {
        balance: 0,
        meta: null,
    },
    walletOffers: null,
    paymentMethods: null,
    autoDebitModes: null,
    paymentMethodsList: null,
    transactionDetails: null,
    paymentMethodsVersion: null,
    currentState: WalletCurrentState.LOADING_INITIAL_DATA,
    hasFetchedFilteredMethods: false,
};
