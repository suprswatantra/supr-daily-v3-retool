import { Injectable } from "@angular/core";

import { Action, Store, select } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { Observable, of as observableOf, forkJoin } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    withLatestFrom,
    tap,
} from "rxjs/operators";

import { CUSTOM_EVENTS } from "@constants";

import {
    RechargeCouponInfo,
    RechargeCouponOffer,
    TransactionDetails,
    AutoDebitPaymentModes,
    OffersList,
} from "@models";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { SegmentService } from "@services/integration/segment.service";

import { SuprApi } from "@types";

import { StoreState } from "../store.state";
import * as walletActions from "./wallet.actions";
import * as cartStoreActions from "../cart/cart.actions";
import * as UserStoreSelectors from "../user/user.selectors";
import * as WalletStoreSelectors from "./wallet.selectors";
import * as CartStoreSelectors from "../cart/cart.selectors";

@Injectable()
export class WalletStoreEffects {
    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private store$: Store<StoreState>,
        private utilService: UtilService,
        private segmentServicce: SegmentService
    ) {}

    @Effect()
    fetchInitialDataEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.FetchInitialData>(
            walletActions.ActionTypes.FETCH_INITIAL_DATA
        ),
        withLatestFrom(
            this.store$.pipe(
                select(
                    CartStoreSelectors.selectCartDiscoutPaymentDetailsInstruments
                )
            ),
            this.store$.pipe(select(CartStoreSelectors.selectCartCouponCode))
        ),
        switchMap(([action, cartPaymentInstruments, cartCouponCode]) => {
            /* use paramsObj to fetch filtered payment methods list */
            let paramsObj = action.payload ? action.payload.body : {};

            /* If fetching initial data when coming coming from cart and coupon has been used */
            if (
                action.payload &&
                action.payload.usingCartPaymentCouponCode &&
                cartCouponCode &&
                this.utilService.isLengthyArray(cartPaymentInstruments)
            ) {
                const paymentInstruments = cartPaymentInstruments[0];
                paramsObj = {
                    coupon_code: cartCouponCode.couponCode,
                    ...paymentInstruments,
                };
            }

            const methodsObservable = this.apiService
                .fetchPaymentMethods(true, paramsObj)
                .pipe(catchError(() => observableOf(null)));

            const apiCalls = [
                this.apiService.fetchWalletBalanceV2(),
                methodsObservable,
            ];

            return forkJoin(apiCalls).pipe(
                switchMap(
                    (
                        response: Array<
                            SuprApi.BalanceRes | SuprApi.PaymentMethodsResData
                        >
                    ) => {
                        return [
                            new walletActions.FetchInitialDataSuccessAction({
                                wallet: response[0] as SuprApi.BalanceRes,
                                paymentMethodsRes: response[1] as SuprApi.PaymentMethodsResData,
                            }),
                            new walletActions.ToggleFetchedFilteredMethodsStatus(
                                { status: !this.utilService.isEmpty(paramsObj) }
                            ),
                        ];
                    }
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.FetchInitialDataFailureAction(error)
                    )
                )
            );
        })
    );

    @Effect()
    fetchWalletBalanceEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.LoadBalanceRequestAction>(
            walletActions.ActionTypes.LOAD_BALANCE_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.selectFeature))
        ),
        switchMap(([_, userState]) => {
            const { user } = userState;
            if (user.suprCreditsEnabled) {
                return observableOf(
                    new walletActions.LoadBalanceV2RequestAction()
                );
            }
            return this.apiService.fetchWalletBalance().pipe(
                map(
                    (balance: number) =>
                        new walletActions.LoadBalanceRequestSuccessAction(
                            balance
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.LoadBalanceRequestFailureAction(error)
                    )
                )
            );
        })
    );

    @Effect()
    fetchWalletBalanceV2Effect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.LoadBalanceV2RequestAction>(
            walletActions.ActionTypes.LOAD_BALANCE_V2_REQUEST
        ),
        switchMap(() =>
            this.apiService.fetchWalletBalanceV2().pipe(
                map(
                    (response: SuprApi.BalanceRes) =>
                        new walletActions.LoadBalanceV2RequestSuccessAction(
                            response
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.LoadBalanceV2RequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    fetchWalletOffersEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.FetchWalletOffersRequestAction>(
            walletActions.ActionTypes.FETCH_WALLET_OFFERS_REQUEST
        ),
        switchMap((action) =>
            this.apiService.fetchWalletOffers(action.payload.amount).pipe(
                map(
                    (response: OffersList) =>
                        new walletActions.FetchWalletOffersRequestSuccessAction(
                            response
                        )
                ),
                catchError((_error) =>
                    observableOf(
                        new walletActions.FetchWalletOffersRequestFailureAction()
                    )
                )
            )
        )
    );

    @Effect()
    createPaymentEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.CreatePaymentRequestAction>(
            walletActions.ActionTypes.CREATE_PAYMENT_REQUEST
        ),
        switchMap((action) => {
            const {
                paymentId,
                amount,
                couponCode,
                buySuprCredits,
            } = action.payload;
            return this.apiService
                .createPayment(paymentId, amount, couponCode, buySuprCredits)
                .pipe(
                    switchMap((response) => [
                        new walletActions.CreatePaymentRequestSuccessAction(
                            response
                        ),
                        new cartStoreActions.EnableRevalidateAction(),
                    ]),
                    catchError((error) =>
                        observableOf(
                            new walletActions.CreatePaymentRequestFailureAction(
                                error
                            )
                        )
                    )
                );
        })
    );

    @Effect()
    applyRechargeCouponEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.ApplyRechargeCouponRequestAction>(
            walletActions.ActionTypes.APPLY_RECHARGE_COUPON_REQUEST
        ),
        switchMap((action) => {
            const { couponCode, amount } = action.payload;
            return this.apiService.applyRechargeCoupon(couponCode, amount).pipe(
                tap((_response: RechargeCouponInfo) => {
                    this.segmentServicce.trackCustom(
                        CUSTOM_EVENTS.WALLET_COUPON_APPLIED,
                        {
                            amount,
                            coupon_code: couponCode,
                        }
                    );
                }),
                switchMap((response: RechargeCouponInfo) => {
                    const actions: Action[] = [
                        new walletActions.ApplyRechargeCouponRequestSuccessAction(
                            response
                        ),
                    ];

                    if (
                        response.isApplied &&
                        response.couponCode &&
                        response.paymentDetails
                    ) {
                        const instruments = this.utilService.isLengthyArray(
                            response.paymentDetails.instruments
                        )
                            ? response.paymentDetails.instruments[0]
                            : {};
                        actions.push(
                            new walletActions.FetchInitialData({
                                body: {
                                    coupon_code: response.couponCode,
                                    ...instruments,
                                },
                            })
                        );
                    }
                    return actions;
                }),
                catchError((error) =>
                    observableOf(
                        new walletActions.ApplyRechargeCouponRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchRechargeCouponOffersEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.FetchRechargeCouponOffersRequestAction>(
            walletActions.ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST
        ),
        switchMap(() => {
            return this.apiService.fetchRechargeCouponOffers().pipe(
                map(
                    (response: RechargeCouponOffer[]) =>
                        new walletActions.FetchRechargeCouponOffersRequestSuccessAction(
                            response
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.FetchRechargeCouponOffersRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchTransactionDetailsEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.FetchTransactionDetailsRequestAction>(
            walletActions.ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST
        ),
        switchMap(() => {
            return this.apiService.fetchWalletTransactionDetails().pipe(
                map(
                    (response: TransactionDetails) =>
                        new walletActions.FetchTransactionDetailsRequestSuccessAction(
                            response
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.FetchTransactionDetailsRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchAutoDebitModesEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.FetchAutoDebitPaymentModesInfoRequestAction>(
            walletActions.ActionTypes
                .FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_REQUEST
        ),
        switchMap((action) =>
            this.apiService.fetchAutoDebitModes(action.payload.silent).pipe(
                map(
                    (response: AutoDebitPaymentModes) =>
                        new walletActions.FetchAutoDebitPaymentModesInfoSuccessAction(
                            response
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new walletActions.FetchAutoDebitPaymentModesInfoFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );

    @Effect()
    captureCustomPaymentEffect$: Observable<Action> = this.actions$.pipe(
        ofType<walletActions.CaptureCustomPaymentRequestAction>(
            walletActions.ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST
        ),
        switchMap((action) => {
            const {
                payment_id,
                order_id,
                partner_customer_id,
                payment_partner,
                amount,
                coupon_code,
                buy_supr_credits,
            } = action.payload;
            return this.apiService
                .captureCustomPayment({
                    payment_id,
                    order_id,
                    partner_customer_id,
                    payment_partner,
                    amount,
                    coupon_code,
                    buy_supr_credits,
                })
                .pipe(
                    switchMap((response) => [
                        new walletActions.CaptureCustomPaymentRequestSuccessAction(
                            response
                        ),
                        new cartStoreActions.EnableRevalidateAction(),
                    ]),
                    catchError((error) =>
                        observableOf(
                            new walletActions.CaptureCustomPaymentRequestFailureAction(
                                error
                            )
                        )
                    )
                );
        })
    );

    @Effect()
    removeCouponEffect$: Observable<Action | null> = this.actions$.pipe(
        ofType<walletActions.ClearRechargeCouponAction>(
            walletActions.ActionTypes.CLEAR_RECHARGE_COUPON
        ),
        withLatestFrom(
            this.store$.pipe(
                select(WalletStoreSelectors.selectFetchedFilteredMethodsStatus)
            )
        ),
        switchMap(([_, hasFetchedFilteredMethods]) => {
            if (hasFetchedFilteredMethods) {
                return observableOf(new walletActions.FetchInitialData());
            }
            return observableOf(new walletActions.DoNothingWalletAction());
        })
    );
}
