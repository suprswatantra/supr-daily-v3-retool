import { createSelector, createFeatureSelector } from "@ngrx/store";

import { PaymentMethods, PaymentMethodsV2, PaymentMethodOption } from "@models";

import { PaymentMethodComponent, PaymentMethodV2Component } from "@types";

import { WalletState, WalletCurrentState } from "./wallet.state";
import { PAYMENT_METHODS_V2 } from "@constants";

export const selectFeature = createFeatureSelector<WalletState>("wallet");

export const selectWalletBalance = createSelector(
    selectFeature,
    (state: WalletState) => state.wallet.balance
);

export const selectSuprCreditsBalance = createSelector(
    selectFeature,
    (state: WalletState) => state.wallet.suprCreditsBalance
);

export const selectSuprCreditsExpiry = createSelector(
    selectFeature,
    (state: WalletState) => state.wallet && state.wallet.expiry
);

export const selectWallet = createSelector(
    selectFeature,
    (state: WalletState) => state.wallet || null
);

export const selectWalletCurrentState = createSelector(
    selectFeature,
    (state: WalletState) => state.currentState
);

export const selectWalletMeta = createSelector(
    selectFeature,
    (state: WalletState) => state.wallet.meta
);

export const selectRechargeCouponInfo = createSelector(
    selectFeature,
    (state: WalletState) =>
        state.wallet && state.wallet.meta
            ? state.wallet.meta.rechargeCouponInfo
            : null
);

export const selectWalletError = createSelector(
    selectFeature,
    (state: WalletState) => state.error
);

export const selectWalletTransactionDetails = createSelector(
    selectFeature,
    (state: WalletState) => state.transactionDetails
);

export const selectWalletOffers = createSelector(
    selectFeature,
    (state: WalletState) => state.walletOffers
);

export const selectIsFetchingWalletOffers = createSelector(
    selectFeature,
    (state: WalletState) =>
        state.currentState === WalletCurrentState.FETCHING_WALLET_OFFERS
);

export const selectIsApplyingCouponCode = createSelector(
    selectFeature,
    (state: WalletState) =>
        state.currentState === WalletCurrentState.APPLYING_RECHARGE_COUPON
);

export const selectWalletTransactionList = createSelector(
    selectFeature,
    (state: WalletState) =>
        (state.transactionDetails && state.transactionDetails.txn_list) || []
);

export const selectWalletExpiryDetails = createSelector(
    selectFeature,
    (state: WalletState) =>
        (state.transactionDetails && state.transactionDetails.expiry) || null
);

export const selectPaymentMethodVersion = createSelector(
    selectFeature,
    (state: WalletState) => state.paymentMethodsVersion
);

export const selectPaymentMethodsList = createSelector(
    selectFeature,
    selectPaymentMethodVersion,
    (state: WalletState, version: number) => {
        if (!state.paymentMethods || version === 2) {
            return null;
        }

        return state.paymentMethodsList as PaymentMethodComponent[];
    }
);

export const selectPaymentMethodsV2List = createSelector(
    selectFeature,
    selectPaymentMethodVersion,
    (state: WalletState, version: number) => {
        if (!state.paymentMethods || version !== 2) {
            return null;
        }

        return state.paymentMethodsList as PaymentMethodV2Component[];
    }
);

export const selectPaymentsMethodV2Dict = createSelector(
    selectFeature,
    selectPaymentMethodVersion,
    (state: WalletState, version: number) => {
        if (!state.paymentMethods || version !== 2) {
            return null;
        }

        return state.paymentMethods;
    }
);

export const selectSelectedPaymentMethod = createSelector(
    selectFeature,
    selectPaymentMethodVersion,
    (state: WalletState, version: number) => {
        if (version === 2) {
            return;
        }

        const paymentMethods = state.paymentMethods as PaymentMethods;

        for (const group in paymentMethods) {
            if (paymentMethods[group] && paymentMethods[group].methods) {
                /* tslint:disable-next-line: forin */
                for (const methodName in paymentMethods[group].methods) {
                    const methodData =
                        paymentMethods[group].methods[methodName];
                    if (methodData && methodData.selected) {
                        return methodName;
                    }
                }
            }
        }

        return null;
    }
);

export const selectSelectedPaymentMethodV2 = createSelector(
    selectFeature,
    selectPaymentMethodVersion,
    (state: WalletState, version: number) => {
        if (version !== 2) {
            return;
        }

        const paymentMethods = state.paymentMethods as PaymentMethodsV2;

        for (const group in paymentMethods) {
            if (
                paymentMethods[group] &&
                isLengthyArray(paymentMethods[group].options)
            ) {
                const selectedOption = paymentMethods[group].options.find(
                    (option: PaymentMethodOption) => option.selected
                );

                return selectedOption
                    ? {
                          id: selectedOption.id,
                          paymentInfo: selectedOption.payment_info,
                          method:
                              paymentMethods[group].method ||
                              selectedOption.method,
                      }
                    : null;
            }
        }

        return null;
    }
);

export const selectAutoDebitPaymentModes = createSelector(
    selectFeature,
    (state: WalletState) => {
        if (!state.autoDebitModes || !state.autoDebitModes.methods) {
            return null;
        }

        return state.autoDebitModes.methods;
    }
);

export const selectNetBankingOptions = createSelector(
    selectPaymentsMethodV2Dict,
    (paymentMethods: PaymentMethodsV2) => {
        const netBankingData =
            paymentMethods && paymentMethods[PAYMENT_METHODS_V2.NETBANKING];

        return isLengthyArray(netBankingData.options)
            ? netBankingData.options
            : [];
    }
);

export const selectFetchedFilteredMethodsStatus = createSelector(
    selectFeature,
    (state: WalletState) => state.hasFetchedFilteredMethods
);

function isLengthyArray(items: Array<any>): boolean {
    return Array.isArray(items) && items.length > 0;
}
