import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { walletReducer } from "./wallet.reducer";
import { WalletStoreEffects } from "./wallet.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("wallet", walletReducer),
        EffectsModule.forFeature([WalletStoreEffects]),
    ],
    providers: [],
})
export class WalletStoreModule {}
