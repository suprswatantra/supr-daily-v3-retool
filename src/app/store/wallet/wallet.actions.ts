import { Action } from "@ngrx/store";

import {
    PaymentInfo,
    RechargeCoupon,
    RechargeCouponInfo,
    TransactionDetails,
    RechargeCouponOffer,
    AutoDebitPaymentModes,
    OffersList,
} from "@models";

import { SuprApi } from "@types";

export enum ActionTypes {
    LOAD_BALANCE_REQUEST = "[Wallet] Balance Request",
    LOAD_BALANCE_REQUEST_SUCCESS = "[Wallet] Balance Request Success",
    LOAD_BALANCE_REQUEST_FAILURE = "[Wallet] Balance Request Failure",

    LOAD_BALANCE_V2_REQUEST = "[Wallet] Balance V2 Request",
    LOAD_BALANCE_V2_REQUEST_SUCCESS = "[Wallet] Balance V2 Request Success",
    LOAD_BALANCE_V2_REQUEST_FAILURE = "[Wallet] Balance V2 Request Failure",

    LOAD_SUPR_CREDITS_BALANCE = "[Wallet] Load SuprCredits",

    FETCH_TRANSACTION_DETAILS_REQUEST = "[Wallet] Transaction Details Request",
    FETCH_TRANSACTION_DETAILS_REQUEST_SUCCESS = "[Wallet] Transaction Details Request Success",
    FETCH_TRANSACTION_DETAILS_REQUEST_FAILURE = "[Wallet] Transaction Details Request Failure",

    CREATE_PAYMENT_REQUEST = "[Wallet] Create Payment Request",
    CREATE_PAYMENT_REQUEST_SUCCESS = "[Wallet] Create Payment Request Success",
    CREATE_PAYMENT_REQUEST_FAILURE = "[Wallet] Create Payment Request Failure",

    APPLY_RECHARGE_COUPON_REQUEST = "[Wallet] Apply Recharge Coupon Request",
    APPLY_RECHARGE_COUPON_REQUEST_SUCCESS = "[Wallet] Apply Recharge Coupon Request Success",
    APPLY_RECHARGE_COUPON_REQUEST_FAILURE = "[Wallet] Apply Recharge Coupon Request Failure",

    FETCH_RECHARGE_COUPON_OFFERS_REQUEST = "[Wallet] Fetch Recharge Coupon Offers Request",
    FETCH_RECHARGE_COUPON_OFFERS_REQUEST_SUCCESS = "[Wallet] Fetch Recharge Coupon Offers Request Success",
    FETCH_RECHARGE_COUPON_OFFERS_REQUEST_FAILURE = "[Wallet] Fetch Recharge Coupon Offers Request Failure",

    CLEAR_RECHARGE_COUPON = "[Wallet] Clear Recharge Coupon",

    CLEAR_WALLET_META = "[Wallet] Clear Wallet Meta",

    FETCH_WALLET_OFFERS_REQUEST = "[Wallet] Offers Request",
    FETCH_WALLET_OFFERS_REQUEST_SUCCESS = "[Wallet] Offers Request Success",
    FETCH_WALLET_OFFERS_REQUEST_FAILURE = "[Wallet] Offers Request Failure",
    FETCH_PAYMENT_METHODS = "[Wallet] Fetch Payment Methods",

    FETCH_INITIAL_DATA = "[Wallet] Fetch Initial Data",
    FETCH_INITIAL_DATA_SUCCESS = "[Wallet] Fetch Initial Data Success",
    FETCH_INITIAL_DATA_FAILURE = "[Wallet] Fetch Initial Data Failure",

    FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_REQUEST = "[Wallet] Fetch Auto Debit Payment Info Request",
    FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_SUCCESS = "[Wallet] Fetch Auto Debit Payment Info Success",
    FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_FAILURE = "[Wallet] Fetch Auto Debit Payment Info Failure",

    CAPTURE_CUSTOM_PAYMENT_REQUEST = "[Wallet] Capture Custom Payment Request",
    CAPTURE_CUSTOM_PAYMENT_REQUEST_SUCCESS = "[Wallet] Capture Custom Payment Request Success",
    CAPTURE_CUSTOM_PAYMENT_REQUEST_FAILURE = "[Wallet] Capture Custom Payment Request Failure",

    DO_NOTHING_WALLET_ACTION = "[Wallet] Do nothing wallet action",
    TOGGLE_FETCHED_FILTERED_METHODS_STATUS = "[Wallet] Toggle Fetched Filtered Methods Status",
}

export class LoadBalanceRequestAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_REQUEST;
}

export class LoadBalanceRequestSuccessAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_REQUEST_SUCCESS;

    constructor(public payload: number) {}
}

export class LoadBalanceRequestFailureAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_REQUEST_FAILURE;
    constructor(public payload: string) {}
}

export class FetchWalletOffersRequestAction implements Action {
    readonly type = ActionTypes.FETCH_WALLET_OFFERS_REQUEST;

    constructor(public payload: { amount: number }) {}
}

export class FetchWalletOffersRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_WALLET_OFFERS_REQUEST_SUCCESS;

    constructor(public payload: OffersList) {}
}

export class FetchWalletOffersRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_WALLET_OFFERS_REQUEST_FAILURE;
}

export class LoadBalanceV2RequestAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_V2_REQUEST;
}

export class LoadBalanceV2RequestSuccessAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_V2_REQUEST_SUCCESS;

    constructor(public payload: SuprApi.BalanceRes) {}
}

export class LoadBalanceV2RequestFailureAction implements Action {
    readonly type = ActionTypes.LOAD_BALANCE_V2_REQUEST_FAILURE;
    constructor(public payload: string) {}
}

export class LoadSuprCreditsBalanceAction implements Action {
    readonly type = ActionTypes.LOAD_SUPR_CREDITS_BALANCE;

    constructor(public payload: number) {}
}

export class FetchTransactionDetailsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST;
}

export class FetchTransactionDetailsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST_SUCCESS;

    constructor(public payload: TransactionDetails) {}
}

export class FetchTransactionDetailsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_TRANSACTION_DETAILS_REQUEST_FAILURE;
    constructor(public payload: string) {}
}

export class CreatePaymentRequestAction implements Action {
    readonly type = ActionTypes.CREATE_PAYMENT_REQUEST;
    constructor(public payload: PaymentInfo) {}
}

export class CreatePaymentRequestSuccessAction implements Action {
    readonly type = ActionTypes.CREATE_PAYMENT_REQUEST_SUCCESS;

    constructor(public payload: SuprApi.PaymentRes) {}
}

export class CreatePaymentRequestFailureAction implements Action {
    readonly type = ActionTypes.CREATE_PAYMENT_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class ApplyRechargeCouponRequestAction implements Action {
    readonly type = ActionTypes.APPLY_RECHARGE_COUPON_REQUEST;
    constructor(public payload: RechargeCoupon) {}
}

export class ApplyRechargeCouponRequestSuccessAction implements Action {
    readonly type = ActionTypes.APPLY_RECHARGE_COUPON_REQUEST_SUCCESS;

    constructor(public payload: RechargeCouponInfo) {}
}

export class ApplyRechargeCouponRequestFailureAction implements Action {
    readonly type = ActionTypes.APPLY_RECHARGE_COUPON_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class FetchRechargeCouponOffersRequestAction implements Action {
    readonly type = ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST;
}

export class FetchRechargeCouponOffersRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST_SUCCESS;

    constructor(public payload: RechargeCouponOffer[]) {}
}

export class FetchRechargeCouponOffersRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_RECHARGE_COUPON_OFFERS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class ClearRechargeCouponAction implements Action {
    readonly type = ActionTypes.CLEAR_RECHARGE_COUPON;
}

export class ClearWalletMetaAction implements Action {
    readonly type = ActionTypes.CLEAR_WALLET_META;
}

export class FetchPaymentMethods implements Action {
    readonly type = ActionTypes.FETCH_PAYMENT_METHODS;
}

export class FetchInitialData implements Action {
    readonly type = ActionTypes.FETCH_INITIAL_DATA;
    constructor(
        public payload?: {
            body?: SuprApi.PaymentMethodsReqParams;
            usingCartPaymentCouponCode?: boolean;
        }
    ) {}
}

export class FetchInitialDataSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_INITIAL_DATA_SUCCESS;

    constructor(
        public payload: {
            wallet: SuprApi.BalanceRes;
            paymentMethodsRes: SuprApi.PaymentMethodsResData;
        }
    ) {}
}

export class FetchInitialDataFailureAction implements Action {
    readonly type = ActionTypes.FETCH_INITIAL_DATA_FAILURE;
    constructor(public payload: any) {}
}

export class FetchAutoDebitPaymentModesInfoRequestAction implements Action {
    readonly type = ActionTypes.FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_REQUEST;
    constructor(public payload: { silent: boolean }) {}
}

export class FetchAutoDebitPaymentModesInfoSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_SUCCESS;
    constructor(public payload: AutoDebitPaymentModes) {}
}

export class FetchAutoDebitPaymentModesInfoFailureAction implements Action {
    readonly type = ActionTypes.FETCH_AUTO_DEBIT_PAYMENT_MODES_INFO_FAILURE;
    constructor(public payload: any) {}
}

export class CaptureCustomPaymentRequestAction implements Action {
    readonly type = ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST;
    constructor(public payload: SuprApi.CustomPaymentCaptureReq) {}
}

export class CaptureCustomPaymentRequestSuccessAction implements Action {
    readonly type = ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST_SUCCESS;

    constructor(public payload: SuprApi.PaymentRes) {}
}

export class CaptureCustomPaymentRequestFailureAction implements Action {
    readonly type = ActionTypes.CAPTURE_CUSTOM_PAYMENT_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class DoNothingWalletAction implements Action {
    readonly type = ActionTypes.DO_NOTHING_WALLET_ACTION;
}

export class ToggleFetchedFilteredMethodsStatus implements Action {
    readonly type = ActionTypes.TOGGLE_FETCHED_FILTERED_METHODS_STATUS;
    constructor(public payload: { status: boolean }) {}
}

export type Actions =
    | LoadBalanceRequestAction
    | LoadBalanceRequestSuccessAction
    | LoadBalanceRequestFailureAction
    | LoadBalanceV2RequestAction
    | LoadBalanceV2RequestSuccessAction
    | LoadBalanceV2RequestFailureAction
    | CreatePaymentRequestAction
    | CreatePaymentRequestSuccessAction
    | CreatePaymentRequestFailureAction
    | ApplyRechargeCouponRequestAction
    | ApplyRechargeCouponRequestSuccessAction
    | ApplyRechargeCouponRequestFailureAction
    | ClearRechargeCouponAction
    | FetchRechargeCouponOffersRequestAction
    | FetchRechargeCouponOffersRequestSuccessAction
    | FetchRechargeCouponOffersRequestFailureAction
    | LoadSuprCreditsBalanceAction
    | FetchTransactionDetailsRequestAction
    | FetchTransactionDetailsRequestSuccessAction
    | FetchTransactionDetailsRequestFailureAction
    | FetchWalletOffersRequestAction
    | FetchWalletOffersRequestSuccessAction
    | FetchWalletOffersRequestFailureAction
    | ClearWalletMetaAction
    | FetchPaymentMethods
    | FetchInitialData
    | FetchInitialDataSuccessAction
    | FetchInitialDataFailureAction
    | FetchAutoDebitPaymentModesInfoRequestAction
    | FetchAutoDebitPaymentModesInfoSuccessAction
    | FetchAutoDebitPaymentModesInfoFailureAction
    | CaptureCustomPaymentRequestAction
    | CaptureCustomPaymentRequestSuccessAction
    | CaptureCustomPaymentRequestFailureAction
    | DoNothingWalletAction
    | ToggleFetchedFilteredMethodsStatus;
