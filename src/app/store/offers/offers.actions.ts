import { Action } from "@ngrx/store";

import { OfferDetails } from "@shared/models/offers.model";

export enum ActionTypes {
    FETCH_OFFER_DETAILS_REQUEST = "[Offer store] Fetch offer details request",
    FETCH_OFFER_DETAILS_REQUEST_SUCCESS = "[Offer store] Fetch offer details request success",
    FETCH_OFFER_DETAILS_REQUEST_FAILURE = "[Offer store] Fetch offer details request failure",
}

export class FetchOfferDetailsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_OFFER_DETAILS_REQUEST;
    constructor(public payload: { offerName: string }) {}
}

export class FetchOfferDetailsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_OFFER_DETAILS_REQUEST_SUCCESS;
    constructor(public payload: OfferDetails) {}
}

export class FetchOfferDetailsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_OFFER_DETAILS_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchOfferDetailsRequestAction
    | FetchOfferDetailsRequestSuccessAction
    | FetchOfferDetailsRequestFailureAction;
