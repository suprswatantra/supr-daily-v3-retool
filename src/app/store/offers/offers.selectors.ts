import { createFeatureSelector, createSelector } from "@ngrx/store";

import { selectUrlParams } from "@store/router/router.selectors";

import { OffersState, OffersCurrentState } from "./offers.state";

export const selectOffersFeature = createFeatureSelector<OffersState>("offers");

export const selectIsFetchingOfferDetails = createSelector(
    selectOffersFeature,
    (state: OffersState) =>
        state.currentState === OffersCurrentState.FETCHING_OFFER_DETAILS
);

export const selectOfferDetailsByName = createSelector(
    selectOffersFeature,
    (state: OffersState, props: { offerName: string }) =>
        state.offerDetails && props.offerName
            ? state.offerDetails[props.offerName]
            : null
);

export const selectOfferDetailsByNameInUrl = createSelector(
    selectOffersFeature,
    selectUrlParams,
    (state: OffersState, urlParams) =>
        state.offerDetails && urlParams && urlParams["offerName"]
            ? state.offerDetails[urlParams["offerName"]]
            : null
);
