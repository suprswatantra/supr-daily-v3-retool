import { Actions, ActionTypes } from "./offers.actions";
import { initialState, OffersState, OffersCurrentState } from "./offers.state";

export function offersReducer(
    state = initialState,
    action: Actions
): OffersState {
    switch (action.type) {
        case ActionTypes.FETCH_OFFER_DETAILS_REQUEST:
            return {
                ...state,
                currentState: OffersCurrentState.FETCHING_OFFER_DETAILS,
            };

        case ActionTypes.FETCH_OFFER_DETAILS_REQUEST_SUCCESS:
            const { offerName } = action.payload;
            return {
                ...state,
                currentState: OffersCurrentState.IDLE,
                offerDetails: {
                    ...state.offerDetails,
                    [offerName]: action.payload,
                },
            };

        case ActionTypes.FETCH_OFFER_DETAILS_REQUEST_FAILURE:
            return {
                ...state,
                currentState: OffersCurrentState.IDLE,
                error: action.payload,
            };

        default: {
            return state;
        }
    }
}
