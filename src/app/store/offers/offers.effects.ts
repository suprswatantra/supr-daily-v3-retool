import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of as observableOf } from "rxjs";
import { catchError, switchMap, map } from "rxjs/operators";

import { OfferDetails } from "@shared/models";

import { ApiService } from "@services/data/api.service";

import * as OffersActions from "./offers.actions";

@Injectable()
export class OffersEffects {
    constructor(private apiService: ApiService, private actions$: Actions) {}

    @Effect()
    fetchOfferDetailsEffect$: Observable<Action> = this.actions$.pipe(
        ofType<OffersActions.FetchOfferDetailsRequestAction>(
            OffersActions.ActionTypes.FETCH_OFFER_DETAILS_REQUEST
        ),
        switchMap((action) =>
            this.apiService.fetchOfferDetails(action.payload.offerName).pipe(
                map((data: OfferDetails) => {
                    return new OffersActions.FetchOfferDetailsRequestSuccessAction(
                        data
                    );
                }),
                catchError((error) =>
                    observableOf(
                        new OffersActions.FetchOfferDetailsRequestFailureAction(
                            error
                        )
                    )
                )
            )
        )
    );
}
