import { OfferDetails } from "@shared/models/offers.model";

export enum OffersCurrentState {
    IDLE = "idle",
    FETCHING_OFFER_DETAILS = "fetching_offer_details",
}

export interface OffersState {
    currentState: OffersCurrentState;
    error?: any;
    offerDetails: {
        [offerName: string]: OfferDetails;
    };
}

export const initialState: OffersState = {
    currentState: OffersCurrentState.IDLE,
    error: null,
    offerDetails: {},
};
