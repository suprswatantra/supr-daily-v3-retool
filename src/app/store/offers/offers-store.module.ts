import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { offersReducer } from "./offers.reducer";
import { OffersEffects } from "./offers.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("offers", offersReducer),
        EffectsModule.forFeature([OffersEffects]),
    ],
    providers: [],
})
export class OffersStoreModule {}
