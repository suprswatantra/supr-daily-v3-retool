import { Action } from "@ngrx/store";
import { CollectionDetail, CollectionListWithPosition } from "@models";

export enum ActionTypes {
    FETCH_COLLECTION_LIST_REQUEST_ACTION = "[COLLECTION] Fetch collection list request",
    FETCH_COLLECTION_LIST_REQUEST_SUCCESS_ACTION = "[COLLECTION] Fetch collection list request success",
    FETCH_COLLECTION_LIST_REQUEST_FAILURE_ACTION = "[COLLECTION] Fetch collection list request failure",

    FETCH_COLLECTION_DETAILS_REQUEST_ACTION = "[COLLECTION] Fetch collection details request",
    FETCH_COLLECTION_DETAILS_REQUEST_SUCCESS_ACTION = "[COLLECTION] Fetch collection details request success",
    FETCH_COLLECTION_DETAILS_REQUEST_FAILURE_ACTION = "[COLLECTION] Fetch collection details request failure",

    FETCH_PAST_ORDER_SKU_LIST_REQUEST_ACTION = "[PAST_ORDER SKU] Fetch PAST_ORDER list request",
    FETCH_PAST_ORDER_SKU_LIST_REQUEST_SUCCESS_ACTION = "[PAST_ORDER SKU] Fetch Past order sku list request success",
    FETCH_PAST_ORDER_SKU_LIST_REQUEST_FAILURE_ACTION = "[PAST_ORDER SKU] Fetch Past order sku request failure",

    UPDATE_PAST_ORDER_SKU_LIST = "[PAST_ORDER SKU] update sku List",
}

export class FetchCollectionListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_LIST_REQUEST_ACTION;
    constructor(public payload?: boolean) {}
}

export class FetchCollectionListRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: CollectionListWithPosition) {}
}

export class FetchCollectionListRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchCollectionDetailsRequestAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_ACTION;
    constructor(public payload: { viewId: number }) {}
}

export class FetchCollectionDetailsRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_SUCCESS_ACTION;
    constructor(public payload: { collection: CollectionDetail }) {}
}

export class FetchCollectionDetailsRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export class FetchPastOrderSkuListRequestAction implements Action {
    readonly type = ActionTypes.FETCH_PAST_ORDER_SKU_LIST_REQUEST_ACTION;
}

export class FetchPastOrderSkuListRequestSuccessAction implements Action {
    readonly type =
        ActionTypes.FETCH_PAST_ORDER_SKU_LIST_REQUEST_SUCCESS_ACTION;
    constructor(public payload: Array<number>) {}
}

export class UpdatePastOrderSkuListAction implements Action {
    readonly type = ActionTypes.UPDATE_PAST_ORDER_SKU_LIST;
    constructor(public payload: number[]) {}
}

export class FetchPastOrderSkuListRequestFailureAction implements Action {
    readonly type =
        ActionTypes.FETCH_PAST_ORDER_SKU_LIST_REQUEST_FAILURE_ACTION;
    constructor(public payload: any) {}
}

export type Actions =
    | FetchCollectionListRequestAction
    | FetchCollectionListRequestSuccessAction
    | FetchCollectionListRequestFailureAction
    | FetchCollectionDetailsRequestAction
    | FetchCollectionDetailsRequestSuccessAction
    | FetchCollectionDetailsRequestFailureAction
    | FetchPastOrderSkuListRequestAction
    | FetchPastOrderSkuListRequestSuccessAction
    | FetchPastOrderSkuListRequestFailureAction
    | UpdatePastOrderSkuListAction;
