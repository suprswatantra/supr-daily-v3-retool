import { Actions, ActionTypes } from "./collection.actions";
import {
    initialState,
    CollectionState,
    CollectionCurrentState,
} from "./collection.state";

export function collectionReducer(
    state = initialState,
    action: Actions
): CollectionState {
    switch (action.type) {
        case ActionTypes.FETCH_COLLECTION_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS,
            };
        }

        case ActionTypes.FETCH_COLLECTION_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS_DONE,
                collectionList: action.payload,
            };
        }

        case ActionTypes.FETCH_COLLECTION_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS_DONE,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState:
                    CollectionCurrentState.FETCHING_COLLECTION_DETAILS,
            };
        }

        case ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState:
                    CollectionCurrentState.FETCHING_COLLECTION_DETAILS_DONE,
                collectionDetailsList: {
                    ...state.collectionDetailsList,
                    [action.payload.collection &&
                    action.payload.collection.viewId]: action.payload
                        .collection,
                },
            };
        }

        case ActionTypes.FETCH_COLLECTION_DETAILS_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState:
                    CollectionCurrentState.FETCHING_COLLECTION_DETAILS_DONE,
                error: action.payload,
            };
        }

        case ActionTypes.FETCH_PAST_ORDER_SKU_LIST_REQUEST_ACTION: {
            return {
                ...state,
                error: null,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS,
            };
        }

        case ActionTypes.FETCH_PAST_ORDER_SKU_LIST_REQUEST_SUCCESS_ACTION: {
            return {
                ...state,
                error: null,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS_DONE,
                pastOrderSkuListOriginalCopy: action.payload,
            };
        }

        case ActionTypes.UPDATE_PAST_ORDER_SKU_LIST: {
            return {
                ...state,
                error: null,
                pastOrderSkuList: action.payload,
            };
        }

        case ActionTypes.FETCH_COLLECTION_LIST_REQUEST_FAILURE_ACTION: {
            return {
                ...state,
                currentState: CollectionCurrentState.FETCHING_COLLECTIONS_DONE,
                error: action.payload,
            };
        }

        default: {
            return state;
        }
    }
}
