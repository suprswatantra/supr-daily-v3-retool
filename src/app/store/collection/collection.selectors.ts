import { createSelector, createFeatureSelector } from "@ngrx/store";

import { selectUrlParams } from "@store/router/router.selectors";

import { CollectionState } from "./collection.state";
import { CollectionListWithPosition } from "@shared/models";

export const selectFeature = createFeatureSelector<CollectionState>(
    "collection"
);

export const selectCollectionList = createSelector(
    selectFeature,
    (state: CollectionState) => state.collectionList
);

export const selectCollectionListByPosition = createSelector(
    selectCollectionList,
    (collectionList: CollectionListWithPosition, props: { position: string }) =>
        collectionList[props.position]
);

export const selectCollectionCurrentState = createSelector(
    selectFeature,
    (state: CollectionState) => state.currentState
);

export const selectFeaturedError = createSelector(
    selectFeature,
    (state: CollectionState) => state.error
);

export const selectCollectionDetailsList = createSelector(
    selectFeature,
    (state: CollectionState) => state.collectionDetailsList
);

export const selectPastOrderSkuList = createSelector(
    selectFeature,
    (state: CollectionState) => state.pastOrderSkuList
);

export const selectPastOrderSkuListOriginal = createSelector(
    selectFeature,
    (state: CollectionState) => state.pastOrderSkuListOriginalCopy
);

export const selectCollectionDetailsById = createSelector(
    selectCollectionDetailsList,
    selectUrlParams,
    (collectionDict, urlParams) => collectionDict[urlParams["viewId"]]
);
