import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, select, Store } from "@ngrx/store";

import { Observable, of as observableOf, forkJoin } from "rxjs";
import {
    catchError,
    map,
    switchMap,
    withLatestFrom,
    filter,
} from "rxjs/operators";

import { SETTINGS } from "@constants";

import {
    CollectionPositions,
    CollectionListWithPosition,
    Collection,
} from "@models";

import { ApiService } from "@services/data/api.service";
import { SettingsService } from "@services/shared/settings.service";
import { SkuService } from "@services/shared/sku.service";

import { StoreState } from "@store/store.state";
import * as collectionActions from "./collection.actions";
import * as SkuStoreSelectors from "@store/sku/sku.selectors";
import * as CollectionStoreSelectors from "./collection.selectors";
import * as CartStoreSelectors from "@store/cart/cart.selectors";
import * as UserStoreSelectors from "@store/user/user.selectors";

@Injectable()
export class CollectionStoreEffects {
    constructor(
        private apiService: ApiService,
        private actions$: Actions,
        private store$: Store<StoreState>,
        private settingService: SettingsService,
        private skuService: SkuService
    ) {}

    @Effect()
    fetchCollectionList$: Observable<Action> = this.actions$.pipe(
        ofType<collectionActions.FetchCollectionListRequestAction>(
            collectionActions.ActionTypes.FETCH_COLLECTION_LIST_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(select(UserStoreSelectors.isAnonymousUser))
        ),
        switchMap(([action, isAnonymousUser]) => {
            const collectionPositions = Object.values(CollectionPositions);
            const apiCalls = collectionPositions.map((position) =>
                this.apiService.fetchCollectionList(position, action.payload)
            );
            return forkJoin(apiCalls).pipe(
                switchMap((results) => {
                    const collectionsWithPosition = <
                        CollectionListWithPosition
                    >{};
                    collectionPositions.forEach((position, index) => {
                        collectionsWithPosition[position] = results[index];
                    });
                    const actionsArray = [
                        new collectionActions.FetchCollectionListRequestSuccessAction(
                            collectionsWithPosition
                        ),
                    ];
                    if (!isAnonymousUser) {
                        return [
                            ...actionsArray,
                            new collectionActions.FetchPastOrderSkuListRequestAction(),
                        ];
                    }
                    return actionsArray;
                }),
                catchError((error) =>
                    observableOf(
                        new collectionActions.FetchCollectionListRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchCollectionDetails$: Observable<Action> = this.actions$.pipe(
        ofType<collectionActions.FetchCollectionDetailsRequestAction>(
            collectionActions.ActionTypes
                .FETCH_COLLECTION_DETAILS_REQUEST_ACTION
        ),
        switchMap((action) => {
            const { viewId } = action.payload;
            return this.apiService.fetchCollectionDetails(viewId).pipe(
                map(
                    (collection) =>
                        new collectionActions.FetchCollectionDetailsRequestSuccessAction(
                            { collection }
                        )
                ),
                catchError((error) =>
                    observableOf(
                        new collectionActions.FetchCollectionDetailsRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    @Effect()
    fetchPastOrderSkuList$: Observable<Action> = this.actions$.pipe(
        ofType<collectionActions.FetchPastOrderSkuListRequestAction>(
            collectionActions.ActionTypes
                .FETCH_PAST_ORDER_SKU_LIST_REQUEST_ACTION
        ),
        withLatestFrom(
            this.store$.pipe(
                select(CollectionStoreSelectors.selectCollectionList)
            ),
            this.store$.pipe(select(SkuStoreSelectors.selectSkuDict)),
            this.store$.pipe(select(CartStoreSelectors.selectCartItems)),
            this.store$.pipe(select(UserStoreSelectors.isAnonymousUser))
        ),
        filter(
            ([
                _action,
                _collectionList,
                _skuDict,
                _cartItems,
                isAnonymousUser,
            ]) => isAnonymousUser === false
        ),
        switchMap(([_, collectionList, skuDict, cartItems]) => {
            return this.apiService.fetchPastOrderSkuList().pipe(
                switchMap((skuList) => {
                    /* Filtering out items not available in Sku while accessing Past order */
                    skuList = this.skuService.filterPastOrderskuFromList(
                        skuList,
                        skuDict
                    );

                    /* Filtering out oos items from the above filtered list */
                    let filteredSkuList = this.skuService.filterOOSSkuFromList(
                        skuList,
                        skuDict
                    );

                    if (cartItems && cartItems.length) {
                        /* Filtering out cart items from past order cart collection */
                        filteredSkuList = this.skuService.filterCartItemsFromPastOrderList(
                            filteredSkuList,
                            cartItems
                        );
                    }

                    return [
                        this.createCollectionFromPastOrders(
                            { ...collectionList },
                            skuList
                        ),
                        new collectionActions.FetchPastOrderSkuListRequestSuccessAction(
                            skuList
                        ),
                        new collectionActions.UpdatePastOrderSkuListAction(
                            filteredSkuList
                        ),
                    ];
                }),
                catchError((error) =>
                    observableOf(
                        new collectionActions.FetchPastOrderSkuListRequestFailureAction(
                            error
                        )
                    )
                )
            );
        })
    );

    /**Helper methods */

    private createCollectionFromPastOrders(
        collectionList: CollectionListWithPosition,
        skuList: Array<number>
    ): Action {
        if (!skuList.length) {
            return new collectionActions.FetchCollectionListRequestSuccessAction(
                collectionList
            );
        }

        const pastOrder = this.settingService.getSettingsValue(
            SETTINGS.PAST_ORDER_COLLECTION
        );

        const items = this.getItems(skuList);

        pastOrder["items"] = items;
        pastOrder.pastOrder = true;

        const homeSectionData = this.getNonPastOrderData(
            collectionList.home_section_1
        );

        collectionList.home_section_1 = [...homeSectionData, pastOrder];

        return new collectionActions.FetchCollectionListRequestSuccessAction(
            collectionList
        );
    }

    private getItems(skuList: Array<number>) {
        return skuList.map((skuId) => {
            return {
                image: null,
                entityId: skuId,
                borderColor: "#F2F2F2",
                preferredMode: "direct_add",
            };
        });
    }

    private getNonPastOrderData(collections: Collection[]) {
        return [...collections].filter((collection) => {
            if (!collection.hasOwnProperty("pastOrder")) {
                return collection;
            }
        });
    }
}
