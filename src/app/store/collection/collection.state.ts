import { CollectionListWithPosition } from "@models";
import { CollectionDict } from "@types";

export enum CollectionCurrentState {
    NO_ACTION = "0",
    FETCHING_COLLECTIONS = "1",
    FETCHING_COLLECTIONS_DONE = "2",
    FETCHING_COLLECTION_DETAILS = "3",
    FETCHING_COLLECTION_DETAILS_DONE = "4",
}

export interface CollectionState {
    currentState?: CollectionCurrentState;
    error?: any;
    collectionList: CollectionListWithPosition;
    collectionDetailsList: CollectionDict;
    pastOrderSkuList: Array<number>;
    pastOrderSkuListOriginalCopy: Array<number>;
}

export const initialState: CollectionState = {
    currentState: CollectionCurrentState.NO_ACTION,
    error: null,
    collectionList: <CollectionListWithPosition>{},
    collectionDetailsList: {},
    pastOrderSkuList: [],
    pastOrderSkuListOriginalCopy: [],
};
