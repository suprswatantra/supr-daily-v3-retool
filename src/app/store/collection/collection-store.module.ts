import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { collectionReducer } from "./collection.reducer";
import { CollectionStoreEffects } from "./collection.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("collection", collectionReducer),
        EffectsModule.forFeature([CollectionStoreEffects]),
    ],
    providers: [],
})
export class CollectionStoreModule {}
