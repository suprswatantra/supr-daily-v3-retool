import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";

import { cartReducer } from "./cart.reducer";
import { CartStoreEffects } from "./cart.effects";

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature("cart", cartReducer),
        EffectsModule.forFeature([CartStoreEffects]),
    ],
    providers: [],
})
export class CartStoreModule {}
