import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action, select, Store } from "@ngrx/store";

import { Observable, of as observableOf, from } from "rxjs";
import {
    map,
    switchMap,
    withLatestFrom,
    catchError,
    tap,
} from "rxjs/operators";

import {
    ANALYTICS_OBJECT_NAMES,
    CART_ITEM_TYPES,
    CUSTOM_EVENTS,
    SETTINGS,
} from "@constants";

import { ApiService } from "@services/data/api.service";
import { ErrorService } from "@services/integration/error.service";
import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { SettingsService } from "@services/shared/settings.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { WalletStoreActions } from "@store/wallet";
import { UserStoreActions, UserStoreSelectors } from "@store/user";
import { SuprPassStoreActions } from "@store/supr-pass";
import { ReferralStoreActions } from "@store/referral";
import { SubscriptionStoreActions } from "@store/subscription";
import { DeliveryStatusStoreActions } from "@store/delivery-status";
import {
    CollectionStoreActions,
    CollectionStoreSelectors,
} from "@store/collection";

import {
    CartItem,
    CartOrder,
    CheckoutError,
    CartItemDeliveryType,
    AutoAdd,
    OffersList,
} from "@models";

import { CartItemsFilterKey, SuprApi } from "@types";

import * as cartActions from "./cart.actions";
import * as CartStoreSelectors from "./cart.selectors";
import { CartService } from "../../services/shared/cart.service";
import { StoreState } from "../store.state";
import * as VacationStoreSelectors from "../vacation/vacation.selectors";
import { SkuStoreSelectors, SkuStoreActions } from "@store/sku";
import { SearchStoreActions } from "@store/search";

@Injectable()
export class CartStoreEffects {
    constructor(
        private apiService: ApiService,
        private skuService: SkuService,
        private utilService: UtilService,
        private cartService: CartService,
        private errorService: ErrorService,
        private settingsService: SettingsService,
        private analyticsService: AnalyticsService,

        private actions$: Actions,
        private store$: Store<StoreState>
    ) {}

    @Effect()
    updateItemInCart$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.UpdateItemInCartAction>(
            cartActions.ActionTypes.UPDATE_ITEM_IN_CART
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature)),
            this.store$.pipe(select(SkuStoreSelectors.selectSkuDict)),
            this.store$.pipe(
                select(CollectionStoreSelectors.selectPastOrderSkuListOriginal)
            )
        ),
        switchMap(([action, cartState, skuDict, pastOrderSkuList]) => {
            const { item, validateCart, addRewardSku } = action.payload;
            const { cartItems, uuid, useSuprCredits } = cartState;

            const updatedItems = this.getUpdatedCartItems(cartItems, item);
            const updatedUuid = this.getUuid(updatedItems, uuid);

            if (updatedItems && updatedItems.length) {
                /* Filtering out cart items from past order cart collection */
                let filteredPastOrderSkuList = this.skuService.filterCartItemsFromPastOrderList(
                    pastOrderSkuList,
                    updatedItems
                );
                /* Filtering out oos items from the above filtered list */
                filteredPastOrderSkuList = this.skuService.filterOOSSkuFromList(
                    filteredPastOrderSkuList,
                    skuDict
                );

                const hasItemBeenRemovedFromCart =
                    this.utilService.isLengthyArray(updatedItems) &&
                    this.utilService.isLengthyArray(cartItems) &&
                    updatedItems.length < cartItems.length;

                if (validateCart) {
                    const cartBody = this.getCartBodyFromItems(
                        updatedItems,
                        updatedUuid,
                        useSuprCredits
                    );

                    // tslint:disable-next-line: no-shadowed-variable
                    const actions: Action[] = [
                        new CollectionStoreActions.UpdatePastOrderSkuListAction(
                            filteredPastOrderSkuList
                        ),
                        new cartActions.ValidateCartRequestAction({
                            cartBody,
                            couponApplying: false,
                            addRewardSku,
                        }),
                    ];

                    /* remove search instrumentation context when item is removed from cart */
                    if (hasItemBeenRemovedFromCart) {
                        actions.push(
                            new SearchStoreActions.UpdateSearchInstrumentationDb(
                                updatedItems
                            )
                        );
                    }

                    return actions;
                }

                // Or just update items in cart
                const _cartBody = {
                    items: updatedItems,
                    uuid: updatedUuid,
                };

                // Update it in DB storage too.
                this.setCartInDB(_cartBody);

                // Update in store
                const actions: Action[] = [
                    new CollectionStoreActions.UpdatePastOrderSkuListAction(
                        filteredPastOrderSkuList
                    ),
                    new cartActions.UpdateItemsInCartAction(_cartBody),
                ];

                /* remove search instrumentation context when item is removed from cart */
                if (hasItemBeenRemovedFromCart) {
                    actions.push(
                        new SearchStoreActions.UpdateSearchInstrumentationDb(
                            updatedItems
                        )
                    );
                }

                return actions;
            } else {
                // Update it in DB storage too.
                this.cartService.clearCartFromDB();
                /* remove search instrumentation context when item is removed from cart */
                return [
                    new cartActions.ClearCartAction(),
                    new SearchStoreActions.ClearSearchInstrumentationDb(),
                ];
            }
        })
    );

    @Effect()
    validateCoupon$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.UpdateCouponInCartAction>(
            cartActions.ActionTypes.UPDATE_COUPON_IN_CART
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature))
        ),
        switchMap(([action, cartState]) => {
            const { couponCode } = action.payload;
            const { cartItems, uuid } = cartState;

            const cartBody = this.getCartBodyWithCoupon(
                cartItems,
                uuid,
                couponCode
            );

            return observableOf(
                new cartActions.ValidateCartRequestAction({
                    cartBody,
                    couponApplying: !!cartBody.coupon_code,
                })
            );
        })
    );

    @Effect()
    useSuprCredits$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.UseSuprCreditsInCartAction>(
            cartActions.ActionTypes.USE_SUPR_CREDITS_IN_CART
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature))
        ),
        switchMap(([action, cartState]) => {
            const { useSuprCredits } = action.payload;
            const { cartItems, uuid } = cartState;
            const disableCoupon = true;

            const cartBody = this.getCartBodyFromItems(
                cartItems,
                uuid,
                useSuprCredits,
                disableCoupon
            );

            return observableOf(
                new cartActions.ValidateCartRequestAction({
                    cartBody,
                })
            );
        })
    );

    @Effect()
    validateCart$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.ValidateCartRequestAction>(
            cartActions.ActionTypes.VALIDATE_CART_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature)),
            this.store$.pipe(select(CartStoreSelectors.selectAddRewardSku))
        ),
        switchMap(([action, cartState, addRewardSku]) => {
            const {
                uuid,
                cartItems,
                useSuprCredits,
                couponApplying,
            } = cartState;
            let cartBody: SuprApi.CartBody;

            if (action.payload && action.payload.cartBody) {
                const useSc =
                    !action.payload.cartBody.coupon_code && useSuprCredits;
                const _uuid =
                    uuid ||
                    action.payload.cartBody.uuid ||
                    this.getUuid(action.payload.cartBody.items, uuid);

                cartBody = {
                    ...action.payload.cartBody,
                    uuid: _uuid,
                    use_supr_credits: useSc,
                };
            } else {
                cartBody = this.getCartBodyFromItems(
                    cartItems,
                    uuid,
                    useSuprCredits
                );
            }

            return from(this.canAutoAdd()).pipe(
                switchMap((canAutoAddValue) => {
                    // Add this only if user is part of auto add group.
                    // This key is used to determine "show_supr_pass_promt" info in cart meta by backend apart from adding suprpass
                    if (
                        canAutoAddValue &&
                        canAutoAddValue.suprAccess !== undefined
                    ) {
                        cartBody.auto_add_supr_access =
                            canAutoAddValue.suprAccess;
                    }

                    if (
                        canAutoAddValue &&
                        canAutoAddValue.referralCode !== undefined
                    ) {
                        cartBody.auto_apply_referral_code =
                            canAutoAddValue.referralCode;
                    }
                    // cartBody.auto_add_reward_sku = true;

                    if (action.payload.addRewardSku || addRewardSku) {
                        // Add to cart discounted basket offer
                        cartBody.add_reward_sku_to_cart = true;
                    } else {
                        // Remove from to cart discounted basket offer
                        cartBody.add_reward_sku_to_cart = false;
                    }

                    // Save it in DB storage before API call
                    this.setCartInDB(cartBody);

                    // Now make api call
                    return this.apiService.validateCart(cartBody).pipe(
                        tap((cartRes: SuprApi.CartRes) => {
                            // track addon regular and alternate delivery
                            this.trackAddonCartItems(cartRes);

                            // track coupon applied event
                            if (couponApplying) {
                                this.trackCartCouponApplied(cartRes);
                            }
                        }),
                        switchMap((cartRes: SuprApi.CartRes) => {
                            // Save it in DB storage after API call success
                            this.setCartResInDB(cartRes);

                            const actions: Action[] = [
                                new cartActions.ValidateCartRequestSuccessAction(
                                    {
                                        cart: cartRes.data.cart,
                                        isValid: this.isValidCartRes(cartRes),
                                    }
                                ),
                            ];

                            if (!couponApplying) {
                                actions.push(
                                    new cartActions.FetchCartOffersRequestAction()
                                );
                            }

                            return actions;
                        }),
                        catchError((error) =>
                            observableOf(
                                new cartActions.ValidateCartRequestFailureAction(
                                    error
                                )
                            )
                        )
                    );
                })
            );
        })
    );

    @Effect()
    checkoutCart$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.CheckoutCartRequestAction>(
            cartActions.ActionTypes.CHECKOUT_CART_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature)),
            this.store$.pipe(
                select(UserStoreSelectors.selectIsUserEligibleForSuprPass)
            ),
            this.store$.pipe(
                select(UserStoreSelectors.selectIsUserEligibleForReferral)
            )
        ),
        switchMap(
            ([
                _,
                cartState,
                isUserEligibleForSuprPass,
                isUserEligibleForReferral,
            ]) => {
                const {
                    cartItems,
                    uuid,
                    useSuprCredits,
                    addRewardSku,
                } = cartState;
                const cartBody = this.getCartBodyFromItems(
                    cartItems,
                    uuid,
                    useSuprCredits,
                    null,
                    addRewardSku
                );

                return this.apiService.checkoutCart(cartBody).pipe(
                    switchMap((checkoutRes: SuprApi.CheckoutRes) => {
                        if (this.isValidCartRes(checkoutRes)) {
                            this.cartService.clearCartFromDB();
                            const skuIds = cartItems.map(
                                (cartItem: CartItem) => cartItem.sku_id
                            );
                            let actions: Action[] = [
                                new WalletStoreActions.LoadBalanceRequestAction(),
                                new DeliveryStatusStoreActions.EnableRefreshDeliveryStatus(),
                                new CollectionStoreActions.FetchPastOrderSkuListRequestAction(),
                                new SubscriptionStoreActions.FetchSubscriptionListRequestAction(),
                                new cartActions.CheckoutCartRequestSuccessAction(
                                    checkoutRes.data as CartOrder
                                ),
                                new SearchStoreActions.SendCheckoutInstrumentation(
                                    skuIds
                                ),
                            ];

                            if (
                                isUserEligibleForSuprPass ||
                                isUserEligibleForReferral
                            ) {
                                actions = actions.concat([
                                    new UserStoreActions.LoadProfileWithAddressRequestAction(
                                        {
                                            address: true,
                                            wallet: true,
                                            t_plus_one: true,
                                            subscriptions: false,
                                        }
                                    ),
                                ]);
                            }

                            if (isUserEligibleForReferral) {
                                actions = actions.concat([
                                    new ReferralStoreActions.FetchReferralRequestAction(
                                        { silent: true }
                                    ),
                                ]);
                            }

                            if (isUserEligibleForSuprPass) {
                                actions = actions.concat([
                                    new SuprPassStoreActions.FetchSuprPassRequestAction(
                                        { silent: true }
                                    ),
                                ]);
                            }

                            if (this.hasSuprPassInCart(cartItems)) {
                                actions = actions.concat([
                                    new SkuStoreActions.FetchSkuListRequestAction(
                                        {
                                            refresh: true,
                                            silent: true,
                                        }
                                    ),
                                    new SkuStoreActions.FetchSkuAttributesRequestAction(
                                        {
                                            refresh: true,
                                            silent: true,
                                        }
                                    ),
                                ]);
                            }

                            return actions;
                        }
                        return [
                            new cartActions.CheckoutCartRequestFailureAction({
                                meta: checkoutRes.data as CheckoutError,
                            }),
                        ];
                    }),
                    catchError((error) =>
                        observableOf(
                            new cartActions.CheckoutCartRequestFailureAction({
                                error,
                            })
                        )
                    )
                );
            }
        )
    );

    @Effect()
    autopushCartItems$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.AutoPushCartItems>(
            cartActions.ActionTypes.AUTOPUSH_CART_ITEMS
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature)),
            this.store$.pipe(select(VacationStoreSelectors.selectVacation)),
            this.store$.pipe(select(SkuStoreSelectors.selectSkuDict))
        ),
        switchMap(([_, cartState, vacation, skuDict]) => {
            const { cartItems, uuid } = cartState;

            const updatedCartItems = this.cartService.autoPushDeliveryDates(
                cartItems,
                vacation,
                skuDict
            );

            return observableOf(
                new cartActions.UpdateItemsInCartAction({
                    items: updatedCartItems,
                    uuid,
                })
            );
        })
    );

    @Effect()
    fetchCartOffersEffect$: Observable<Action> = this.actions$.pipe(
        ofType<cartActions.FetchCartOffersRequestAction>(
            cartActions.ActionTypes.FETCH_CART_OFFERS_REQUEST
        ),
        withLatestFrom(
            this.store$.pipe(select(CartStoreSelectors.selectFeature))
        ),
        switchMap(([_action, cartState]) => {
            const { uuid, cartItems, useSuprCredits } = cartState;
            const cartBody: SuprApi.CartBody = this.getCartBodyFromItems(
                cartItems,
                uuid,
                useSuprCredits
            );

            return this.apiService.fetchCartOffers(cartBody).pipe(
                map(
                    (response: OffersList) =>
                        new cartActions.FetchCartOffersRequestSuccessAction({
                            offersList: response,
                        })
                ),
                catchError((_error) =>
                    observableOf(
                        new cartActions.FetchCartOffersRequestFailureAction()
                    )
                )
            );
        })
    );

    private getUpdatedCartItems(
        items: CartItem[],
        newItem: CartItem
    ): CartItem[] {
        if (!newItem) {
            return [];
        }

        /**  using different keys for recharge and normal helps us in using sku
         * in two modes simultaneously: sub_recharge and addon/sub_new */
        const keyToUse =
            newItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
                ? "subscription_id"
                : "sku_id";

        /** Segregate items into check and not check list for data sanity.
         example if we have a sku present in both sub_recharge and addon/sub_new,
         deleting it from one shouldn't remove it everywhere.

         Segregation is done based on sub_recharge type because addon/sub_new can be interchanged at cart
         but recharge can not be moved into another type*/

        const itemsToCheck = items.filter((item) => {
            if (!item) {
                return;
            }

            return newItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
                ? item.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
                : item.type !== CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE;
        });

        const itemsToNotCheck = items.filter((item) => {
            if (!item) {
                return;
            }

            return newItem.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
                ? item.type !== CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE
                : item.type === CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE;
        });

        let updatedItems = [];
        /**  NOTE: Subscription_recharge doesn't not have quantity
         * until and unless it is being deleted from cart, in which case the quantity is 0. */
        if (newItem.quantity === 0) {
            updatedItems = this.removeItemsHelper(
                itemsToCheck,
                newItem,
                keyToUse
            );
        } else {
            updatedItems = this.updateItemsHelper(
                itemsToCheck,
                newItem,
                keyToUse
            );
        }
        return [...itemsToNotCheck, ...updatedItems];
    }

    private updateItemsHelper(
        items: CartItem[],
        newItem: CartItem,
        keyToUse: CartItemsFilterKey
    ): CartItem[] {
        let foundItem = false;
        const updatedItems = items.map((item) => {
            if (!item.is_reward_sku) {
                if (item[keyToUse] === newItem[keyToUse]) {
                    foundItem = true;
                    return newItem;
                } else {
                    return item;
                }
            } else {
                return item;
            }
        });

        if (!foundItem) {
            updatedItems.push(newItem);
        }

        return updatedItems;
    }

    private removeItemsHelper(
        items: CartItem[],
        newItem: CartItem,
        keyToUse: CartItemsFilterKey
    ): CartItem[] {
        return items.filter((item) => item[keyToUse] !== newItem[keyToUse]);
    }

    private getCartBodyFromItems(
        items: CartItem[],
        uuid: string,
        useSuprCredits?: boolean,
        disableCoupon?: boolean,
        addRewardSku?: boolean
    ): SuprApi.CartBody {
        let coupon_code: string;
        const itemWithCoupon = items.find((item) => !!item.coupon_code);
        if (itemWithCoupon) {
            coupon_code = itemWithCoupon.coupon_code;
        }

        const body = <SuprApi.CartBody>{
            items,
            uuid,
        };

        if (!body.uuid) {
            body.uuid = this.getUuid(items, uuid);
        }

        if (coupon_code && !disableCoupon) {
            body.coupon_code = coupon_code;
        } else {
            body.use_supr_credits = useSuprCredits;
        }

        body.add_reward_sku_to_cart =
            addRewardSku !== undefined ? addRewardSku : false;

        return body;
    }

    private hasSuprPassInCart(cartItems: CartItem[]): boolean {
        if (!this.utilService.isLengthyArray(cartItems)) {
            return false;
        }

        return cartItems.some(
            (cartItem) => cartItem && cartItem.is_supr_access
        );
    }

    private getCartBodyWithCoupon(
        items: CartItem[],
        uuid: string,
        couponCode?: string
    ): SuprApi.CartBody {
        const body = <SuprApi.CartBody>{
            items,
            uuid,
        };

        if (couponCode) {
            body.coupon_code = couponCode;
            body.use_supr_credits = false;
        }

        return body;
    }

    private getUuid(cartItems: CartItem[], uuid?: string): string {
        if (!cartItems || !cartItems.length) {
            return "";
        } else if (uuid) {
            return uuid;
        } else {
            return this.utilService.uuid();
        }
    }

    private isValidCartRes(
        res: SuprApi.CartRes | SuprApi.CheckoutRes
    ): boolean {
        return res && res.statusCode === 0;
    }

    private setCartResInDB(res: SuprApi.CartRes) {
        const { cart } = res.data;
        const { items, uuid } = cart;

        this.setCartInDB({
            items,
            uuid,
        });
    }

    private setCartInDB(cartBody: SuprApi.CartBody) {
        this.cartService.setCartInDB(cartBody);
    }

    private trackCartCouponApplied(res: SuprApi.CartRes) {
        try {
            const { cart } = res.data;
            const discount_info = this.utilService.getNestedValue(
                cart,
                "meta.discount_info",
                null
            );
            if (
                discount_info &&
                discount_info.coupon_code &&
                discount_info.is_applied
            ) {
                const payment_data = this.utilService.getNestedValue(
                    cart,
                    "meta.payment_data",
                    {}
                );
                this.analyticsService.trackCustom(
                    CUSTOM_EVENTS.CART_COUPON_APPLIED,
                    {
                        coupon_code: discount_info.coupon_code,
                        cart_value: payment_data.pre_discount,
                        payable_ammount: payment_data.real_amount,
                        discount_type: discount_info.discount_type,
                        discount_amount: discount_info.discount_amount,
                        is_payment_coupon: discount_info.is_payment_coupon,
                        is_recharge_coupon: discount_info.is_recharge_coupon,
                    }
                );
            }
        } catch (error) {
            this.errorService.logSentryError(error, {
                message: "Error tracking cart coupon applied event",
            });
        }
    }

    private trackAddonCartItems(res: SuprApi.CartRes) {
        try {
            const { cart } = res.data;
            const { items = [], uuid } = cart;

            if (items.length > 0) {
                const addOns = items.filter(
                    (item) => item && item.type === CART_ITEM_TYPES.ADDON
                );
                const ad_value = this.getAddonsAlternateDeliveryValue(addOns);
                const rd_value = this.getAddonsRegularDeliveryValue(addOns);
                const suprPassInfo = items.find((item) => item.is_supr_access);

                const contextList = [
                    { name: "cartId", value: uuid },
                    { name: "ad_value", value: ad_value },
                    { name: "rd_value", value: rd_value },
                    { name: "total_addon_value", value: rd_value + ad_value },
                    {
                        name: "total_cart_value",
                        value: this.utilService.getNestedValue(
                            cart,
                            "meta.payment_data.post_discount",
                            "-"
                        ),
                    },
                ];

                if (suprPassInfo) {
                    contextList.push({
                        name: "supr_access",
                        value: suprPassInfo.plan_id,
                    });
                    contextList.push({
                        name: "supr_access_auto_added",
                        value: suprPassInfo.is_auto_added,
                    });
                }

                const obj = {};

                contextList.forEach((item, index) => {
                    obj[`context_${index + 1}_name`] = item.name;
                    obj[`context_${index + 1}_value`] = item.value;
                });

                this.analyticsService.trackImpression({
                    objectName: ANALYTICS_OBJECT_NAMES.IMPRESSION.CART_CHANGES,
                    ...obj,
                });
            }
        } catch (error) {
            this.errorService.logSentryError(error, {
                message: "Error tracking cart changes impression",
            });
        }
    }

    private getAddonsAlternateDeliveryValue(items: CartItem[]): number {
        return items.reduce((acc, item) => {
            if (
                item.delivery_type &&
                item.delivery_type === CartItemDeliveryType.ALTERNATE_DELIVERY
            ) {
                const price = this.utilService.getNestedValue(
                    item,
                    "payment_data.post_discount",
                    0
                );
                acc = acc + price;
            }
            return acc;
        }, 0);
    }

    private getAddonsRegularDeliveryValue(items: CartItem[]): number {
        return items.reduce((acc, item) => {
            if (
                !item.delivery_type ||
                item.delivery_type === CartItemDeliveryType.REGULAR_DELIVERY
            ) {
                const price = this.utilService.getNestedValue(
                    item,
                    "payment_data.post_discount",
                    0
                );
                acc = acc + price;
            }
            return acc;
        }, 0);
    }

    private async canAutoAdd(): Promise<AutoAdd> {
        const addSuprAccessToCartConfig = this.settingsService.getSettingsValue(
            SETTINGS.ADD_SUPR_ACCESS_TO_CART_CONFIG
        );
        const {
            isEnabled = false,
            coolOffTime = 0,
            maxCount = 3,
        } = addSuprAccessToCartConfig;
        if (!isEnabled) {
            return undefined;
        }
        const suprAccessRemovedFromCartInfo = await this.cartService.getSuprPassRemovedInfo();
        const { removed = false, timestamp = new Date().getTime(), count = 0 } =
            suprAccessRemovedFromCartInfo || {};

        const canAutoAddSuprAccess =
            !removed ||
            (removed &&
                this.hasSuprAcessCoolOffPeriodPassed(timestamp, coolOffTime) &&
                count < maxCount);
        const isReferralCouponRemovedFromCart = await this.cartService.getIsReferralCouponRemovedFromCart();

        return {
            suprAccess: canAutoAddSuprAccess,
            referralCode: !isReferralCouponRemovedFromCart,
        };
    }

    private hasSuprAcessCoolOffPeriodPassed(
        removedTimestamp,
        coolOffTime
    ): boolean {
        const currentTimestamp = new Date().getTime();
        return currentTimestamp - removedTimestamp >= coolOffTime;
    }
}
