import { createFeatureSelector, createSelector } from "@ngrx/store";

import { CART_ITEM_TYPES, CART_DISCOUNT_TYPES } from "@constants";
import {
    CartItem,
    CartMeta,
    RechargeCouponInfo,
    RechargeCouponPaymentDetails,
} from "@models";
import { CartMini, CartItemDict, SkuDict, CouponCode } from "@types";

import { selectSkuDict } from "@store/sku/sku.selectors";
import { CartState } from "./cart.state";

export const selectFeature = createFeatureSelector<CartState>("cart");

export const selectCartItems = createSelector(
    selectFeature,
    (state: CartState) => state.cartItems
);

export const selectCartMeta = createSelector(
    selectFeature,
    (state: CartState) => state.cartMeta
);

export const selectCartCouponCode = createSelector(
    selectFeature,
    (state: CartState) => getCouponCode(state.cartMeta, state.couponApplying)
);

export const selectCartCurrentState = createSelector(
    selectFeature,
    (state: CartState) => state.currentState
);

export const selectMiniCart = createSelector(
    selectCartItems,
    selectSkuDict,
    (cartItems: CartItem[], skuDict: SkuDict) => getMiniCart(cartItems, skuDict)
);

export const selectCartId = createSelector(
    selectFeature,
    (state: CartState) => state.uuid
);

export const selectAddRewardSku = createSelector(
    selectFeature,
    (state: CartState) => state.addRewardSku || false
);

export const selectCartItemsDict = createSelector(
    selectFeature,
    (state: CartState) => state.cartItemsDict
);

export const selectCartItemQtyById = createSelector(
    selectCartItemsDict,
    (cartItemDict: CartItemDict, props: { skuId: number }) =>
        getItemQty(cartItemDict, props.skuId)
);

// NOTE: To select subscription_recharge cart item, pass sku id appended with "_SR"
// ex: skuId: 123_SR
export const selectCartItemById = createSelector(
    selectCartItemsDict,
    (cartItemDict: CartItemDict, props: { skuId: number }) =>
        getCartItem(cartItemDict, props.skuId)
);

export const selectCartError = createSelector(
    selectFeature,
    (state: CartState) => state.error
);

export const selectCartModified = createSelector(
    selectFeature,
    (state: CartState) => state.cartModified
);

export const selectUseSuprCredits = createSelector(
    selectFeature,
    (state: CartState) => state.useSuprCredits
);

export const selectCartReValidate = createSelector(
    selectFeature,
    (state: CartState) => state.reValidate
);

export const selectCashBackAmount = createSelector(
    selectCartMeta,
    (cartMeta: CartMeta) => getCashBackAmount(cartMeta)
);

export const selectCartOrder = createSelector(
    selectFeature,
    (state: CartState) => state.cartOrder
);

export const selectIsCouponApplying = createSelector(
    selectFeature,
    (state: CartState) => state.couponApplying
);

export const selectCartIsEmpty = createSelector(
    selectFeature,
    (state: CartState) => !state.cartItems || !state.cartItems.length
);

export const selectShowFloatingCart = createSelector(
    selectFeature,
    (state: CartState) => state.showFloatingCart
);

export const selectExitCartShowModal = createSelector(
    selectFeature,
    (state: CartState) => state.showExitCartModal
);

export const selectCartMessageContent = createSelector(
    selectFeature,
    (state: CartState) =>
        state.cartMeta &&
        state.cartMeta.message_info &&
        state.cartMeta.message_info.title
            ? state.cartMeta.message_info
            : null
);

export const selectisFirstPurchase = createSelector(
    selectFeature,
    (state: CartState) => state.cartOrder && state.cartOrder.isFirstPurchase
);

export const selectOrderCalendarBanner = createSelector(
    selectFeature,
    (state: CartState) =>
        (state.cartOrder && state.cartOrder.orderCalendarBanner) || null
);

export const selectCartDiscoutPaymentDetails = createSelector(
    selectCartMeta,
    (cartMeta: CartMeta) =>
        cartMeta &&
        cartMeta.discount_info &&
        cartMeta.discount_info.paymentDetails
);

export const selectCartDiscoutPaymentDetailsInstruments = createSelector(
    selectCartMeta,
    (cartMeta: CartMeta) =>
        cartMeta &&
        cartMeta.discount_info &&
        cartMeta.discount_info.paymentDetails
            ? cartMeta.discount_info.paymentDetails.instruments
            : null
);

export const selectCouponCodeInfoForWalletPayment = createSelector(
    selectCartCouponCode,
    selectCartDiscoutPaymentDetails,
    (
        cartCouponCode,
        paymentDetails: RechargeCouponPaymentDetails
    ): RechargeCouponInfo => {
        return {
            isApplied: cartCouponCode && cartCouponCode.isApplied,
            couponCode: cartCouponCode && cartCouponCode.couponCode,
            paymentDetails,
        };
    }
);

export const selectIsFetchingCartOffers = createSelector(
    selectFeature,
    (state: CartState) => state.fetchingOffers
);

export const selectCartOffers = createSelector(
    selectFeature,
    (state: CartState) => state.offers
);

// -----------------------------------//
//        Helper methods              //
// -----------------------------------//

function getItemQty(cartItemDict: CartItemDict, skuId: number): number {
    if (!cartItemDict || !cartItemDict[skuId]) {
        return 0;
    }

    const item = cartItemDict[skuId];
    return item.quantity;
}

function getCartItem(cartItemDict: CartItemDict, skuId: number): CartItem {
    if (
        cartItemDict &&
        cartItemDict[skuId] &&
        !cartItemDict[skuId].is_reward_sku
    ) {
        return cartItemDict[skuId];
    } else {
        return null;
    }
}

function getMiniCart(cartItems: CartItem[], skuDict: SkuDict): CartMini {
    const cart = {
        cartTotal: 0,
        itemCount: 0,
        items: [],
    };

    if (!cartItems || !cartItems.length || !Object.keys(skuDict).length) {
        return cart;
    }

    cartItems.forEach((item) => {
        if (!item || !skuDict[item.sku_id]) {
            return;
        }

        const sku = skuDict[item.sku_id];

        if (sku.is_virtual_sku) {
            if (
                sku.virtual_sku_info &&
                sku.virtual_sku_info.data &&
                sku.virtual_sku_info.data.plans
            ) {
                const planObj = sku.virtual_sku_info.data.plans.find(
                    (plan) => plan.id === item.plan_id
                );

                if (planObj && planObj.unitPrice) {
                    cart.cartTotal += planObj.unitPrice;
                }
            }
        } else {
            if (item.type === CART_ITEM_TYPES.ADDON) {
                cart.cartTotal += sku.unit_price * item.quantity;
            } else {
                cart.cartTotal += sku.unit_price * item.recharge_quantity;
            }
        }

        cart.itemCount += 1;

        // For analytics
        cart.items.push({
            item,
            sku,
        });
    });

    // Round off the cartTotal to 2 decimals
    try {
        const roundedValue = Math.round(Number(`${cart.cartTotal}e2`));
        cart.cartTotal = Number(`${roundedValue}e-2`);
    } catch (err) {
        console.log(err);
    }

    return cart;
}

function getCouponCode(
    cartMeta?: CartMeta,
    couponApplying?: boolean
): CouponCode {
    if (!cartMeta) {
        return null;
    }

    const { discount_info } = cartMeta;
    if (discount_info && discount_info.coupon_code) {
        return {
            couponCode: discount_info.coupon_code,
            isApplied: discount_info.is_applied,
            message: discount_info.message,
            couponApplying: couponApplying,
        };
    }

    return null;
}

function getCashBackAmount(cartMeta: CartMeta) {
    if (!cartMeta) {
        return 0;
    }

    const { discount_info } = cartMeta;

    if (discount_info) {
        const { is_applied, discount_amount, discount_type } = discount_info;
        if (is_applied && discount_type === CART_DISCOUNT_TYPES.CASH_BACK) {
            return discount_amount;
        }
    }

    return 0;
}
