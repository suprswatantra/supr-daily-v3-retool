import { TestBed } from "@angular/core/testing";

import { Store } from "@ngrx/store";
import { Actions } from "@ngrx/effects";
import { provideMockActions } from "@ngrx/effects/testing";
import { provideMockStore, MockStore } from "@ngrx/store/testing";

import { addDays, format } from "date-fns";

import { ApiService } from "@services/data/api.service";
import { UtilService } from "@services/util/util.service";
import { SkuService } from "@services/shared/sku.service";
import { CartService } from "@services/shared/cart.service";
import { ErrorService } from "@services/integration/error.service";
import { SettingsService } from "@services/shared/settings.service";
import { AnalyticsService } from "@services/integration/analytics.service";

import { StoreState } from "../store.state";
import { rootReducer } from "../store.reducer";
import { CartStoreEffects } from "./cart.effects";

import {
    BaseUnitTestImportsWithRouterIonic,
    BaseUnitTestProvidersIonic,
} from "./../../../unitTest.conf";

class SettingsServiceMock {
    private settingsData = {};

    getSettingsValue(key: string, defaultValue?: any) {
        return this.settingsData[key] || defaultValue;
    }
}

class CartServiceMock {
    setCartInDB() {}
}
describe("CartStoreEffects", () => {
    let cartStoreEffects: CartStoreEffects;

    let apiService: ApiService;
    let skuService: SkuService;
    let utilService: UtilService;
    let cartService: CartService;
    let errorService: ErrorService;
    let settingsService: SettingsService;
    let analyticsService: AnalyticsService;

    let actions$: Actions;
    let store$: MockStore<StoreState>;

    const initialState = { ...rootReducer };

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [...BaseUnitTestImportsWithRouterIonic],

            providers: [
                ApiService,
                SkuService,
                UtilService,
                { provide: CartService, useClass: CartServiceMock },
                ErrorService,
                AnalyticsService,
                { provide: SettingsService, useClass: SettingsServiceMock },
                provideMockActions(() => actions$),
                provideMockStore({ initialState }),
                ...BaseUnitTestProvidersIonic,
            ],
        });

        apiService = TestBed.get(ApiService);
        skuService = TestBed.get(SkuService);
        utilService = TestBed.get(UtilService);
        cartService = TestBed.get(CartService);
        errorService = TestBed.get(ErrorService);
        settingsService = TestBed.get(SettingsService);
        analyticsService = TestBed.get(AnalyticsService);

        actions$ = TestBed.get(Actions);
        store$ = TestBed.get(Store);

        cartStoreEffects = new CartStoreEffects(
            apiService,
            skuService,
            utilService,
            cartService,
            errorService,
            settingsService,
            analyticsService,

            actions$,
            store$
        );
    });

    const deliveryDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
    const cartItemsWithAddon = [
        {
            type: "addon",
            sku_id: 1,
            delivery_date: deliveryDate,
            quantity: 1,
            coupon_code: "ADD1",
            validation: {
                is_valid: true,
                message: "Ok",
            },
            payment_data: {
                pre_discount: 44,
                post_discount: 1,
                discount: 0,
            },
        },
    ];
    const subRechargeItem = {
        recharge_quantity: 6,
        sku_id: 2539,
        subscription_id: 1915518,
        type: "subscription_recharge",
    };

    it("#should create cart effects instance", () => {
        expect(cartStoreEffects).toBeTruthy();
    });

    it("should add cart items to local storage when only subscription \
    recharge is present", () => {
        const cartItems = [subRechargeItem];
        spyOn(cartService, "setCartInDB").and.returnValue(true);
        (cartStoreEffects as any).setCartInDB({ items: cartItems });
        expect(cartService.setCartInDB).toHaveBeenCalled();
    });

    it("should add cart items to local storage when cart items has subscription \
    recharge", () => {
        const cartItems = [...cartItemsWithAddon, subRechargeItem];
        spyOn(cartService, "setCartInDB").and.returnValue(true);
        (cartStoreEffects as any).setCartInDB({ items: cartItems });
        expect(cartService.setCartInDB).toHaveBeenCalled();
    });

    it("getUpdatedCartItems should only append subscription recharge item", () => {
        const cartItems = [...cartItemsWithAddon];
        const newItem = { ...subRechargeItem };
        const updatedItems = (cartStoreEffects as any).getUpdatedCartItems(
            cartItems,
            newItem
        );
        expect(updatedItems).toEqual([...cartItemsWithAddon, newItem]);
    });

    it("getUpdatedCartItems should upadte subscription recharge item's recharge qty when \
    qty changes", () => {
        const cartItems = [subRechargeItem];
        const newItem = { ...subRechargeItem, recharge_quantity: 30 };
        const updatedItems = (cartStoreEffects as any).getUpdatedCartItems(
            cartItems,
            newItem
        );
        expect(updatedItems).toEqual([newItem]);
    });

    it("getUpdatedCartItems should remove subscription recharge item on delete click \
    i.e qty=0", () => {
        const cartItems = [...cartItemsWithAddon, subRechargeItem];
        const newItem = { ...subRechargeItem, quantity: 0 };
        const updatedItems = (cartStoreEffects as any).getUpdatedCartItems(
            cartItems,
            newItem
        );
        expect(updatedItems).toEqual([...cartItemsWithAddon]);
    });

    it("getUpdatedCartItems should only remove subscription recharge item on delete click \
    i.e qty=0 when sku is present in both sub_recharge and addon/sub_new both", () => {
        const addOnItem = { ...cartItemsWithAddon[0], sku_id: 2539 };
        const cartItems = [addOnItem, subRechargeItem];
        const newItem = { ...subRechargeItem, quantity: 0 };
        const updatedItems = (cartStoreEffects as any).getUpdatedCartItems(
            cartItems,
            newItem
        );
        expect(updatedItems).toEqual([addOnItem]);
    });

    it("getUpdatedCartItems should only remove addon?sub_new item on delete click \
    i.e qty=0 when sku is present in both sub_recharge and addon/sub_new both", () => {
        const addOnItem = { ...cartItemsWithAddon[0], sku_id: 2539 };
        const cartItems = [addOnItem, subRechargeItem];
        const newItem = { ...addOnItem, quantity: 0 };
        const updatedItems = (cartStoreEffects as any).getUpdatedCartItems(
            cartItems,
            newItem
        );
        expect(updatedItems).toEqual([subRechargeItem]);
    });
});
