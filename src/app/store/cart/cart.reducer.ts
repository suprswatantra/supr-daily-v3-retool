import { CART_ITEM_TYPES } from "@constants";
import { Actions, ActionTypes } from "./cart.actions";
import { initialState, CartState, CartCurrentState } from "./cart.state";
import { Cart, CartItem } from "@models";
import { CartItemDict, SuprApi } from "@types";

export function cartReducer(state = initialState, action: Actions): CartState {
    switch (action.type) {
        case ActionTypes.UPDATE_ITEMS_IN_CART: {
            const cartItems = (action.payload.items || []).filter((i) => i);
            return {
                ...state,
                ..._getCartStateWithNewItemList(cartItems),
                uuid: action.payload.uuid,
                cartOrder: null,
                error: null,
            };
        }

        case ActionTypes.VALIDATE_CART_REQUEST: {
            return {
                ...state,
                currentState: CartCurrentState.UPDATING_CART,
                couponApplying: !!action.payload.couponApplying,
                reValidate: false,
                error: null,
            };
        }

        case ActionTypes.VALIDATE_CART_REQUEST_SUCCESS: {
            return {
                ...state,
                useSuprCredits: isSuprCreditsUsed(action.payload.cart),
                couponApplying: false,
                currentState: action.payload.isValid
                    ? CartCurrentState.NO_ACTION
                    : CartCurrentState.VALIDATION_FAILED,
                ..._getCartAfterValidation(action.payload.cart),
            };
        }

        case ActionTypes.VALIDATE_CART_REQUEST_FAILURE: {
            return {
                ...state,
                currentState: CartCurrentState.NO_ACTION,
                couponApplying: false,
                error: action.payload,
            };
        }

        case ActionTypes.CHECKOUT_CART_REQUEST: {
            return {
                ...state,
                currentState: CartCurrentState.PLACING_ORDER,
                error: null,
            };
        }

        case ActionTypes.CHECKOUT_CART_REQUEST_SUCCESS: {
            return {
                ...state,
                currentState: CartCurrentState.NO_ACTION,
                cartOrder: action.payload,
            };
        }

        case ActionTypes.CHECKOUT_CART_REQUEST_FAILURE: {
            return {
                ...state,
                error: action.payload.error,
                cartMeta: {
                    ...(state.cartMeta || {}),
                    ...(action.payload.meta || {}),
                },
                currentState: CartCurrentState.CHECKOUT_FAILED,
            };
        }

        case ActionTypes.UPDATE_CART: {
            return {
                ...state,
                ..._getCartFromCartBody(action.payload.cartBody),
            };
        }

        case ActionTypes.UPDATE_CART_ADD_REWARD_SKU: {
            return {
                ...state,
                addRewardSku: action.payload,
            };
        }

        case ActionTypes.ENABLE_RE_VALIDATE: {
            return {
                ...state,
                reValidate: true,
            };
        }

        case ActionTypes.CLEAR_CART:
            return { ...initialState, cartOrder: state.cartOrder };

        case ActionTypes.TOGGLE_FLOATING_CART: {
            return {
                ...state,
                showFloatingCart: !state.showFloatingCart,
            };
        }

        case ActionTypes.USE_SUPR_CREDITS_IN_CART: {
            return {
                ...state,
                useSuprCredits: action.payload.useSuprCredits,
            };
        }

        case ActionTypes.SHOW_EXIT_CART_MODAL: {
            return {
                ...state,
                showExitCartModal: true,
            };
        }

        case ActionTypes.HIDE_EXIT_CART_MODAL: {
            return {
                ...state,
                showExitCartModal: false,
            };
        }

        case ActionTypes.FETCH_CART_OFFERS_REQUEST: {
            return {
                ...state,
                fetchingOffers: true,
            };
        }

        case ActionTypes.FETCH_CART_OFFERS_REQUEST_SUCCESS: {
            return {
                ...state,
                fetchingOffers: false,
                offers: action.payload.offersList || [],
            };
        }

        case ActionTypes.FETCH_CART_OFFERS_REQUEST_FAILURE: {
            return {
                ...state,
                fetchingOffers: false,
                offers: [],
            };
        }

        default:
            return state;
    }
}

// -----------------------------------//
//        Helper methods              //
// -----------------------------------//
function _getCartStateWithNewItemList(cartItems: CartItem[]) {
    const cartItemsDict = convertItemsToDict(cartItems);

    return {
        cartItems,
        cartItemsDict,
        cartModified: true,
    };
}

function _getCartAfterValidation(cart: Cart) {
    const cartItems = ((cart && cart.items) || []).filter((i) => i);
    const cartMeta = cart.meta;
    const cartItemsDict = convertItemsToDict(cartItems);
    const uuid = cart.uuid;

    return {
        cartItems,
        cartMeta,
        cartItemsDict,
        cartModified: false,
        uuid,
    };
}

function _getCartFromCartBody(cart: SuprApi.CartBody) {
    const cartItems = ((cart && cart.items) || []).filter((i) => i);
    const cartItemsDict = convertItemsToDict(cartItems);
    const uuid = cart.uuid;

    return {
        cartItems,
        cartItemsDict,
        cartModified: true,
        uuid,
    };
}

function convertItemsToDict(items: CartItem[]): CartItemDict {
    return items.reduce((dict, item) => {
        if (item && item.sku_id && item.type) {
            if (!item.is_reward_sku) {
                if (item.type !== CART_ITEM_TYPES.SUBSCRIPTION_RECHARGE) {
                    dict[item.sku_id] = item;
                } else {
                    dict[`${item.sku_id}_SR`] = item;
                }
            } else {
                dict[`${item.sku_id}_RW`] = item;
            }
        }
        return dict;
    }, {});
}

function isSuprCreditsUsed(cart: Cart) {
    if (
        !cart ||
        !cart.meta ||
        !cart.meta.payment_data ||
        !cart.meta.payment_data.supr_credits_used
    ) {
        return false;
    }
    return true;
}
