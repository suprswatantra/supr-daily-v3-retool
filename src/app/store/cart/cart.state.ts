import { CartItem, CartMeta, CartOrder, OffersList } from "@models";
import { CartItemDict } from "@types";

export enum CartCurrentState {
    NO_ACTION = "0",
    UPDATING_CART = "1",
    PLACING_ORDER = "2",
    VALIDATION_FAILED = "3",
    CHECKOUT_FAILED = "4",
}

export interface CartState {
    currentState: CartCurrentState;
    cartItems?: CartItem[];
    cartMeta?: CartMeta;
    cartItemsDict?: CartItemDict;
    cartModified?: boolean;
    couponApplying?: boolean;
    reValidate?: boolean;
    error?: any;
    uuid?: string;
    cartOrder?: CartOrder;
    showFloatingCart?: boolean;
    showExitCartModal: boolean;
    useSuprCredits?: boolean;
    offers: OffersList;
    fetchingOffers: boolean;
    addRewardSku?: boolean;
}

export const initialState: CartState = {
    currentState: CartCurrentState.NO_ACTION,
    cartItems: [],
    cartItemsDict: null,
    cartMeta: null,
    cartModified: false,
    couponApplying: false,
    reValidate: false,
    error: null,
    uuid: "",
    cartOrder: null,
    showFloatingCart: true,
    showExitCartModal: false,
    useSuprCredits: true,
    offers: [],
    fetchingOffers: false,
    addRewardSku: false,
};
