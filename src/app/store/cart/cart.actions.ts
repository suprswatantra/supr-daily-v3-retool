import { Action } from "@ngrx/store";

import { Cart, CartItem, CartOrder, CheckoutError, OffersList } from "@models";
import { SuprApi } from "@types";

export enum ActionTypes {
    UPDATE_ITEM_IN_CART = "[Cart] Update item in cart",
    UPDATE_ITEMS_IN_CART = "[Cart] Update item list in cart",
    UPDATE_COUPON_IN_CART = "[Cart] Update coupon in cart",

    USE_SUPR_CREDITS_IN_CART = "[Cart] Use supr credits in cart",

    UPDATE_CART = "[Cart] Update the cart",
    CLEAR_CART = "[Cart] Clear the cart",

    VALIDATE_CART_REQUEST = "[Cart] Validate cart request",
    VALIDATE_CART_REQUEST_SUCCESS = "[Cart] Validate cart request success",
    VALIDATE_CART_REQUEST_FAILURE = "[Cart] Validate cart request failure",

    CHECKOUT_CART_REQUEST = "[Cart] Checkout cart request",
    CHECKOUT_CART_REQUEST_SUCCESS = "[Cart] Checkout cart request success",
    CHECKOUT_CART_REQUEST_FAILURE = "[Cart] Checkout cart request failure",

    ENABLE_RE_VALIDATE = "[Cart] Enable revalidate",

    AUTOPUSH_CART_ITEMS = "[Cart] Autopush items in the cart",

    TOGGLE_FLOATING_CART = "[Cart] Toggle floating cart",

    SHOW_EXIT_CART_MODAL = "[Cart] Exit Show Modal",
    HIDE_EXIT_CART_MODAL = "[Cart] Exit Hide Modal",

    FETCH_CART_OFFERS_REQUEST = "[Cart] Fetch cart offers request",
    FETCH_CART_OFFERS_REQUEST_SUCCESS = "[Cart] Fetch cart offers request success",
    FETCH_CART_OFFERS_REQUEST_FAILURE = "[Cart] Fetch cart offers request failure",

    UPDATE_CART_ADD_REWARD_SKU = "[Cart] update add reward sku",
}

export class UpdateItemInCartAction implements Action {
    readonly type = ActionTypes.UPDATE_ITEM_IN_CART;
    constructor(
        public payload: {
            item: CartItem;
            validateCart?: boolean;
            addRewardSku?: boolean;
        }
    ) {}
}

export class UpdateAddRewardSku implements Action {
    readonly type = ActionTypes.UPDATE_CART_ADD_REWARD_SKU;
    constructor(public payload: boolean) {}
}

export class UpdateItemsInCartAction implements Action {
    readonly type = ActionTypes.UPDATE_ITEMS_IN_CART;
    constructor(public payload: { items: CartItem[]; uuid?: string }) {}
}

export class UpdateCouponInCartAction implements Action {
    readonly type = ActionTypes.UPDATE_COUPON_IN_CART;
    constructor(public payload: { couponCode?: string }) {}
}

export class UseSuprCreditsInCartAction implements Action {
    readonly type = ActionTypes.USE_SUPR_CREDITS_IN_CART;
    constructor(public payload: { useSuprCredits?: boolean }) {}
}

export class ValidateCartRequestAction implements Action {
    readonly type = ActionTypes.VALIDATE_CART_REQUEST;
    constructor(
        public payload?: {
            cartBody: SuprApi.CartBody;
            couponApplying?: boolean;
            addRewardSku?: boolean;
        }
    ) {}
}

export class ValidateCartRequestSuccessAction implements Action {
    readonly type = ActionTypes.VALIDATE_CART_REQUEST_SUCCESS;
    constructor(public payload: { cart: Cart; isValid: boolean }) {}
}

export class ValidateCartRequestFailureAction implements Action {
    readonly type = ActionTypes.VALIDATE_CART_REQUEST_FAILURE;
    constructor(public payload: any) {}
}

export class CheckoutCartRequestAction implements Action {
    readonly type = ActionTypes.CHECKOUT_CART_REQUEST;
}

export class CheckoutCartRequestSuccessAction implements Action {
    readonly type = ActionTypes.CHECKOUT_CART_REQUEST_SUCCESS;
    constructor(public payload: CartOrder) {}
}

export class CheckoutCartRequestFailureAction implements Action {
    readonly type = ActionTypes.CHECKOUT_CART_REQUEST_FAILURE;
    constructor(public payload: { error?: any; meta?: CheckoutError }) {}
}

export class UpdateCartAction implements Action {
    readonly type = ActionTypes.UPDATE_CART;
    constructor(public payload?: { cartBody: SuprApi.CartBody }) {}
}

export class ClearCartAction implements Action {
    readonly type = ActionTypes.CLEAR_CART;
}

export class EnableRevalidateAction implements Action {
    readonly type = ActionTypes.ENABLE_RE_VALIDATE;
}

export class AutoPushCartItems implements Action {
    readonly type = ActionTypes.AUTOPUSH_CART_ITEMS;
}

export class ToggleFloatingCart implements Action {
    readonly type = ActionTypes.TOGGLE_FLOATING_CART;
}

export class ShowExitCartModal implements Action {
    readonly type = ActionTypes.SHOW_EXIT_CART_MODAL;
}

export class HideExitCartModal implements Action {
    readonly type = ActionTypes.HIDE_EXIT_CART_MODAL;
}

export class FetchCartOffersRequestAction implements Action {
    readonly type = ActionTypes.FETCH_CART_OFFERS_REQUEST;
}

export class FetchCartOffersRequestSuccessAction implements Action {
    readonly type = ActionTypes.FETCH_CART_OFFERS_REQUEST_SUCCESS;
    constructor(public payload: { offersList: OffersList }) {}
}

export class FetchCartOffersRequestFailureAction implements Action {
    readonly type = ActionTypes.FETCH_CART_OFFERS_REQUEST_FAILURE;
}

export type Actions =
    | UpdateItemInCartAction
    | UpdateItemsInCartAction
    | UpdateCouponInCartAction
    | UseSuprCreditsInCartAction
    | ValidateCartRequestAction
    | ValidateCartRequestSuccessAction
    | ValidateCartRequestFailureAction
    | CheckoutCartRequestAction
    | CheckoutCartRequestSuccessAction
    | CheckoutCartRequestFailureAction
    | UpdateCartAction
    | ClearCartAction
    | EnableRevalidateAction
    | AutoPushCartItems
    | ToggleFloatingCart
    | ShowExitCartModal
    | HideExitCartModal
    | FetchCartOffersRequestAction
    | FetchCartOffersRequestSuccessAction
    | FetchCartOffersRequestFailureAction
    | UpdateAddRewardSku;
