/** Version to full_version/check_version conversion
 *
 * version -> x.y.x
 * check_version -> (x * 10^4) + (y * 10^2) +z
 * ex: 3.1.0 => 30000 + 100 + 0 = 30100
 * ex: 3.0.95 => 30000 + 0 + 95 = 30095
 * ex: 3.0.1 => 30000 + 0 + 1 = 30001
 */
export const environment = {
    production: true,
    api_host: "https://api.suprdaily.com/",
    app_api_host: "https://app.api.suprdaily.com/api/",
    cx_host: "http://complaints-api.internal.suprdaily.com/",
    name: "Supr Daily 2.0",
    version: "30117",
    contact: "+919699000035",
    ionic_patch_version: 9,
    ionic_build: false,
    android: {
        package_name: "com.supr.suprdaily",
        version: "3.1.17",
        full_version: "30117",
        check_version: 30117,
    },
    ios: {
        name: "com.supr.suprdailyios",
        id: 1458875299,
        version: "3.0.14",
        full_version: "30014",
        check_version: 30014,
    },
    analytics: {
        segment: "mp489kgvocgH9j26tz6gWWz42RCMFxNU",
        ga: "UA-76696244-5",
    },
    sentry: {
        key: "c7faa7738c714a57b0fa5eaa9f87f40f",
        project: "1778950",
    },
    oneSignal: {
        key: "7294c347-0c62-4d96-b679-bdf336c20fc4",
    },
    appsFlyer: {
        key: "fyHqBAJ4PJQPp7XzT7tSLe",
    },
    freshChat: {
        appId: "b7a7326e-a4b7-4ea3-9904-a84c3857bcdf",
        appKey: "db1fd9af-ce22-42d9-b1db-d6da66bc93fb",
    },
    razorpay: {
        key: "rzp_live_5v8iCUVoBZca3J",
    },
    mapKey: {
        android: "AIzaSyCDrbb0P5E9INfPvF7gdqqtqmWk0cPTfwQ",
    },
    smartlook: {
        apiKey: "806fea63992d1bc78bf763ba8f3addaa05fbfb37",
    },
    freshbot: {
        dataClient: "ddae76e0fb6e8a39c8b90c046a68737cacec4d8c",
        dataBotHash: "4e973abbbd536e43fa3799d21e36232ab4ac8db6",
    },
    rudderStack: {
        dataPlaneUrl: "https://suprdaily-dataplane.rudderstack.com/",
        writeKey: "1fwUyiwCVTN0FUksl4ItrbqO9BN",
    },
};
